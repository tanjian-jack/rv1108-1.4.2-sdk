//
//  MainDevObj.h
//  UI
//
//  Created by leiqibin on 16-4-12.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainDevObj : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * host;
@property (nonatomic, strong) NSString * form;
@end
