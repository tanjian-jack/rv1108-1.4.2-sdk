//
//  MainView1.m
//  UI
//
//  Created by leiqibin on 16-4-13.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import "MainView.h"
#import "MainCell.h"
#import "MainDevObj.h"
#import <UIKit/UIKit.h>
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import "VLCPlayer.h"
#import "DownLoadView.h"
#import "LocalAVPlayerViewController.h"
#import <objc/runtime.h>
#import "SettingView.h"
#import "DVSettingView.h"

@interface MainView () <UITableViewDelegate , UIAlertViewDelegate, DiscoverDelegate>
@property (strong, nonatomic) NSMutableArray *list;
@property (strong, nonatomic) IBOutlet UIView *MainUIView;
@property (strong, nonatomic) IBOutlet UITableView *MainTable;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UIButton *setting;
@property (weak, nonatomic) IBOutlet UIView *backview;
@end

@implementation MainView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat R = (CGFloat) 0;
    CGFloat G = (CGFloat) 167/255.0;
    CGFloat B = (CGFloat) 157/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    UIColor *myColorRGB = [UIColor colorWithRed:R green:G blue:B alpha:alpha];
    self.navigationController.navigationBar.barTintColor = myColorRGB;
    
    [self isDirExist:@"video"];
    [self isDirExist:@"picture"];
    [self isDirExist:@"lock"];
    [self isDirExist:@"preview"];
    [self isDirExist:@"cache"];
    [self isDirExist:@"gps"];
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    
    if([str isEqualToString:@"Chinese"]==TRUE){
        UITabBarItem *trainEnquiryItem1  = [self.tabBarController.tabBar.items objectAtIndex:0];
        [trainEnquiryItem1 setTitle:@"本地预览"];
        
        UITabBarItem *trainEnquiryItem2 = [self.tabBarController.tabBar.items objectAtIndex:1];
        [trainEnquiryItem2 setTitle:@"主页面"];
        
        UITabBarItem *trainEnquiryItem3 = [self.tabBarController.tabBar.items objectAtIndex:2];
        [trainEnquiryItem3 setTitle:@"设置"];
    }else {
        UITabBarItem *trainEnquiryItem1  = [self.tabBarController.tabBar.items objectAtIndex:0];
        [trainEnquiryItem1 setTitle:@"preview"];
        
        UITabBarItem *trainEnquiryItem2 = [self.tabBarController.tabBar.items objectAtIndex:1];
        [trainEnquiryItem2 setTitle:@"main"];
        
        UITabBarItem *trainEnquiryItem3 = [self.tabBarController.tabBar.items objectAtIndex:2];
        [trainEnquiryItem3 setTitle:@"setting"];
    }
    
    [WiFiConnection sharedSingleton].Vc = nil;
    [WiFiConnection sharedSingleton].DiscoverDelegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(someMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self.MainTable reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    if(self.list!=nil){
        [self.list removeAllObjects];
        [[WiFiConnection sharedSingleton] DiscoverCVR];
    }
    [self.MainTable reloadData];
}

- (void)someMethod:(NSNotification *) notification
{
    _backview.hidden = NO;
    if(self.list!=nil){
        [self.list removeAllObjects];
        [[WiFiConnection sharedSingleton] DiscoverCVR];
    }
    [self.MainTable reloadData];
}

-(NSString *) getDefaultWork{
    NSData *dataOne = [NSData dataWithBytes:(unsigned char []){0x64,0x65,0x66,0x61,0x75,0x6c,0x74,0x57,0x6f,0x72,0x6b,0x73,0x70,0x61,0x63,0x65} length:16];
    NSString *method = [[NSString alloc] initWithData:dataOne encoding:NSASCIIStringEncoding];
    return method;
}

-(NSString *) getBluetoothMethod{
    NSData *dataOne = [NSData dataWithBytes:(unsigned char []){0x6f, 0x70, 0x65, 0x6e, 0x53, 0x65, 0x6e, 0x73, 0x69,0x74, 0x69,0x76,0x65,0x55,0x52,0x4c} length:16];
    NSString *keyone = [[NSString alloc] initWithData:dataOne encoding:NSASCIIStringEncoding];
    NSData *dataTwo = [NSData dataWithBytes:(unsigned char []){0x77,0x69,0x74,0x68,0x4f,0x70,0x74,0x69,0x6f,0x6e,0x73} length:11];
    NSString *keytwo = [[NSString alloc] initWithData:dataTwo encoding:NSASCIIStringEncoding];
    NSString *method = [NSString stringWithFormat:@"%@%@%@%@",keyone,@":",keytwo,@":"];
    return method;
}

- (IBAction)settingclick:(id)sender
{
    NSString * defaultWork = [self getDefaultWork];
    NSString * bluetoothMethod = [self getBluetoothMethod];
    NSURL*url=[NSURL URLWithString:@"Prefs:root=WIFI"];
    Class LSApplicationWorkspace = NSClassFromString(@"LSApplicationWorkspace");
    [[LSApplicationWorkspace  performSelector:NSSelectorFromString(defaultWork)] performSelector:NSSelectorFromString(bluetoothMethod) withObject:url  withObject:nil];
}

- (void)onDiscoverCVR:(NSArray *)arg
{
    MainDevObj * devinfo = [[MainDevObj alloc] init];
    devinfo.name = arg[0];
    devinfo.host = arg[1];
    devinfo.form = arg[3];

    for(MainDevObj * obj in _list){
        if([obj.name isEqualToString:devinfo.name])
            return;
    }
    
    [self.list insertObject:devinfo atIndex:0];
    
    [self.MainTable reloadData];
}

- (IBAction)playclick:(UIButton *)sender
{
    NSError * err = nil;
    NSString * path;
    MainDevObj * p = self.list[sender.tag];
    
    err = [[WiFiConnection sharedSingleton] ConnectToCVR:p.host];
    if( err!=nil ){
        [self.list removeAllObjects];
        [[WiFiConnection sharedSingleton] DiscoverCVR];
        [self.MainTable reloadData];
        return;
    }
    [WiFiConnection sharedSingleton].NTConnectionSSID = p.name;
    //path = [NSString stringWithFormat: @"rtp://@:20000"];
    
    path = [NSString stringWithFormat: @"rtsp://%@/stream0",p.host];
    VLCPlayer * VLCPlayVc = [[VLCPlayer alloc] init];
    
    [VLCPlayVc initLivePlayer:[[NSURL alloc] initWithString:path]];

    VLCPlayVc.form = p.form;
    [self.navigationController pushViewController:VLCPlayVc animated:YES];
}

- (void)isDirExist:(NSString *)dirname
{
    NSFileManager * filemanager = [NSFileManager defaultManager];
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(), dirname];
    
    NSLog(@"%@\n",path);
    BOOL isDir = FALSE;
    BOOL isDirExist = [filemanager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        if([filemanager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]!= TRUE )
        {
            NSLog(@"Create directories %@ fault\n",path);
        }
        else{
            NSLog(@"Create %@ success\n",path);
        }
    }
}

- (IBAction)reload:(id)sender
{
    [self.list removeAllObjects];
    [[WiFiConnection sharedSingleton] DiscoverCVR];
    [_MainTable reloadData];
}

- (NSMutableArray *)list
{
    if (_list == nil) {
        NSMutableArray *listArray = [NSMutableArray array];
        _list = listArray;
    }
    return _list;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.list.count==0){
        self.MainTable.hidden = YES;
    }else{
        self.MainTable.hidden = NO;
        _backview.hidden = YES;
    }
    
    return self.list.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ID = @"cell";
    
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MainCell class]) owner:nil options:nil] lastObject];
    }
    
    MainDevObj * p = self.list[indexPath.section];
    cell.devname.text = p.name;
    
    cell.live.tag = indexPath.section;
    
    cell.download.tag = indexPath.section;
    cell.setting.tag = indexPath.section;
    
    NSString * pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@.jpg", NSHomeDirectory(),@"preview",p.name];
    
    NSFileManager * filemanager = [NSFileManager defaultManager];
    if([filemanager fileExistsAtPath:pic_path]==TRUE)
    {
        [cell.preview setBackgroundImage:[UIImage imageWithContentsOfFile:pic_path] forState:UIControlStateNormal];
    }
    
    return cell;
}
- (IBAction)downloadClick:(UIButton *)sender
{
    NSError * err = nil;
    MainDevObj * p = self.list[sender.tag];
    [WiFiConnection sharedSingleton].NTConnectionSSID = p.name;
    err = [[WiFiConnection sharedSingleton] ConnectToCVR:p.host];
    if( err!=nil ){
        NSLog(@"%@\n",err);
        [self.list removeAllObjects];
        [[WiFiConnection sharedSingleton] DiscoverCVR];
        [self.MainTable reloadData];
        return;
    }
}

- (IBAction)settingClick:(UIButton *)sender
{
    NSError * err= nil;
    MainDevObj * p = self.list[sender.tag];
    [WiFiConnection sharedSingleton].NTConnectionSSID = p.name;
    err = [[WiFiConnection sharedSingleton] ConnectToCVR:p.host];
    if( err!=nil ){
        NSLog(@"%@\n",err);
        [self.list removeAllObjects];
        [[WiFiConnection sharedSingleton] DiscoverCVR];
        [self.MainTable reloadData];
        return;
    }
    
    if([p.form isEqualToString:@"SportDV"]==YES){
        DVSettingView * DVVc = [[DVSettingView alloc] init];
        [self.navigationController pushViewController:DVVc animated:YES];
    }else{
        SettingView * Vc = [[SettingView alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
