//
//  MainCell.h
//  UI
//
//  Created by leiqibin on 16-4-12.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainDevObj;

@interface MainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *preview;
@property (weak, nonatomic) IBOutlet UIImageView *radio;
@property (weak, nonatomic) IBOutlet UIButton *setting;
@property (weak, nonatomic) IBOutlet UIButton *download;
@property (weak, nonatomic) IBOutlet UILabel *devname;
@property (weak, nonatomic) IBOutlet UIButton *live;
@property (nonatomic, strong) MainDevObj *devlist;
@end
