//
//  GPSObject.h
//  CVR
//
//  Created by rk on 04/08/2017.
//  Copyright © 2017 雷起斌. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GPSObject : NSObject
@property (strong, nonatomic) NSString *gps;
@property (strong, nonatomic) NSMutableArray * list;
@end
