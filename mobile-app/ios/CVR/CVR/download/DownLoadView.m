//
//  DownLoadView.m
//  UI
//
//  Created by leiqibin on 16-4-17.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import "DownLoadView.h"
#import "DownLoadCell.h"
#import "DownInfo.h"

#import "SDProgressView.h"
#import "SDDemoItemView.h"
#import "VLCPlayer.h"
#import "LocalAVPlayerViewController.h"
#import "MBProgressHUD+XMG.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"
//#import "ZoomingViewController.h"
#import "FLImageShowVC.h"

@interface DownLoadView () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NSURLSessionDownloadDelegate, DirectoryContentsDelegate, DeleteFileDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftbarbutton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *BarButton;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) UIButton * BTDownload;
@property (strong, nonatomic) UIButton * BTDelete;
@property (strong, nonatomic) UIView * ButtonControl;
@property (strong, nonatomic) NSOperationQueue *queue;
@property (strong, nonatomic) NSFileManager * filemanager;
@property (strong, nonatomic) NSMutableArray * list;
@property (weak, nonatomic) IBOutlet UIProgressView *deleteprogress;
@property (strong, nonatomic) NSMutableArray * downloadlist;
@property (weak, nonatomic) IBOutlet UITextField *deletetext;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, assign) NSInteger leftbarbuttonMode;
@property (nonatomic, assign) NSInteger deletecount;
@property (nonatomic, assign) BOOL isDelete;
@property (strong, nonatomic) NSThread * thread;
@end

@implementation DownLoadView

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@" "];
    
    _filemanager= [NSFileManager defaultManager];
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 1;
    _downloadlist = [NSMutableArray array];
    
    self.table.dataSource = self;
    self.table.delegate = self;
    self.table.allowsMultipleSelectionDuringEditing = YES;
    
    CGFloat R = (CGFloat) 249/255.0;
    CGFloat G = (CGFloat) 249/255.0;
    CGFloat B = (CGFloat) 249/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    
    _ButtonControl = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height- 49, [UIScreen mainScreen].bounds.size.width, 49)];
    _ButtonControl.hidden = YES;
    _ButtonControl.backgroundColor = myColorRGB;
    
    _BTDelete = [[UIButton alloc] init];
    [_BTDelete setBackgroundImage:[UIImage imageNamed:@"icon_delete_01"] forState:UIControlStateNormal];
    _BTDelete.frame = CGRectMake(_ButtonControl.center.x/2-15, (49-30)/2 , 30, 30);
    [_BTDelete addTarget:self action:@selector(btdeleteclick:) forControlEvents:UIControlEventTouchUpInside];
    [_ButtonControl addSubview:_BTDelete];
    
    _BTDownload = [[UIButton alloc] init];
    [_BTDownload setBackgroundImage:[UIImage imageNamed:@"icon_download"] forState:UIControlStateNormal];
    _BTDownload.frame = CGRectMake(_ButtonControl.center.x+_ButtonControl.center.x/2-15, (49-30)/2 , 30, 30);
    [_BTDownload addTarget:self action:@selector(btdownloadclick:) forControlEvents:UIControlEventTouchUpInside];
    [_ButtonControl addSubview:_BTDownload];
    
    R = (CGFloat) 178/255.0;
    G = (CGFloat) 178/255.0;
    B = (CGFloat) 178/255.0;
    
    myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    UIView * temp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _ButtonControl.frame.size.width, 0.5)];
    temp.backgroundColor = myColorRGB;
    [_ButtonControl addSubview:temp];
    
    [self.view addSubview:_ButtonControl];
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _leftbarbutton.title = @"返回";
        _BarButton.title  = @"选择";
    }else {
        _leftbarbutton.title = @"Back";
        _BarButton.title  = @"Edit";
    }
    
    [WiFiConnection sharedSingleton].GetDirectoryContentsDelegate = self;
    [WiFiConnection sharedSingleton].DeleteFileDelegate = self;
    [self.list removeAllObjects];
    [[WiFiConnection sharedSingleton] GetDirectoryContents];
}

- (void)isDirExist:(NSString *)dirname
{
    NSFileManager * filemanager = [NSFileManager defaultManager];
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/gps/%@/", NSHomeDirectory(), dirname];
    
    BOOL isDir = FALSE;
    BOOL isDirExist = [filemanager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        if([filemanager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]!= TRUE )
        {
            NSLog(@"Create directories %@ fault\n",path);
        }
        else{
            NSLog(@"Create %@ success\n",path);
        }
    }
}
#if 1
-(void)downloadgps:(GPSObject *)obj
{
    NSArray * tmp_list = [obj.list copy];
    
    if(_downloadlist.count==0)
        return;
    DownInfo * fileinfo = _downloadlist[0];

    if([fileinfo.fileinfo.name isEqualToString:obj.gps]==NO){
        for(DownInfo * fileinfo in _downloadlist){
            if([fileinfo.fileinfo.name isEqualToString:obj.gps]==YES){
                break;
            }
        }
    }
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/gps/%@&%@/", NSHomeDirectory(), [[tmp_list firstObject] substringWithRange:NSMakeRange(0, 15)],[[tmp_list lastObject] substringWithRange:NSMakeRange(0, 15)]];
    
    BOOL isDir = FALSE;
    BOOL isDirExist = [_filemanager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        if([_filemanager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]!= TRUE )
        {
            NSLog(@"Create directories %@ fault\n",path);
            return;
        }
        else{
            NSLog(@"Create %@ success\n",path);
        }
    }
    
    for(NSString * filename in tmp_list){
        NSString * gps_path = [path stringByAppendingString:filename];
        if([_filemanager fileExistsAtPath:gps_path]!=YES){
            NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@", [WiFiConnection sharedSingleton].NTConnectionHost, fileinfo.fileinfo.gps, obj.gps];
            NSLog(@"%@ %@\n",path, gps_path);
            NSURL * url = [NSURL URLWithString:path];
            NSData *data = [NSData dataWithContentsOfURL:url];
            if([[NSThread currentThread] isCancelled]){
                [NSThread exit];
            }
            [data writeToFile:gps_path atomically:NO];
            if([[NSThread currentThread] isCancelled]){
                [NSThread exit];
            }
        }
    }
    
    [self resumeDownload:fileinfo];
}
#endif
- (void)GetGpsList:(NSNotification *) notification
{
    GPSObject * obj = [notification object];
#if 1
//    _thread = [[NSThread alloc] initWithTarget:self selector:@selector(downloadgps:) object:obj];
//    [_thread start];
#else
    DownInfo * fileinfo = _downloadlist[0];
    
//    NSLog(@"%@\n gps= %@\n",obj.list, obj.gps);
    
    if([fileinfo.fileinfo.name isEqualToString:obj.gps]==NO){
        for(DownInfo * fileinfo in _downloadlist){
            if([fileinfo.fileinfo.name isEqualToString:obj.gps]==YES){
                break;
            }
        }
    }
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/gps/%@&%@/", NSHomeDirectory(), [[obj.list firstObject] substringWithRange:NSMakeRange(0, 15)],[[obj.list lastObject] substringWithRange:NSMakeRange(0, 15)]];
    
    BOOL isDir = FALSE;
    BOOL isDirExist = [_filemanager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        if([_filemanager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]!= TRUE )
        {
            NSLog(@"Create directories %@ fault\n",path);
            return;
        }
        else{
            NSLog(@"Create %@ success\n",path);
        }
    }
    
    for(NSString * filename in obj.list){
        NSString * gps_path = [path stringByAppendingString:filename];
        if([_filemanager fileExistsAtPath:gps_path]!=YES){
            NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@", [WiFiConnection sharedSingleton].NTConnectionHost, fileinfo.fileinfo.gps, filename];
            NSURL * url = [NSURL URLWithString:path];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [data writeToFile:gps_path atomically:NO];
        }
    }
    
    [self resumeDownload:fileinfo];
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
    NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(GetGpsList:) name:@"GetGpsList" object:nil];
    
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}

- (void)onDeleteFileSuccess:(NTFileObj *)fileinfo
{
    if(fileinfo==nil){
        NSLog(@"onDeleteFileSuccess fault\n");
        while(1);
    }
    NSLog(@"delete %@ success\n",fileinfo.name);

    if([_downloadlist count]==0){
        _deletetext.hidden = YES;
        _deleteprogress.hidden = YES;
    }else{
        NSString * text = [NSString stringWithFormat:@"%zi/%zi",_deletecount- [_downloadlist count],_deletecount];
        _deletetext.text = text;
        _deleteprogress.progress = (_deletecount- [_downloadlist count])/_deletecount;
    }
    
    for(DownInfo * key in self.list){
        if( [fileinfo.name isEqualToString:key.fileinfo.name]==YES ){
            [self.list removeObject:key];
            return;
        }
    }
}

-(void)onDeleteFileFault:(NSString *)file
{
    NSLog(@"delete %@ fault\n",file);
    return;
}

-(void)onDeleteFileFinish
{
    [_loading stopAnimating];
    [_table reloadData];

    _isDelete = NO;
    _table.scrollEnabled = YES;
    _leftbarbutton.enabled = YES;
    _BarButton.enabled = YES;
    _BTDelete.enabled = YES;
    _BTDownload.enabled = YES;
}

- (void)getFileFinish:(NSNotification *) notification
{
    //[self.table reloadData];
}

- (void)onDirectoryContents:(NTFileObj *)fileinfo
{
    DownInfo * file = [[DownInfo alloc] init];
    file.preview = [UIImage imageNamed:@"img_nopic_01"];
    if(fileinfo==nil){
        NSLog(@"download list fileinfo fault\n");
        return ;
    }
    file.fileinfo = fileinfo;
    
    if([self checkDownloadFileExist:file]==TRUE)
        file.state = 2;
    else {
        file.state = 0;
        [self countProgress:file];
    }
    [self.list insertObject:file atIndex:0];
    [self.table reloadData];
    
    if([[fileinfo.name pathExtension] isEqualToString:@"jpg"]==YES||[[fileinfo.name pathExtension] isEqualToString:@"JPG"]==YES){
        NSBlockOperation * operation = [NSBlockOperation blockOperationWithBlock:
        ^{
            NSString * cachepath = [NSString stringWithFormat: @"%@/Documents/%@/%@", NSHomeDirectory(), @"preview",fileinfo.name];
            if([_filemanager fileExistsAtPath:cachepath]==NO){
                NSString * path = [self SearchHTTPPathForName:fileinfo.path :fileinfo.name];
                NSData * resuiltData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
                if(resuiltData!=nil){
                    UIImage * img = [UIImage imageWithData:resuiltData];
                    if(img!=nil){
                        file.preview = img;
                        [UIImageJPEGRepresentation(img, 1.0) writeToFile:cachepath atomically:YES];
                    }else{
                        NSLog(@"获取jpg预览错误\n");
                        file.preview = [UIImage imageNamed:@"img_nopic_01"];
                    }
                }
            }else{
                file.preview = [UIImage imageWithContentsOfFile:cachepath];
                if(file.preview==nil){
                    NSLog(@"从本地获取缓存缩略图错误\n");
                    while(1);
                }
            }
        
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSArray * SelectedRowsindexPaths = [self.table indexPathsForSelectedRows];
                
                NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:[_list indexOfObject:file]];
                [_table reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];

                if(self.table.isEditing==YES)
                {
                    for(NSIndexPath * indexPaths in SelectedRowsindexPaths){
                        if(indexPaths.section == [_list indexOfObject:file]){
                            [_table selectRowAtIndexPath:indexPaths animated:NO scrollPosition:UITableViewScrollPositionNone];
                        }
                    }
                }
            }];
        }];
        [self.queue addOperation:operation];
    }else{
        NSBlockOperation * operation = [NSBlockOperation blockOperationWithBlock:
        ^{
            NSString * cachepath = [NSString stringWithFormat: @"%@/Documents/%@/%@", NSHomeDirectory(), @"preview",fileinfo.name];
            if([_filemanager fileExistsAtPath:cachepath]==NO){
                
                NSString * filename = [[fileinfo.name stringByDeletingPathExtension] stringByAppendingString:@".jpg"];
                NSString * path = [self SearchHTTPPathForName:fileinfo.thumb :filename];
                NSData * resuiltData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
                if(resuiltData!=nil){
                    UIImage * img = [UIImage imageWithData:resuiltData];
                    if(img!=nil){
                        file.preview = img;
                        [UIImageJPEGRepresentation(img, 1.0) writeToFile:cachepath atomically:YES];
                    }else{
                        NSLog(@"获取jpg预览错误\n");
                        file.preview = [UIImage imageNamed:@"img_nopic_01"];
                    }
                }
            }else{
                file.preview = [UIImage imageWithContentsOfFile:cachepath];
                if(file.preview==nil){
                    NSLog(@"从本地获取缓存缩略图错误\n");
                    while(1);
                }
            }
                                            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSArray * SelectedRowsindexPaths = [self.table indexPathsForSelectedRows];
                
                NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:[_list indexOfObject:file]];
                [_table reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                
                if(self.table.isEditing==YES)
                {
                    for(NSIndexPath * indexPaths in SelectedRowsindexPaths){
                        if(indexPaths.section == [_list indexOfObject:file]){
                            [_table selectRowAtIndexPath:indexPaths animated:NO scrollPosition:UITableViewScrollPositionNone];
                        }
                    }
                }
            }];
        }];
        [self.queue addOperation:operation];
    }
}

- (NSString *)SearchHomeXMLPath:(DownInfo *)fileinfo
{
    if(fileinfo.fileinfo.name==nil)return nil;
    return [NSString stringWithFormat: @"%@/Documents/%@/%@", NSHomeDirectory(),@"cache",fileinfo.fileinfo.name];
}

- (NSString *)SearchHTTPPathForName:(NSString *)dir:(NSString *)filename
{
    NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@",[WiFiConnection sharedSingleton].NTConnectionHost,dir,filename];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return path;
}

- (NSString *)SearchCacheFilePath:(DownInfo *)fileinfo
{
    if(fileinfo.DownloadFile==nil)return nil;
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:fileinfo.DownloadFile];
    return path;
}

- (NSString *)SearchHomeCacheFilePath:(DownInfo *)fileinfo
{
    if(fileinfo.DownloadFile==nil)return nil;
    
    return [NSString stringWithFormat: @"%@/Documents/%@/%@", NSHomeDirectory(),@"cache",fileinfo.DownloadFile];
}

- (NSString *)SearchHomeFilePathForDownInfo:(DownInfo *)fileinfo
{
    if(fileinfo.fileinfo.name==nil)return nil;
    
    if([[fileinfo.fileinfo.name pathExtension] isEqualToString:@"jpg"]==YES||[[fileinfo.fileinfo.name pathExtension] isEqualToString:@"JPG"]==YES){
        return [[NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"picture"] stringByAppendingPathComponent:fileinfo.fileinfo.name];
    }else{
        return [[NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"video"] stringByAppendingPathComponent:fileinfo.fileinfo.name];
    }
}

- (void)DownloadFirst:(DownInfo *)fileinfo
{
    NSString * httppath;
    NSURL * url;
    
    if(fileinfo==nil)return;
    
    if(fileinfo.session==nil){
        httppath = [self SearchHTTPPathForName:fileinfo.fileinfo.path :fileinfo.fileinfo.name];
        httppath = [httppath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        if(httppath!=nil)
        {
            url = [NSURL URLWithString:httppath];
            
            fileinfo.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
            
            if(fileinfo.session==nil){
                NSLog(@"session create fault\n");
                while(1);
            }
        }else
        {
            NSLog(@"session create fault\n");
            while(1);
        }
    }
    
    fileinfo.task = [fileinfo.session downloadTaskWithURL:url];
    if(fileinfo.task==nil){
        NSLog(@"file task create fault\n");
        while(1);
    }
}

- (void)DownloadErrorRecovery:(DownInfo *)fileinfo :(BOOL)redownload
{
    NSString * xml = [self SearchHomeXMLPath:fileinfo];
    NSString * Cachefile = [self SearchCacheFilePath:fileinfo];
    NSString * HomeCachefile = [self SearchHomeCacheFilePath:fileinfo];
    
    if(xml!=nil)
        [_filemanager removeItemAtPath:xml error:nil];
    if(Cachefile!=nil)
        [_filemanager removeItemAtPath:Cachefile error:nil];
    if(HomeCachefile!=nil)
        [_filemanager removeItemAtPath:HomeCachefile error:nil];
    
    if(redownload==YES){
        [self DownloadFirst:fileinfo];
        [fileinfo.task resume];
    }
}

- (void)removeDownloadFile:(DownInfo *)fileinfo
{
    NSString * xml = [self SearchHomeXMLPath:fileinfo];
    NSString * Cachefile = [self SearchCacheFilePath:fileinfo];
    NSString * HomeCachefile = [self SearchHomeCacheFilePath:fileinfo];
    NSString * file = [self SearchHomeFilePathForDownInfo:fileinfo];
    
    if(xml!=nil)
        [_filemanager removeItemAtPath:xml error:nil];
    if(Cachefile!=nil)
        [_filemanager removeItemAtPath:Cachefile error:nil];
    if(HomeCachefile!=nil)
        [_filemanager removeItemAtPath:HomeCachefile error:nil];
    if(file!=nil)
        [_filemanager removeItemAtPath:file error:nil];
    
    fileinfo.state = 0;
    [_downloadlist removeObject:fileinfo];
}

- (void)btdeleteclick:(UIButton *)btn
{
    NSString * Title;
    NSString * action1;
    NSString * action2;
    NSString * leftText;
    NSString * rightText;
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
        leftText = @"返回";
        rightText = @"选择";
    }else {
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
        leftText = @"Back";
        rightText = @"Edit";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        NSArray * indexPaths = [self.table indexPathsForSelectedRows];
                          
        if(indexPaths==nil){
            return;
        }
        
        if(self.list!=nil){
            for(DownInfo * k in self.list){
                if([self checkDownloadFileExist:k]==TRUE)
                    k.state = 2;
                else {
                    k.state = 0;
                    [self countProgress:k];
                }
            }
            [_table reloadData];
        }
        
        NSMutableArray * deletelist = [[NSMutableArray alloc] init];
        for(NSIndexPath * path in indexPaths){
            if(path.section >= self.list.count){
                NSLog(@"self.list error\n");
                while(1);
            }
            DownInfo * fileinfo = self.list[path.section];
            if(fileinfo==nil){
                NSLog(@"delete fileinfo error\n");
                while(1);
            }
            
            [deletelist addObject:fileinfo.fileinfo];
        }

        _deletecount = [deletelist count];
        NSString * text = [NSString stringWithFormat:@"%zi/%zi",0,[deletelist count]];
        _deletetext.text = text;
        _deletetext.hidden = NO;

        _isDelete = YES;
        _leftbarbutton.enabled = NO;
        _BarButton.enabled = NO;
        _BTDelete.enabled = NO;
        _BTDownload.enabled = NO;
        
        _deleteprogress.progress = (_deletecount- [_downloadlist count])/_deletecount;
        _deleteprogress.hidden = NO;
 
        _table.scrollsToTop = YES;
        _table.scrollEnabled = NO;

        [self.queue cancelAllOperations];
        [self.queue waitUntilAllOperationsAreFinished];
        
        [[WiFiConnection sharedSingleton] deleteFile:deletelist];

        [_table setEditing:NO animated:YES];
        _BarButton.title = rightText;
        _leftbarbuttonMode = 0;
        _leftbarbutton.title = leftText;
        self.tabBarController.tabBar.hidden = NO;
        _ButtonControl.hidden = YES;
        [_loading startAnimating];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
    }]];
}

- (void)btdownloadclick:(UIButton *)btn
{
    NSArray * indexPaths = [self.table indexPathsForSelectedRows];
    
    for(NSIndexPath * path in indexPaths){
        DownInfo * fileinfo = self.list[path.section];
        
        if(fileinfo.state==0){
            fileinfo.state = 1;
            [_downloadlist removeObject:fileinfo];
            [_downloadlist addObject:fileinfo];
//            [self resumeDownload:fileinfo];
        }
    }
    
    NSLog(@"%zi\n",_downloadlist.count);
    if(_downloadlist.count>0){
        DownInfo * gpsfileinfo= _downloadlist[0];
        [[WiFiConnection sharedSingleton] GetGpsContents:gpsfileinfo.fileinfo.name];
    }
        
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _leftbarbutton.title = @"返回";
        _BarButton.title  = @"选择";
    }else {
        _leftbarbutton.title = @"Back";
        _BarButton.title  = @"Edit";
    }
    
    [_table setEditing:NO animated:YES];
    _leftbarbuttonMode = 0;
    self.tabBarController.tabBar.hidden = NO;
    _ButtonControl.hidden = YES;
    
    [self.table reloadData];
}

- (void)countProgress:(DownInfo *)fileinfo
{
    if(fileinfo==nil)return;
    
    NSString * path = [self SearchHomeXMLPath:fileinfo] ;
    if([_filemanager fileExistsAtPath:path]==NO){
        fileinfo.progress = 0;
        return;
    }
    
    NSData * resumeData = [NSData dataWithContentsOfFile:path];  // 获取xml数据
    if(resumeData!=nil)
    {
        NSString * DownloadFileName= [self getResumCacheFileName:resumeData];
        NSString * DownloadFilePath= [NSString stringWithFormat: @"%@/Documents/%@/%@", NSHomeDirectory(),@"cache",DownloadFileName];
        if([_filemanager fileExistsAtPath:DownloadFilePath]==YES){
            fileinfo.progress= (CGFloat)[[_filemanager attributesOfItemAtPath:DownloadFilePath error:nil] fileSize]/1.0 / fileinfo.fileinfo.size_int;
        }else{
            fileinfo.progress = 0;
        }
    }
    else
    {
        fileinfo.progress = 0;
    }
}

- (void)resumeDownload:(DownInfo *)fileinfo
{
    if(fileinfo==nil)return;
    
    NSString * path = [self SearchHomeXMLPath:fileinfo] ;
    
    if([_filemanager fileExistsAtPath:path]==NO){
        [self DownloadErrorRecovery:fileinfo :YES];
        return;
    }
    
    if(fileinfo.session==nil)
    {
        [self DownloadFirst:fileinfo];
    }
    
    NSData * resumeData = [NSData dataWithContentsOfFile:path];
    if(resumeData!=nil)
    {
        fileinfo.DownloadFile= [self getResumCacheFileName:resumeData];
        if([self resumeCacheFile:fileinfo]==TRUE)
        {
            fileinfo.task = [fileinfo.session downloadTaskWithResumeData:resumeData];
            [fileinfo.task resume];
        }
        else
        {
            NSLog(@"临时下载文件不存在，重新下载\n");
            [self DownloadErrorRecovery:fileinfo :YES];
            return;
        }
    }
    else
    {
        [self DownloadErrorRecovery:fileinfo :YES];
    }
}

- (NSString *)getResumCacheFileName:(NSData *)resumeData
{
    if(resumeData==nil)return nil;
    
    NSString * partialData = [[NSString alloc] initWithData:resumeData encoding:NSUTF8StringEncoding];
    NSArray * BFDownloadFileArry;
    NSArray * AFDownloadFileArry;
    NSString * AFDownloadFilePath;
    int i;
    
    BFDownloadFileArry= [partialData componentsSeparatedByString:@"<string>"];
    
    if([BFDownloadFileArry count]==0){
        return nil;
    }
    
    for(i= 1;i< [BFDownloadFileArry count];i++)
    {
        AFDownloadFileArry= [BFDownloadFileArry[i] componentsSeparatedByString:@"</string>"];
        if( AFDownloadFileArry[0]== nil ){
            break;
        }
        if ([AFDownloadFileArry[0] rangeOfString:@"CFNetworkDownload"].length>0){
            AFDownloadFilePath = AFDownloadFileArry[0];
        }
    }
    
    return AFDownloadFilePath;
}

- (void)stopDownLoad:(DownInfo *)fileinfo
{
    NSLog(@"stop %@ download\n",fileinfo.fileinfo.name);
    
    if(fileinfo.task==nil)return;
    
    [fileinfo.task cancelByProducingResumeData:^(NSData * _Nullable resumeData)
     {
         if(resumeData!=nil)
         {
             fileinfo.DownloadFile= [self getResumCacheFileName:resumeData];
             if(fileinfo.DownloadFile==nil){
                 NSLog(@"getResumDownloadfile fault\n");
                 [self DownloadErrorRecovery:fileinfo :YES];
                 return;
             }
             
             if([self saveCacheFile:fileinfo]==FALSE)
             {
                 [self DownloadErrorRecovery:fileinfo :YES];
                 return;
             }
             
             NSString * path = [self SearchHomeXMLPath:fileinfo] ;
             if(path==nil){
                 NSLog(@"stopDownLoad get xml path fault\n");
                 [self DownloadErrorRecovery:fileinfo :YES];
                 return;
             }
             [resumeData writeToFile:path atomically:YES];
         }
     }];
}

- (BOOL)removeTempFile:(DownInfo *)fileinfo
{
    if(fileinfo.DownloadFile==nil){
        return FALSE;
    }
    
    NSString * xml = [self SearchHomeXMLPath:fileinfo];
    NSString * Cachefile = [self SearchCacheFilePath:fileinfo];
    NSString * HomeCachefile = [self SearchHomeCacheFilePath:fileinfo];
    if(xml!=nil)
        [_filemanager removeItemAtPath:xml error:nil];
    if(Cachefile!=nil)
        [_filemanager removeItemAtPath:Cachefile error:nil];
    if(HomeCachefile!=nil)
        [_filemanager removeItemAtPath:HomeCachefile error:nil];
    
    return TRUE;
}

- (BOOL)saveCacheFile:(DownInfo *)fileinfo
{
    if(fileinfo.DownloadFile==nil){
        NSLog(@"SaveDownloadFile:without tmp name\n");
        return FALSE;
    }
    
    NSString * CacheFilePath = [self SearchCacheFilePath:fileinfo];
    NSString * HomeCacheFilePath = [self SearchHomeCacheFilePath:fileinfo];
    
    if([self.filemanager fileExistsAtPath:CacheFilePath]==YES)
    {
        NSError * err= nil;
        [self.filemanager removeItemAtPath:HomeCacheFilePath error:nil];
        [self.filemanager copyItemAtPath:CacheFilePath toPath:HomeCacheFilePath error:&err];
        if(err!=nil){
            NSLog(@"copy download temp file fault : %@\n",err);
        }
        return TRUE;
    }
    else
    {
        NSLog(@"saveCacheFile get temp file fault\n");
        return FALSE;
    }
}

- (BOOL)resumeCacheFile:(DownInfo *)fileinfo
{
    NSString * HomeCacheFilePath = [self SearchHomeCacheFilePath:fileinfo];
    NSString * CacheFilePath = [self SearchCacheFilePath:fileinfo];
    
    if([self.filemanager fileExistsAtPath:HomeCacheFilePath]==YES){
        [self.filemanager copyItemAtPath:HomeCacheFilePath toPath:CacheFilePath error:nil];
        return TRUE;
    }
    else{
        NSLog(@"resumeCacheFile fault\n");
        return FALSE;
    }
}

- (BOOL)checkDownloadFileExist:(DownInfo *)fileinfo
{
    NSString * path;
    path = [self SearchHomeFilePathForDownInfo:fileinfo];
    
    return [self.filemanager fileExistsAtPath:path];
}

+ (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.list.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ID = @"downcell";
    DownLoadCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([DownLoadCell class]) owner:nil options:nil] lastObject];
    }
    
    if(_downloadlist.count==0){
        _BarButton.enabled=YES;
        _leftbarbutton.enabled=YES;
    }else{
        _BarButton.enabled=NO;
        _leftbarbutton.enabled=NO;
    }
    
    cell.DLButton.tag= indexPath.section;
    cell.process.tag = indexPath.section;
    cell.openbutton.tag = indexPath.section;
    
    if(indexPath.section >= self.list.count){
        NSLog(@"section错误!!!!!!!!!!\n");
        while(1);
    }
    
    __weak DownInfo * obj= self.list[indexPath.section];
    if(obj==nil){
        NSLog(@"list obj错误!!!!!\n");
        while(1);
    }
    if(obj.preview==nil){
        NSLog(@"obj.preview==nil\n");
        while(1);
    }

    cell.preview.image = obj.preview;
    cell.MVName.text = obj.fileinfo.name;
    cell.MVSize.text = obj.fileinfo.size;
    cell.MVDay.text  = obj.fileinfo.day;
    cell.MVTime.text = obj.fileinfo.time;
    
    if( obj.state==0 ){
        cell.openbutton.hidden = YES;
        cell.process.hidden = NO;
        cell.DLButton.hidden = NO;
        
        [cell.process setProgress:obj.progress];
        [cell.process setNeedsDisplay];
        [cell.process removeGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]];
        
        [cell.DLButton setTitle:nil forState:UIControlStateNormal];
        [cell.DLButton  setImage:[UIImage imageNamed:@"icon_play_01"] forState:UIControlStateNormal];
    }
    else if( obj.state==1){
        cell.openbutton.hidden = YES;
        cell.DLButton.hidden = YES;
        cell.process.hidden = NO;
        
        [cell.process setProgress:obj.progress];
        [cell.process setNeedsDisplay];
        [cell.process addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]];
    }
    else if( obj.state==2 ){
        cell.process.hidden = YES;
        cell.DLButton.hidden = YES;
        cell.openbutton.hidden = NO;
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            [cell.openbutton setTitle:@"打开" forState:UIControlStateNormal];
        }else {
            [cell.openbutton setTitle:@"open" forState:UIControlStateNormal];
        }
    }
    return cell;
}

- (void)tap:(UITapGestureRecognizer *)recognizer
{
    DownInfo * fileinfo = self.list[recognizer.view.tag];
    DownInfo * curren = _downloadlist[0];
    
    if(_thread!=nil){
        [_thread cancel];
    }
    
    [_downloadlist removeObject:fileinfo];
    [self stopDownLoad:fileinfo];
    fileinfo.state = 0;
    
    if(fileinfo==curren){
        NSLog(@"%zi\n",_downloadlist.count);
        if(_downloadlist.count> 0){
            DownInfo * fileinfo = _downloadlist[0];
            [[WiFiConnection sharedSingleton] GetGpsContents:fileinfo.fileinfo.name];
        }
    }
    
    [self.table reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.table.isEditing==NO)
    {
        NSInteger n= 0, index = 0;
        DownInfo * touchfile = self.list[indexPath.section];
        
        if( [[touchfile.fileinfo.name pathExtension] isEqualToString:@"jpg"] ||
           [[touchfile.fileinfo.name pathExtension] isEqualToString:@"JPG"])
        {
            if(_downloadlist.count!=0)
                return;
            self.navigationController.navigationBar.hidden = YES;
            self.tabBarController.tabBar.hidden = YES;
            
            NSMutableArray * filelist = [[NSMutableArray alloc] init];
            NSString * path = [self SearchHTTPPathForName:touchfile.fileinfo.path :touchfile.fileinfo.name];
            [filelist addObject:path];
            
            FLImageShowVC *fvc = [[FLImageShowVC alloc] init];
            fvc.netImageUrlsArray = [filelist copy];
            fvc.currentIndex = 0;
            [self.navigationController pushViewController:fvc animated:YES];
        }
        else if( [[touchfile.fileinfo.name pathExtension] isEqualToString:@"mp4"] ||
                [[touchfile.fileinfo.name pathExtension] isEqualToString:@"MP4"])
        {
#if 1
            if(_downloadlist.count!=0)
                return;
            VLCPlayer * VLCPlayVc = [[VLCPlayer alloc] init];
            VLCPlayVc.filelist = [[NSMutableArray alloc] init];
            
            for(DownInfo * file in self.list){
                if( [[file.fileinfo.name pathExtension] isEqualToString:@"mp4"] ||
                   [[file.fileinfo.name pathExtension] isEqualToString:@"MP4"])
                {
                    //NSString * path = [self SearchHTTPPathForName:touchfile.fileinfo.path :file.fileinfo.name];
                    [VLCPlayVc.filelist addObject:file.fileinfo];
                    if( [touchfile.fileinfo.name isEqualToString:file.fileinfo.name]==YES ){
                        index = n;
                    }
                    n++;
                }
            }
            
            [VLCPlayVc playWithIndex:index];
            
            [self.table reloadData];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GetGpsList" object:nil];
            [self.navigationController pushViewController:VLCPlayVc animated:YES];
#endif
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        return @"删除";
    }else {
        return @"delete";
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_isDelete==YES)
        return;
    
    NSString * Title;
    NSString * action1;
    NSString * action2;
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
    }else {
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
    }
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
        {
            DownInfo * fileinfo = self.list[indexPath.section];
            [self removeDownloadFile:fileinfo];
                              
            NSMutableArray * array = [[NSMutableArray alloc] init];
                              
            [array addObject:fileinfo.fileinfo];
            [[WiFiConnection sharedSingleton] deleteFile:array];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    }
    
}
- (IBAction)SelectFileClick:(id)sender
{
    [_table setEditing:!self.table.isEditing animated:YES];
    
    if(self.table.isEditing==NO){
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            _leftbarbutton.title = @"返回";
            _BarButton.title = @"选择";
        }else {
            _leftbarbutton.title = @"Back";
            _BarButton.title = @"Edit";
        }
        
        _leftbarbuttonMode = 0;
        self.tabBarController.tabBar.hidden = NO;
        _ButtonControl.hidden = YES;
    }
    else{
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            _leftbarbutton.title = @"全选";
            _BarButton.title = @"取消";
        }else {
            _leftbarbutton.title = @"All";
            _BarButton.title = @"Cancel";
        }
        _leftbarbuttonMode = 1;
        self.tabBarController.tabBar.hidden = YES;
        _ButtonControl.hidden = NO;
    }
}
- (IBAction)openclick:(UIButton *)sender
{
    DownInfo * fileinfo = self.list[sender.tag];
    
    for(DownInfo * path in _downloadlist){
        if(path.state==1){
            [self stopDownLoad:path];
            path.state = 0;
        }
    }
    
    NSString * path= [self SearchHomeFilePathForDownInfo:fileinfo];
    NSLog(@"%@ %@\n",path, [path pathExtension]);
    
    if(([[path pathExtension] isEqualToString:@"jpg"]==YES)||([[path pathExtension] isEqualToString:@"JPG"]==YES)){
        if(_downloadlist.count!=0)
            return;
        
        self.navigationController.navigationBar.hidden = YES;
        self.tabBarController.tabBar.hidden = YES;

        NSMutableArray * filelist = [[NSMutableArray alloc] init];
        [filelist addObject:path];
        FLImageShowVC *fvc = [[FLImageShowVC alloc] init];
        fvc.localImageNamesArray = [filelist copy];
        fvc.currentIndex = 0;
        
        [self.navigationController pushViewController:fvc animated:YES];
    }else{
        if(_downloadlist.count!=0)
            return;
        
        LocalAVPlayerViewController * avplayVc = [[LocalAVPlayerViewController alloc] init];
        
        avplayVc.filelist = [[NSMutableArray alloc] init];
        [avplayVc.filelist addObject:path];
        [avplayVc playWithIndex:0];
        
        [self.navigationController pushViewController:avplayVc animated:YES];
    }
}
- (IBAction)downloadclick:(UIButton *)sender
{
    DownInfo * fileinfo = self.list[sender.tag];
    
    [_downloadlist removeObject:fileinfo];
    [_downloadlist addObject:fileinfo];
    
    if(_downloadlist.count==1){
        [[WiFiConnection sharedSingleton] GetGpsContents:fileinfo.fileinfo.name];
    }
    [self resumeDownload:fileinfo];
    
    fileinfo.state = 1;
    
    [self.table reloadData];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    int i= 0;
    for(DownInfo * fileinfo in self.list)
    {
        if(fileinfo.session==session){
            float progress = 1.0* totalBytesWritten/ totalBytesExpectedToWrite;
            if(progress>=0.99)
                progress= 0.99;
            
            if(progress- fileinfo.progress > 0.01){
                fileinfo.progress = progress;
                [_table reloadData];
#if 0
                BOOL isSel = NO;
                NSArray * indexPaths = [self.table indexPathsForSelectedRows];
                for(NSIndexPath * path in indexPaths){
                    if(path.section==i)isSel = YES;
                }
                
                NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:i];
                [_table reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                if(isSel==YES){
                    NSIndexPath *indexPath=  [NSIndexPath indexPathForRow:0 inSection:i];
                    [_table selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
#endif
            }
            return ;
        }
        i++;
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(error==nil)return;
    if([error code]==NSURLErrorCancelled)return;
    NSLog(@"error:%@\n",error);
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    for(DownInfo * fileinfo in self.list)
    {
        if(fileinfo.session==session){
            NSString * file = [self SearchHomeFilePathForDownInfo:fileinfo];
            if(file==nil){
                NSLog(@"finish get path error\n");
                return;
            }
            
            NSString * path = [NSString stringWithFormat: @"file://%@",file];
            
            NSError * err;
            [self.filemanager moveItemAtURL:location toURL:[NSURL URLWithString:path] error:&err];
            if(err!=nil){
                NSLog(@"finish move error:%@\n",err);
                return;
            }
            [self removeTempFile:fileinfo];
            
            fileinfo.state= 2;
            [_downloadlist removeObject:fileinfo];
            
            NSLog(@"count = %zi\n",_downloadlist.count);
            if(_downloadlist.count>0){
                DownInfo * gpsfileinfo= _downloadlist[0];
                [[WiFiConnection sharedSingleton] GetGpsContents:gpsfileinfo.fileinfo.name];
            }else{
                
            }
            
            [self.table reloadData];
            return;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [WiFiConnection sharedSingleton].Vc = self;
    if(self.list!=nil){
        for(DownInfo * k in self.list){
            if([self checkDownloadFileExist:k]==TRUE)
                k.state = 2;
            else {
                k.state = 0;
                [self countProgress:k];
            }
        }
        [_table reloadData];
    }
}

- (void)onEnterBackground
{
    [self.queue cancelAllOperations];
    [self.queue waitUntilAllOperationsAreFinished];
    self.queue.suspended = NO;
    self.queue = nil;
    if(_thread!=nil){
        [_thread cancel];
    }
    
    for(DownInfo *fileinfo in _downloadlist)
    {
        [self stopDownLoad:fileinfo];
    }
    
    [WiFiConnection sharedSingleton].GetDirectoryContentsDelegate = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [WiFiConnection sharedSingleton].Vc = nil;

    [self.queue cancelAllOperations];
    [self.queue waitUntilAllOperationsAreFinished];
    self.queue.suspended = NO;
    self.queue = nil;
    
    for(DownInfo *fileinfo in _downloadlist)
    {
        [self stopDownLoad:fileinfo];
    }
    
    if(_thread!=nil){
        [_thread cancel];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [WiFiConnection sharedSingleton].GetDirectoryContentsDelegate = nil;
}

- (IBAction)leftbarbuttonclick:(id)sender
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    BOOL isChinese = [str isEqualToString:@"Chinese"];
    NSString * leftString;
    
    if(isChinese==YES){
        leftString = @"返回";
    }else {
        leftString = @"Back";
    }
    
    if([_leftbarbutton.title isEqualToString:leftString]==YES){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@" "];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if(_leftbarbuttonMode==0){
            _leftbarbuttonMode= 1;
            
            if(isChinese==YES){
                _leftbarbutton.title = @"全选";
            }else {
                _leftbarbutton.title = @"All";
            }
            
            for (int i = 0; i < self.list.count; i++) {
                NSIndexPath *indexPath=  [NSIndexPath indexPathForRow:0 inSection:i];
                [_table deselectRowAtIndexPath:indexPath animated:YES];
            }
        }else{
            _leftbarbuttonMode= 0;
            
            if(isChinese==YES){
                _leftbarbutton.title = @"全不选";
            }else {
                _leftbarbutton.title = @"None";
            }
            
            for (int i = 0; i < self.list.count; i++) {
                NSIndexPath *indexPath=  [NSIndexPath indexPathForRow:0 inSection:i];
                [_table selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            }
        }
    }
}

- (NSMutableArray *)list
{
    if (_list == nil) {
        NSMutableArray *listArray = [NSMutableArray array];
        _list = listArray;
    }
    return _list;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
