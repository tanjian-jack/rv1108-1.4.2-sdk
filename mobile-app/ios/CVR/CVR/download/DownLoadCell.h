//
//  DownLoadCell.h
//  UI
//
//  Created by leiqibin on 16-4-17.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDProgressView.h"
#import "SDDemoItemView.h"

@interface DownLoadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *openbutton;
@property (weak, nonatomic) IBOutlet UIImageView *preview;
@property (weak, nonatomic) IBOutlet UILabel *MVName;
@property (weak, nonatomic) IBOutlet UILabel *MVTime;
@property (weak, nonatomic) IBOutlet UILabel *MVSize;
@property (weak, nonatomic) IBOutlet UILabel *MVDay;
@property (weak, nonatomic) IBOutlet SDPieLoopProgressView *process;
@property (weak, nonatomic) IBOutlet UIButton *DLButton;
@end
