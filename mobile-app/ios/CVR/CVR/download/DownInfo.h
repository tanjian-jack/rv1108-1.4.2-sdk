//
//  DownInfo.h
//  UI
//
//  Created by leiqibin on 16-4-17.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDProgressView.h"
#import "SDDemoItemView.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface DownInfo : NSObject

@property (nonatomic, strong) NTFileObj *fileinfo;
//@property (nonatomic, strong) NSOperation *operation;
@property (nonatomic, strong) NSURLSessionDownloadTask * task;
@property (nonatomic, strong) NSURLSession * session;
@property (nonatomic, strong) NSString *DownloadFile;
@property (nonatomic, strong) NSFileHandle * filehandle;
@property (nonatomic, strong) UIImage * preview;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign) NSInteger state;
@property (nonatomic, assign) BOOL cencal;
@end
