//
//  VLCPlayer.m
//  CVR
//
//  Created by 雷起斌 on 6/16/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "VLCPlayer.h"
#import <MobileVLCKit/MobileVLCKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import <MAMapKit/MAMapKit.h>
#import "PageFlowObj.h"
#import "UIImageView+WebCache.h"

@interface VLCPlayer ()<UIGestureRecognizerDelegate , MAMapViewDelegate , DirectoryContentsDelegate>
{
    float progressSlider;
    BOOL isPlay;
    CGPoint _startPoint;
}
@property (strong, nonatomic) VLCMediaPlayer *player;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) UIView *processView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIView *controlView;
@property (strong, nonatomic) UIView *playerView;
@property (strong, nonatomic) UIView *map;

@property (strong, nonatomic) UIButton * backButton;

@property (strong, nonatomic) UIButton * delete;
@property (strong, nonatomic) UIButton * camera;
@property (strong, nonatomic) UIButton * last;
@property (strong, nonatomic) UIButton * next;
@property (strong, nonatomic) UIButton * play;
@property (strong, nonatomic) UIButton * stop;
@property (strong, nonatomic) UILabel * titlelabel;

@property (strong, nonatomic) UISlider *topProgressSlider;
@property (strong, nonatomic) UILabel *topPastTimeLabel;
@property (strong, nonatomic) UILabel *totalTimeLabel;
@property (strong, nonatomic) UITapGestureRecognizer  *tapGesture;

@property (nonatomic, assign) BOOL isReview;
@property (nonatomic, assign) BOOL isHiddenAllView;
@property (nonatomic, assign) BOOL isContinuous;
@property (nonatomic, assign) UIInterfaceOrientation orientation;

@property (nonatomic, assign) int totalMovieDuration;

@property (nonatomic, strong) NSFileManager * filemanager;

@property (nonatomic, assign) UIInterfaceOrientation RotatorTap;
@property (strong, nonatomic) UILabel *hud;
@property (assign, nonatomic) NSInteger language;

@property (nonatomic, strong) NSMutableArray *videoArray;

//@property (nonatomic, strong) UIScrollView *bottomScrollView;
//@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (strong, nonatomic) UILabel *pageTimeLabel;
@property (strong, nonatomic) UIImageView *preView;
//@property (strong, nonatomic) NSThread * thread;
@property (assign, nonatomic) bool isthumbnail;
@property (nonatomic, assign) NSTimeInterval VideoTotalLength;
@property (strong, nonatomic) UIButton * playButton;
@property (strong, nonatomic) PageFlowObj * currentPlayVideo;
@end

@implementation VLCPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    
    _filemanager = [NSFileManager defaultManager];

    NSNumber * value;
    UIView * playerView = [[UIView alloc] init];
    UIView * topView = [[UIView alloc] init];
    
    if(_isLiveStream==YES){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTP_TS_TRANS_START"];
        _RotatorTap = UIInterfaceOrientationPortrait;
        value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        topView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        playerView.frame = CGRectMake(0, topView.frame.size.height, self.view.frame.size.width, self.view.frame.size.width*384/640);
        _map = [[MAMapView alloc] initWithFrame:CGRectMake(0, playerView.frame.origin.y+playerView.frame.size.height+30, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height- playerView.frame.size.height-30)];
        [self.view addSubview:_map];
    }else{
        _RotatorTap = UIInterfaceOrientationLandscapeRight;
        value = [NSNumber numberWithInt:UIDeviceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        _playerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        topView.frame = CGRectMake(0, 0, self.view.frame.size.height, 40);
    }
    [self.view addSubview:playerView];
    _playerView = playerView;
    _playerView.backgroundColor = [UIColor blackColor];
    [self.player setDrawable:_playerView];

    UIColor * myColorRGB = [UIColor blackColor];
    topView.backgroundColor = myColorRGB;
    _backButton = [[UIButton alloc] init];
    _backButton.frame = CGRectMake(5, 5, 30, 30);
    [_backButton setBackgroundImage:[UIImage imageNamed:@"play_black_02"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backclick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_backButton];
    [self.view addSubview:topView];
    _topView= topView;
    _titlelabel = [[UILabel alloc] initWithFrame:_topView.frame];
    _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];
    _titlelabel.textAlignment = UITextAlignmentCenter;
    _titlelabel.textColor = UIColor.whiteColor;
    _topView.alpha = 0.6f;
    [_topView addSubview:_titlelabel];
    
    if(_isLiveStream==NO)
    {
        UIView * buttonView = [[UIView alloc] init];
        buttonView.backgroundColor = myColorRGB;
        
        [self.view addSubview:buttonView];
        _processView = buttonView;

        UILabel * labelright = [[UILabel alloc] init];
        labelright.text = @"00:00";
        labelright.numberOfLines = 0;
        labelright.textAlignment = UITextAlignmentRight;
        labelright.textColor = UIColor.whiteColor;
        labelright.adjustsFontSizeToFitWidth = YES;
        _totalTimeLabel = labelright;
        [buttonView addSubview:labelright];

        UILabel * labelleft = [[UILabel alloc] init];
        labelleft.text = @"00:00";
        labelleft.numberOfLines = 0;
        labelleft.textColor = UIColor.whiteColor;
        labelleft.adjustsFontSizeToFitWidth = YES;
        _topPastTimeLabel = labelleft;
        [buttonView addSubview:labelleft];
        
        UISlider * slider = [[UISlider alloc] init];
        [slider setMinimumValue:0];
        [slider setMaximumValue:1];
        
        [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateNormal];
        [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateHighlighted];
        
        [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        slider.value = 0;
        
        CGFloat top = 10; // 顶端盖高度
        CGFloat bottom = 10 ; // 底端盖高度
        CGFloat left = 10; // 左端盖宽度
        CGFloat right = 10; // 右端盖宽度
        UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
        
        [slider setMinimumTrackImage:[[UIImage imageNamed:@"progress-bar_02.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
        
        [slider setMaximumTrackImage:[[UIImage imageNamed:@"progress-bar_01.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
        
        _topProgressSlider = slider;
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
        _tapGesture.delegate = self;
        [slider addGestureRecognizer:_tapGesture];
        
        [buttonView addSubview:slider];
        
        UIView * controlView = [[UIView alloc] init];
        
        controlView.backgroundColor = myColorRGB;
        [self.view addSubview:controlView];
        
        _camera = [[UIButton alloc] init];
        [_camera setBackgroundImage:[UIImage imageNamed:@"play_camera"] forState:UIControlStateNormal];
        [_camera addTarget:self action:@selector(CameraClick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_camera];
        
        _last = [[UIButton alloc] init];
        [_last setBackgroundImage:[UIImage imageNamed:@"play_previous"] forState:UIControlStateNormal];
        [_last addTarget:self action:@selector(ktclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_last];
        
        _stop = [[UIButton alloc] init];
        [_stop setBackgroundImage:[UIImage imageNamed:@"play_stop"] forState:UIControlStateNormal];
        [_stop addTarget:self action:@selector(psclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_stop];
        
        _play = [[UIButton alloc] init];
        _play.hidden = YES;
        [_play setBackgroundImage:[UIImage imageNamed:@"play_play"] forState:UIControlStateNormal];
        [_play addTarget:self action:@selector(psclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_play];
        
        _next = [[UIButton alloc] init];
        [_next setBackgroundImage:[UIImage imageNamed:@"play_next"] forState:UIControlStateNormal];
        [_next addTarget:self action:@selector(kjclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_next];
        
        _delete = [[UIButton alloc] init];
        [_delete setBackgroundImage:[UIImage imageNamed:@"play_delete"] forState:UIControlStateNormal];
        [_delete addTarget:self action:@selector(delclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_delete];
        _controlView= controlView;
        _processView.alpha = 0.6;
        _controlView.alpha = 0.8f;
    }else{
        UIView * buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, _playerView.frame.origin.y+ _playerView.frame.size.height, [UIScreen mainScreen].bounds.size.width, 30)];
        buttonView.backgroundColor = [UIColor redColor];
        
        [self.view addSubview:buttonView];
        _processView = buttonView;
        
        UISlider * slider = [[UISlider alloc] init];
        [slider setMinimumValue:0];
        [slider setMaximumValue:1];
        
        [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateNormal];
        [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateHighlighted];
        
        [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [slider addTarget:self action:@selector(sliderValueChangedEnd:) forControlEvents:UIControlEventTouchUpInside];
        
        slider.value = 0;
        
        CGFloat top = 10; // 顶端盖高度
        CGFloat bottom = 10 ; // 底端盖高度
        CGFloat left = 10; // 左端盖宽度
        CGFloat right = 10; // 右端盖宽度
        UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
        
        [slider setMinimumTrackImage:[[UIImage imageNamed:@"progress-bar_02.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[[UIImage imageNamed:@"progress-bar_01.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
        
        _topProgressSlider = slider;
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
        _tapGesture.delegate = self;
        [slider addGestureRecognizer:_tapGesture];
        _topProgressSlider.frame = CGRectMake(0, 0, _processView.frame.size.width, _processView.frame.size.height);

        [buttonView addSubview:slider];
        
        _videoArray = [[NSMutableArray alloc] init];
        _isReview = NO;
        _isthumbnail = NO;
        _VideoTotalLength = 0;
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(doFragmentThumbnail:) name:@"GET_Fragment_Thumbnail" object:nil];
        [_videoArray removeAllObjects];
        [[WiFiConnection sharedSingleton] GetThumbContents];
        
        _preView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.playerView.frame.size.width, self.playerView.frame.size.height) ];
        _preView.hidden = YES;
        
        _playButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        _playButton.center = _preView.center;
        [_playButton setBackgroundImage:[UIImage imageNamed:@"icon_play_01"] forState:UIControlStateNormal];
        [_playButton addTarget:self action:@selector(playClick) forControlEvents:UIControlEventTouchUpInside];
        _playButton.hidden = YES;
        [_playerView addSubview:_preView];
        [_playerView addSubview:_playButton];
        
        _pageTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.playerView.frame.size.height- 30- 5, self.playerView.frame.size.width, 30)];
        _pageTimeLabel.textAlignment = UITextAlignmentCenter;
        _pageTimeLabel.textColor = [UIColor yellowColor];
        _pageTimeLabel.font = [UIFont systemFontOfSize:18];
        _pageTimeLabel.numberOfLines = 0;
        _pageTimeLabel.hidden = YES;
        [_playerView addSubview:_pageTimeLabel];
        //_thread = [[NSThread alloc] initWithTarget:self selector:@selector(dothumbnail) object:nil];
    }
    self.isContinuous = NO;
    self.isHiddenAllView = YES;

    UILabel *labelOne = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelOne.backgroundColor = [UIColor clearColor];
    labelOne.textColor = [UIColor whiteColor];
    labelOne.font = [UIFont systemFontOfSize:28];
    labelOne.numberOfLines = 0;
    [labelOne sizeToFit];
    labelOne.center = CGPointMake(self.view.center.y, self.view.center.x);
    
    _hud= labelOne;
    _hud.hidden = YES;
    [self.view addSubview:_hud];
  
    [self doRotatorFrame];
    [self addProgressObserver];
    [self addGestureRecognizer];
    [self addNotificationCenters];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EnterBackground) name:@"EnterBackground" object:nil];

    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
        labelOne.text = @"截图成功";
    }else{
        _language = 0;
        labelOne.text = @"Screenshot success";
    }

    isPlay = YES;
    if(_player!=nil){
        [self.player play];
    }
}

- (void)playClick
{
    [_player stop];
    _player = nil;
    
    NSTimeInterval value = _topProgressSlider.value * _VideoTotalLength;
    for(PageFlowObj * n in _videoArray){
        if( value > n.sTimeInterval && value < n.pTimeInterval ){
            NSString * path = [NSString stringWithFormat: @"http://%@/DCIM/%@",[WiFiConnection sharedSingleton].NTConnectionHost,n.name];
            NSURL * url = [NSURL URLWithString:path];
            
            VLCMediaPlayer *player = [[VLCMediaPlayer alloc] initWithOptions:nil];
            self.player = player;
            self.player.media = [VLCMedia mediaWithURL:url];
            _url = url;
            
            if(_player!=nil){
                _isReview = NO;
                _playButton.hidden = YES;
                _preView.hidden = YES;
                [self.player setDrawable:_playerView];
                
                _currentPlayVideo = n;
                _isContinuous = YES;
                [_player play];
                
                [_player setTime:[[VLCTime alloc] initWithInt:(int)((value-n.sTimeInterval)*1000/1)]];
                return ;
            }
        }
    }
}

- (void)doFragmentThumbnail:(NSNotification *) notification
{
    PageFlowObj * obj = [[PageFlowObj alloc] init];
    NSString * aStr = [notification object];
    
    NSLog(@"aStr=%@\n",aStr);
    NSArray * Head= [aStr componentsSeparatedByString:@"NAME:"];
    if(Head==nil){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }
    if([Head count]< 2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }
    
    NSArray * Name= [Head[1] componentsSeparatedByString:@"Length:"];
    if(Name==nil){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }
    if([Name count]< 2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }
    obj.name = Name[0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd_HHmmss"];
    if(obj.name.length<16){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }
    obj.startTime = [dateFormatter dateFromString:[obj.name substringToIndex:15]];
    
    NSArray * Length= [Name[1] componentsSeparatedByString:@"END"];
    if(Length==nil){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
        return;
    }

    obj.Length = [Length[0] intValue];
    obj.sTimeInterval = _VideoTotalLength;
    _VideoTotalLength += obj.Length;
    obj.pTimeInterval = _VideoTotalLength;
    
    [_videoArray addObject:obj];

    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_NEXT_THUMB"];
}

- (void)doRotatorFrame
{
    CGFloat interval;
    if(_isLiveStream==NO){
        _playerView.frame = [UIScreen mainScreen].bounds;
        _processView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, self.view.frame.size.height, 40);
        _topView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
        _controlView.frame = CGRectMake((self.view.frame.size.height- 400)/ 2, _processView.frame.origin.y- 60, 400, 40 );
        
        interval = (400-30*5)/ 6;
        _camera.frame = CGRectMake(interval, 5, 30, 30);
        _last.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
        _stop.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _play.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _next.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
        _delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
        
        CGRect bounds = _processView.bounds;
        bounds.size.width -= 5;
        _totalTimeLabel.frame= bounds;
        
        bounds = _processView.bounds;
        bounds.origin.x += 5;
        _topPastTimeLabel.frame= bounds;
        
        CGSize size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        
        _topProgressSlider.frame = CGRectMake(size1.width+ 5, 0, _processView.frame.size.width- size1.width- size2.width- 25, _topPastTimeLabel.frame.size.height);
    }
    
    if(_isLiveStream==YES)
    {
        switch(_RotatorTap)
        {
            case UIInterfaceOrientationLandscapeRight:
                _playerView.frame = [UIScreen mainScreen].bounds;
                _topView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
                if(_isReview==YES){
                    _preView.hidden = YES;
                    [_player play];
                    _isReview= NO;
                    [_topProgressSlider setValue:1 animated:YES];
                }
                _processView.hidden = YES;
                break;
                
            case UIInterfaceOrientationLandscapeLeft:
                _playerView.frame = [UIScreen mainScreen].bounds;
                _topView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
                if(_isReview==YES){
                    _preView.hidden = YES;
                    [_player play];
                    _isReview= NO;
                    [_topProgressSlider setValue:1 animated:YES];
                }
                _processView.hidden = YES;
                break;
                
            case UIInterfaceOrientationPortrait:
                [_player play];
                _topView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40);
                _playerView.frame = CGRectMake(0, _topView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width*384/640);
                _processView.hidden = NO;
                break;
                
            case UIInterfaceOrientationPortraitUpsideDown:
                break;
                
            default:
                break;
        }
    }
}

- (void)delclick
{
    NSString * Title;
    NSString * action1;
    NSString * action2;
    if(_language==1){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
    }else {
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        [_player pause];
        
        NSString * path = [[NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"video"] stringByAppendingPathComponent:[_filelist[_index] lastPathComponent]];
        
        NSError * error= nil;
        [_filemanager removeItemAtPath:path error:&error];
        if(error!=nil){
            NSLog(@"delete fault:%@\n",error);
        }

        NSMutableArray * delNames = [[NSMutableArray alloc] init];
        [delNames addObject:[_filelist[_index] lastPathComponent]];
        [[WiFiConnection sharedSingleton] deleteFile:delNames];

        [_filelist removeObjectAtIndex:_index];
        if(_index >= [_filelist count]){
            _index = 0;
        }
        _totalMovieDuration = 0;
        [self playWithIndex:_index];
        [self.player setDrawable:_playerView];
        [self setMoviePlay];
        _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
                          
    }]];
}

- (void)savePreview
{
    NSString * pic_path = [[NSString alloc] init];
    
    if( self.isLiveStream == NO ){
        return;
    }
    else{
        pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@.jpg", NSHomeDirectory(),@"preview",[WiFiConnection sharedSingleton].NTConnectionHost];
    }
    [self doCamera:pic_path];
}

-(void)doCamera:(NSString *)pic_path
{
    [_player saveVideoSnapshotAt:pic_path withWidth:0 andHeight:0];
}

-(void)CameraClick
{
    NSString * pic_path = [[NSString alloc] init];
    
    if( self.isLiveStream == NO ){
        NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *currentTime = [formatter stringFromDate:[NSDate date]];
        
        NSString * path= [_url absoluteString];
        
        pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@_%@.jpg", NSHomeDirectory(),@"picture",[[path lastPathComponent]stringByDeletingPathExtension],currentTime];
    }
    else{
        pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@.jpg", NSHomeDirectory(),@"preview",[WiFiConnection sharedSingleton].NTConnectionHost];
    }
    [self doCamera:pic_path];
    
    _hud.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        _hud.hidden = YES;
    });
}

- (void)kjclick
{
    _index++;
    if(_index >= [_filelist count]){
        _index = [_filelist count]-1;
        return;
    }
    _totalMovieDuration = 0;
    [self playWithIndex:_index];
    [self.player setDrawable:_playerView];
    [self setMoviePlay];
    
    _next.enabled = NO;
    _last.enabled = NO;
    
    _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        _next.enabled = YES;
        _last.enabled = YES;
    });
}
- (void)ktclick
{
    if(_index==0){
        return;
    }
    _index--;
    _totalMovieDuration = 0;
    [self playWithIndex:_index];
    [self.player setDrawable:_playerView];
    [self setMoviePlay];
    
    _next.enabled = NO;
    _last.enabled = NO;
    
    _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        _next.enabled = YES;
        _last.enabled = YES;
    });
}

- (void)EnterBackground
{
    if(_isLiveStream==YES)return;
    
    if(_player==nil)return;
    [_player pause];
    isPlay = NO;
    
    _play.hidden = NO;
    _stop.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
            //记录开始点击的位置
        _startPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
        //单指移动
        CGPoint tempPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
        
        CGFloat moveX = tempPoint.x - _startPoint.x;
        CGFloat moveY = tempPoint.y - _startPoint.y;
#if 0
        if(fabs(moveX) > fabs(moveY))
        {
            if(_isLiveStream==YES){
                if(moveX > 0){
                    NSLog(@"%zi %zi\n", _pageFlowView.currentPageIndex, _pageFlowView.pageCount);
                    if( _pageFlowView.currentPageIndex< _pageFlowView.pageCount){
                        [_pageFlowView scrollToPage:(_pageFlowView.currentPageIndex+1)];
                    }
                }else{
                    NSLog(@"%zi %zi\n", _pageFlowView.currentPageIndex, _pageFlowView.pageCount);
                    if( _pageFlowView.currentPageIndex> 0){
                        [_pageFlowView scrollToPage:(_pageFlowView.currentPageIndex-1)];
                    }
                }
            }
        }
        else
        {
//            if(_startPoint.x > [UIScreen mainScreen].bounds.size.height/2)
            {
                float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
                float newVolume = volume;
                
                if(fabs(moveY) > 5.0){
                    newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    
                    if (newVolume < 0) {
                        newVolume = 0;
                    } else if (newVolume > 1.0) {
                        newVolume = 1.0;
                    }
                    
                    [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
                }
            }
/*
            else
            {
                float brightness = [[UIScreen mainScreen] brightness];
                float newbrightness = brightness;
                
                if(fabs(moveY) > 5.0){
                    newbrightness -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    if (newbrightness < 0) {
                        newbrightness = 0;
                    } else if (newbrightness > 1.0) {
                        newbrightness = 1.0;
                    }
                    
                    [[UIScreen mainScreen] setBrightness:newbrightness];
                }
            }
*/
        }
#else
        float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
        float newVolume = volume;
        
        if(fabs(moveY) > 5.0){
            newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
            
            if (newVolume < 0) {
                newVolume = 0;
            } else if (newVolume > 1.0) {
                newVolume = 1.0;
            }
            [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
        }
#endif
    }
}

- (void)playWithIndex:(NSUInteger)index
{
    _index= index;

    NSURL *url = [NSURL URLWithString:_filelist[index]];
    [self initwithURL:url];
}

- (void)initwithURL:(NSURL *)url
{
    if(_player!=nil){
        [_player stop];
    }
    
    NSArray* options;
    
    VLCMediaPlayer *player;
    
    if(_isLiveStream==NO){
        options= nil;
    }else{
        options= @[@"--clock-synchro=1", @"--network-caching=150", @"--fullscreen"];
    }
    
    player= [[VLCMediaPlayer alloc] initWithOptions:options];
    self.player = player;
    
    self.player.media = [VLCMedia mediaWithURL:url];
    _url = url;
}

- (void)psclick
{
    if (isPlay){
        [self setMovieParse];
    } else {
        [self setMoviePlay];
    }
}

#pragma mark - 暂停
- (void)setMovieParse {
    if(_player!=nil){
        [_player pause];
    }
    isPlay = NO;
    _play.hidden = NO;
    _stop.hidden = YES;
}
#pragma mark - 播放
- (void)setMoviePlay {
    if(_player!=nil){
        [_player play];
    }
    isPlay = YES;
    _play.hidden = YES;
    _stop.hidden = NO;
}

#pragma mark -  添加进度观察 - addProgressObserver
- (void)addProgressObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaPlayerTimeChanged:) name:VLCMediaPlayerTimeChanged object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaPlayerSnapshot:) name:VLCMediaPlayerSnapshotTaken object:nil];
}

- (void)actionTapGesture:(UITapGestureRecognizer *)sender
{
    if(_isLiveStream==NO){
        CGPoint touchPoint = [sender locationInView:_topProgressSlider];
        CGFloat value = (_topProgressSlider.maximumValue - _topProgressSlider.minimumValue) * (touchPoint.x / _topProgressSlider.frame.size.width);
        [_topProgressSlider setValue:value animated:YES];

        int totalMovieDuration = _totalMovieDuration * _topProgressSlider.value;
        [_player setTime:[[VLCTime alloc] initWithInt:totalMovieDuration]];
        [self.player play];
    }
}

- (UIImage *)thumbnailImage:(NSString *)path :(NSTimeInterval)time
{
    NSLog(@"url = %@ %f\n",path, time);
    MPMoviePlayerController * mplayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:path]];
    mplayer.shouldAutoplay = NO;
    
//    NSLog(@"AAA..........\n");
    UIImage * img = [mplayer thumbnailImageAtTime:time timeOption:MPMovieTimeOptionNearestKeyFrame];
//    NSLog(@"BBB..........\n");
    
    if(img==nil){
        NSLog(@"thumbnailImageAtTime:%@ %f fault\n",path,time);
    }
    
    return img;

//    return nil;
}

-(void)dothumbnail
{
    NSTimeInterval value = _topProgressSlider.value * _VideoTotalLength;
    for(PageFlowObj * n in _videoArray){
        if( value > n.sTimeInterval && value < n.pTimeInterval ){
            NSString * path = [NSString stringWithFormat: @"http://%@/DCIM/%@",[WiFiConnection sharedSingleton].NTConnectionHost,n.name];
            _preView.image = [self thumbnailImage:path :value-n.sTimeInterval];
        }
    }

    _isthumbnail = NO;
    NSLog(@"test:NO\n");
    [NSThread exit];
}

-(void)sliderValueChanged:(id)sender
{
    if(_isLiveStream==NO){
        UISlider *senderSlider = sender;
        int totalMovieDuration = _totalMovieDuration * senderSlider.value;
        VLCTime * dragedCMTime = [[VLCTime alloc] initWithInt:totalMovieDuration];
        [_player setTime:dragedCMTime];
    }else{
        _isReview = YES;
        [_player stop];
        _preView.hidden = NO;
        _pageTimeLabel.hidden = NO;
        _playButton.hidden = NO;
        
        NSTimeInterval value = _topProgressSlider.value * _VideoTotalLength;
        for(PageFlowObj * n in _videoArray){
            if( value > n.sTimeInterval && value < n.pTimeInterval ){
                NSDate * sDate = [NSDate dateWithTimeInterval:value-n.sTimeInterval sinceDate:n.startTime];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                _pageTimeLabel.text = [dateFormatter stringFromDate:sDate];
            }
        }
        
        if(_isthumbnail==NO){
            _isthumbnail = YES;
            NSThread * thread = [[NSThread alloc] initWithTarget:self selector:@selector(dothumbnail) object:nil];
            [thread start];
        }
    }
}

-(void)sliderValueChangedEnd:(id)sender
{
    if(_isthumbnail==NO){
        _isthumbnail = YES;
        NSThread * thread = [[NSThread alloc] initWithTarget:self selector:@selector(dothumbnail) object:nil];
        [thread start];
    }else{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
            static int i= 0;
            NSLog(@"sleep = %d %d\n",i , _isthumbnail);
            if(_isthumbnail==NO){
                _isthumbnail = YES;
                NSLog(@"_thread start\n");
                NSThread * thread = [[NSThread alloc] initWithTarget:self selector:@selector(dothumbnail) object:nil];
                [thread start];
                NSLog(@"_thread start end\n");
                i= 0;
                return;
            }else{
                i++;
                if(i > 10)
                {
                    i= 0;
                    _preView.image = nil;
                    NSLog(@"time out........\n");
                    while(1);
                    return;
                }
            }
        });
        //[self performSelector:@selector(delayexec) withObject:nil afterDelay:1];
    }
}

//  播放页面添加轻拍手势
- (void)addGestureRecognizer
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAllSubViews:)];
    tap.delegate = self;
    [_playerView addGestureRecognizer:tap];
}

- (void)hiddenAllView
{
    self.isHiddenAllView = YES;
    
    [_processView setHidden:YES];
    [_topView setHidden:YES];
    [_controlView setHidden:YES];
}

- (void)DisplayAllView
{
    self.isHiddenAllView = NO;
    _processView.hidden = NO;
    _topView.hidden = NO;
    _controlView.hidden = NO;
}

- (void)dismissAllSubViews:(UITapGestureRecognizer *)tap
{
    if (!self.isHiddenAllView)
    {
        [self hiddenAllView];
    }
    else
    {
        [self DisplayAllView];
    }
}

#pragma mark - 观察者 观察播放完毕
- (void)addNotificationCenters
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaPlayerStateChanged:) name:VLCMediaPlayerStateChanged object:nil];
}
#if 1
- (void)mediaPlayerStateChanged:(NSNotification *)aNotification
{
    switch (_player.state)
    {
        case VLCMediaPlayerStatePaused:
            NSLog(@"VLCMediaPlayerStatePaused\n");
            break;
        case VLCMediaPlayerStateStopped:
            NSLog(@"VLCMediaPlayerStateStopped\n");
#if 1
            if(_isContinuous==YES){
                NSInteger index = [_videoArray indexOfObject:_currentPlayVideo]+ 1;
                
                if(index >= _videoArray.count)return;
                
                PageFlowObj * current = _videoArray[index];
                NSString * path = [NSString stringWithFormat: @"http://%@/DCIM/%@",[WiFiConnection sharedSingleton].NTConnectionHost,current.name];
                NSURL * url = [NSURL URLWithString:path];
                VLCMediaPlayer *player;
                
                player= [[VLCMediaPlayer alloc] initWithOptions:nil];
                self.player = player;
                self.player.media = [VLCMedia mediaWithURL:url];
                [self.player setDrawable:_playerView];
                _currentPlayVideo = current;
                _url = url;
                if(_player!=nil){
                    [_player play];
                }
            }
#endif
            break;
        case VLCMediaPlayerStateBuffering:
            NSLog(@"VLCMediaPlayerStateBuffering\n");
            break;
        case VLCMediaPlayerStatePlaying:
            NSLog(@"VLCMediaPlayerStatePlaying\n");
            break;
        case VLCMediaPlayerStateEnded:
            NSLog(@"VLCMediaPlayerStateEnded\n");
            break;
        case VLCMediaPlayerStateError:
            NSLog(@"VLCMediaPlayerStateError\n");
            break;
        default:
            break;
    }
}
#endif

- (void)mediaPlayerTimeChanged:(NSNotification *)aNotification
{
    if(_isLiveStream==NO){
        NSString * currentTime = [_player.time stringValue];
        NSString * remainingTime = [_player.remainingTime stringValue];
    
        _topPastTimeLabel.text = currentTime;
        _totalTimeLabel.text = remainingTime;

        if(_totalMovieDuration==0)
        {
            _totalMovieDuration = [_player.remainingTime intValue]*-1 + [_player.time intValue];
        }
        _topProgressSlider.value =  [_player.time intValue]/1.0 / _totalMovieDuration;
    }else{
        int totalTime = (int)(_currentPlayVideo.sTimeInterval/1)+ ([_player.time intValue]/1000);
        _topProgressSlider.value =  totalTime/ _VideoTotalLength;
    }
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    /* Step 2. */
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        if ([annotation.title isEqualToString:@"Car"])
        {
            static NSString *pointReuseIndetifier = @"pointReuseIndetifier";
            
            UIImage *imge  =  [UIImage imageNamed:@"userPosition"];
            MAAnnotationView * annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                                             reuseIdentifier:pointReuseIndetifier];
            annotationView.image =  imge;
            //            CGPoint centerPoint= CGPointZero;
            //            [annotation setCenterOffset:centerPoint];
            return annotationView;
        }
    }
    
    return nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 禁用 iOS7 返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)backclick:(UIButton *)btn
{
    if(self.isLiveStream == YES){
        [self savePreview];
    }
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    
    if(_player!=nil){
        [_player stop];
        _player = nil;
    }
    
    [_filelist removeAllObjects];
    
    NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if(_isLiveStream==YES){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTP_TS_TRANS_STOP"];
    }
}

- (void)statusBarOrientationChange:(NSNotification *)notification
{
    _RotatorTap = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self doRotatorFrame];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if(_isLiveStream==NO)
    {
        return UIInterfaceOrientationMaskLandscape;
    }else{
        return UIInterfaceOrientationMaskAll;
    }
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
