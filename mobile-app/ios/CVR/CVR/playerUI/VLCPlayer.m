//
//  VLCPlayer.m
//  CVR
//
//  Created by 雷起斌 on 6/16/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "VLCPlayer.h"
#import <MobileVLCKit/MobileVLCKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <MAMapKit/MAMapKit.h>
#import "nmea_parse.h"
#import "transform.h"
#import "RNGridMenu.h"
#import "HMSideMenu.h"

#define RealTimePreviewRPWidth   384
#define RealTimePreviewRPHeight  640

#define mainScreenWidth     [UIScreen mainScreen].bounds.size.width
#define mainScreenHeight    [UIScreen mainScreen].bounds.size.height

#define UIInterfaceOrientationPortraitTopViewFrame  CGRectMake(0, 0, mainScreenWidth, 40)
#define UIInterfaceOrientationLandscapeTopViewFrame CGRectMake(0, 0, mainScreenWidth, 40)
#define UIInterfaceOrientationLandscapeTopViewColor [UIColor blackColor]

#define UIInterfaceOrientationPortraitPlayViewFrame CGRectMake(0, 0, mainScreenWidth, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight)
#define UIInterfaceOrientationLandscapePlayViewFrame CGRectMake(0, 0, mainScreenWidth, mainScreenHeight)

#define UIInterfaceOrientationPortraitProccessViewFrame CGRectMake(0, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight, [UIScreen mainScreen].bounds.size.width, 30)
#define UIInterfaceOrientationLandscapeProccessViewFrame CGRectMake(0, mainScreenHeight-40, mainScreenWidth, 40)

#define UIInterfaceOrientationPortraitControlViewFrame CGRectMake((mainScreenWidth- 300)/ 2, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight- 40- 10, 300, 40)
#define UIInterfaceOrientationLandscapeControlViewFrame CGRectMake((mainScreenWidth- 400)/ 2, mainScreenHeight- 100, 400, 40 )

@interface VLCPlayer ()<UIGestureRecognizerDelegate,MAMapViewDelegate,CAAnimationDelegate, RNGridMenuDelegate>

@property (nonatomic, assign) float progressSlider;
@property (nonatomic, assign) BOOL isPlay;
@property (nonatomic, assign) CGPoint startPoint;

@property (nonatomic, assign) BOOL isLiveStream;
@property (strong, nonatomic) VLCMediaPlayer *player;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) UIView *processView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIView *controlView;
@property (strong, nonatomic) UIView *playerView;

@property (strong, nonatomic) UIButton * backButton;

@property (strong, nonatomic) UIButton * delete;
@property (strong, nonatomic) UIButton * camera;
@property (strong, nonatomic) UIButton * last;
@property (strong, nonatomic) UIButton * next;
@property (strong, nonatomic) UIButton * play;
@property (strong, nonatomic) UIButton * stop;
@property (strong, nonatomic) UISwitch * fllow;

@property (strong, nonatomic) UISlider *topProgressSlider;
@property (strong, nonatomic) UILabel *topPastTimeLabel;
@property (strong, nonatomic) UILabel *totalTimeLabel;
@property (strong, nonatomic) UITapGestureRecognizer  *tapGesture;

@property (nonatomic, assign) BOOL isHiddenAllView;
@property (nonatomic, assign) BOOL isLiveLandscapeEnter;

@property (nonatomic, assign) int totalMovieDuration;

@property (nonatomic, strong) NSFileManager * filemanager;

@property (strong, nonatomic) NSArray *video_list;
@property (strong, nonatomic) UILabel *hud;
@property (assign, nonatomic) NSInteger language;
@property (assign, nonatomic) UIInterfaceOrientation oriention;
@property (nonatomic, assign) CGSize size1;
@property (nonatomic, assign) CGSize size2;

@property (nonatomic, strong) MAMapView * map;
@property (nonatomic, strong) MAAnnotationView * annotationView;
@property (nonatomic, strong) MAPointAnnotation * car;
@property (nonatomic, assign) CLLocationCoordinate2D * coords;
@property (nonatomic, assign) CLLocationCoordinate2D * route_coords;
@property (nonatomic, assign) NSUInteger count;
@property (nonatomic, assign) NSUInteger malloc_count;
@property (nonatomic, assign) NSUInteger route_count;
@property (nonatomic, assign) int route_start;
@property (nonatomic, strong) MAPolyline *polyline;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, assign) BOOL setCenterCoordinate;
@property (nonatomic, strong) NSMutableArray * gpsArray;
@property (nonatomic, strong) NSString * gps;
@property (nonatomic, strong) NSString * playvideo;
@property (nonatomic, strong) NSString * gpspath;
@property (nonatomic, strong) NSThread * thread;

@property (assign, nonatomic) NSInteger mode_flag;          // 0:undefine 1:recode mode 2:photo mode 3:lapse 4:burst
@property (assign, nonatomic) NSInteger lapse_arg;
@property (assign, nonatomic) NSInteger burst_arg;

@property (nonatomic, assign) BOOL isModeSwitch;
@property (nonatomic, assign) BOOL menuIsVisible;
@property (nonatomic, strong) HMSideMenu *sideMenu;
@property (nonatomic, strong) HMSideMenu *sideMenuleft;

@property (nonatomic, strong) UIView *offItem;
@property (nonatomic, strong) UIView *oneItem;
@property (nonatomic, strong) UIView *twoItem;
@property (nonatomic, strong) UIView *threeItem;
@property (nonatomic, strong) UIView *fourItem;
@property (nonatomic, strong) UIView *fiveItem;

@property (nonatomic, strong) UIImageView *offIcon;
@property (nonatomic, strong) UIImageView *oneIcon;
@property (nonatomic, strong) UIImageView *twoIcon;
@property (nonatomic, strong) UIImageView *threeIcon;
@property (nonatomic, strong) UIImageView *fourIcon;
@property (nonatomic, strong) UIImageView *fiveIcon;
@end

@implementation VLCPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;

    _oriention = [UIApplication sharedApplication].statusBarOrientation;
    
    _filemanager = [NSFileManager defaultManager];
    _gpsArray = [[NSMutableArray alloc] init];
    self.sideMenuleft = nil;
    
    self.isHiddenAllView = NO;
    
    [self PlayViewInit];
    [self processViewInit];
    [self controlViewInit];
    [self topViewInit];
    [self hudViewInit];
    
    [self addGestureRecognizer];
    [self addNotificationCenters];
    [self addProgressObserver];

    if(_isLiveStream==YES){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRecordingState:) name:@"GetRecordingState" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ModeChange:) name:@"ModeChange" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAckLiveStart:) name:@"onAckLiveStart" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetVideoList:) name:@"onGetVideoList" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGetLiveVideo:) name:@"onGetLiveVideo" object:nil];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_MODE"];
    }else{
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(GetGpsList:) name:@"GetGpsList" object:nil];
        [[WiFiConnection sharedSingleton] GetGpsContents:_playvideo];
        self.sideMenuleft.hidden = YES;
    }
    
    if(_isLiveLandscapeEnter==YES){
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2*3);
    }
    
    if([_form isEqualToString:@"CVR_GPS"]==NO){
        NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    }
    
    if(_player!=nil){
        [self.player play];
    }
}

- (void)onGetLiveVideo:(NSNotification *) notification
{
     [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTSP_TRANS_START"];
}

- (void)onGetVideoList:(NSNotification *) notification
{
    _video_list= [notification object];
}

- (void)onAckLiveStart:(NSNotification *) notification
{
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_Control_Recording"];
}

- (void)ModeChange:(NSNotification *) notification
{
    NSString * aStr = [notification object];
#if 0
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_LIVE_STOP"];
    [_player stop];
    if(_isLiveStream==YES){
        usleep(50000);
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTSP_TRANS_START"];
        [_player play];
    }
#endif
    
    NSArray * Head= [aStr componentsSeparatedByString:@"&"];
    if(Head==nil){
        _lapse_arg= -1;
        _burst_arg= -1;
        _mode_flag= 0;
        [self doRotatorFrame];
        _offIcon.highlighted = YES;
        return;
    }
    if([Head count]< 2){
        _lapse_arg= -1;
        _burst_arg= -1;
        _mode_flag= 0;
        [self doRotatorFrame];
        _offIcon.highlighted = YES;
        return;
    }
    
    if( [Head[0] isEqualToString:@"RECORDING"] ){
        _mode_flag= 1;
        [self doRotatorFrame];
        _offIcon.highlighted = YES;
    }else if( [Head[0] isEqualToString:@"PHOTO"] ){
        _mode_flag= 2;
        [self doRotatorFrame];
        _offIcon.highlighted = YES;
    }else if([Head[0] rangeOfString:@"LAPSE"].location != NSNotFound){
        _burst_arg= -1;
        _mode_flag= 3;
        [self doRotatorFrame];
        
        _lapse_arg= [Head[1] integerValue];
        if(_lapse_arg==1)
            _oneIcon.highlighted= YES;
        else if(_lapse_arg==5)
            _twoIcon.highlighted= YES;
        else if(_lapse_arg==10)
            _threeIcon.highlighted= YES;
        else if(_lapse_arg==30)
            _fourIcon.highlighted= YES;
        else if(_lapse_arg==60)
            _fiveIcon.highlighted= YES;
        else
            _offIcon.highlighted = YES;
    }else if([Head[0] rangeOfString:@"BURST"].location != NSNotFound){
        _lapse_arg= -1;
        _burst_arg= [Head[1] integerValue];
        [self doRotatorFrame];
        
        if(_burst_arg==3)
            _oneIcon.highlighted= YES;
        else if(_burst_arg==4)
            _twoIcon.highlighted= YES;
        else if(_burst_arg==5)
            _threeIcon.highlighted= YES;
        else
            _offIcon.highlighted = YES;
        _mode_flag= 4;

    }
    else{
        [self doRotatorFrame];
    }
    
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTSP_TRANS_START"];
}

- (void)sideMenuInit
{
    if(self.sideMenuleft!=nil){
        [self.sideMenuleft removeFromSuperview];
        self.sideMenuleft= nil;
    }
    if(self.sideMenu!=nil){
        [self.sideMenu removeFromSuperview];
        self.sideMenu= nil;
    }
    
    UIView *modeItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [modeItem setMenuActionWithBlock:^{
        [self showGrid];
    }];
    UIImageView *modeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [modeIcon setImage:[UIImage imageNamed:@"icon_switch"]];
    [modeItem addSubview:modeIcon];
    modeItem.backgroundColor = [UIColor grayColor];
    modeItem.alpha = 0.4f;
    
    UIView *switchItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [switchItem setMenuActionWithBlock:^{
        [self showSwitchGrid];
    }];
    UIImageView *switchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [switchIcon setImage:[UIImage imageNamed:@"icon_video"]];
    [switchItem addSubview:switchIcon];
    switchItem.backgroundColor = [UIColor grayColor];
    switchItem.alpha = 0.4f;
    
    self.sideMenuleft = [[HMSideMenu alloc] initWithItems:@[modeItem, switchItem]];
    [self.sideMenuleft setItemSpacing:5.0f];
    self.sideMenuleft.menuPosition = HMSideMenuPositionLeft;
    
    [self.view addSubview:self.sideMenuleft];
//    [self.sideMenuleft open];
    
    if([_form isEqualToString:@"SportDV"]==NO)
        return;
    
    _oneItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _oneItem.userInteractionEnabled = YES;
    [_oneItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1|| _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:1"];
        }else{
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGBurst:3"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _oneIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40 , 40)];
    
    _twoItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _twoItem.userInteractionEnabled = YES;
    [_twoItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1|| _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:5"];
        }else{
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGBurst:4"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _twoIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    _threeItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _threeItem.userInteractionEnabled = YES;
    [_threeItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1|| _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:10"];
        }else{
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGBurst:5"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _threeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    _fourItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _fourItem.userInteractionEnabled = YES;
    [_fourItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1 || _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:30"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _fourIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    _fiveItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _fiveItem.userInteractionEnabled = YES;
    [_fiveItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1 || _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:60"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _fiveIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    
    _offItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _offItem.userInteractionEnabled = YES;
    [_offItem setMenuActionWithBlock:^{
        NSString * msg;
        if(_mode_flag==1 || _mode_flag==3){
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGTimeLapse:OFF"];
        }else{
            msg = [NSString stringWithFormat:@"CMD_ARGSETTINGBurst:OFF"];
        }
        [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    }];
    _offIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40 , 40)];
    
    if(_mode_flag==1||_mode_flag==3){
        [_offIcon setImage:[UIImage imageNamed:@"icon_close_01"]];
        [_offIcon setHighlightedImage:[UIImage imageNamed:@"icon_close_02"]];
        [_offItem addSubview:_offIcon];
        _offItem.backgroundColor = [UIColor grayColor];
        _offItem.alpha = 0.3f;
        
        [_oneIcon setImage:[UIImage imageNamed:@"icon_1s"]];
        [_oneIcon setHighlightedImage:[UIImage imageNamed:@"icon_1s_02"]];
        [_oneItem addSubview:_oneIcon];
        _oneItem.backgroundColor = [UIColor grayColor];
        _oneItem.alpha = 0.3f;
        
        [_twoIcon setImage:[UIImage imageNamed:@"icon_5s"]];
        [_twoIcon setHighlightedImage:[UIImage imageNamed:@"icon_5s_02"]];
        [_twoItem addSubview:_twoIcon];
        _twoItem.backgroundColor = [UIColor grayColor];
        _twoItem.alpha = 0.3f;
        
        [_threeIcon setImage:[UIImage imageNamed:@"icon_10s"]];
        [_threeIcon setHighlightedImage:[UIImage imageNamed:@"icon_10s_02"]];
        [_threeItem addSubview:_threeIcon];
        _threeItem.backgroundColor = [UIColor grayColor];
        _threeItem.alpha = 0.3f;
        
        [_fourIcon setImage:[UIImage imageNamed:@"icon_30s"]];
        [_fourIcon setHighlightedImage:[UIImage imageNamed:@"icon_30s_02"]];
        [_fourItem addSubview:_fourIcon];
        _fourItem.backgroundColor = [UIColor grayColor];
        _fourItem.alpha = 0.3f;
        
        [_fiveIcon setImage:[UIImage imageNamed:@"icon_60s"]];
        [_fiveIcon setHighlightedImage:[UIImage imageNamed:@"icon_60s_02"]];
        [_fiveItem addSubview:_fiveIcon];
        _fiveItem.backgroundColor = [UIColor grayColor];
        _fiveItem.alpha = 0.3f;
        
        self.sideMenu = [[HMSideMenu alloc] initWithItems:@[_oneItem, _twoItem, _threeItem, _fourItem, _fiveItem, _offItem]];
    }else if(_mode_flag==2||_mode_flag==4){
        [_oneIcon setImage:[UIImage imageNamed:@"icon_3"]];
        [_oneIcon setHighlightedImage:[UIImage imageNamed:@"icon_3_02"]];
        [_oneItem addSubview:_oneIcon];
        _oneItem.backgroundColor = [UIColor grayColor];
        _oneItem.alpha = 0.3f;
        
        [_twoIcon setImage:[UIImage imageNamed:@"icon_4"]];
        [_twoIcon setHighlightedImage:[UIImage imageNamed:@"icon_4_02"]];
        [_twoItem addSubview:_twoIcon];
        _twoItem.backgroundColor = [UIColor grayColor];
        _twoItem.alpha = 0.3f;
        
        [_threeIcon setImage:[UIImage imageNamed:@"icon_5"]];
        [_threeIcon setHighlightedImage:[UIImage imageNamed:@"icon_5_02"]];
        [_threeItem addSubview:_threeIcon];
        _threeItem.backgroundColor = [UIColor grayColor];
        _threeItem.alpha = 0.3f;
        
        [_offIcon setImage:[UIImage imageNamed:@"icon_close_01"]];
        [_offIcon setHighlightedImage:[UIImage imageNamed:@"icon_close_02"]];
        [_offItem addSubview:_offIcon];
        _offItem.backgroundColor = [UIColor grayColor];
        _offItem.alpha = 0.3f;
        
        self.sideMenu = [[HMSideMenu alloc] initWithItems:@[_oneItem, _twoItem, _threeItem, _offItem]];
    }
    [self.sideMenu setItemSpacing:5.0f];
    [self.view addSubview:self.sideMenu];

//    [self.sideMenu open];
}

////////////////////////////////////////////////////////////////////////
#pragma mark - RNGridMenuDelegate
////////////////////////////////////////////////////////////////////////

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex
{
    if(_isModeSwitch==NO){
        if(itemIndex==0){
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_CHANGE_MODE:RECORDING"];
        }else if(itemIndex==1){
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_CHANGE_MODE:PHOTO"];
        }
    }else{
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_LIVE_STOP"];
        usleep(500000);
        NSString * str = [NSString stringWithFormat:@"CMD_SET_LIVE_VIDEO:%@",_video_list[itemIndex*2+1]];
        NSLog(@"%@\n",str);
        [[WiFiConnection sharedSingleton] SendMsgToCVR:str];
    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - Private
////////////////////////////////////////////////////////////////////////

- (void)showGrid {
    _isModeSwitch= NO;
    NSInteger numberOfOptions = 2;
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"icon_video_02-1"] title:@"Record"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"icon_camera_02-1"] title:@"Photo"],
                       ];
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.bounces = NO;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

- (void)showSwitchGrid {
    unsigned long int i;
    if(_video_list.count<= 1)
        return;
    _isModeSwitch= YES;
    
    NSInteger numberOfOptions = (_video_list.count-1)/2;
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    i= (_video_list.count-1)/2;
    for(int j= 0;j< i;j++){
        [items addObject:[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"icon_video_02-1"] title:_video_list[j*2]]];
    }
    
    NSArray *items2= [items copy];
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[items2 subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

- (void)settingclick
{
    if (self.sideMenuleft.isOpen==YES)
        [self.sideMenuleft close];
    else
        [self.sideMenuleft open];
    
    if (self.sideMenu.isOpen==YES)
        [self.sideMenu close];
    else
        [self.sideMenu open];
}

- (void)animationDidStart:(CAAnimation *)anim
{
    [self makeMapViewEnable:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag)
    {
        [self.map addOverlay:self.polyline];
        [self.shapeLayer removeFromSuperlayer];
    }
    
    [self makeMapViewEnable:YES];
}

- (MAOverlayView *)mapView:(MAMapView *)mapView viewForOverlay:(id <MAOverlay>)overlay
{
    MAPolylineView *polylineView = [[MAPolylineView alloc] initWithPolyline:overlay];
    
    if(polylineView.polyline == _polyline){
        polylineView.lineWidth   = 4.f;
        polylineView.strokeColor = [UIColor redColor];
    }else{
        polylineView.lineWidth   = 4.f;
        polylineView.strokeColor = [UIColor blueColor];
    }
    
    return polylineView;
}

/* Enable/Disable mapView. */
- (void)makeMapViewEnable:(BOOL)enabled
{
    self.map.scrollEnabled          = enabled;
    self.map.zoomEnabled            = enabled;
}

/* 经纬度转屏幕坐标, 调用着负责释放内存. */
- (CGPoint *)pointsForCoordinates:(CLLocationCoordinate2D *)coordinates count:(NSUInteger)count
{
    if (coordinates == NULL || count <= 1)
    {
        return NULL;
    }
    
    /* 申请屏幕坐标存储空间. */
    CGPoint *points = (CGPoint *)malloc(count * sizeof(CGPoint));
    
    /* 经纬度转换为屏幕坐标. */
    for (int i = 0; i < count; i++)
    {
        points[i] = [self.map convertCoordinate:_route_coords[i] toPointToView:self.map];
    }
    
    return points;
}

/* 构建path, 调用着负责释放内存. */
- (CGMutablePathRef)pathForPoints:(CGPoint *)points count:(NSUInteger)count
{
    if (points == NULL || count <= 1)
    {
        return NULL;
    }
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathAddLines(path, NULL, points, count);
    
    return path;
}

- (void)getGpsStartTime
{
    if(_gpsArray==nil)
        return;
    
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    
    [dataFormatter setDateFormat:@"HHmmss"];

    if(self.gps==nil || [self.gps isEqualToString:@"NULL"]){
        NSLog(@"get start error gps = nil\n");
        _route_start = 0;
        return;
    }
    
    if(_playvideo==nil){
        NSLog(@"get start error _playvideo = nil\n");
        return;
    }
    
    NSDate * date1 = [dataFormatter dateFromString:[self.gps substringWithRange:NSMakeRange(9, 6)]];
    NSDate * date2 = [dataFormatter dateFromString:[_playvideo substringWithRange:NSMakeRange(9, 6)]];
    
    _route_start = [_gpsArray indexOfObject:self.gps]*60+ [date2 timeIntervalSince1970]- [date1 timeIntervalSince1970];
    if(_route_start< 0){
        NSLog(@"_route_start < 0 !!!\n");
        _route_start= 0;
    }
    
//    if(_coords[_route_start].latitude!= -1 && _coords[_route_start].longitude!= -1)
//        self.car.coordinate = _coords[_route_start];
//    else
//        self.car.coordinate = _route_coords[0];
}

- (void)initRoute
{
    struct gps_info info;
    const char * char_path;
    
    _count = 0;
    _route_count= 0;
    _route_start= 0;
    
    if (_coords != NULL)
    {
        free(_coords), _coords = NULL;
    }
    _coords = malloc(60 * 5 * sizeof(CLLocationCoordinate2D)* _gpsArray.count);
    
    if(_route_coords!=NULL){
        free(_route_coords), _route_coords = NULL;
    }
    _route_coords = malloc(60 * 5 * sizeof(CLLocationCoordinate2D)* _gpsArray.count);
    
    _filemanager = [NSFileManager defaultManager];
    NSString * gps_path;
    for( NSString * gps in _gpsArray ){
        gps_path = [NSString stringWithFormat: @"%@/%@", NSTemporaryDirectory(), gps];
        if([_filemanager fileExistsAtPath:gps_path]!=YES){
            NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@", [WiFiConnection sharedSingleton].NTConnectionHost, _gpspath, gps];
            NSURL * url = [NSURL URLWithString:path];
            NSData *data = [NSData dataWithContentsOfURL:url];
            if([[NSThread currentThread] isCancelled]){
                [NSThread exit];
            }
            
            [data writeToFile:gps_path atomically:NO];
            if([[NSThread currentThread] isCancelled]){
                [NSThread exit];
            }
        }
        
        char_path = [gps_path UTF8String];
        gps_set_data_from_fp((char *)char_path);
        
        while(gps_get_data_from_fp(&info)==1){
            double wgsLat = gps_degree_minute_to_degree(info.latitude);
            double wgsLng = gps_degree_minute_to_degree(info.longitude);
            double gcjLat , gcjLng;

            wgs2gcj(wgsLat, wgsLng, &gcjLat, &gcjLng);
            
            if(wgsLat==0&&wgsLng==0){
                _coords[_count++]= CLLocationCoordinate2DMake(-1 ,-1);
                continue;
            }
            
            _coords[_count++]= CLLocationCoordinate2DMake(gcjLat ,gcjLng);
            _route_coords[_route_count++]= CLLocationCoordinate2DMake(gcjLat ,gcjLng);
        }
        gps_close_data_from_fp();
    }
    
    self.polyline = [MAPolyline polylineWithCoordinates:_route_coords count:_route_count];
    [self getGpsStartTime];
}

/* 构建shapeLayer. */
- (void)initShapeLayer
{
    self.shapeLayer = [[CAShapeLayer alloc] init];
    self.shapeLayer.lineWidth         = 4;
    self.shapeLayer.strokeColor       = [UIColor redColor].CGColor;
    self.shapeLayer.fillColor         = [UIColor clearColor].CGColor;
    self.shapeLayer.lineJoin          = kCALineCapRound;
}

- (void)showRouteForCoords:(CLLocationCoordinate2D *)coords count:(NSUInteger)count
{
    MAPolyline *route = [MAPolyline polylineWithCoordinates:coords count:count];
    [self.map addOverlay:route];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    /* Step 2. */
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        if ([annotation.title isEqualToString:@"Car"])
        {
            static NSString *pointReuseIndetifier = @"trackingReuseIndetifier";
            MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndetifier];
            if (annotationView == nil){
                annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndetifier];
            }
            annotationView.canShowCallout = NO;
            
            annotationView.image = [UIImage imageNamed:@"ball"];
            //annotationView.image =  [UIImage imageNamed:@"userPosition"];
            return annotationView;
        }
    }
    
    return nil;
}

/* 构建shapeLayer的basicAnimation. */
- (CAAnimation *)constructShapeLayerAnimation
{
    CABasicAnimation *theStrokeAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    theStrokeAnimation.duration         = 2.f;
    theStrokeAnimation.fromValue        = @0.f;
    theStrokeAnimation.toValue          = @1.f;
    
    return theStrokeAnimation;
}

/* 构建annotationView的keyFrameAnimation. */
- (CAAnimation *)constructAnnotationAnimationWithPath:(CGPathRef)path
{
    if (path == NULL)
    {
        return nil;
    }
    
    CAKeyframeAnimation *thekeyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    thekeyFrameAnimation.duration        = 2.f;
    thekeyFrameAnimation.path            = path;
    thekeyFrameAnimation.calculationMode = kCAAnimationPaced;
    
    return thekeyFrameAnimation;
}

- (void)hudViewInit
{
    UILabel *labelOne = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelOne.backgroundColor = [UIColor clearColor];
    labelOne.textColor = [UIColor whiteColor];
    labelOne.font = [UIFont systemFontOfSize:28];
    labelOne.numberOfLines = 0;
    [labelOne sizeToFit];
    labelOne.center = CGPointMake(self.view.center.y, self.view.center.x);
    
    _hud= labelOne;
    _hud.hidden = YES;
    [self.view addSubview:_hud];
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
        labelOne.text = @"截图成功";
    }else{
        _language = 0;
        labelOne.text = @"Screenshot success";
    }
}

- (void)topViewInit
{
    UIView * topView = [[UIView alloc] init];
    _topView= topView;
    
    if(_isLiveStream==NO){
        topView.frame = UIInterfaceOrientationPortraitTopViewFrame;
    }else{
        topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
    }
    
    topView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;

    _backButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
    [_backButton setBackgroundImage:[UIImage imageNamed:@"play_black_02"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backclick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_backButton];
    [self.view addSubview:topView];
    _topView.alpha = 0.6f;
    
    if(_isLiveLandscapeEnter==YES)
        _topView.frame = CGRectMake(0, 0, mainScreenHeight, 40);
    else
        _topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
}

- (void)PlayViewInit
{
    UIView * playerView = [[UIView alloc] init];
    _playerView = playerView;
    
//    if(_isLiveStream==NO){
        _playerView.frame = UIInterfaceOrientationPortraitPlayViewFrame;
    
        if(_isLiveStream==NO)
            _map = [[MAMapView alloc] initWithFrame:CGRectMake(0, playerView.frame.origin.y+playerView.frame.size.height+30, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height- playerView.frame.size.height-30)];
        else
            _map = [[MAMapView alloc] initWithFrame:CGRectMake(0, playerView.frame.origin.y+playerView.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height- playerView.frame.size.height)];
    
        _map.userTrackingMode = MAUserTrackingModeFollow;
        [_map setDelegate:self];
        [self.view addSubview:_map];
        
        _fllow = [[UISwitch alloc] initWithFrame:CGRectMake(_map.frame.size.width-40-10, _map.frame.size.height-40-10, 40, 40)];
        [_fllow addTarget:self action:@selector(fllowclick:) forControlEvents:UIControlEventValueChanged];
        [_map addSubview:_fllow];
        _map.showsUserLocation = NO;
//    }else{
//        if(_isLiveLandscapeEnter==YES)
//            _playerView.frame = CGRectMake(0, 0, mainScreenHeight, mainScreenWidth);
//        else
//            _playerView.frame = CGRectMake(0, 0, mainScreenWidth, mainScreenHeight);
//    }
    
    [self.view addSubview:playerView];
    _playerView.backgroundColor = [UIColor blackColor];

    [self.player setDrawable:_playerView];
}

-(void)fllowclick:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    if(self.polyline!=NULL){
        [_map setZoomLevel:15 animated:NO];
    }
    
    _setCenterCoordinate = isButtonOn;
}

- (void)processViewInit
{
    if(_isLiveStream==YES)
        return;
    
    if(_processView!=nil){
        [_processView removeFromSuperview];
    }
    
    UIView * buttonView = [[UIView alloc] init];
    buttonView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
    [self.view addSubview:buttonView];
    _processView = buttonView;
    _processView.alpha = 0.6;
    
    UISlider * slider = [[UISlider alloc] init];
    [slider setMinimumValue:0];
    [slider setMaximumValue:1];
    
    [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateHighlighted];
    slider.value = 0;
    
    CGFloat top = 10; // 顶端盖高度
    CGFloat bottom = 10 ; // 底端盖高度
    CGFloat left = 10; // 左端盖宽度
    CGFloat right = 10; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    
    [slider setMinimumTrackImage:[[UIImage imageNamed:@"progress-bar_02.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[[UIImage imageNamed:@"progress-bar_01.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    
    _topProgressSlider = slider;
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
    _tapGesture.delegate = self;
    [slider addGestureRecognizer:_tapGesture];
    [buttonView addSubview:slider];
    
    UILabel * labelright = [[UILabel alloc] initWithFrame:CGRectZero];
    labelright.text = @" -88:88";
    labelright.numberOfLines = 0;
    labelright.textColor = UIColor.whiteColor;
    [labelright sizeToFit];
    _totalTimeLabel = labelright;
    [buttonView addSubview:labelright];
    
    UILabel * labelleft = [[UILabel alloc] initWithFrame:CGRectZero];
    labelleft.text = @" -88:88 ";
    labelleft.numberOfLines = 0;
    labelleft.textColor = UIColor.whiteColor;
    [labelleft sizeToFit];
    _topPastTimeLabel = labelleft;
    [buttonView addSubview:labelleft];
    
    [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    _processView.frame = UIInterfaceOrientationPortraitProccessViewFrame;
  
    _size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _topProgressSlider.frame = CGRectMake(_size1.width, 0, _processView.frame.size.width- _size1.width- _size2.width, _processView.frame.size.height);
    _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
    
    _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
    _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
    
    _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
}

- (void)controlViewInit
{
    if(_controlView!=nil){
        [_controlView removeFromSuperview];
    }
    
    if(_isLiveStream==YES)
    {
        UIView * controlView = [[UIView alloc] init];
        controlView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
        [self.view addSubview:controlView];
        
        _camera = [[UIButton alloc] init];
        [_camera setBackgroundImage:[UIImage imageNamed:@"play_camera"] forState:UIControlStateNormal];
        [_camera addTarget:self action:@selector(picture) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_camera];
        
        _stop = [[UIButton alloc] init];
        [_stop setBackgroundImage:[UIImage imageNamed:@"play_stop"] forState:UIControlStateNormal];
        [_stop addTarget:self action:@selector(livestopClick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_stop];
        
        _play = [[UIButton alloc] init];
        _play.hidden = YES;
        [_play setBackgroundImage:[UIImage imageNamed:@"play_play"] forState:UIControlStateNormal];
        [_play addTarget:self action:@selector(livePlayClick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_play];

        _controlView= controlView;
        _controlView.alpha = 0.7f;
    }else{
        UIView * controlView = [[UIView alloc] init];
        controlView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
        [self.view addSubview:controlView];
        
        //_camera = [[UIButton alloc] init];
        //[_camera setBackgroundImage:[UIImage imageNamed:@"play_camera"] forState:UIControlStateNormal];
        //[_camera addTarget:self action:@selector(CameraClick) forControlEvents:UIControlEventTouchUpInside];
        //[controlView addSubview:_camera];
        
        _last = [[UIButton alloc] init];
        [_last setBackgroundImage:[UIImage imageNamed:@"play_previous"] forState:UIControlStateNormal];
        [_last addTarget:self action:@selector(ktclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_last];
        
        _play = [[UIButton alloc] init];
        _play.hidden = YES;
        [_play setBackgroundImage:[UIImage imageNamed:@"play_play"] forState:UIControlStateNormal];
        [_play addTarget:self action:@selector(setMoviePlay) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_play];
        
        _stop = [[UIButton alloc] init];
        [_stop setBackgroundImage:[UIImage imageNamed:@"play_stop"] forState:UIControlStateNormal];
        [_stop addTarget:self action:@selector(setMovieParse) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_stop];
        
        _next = [[UIButton alloc] init];
        [_next setBackgroundImage:[UIImage imageNamed:@"play_next"] forState:UIControlStateNormal];
        [_next addTarget:self action:@selector(kjclick) forControlEvents:UIControlEventTouchUpInside];
        [controlView addSubview:_next];
        
        //_delete = [[UIButton alloc] init];
        //[_delete setBackgroundImage:[UIImage imageNamed:@"play_delete"] forState:UIControlStateNormal];
        //[_delete addTarget:self action:@selector(delclick) forControlEvents:UIControlEventTouchUpInside];
        //[controlView addSubview:_delete];
        _controlView= controlView;
        _controlView.alpha = 0.8f;
    }
    
    if(_isLiveStream== NO)
    {
        CGFloat interval= (300-30*3)/ 4;
        //_camera.frame = CGRectMake(interval, 5, 30, 30);
        _last.frame = CGRectMake(interval, 5, 30, 30);
        _play.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
        _stop.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
        _next.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        //_delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
        
        _controlView.frame = UIInterfaceOrientationPortraitControlViewFrame;
    }
    else
    {
        if(_isLiveLandscapeEnter==YES)
            _controlView.frame = CGRectMake((mainScreenHeight- 400)/ 2, mainScreenWidth- 100, 400, 40 );
        else
            _controlView.frame = UIInterfaceOrientationPortraitControlViewFrame;
        
        if(_mode_flag==2){
            _camera.frame = CGRectMake((300-30)/2, 5, 30, 30);
            
            [_stop removeFromSuperview];
            [_play removeFromSuperview];
        }else{
            CGFloat interval= (300-30*2)/ 3;
            _stop.frame = CGRectMake(interval, 5, 30, 30);
            _play.frame = CGRectMake(interval, 5, 30, 30);
            _camera.frame = CGRectMake(interval* 2+ 30*1, 5, 30, 30);
            
            [_stop removeFromSuperview];
            [_play removeFromSuperview];
            [_controlView addSubview:_stop];
            [_controlView addSubview:_play];
        }
    }
}

- (void)setOrientationLandscapeFrame
{
    [self controlViewInit];
    [self processViewInit];
    
    if(!_isLiveLandscapeEnter){
        _playerView.frame = CGRectMake(0, 0, mainScreenWidth, mainScreenHeight);
        _topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
        _topView.hidden = NO;
        _controlView.frame = UIInterfaceOrientationLandscapeControlViewFrame;
    }

    if(_isLiveStream== NO)
    {
        CGFloat interval= (400-30*5)/ 6;
        _camera.frame = CGRectMake(interval, 5, 30, 30);
        _last.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
        _play.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _stop.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _next.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
        _delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
        
        _processView.frame = UIInterfaceOrientationLandscapeProccessViewFrame;
        _size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        _size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        _topProgressSlider.frame = CGRectMake(_size1.width, 0, _processView.frame.size.width- _size1.width- _size2.width, _processView.frame.size.height);
        _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
        
        _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
        _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
        
        _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
        
        self.sideMenuleft.hidden = YES;
    }
    else
    {
        if(_mode_flag==2){
            _camera.frame = CGRectMake((400-30)/2, 5, 30, 30);
            
            [_stop removeFromSuperview];
            [_play removeFromSuperview];
        }else{
            CGFloat interval= (400-30*2)/ 3;
            _stop.frame = CGRectMake(interval, 5, 30, 30);
            _play.frame = CGRectMake(interval, 5, 30, 30);
            _camera.frame = CGRectMake(interval* 2+ 30*1, 5, 30, 30);
            
            [_stop removeFromSuperview];
            [_play removeFromSuperview];
            [_controlView addSubview:_stop];
            [_controlView addSubview:_play];
        }
        
        //CGFloat interval= (400-30*2)/ 3;
        //_stop.frame = CGRectMake(interval, 5, 30, 30);
        //_play.frame = CGRectMake(interval, 5, 30, 30);
        //_camera.frame = CGRectMake(interval* 2+ 30*1, 5, 30, 30);
    }
}

- (void)setOrientationPortraitFrame
{
    [self controlViewInit];
    [self processViewInit];
    
    if([[UIApplication sharedApplication] statusBarOrientation]==UIInterfaceOrientationPortrait)
    {
        _topView.frame = UIInterfaceOrientationPortraitTopViewFrame;
        _topView.hidden = NO;
        _playerView.frame = UIInterfaceOrientationPortraitPlayViewFrame;
        _processView.frame = UIInterfaceOrientationPortraitProccessViewFrame;
        _controlView.frame = UIInterfaceOrientationPortraitControlViewFrame;
        [self.player setDrawable:_playerView];
        
        CGSize size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        _topProgressSlider.frame = CGRectMake(size1.width, 0, _processView.frame.size.width- size1.width- size2.width, _processView.frame.size.height);
        _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
        
        _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
        _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
        
        _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
        
        if(_isLiveStream== NO){
            CGFloat interval= (300-30*5)/ 6;
            _camera.frame = CGRectMake(interval, 5, 30, 30);
            _last.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
            _play.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
            _stop.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
            _next.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
            _delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
            self.sideMenuleft.hidden = YES;
        }else{
            if(_mode_flag==2){
                _camera.frame = CGRectMake((300-30)/2, 5, 30, 30);
                
                [_stop removeFromSuperview];
                [_play removeFromSuperview];
            }else{
                CGFloat interval= (300-30*2)/ 3;
                _stop.frame = CGRectMake(interval, 5, 30, 30);
                _play.frame = CGRectMake(interval, 5, 30, 30);
                _camera.frame = CGRectMake(interval* 2+ 30*1, 5, 30, 30);
                
                [_stop removeFromSuperview];
                [_play removeFromSuperview];
                [_controlView addSubview:_stop];
                [_controlView addSubview:_play];
            }
        }
    }
}

- (void)doRotatorFrame
{
//    if(_isLiveStream==YES){
//        [self setOrientationLandscapeFrame];
//    }
//    else
    {
        switch(_oriention)
        {
            case UIInterfaceOrientationLandscapeRight:
                [self setOrientationLandscapeFrame];
                break;
                
            case UIInterfaceOrientationLandscapeLeft:
                [self setOrientationLandscapeFrame];
                break;
                
            case UIInterfaceOrientationPortrait:
                [self setOrientationPortraitFrame];
                break;
                
            case UIInterfaceOrientationPortraitUpsideDown:
                break;
                
            default:
                break;
        }
    }
    
    [self sideMenuInit];
    [self hiddenAllView];
}

- (void)mediaPlayerStateChanged:(NSNotification *)aNotification
{
    switch (_player.state)
    {
        case VLCMediaPlayerStateOpening:
            break;
            
        case VLCMediaPlayerStatePaused:
            NSLog(@"VLCMediaPlayerStatePaused\n");
            break;
        case VLCMediaPlayerStateStopped:
            NSLog(@"VLCMediaPlayerStateStopped\n");
            break;
        case VLCMediaPlayerStateBuffering:
//            NSLog(@"VLCMediaPlayerStateBuffering\n");
            break;
        case VLCMediaPlayerStatePlaying:
            NSLog(@"VLCMediaPlayerStatePlaying\n");
            break;
        case VLCMediaPlayerStateEnded:
            NSLog(@"VLCMediaPlayerStateEnded\n");
            if(_topProgressSlider!=nil)
                _topProgressSlider.value = 1.0;
            
            if(_isLiveStream==NO){
                [self initNormalPlayer:_url];
                [self.player setDrawable:_playerView];
                _play.hidden = NO;
                _stop.hidden = YES;
                [_player play];
                while([_player isPlaying]==NO);
                [_player pause];
            }
            break;
        case VLCMediaPlayerStateError:
            NSLog(@"VLCMediaPlayerStateError\n");
            break;
            
        default:
            NSLog(@"VLCMediaPlayerStateBuffering default\n");
            break;
    }
}

- (void)gpsUpdata:(NSNotification *) notification
{
    NSString * obj = [notification object];
    
    NSLog(@"%@\n",obj);
    
    static struct gps_info g_gps_info;
    char * data= [obj UTF8String];
    int search_type = 0;
    
    if(self.car==nil){
        self.car = [[MAPointAnnotation alloc] init];
        self.car.title = @"Car";
        
        [self initShapeLayer];
        [self.map.layer insertSublayer:self.shapeLayer atIndex:1];
        [self.map addAnnotation:self.car];
        [self.map setVisibleMapRect:self.polyline.boundingMapRect edgePadding:UIEdgeInsetsMake(30, 30, 30, 30) animated:NO];
    }
    
    if (_route_coords == NULL){
        _route_coords = malloc(60 * 5 * sizeof(CLLocationCoordinate2D));
        _malloc_count= 60* 5;
    }else if(_count >= _malloc_count){
        if(_route_coords!=NULL){
            free(_route_coords), _route_coords = NULL;
        }
        
        _malloc_count+= 60* 5;
        _route_coords = malloc(_malloc_count);
    }
    
    if (!strncmp(data, GPS_XXRMC, strlen(GPS_XXRMC))) {
        search_type = GPS_RMC;
    }
    else if (!strncmp(data, GPS_XXGGA, strlen(GPS_XXGGA))) {
        search_type = GPS_GGA;
    }
    else if (!strncmp(data, "$GPSENDTIME:", strlen("$GPSENDTIME:"))) {
        memset(&g_gps_info, 0, sizeof(g_gps_info));
        return;
    }
    else {
        memset(&g_gps_info, 0, sizeof(g_gps_info));
        return;
    }
    
    gps_nmea_data_parse(&g_gps_info, data, search_type, strlen(data));
    
    double wgsLat = gps_degree_minute_to_degree(g_gps_info.latitude);
    double wgsLng = gps_degree_minute_to_degree(g_gps_info.longitude);
    double gcjLat , gcjLng;
    
    wgs2gcj(wgsLat, wgsLng, &gcjLat, &gcjLng);
    
    if(wgsLat==0&&wgsLng==0){
        return;
    }
    
    _route_coords[_count++]= CLLocationCoordinate2DMake(gcjLat ,gcjLng);
    
    self.polyline = [MAPolyline polylineWithCoordinates:_route_coords count:_count];

    CGPoint *points = [self pointsForCoordinates:_route_coords count:_count];
    CGPathRef path = [self pathForPoints:points count:_count];
    self.shapeLayer.path = path;
    
    _annotationView = [self.map viewForAnnotation:_car];
    if(_annotationView!=nil){
        CAAnimation * annottionAnimation = [self constructAnnotationAnimationWithPath:path];
        [_annotationView.layer addAnimation:annottionAnimation forKey:@"annotation"];
        [_annotationView.annotation setCoordinate:_route_coords[_count-1]];
        
        /* ShapeLayer animation. */
        CAAnimation *shapeLayerAnimation = [self constructShapeLayerAnimation];
        shapeLayerAnimation.delegate = self;
        [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"shape"];
    }
    
    [self.map addOverlay:self.polyline];
    [self.shapeLayer removeFromSuperlayer];
    [self makeMapViewEnable:YES];
    
    free(points),           points  = NULL;
    CGPathRelease(path),    path    = NULL;

//    self.car.coordinate = _coords[i+_route_start];
//    [_annotationView.annotation setCoordinate:_coords[i+_route_start]];
    
    if(_count==1){
        [_map setZoomLevel:15 animated:NO];
        [_map setCenterCoordinate:_route_coords[0] animated:NO];
    }
    
    if(_setCenterCoordinate==YES){
        [_map setCenterCoordinate:_route_coords[_count- 1] animated:NO];
    }
}

-(void)downloadgps:(GPSObject *)obj
{
    /* Step 1. */
    //show car
    if(self.car==nil){
        self.car = [[MAPointAnnotation alloc] init];
        self.car.title = @"Car";
    
        [self initShapeLayer];
        [self.map.layer insertSublayer:self.shapeLayer atIndex:1];
        [self.map addAnnotation:self.car];
    }
    
    [self initRoute];
    
    [self.map setVisibleMapRect:self.polyline.boundingMapRect edgePadding:UIEdgeInsetsMake(30, 30, 30, 30) animated:NO];
    CGPoint *points = [self pointsForCoordinates:_route_coords count:_route_count];
    CGPathRef path = [self pathForPoints:points count:_count];
    self.shapeLayer.path = path;
    
    _annotationView = [self.map viewForAnnotation:_car];
    if(_annotationView!=nil){
        CAAnimation * annottionAnimation = [self constructAnnotationAnimationWithPath:path];
        [_annotationView.layer addAnimation:annottionAnimation forKey:@"annotation"];
        [_annotationView.annotation setCoordinate:_route_coords[0]];
        
        /* ShapeLayer animation. */
        CAAnimation *shapeLayerAnimation = [self constructShapeLayerAnimation];
        shapeLayerAnimation.delegate = self;
        [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"shape"];
    }
    
    [self.map addOverlay:self.polyline];
    [self.shapeLayer removeFromSuperlayer];
    [self makeMapViewEnable:YES];
    
    free(points),           points  = NULL;
    CGPathRelease(path),    path    = NULL;
}

- (void)GetGpsList:(NSNotification *) notification
{
    GPSObject * obj = [notification object];
    
    [self makeMapViewEnable:NO];

    self.gpsArray = [obj.list copy];
    self.gps = obj.gps;
#if 1
    _thread = [[NSThread alloc] initWithTarget:self selector:@selector(downloadgps:) object:obj];
    [_thread start];
#else
    /* Step 1. */
    //show car
    self.car = [[MAPointAnnotation alloc] init];
    self.car.title = @"Car";
    
    [self initRoute];

    [self.map setVisibleMapRect:self.polyline.boundingMapRect edgePadding:UIEdgeInsetsMake(30, 30, 30, 30) animated:NO];
    CGPoint *points = [self pointsForCoordinates:_route_coords count:_route_count];
    CGPathRef path = [self pathForPoints:points count:_count];
    self.shapeLayer.path = path;

    [self.map.layer insertSublayer:self.shapeLayer atIndex:1];
    [self.map addAnnotation:self.car];

    _annotationView = [self.map viewForAnnotation:_car];
    if(_annotationView!=nil){
        CAAnimation * annottionAnimation = [self constructAnnotationAnimationWithPath:path];
        [_annotationView.layer addAnimation:annottionAnimation forKey:@"annotation"];
        [_annotationView.annotation setCoordinate:_route_coords[0]];
        
        /* ShapeLayer animation. */
        CAAnimation *shapeLayerAnimation = [self constructShapeLayerAnimation];
        shapeLayerAnimation.delegate = self;
        [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"shape"];
    }
    
    [self.map addOverlay:self.polyline];
    [self.shapeLayer removeFromSuperlayer];
    
//    [self showRouteForCoords:_route_coords count:_route_start];
    [self makeMapViewEnable:YES];
    
    free(points),           points  = NULL;
    CGPathRelease(path),    path    = NULL;
#endif
}

- (void)getRecordingState:(NSNotification *) notification
{
    NSString * aStr = [notification object];
    NSLog(@"%@\n",aStr);
    if([aStr isEqualToString:@"idle"]==YES ){
        _play.hidden = NO;
        _stop.hidden = YES;
    }else if([aStr isEqualToString:@"busy"]==YES ){
        _play.hidden = YES;
        _stop.hidden = NO;
    }
    
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_VIDEO_LIST"];
}


- (void)precord
{
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_Control_Recording:off"];
    usleep(100000);
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_Control_Recording"];
}
- (void)srecord
{
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_Control_Recording:on"];
    usleep(100000);
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_Control_Recording"];
}

- (void)livePlayClick
{
    if(_mode_flag< 2){
        [self srecord];
    }
}

- (void)livestopClick
{
    if(_mode_flag< 2){
        [self precord];
    }
}

- (void)picture
{
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_Control_Photograph"];
}

- (void)delclick
{
    NSString * Title;
    NSString * action1;
    NSString * action2;
    if(_language==1){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
    }else {
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        [_player pause];
                          
        NTFileObj *fileinfo = _filelist[_index];
        NSString * path = [[NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"video"] stringByAppendingPathComponent:[fileinfo.name lastPathComponent]];
                          
        [_filemanager removeItemAtPath:path error:nil];
                          
        NSMutableArray * delNames = [[NSMutableArray alloc] init];
        [delNames addObject:_filelist[_index]];
        [[WiFiConnection sharedSingleton] deleteFile:delNames];
        [_filelist removeObjectAtIndex:_index];
        if(_filelist.count==0){
            self.navigationController.navigationBar.hidden = NO;
            self.tabBarController.tabBar.hidden = NO;
            
            //NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
            //[[UIDevice currentDevice] setValue:value forKey:@"orientation"];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        if(_index >= [_filelist count]){
            _index = 0;
        }
                          
        for(NTFileObj * i in _filelist)
        {
            NSLog(@"%@\n",i.name);
        }
        
        _totalMovieDuration = 0;
        [self playWithIndex:_index];
        [self.player setDrawable:_playerView];
        [self setMoviePlay];
        //        _titlelabel.text = fileinfo.name;
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
    }]];
    
}

- (void)savePreview
{
    NSString * pic_path = [[NSString alloc] init];
    
    if( self.isLiveStream == NO ){
        return;
    }
    else{
        pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@.jpg", NSHomeDirectory(),@"preview",[WiFiConnection sharedSingleton].NTConnectionSSID];
    }
    [self doCamera:pic_path];
}

-(void)doCamera:(NSString *)pic_path
{
    double time = [_player.time intValue]/1000.0;
    MPMoviePlayerController * mp = [[MPMoviePlayerController alloc] initWithContentURL:_url];
    UIImage * thumbImage = [mp thumbnailImageAtTime:time timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    [UIImageJPEGRepresentation(thumbImage, 1.0) writeToFile:pic_path atomically:YES];
    
    _hud.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        // 隐藏蒙版
        _hud.hidden = YES;
    });
}

-(void)CameraClick
{
    if(_mode_flag==2){
        [self picture];
    }else{
        NSString * pic_path = [[NSString alloc] init];
    
        NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *currentTime = [formatter stringFromDate:[NSDate date]];
    
        NSString * path= [_url absoluteString];
    
        pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@_%@.jpg", NSHomeDirectory(),@"picture",[[path lastPathComponent]stringByDeletingPathExtension],currentTime];
        [self doCamera:pic_path];
    
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            _language = 1;
            _hud.text = @"截图成功";
        }else{
            _language = 0;
            _hud.text = @"Screenshot Success";
        }
        _hud.frame = CGRectZero;
        [_hud sizeToFit];
        _hud.center = self.view.center;
    }
}

- (void)kjclick
{
    _index++;
    if(_index >= [_filelist count]){
        _index = [_filelist count]-1;
        return;
    }

    if(_thread!=nil){
        [_thread cancel];
    }

    _totalMovieDuration = 0;
    [self playWithIndex:_index];
    [self.player setDrawable:_playerView];
    [self setMoviePlay];
    
    [[WiFiConnection sharedSingleton] GetGpsContents:_playvideo];
    
    _next.enabled = NO;
    _last.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        _next.enabled = YES;
        _last.enabled = YES;
    });
    
//    [self.map removeAnnotation:self.car];
    [self getGpsStartTime];
}

- (void)ktclick
{
    if(_index==0){
        return;
    }

    if(_thread!=nil){
        [_thread cancel];
    }
    
    _index--;
    _totalMovieDuration = 0;
    [self playWithIndex:_index];
    [self.player setDrawable:_playerView];
    [self setMoviePlay];
    [[WiFiConnection sharedSingleton] GetGpsContents:_playvideo];
    
    _next.enabled = NO;
    _last.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        _next.enabled = YES;
        _last.enabled = YES;
    });

//    [self.map removeAnnotation:self.car];
    [self getGpsStartTime];
}

- (void)EnterBackground
{
    if(_isLiveStream==YES)return;
    
    if(_player==nil)return;
    [_player pause];
    
    _play.hidden = NO;
    _stop.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
        //记录开始点击的位置
        _startPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
        //单指移动
        CGPoint tempPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
        
        CGFloat moveX = tempPoint.x - _startPoint.x;
        CGFloat moveY = tempPoint.y - _startPoint.y;
#if 0
        if(fabs(moveX) > fabs(moveY))
        {
            
        }
        else
        {
            if(_startPoint.x > [UIScreen mainScreen].bounds.size.height/2)
            {
                float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
                float newVolume = volume;
                
                if(fabs(moveY) > 5.0){
                    newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    
                    if (newVolume < 0) {
                        newVolume = 0;
                    } else if (newVolume > 1.0) {
                        newVolume = 1.0;
                    }
                    
                    [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
                }
            }else{
                float brightness = [[UIScreen mainScreen] brightness];
                float newbrightness = brightness;
                
                if(fabs(moveY) > 5.0){
                    newbrightness -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    if (newbrightness < 0) {
                        newbrightness = 0;
                    } else if (newbrightness > 1.0) {
                        newbrightness = 1.0;
                    }
                    
                    [[UIScreen mainScreen] setBrightness:newbrightness];
                }
            }
        }
#else
        float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
        float newVolume = volume;
        
        if(fabs(moveY) > 5.0){
            newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
            
            if (newVolume < 0) {
                newVolume = 0;
            } else if (newVolume > 1.0) {
                newVolume = 1.0;
            }
            [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
        }
#endif
    }
}

- (void)playWithIndex:(NSUInteger)index
{
    _index= index;
    NTFileObj *fileinfo = _filelist[index];
    NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@",[WiFiConnection sharedSingleton].NTConnectionHost,fileinfo.path,fileinfo.name];
    NSURL *url = [NSURL URLWithString:path];
    [self initNormalPlayer:url];
    
    _playvideo= fileinfo.name;
    _gpspath= fileinfo.gps;
}

- (void)initNormalPlayer:(NSURL *)url
{
    if(_player!=nil){
        [_player stop];
    }
    
    VLCMediaPlayer *player;
    player= [[VLCMediaPlayer alloc] initWithOptions:nil];
    self.player = player;
    
    self.player.media = [VLCMedia mediaWithURL:url];
    _url = url;
}

- (void)initLivePlayer:(NSURL *)url
{
    char buf[100];
    _isLiveStream = YES;
    if(_player!=nil){
        [_player stop];
    }

    NSArray* options = @[@"--rtsp-tcp", @"--clock-synchro=1", @"--network-caching=180", @"--clock-jitter=-2147483647", @"-vvvv"];
    //NSArray* options = @[@"--clock-synchro=1", @"--network-caching=180", @"--clock-jitter=-2147483647", @"-vvvv"];
    VLCMediaPlayer *player;
    
    player= [[VLCMediaPlayer alloc] initWithOptions:options];
    
    self.player = player;
    
    self.player.media = [VLCMedia mediaWithURL:url];
    sprintf(buf,"%d:%d",(int)[UIScreen mainScreen].bounds.size.height,(int)[UIScreen mainScreen].bounds.size.width);
    [self.player setVideoAspectRatio:buf];
    
    _url = url;
}

- (void)psclick
{
    if ([_player isPlaying]){
        [self setMovieParse];
    } else {
        [self setMoviePlay];
    }
}

#pragma mark - 暂停
- (void)setMovieParse {
    if(_player!=nil){
        [_player pause];
    }
    _play.hidden = NO;
    _stop.hidden = YES;
    
//    if(_thread!=NULL){
//        [_thread cancel];
//    }
//    [self.map removeAnnotation:self.car];
//    [self getGpsStartTime];
}
#pragma mark - 播放
- (void)setMoviePlay {
    if(_player!=nil){
        [_player play];
    }
    _play.hidden = YES;
    _stop.hidden = NO;
    
//    if(_thread!=NULL){
//        [_thread cancel];
//    }
//    [self.map removeAnnotation:self.car];
//    [self getGpsStartTime];
}

#pragma mark -  添加进度观察 - addProgressObserver
- (void)addProgressObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaPlayerTimeChanged:) name:VLCMediaPlayerTimeChanged object:nil];
}

- (void)actionTapGesture:(UITapGestureRecognizer *)sender
{
    if(_thread !=nil){
        [_thread cancel];
    }
    
    CGPoint touchPoint = [sender locationInView:_topProgressSlider];
    CGFloat value = (_topProgressSlider.maximumValue - _topProgressSlider.minimumValue) * (touchPoint.x / _topProgressSlider.frame.size.width);
    [_topProgressSlider setValue:value animated:YES];
    
    int totalMovieDuration = _totalMovieDuration * _topProgressSlider.value;
    
    [_player setTime:[[VLCTime alloc] initWithInt:totalMovieDuration]];
}
-(void)sliderValueChanged:(id)sender
{
    int i;
    UISlider *senderSlider = sender;
    int totalMovieDuration = _totalMovieDuration * senderSlider.value;
    VLCTime * dragedCMTime = [[VLCTime alloc] initWithInt:totalMovieDuration];
    [_player setTime:dragedCMTime];
    
    if(_route_start==0)
        return;
    
    i= (int)(_topProgressSlider.value*60);
    if(_coords[i+_route_start].latitude== -1 && _coords[i+_route_start].longitude== -1)
        return;
    
    [_annotationView.annotation setCoordinate:_coords[i+_route_start]];
}

//  播放页面添加轻拍手势
- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAllSubViews:)];
    tap.delegate = self;
    [_playerView addGestureRecognizer:tap];
}

- (void)hiddenAllView
{
    self.isHiddenAllView = YES;
    
    [_processView setHidden:YES];
    [_topView setHidden:YES];
    [_controlView setHidden:YES];
    
    if (self.sideMenuleft.isOpen==YES)
        [self.sideMenuleft close];
    if (self.sideMenu.isOpen==YES)
        [self.sideMenu close];
}

- (void)DisplayAllView
{
    self.isHiddenAllView = NO;
    _processView.hidden = NO;
    _topView.hidden = NO;
    _controlView.hidden = NO;

    if (self.sideMenuleft.isOpen==NO)
        [self.sideMenuleft open];
    if (self.sideMenu.isOpen==NO)
        [self.sideMenu open];
}

- (void)dismissAllSubViews:(UITapGestureRecognizer *)tap
{
    if (!self.isHiddenAllView)
    {
        [self hiddenAllView];
    }
    else
    {
        [self DisplayAllView];
    }
    
    if(_isLiveStream==NO){
        self.sideMenuleft.hidden = YES;

    }
}

#pragma mark - 观察者 观察播放完毕
- (void)addNotificationCenters
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaPlayerStateChanged:) name:VLCMediaPlayerStateChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EnterBackground) name:@"EnterBackground" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gpsUpdata:) name:@"gpsUpdata" object:nil];
}

- (void)mediaPlayerTimeChanged:(NSNotification *)aNotification
{
    int i= 0;
    NSString * currentTime = [_player.time stringValue];
    NSString * remainingTime = [_player.remainingTime stringValue];
    
    _topPastTimeLabel.text = currentTime;
    _totalTimeLabel.text = remainingTime;
    
    if(_totalMovieDuration==0)
    {
        _totalMovieDuration = [_player.remainingTime intValue]*-1 + [_player.time intValue];
    }
    
    _topProgressSlider.value =  [_player.time intValue]/1.0 / _totalMovieDuration;

    if(_route_start<=0)
        return;
    
    i= _topProgressSlider.value*60;
    if(_coords[i+_route_start].latitude== -1 && _coords[i+_route_start].longitude== -1){
        return;
    }
    
    self.car.coordinate = _coords[i+_route_start];
    [_annotationView.annotation setCoordinate:_coords[i+_route_start]];
    if(_setCenterCoordinate==YES){
        [_map setCenterCoordinate:_coords[i+_route_start] animated:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 禁用 iOS7 返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)backclick:(UIButton *)btn
{
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if(_player!=nil){
        [_player stop];
        _player = nil;
    }
    
    if(_isLiveStream==YES){
//        [self savePreview];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_LIVE_STOP"];
//        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_RTP_TS_TRANS_STOP"];
    }
    
    if(_thread!=nil){
        [_thread cancel];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_filelist removeAllObjects];
}

- (void)statusBarOrientationChange:(NSNotification *)notification
{
    _oriention = [UIApplication sharedApplication].statusBarOrientation;
    
    if(_isLiveLandscapeEnter==YES){
        self.view.transform = CGAffineTransformMakeRotation(M_PI*2);
        _isLiveLandscapeEnter = NO;
    }
    
    [self doRotatorFrame];
}

- (void)dealloc
{
    if (_coords != NULL)
    {
        free(_coords), _coords = NULL;

    }
    if(_route_coords!=NULL){
        free(_route_coords), _route_coords = NULL;
    }
}

#if 1
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if([_form isEqualToString:@"CVR_GPS"]==YES)
        return UIInterfaceOrientationMaskAll;
    else
        return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#endif
@end
