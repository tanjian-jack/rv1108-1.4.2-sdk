//
//  LocalAVPlayerViewController.m
//  CVR
//
//  Created by 雷起斌 on 5/26/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "LocalAVPlayerViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import "MBProgressHUD+XMG.h"
#import <MAMapKit/MAMapKit.h>
#import "nmea_parse.h"
#import "transform.h"

#define RealTimePreviewRPWidth   384
#define RealTimePreviewRPHeight  640

#define mainScreenWidth     [UIScreen mainScreen].bounds.size.width
#define mainScreenHeight    [UIScreen mainScreen].bounds.size.height

#define UIInterfaceOrientationPortraitTopViewFrame  CGRectMake(0, 0, mainScreenWidth, 40)
#define UIInterfaceOrientationLandscapeTopViewFrame CGRectMake(0, 0, mainScreenWidth, 40)
#define UIInterfaceOrientationLandscapeTopViewColor [UIColor blackColor]

#define UIInterfaceOrientationPortraitPlayViewFrame CGRectMake(0, 0, mainScreenWidth, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight)
#define UIInterfaceOrientationLandscapePlayViewFrame CGRectMake(0, 0, mainScreenWidth, mainScreenHeight)

#define UIInterfaceOrientationPortraitProccessViewFrame CGRectMake(0, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight, [UIScreen mainScreen].bounds.size.width, 30)
#define UIInterfaceOrientationLandscapeProccessViewFrame CGRectMake(0, mainScreenHeight-40, mainScreenWidth, 40)

#define UIInterfaceOrientationPortraitControlViewFrame CGRectMake((mainScreenWidth- 300)/ 2, mainScreenWidth*RealTimePreviewRPWidth/RealTimePreviewRPHeight- 40- 10, 300, 40)
#define UIInterfaceOrientationLandscapeControlViewFrame CGRectMake((mainScreenWidth- 400)/ 2, mainScreenHeight- 100, 400, 40 )

@interface LocalAVPlayerViewController ()<UIGestureRecognizerDelegate, MAMapViewDelegate, MAMapViewDelegate, CAAnimationDelegate>
{
    float progressSlider;
    BOOL isPlay;
    CGPoint _startPoint;
}
@property (nonatomic,strong) AVPlayer *player;//播放器对象
@property (nonatomic,strong) AVPlayerLayer *playerLayer;
@property (nonatomic,strong) AVPlayerItem *playItem;

@property (strong, nonatomic) UIView *processView;
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIView *controlView;

@property (strong, nonatomic) UIButton * backButton;

@property (strong, nonatomic) UIButton * camera;
@property (strong, nonatomic) UIButton * delete;
@property (strong, nonatomic) UIButton * prevMv;
@property (strong, nonatomic) UIButton * nextMv;
@property (strong, nonatomic) UIButton * playSt;
@property (strong, nonatomic) UIButton * stopSt;
@property (strong, nonatomic) UILabel * titlelabel;

@property (strong, nonatomic) UISlider *topProgressSlider;
@property (strong, nonatomic) UILabel *topPastTimeLabel;
@property (strong, nonatomic) UILabel *totalTimeLabel;
@property (strong, nonatomic) UITapGestureRecognizer  *tapGesture;

@property (strong, nonatomic) UILabel *hud;

@property (nonatomic, assign) BOOL setCenterCoordinate;
@property (nonatomic, assign) BOOL isFirstRotatorTap;//  横屏下第一次点击
@property (nonatomic, assign) int hiddenSec;//  横屏下第一次点击

@property (nonatomic, assign) CGFloat totalMovieDuration;//  保存该视频资源的总时长，快进或快退的时候要用

@property (nonatomic, strong) NSFileManager * filemanager;
@property (nonatomic, assign) UIInterfaceOrientation orientation;
@property (assign, nonatomic) NSInteger language;

@property (nonatomic, strong) MAMapView * map;
@property (nonatomic, strong) MAAnnotationView * annotationView;
@property (nonatomic, strong) MAPointAnnotation * car;
@property (nonatomic, assign) CLLocationCoordinate2D * coords;
@property (nonatomic, assign) CLLocationCoordinate2D * route_coords;
@property (strong, nonatomic) UISwitch * fllow;
@property (nonatomic, assign) CGSize size1;
@property (nonatomic, assign) CGSize size2;
@property (assign, nonatomic) UIInterfaceOrientation oriention;
@property (nonatomic, strong) NSString * video_gps_dir;
@property (nonatomic, strong) NSArray * video_gps_file_array;

@property (nonatomic, assign) NSUInteger count;
@property (nonatomic, assign) NSUInteger route_count;
@property (nonatomic, assign) NSUInteger route_start;
@property (nonatomic, strong) MAPolyline *polyline;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation LocalAVPlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    
    self.filemanager= [NSFileManager defaultManager];

    isPlay = YES;

    [self PlayViewInit];
    [self processViewInit];
    [self controlViewInit];
    [self topViewInit];
    [self hudViewInit];
 
    [self addGestureRecognizer];
    [self addNotificationCenters];
    [self addProgressObserver];
    
    if(_player!=nil){
        [_player play];
    }
    
    self.car = [[MAPointAnnotation alloc] init];
    self.car.title = @"Car";
    
    [self initRoute];

    [self.map setVisibleMapRect:self.polyline.boundingMapRect edgePadding:UIEdgeInsetsMake(30, 30, 30, 30) animated:NO];
    CGPoint *points = [self pointsForCoordinates:_route_coords count:_route_count];
    CGPathRef path = [self pathForPoints:points count:_count];
    self.shapeLayer.path = path;
    
    [self.map.layer insertSublayer:self.shapeLayer atIndex:1];
    [self.map addAnnotation:self.car];
    
    _annotationView = [self.map viewForAnnotation:_car];
    if(_annotationView!=nil){
        CAAnimation * annottionAnimation = [self constructAnnotationAnimationWithPath:path];
        [_annotationView.layer addAnimation:annottionAnimation forKey:@"annotation"];
        [_annotationView.annotation setCoordinate:_route_coords[0]];
        
        /* ShapeLayer animation. */
        CAAnimation *shapeLayerAnimation = [self constructShapeLayerAnimation];
        shapeLayerAnimation.delegate = self;
        [self.shapeLayer addAnimation:shapeLayerAnimation forKey:@"shape"];
    }
    
    [self.map addOverlay:self.polyline];
    [self.shapeLayer removeFromSuperlayer];
    [self makeMapViewEnable:YES];

    free(points),           points  = NULL;
    CGPathRelease(path),    path    = NULL;
    
    NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)initRoute
{
    struct gps_info info;
    const char * char_path;
    
    _count = 0;
    _route_count= 0;
    _route_start= 0;
    
    NSLog(@"_video_gps_file_array %@\n",_video_gps_file_array);
    
    if(_video_gps_file_array==nil)
        return;
    
    if (_coords != NULL)
    {
        free(_coords), _coords = NULL;
    }
    _coords = malloc(60 * 5 * sizeof(CLLocationCoordinate2D)* _video_gps_file_array.count);

    if(_route_coords!=NULL){
        free(_route_coords), _route_coords = NULL;
    }
    _route_coords = malloc(60 * 5 * sizeof(CLLocationCoordinate2D)* _video_gps_file_array.count);

    _filemanager = [NSFileManager defaultManager];
    NSString * gps_path;
    for( NSString * gps in _video_gps_file_array ){
        gps_path = [NSString stringWithFormat: @"%@/Documents/gps/%@/%@", NSHomeDirectory(), _video_gps_dir, gps];
        if([_filemanager fileExistsAtPath:gps_path]!=YES){
            NSLog(@"%@\n",gps_path);
            return;
//            NSString * path = [NSString stringWithFormat: @"http://%@/%@/%@", [WiFiConnection sharedSingleton].NTConnectionHost, _gpspath, gps];
//            NSURL * url = [NSURL URLWithString:path];
//            NSData *data = [NSData dataWithContentsOfURL:url];
//            [data writeToFile:gps_path atomically:NO];
        }
        
        char_path = [gps_path UTF8String];
        gps_set_data_from_fp((char *)char_path);
        
        while(gps_get_data_from_fp(&info)==1){
            double wgsLat = gps_degree_minute_to_degree(info.latitude);
            double wgsLng = gps_degree_minute_to_degree(info.longitude);
            double gcjLat , gcjLng;
            
            wgs2gcj(wgsLat, wgsLng, &gcjLat, &gcjLng);
            
            if(wgsLat==0&&wgsLng==0){
                _coords[_count++]= CLLocationCoordinate2DMake(-1 ,-1);
                continue;
            }
            
            _coords[_count++]= CLLocationCoordinate2DMake(gcjLat ,gcjLng);
            _route_coords[_route_count++]= CLLocationCoordinate2DMake(gcjLat ,gcjLng);
        }
        
        NSLog(@"%@ %zi\n",gps,_count);
        gps_close_data_from_fp();
    }
    self.polyline = [MAPolyline polylineWithCoordinates:_route_coords count:_route_count];
    [self initShapeLayer];
    
    [self getGpsStartTime];

}

- (void)animationDidStart:(CAAnimation *)anim
{
    [self makeMapViewEnable:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag)
    {
        [self.map addOverlay:self.polyline];
        [self.shapeLayer removeFromSuperlayer];
    }
    
    [self makeMapViewEnable:YES];
}

- (MAOverlayView *)mapView:(MAMapView *)mapView viewForOverlay:(id <MAOverlay>)overlay
{
    MAPolylineView *polylineView = [[MAPolylineView alloc] initWithPolyline:overlay];
    
    if(polylineView.polyline == _polyline){
        polylineView.lineWidth   = 4.f;
        polylineView.strokeColor = [UIColor redColor];
    }else{
        polylineView.lineWidth   = 4.f;
        polylineView.strokeColor = [UIColor blueColor];
    }
    
    return polylineView;
}

- (void)showRouteForCoords:(CLLocationCoordinate2D *)coords count:(NSUInteger)count
{
    MAPolyline *route = [MAPolyline polylineWithCoordinates:coords count:count];
    [self.map addOverlay:route];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    /* Step 2. */
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        if ([annotation.title isEqualToString:@"Car"])
        {
            static NSString *pointReuseIndetifier = @"trackingReuseIndetifier";
            MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndetifier];
            if (annotationView == nil){
                annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndetifier];
            }
            annotationView.canShowCallout = NO;
            
            annotationView.image = [UIImage imageNamed:@"ball"];
            //annotationView.image =  [UIImage imageNamed:@"userPosition"];
            return annotationView;
        }
    }
    
    return nil;
}

/* 构建shapeLayer. */
- (void)initShapeLayer
{
    self.shapeLayer = [[CAShapeLayer alloc] init];
    self.shapeLayer.lineWidth         = 4;
    self.shapeLayer.strokeColor       = [UIColor redColor].CGColor;
    self.shapeLayer.fillColor         = [UIColor clearColor].CGColor;
    self.shapeLayer.lineJoin          = kCALineCapRound;
}

/* Enable/Disable mapView. */
- (void)makeMapViewEnable:(BOOL)enabled
{
    self.map.scrollEnabled          = enabled;
    self.map.zoomEnabled            = enabled;
}

/* 构建shapeLayer的basicAnimation. */
- (CAAnimation *)constructShapeLayerAnimation
{
    CABasicAnimation *theStrokeAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    theStrokeAnimation.duration         = 2.f;
    theStrokeAnimation.fromValue        = @0.f;
    theStrokeAnimation.toValue          = @1.f;
    
    return theStrokeAnimation;
}

/* 构建annotationView的keyFrameAnimation. */
- (CAAnimation *)constructAnnotationAnimationWithPath:(CGPathRef)path
{
    if (path == NULL)
    {
        return nil;
    }
    
    CAKeyframeAnimation *thekeyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    thekeyFrameAnimation.duration        = 2.f;
    thekeyFrameAnimation.path            = path;
    thekeyFrameAnimation.calculationMode = kCAAnimationPaced;
    
    return thekeyFrameAnimation;
}

/* 构建path, 调用着负责释放内存. */
- (CGMutablePathRef)pathForPoints:(CGPoint *)points count:(NSUInteger)count
{
    if (points == NULL || count <= 1)
    {
        return NULL;
    }
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathAddLines(path, NULL, points, count);
    
    return path;
}

/* 经纬度转屏幕坐标, 调用着负责释放内存. */
- (CGPoint *)pointsForCoordinates:(CLLocationCoordinate2D *)coordinates count:(NSUInteger)count
{
    if (coordinates == NULL || count <= 1)
    {
        return NULL;
    }
    
    /* 申请屏幕坐标存储空间. */
    CGPoint *points = (CGPoint *)malloc(count * sizeof(CGPoint));
    
    /* 经纬度转换为屏幕坐标. */
    for (int i = 0; i < count; i++)
    {
        points[i] = [self.map convertCoordinate:_route_coords[i] toPointToView:self.map];
    }
    
    return points;
}

- (void)doRotatorFrame
{
    switch(_oriention)
    {
        case UIInterfaceOrientationLandscapeRight:
            [self setOrientationLandscapeFrame];
            break;
                
        case UIInterfaceOrientationLandscapeLeft:
            [self setOrientationLandscapeFrame];
            break;
                
        case UIInterfaceOrientationPortrait:
            [self setOrientationPortraitFrame];
            break;
                
        case UIInterfaceOrientationPortraitUpsideDown:
            break;
                
        default:
            break;
    }
}

- (void)setOrientationLandscapeFrame
{
    [self controlViewInit];
    [self processViewInit];
    
    _map.hidden = YES;
    _playerLayer.frame = UIInterfaceOrientationLandscapePlayViewFrame;
    _topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
    _controlView.frame = UIInterfaceOrientationLandscapeControlViewFrame;
    
    CGFloat interval= (400-30*5)/ 6;
    _camera.frame = CGRectMake(interval, 5, 30, 30);
    _prevMv.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
    _playSt.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
    _stopSt.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
    _nextMv.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
    //_delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
        
    _processView.frame = UIInterfaceOrientationLandscapeProccessViewFrame;
    _size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _topProgressSlider.frame = CGRectMake(_size1.width, 0, _processView.frame.size.width- _size1.width- _size2.width, _processView.frame.size.height);
    _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
        
    _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
    _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
        
    _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
}

- (void)setOrientationPortraitFrame
{
    [self controlViewInit];
    [self processViewInit];
    
    if([[UIApplication sharedApplication] statusBarOrientation]==UIInterfaceOrientationPortrait)
    {
        _map.hidden = NO;
        _topView.frame = UIInterfaceOrientationPortraitTopViewFrame;
        _playerLayer.frame = UIInterfaceOrientationPortraitPlayViewFrame;
        _processView.frame = UIInterfaceOrientationPortraitProccessViewFrame;
        _controlView.frame = UIInterfaceOrientationPortraitControlViewFrame;
        
        CGSize size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        _topProgressSlider.frame = CGRectMake(size1.width, 0, _processView.frame.size.width- size1.width- size2.width, _processView.frame.size.height);
        _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
        
        _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
        _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
        
        _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
        
        CGFloat interval= (300-30*5)/ 6;
        _camera.frame = CGRectMake(interval, 5, 30, 30);
        _prevMv.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
        _playSt.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _stopSt.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
        _nextMv.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
        //_delete.frame = CGRectMake(interval* 5+ 30*4, 5, 30, 30);
    }
}

- (void)statusBarOrientationChange:(NSNotification *)notification
{
    _oriention = [UIApplication sharedApplication].statusBarOrientation;
    [self doRotatorFrame];
}

-(void)fllowclick:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    _setCenterCoordinate = isButtonOn;
}

- (void)hudViewInit
{
    UILabel *labelOne = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelOne.backgroundColor = [UIColor clearColor];
    labelOne.textColor = [UIColor whiteColor];
    labelOne.font = [UIFont systemFontOfSize:28];
    labelOne.numberOfLines = 0;
    [labelOne sizeToFit];
    labelOne.center = CGPointMake(self.view.center.y, self.view.center.x);
    
    _hud= labelOne;
    _hud.hidden = YES;
    [self.view addSubview:_hud];
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
        labelOne.text = @"截图成功";
    }else{
        _language = 0;
        labelOne.text = @"Screenshot success";
    }
}

- (void)controlViewInit
{
    if(_controlView!=nil){
        [_controlView removeFromSuperview];
    }
    
    UIView * controlView = [[UIView alloc] init];
    controlView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
    [self.view addSubview:controlView];
    
//    _camera = [[UIButton alloc] init];
//    [_camera setBackgroundImage:[UIImage imageNamed:@"play_camera"] forState:UIControlStateNormal];
//    [_camera addTarget:self action:@selector(CameraClick) forControlEvents:UIControlEventTouchUpInside];
//    [controlView addSubview:_camera];
    
    _prevMv = [[UIButton alloc] init];
    [_prevMv setBackgroundImage:[UIImage imageNamed:@"play_previous"] forState:UIControlStateNormal];
    [_prevMv addTarget:self action:@selector(ktclick) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:_prevMv];
    
    _playSt = [[UIButton alloc] init];
    _playSt.hidden = YES;
    [_playSt setBackgroundImage:[UIImage imageNamed:@"play_play"] forState:UIControlStateNormal];
    [_playSt addTarget:self action:@selector(setMoviePlay) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:_playSt];
    
    _stopSt = [[UIButton alloc] init];
    [_stopSt setBackgroundImage:[UIImage imageNamed:@"play_stop"] forState:UIControlStateNormal];
    [_stopSt addTarget:self action:@selector(setMovieParse) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:_stopSt];
    
    _nextMv = [[UIButton alloc] init];
    [_nextMv setBackgroundImage:[UIImage imageNamed:@"play_next"] forState:UIControlStateNormal];
    [_nextMv addTarget:self action:@selector(kjclick) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:_nextMv];
        
    _delete = [[UIButton alloc] init];
    [_delete setBackgroundImage:[UIImage imageNamed:@"play_delete"] forState:UIControlStateNormal];
    [_delete addTarget:self action:@selector(delclick) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:_delete];
    _controlView= controlView;
    _controlView.alpha = 0.7f;

    CGFloat interval= (300-30*4)/ 5;
    //_camera.frame = CGRectMake(interval, 5, 30, 30);
    _prevMv.frame = CGRectMake(interval, 5, 30, 30);
    _playSt.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
    _stopSt.frame = CGRectMake(interval* 2+ 30, 5, 30, 30);
    _nextMv.frame = CGRectMake(interval* 3+ 30*2, 5, 30, 30);
    //_delete.frame = CGRectMake(interval* 4+ 30*3, 5, 30, 30);
        
    _controlView.frame = UIInterfaceOrientationPortraitControlViewFrame;
}

- (void)processViewInit
{
    if(_processView!=nil){
        [_processView removeFromSuperview];
    }
    
    UIView * buttonView = [[UIView alloc] init];
    buttonView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
    [self.view addSubview:buttonView];
    _processView = buttonView;
    _processView.alpha = 0.6;
    
    UISlider * slider = [[UISlider alloc] init];
    [slider setMinimumValue:0];
    [slider setMaximumValue:1];
    
    [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"play_dot"] forState:UIControlStateHighlighted];
    slider.value = 0;
    
    CGFloat top = 10; // 顶端盖高度
    CGFloat bottom = 10 ; // 底端盖高度
    CGFloat left = 10; // 左端盖宽度
    CGFloat right = 10; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    
    [slider setMinimumTrackImage:[[UIImage imageNamed:@"progress-bar_02.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[[UIImage imageNamed:@"progress-bar_01.9"] resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    
    _topProgressSlider = slider;
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
    _tapGesture.delegate = self;
    [slider addGestureRecognizer:_tapGesture];
    [buttonView addSubview:slider];
    
    UILabel * labelright = [[UILabel alloc] initWithFrame:CGRectZero];
    labelright.text = @"-00:00";
    labelright.numberOfLines = 0;
    labelright.textColor = UIColor.whiteColor;
    [labelright sizeToFit];
    _totalTimeLabel = labelright;
    [buttonView addSubview:labelright];
    
    UILabel * labelleft = [[UILabel alloc] initWithFrame:CGRectZero];
    labelleft.text = @"-00:00";
    labelleft.numberOfLines = 0;
    labelleft.textColor = UIColor.whiteColor;
    [labelleft sizeToFit];
    _topPastTimeLabel = labelleft;
    [buttonView addSubview:labelleft];
    
    [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    _processView.frame = UIInterfaceOrientationPortraitProccessViewFrame;
    
    _size1 = [_topPastTimeLabel.text sizeWithFont:_topPastTimeLabel.font constrainedToSize:CGSizeMake(_topPastTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _size2 = [_totalTimeLabel.text sizeWithFont:_totalTimeLabel.font constrainedToSize:CGSizeMake(_totalTimeLabel.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    _topProgressSlider.frame = CGRectMake(_size1.width, 0, _processView.frame.size.width- _size1.width- _size2.width, _processView.frame.size.height);
    _topProgressSlider.center = CGPointMake(_topProgressSlider.center.x, _processView.frame.size.height/2) ;
    
    _topPastTimeLabel.frame = CGRectMake(0,  0, _topPastTimeLabel.frame.size.width, _topPastTimeLabel.frame.size.height);
    _topPastTimeLabel.center = CGPointMake(_topPastTimeLabel.center.x, _processView.frame.size.height/2) ;
    
    _totalTimeLabel.frame = CGRectMake(_topProgressSlider.frame.size.width+ _topPastTimeLabel.frame.size.width,  _topPastTimeLabel.frame.origin.y, _totalTimeLabel.frame.size.width, _totalTimeLabel.frame.size.height);
}

- (void)topViewInit
{
    UIView * topView = [[UIView alloc] init];
    _topView= topView;
    
    topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
    topView.backgroundColor = UIInterfaceOrientationLandscapeTopViewColor;
    
    _backButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
    [_backButton setBackgroundImage:[UIImage imageNamed:@"play_black_02"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backclick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_backButton];
    [self.view addSubview:topView];
    _topView.alpha = 0.6f;
    
    _topView.frame = UIInterfaceOrientationLandscapeTopViewFrame;
}

- (void)PlayViewInit
{
    _playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    _playerLayer.frame = UIInterfaceOrientationPortraitPlayViewFrame;
    _playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer insertSublayer:_playerLayer atIndex:0];
    
    _map = [[MAMapView alloc] initWithFrame:CGRectMake(0, _playerLayer.frame.origin.y+_playerLayer.frame.size.height+30, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height- _playerLayer.frame.size.height-30)];
    
    _map.userTrackingMode = MAUserTrackingModeFollow;
    [_map setDelegate:self];
    [self.view addSubview:_map];
    
    _fllow = [[UISwitch alloc] initWithFrame:CGRectMake(_map.frame.size.width-40-10, _map.frame.size.height-40-10, 40, 40)];
    [_fllow addTarget:self action:@selector(fllowclick:) forControlEvents:UIControlEventValueChanged];
    [_map addSubview:_fllow];
    _map.showsUserLocation = NO;
    
//    [self.view addSubview:playerView];
//    _playerView.backgroundColor = [UIColor blackColor];
}

-(void)brightnessValueChanged:(id)sender
{
    UISlider *senderSlider = sender;
    [[UIScreen mainScreen] setBrightness:senderSlider.value];
}

- (void)actionTapGesture:(UITapGestureRecognizer *)sender
{
    CGPoint touchPoint = [sender locationInView:_topProgressSlider];
    CGFloat value = (_topProgressSlider.maximumValue - _topProgressSlider.minimumValue) * (touchPoint.x / _topProgressSlider.frame.size.width);
    [_topProgressSlider setValue:value animated:YES];
    
    double currentTime = floor(_totalMovieDuration * value);
    //转换成CMTime才能给player来控制播放进度
    CMTime dragedCMTime = CMTimeMake(currentTime, 1);
    
    if(_player!=nil){
        [_player seekToTime:dragedCMTime completionHandler:^(BOOL finished){
            [self addProgressObserver];
            if (isPlay) {
                [_player play];
            }
        }];
    }
}

-(void)sliderValueChanged:(id)sender
{
    UISlider *senderSlider = sender;
    double currentTime = floor(_totalMovieDuration * senderSlider.value);
    //转换成CMTime才能给player来控制播放进度
    CMTime dragedCMTime = CMTimeMake(currentTime, 1);

    if(_player!=nil){
        [_player seekToTime:dragedCMTime completionHandler:^(BOOL finished){
            [self addProgressObserver];
            if (isPlay) {
                [_player play];
            }
        }];
    }

    
}

#pragma mark - 暂停
- (void)setMovieParse {
    if(_player!=nil){
        [_player pause];
    }
    isPlay = NO;
    _playSt.hidden = NO;
    _stopSt.hidden = YES;
}
#pragma mark - 播放
- (void)setMoviePlay {
    if(_player!=nil){
        [_player play];
    }
    isPlay = YES;
    _playSt.hidden = YES;
    _stopSt.hidden = NO;
}

- (void)removeclick
{
    NSString * Title;
    NSString * action1;
    NSString * action2;
    if(_language==1){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
    }else {
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];

    [self presentViewController:alert animated:YES completion:nil];

    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        NSLog(@"delete %@\n",[_filelist objectAtIndex:_index]);
        
        [_player pause];
        NSError * error= nil;
        [_filemanager removeItemAtPath:[_filelist objectAtIndex:_index] error:&error];
        if(error!=nil){
            NSLog(@"delete fault:%@\n",error);
        }
        [_filelist removeObjectAtIndex:_index];
        if(_filelist.count==0){
            self.navigationController.navigationBar.hidden = NO;
            self.tabBarController.tabBar.hidden = NO;
            
            if(_player!=nil){
                [_player pause];
            }
            
            NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
            [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        
        if(_index >= [_filelist count]){
            _index = 0;
        }
        [self switchVideo];
        _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
        
    }]];
}

- (void)psclick
{
    if (isPlay){
        [self setMovieParse];
    } else {
        [self setMoviePlay];
    }
}

//获取屏幕截屏方法
- (UIImage *)capture
{
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)cameraclick
{
    // 提示用户，截图成功
    // ...

    [self thumbnailImageRequest:[_filelist objectAtIndex:_index]];
    
    _hud.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        // 隐藏蒙版
        _hud.hidden = YES;
    });
}

- (void)backclick:(UIButton *)btn
{
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    
    if(_player!=nil){
        [_player pause];
    }
    
    NSNumber * value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getGpsStartTime
{
    NSString * name = _filelist[_index];
    NSString * playvideo = [name lastPathComponent];
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    
    [dataFormatter setDateFormat:@"yyyyMMdd_HHmmss"];
    
    NSDate * date1 = [dataFormatter dateFromString:[_video_gps_file_array[0] substringWithRange:NSMakeRange(0, 15)]];
    NSDate * date2 = [dataFormatter dateFromString:[playvideo substringWithRange:NSMakeRange(0, 15)]];
    
    _route_start = [date2 timeIntervalSince1970]- [date1 timeIntervalSince1970];
    
    //    if(_coords[_route_start].latitude!= -1 && _coords[_route_start].longitude!= -1)
    //        self.car.coordinate = _coords[_route_start];
    //    else
    //        self.car.coordinate = _route_coords[0];getGpsStartTime
}

- (NSString *)searchVideoGpsInPath:(NSString *)filename
{
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    
    [dataFormatter setDateFormat:@"yyyyMMdd_HHmmss"];
    
    NSDate * filedate = [dataFormatter dateFromString:[filename substringWithRange:NSMakeRange(0, 15)]];
    
    NSError *error = nil;
    NSString * gps_path = [NSString stringWithFormat: @"%@/Documents/gps", NSHomeDirectory()];
    NSArray * gps_file_array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:gps_path error:&error];
    if(error!=nil){
        NSLog(@"error= %@\n",error);
        return nil;
    }
    
    for(NSString * s in gps_file_array){
        NSArray * array= [s componentsSeparatedByString:@"&"];
        if(array==nil){
            return nil;
        }
        if(array.count!= 2){
            return nil;
        }
        
        NSDate * date1 = [dataFormatter dateFromString:array[0]];
        NSDate * date2 = [dataFormatter dateFromString:array[1]];
        
        if( [filedate timeIntervalSince1970]>= [date1 timeIntervalSince1970] &&  [filedate timeIntervalSince1970]<= [date2 timeIntervalSince1970] ){
            return s;
        }
    }
    
    return nil;
}

- (void)getGpsFileListWithPath:(NSString *)path
{
    NSError *error = nil;
    NSString * gps_path = [NSString stringWithFormat: @"%@/Documents/gps/%@", NSHomeDirectory(),path];
    NSArray * gps_file_array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:gps_path error:&error];
    if(error!=nil){
        NSLog(@"error= %@\n",error);
        return;
    }
    
    _video_gps_file_array = [gps_file_array copy];
}

- (void)initPlayerWithPath:(NSString *)path
{
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAsset *movieAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    _playItem= [AVPlayerItem playerItemWithAsset:movieAsset];
    [_playItem seekToTime:kCMTimeZero];
    _player= [AVPlayer playerWithPlayerItem:_playItem];
}

- (void)playWithIndex:(NSUInteger)index
{
    _index= index;
    [self initPlayerWithPath:[_filelist objectAtIndex:index]];
    
    NSString * name = _filelist[index];
    _video_gps_dir= [self searchVideoGpsInPath:[[name lastPathComponent] stringByDeletingPathExtension]];
    [self getGpsFileListWithPath:_video_gps_dir];
}


- (void)hiddenAllView
{
    self.isFirstRotatorTap = YES;
    _processView.hidden = YES;
    _topView.hidden = YES;
    _controlView.hidden = YES;
}

- (void)DisplayAllView
{
    self.isFirstRotatorTap = NO;
    _processView.hidden = NO;
    _topView.hidden = NO;
    _controlView.hidden = NO;
}

//  播放页面添加轻拍手势
- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAllSubViews:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)dismissAllSubViews:(UITapGestureRecognizer *)tap
{
    if (!self.isFirstRotatorTap)
    {
        [self hiddenAllView];
    }
    else
    {
        [self DisplayAllView];
    }

}

- (void)switchVideo
{
    _player= nil;
    _playItem= nil;
    
    if(_filelist.count==0)
        return;
    
    [self initPlayerWithPath:[_filelist objectAtIndex:_index]];
    
    [_playerLayer removeFromSuperlayer];
    
    _playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    _playerLayer.frame = self.view.frame;
    
    _playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer insertSublayer:_playerLayer atIndex:0];
    
    //关键代码
    [_playItem seekToTime:kCMTimeZero];
    [self addProgressObserver];
    
    if(_player!=nil){
        [_player play];
    }
}

#pragma mark - 观察者 观察播放完毕
- (void)addNotificationCenters {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChange:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

#pragma mark 播放结束后的代理回调
- (void)moviePlayDidEnd:(NSNotification *)notify
{
    isPlay = NO;

    _playSt.hidden = NO;
    _stopSt.hidden = YES;
}

- (void)kjclick
{
    _index++;
    if(_index >= [_filelist count]){
        _index = [_filelist count]-1;
        return;
    }
    
    [self switchVideo];
    _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];
}
- (void)ktclick
{
    if(_index==0){
        return;
    }
    _index--;
    [self switchVideo];
    _titlelabel.text = [[_filelist objectAtIndex:_index] lastPathComponent];
}
#pragma mark -  添加进度观察 - addProgressObserver
- (void)addProgressObserver
{
    //  设置每秒执行一次
    [_player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue: NULL usingBlock:^(CMTime time) {
        //  获取当前时间
        CMTime currentTime = _player.currentItem.currentTime;
        //  转化成秒数
        CGFloat currentPlayTime = (CGFloat)currentTime.value / currentTime.timescale;
        //  总时间
        CMTime totalTime = _playItem.duration;
        //  转化成秒
        _totalMovieDuration = (CGFloat)totalTime.value / totalTime.timescale;
        //  相减后
        _topProgressSlider.value = CMTimeGetSeconds(currentTime) / _totalMovieDuration;
	
        progressSlider = CMTimeGetSeconds(currentTime) / _totalMovieDuration;
        NSDate *pastDate = [NSDate dateWithTimeIntervalSince1970: currentPlayTime];
        
        _topPastTimeLabel.text = [self getTimeByDate:pastDate byProgress: currentPlayTime];
        CGFloat totalTimeF = _totalMovieDuration;
        NSDate *totalDate = [NSDate dateWithTimeIntervalSince1970: totalTimeF];
        _totalTimeLabel.text = [self getTimeByDate:totalDate byProgress: totalTimeF];
        
        int i= 0;
        if(_route_start==0)
            return;
        
        i= (int)(_topProgressSlider.value*60);
        
        if(_coords==nil)
            return;
        
        NSLog(@"_coords %@\n",_coords);
        
        if(_coords[i+_route_start].latitude== -1 && _coords[i+_route_start].longitude== -1)
            return;
        
        self.car.coordinate = _coords[i+_route_start];
        [_annotationView.annotation setCoordinate:_coords[i+_route_start]];
        if(_setCenterCoordinate==YES){
            [_map setCenterCoordinate:_coords[i+_route_start] animated:YES];
        }
    }];
}

- (NSString *)getTimeByDate:(NSDate *)date byProgress:(float)current
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (current / 3600 >= 1) {
        [formatter setDateFormat:@"HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"mm:ss"];
    }
    return [formatter stringFromDate:date];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // 禁用 iOS7 返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}


-(void)thumbnailImageRequest:(NSString *)path
{
    NSURL *url = [NSURL fileURLWithPath:path];
    AVURLAsset *urlAssert= [AVURLAsset assetWithURL:url];
    AVAssetImageGenerator * imageGennerator = [[AVAssetImageGenerator alloc ] initWithAsset:urlAssert];
    
    NSError * error = nil;
    CMTime actualTime;
    CGImageRef cgImage = [imageGennerator copyCGImageAtTime:_player.currentItem.currentTime actualTime:&actualTime error:&error];
    if(error!=nil){
        NSLog(@"%@\n",error.localizedDescription);
        return;
    }
    CMTimeShow(actualTime);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *currentTime = [formatter stringFromDate:[NSDate date]];
    NSString * pic_path = [NSString stringWithFormat: @"%@/Documents/%@/%@_%@.jpg", NSHomeDirectory(),@"picture",[[path lastPathComponent]stringByDeletingPathExtension],currentTime];

    [UIImageJPEGRepresentation(image, 1.0) writeToFile:pic_path atomically:YES];
    CGImageRelease(cgImage);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
        //记录开始点击的位置
        _startPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    NSArray *arrayTouches = [[event allTouches] allObjects];
    if (arrayTouches.count == 1) {
        //单指移动
        CGPoint tempPoint = [[arrayTouches objectAtIndex:0] locationInView:self.view];
        
        CGFloat moveX = tempPoint.x - _startPoint.x;
        CGFloat moveY = tempPoint.y - _startPoint.y;
#if 0
        if(fabs(moveX) > fabs(moveY))
        {
            
        }
        else
        {
            if(_startPoint.x > [UIScreen mainScreen].bounds.size.height/2)
            {
                float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
                float newVolume = volume;
                
                if(fabs(moveY) > 5.0){
                    newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    
                    if (newVolume < 0) {
                        newVolume = 0;
                    } else if (newVolume > 1.0) {
                        newVolume = 1.0;
                    }
                    
                    [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
                }
            }else{
                float brightness = [[UIScreen mainScreen] brightness];
                float newbrightness = brightness;
                
                if(fabs(moveY) > 5.0){
                    newbrightness -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
                    if (newbrightness < 0) {
                        newbrightness = 0;
                    } else if (newbrightness > 1.0) {
                        newbrightness = 1.0;
                    }
                    
                    [[UIScreen mainScreen] setBrightness:newbrightness];
                }
            }
        }
#else
        float volume = [[MPMusicPlayerController applicationMusicPlayer] volume];
        float newVolume = volume;
        
        if(fabs(moveY) > 5.0){
            newVolume -= moveY / [UIScreen mainScreen].bounds.size.height / 8;
            
            if (newVolume < 0) {
                newVolume = 0;
            } else if (newVolume > 1.0) {
                newVolume = 1.0;
            }
            [[MPMusicPlayerController applicationMusicPlayer] setVolume:newVolume];
        }
#endif
    }
}

#if 1
- (BOOL)shouldAutorotate
{
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#endif
@end
