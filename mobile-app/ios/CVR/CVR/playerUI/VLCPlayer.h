//
//  VLCPlayer.h
//  CVR
//
//  Created by 雷起斌 on 6/16/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VLCPlayer : UIViewController
- (void)initLivePlayer:(NSURL *)url;
- (void)playWithIndex:(NSUInteger)index;

//@property (nonatomic, assign) BOOL isLiveStream;

@property (nonatomic,strong)NSMutableArray * filelist;
@property (nonatomic, assign)NSUInteger index;
@property (nonatomic, strong)NSString * form;

@end
