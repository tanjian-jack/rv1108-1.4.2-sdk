//
//  LocalAVPlayerViewController.h
//  CVR
//
//  Created by 雷起斌 on 5/26/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface LocalAVPlayerViewController : UIViewController
@property (nonatomic,strong)NSMutableArray * filelist;
@property (nonatomic, assign)NSUInteger index;

- (void)playWithIndex:(NSUInteger)index;

@end
