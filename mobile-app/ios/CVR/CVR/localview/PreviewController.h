//
//  PreviewController.h
//  CVR
//
//  Created by 雷起斌 on 5/24/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewController : UIViewController <UIViewControllerAnimatedTransitioning>
@property NSInteger mode;
@end
