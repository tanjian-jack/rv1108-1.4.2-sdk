//
//  localfilecell.h
//  CVR
//
//  Created by 雷起斌 on 5/23/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface localfilecell : UICollectionViewCell
//@property (weak, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIImageView *selstatus;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *day;

//@property (nonatomic, assign) BOOL isSelect;
@end
