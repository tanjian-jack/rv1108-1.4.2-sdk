//
//  localfileView.m
//  CVR
//
//  Created by 雷起斌 on 5/23/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "localfileView.h"
#import "PreviewController.h"

@interface localfileView ()
@property (strong, nonatomic) IBOutlet UIButton *allpic;
@property (strong, nonatomic) IBOutlet UILabel *allpicnum;
@property (strong, nonatomic) IBOutlet UILabel *allvadionum;


@property (nonatomic, strong) NSFileManager * filemanager;

@property (nonatomic, strong)NSArray * filelist;
@property (nonatomic, strong)NSArray * picfilelist;
@end

@implementation localfileView

- (void)showFileCount
{
    NSString * normal_path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"video"] ;
    NSString * pic_path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"picture"];
    
    CGFloat R  = (CGFloat) 0;
    CGFloat G = (CGFloat) 167/255.0;
    CGFloat B = (CGFloat) 157/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R green:G blue:B  alpha:alpha];
    self.navigationController.navigationBar.barTintColor = myColorRGB;
    
    NSError *error = nil;
    _filelist = [_filemanager contentsOfDirectoryAtPath:normal_path error:&error];
    _picfilelist = [_filemanager contentsOfDirectoryAtPath:pic_path error:&error];

    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"English"]==TRUE){
        _allvadionum.text = [NSString stringWithFormat:@"Video(%zi)",_filelist.count];
        _allpicnum.text = [NSString stringWithFormat:@"Pictures(%zi)",_picfilelist.count];
    }else if([str isEqualToString:@"Chinese"]==TRUE){
        _allvadionum.text = [NSString stringWithFormat:@"全部视频(%zi)",_filelist.count];
        _allpicnum.text = [NSString stringWithFormat:@"全部图片(%zi)",_picfilelist.count];
    }else {
        _allvadionum.text = [NSString stringWithFormat:@"Video(%zi)",_filelist.count];
        _allpicnum.text = [NSString stringWithFormat:@"Pictures(%zi)",_picfilelist.count];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat R  = (CGFloat) 0;
    CGFloat G = (CGFloat) 167/255.0;
    CGFloat B = (CGFloat) 157/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    self.navigationController.navigationBar.barTintColor = myColorRGB;
    
    self.filemanager= [NSFileManager defaultManager];
//    [self showFileCount];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self showFileCount];
}

// 跳转之前的时候调用
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"local2pic"])
    {
        PreviewController * dest = segue.destinationViewController;
        dest.mode = 0;
    }
    else if([segue.identifier isEqualToString:@"local2video"])
    {
        PreviewController * dest = segue.destinationViewController;
        dest.mode = 1;
    }
}

- (BOOL)shouldAutorotate
{
    //是否支持转屏
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
