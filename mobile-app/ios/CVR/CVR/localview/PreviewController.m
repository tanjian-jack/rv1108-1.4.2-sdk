//
//  PreviewController.m
//  CVR
//
//  Created by 雷起斌 on 5/24/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "PreviewController.h"
#import "localfilecell.h"
#import "LocalAVPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "FLImageShowVC.h"

@interface PreviewController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftbarbutton;
@property (nonatomic, assign)NSInteger leftbarbuttonMode;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;

@property (nonatomic, strong)NSMutableArray * indexPathsArray;
@property (nonatomic, strong)NSMutableArray * filelist;
@property (nonatomic, strong)NSFileManager * filemanager;

@property (nonatomic, strong)NSArray * normalfilelist;
//@property (nonatomic, strong)NSArray * lockfilelist;
@property (nonatomic, strong)NSArray * picturefilelist;

@property (nonatomic, strong) AVPlayer *player;//播放器对象
@property (strong, nonatomic) IBOutlet UIBarButtonItem *NacButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *deletebutton;

@property (nonatomic, assign)BOOL isSeleleMode;
@end

@implementation PreviewController
- (IBAction)deleteclick:(id)sender
{
    NSString * Title;
    NSString * action1;
    NSString * action2;
    NSString * leftText;
    NSString * rightText;
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"是否需要删除文件？";
        action1 = @"确定";
        action2 = @"取消";
        leftText = @"返回";
        rightText = @"选择";
    }else{
        Title = @"Do you need to delete the file";
        action1 = @"YES";
        action2 = @"NO";
        leftText = @"Back";
        rightText = @"Edit";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:Title preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        NSMutableArray * deleteDeals = [NSMutableArray array];
        for(NSIndexPath * path in _indexPathsArray){
            [deleteDeals addObject:[_filelist objectAtIndex:path.item]];
            NSError * error= nil;
            [self.filemanager removeItemAtPath:[_filelist objectAtIndex:path.item] error:&error];
            if(error!=nil){
                NSLog(@"delete fault %@\n",error);
                return;
            }
        }
        [_filelist removeObjectsInArray:deleteDeals];
        
        [_indexPathsArray removeAllObjects];
        [_collectionview reloadData];
        _deletebutton.enabled = NO;
        
        _leftbarbuttonMode = 1;
        _leftbarbutton.title = leftText;
        _isSeleleMode = NO;
        _deletebutton.enabled = NO;
        [_indexPathsArray removeAllObjects];
        [_collectionview reloadData];
        _NacButton.title= rightText;
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
}

- (IBAction)selclick:(id)sender
{
    _isSeleleMode = !_isSeleleMode;
    if( _isSeleleMode==NO ){
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            _leftbarbutton.title = @"返回";
            _NacButton.title = @"选择";
        }else {
            _leftbarbutton.title = @"Back";
            _NacButton.title = @"Edit";
        }
        
        _leftbarbuttonMode = 0;
        _deletebutton.enabled = NO;
        [_indexPathsArray removeAllObjects];
        [_collectionview reloadData];
    }
    else{
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        NSString * str = [userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            _leftbarbutton.title = @"全选";
            _NacButton.title = @"取消";
        }else{
            _leftbarbutton.title = @"All";
            _NacButton.title = @"Cancel";
        }
        
        _leftbarbuttonMode = 1;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    CGFloat R  = (CGFloat) 0;
    CGFloat G = (CGFloat) 167/255.0;
    CGFloat B = (CGFloat) 157/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R green:G blue:B  alpha:alpha];
    self.navigationController.navigationBar.barTintColor = myColorRGB;
    
    _leftbarbuttonMode = 0;
    
    _indexPathsArray = [[NSMutableArray alloc] init];
    _filelist = [[NSMutableArray alloc] init];
    _filemanager= [NSFileManager defaultManager];
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(85, 85);
    
    [_collectionview registerNib:[UINib nibWithNibName:NSStringFromClass([localfilecell class]) bundle:nil] forCellWithReuseIdentifier:@"filepreview"];
    
    _collectionview.allowsMultipleSelection= NO;
    
    [_collectionview setCollectionViewLayout:layout];
    _collectionview.backgroundColor = [UIColor whiteColor];
    _collectionview.dataSource = self;
    _collectionview.delegate = self;

    _deletebutton.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _leftbarbutton.title = @"返回";
        _NacButton.title = @"选择";
    }else {
        _leftbarbutton.title = @"Back";
        _NacButton.title = @"Edit";
    }
    _isSeleleMode = NO;
    _leftbarbuttonMode = 0;
    _deletebutton.enabled = NO;
    
    [_filelist removeAllObjects];
    [self filelist_init];
    [self.collectionview reloadData];
    
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}

- (void)filelist_init
{
    NSString * normal_path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"video"] ;
    NSString * pic_path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(),@"picture"];
    
    NSError *error = nil;
    _normalfilelist = [_filemanager contentsOfDirectoryAtPath:normal_path error:&error];
    if(error!=nil){
        NSLog(@"open %@ fault:%@\n",normal_path, error.localizedDescription);
    }
    
    _picturefilelist = [_filemanager contentsOfDirectoryAtPath:pic_path error:&error];
    if(error!=nil){
        NSLog(@"open %@ fault:%@\n",pic_path, error.localizedDescription);
    }
    
    if(self.mode==0)
    {
        for( NSString * filename in _picturefilelist ){
            if([[filename pathExtension] isEqualToString:@"jpg"]==YES)
            {
                [_filelist addObject:[NSString stringWithFormat:@"%@%@",pic_path,filename]];
            }
        }
    }
    else if(self.mode==1)
    {
        for( NSString * filename in _normalfilelist ){
            if([[filename pathExtension] isEqualToString:@"mp4"]==YES)
            {
                [_filelist addObject:[NSString stringWithFormat:@"%@%@",normal_path,filename]];
            }
        }
    }else if(self.mode==2){
        
    }else if(self.mode==3){
        
    }else{
        
    }
}

//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(_isSeleleMode==NO){
        if(self.mode!=0){
            LocalAVPlayerViewController * avplayVc = [[LocalAVPlayerViewController alloc] init];
        
            avplayVc.filelist = [[NSMutableArray alloc] init];
            avplayVc.filelist = _filelist;
            [avplayVc playWithIndex:indexPath.item];
            [self.navigationController pushViewController:avplayVc animated:YES];
        }
        else{
            self.navigationController.navigationBar.hidden = YES;
            self.tabBarController.tabBar.hidden = YES;
            
            FLImageShowVC *fvc = [[FLImageShowVC alloc] init];
            fvc.localImageNamesArray = [_filelist copy];
            fvc.currentIndex = indexPath.item;
            [self.navigationController pushViewController:fvc animated:YES];
        }
    }
    else{
        _deletebutton.enabled = YES;
        
        for(NSIndexPath * i in _indexPathsArray){
            if(i==indexPath){
                [_indexPathsArray removeObject:indexPath];
                [self.collectionview reloadData];
                if( [_indexPathsArray count]==0 ){
                    _deletebutton.enabled = NO;
                }
                return;
            }
        }
            
        [_indexPathsArray addObject:indexPath];
        [self.collectionview reloadData];
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _filelist.count;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    localfilecell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"filepreview" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([localfilecell class]) owner:nil options:nil] lastObject];
    }
    
    if(self.mode!=0){
        NSString *path = NSTemporaryDirectory();
        path = [path stringByAppendingPathComponent:[[_filelist objectAtIndex:indexPath.item] lastPathComponent]];
        
        path = [path stringByAppendingString:@".jpg"];
        if([self.filemanager fileExistsAtPath:path]==YES){
            cell.image.image = [UIImage imageWithContentsOfFile:path];
        }else{
            cell.image.image = [self thumbnailImageRequest:[_filelist objectAtIndex:indexPath.item]];
            if(cell.image.image!=nil){
                [UIImageJPEGRepresentation(cell.image.image, 1) writeToFile:path atomically:YES];
            }
        }
    }else{
        cell.image.image = [UIImage imageWithContentsOfFile:[_filelist objectAtIndex:indexPath.item]];
    }
    
    cell.selstatus.hidden = YES;
    if([_indexPathsArray count]==0)return cell;
    
    for(NSIndexPath * path in _indexPathsArray){
        if(path.item==indexPath.item){
            cell.selstatus.hidden = NO;
            return cell;
        }
    }
    return cell;
}

-(UIImage *)thumbnailImageRequest:(NSString *)path
{
    NSURL *url = [NSURL fileURLWithPath:path];
    AVURLAsset *urlAssert= [AVURLAsset assetWithURL:url];
    AVAssetImageGenerator * imageGennerator = [[AVAssetImageGenerator alloc ] initWithAsset:urlAssert];
    
    NSError * error = nil;
    CMTime time = CMTimeMakeWithSeconds(0, 30);
    CMTime actualTime;
    CGImageRef cgImage = [imageGennerator copyCGImageAtTime:time actualTime:&actualTime error:&error];
    if(error!=nil){
        NSLog(@"%@\n",error.localizedDescription);
        return nil;
    }
    CMTimeShow(actualTime);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return image;
}

- (IBAction)leftbarbutton:(id)sender
{
    if(_isSeleleMode==NO){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if(_leftbarbuttonMode==0){
            _leftbarbuttonMode= 1;
            
            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            NSString * str = [userDefaults objectForKey:@"app_language"];
            if([str isEqualToString:@"Chinese"]==YES){
                _leftbarbutton.title = @"全选";
            }else{
                _leftbarbutton.title = @"All";
            }

            _deletebutton.enabled = NO;
            [_indexPathsArray removeAllObjects];
            [_collectionview reloadData];
        }else{
            _leftbarbuttonMode= 0;

            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            NSString * str = [userDefaults objectForKey:@"app_language"];
            if([str isEqualToString:@"Chinese"]==YES){
                _leftbarbutton.title = @"全不选";
            }else{
                _leftbarbutton.title = @"None";
            }
            
            [_indexPathsArray removeAllObjects];
            
            for(NSInteger i= 0; i< _filelist.count; i++){
                NSIndexPath *index = [NSIndexPath indexPathForItem:i inSection:0];
                [_indexPathsArray addObject:index];
            }
             _deletebutton.enabled = YES;
            [_collectionview reloadData];
        }
    }
}


- (BOOL)shouldAutorotate
{
    //是否支持转屏
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
