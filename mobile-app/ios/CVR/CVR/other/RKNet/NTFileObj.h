//
//  NTFileObj.h
//  CVR
//
//  Created by 雷起斌 on 9/29/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NTFileObj : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *form;
@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) NSString *thumb;
@property (strong, nonatomic) NSString *gps;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *day;
@property (strong, nonatomic) NSString *size;
@property (nonatomic, assign) NSInteger size_int;
@end

