//
//  WiFiConnection.h
//  CVR
//
//  Created by 雷起斌 on 4/20/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WiFiConnectionProtocol.h"
#import "NTFileObj.h"
#import "GPSObject.h"

@interface WiFiConnection : NSObject
- (NSError *)ConnectToCVR:(NSString *)host;
- (void)disConnectFromCVR;

- (void)SendMsgToCVR:(NSString *)longConnect;
- (void)SendDataToCVR:(NSData *)dataStream;
- (void)broadcast_send:(NSString *)longConnect;

- (void)deleteFile:(NSMutableArray *)delNames;
- (void)GetDirectoryContents;
- (void)DiscoverCVR;
- (void)GetThumbContents;
- (void)GetGpsContents:(NSString *)file;

+ (WiFiConnection *)sharedSingleton;
+ (NSMutableArray *)analysisStringFromString:(NSString *)aStr :(NSArray *)array;
- (NSError *)UploadFile:(NSString *)path;

@property (nonatomic, weak) id Vc;

@property (nonatomic, strong) NSString * NTConnectionHost;
@property (nonatomic, strong) NSString * NTConnectionSSID;

@property (nonatomic, weak) id <DirectoryContentsDelegate> GetDirectoryContentsDelegate;
@property (nonatomic, weak) id <DiscoverDelegate> DiscoverDelegate;
@property (nonatomic, weak) id <GetSettingDelegate> GetSettingDelegate;
@property (nonatomic, weak) id <GetFontCameraSettingDelegate> GetFontCameraSettingDelegate;
@property (nonatomic, weak) id <GetBackCameraSettingDelegate> GetBackCameraSettingDelegate;
@property (nonatomic, weak) id <GetPhotoQualitySettingDelegate> GetPhotoQualitySettingDelegate;
@property (nonatomic, weak) id <DeleteFileDelegate> DeleteFileDelegate;
@end
