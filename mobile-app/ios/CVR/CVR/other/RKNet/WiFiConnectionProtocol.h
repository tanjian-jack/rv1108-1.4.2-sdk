//
//  WiFiConnectionProtocol.h
//  RockchipsCVR
//
//  Created by 雷起斌 on 10/7/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NTFileObj.h"

@protocol DirectoryContentsDelegate <NSObject>
- (void)onDirectoryContents:(NTFileObj *)fileinfo;
- (void)onGPSGetAllBreak:(NSArray *)array;
@end

@protocol DiscoverDelegate <NSObject>
- (void)onDiscoverCVR:(NSArray *)arg;
@end

@protocol GetSettingDelegate <NSObject>
- (void)onGetSettingCVR:(NSMutableDictionary *)dictM;
@end

@protocol GetFontCameraSettingDelegate <NSObject>
- (void)onGetFontCameraSetting:(NSMutableArray *)arryM;
@end

@protocol GetBackCameraSettingDelegate <NSObject>
- (void)onGetBackCameraSetting:(NSMutableArray *)arryM;
@end

@protocol GetPhotoQualitySettingDelegate <NSObject>
- (void)onGetPhotoQualitySetting:(NSMutableArray *)arryM;
@end

@protocol DeleteFileDelegate <NSObject>
- (void)onDeleteFileSuccess:(NSString *)file;
- (void)onDeleteFileFault:(NSString *)file;
- (void)onDeleteFileFinish;
@end
