//
//  WiFiConnection.m
//  CVR
//
//  Created by 雷起斌 on 4/20/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "WiFiConnection.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "AsyncSocket.h"
#import "AsyncUdpSocket.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "NTFileObj.h"
#import <UIKit/UIKit.h>

@interface WiFiConnection () <AsyncSocketDelegate>
@property (nonatomic, strong)AsyncSocket * asyncSocket;
@property (nonatomic, strong)AsyncSocket * asyncUploadSocket;
@property (nonatomic, strong)AsyncUdpSocket *asyncUdpSocket;
@property (nonatomic, strong)NSMutableArray *deletelist;
@property (nonatomic, strong)NSMutableArray *gpslist;
@property (nonatomic, strong)NSMutableArray *allgpslist;
@property (nonatomic, assign)NSInteger type_index;
@property (nonatomic, assign)unsigned short crc16;
//@property (nonatomic, assign)BOOL isDelete;
@end

@implementation WiFiConnection

static const unsigned long int CRC32_Table[256] =
{
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

-(unsigned long int) crc32:(unsigned char *)Data_Array length:(NSInteger)uSize
{
    unsigned long uCRCValue;
    unsigned char *pData;
    
    /* init the start value */
    uCRCValue = 0xffffffff;
    pData = Data_Array;
    
    /* calculate CRC */
    while (uSize --)
    {
        uCRCValue = CRC32_Table[(uCRCValue ^ *pData++) & 0xFF] ^ (uCRCValue >> 8);
    }
    /* XOR the output value */
    return uCRCValue ^ 0xffffffff;
}

- (unsigned short)crc16:(const char *)Data_Array length:(NSInteger)DataLength
{
    unsigned long int i, j;
    unsigned short int CRCTemp = 0xffff;
    
    for(i= 0; i< DataLength; i++)
    {
        CRCTemp = CRCTemp ^Data_Array[i];
        for(j=0;j<=7;j++)
        {
            if((CRCTemp & 0x0001)==0x0001)
            {
                CRCTemp = CRCTemp >>1;
                CRCTemp ^= 0xa001;
            }
            else CRCTemp = CRCTemp >> 1;
        }
    }
    return CRCTemp;
}

+ (NSMutableArray *)analysisStringFromString:(NSString *)aStr :(NSArray *)array
{
    NSMutableArray * stringArray = [[NSMutableArray alloc] init];
    NSArray * separate;
    NSString * remain;
    
    remain= aStr;
    for(NSString * s in array)
    {
        separate= [remain componentsSeparatedByString:s];
        [stringArray addObject:separate[0]];
        
        if(separate.count==2)
            remain= separate[1];
    }
    
    [stringArray addObject:separate[1]];
    return stringArray;
}

+ (WiFiConnection *)sharedSingleton
{
    static WiFiConnection *sharedSingleton;
    @synchronized(self)
    {
        if (!sharedSingleton){
            sharedSingleton = [[WiFiConnection alloc] init];
            sharedSingleton.deletelist = [[NSMutableArray alloc] init];
            sharedSingleton.gpslist = [[NSMutableArray alloc] init];
            sharedSingleton.allgpslist = [[NSMutableArray alloc] init];
        }
        return sharedSingleton;
    }
}

+ (NSString *)getBroadcastAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    struct in_addr broadcast;
    in_addr_t mask, ip;
    
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"bridge100"] || [[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C Strin
                    
                    ip= (((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr).s_addr;
                    mask=  (((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr).s_addr;
                    
                    broadcast.s_addr = ip & mask;
                    broadcast.s_addr |= ~mask;
                    address= [NSString stringWithUTF8String:inet_ntoa(broadcast)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

- (void)broadcast_init
{
    self.asyncUdpSocket=[[AsyncUdpSocket alloc] initWithDelegate:self];
    NSError *err = nil;
    [self.asyncUdpSocket enableBroadcast:YES error:&err];
    [self.asyncUdpSocket bindToPort:18888 error:nil];
    [self.asyncUdpSocket receiveWithTimeout:-1 tag:0];
}

- (void)broadcast_send:(NSString *)longConnect
{
    NSString *address = [WiFiConnection getBroadcastAddress];
    NSData *dataStream=[longConnect dataUsingEncoding:NSUTF8StringEncoding];
    [self.asyncUdpSocket sendData:dataStream toHost:address port:18889 withTimeout:-1 tag:1];
}

- (NSError *)ConnectToCVR:(NSString *)host
{
    if(self.asyncSocket!=nil){
        [self.asyncSocket disconnect];
        self.asyncSocket = nil;
    }
    
    self.asyncSocket  = [[AsyncSocket alloc] initWithDelegate:self];
    
    NSError *error = nil;
    [self.asyncSocket connectToHost:host onPort:8888 withTimeout:-1 error:&error];
    [self.asyncSocket readDataWithTimeout:-1 tag:1];
    
    _NTConnectionHost = host;
    
    return error;
}

- (void)disConnectFromCVR
{
    [self.asyncSocket disconnect];
    self.asyncSocket = nil;
    _NTConnectionHost = nil;
}

- (void)CreateAPServer
{
    self.asyncUdpSocket=[[AsyncUdpSocket alloc] initWithDelegate:self];
    
    [self.asyncUdpSocket bindToPort:18889 error:nil];
    [self.asyncUdpSocket receiveWithTimeout:-1 tag:0];
}

- (void)SendMsgToCVR:(NSString *)longConnect
{
    NSData *dataStream = [longConnect dataUsingEncoding:
                          NSUTF8StringEncoding];
    
    [self.asyncSocket writeData:dataStream withTimeout:-1 tag:1];
}

- (void)SendDataToCVR:(NSData *)dataStream
{
    [self.asyncSocket writeData:dataStream withTimeout:-1 tag:1];
}

- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port
{
    [self.asyncUdpSocket receiveWithTimeout:-1 tag:0];
    
    NSString* aStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@\n",aStr);
#if 1
    if([aStr rangeOfString:@"CMD_DISCOVER"].location != NSNotFound)
    {
        NSLog(@"%@\n",aStr);
        
        NSArray * arry= [aStr componentsSeparatedByString:@"WIFINAME:"];
        if(arry==nil){
            return FALSE;
        }
        if([arry count]< 2){
            return FALSE;
        }
        
        NSArray * arry1= [arry[1] componentsSeparatedByString:@"SID:"];
        if(arry1==nil){
            return FALSE;
        }
        
        NSArray * arry2= [arry1[1] componentsSeparatedByString:@"PRODUCT:"];
        if(arry2==nil){
            return FALSE;
        }
        
        NSArray * arry3= [arry2[1] componentsSeparatedByString:@"END"];
        if(arry3==nil){
            return FALSE;
        }
        
        NSArray * arg = @[arry1[0], host, arry2[0], arry3[0]];
        
        if(self.DiscoverDelegate==nil)return TRUE;
        if([self.DiscoverDelegate respondsToSelector:@selector(onDiscoverCVR:)])
        {
            [self.DiscoverDelegate onDiscoverCVR:arg];
        }
    }
#else
    if([aStr rangeOfString:@"CMD_DISCOVER"].location != NSNotFound)
    {
        NSLog(@"%@\n",aStr);
        
        NSArray * arry= [aStr componentsSeparatedByString:@"WIFINAME:"];
        if(arry==nil){
            return FALSE;
        }
        if([arry count]< 2){
            return FALSE;
        }
        
        NSArray * arry1= [arry[1] componentsSeparatedByString:@"SID:"];
        if(arry1==nil){
            return FALSE;
        }
        
        NSArray * arry2= [arry1[1] componentsSeparatedByString:@"END"];
        if(arry2==nil){
            return FALSE;
        }
        
        NSArray * arg = @[arry1[0], host, arry2[0]];
        
        if(self.DiscoverDelegate==nil)return TRUE;
        if([self.DiscoverDelegate respondsToSelector:@selector(onDiscoverCVR:)])
        {
            [self.DiscoverDelegate onDiscoverCVR:arg];
        }
    }
#endif
    return TRUE;
}

-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    [self.asyncSocket readDataWithTimeout:-1 tag:0];
    NSString* aStr = [[NSString alloc] initWithData:data encoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
    
    NSLog(@"%@\n",aStr);

    if([aStr rangeOfString:@"CMD_GETCAMFILE"].location != NSNotFound)
    {
        NTFileObj * fileinfo = [[NTFileObj alloc] init];
        NSArray * Head= [aStr componentsSeparatedByString:@"NAME:"];
        if(Head==nil){
            return;
        }
        if([Head count]< 2){
            return;
        }
        
        NSArray * NameArry= [Head[1] componentsSeparatedByString:@"TYPE:"];
        if(NameArry==nil){
            return;
        }
        if([NameArry count]< 2){
            return;
        }
        fileinfo.name = NameArry[0];
        
        NSArray * TypeArry = [[NSArray alloc] init];
        TypeArry=[NameArry[1] componentsSeparatedByString:@"PATH:"];
        if( TypeArry==nil){
            return;
        }
        if([TypeArry count]!= 2){
            return;
        }
        fileinfo.type = TypeArry[0];
        
        NSArray * PATH = [[NSArray alloc] init];
        PATH=[TypeArry[1] componentsSeparatedByString:@"FORM:"];
        if( PATH==nil){
            return;
        }
        if([PATH count]!= 2){
            return;
        }
        
        NSArray * file_path = [[NSArray alloc] init];
        file_path=[PATH[0] componentsSeparatedByString:@"&"];
        if( file_path==nil){
            return;
        }
        
        if([file_path[0] isEqualToString:@"NULL"]==YES){
            fileinfo.path= nil;
        }
        fileinfo.path = file_path[0];
        
        if([file_path[1] isEqualToString:@"NULL"]==YES){
            fileinfo.thumb = nil;
        }
        fileinfo.thumb = file_path[1];
        
        if([file_path[2] isEqualToString:@"NULL"]==YES){
            fileinfo.gps = nil;
        }
        fileinfo.gps = file_path[2];
        
        NSArray * Form= [[NSArray alloc] init];
        Form= [PATH[1] componentsSeparatedByString:@"SIZE:"];
        if( Form== nil ){
            return;
        }
        if( [Form count]< 2){
            return;
        }
        fileinfo.form = Form[0];
        
        NSArray * SizeArry= [[NSArray alloc] init];
        SizeArry= [Form[1] componentsSeparatedByString:@"DAY:"];
        if( SizeArry== nil ){
            return;
        }
        if( [SizeArry count]< 2){
            return;
        }
        NSString * size = SizeArry[0];
        
        fileinfo.size_int= [size integerValue];
        if(fileinfo.size_int<= 1024){
            fileinfo.size = [NSString stringWithFormat:@"%ziB",fileinfo.size_int];
        }
        else if(fileinfo.size_int> 1024*1024*1024){
            fileinfo.size = [NSString stringWithFormat:@"%.2fGB",(float)fileinfo.size_int/(1024*1024*1024)];
        }
        else if(fileinfo.size_int> 1024*1024){
            fileinfo.size = [NSString stringWithFormat:@"%.2fMB",(float)fileinfo.size_int/(1024*1024)];
        }
        else if(fileinfo.size_int> 1024){
            fileinfo.size = [NSString stringWithFormat:@"%.2fKB",(float)fileinfo.size_int/1024];
        }
        
        NSArray * DayArry= [SizeArry[1] componentsSeparatedByString:@"TIME:"];
        if(DayArry==nil){
            NSLog(@"CMD_GETFCAMFILE:TIME ERROR %@\n",aStr);
            return;
        }
        if([DayArry count]< 2){
            NSLog(@"CMD_GETFCAMFILE:TIME ERROR %@\n",aStr);
            return;
        }
        fileinfo.day = DayArry[0];
        
        NSArray * TimeArry= [DayArry[1] componentsSeparatedByString:@"COUNT:"];
        if( TimeArry==nil){
            NSLog(@"CMD_GETFCAMFILE:SIZE ERROR %@\n",aStr);
            return;
        }
        fileinfo.time = TimeArry[0];
        
        if(self.GetDirectoryContentsDelegate==nil)return;
        if([self.GetDirectoryContentsDelegate respondsToSelector:@selector(onDirectoryContents:)])
        {
            [self.GetDirectoryContentsDelegate onDirectoryContents:fileinfo];
        }
        
        [self SendMsgToCVR:@"CMD_NEXTFILE"];
    }
    else if([aStr rangeOfString:@"CMD_ACK_GET_ARGSETTING"].location != NSNotFound)
    {
        int i= 1;
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSArray * cmd_arry = @[@"Videolength:",@"RecordMode:",@"3DNR:", @"ADAS:", @"WhiteBalance:", @"Exposure:", @"MotionDetection:", @"DataStamp:" , @"RecordAudio:", @"BootRecord:", @"Language:", @"Frequency:", @"Bright:", @"IDC:", @"FLIP:", @"TIME_LAPSE:", @"AUTOOFF_SCREEN:", @"QUAITY:", @"Collision:", @"LICENCE_PLATE:", @"PIP:", @"DVS:", @"KeyTone:", @"Time:", @"Version:"];
        NSMutableArray * arry= [WiFiConnection analysisStringFromString:aStr :cmd_arry];
        
        for(NSString * arg in cmd_arry){
            NSLog(@"%@ %@\n",arry[i], arg);
            [userDefaults setObject:arry[i] forKey:arg];
            i++;
        }

        if(self.GetSettingDelegate==nil)return;
        if([self.GetSettingDelegate respondsToSelector:@selector(onGetSettingCVR:)])
        {
            [self.GetSettingDelegate onGetSettingCVR:nil];
        }
    }else if([aStr rangeOfString:@"CMD_WIFI_INFO"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CMD_WIFI_INFO" object:aStr];
    }
    else if([aStr rangeOfString:@"CMD_ACK_FRONT_CAMERARESPLUTION"].location != NSNotFound)
    {
        NSLog(@"%@\n",aStr);
        NSMutableArray * font = [[NSMutableArray alloc] init];
        
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_ACK_FRONT_CAMERARESPLUTION:"];
        if(head==nil){
            return;
        }
        if([head count]< 2){
            return;
        }
        
        NSArray * dat= [head[1] componentsSeparatedByString:@":"];
        if(dat==nil){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        if([dat count]< 2){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        
        for(NSInteger i = 0; i< [dat count]-1;i++){
            NSLog(@"%@\n",dat[i]);
            [font addObject:dat[i]];
        }
        
        if(self.GetFontCameraSettingDelegate==nil)return;
        if([self.GetFontCameraSettingDelegate respondsToSelector:@selector(onGetFontCameraSetting:)])
        {
            [self.GetFontCameraSettingDelegate onGetFontCameraSetting:font];
        }
    }else if([aStr rangeOfString:@"CMD_ACK_BACK_CAMERARESPLUTION"].location != NSNotFound)
    {
        NSMutableArray * back = [[NSMutableArray alloc] init];
        
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_ACK_BACK_CAMERARESPLUTION:"];
        if(head==nil){
            NSLog(@"CMD_ACK_BACK_CAMERARESPLUTION ERROR %@\n",aStr);
            return;
        }
        if([head count]< 2){
            NSLog(@"CMD_ACK_BACK_CAMERARESPLUTION ERROR %@\n",aStr);
            return;
        }
        
        NSArray * dat= [head[1] componentsSeparatedByString:@":"];
        if(dat==nil){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        if([dat count]< 2){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        
        for(NSInteger i = 0; i< [dat count]-1;i++){
            [back addObject:dat[i]];
        }
        
        if(self.GetBackCameraSettingDelegate==nil)return;
        if([self.GetBackCameraSettingDelegate respondsToSelector:@selector(onGetBackCameraSetting:)])
        {
            [self.GetBackCameraSettingDelegate onGetBackCameraSetting:back];
        }
    }else if([aStr rangeOfString:@"CMD_ACK_GET_PHOTO_QUALITY"].location != NSNotFound)
    {
        NSMutableArray * quality = [[NSMutableArray alloc] init];
        
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_ACK_GET_PHOTO_QUALITY:"];
        if(head==nil){
            NSLog(@"CMD_ACK_GET_PHOTO_QUALITY ERROR %@\n",aStr);
            return;
        }
        if([head count]< 2){
            NSLog(@"CMD_ACK_GET_PHOTO_QUALITY ERROR %@\n",aStr);
            return;
        }
        
        NSArray * dat= [head[1] componentsSeparatedByString:@":"];
        if(dat==nil){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        if([dat count]< 2){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        
        for(NSInteger i = 0; i< [dat count]-1;i++){
            [quality addObject:dat[i]];
        }
        
        if(self.GetPhotoQualitySettingDelegate==nil)return;
        if([self.GetPhotoQualitySettingDelegate respondsToSelector:@selector(onGetPhotoQualitySetting:)])
        {
            [self.GetPhotoQualitySettingDelegate onGetPhotoQualitySetting:quality];
        }
    }else if([aStr rangeOfString:@"CMD_GET_ACK_FORMAT_STATUS_IDLE"].location != NSNotFound)
    {
        NSNumber * status = [NSNumber numberWithInteger:0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FORMAT_STATUS" object:status];
    }else if([aStr rangeOfString:@"CMD_GET_ACK_FORMAT_STATUS_BUSY"].location != NSNotFound)
    {
        NSNumber * status = [NSNumber numberWithInteger:1];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FORMAT_STATUS" object:status];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_FORMAT_STATUS"];
        });
    }else if([aStr rangeOfString:@"CMD_GET_ACK_FORMAT_STATUS_ERR"].location != NSNotFound){
        NSNumber * status = [NSNumber numberWithInteger:2];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FORMAT_STATUS" object:status];
    }
    else if([aStr rangeOfString:@"CMD_DELSUCCESS"].location != NSNotFound)
    {
        if([_deletelist count] > 0){
            if(self.DeleteFileDelegate==nil)return;
            if([self.DeleteFileDelegate respondsToSelector:@selector(onDeleteFileSuccess:)])
            {
                [self.DeleteFileDelegate onDeleteFileSuccess:_deletelist[0]];
            }
            
            [_deletelist removeObjectAtIndex:0];
        }else{
            NSLog(@"CMD_DELSUCCESS deletelist[0]==nil\n");
            while(1);
        }
        
        if([_deletelist count] > 0){
            if(_deletelist[0]==nil){
                NSLog(@"CMD_DELSUCCESS deletelist[0]==nil\n");
                while(1);
            }
            NTFileObj * fileinfo = _deletelist[0];
            [self SendMsgToCVR:[[NSString alloc] initWithFormat:@"CMD_DELFCAMFILENAME:%@TYPE:%@FORM:%@",fileinfo.name,fileinfo.type,fileinfo.form]];
        }else{
            if(self.DeleteFileDelegate==nil)return;
            if([self.DeleteFileDelegate respondsToSelector:@selector(onDeleteFileFinish)])
            {
                [self.DeleteFileDelegate onDeleteFileFinish];
            }
        }
    }else if([aStr rangeOfString:@"CMD_DELFAULT"].location != NSNotFound)
    {
        if(self.DeleteFileDelegate==nil)return;
        if([self.DeleteFileDelegate respondsToSelector:@selector(onDeleteFileFault:)])
        {
            [self.DeleteFileDelegate onDeleteFileFault:_deletelist[0]];
        }
    }else if([aStr rangeOfString:@"CMD_ACK_GET_Fragment_Thumbnail"].location != NSNotFound)
    {
        NSLog(@"CMD_ACK_GET_Fragment_Thumbnail:%@\n", aStr);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GET_Fragment_Thumbnail" object:aStr];
    }else if([aStr rangeOfString:@"CMD_ACK_GETCAMFILE_FINISH"].location != NSNotFound)
    {
        NSLog(@"CMD_ACK_GETCAMFILE_FINISH\n");
        if(_type_index==1){
            _type_index = 2;
            [self SendMsgToCVR:@"CMD_GETFCAMFILETYPE:normal"];
        }else if(_type_index==2){
            _type_index = 0;
            [self SendMsgToCVR:@"CMD_GETFCAMFILETYPE:lock"];
        }else{
            _type_index = 0;
        }
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"CMD_ACK_GETCAMFILE_FINISH" object:aStr];
    }else if([aStr rangeOfString:@"CMD_ACK_GET_DEBUG_ARGSETTING"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CMD_ACK_GET_DEBUG_ARGSETTING" object:aStr];
    }else if([aStr rangeOfString:@"CMD_ACK_GET_Control_Recording_IDLE"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRecordingState" object:@"idle"];
    }else if([aStr rangeOfString:@"CMD_ACK_GET_Control_Recording_BUSY"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRecordingState" object:@"busy"];
    }else if([aStr rangeOfString:@"CMD_ACK_OTA_Upload_File"].location != NSNotFound)
    {
        [self startUploadFile:[NSString stringWithFormat: @"%@/Documents/%@", NSHomeDirectory(), @"Firmware.img"]];
    }else if([aStr rangeOfString:@"CMD_ACK_OTA_Upload_Success"].location != NSNotFound)
    {
        [self SendMsgToCVR:@"CMD_OTA_Update"];
    }
    else if([aStr rangeOfString:@"CMD_CB_DVS_CALIB:"].location != NSNotFound)
    {
        NSLog(@"%@\n",aStr);
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_CB_DVS_CALIB:"];
        if(head==nil){
            return;
        }
        if([head count]< 2){
            return;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DVS_CALIB" object:head[1]];
    }
    else if([aStr rangeOfString:@"CMD_CB_StartRec"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRecordingState" object:@"busy"];
    }
    else if([aStr rangeOfString:@"CMD_CB_StopRec"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetRecordingState" object:@"idle"];
    }
    else if([aStr rangeOfString:@"CMD_CB_GET_MODE"].location != NSNotFound)
    {
        NSLog(@"%@\n",aStr);
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_CB_GET_MODE"];
        if(head==nil){
            return;
        }
        if([head count]< 2){
            return;
        }
        
        NSArray * dat= [head[1] componentsSeparatedByString:@":"];
        if(dat==nil){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ModeChange" object:dat[1]];
    }
    else if([aStr rangeOfString:@"CMD_CB_GPS_UPDATA"].location != NSNotFound)
    {
        NSArray * head= [aStr componentsSeparatedByString:@"CMD_CB_GPS_UPDATA"];
        if(head==nil){
            return;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gpsUpdata" object:head[1]];
    }
    else if([aStr rangeOfString:@"CMD_CB_"].location != NSNotFound)
    {
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSArray * Head= [aStr componentsSeparatedByString:@"CMD_CB_"];
        if(Head==nil){
            return;
        }
        if([Head count]< 2){
            return;
        }
        
        NSArray * dat= [Head[1] componentsSeparatedByString:@":"];
        if(dat==nil){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        if([dat count]< 2){
            NSLog(@"dat ERROR %@\n",aStr);
            return;
        }
        
        [userDefaults setObject:dat[1] forKey:[dat[0] stringByAppendingString:@":"]];
        
        if(self.GetSettingDelegate==nil)return;
        if([self.GetSettingDelegate respondsToSelector:@selector(onGetSettingCVR:)])
        {
            [self.GetSettingDelegate onGetSettingCVR:nil];
        }
    }else if([aStr rangeOfString:@"CMD_ACK_GET_ALL_GPS_END"].location != NSNotFound){
    
    }else if([aStr rangeOfString:@"CMD_ACK_GET_ALL_GPS_BREAK"].location != NSNotFound){
        if(self.GetDirectoryContentsDelegate==nil)return;
        if([self.GetDirectoryContentsDelegate respondsToSelector:@selector(onGPSGetAllBreak:)])
        {
            [self.GetDirectoryContentsDelegate onGPSGetAllBreak:[_allgpslist copy]];
        }
        
        [self SendMsgToCVR:@"CMD_NEXTFILE"];
    }
    else if([aStr rangeOfString:@"CMD_ACK_GET_ALL_GPS"].location != NSNotFound){
        NSArray * Head= [aStr componentsSeparatedByString:@"CMD_ACK_GET_ALL_GPS:"];
        if(Head==nil){
            return;
        }
        if(Head[1]==nil){
            return;
        }
        
        [_allgpslist addObject:Head[1]];
        [self SendMsgToCVR:@"CMD_NEXTFILE"];
    }
    else if([aStr rangeOfString:@"CMD_ACK_GET_GPS_LIST_END"].location != NSNotFound)
    {
        GPSObject * obj = [[GPSObject alloc] init];
        NSArray * Head= [aStr componentsSeparatedByString:@"CMD_ACK_GET_GPS_LIST_END:"];
        if(Head==nil){
            return;
        }
     
        obj.list = self.gpslist;
        obj.gps = Head[1];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetGpsList" object:obj];
    }
    else if([aStr rangeOfString:@"CMD_ACK_GET_GPS_LIST:"].location != NSNotFound)
    {
        NSArray * Head= [aStr componentsSeparatedByString:@"CMD_ACK_GET_GPS_LIST:"];
        if(Head==nil){
            return;
        }
        [self.gpslist addObject:Head[1]];
        [self SendMsgToCVR:@"CMD_NEXTFILE"];
    }
    else if([aStr rangeOfString:@"CMD_ACK_START_RTSP_LIVE"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onAckLiveStart" object:nil];
    }else if([aStr rangeOfString:@"CMD_ACK_GET_VIDEO_LIST:"].location != NSNotFound)
    {
        NSArray * Head= [aStr componentsSeparatedByString:@"CMD_ACK_GET_VIDEO_LIST:"];
        if(Head==nil){
            return;
        }
        
        NSArray * arg= [Head[1] componentsSeparatedByString:@"&"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onGetVideoList" object:arg];
    }else if([aStr rangeOfString:@"CMD_ACK_SET_LIVE_VIDEO"].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onGetLiveVideo" object:nil];
    }
}

- (void)deleteFile:(NSMutableArray *)delNames
{
    if(delNames.count > 0){
        _deletelist = delNames;
        if(delNames[0]==nil){
            NSLog(@"wifi delname[0]==nil\n");
            while(1);
        }
        NTFileObj * fileinfo = delNames[0];
        NSLog(@"%@\n",fileinfo.name);
        [self SendMsgToCVR:[[NSString alloc] initWithFormat:@"CMD_DELFCAMFILENAME:%@TYPE:%@FORM:%@",fileinfo.name,fileinfo.type,fileinfo.form]];
        
        /*
         _isDelete = NO;
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
         if(_isDelete == NO){
         [self SendMsgToCVR:[[NSString alloc] initWithFormat:@"CMD_DELFCAMFILENAME:%@TYPE:%@FORM:%@",fileinfo.name,fileinfo.type,fileinfo.form]];
         }
         });
         */
    }
}

- (NSError *)startUploadFile:(NSString *)path
{
    if(self.asyncUploadSocket!=nil){
        [self.asyncUploadSocket disconnect];
        self.asyncUploadSocket = nil;
    }
    
    NSData * data = [NSData dataWithContentsOfFile:[NSString stringWithFormat: @"%@/Documents/%@", NSHomeDirectory(), @"Firmware.img"]];
    self.asyncUploadSocket  = [[AsyncSocket alloc] initWithDelegate:self];
    
    NSError *error = nil;
    NSLog(@"connecthost:%@\n",self.asyncSocket.connectedHost);
    [self.asyncUploadSocket connectToHost:self.asyncSocket.connectedHost onPort:18890 withTimeout:-1 error:&error];
    if(error!=nil){
        NSLog(@"error:%@\n",error);
        return error;
    }
    
    [self.asyncUploadSocket writeData:data withTimeout:-1 tag:100];
    
    return nil;
}

- (NSError *)UploadFile:(NSString *)path
{
    NSData * data = [NSData dataWithContentsOfFile:[NSString stringWithFormat: @"%@/Documents/%@", NSHomeDirectory(), @"Firmware.img"]];
    //NSData * data = [@"a" dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned long int crc32 = [self crc32:[data bytes] length:data.length];
    
    NSString * cmd = [NSString stringWithFormat:@"CMD_OTA_Upload_FileCRC:%x",crc32];
    NSLog(@"%@  %zi\n",cmd, data.length);
    [self SendMsgToCVR:cmd];
    
    return nil;
}

- (void)GetDirectoryContents
{
    _type_index = 1;
    [self SendMsgToCVR:@"CMD_GETFCAMFILETYPE:picture"];
}

- (void)GetThumbContents
{
    [self SendMsgToCVR:@"CMD_GET_Fragment_Thumbnail"];
}

- (void)GetGpsContents:(NSString *)file
{
    [self.gpslist removeAllObjects];
    
    NSString * cmd = [NSString stringWithFormat:@"CMD_GET_GPS_LIST:%@",file];
    [self SendMsgToCVR:cmd];
}

- (void)DiscoverCVR
{
    [self broadcast_send:@"CMD_DISCOVER"];
}

- (id)init
{
    if(self=[super init]){
        [self broadcast_init];
    }
    return self;
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    if(tag==100){
        [self.asyncUploadSocket disconnect];
        self.asyncUploadSocket = nil;
    }
}

- (void)onWiFiConnectionDisconnect
{
    if(self.Vc==nil)
        return;
    
    NSString * Title;
    NSString * action1;
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"连接已断开!ps:ios exit退出不干净，注意清除后台进程。";
        action1 = @"退出";
    }else {
        Title = @"connection dropped! ps:ios exit退出不干净，注意清除后台进程。";
        action1 = @"exit";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [self.Vc presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                      {
                          exit(0);
                      }]];
}

- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"onWiFiConnectionDisconnect" object:err];
    NSLog(@"断开连接 %@\n",err);
    [self onWiFiConnectionDisconnect];
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"onWiFiConnectionDisconnect" object:nil];
    NSLog(@"断开连接\n");
    [self onWiFiConnectionDisconnect];
}
@end


