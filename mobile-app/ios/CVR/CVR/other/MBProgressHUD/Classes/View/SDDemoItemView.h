#import <UIKit/UIKit.h>
#import "SDBaseProgressView.h"

@interface SDDemoItemView : UIView

@property (nonatomic, assign) Class progressViewClass;

@property (nonatomic, strong) SDBaseProgressView *progressView;

+ (id)demoItemViewWithClass:(Class)class;

@end
