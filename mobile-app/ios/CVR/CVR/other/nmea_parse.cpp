/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "nmea_parse.h"

using namespace std;

static void analysis_gsv(int nmea_count,
                         char *nmea_data,
                         struct gps_info *info)
{
    switch (nmea_count) {
    case GPS_DATA3:
        info->satellites = atoi(nmea_data);
        GPS_DBG("satellites: %d\n", info->satellites);
        break;

    default:
        break;
    }
}

static void analysis_gga(int nmea_count,
                         char *nmea_data,
                         struct gps_info *info)
{
    int date;

    switch (nmea_count) {
    case GPS_DATA1:
        date = atoi(nmea_data);
        info->date.sec = date % 100;
        info->date.min = (date / 100) % 100;

        /* the calibration time zone, Beijing time */
        info->date.hour = date / 10000 + 8;

        GPS_DBG("hour-min-sec: %d-%d-%d\n", info->date.hour,
                info->date.min, info->date.sec);
        break;

    case GPS_DATA2:
        info->latitude = atof(nmea_data);
        GPS_DBG("latitude: %f\n", info->latitude);
        break;

    case GPS_DATA3:
        info->latitude_indicator = nmea_data[0];
        GPS_DBG("latitude_indicator: %c\n", info->latitude_indicator);
        break;

    case GPS_DATA4:
        info->longitude = atof(nmea_data);
        GPS_DBG("longitude: %f\n", info->longitude);
        break;

    case GPS_DATA5:
        info->longitude_indicator = nmea_data[0];
        GPS_DBG("longitude_indicator: %c\n", info->longitude_indicator);
        break;

    case GPS_DATA6:
        info->position_fix_indicator = atoi(nmea_data);
        GPS_DBG("position_fix_indicator: %d\n", info->position_fix_indicator);
        break;

    case GPS_DATA7:
        info->satellites = atoi(nmea_data);
        GPS_DBG("satellites: %d\n", info->satellites);
        break;

    case GPS_DATA9:
        info->altitude = atof(nmea_data);
        GPS_DBG("altitude: %f\n", info->altitude);
        break;

    case GPS_DATA8:
    case GPS_DATA10:
    case GPS_DATA11:
    case GPS_DATA12:
    case GPS_DATA13:
    case GPS_DATA14:
        break;

    default:
        break;
    }
}

static void analysis_rmc(int nmea_count,
                         char *nmea_data,
                         struct gps_info *info)
{
    int date;

    switch (nmea_count) {
    case GPS_DATA1:
        date = atoi(nmea_data);
        info->date.sec = date % 100;
        info->date.min = (date / 100) % 100;

        /* the calibration time zone, Beijing time */
        info->date.hour = date / 10000 + 8;

        GPS_DBG("hour-min-sec: %d-%d-%d\n", info->date.hour,
                info->date.min, info->date.sec);
        break;

    case GPS_DATA2:
        info->status = nmea_data[0];
        GPS_DBG("status: %c\n", info->status);
        break;

    case GPS_DATA3:
        info->latitude = atof(nmea_data);
        GPS_DBG("latitude: %f\n", info->latitude);
        break;

    case GPS_DATA4:
        info->latitude_indicator = nmea_data[0];
        GPS_DBG("latitude_indicator: %c\n", info->latitude_indicator);
        break;

    case GPS_DATA5:
        info->longitude = atof(nmea_data);
        GPS_DBG("longitude: %f\n", info->longitude);
        break;

    case GPS_DATA6:
        info->longitude_indicator = nmea_data[0];
        GPS_DBG("longitude_indicator: %c\n", info->longitude_indicator);
        break;

    case GPS_DATA7:
        info->speed = atof(nmea_data);

        /* nmi/h converted to km/h */
        info->speed = (info->speed * NAUTICAL_MILE) / 1000;
        GPS_DBG("speed: %f\n", info->speed);
        break;

    case GPS_DATA8:
        info->direction = atof(nmea_data);
        GPS_DBG("direction: %f\n", info->direction);
        break;

    case GPS_DATA9:
        date = atoi(nmea_data);
        info->date.years = date % 100 + 100 + 1900;
        info->date.months = (date / 100) % 100;
        info->date.day = date / 10000;

        GPS_DBG("years-months-day: %d-%d-%d\n", info->date.years,
                info->date.months, info->date.day);
        break;

    case GPS_DATA10:
    case GPS_DATA11:
    case GPS_DATA12:
        break;

    default:
        break;
    }
}

static int analysis_msg_type(int search_type,
                             int nmea_count,
                             char *nmea_data,
                             struct gps_info *info)
{
    switch (search_type) {
    case GPS_RMC:
        if (nmea_count == GPS_DATA2 && nmea_data[0] == '0') {
            printf("gps no locate\n");
            return -1;
        }

        analysis_rmc(nmea_count, nmea_data, info);
        break;

    case GPS_GSV:
        analysis_gsv(nmea_count, nmea_data, info);
        break;

    case GPS_GGA:
        analysis_gga(nmea_count, nmea_data, info);
        break;
    }

    return 0;
}

extern "C" double gps_degree_minute_to_degree(float dm)
{
    double degree = 0.0;
    int d = 0, integer = 0;
    double m = 0.0, decimals = 0.0;

    integer = (int)dm;
    decimals = dm - integer;

    /* ddmm.mmmmm or dddmm.mmmmm */
    d = integer / 100;
    m = integer % 100 + decimals;

    /* 1 degree = 60 minute */
    degree = d + (m / 60);

    return degree;
}

extern "C" int gps_nmea_data_parse(struct gps_info *info,
                                   const char *data,
                                   int search_type,
                                   int len)
{
    int i, ret = -1;
    int nmea_count = 0;
    int data_pos = 0;
    char nmea_data[NMEA_MESSAGE_LEN] = "";

    GPS_DBG("%s, gps len %d\n", __func__, len);
    for (i = 0; i < len; i++)
        GPS_DBG("%c", data[i]);
    GPS_DBG("\n");

    if (len <= NMEA_MESSAGE_ID_LEN) {
        printf("%s, gps data is invalid, len = %d\n", __func__, len);
        return -1;
    }

    for (i = 0; i < len; i++) {
        if (data[i] == ',') {
            nmea_data[0] = '0';
            nmea_data[1] = '\0';
            data_pos = 0;

            ret = analysis_msg_type(search_type, nmea_count, nmea_data, info);
            if (ret < 0)
                break;

            nmea_count++;
            continue;
        }

        if (data[i + 1] == ',') {
            nmea_data[data_pos] = data[i];
            nmea_data[data_pos + 1] = '\0';
            data_pos = 0;

            ret = analysis_msg_type(search_type, nmea_count, nmea_data, info);
            if (ret < 0)
                break;

            nmea_count++;
            i++;
        } else {
            nmea_data[data_pos] = data[i];
            data_pos++;
        }
    }

    return ret;
}

static FILE * fp;
extern "C" void gps_set_data_from_fp(char * path)
{
    fp = fopen(path, "rb+");
    if(fp==NULL){
        printf("open %s fault\n",path);
    }
}

extern "C" void gps_close_data_from_fp(void)
{
    fclose(fp);
    fp = NULL;
}

#if 1
extern "C" int gps_get_data_from_fp(struct gps_info *info)
{
    int line = 0, search_type = 0;
    ssize_t read = 0;
    size_t len = GPS_BUF_LEN;
    char *data = NULL;

    GPS_DBG("%s\n", __func__);

    if (fp == NULL) {
        printf("%s: get gps file fp failed\n", __func__);
        return -1;
    }

    if ((data = (char *)malloc(len)) == NULL) {
        printf("%s: no memory!\n", __func__);
        return -1;
    }

    memset(info, 0, sizeof(struct gps_info));

    /* read 2 line, rmc info and gga info. */
    while (line < 2) {
        memset(data, 0, GPS_BUF_LEN);
        
        if ((read = getline(&data, &len, fp)) != -1) {
            if (!strncmp(data, GPS_XXRMC, strlen(GPS_XXRMC))) {
                search_type = GPS_RMC;
            } else if (!strncmp(data, GPS_XXGGA, strlen(GPS_XXGGA))) {
                search_type = GPS_GGA;
            }
            else if (!strncmp(data, "$GPSENDTIME:", strlen("$GPSENDTIME:"))) {
                return 0;
            }
            else {
                printf("%s: gps data error, %s\n", __func__, data);
                continue;
            }

            gps_nmea_data_parse(info, data, search_type, read);

            line++;
        } else {
            printf("%s: End of the gps file\n", __func__);
            free(data);
            return 0;
        }
    }

    free(data);
    return 1;
}
#endif
