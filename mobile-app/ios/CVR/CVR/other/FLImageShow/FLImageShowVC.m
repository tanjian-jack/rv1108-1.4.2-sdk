//
//  FLImageShowVC.m
//  FLImageShowDemo
//
//  Created by fuliang on 16/2/25.
//  Copyright © 2016年 fuliang. All rights reserved.
//

#import "FLImageShowVC.h"
#import "FLImageShowCell.h"

typedef enum : NSUInteger {
    FLImageShowTypeLocal,
    FLImageShowTypeNet,
    FLImageShowTypeAlbum,
    FLImageShowTypeError,
} ImageType;

@interface FLImageShowVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (nonatomic,assign)ImageType imageType;
@property (strong, nonatomic) UICollectionViewFlowLayout *layout;
/**
 是否第一次进入到此界面
 */
@property (nonatomic,assign)BOOL isFirst;
@end

@implementation FLImageShowVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _isFirst = YES;
    //添加检测旋转通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotWithIentationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    //ui设置
    _topView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    //根据数组得出图片类型
    if (_localImageNamesArray.count > 0)
    {
        _imageType = FLImageShowTypeLocal;
        _topLabel.text = [NSString stringWithFormat:@"%ld/%lu",_currentIndex + 1, (unsigned long)_localImageNamesArray.count];
    }
    else if (_netImageUrlsArray.count > 0)
    {
        _imageType = FLImageShowTypeNet;
        _topLabel.text = [NSString stringWithFormat:@"%ld/%lu",_currentIndex + 1, (unsigned long)_netImageUrlsArray.count];
    }
    else if (_albumImageUrlArray.count > 0)
    {
        _imageType = FLImageShowTypeAlbum;
        _topLabel.text = [NSString stringWithFormat:@"%ld/%lu",_currentIndex + 1, (unsigned long)_albumImageUrlArray.count];
    }
    else
    {
        _imageType = FLImageShowTypeError;
        _topLabel.text = @"0/0";
    }
    
    [_myCollectionView registerNib:[UINib nibWithNibName:@"FLImageShowCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"FLImageShowCell"];

    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenheight = [UIScreen mainScreen].bounds.size.height;

    _myCollectionView.pagingEnabled = YES;
    _layout = [[UICollectionViewFlowLayout alloc] init];
    _layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _layout.itemSize = CGSizeMake(screenWidth, screenheight);
    _layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 10);
    _layout.minimumInteritemSpacing = 0;
    _layout.minimumLineSpacing = 10;
    _myCollectionView.collectionViewLayout = _layout;

    _myCollectionView.showsVerticalScrollIndicator = NO;
    _myCollectionView.showsHorizontalScrollIndicator = NO;

    //添加手势
    UITapGestureRecognizer *singleViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleViewTapAction)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:singleViewTap];
    
    UITapGestureRecognizer *doubleViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleViewTapAction:)];
    doubleViewTap.numberOfTapsRequired = 2;
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:doubleViewTap];
    
    [singleViewTap requireGestureRecognizerToFail:doubleViewTap];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
}

- (void)viewDidLayoutSubviews
{
    if (_isFirst)
    {
        //滚动到当前图片位置
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_currentIndex inSection:0];
        [_myCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        _isFirst = NO;
    }
    
}

#pragma mark--旋转
/**
 *  旋转self.view
 */
- (void)rotateView
{
    CGFloat mainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat mainScreenHeight = [UIScreen mainScreen].bounds.size.height;
    
    UIDeviceOrientation orient = [UIDevice currentDevice].orientation;
    switch (orient)
    {
        case UIDeviceOrientationPortrait:
        {
            _layout.itemSize = CGSizeMake(mainScreenWidth, mainScreenHeight);
            break;
        }
        case UIDeviceOrientationLandscapeLeft:
        {
            _layout.itemSize = CGSizeMake(mainScreenWidth, mainScreenHeight);
            break;
        }
        case UIDeviceOrientationLandscapeRight:
        {
            _layout.itemSize = CGSizeMake(mainScreenWidth, mainScreenHeight);
            break;
        }
        default:
            break;
    }
}
#pragma mark--通知
- (void)getNotWithIentationChange:(NSNotification *)not
{
    [self rotateView];
}
#pragma mark--点击
- (void)singleViewTapAction
{
    _topView.hidden = !_topView.hidden;
}
- (void)doubleViewTapAction:(UITapGestureRecognizer *)tap
{
    FLImageShowCell *cell = (FLImageShowCell *)[_myCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:_currentIndex inSection:0]];
    CGFloat newScale = cell.scrollView.zoomScale == 1 ? cell.scrollView.maximumZoomScale : 1;
    CGFloat newWidth = cell.scrollView.frame.size.width / newScale;
    CGFloat newHeight = cell.scrollView.frame.size.height / newScale;
    CGPoint point = [tap locationInView:cell.scrollView];
    CGRect zoomRect = CGRectMake(point.x - newWidth / 2, point.y - newHeight / 2, newWidth, newHeight);
    [cell.scrollView zoomToRect:zoomRect animated:YES];
}

- (IBAction)topLeftBtnClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark--UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    _currentIndex = scrollView.contentOffset.x / screenWidth + 0.5;
    NSInteger allCount = 0;
    switch (_imageType)
    {
        case FLImageShowTypeLocal:
        {
            allCount = _localImageNamesArray.count;
        }
            break;
        case FLImageShowTypeNet:
        {
            allCount = _netImageUrlsArray.count;
        }
            break;
        case FLImageShowTypeAlbum:
        {
            allCount = _albumImageUrlArray.count;
        }
            break;
            
        default:
        {
            allCount = 0;
        }
            break;
    }
    _topLabel.text = [NSString stringWithFormat:@"%ld/%ld",_currentIndex + 1, allCount];
}

#pragma mark--UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (_imageType)
    {
        case FLImageShowTypeLocal:
        {
            return _localImageNamesArray.count;
        }
            break;
        case FLImageShowTypeNet:
        {
            return _netImageUrlsArray.count;
        }
            break;
        case FLImageShowTypeAlbum:
        {
            return _albumImageUrlArray.count;
        }
            break;
            
        default:
        {
            return 0;
        }
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FLImageShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FLImageShowCell" forIndexPath:indexPath];
    cell.scrollView.zoomScale = 1.0;
    switch (_imageType)
    {
        case FLImageShowTypeLocal:
        {
            cell.localImageName = _localImageNamesArray[indexPath.row];
        }
        break;
            
        case FLImageShowTypeNet:
        {
            cell.netImageUrl = _netImageUrlsArray[indexPath.row];
        }
        break;
            
        case FLImageShowTypeAlbum:
        {
            cell.albumImageUrl = _albumImageUrlArray[indexPath.row];
        }
            break;
            
        default:
            return nil;
    }
    return cell;
}

#if 0
- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#endif
@end
