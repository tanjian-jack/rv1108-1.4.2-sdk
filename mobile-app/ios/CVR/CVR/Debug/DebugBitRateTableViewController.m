//
//  DebugBitRateTableViewController.m
//  CVR
//
//  Created by rk on 12/12/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "DebugBitRateTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface DebugBitRateTableViewController ()
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@end

@implementation DebugBitRateTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"bit_rate_per_pixel"];
    if([str isEqualToString:@"1"]){
        _sel = 0;
    }else if([str isEqualToString:@"2"]){
        _sel = 1;
    }else if([str isEqualToString:@"4"]){
        _sel = 2;
    }else if([str isEqualToString:@"6"]){
        _sel = 3;
    }else if([str isEqualToString:@"8"]){
        _sel = 4;
    }else if([str isEqualToString:@"10"]){
        _sel = 5;
    }else if([str isEqualToString:@"12"]){
        _sel = 6;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    if(indexPath.section==0){
        if(indexPath.row== 0){
            cell.textLabel.text = @"1";
        }else if(indexPath.row== 1){
            cell.textLabel.text = @"2";
        }else if(indexPath.row== 2){
            cell.textLabel.text = @"4";
        }else if(indexPath.row== 3){
            cell.textLabel.text = @"6";
        }else if(indexPath.row== 4){
            cell.textLabel.text = @"8";
        }else if(indexPath.row== 5){
            cell.textLabel.text = @"10";
        }else if(indexPath.row== 6){
            cell.textLabel.text = @"12";
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row== 0 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"1" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:1"];
    }else if( indexPath.row== 1 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"2" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:2"];
    }else if( indexPath.row== 2 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"4" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:4"];
    }else if( indexPath.row== 3 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"6" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:6"];
    }else if( indexPath.row== 4 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"8" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:8"];
    }else if( indexPath.row== 5 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"10" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:10"];
    }else if( indexPath.row== 6 && indexPath.section==0 )
    {
        [_userDefaults setObject:@"12" forKey:@"bit_rate_per_pixel"];
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_bit_rate_per_pixel:12"];
    }
    _sel = indexPath.row;
    [self.tableView reloadData];
}
@end
