//
//  DebugTableViewController.m
//  CVR
//
//  Created by rk on 12/9/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "DebugTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import "DebugBitRateTableViewController.h"

@interface DebugTableViewController ()
@property (strong, nonatomic) UISwitch * switch_reboot;
@property (strong, nonatomic) UISwitch * switch_revoery;
@property (strong, nonatomic) UISwitch * switch_awake;
@property (strong, nonatomic) UISwitch * switch_standby;
@property (strong, nonatomic) UISwitch * switch_mode_change;
@property (strong, nonatomic) UISwitch * switch_video;
@property (strong, nonatomic) UISwitch * switch_beg_end_video;
@property (strong, nonatomic) UISwitch * switch_photo;
@property (strong, nonatomic) UISwitch * switch_temp_control;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@end

@implementation DebugTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _switch_reboot = [[UISwitch alloc] init];
    [_switch_reboot addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_revoery = [[UISwitch alloc] init];
    [_switch_revoery addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_awake = [[UISwitch alloc] init];
    [_switch_awake addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_standby = [[UISwitch alloc] init];
    [_switch_standby addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_mode_change = [[UISwitch alloc] init];
    [_switch_mode_change addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_video = [[UISwitch alloc] init];
    [_switch_video addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];

    _switch_beg_end_video = [[UISwitch alloc] init];
    [_switch_beg_end_video addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_photo = [[UISwitch alloc] init];
    [_switch_photo addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_temp_control = [[UISwitch alloc] init];
    [_switch_temp_control addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(doGetDebugSetting:) name:@"CMD_ACK_GET_DEBUG_ARGSETTING" object:nil];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_DEBUG_ARGSETTING"];
}


- (void)doGetDebugSetting:(NSNotification*) notification
{
    int i = 0;
    NSString * aStr= [notification object];
    NSMutableArray * mutable = [WiFiConnection analysisStringFromString:aStr :@[@"reboot:",@"recovery:",@"awake:",@"standby:",@"mode_change:",@"debug_video:",@"begin_end_video:",@"photo:",@"temp_control:",@"temp_video_bit_rate_per_pixel:"]];
    NSLog(@"%@\n",mutable);
    if( mutable.count != 11)return;
    
    for(NSString * s in mutable){
        switch (i++) {
            case 1:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_reboot setOn:NO];
                }else{
                    [_switch_reboot setOn:YES];
                }
                break;
            case 2:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_revoery setOn:NO];
                }else{
                    [_switch_revoery setOn:YES];
                }
                break;
            case 3:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_awake setOn:NO];
                }else{
                    [_switch_awake setOn:YES];
                }
                break;
            case 4:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_standby setOn:NO];
                }else{
                    [_switch_standby setOn:YES];
                }
                break;
            case 5:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_mode_change setOn:NO];
                }else{
                    [_switch_mode_change setOn:YES];
                }
                break;
            case 6:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_video setOn:NO];
                }else{
                    [_switch_video setOn:YES];
                }
                break;
            case 7:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_beg_end_video setOn:NO];
                }else{
                    [_switch_beg_end_video setOn:YES];
                }
                break;
            case 8:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_photo setOn:NO];
                }else{
                    [_switch_photo setOn:YES];
                }
                break;
            case 9:
                if( [s isEqualToString:@"on"]==0 ){
                    [_switch_temp_control setOn:NO];
                }else{
                    [_switch_temp_control setOn:YES];
                }
                break;
                
            case 10:
                [_userDefaults setObject:s forKey:@"bit_rate_per_pixel"];
                break;
                
            default:
                break;
        }
    }
    [self.tableView reloadData];
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];

    if(switchButton==_switch_reboot)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_reboot:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_reboot:off"];
        }
    }
    else if(switchButton==_switch_revoery)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_recovery:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_recovery:off"];
        }
    }
    else if(switchButton==_switch_awake)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_awake:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_awake:off"];
        }
    }
    else if(switchButton==_switch_standby)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_standby:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_standby:off"];
        }
    }
    else if(switchButton==_switch_mode_change)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_mode_change:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_mode_change:off"];
        }
    }
    else if(switchButton==_switch_video)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_video:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_video:off"];
        }
    }
    else if(switchButton==_switch_beg_end_video)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_beg_end_video:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_beg_end_video:off"];
        }
    }
    else if(switchButton==_switch_photo)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_photo:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_photo:off"];
        }
    }
    else if(switchButton==_switch_temp_control)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_temp_control:on"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_DEBUG_ARGSETTINGdebug_temp_control:off"];
        }
    }
//    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_DEBUG_ARGSETTING"];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    if(indexPath.section==0){
        if(indexPath.row== 0){
            cell.accessoryView = _switch_reboot;
            cell.textLabel.text = @"reboot";
        }else if(indexPath.row== 1){
            cell.accessoryView = _switch_revoery;
            cell.textLabel.text = @"revoery";
        }else if(indexPath.row== 2){
            cell.accessoryView = _switch_awake;
            cell.textLabel.text = @"awake";
        }else if(indexPath.row== 3){
            cell.accessoryView = _switch_standby;
            cell.textLabel.text = @"standby";
        }else if(indexPath.row== 4){
            cell.accessoryView = _switch_mode_change;
            cell.textLabel.text = @"mode_change";
        }else if(indexPath.row== 5){
            cell.accessoryView = _switch_video;
            cell.textLabel.text = @"video";
        }else if(indexPath.row== 6){
            cell.accessoryView = _switch_beg_end_video;
            cell.textLabel.text = @"beg_end_video";
        }else if(indexPath.row== 7){
            cell.accessoryView = _switch_photo;
            cell.textLabel.text = @"photo";
        }else if(indexPath.row== 8){
            cell.accessoryView = _switch_temp_control;
            cell.textLabel.text = @"temp_control";
        }else if(indexPath.row== 9){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"bit_rate_per_pixel";
            cell.detailTextLabel.text = [_userDefaults objectForKey:@"bit_rate_per_pixel"];

        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row== 9 && indexPath.section==0 )
    {
        DebugBitRateTableViewController * Vc = [[DebugBitRateTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
}
@end
