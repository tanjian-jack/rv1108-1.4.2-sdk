//
//  TableCellField.h
//  CVR
//
//  Created by 雷起斌 on 7/24/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellField : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UITextField *content;
@property (strong, nonatomic) IBOutlet UILabel *passwd;
@end
