//
//  TimeLapseConfigController.m
//  CVR
//
//  Created by rk on 25/05/2017.
//  Copyright © 2017 雷起斌. All rights reserved.
//

#import "TimeLapseConfigController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface TimeLapseConfigController ()<GetSettingDelegate>
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@end

@implementation TimeLapseConfigController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"TIME_LAPSE:"];
    if([str isEqualToString:@"OFF"]==YES){
        _sel = 0;
    }else if([str isEqualToString:@"1"]==YES){
        _sel = 1;
    }else if([str isEqualToString:@"5"]==YES){
        _sel = 2;
    }else if([str isEqualToString:@"10"]==YES){
        _sel = 3;
    }else if([str isEqualToString:@"30"]==YES){
        _sel = 4;
    }else if([str isEqualToString:@"60"]==YES){
        _sel = 5;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 6;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            if(_language==1)
                cell.textLabel.text= @"关";
            else
                cell.textLabel.text= @"OFF";
        }else if(indexPath.row== 1){
            cell.textLabel.text= @"1";
        }else if(indexPath.row== 2){
            cell.textLabel.text= @"5";
        }else if(indexPath.row== 3){
            cell.textLabel.text= @"10";
        }else if(indexPath.row== 4){
            cell.textLabel.text= @"30";
        }else if(indexPath.row== 5){
            cell.textLabel.text= @"60";
        }
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    if(indexPath.row==0){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:OFF"];
    }else if(indexPath.row==1){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:1"];
    }else if(indexPath.row==2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:5"];
    }else if(indexPath.row==3){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:10"];
    }else if(indexPath.row==4){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:30"];
    }else if(indexPath.row==5){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGTimeLapse:60"];
    }
    [tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    NSString * str =  [_userDefaults objectForKey:@"TIME_LAPSE:"];
    if([str isEqualToString:@"OFF"]==YES){
        _sel = 0;
    }else if([str isEqualToString:@"1"]==YES){
        _sel = 1;
    }else if([str isEqualToString:@"5"]==YES){
        _sel = 2;
    }else if([str isEqualToString:@"10"]==YES){
        _sel = 3;
    }else if([str isEqualToString:@"30"]==YES){
        _sel = 4;
    }else if([str isEqualToString:@"60"]==YES){
        _sel = 5;
    }
    [self.tableView reloadData];
}

@end
