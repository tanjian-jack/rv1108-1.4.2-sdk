//
//  VideoLengthConfigController.m
//  CVR
//
//  Created by 雷起斌 on 10/11/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "VideoLengthConfigController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface VideoLengthConfigController () <GetSettingDelegate>
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@end

@implementation VideoLengthConfigController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"Videolength:"];
    if([str isEqualToString:@"1min"]){
        _sel = 0;
    }else if([str isEqualToString:@"3min"]){
        _sel = 1;
    }else if([str isEqualToString:@"5min"]){
        _sel = 2;
    }else if([str isEqualToString:@"infinite"]){
        _sel = 3;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 4;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            cell.textLabel.text= @"1min";
        }else if(indexPath.row== 1){
            cell.textLabel.text= @"3min";
        }else if(indexPath.row== 2){
            cell.textLabel.text= @"5min";
        }else if(indexPath.row== 3){
            cell.textLabel.text= @"infinite";
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    if(indexPath.row==0){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGVideolength:1min"];
    }else if(indexPath.row==1){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGVideolength:3min"];
    }else if(indexPath.row==2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGVideolength:5min"];
    }else if(indexPath.row==3){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGVideolength:infinite"];
    }
    [tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    NSString * str =  [_userDefaults objectForKey:@"Videolength:"];
    if([str isEqualToString:@"1min"]){
        _sel = 0;
    }else if([str isEqualToString:@"3min"]){
        _sel = 1;
    }else if([str isEqualToString:@"5min"]){
        _sel = 2;
    }else if([str isEqualToString:@"infinite"]){
        _sel = 3;
    }
    [self.tableView reloadData];
}
@end
