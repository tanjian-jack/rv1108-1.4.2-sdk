//
//  PhotoResolutionController.m
//  CVR
//
//  Created by rk on 4/27/17.
//  Copyright © 2017 雷起斌. All rights reserved.
//

#import "PhotoResolutionController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface PhotoResolutionController ()
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@end

@implementation PhotoResolutionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"PhotoResolution:"];
    if([str isEqualToString:@"1M"]){
        _sel = 0;
    }else if([str isEqualToString:@"2M"]){
        _sel = 1;
    }else if([str isEqualToString:@"3M"]){
        _sel = 2;
    }
    self.navigationItem.leftBarButtonItem.title = @" ";
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section== 0){
        return 3;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            cell.textLabel.text= @"1M";
        }else if(indexPath.row== 1){
            cell.textLabel.text= @"2M";
        }else if(indexPath.row== 2){
            cell.textLabel.text= @"3M";
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    
    if(indexPath.row==0){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGPhotoResolution:1M"];
    }else if(indexPath.row==1){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGPhotoResolution:2M"];
    }else if(indexPath.row==2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGPhotoResolution:3M"];
    }
    
    [tableView reloadData];
}
@end
