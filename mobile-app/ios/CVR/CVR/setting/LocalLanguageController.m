//
//  LocalLanguageController.m
//  CVR
//
//  Created by 雷起斌 on 10/26/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "LocalLanguageController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface LocalLanguageController ()
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@end

@implementation LocalLanguageController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat R  = (CGFloat) 0;
    CGFloat G = (CGFloat) 89/255.0;
    CGFloat B = (CGFloat) 84/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    self.navigationController.navigationBar.tintColor = myColorRGB;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"English"]==YES){
        _sel = 0;
    }else if([str isEqualToString:@"Chinese"]==YES){
        _sel = 1;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            cell.textLabel.text= @"English";
        }else if(indexPath.row== 1){
            cell.textLabel.text= @"简体中文";
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    if(indexPath.row==0){
        [_userDefaults setObject:@"English" forKey:@"app_language"];
    }else if(indexPath.row==1){
        [_userDefaults setObject:@"Chinese" forKey:@"app_language"];
    }
    [tableView reloadData];
    
    if(_sel==0){
        UITabBarItem *trainEnquiryItem1  = [self.tabBarController.tabBar.items objectAtIndex:0];
        [trainEnquiryItem1 setTitle:@"preview"];
        
        UITabBarItem *trainEnquiryItem2 = [self.tabBarController.tabBar.items objectAtIndex:1];
        [trainEnquiryItem2 setTitle:@"main"];
        
        UITabBarItem *trainEnquiryItem3 = [self.tabBarController.tabBar.items objectAtIndex:2];
        [trainEnquiryItem3 setTitle:@"setting"];
    }else if(_sel==1){
        UITabBarItem *trainEnquiryItem1  = [self.tabBarController.tabBar.items objectAtIndex:0];
        [trainEnquiryItem1 setTitle:@"本地预览"];
        
        UITabBarItem *trainEnquiryItem2 = [self.tabBarController.tabBar.items objectAtIndex:1];
        [trainEnquiryItem2 setTitle:@"主页面"];
        
        UITabBarItem *trainEnquiryItem3 = [self.tabBarController.tabBar.items objectAtIndex:2];
        [trainEnquiryItem3 setTitle:@"设置"];
    }
}

@end
