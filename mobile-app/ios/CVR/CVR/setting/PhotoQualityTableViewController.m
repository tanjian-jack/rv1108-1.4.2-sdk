//
//  PhotoQualityTableViewController.m
//  CVR
//
//  Created by rk on 04/09/2017.
//  Copyright © 2017 雷起斌. All rights reserved.
//

#import "PhotoQualityTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface PhotoQualityTableViewController () <GetPhotoQualitySettingDelegate, GetSettingDelegate>
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (strong, nonatomic) NSMutableArray * Resolve;
@property (assign, nonatomic) NSInteger language;
@end

@implementation PhotoQualityTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [WiFiConnection sharedSingleton].GetPhotoQualitySettingDelegate = self;
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_SETTING_PHOTO_QUALITY"];
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"Photo_Quality:"];
    _sel = [str integerValue];
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onGetPhotoQualitySetting:(NSMutableArray *)arryM
{
    NSLog(@"%@\n",_Resolve);
    _Resolve = arryM;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return [_Resolve count];
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text= _Resolve[indexPath.row];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_PHOTO_QUALITY"];
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    NSString * str =  [_userDefaults objectForKey:@"Photo_Quality:"];
    _sel = [str integerValue];
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;

    NSString * cmd = [NSString stringWithFormat:@"CMD_ARGSETTINGPhotoQuality:%zi",_sel];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:cmd];
    [_userDefaults setObject:[NSString stringWithFormat:@"%zi",_sel] forKey:@"Photo_Quality:"];
    [tableView reloadData];
}

@end
