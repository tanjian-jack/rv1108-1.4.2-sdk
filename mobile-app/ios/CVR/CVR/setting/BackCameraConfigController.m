//
//  BackCameraConfigController.m
//  CVR
//
//  Created by 雷起斌 on 10/11/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "BackCameraConfigController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface BackCameraConfigController ()<GetBackCameraSettingDelegate, GetSettingDelegate>
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (strong, nonatomic) NSMutableArray * Resolve;
@property (assign, nonatomic) NSInteger language;
@end

@implementation BackCameraConfigController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [WiFiConnection sharedSingleton].GetBackCameraSettingDelegate = self;
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_BACK_CAMERARESPLUTION"];
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"BackCamera:"];
    if([str isEqualToString:@"back1"]){
        _sel = 0;
    }else if([str isEqualToString:@"back2"]){
        _sel = 1;
    }else if([str isEqualToString:@"back3"]){
        _sel = 2;
    }else if([str isEqualToString:@"back4"]){
        _sel = 3;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return [_Resolve count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text= _Resolve[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    if(indexPath.row==0){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBack_camera:back1"];
        [_userDefaults setObject:@"back1" forKey:@"BackCamera:"];
    }else if(indexPath.row==1){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBack_camera:back2"];
        [_userDefaults setObject:@"back2" forKey:@"BackCamera:"];
    }
    [tableView reloadData];
}

- (void)onGetBackCameraSetting:(NSMutableArray *)arryM
{
    _Resolve = arryM;
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_BACK_SETTING_RESPLUTION"];
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    NSString * str =  [_userDefaults objectForKey:@"BackCamera:"];
    if([str isEqualToString:@"back1"]){
        _sel = 0;
    }else if([str isEqualToString:@"back2"]){
        _sel = 1;
    }else if([str isEqualToString:@"back3"]){
        _sel = 2;
    }else if([str isEqualToString:@"back4"]){
        _sel = 3;
    }
    [self.tableView reloadData];
}
@end
