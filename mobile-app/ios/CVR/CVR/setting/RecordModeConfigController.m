//
//  RecordModeConfigController.m
//  CVR
//
//  Created by 雷起斌 on 10/11/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "RecordModeConfigController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface RecordModeConfigController () <GetSettingDelegate>
@property (nonatomic, assign) NSInteger sel;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@end

@implementation RecordModeConfigController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str =  [_userDefaults objectForKey:@"RecordMode:"];
    if([str isEqualToString:@"Front"]){
        _sel = 0;
    }else if([str isEqualToString:@"Rear"]){
        _sel = 1;
    }else if([str isEqualToString:@"Double"]){
        _sel = 2;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 3;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(_sel==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Front video";
            else
                cell.textLabel.text= @"前置模式";
        }else if(indexPath.row== 1){
            if(_language==0)
                cell.textLabel.text= @"Rear video";
            else
                cell.textLabel.text= @"后置模式";
        }else if(indexPath.row== 2){
            if(_language==0)
                cell.textLabel.text= @"Double video";
            else
                cell.textLabel.text= @"双录模式";
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _sel = indexPath.row;
    if(indexPath.row==0){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordMode:Front"];
    }else if(indexPath.row==1){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordMode:Rear"];
    }else if(indexPath.row==2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordMode:Double"];
    }
    [tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    NSString * str =  [_userDefaults objectForKey:@"RecordMode:"];
    if([str isEqualToString:@"Front"]){
        _sel = 0;
    }else if([str isEqualToString:@"Rear"]){
        _sel = 1;
    }else if([str isEqualToString:@"Double"]){
        _sel = 2;
    }
    [self.tableView reloadData];
}
@end
