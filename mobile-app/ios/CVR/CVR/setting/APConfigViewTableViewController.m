//
//  APConfigViewTableViewController.m
//  CVR
//
//  Created by 雷起斌 on 7/24/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "APConfigViewTableViewController.h"
#import "TableCellField.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface APConfigViewTableViewController ()<UITableViewDelegate , UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSUserDefaults * userDefaults;

@property (strong, nonatomic) TableCellField * APNameField;
@property (strong, nonatomic) TableCellField * APPasswdField;
@property (strong, nonatomic) TableCellField * STANameField;
@property (strong, nonatomic) TableCellField * STAPasswdField;

@property (strong, nonatomic) NSString *AP_SSID;
@property (strong, nonatomic) NSString *AP_PASSWD;
@property (strong, nonatomic) NSString *STA_SSID;
@property (strong, nonatomic) NSString *STA_PASSWD;

@property (nonatomic, assign) BOOL isStation;
@property (assign, nonatomic) NSInteger language;
@end

@implementation APConfigViewTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
     _userDefaults = [NSUserDefaults standardUserDefaults];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(execute:) name:@"CMD_WIFI_INFO" object:nil];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GETWIFI"];
    
    _isStation = YES;
    
    _APNameField= [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([TableCellField class]) owner:nil options:nil] lastObject];
    [_APNameField.content addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    _APNameField.content.delegate = self;
    
    _STANameField= [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([TableCellField class]) owner:nil options:nil] lastObject];
    [_STANameField.content addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    _STANameField.content.delegate = self;
    
    _APPasswdField= [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([TableCellField class]) owner:nil options:nil] lastObject];
    [_APPasswdField.content addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    _APPasswdField.content.delegate = self;
    
    _STAPasswdField= [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([TableCellField class]) owner:nil options:nil] lastObject];
    [_STAPasswdField.content addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    _STAPasswdField.content.delegate = self;
    
    _STA_SSID = [[UIDevice currentDevice] name];
    _STA_PASSWD = [_userDefaults objectForKey:@"STAPasswd"];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (dismissKeyboard) name: UIKeyboardDidShowNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)dismissKeyboard
{
    [_APNameField.content resignFirstResponder];
    [_STANameField.content resignFirstResponder];
    [_APPasswdField.content resignFirstResponder];
    [_STAPasswdField.content resignFirstResponder];
}

- (void) execute:(NSNotification*) notification
{
    NSString * aStr = [notification object];//通过这个获取到传递的对象
    
    NSArray * arry= [aStr componentsSeparatedByString:@"WIFINAME:"];
    if(arry[1]==nil)return;
    NSArray * arry1= [arry[1] componentsSeparatedByString:@"WIFIPASSWORD:"];
    _AP_SSID = arry1[0];
    if(arry1[1]==nil)return;
    NSArray * arry2= [arry1[1] componentsSeparatedByString:@"MODE:"];
    _AP_PASSWD = arry2[0];

    if(arry2[1]==nil)return;
    NSArray * arry3= [arry2[1] componentsSeparatedByString:@"END"];
    
    NSString * mode= arry3[0];
    _isStation = [mode isEqualToString:@"STA"];
    
    [self.tableView reloadData];
}

- (void)textChange:(UITextField *) TextField
{
    if(TextField==_APNameField.content){
        _AP_SSID = TextField.text;
//        [_userDefaults setObject:_AP_SSID forKey:@"APSSID"];
    }
    else if(TextField==_APPasswdField.content){
        _AP_PASSWD = TextField.text;
        [_userDefaults setObject:_AP_PASSWD forKey:@"APPasswd"];
    }
    else if(TextField==_STANameField.content){
        _STA_SSID = TextField.text;
//        [_userDefaults setObject:_STA_SSID forKey:@"STASSID"];
    }
    else if(TextField==_STAPasswdField.content){
        _STA_PASSWD = TextField.text;
        [_userDefaults setObject:_STA_PASSWD forKey:@"STAPasswd"];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 2;
    }else if (section== 1){
        return 2;
    }else if (section== 2){
        return 2;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==2){
        UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        
        if(indexPath.row==0){
            if(_language==0)
                cell.textLabel.text = @"AP";
            else
                cell.textLabel.text = @"热点模式";
            if(_isStation==NO){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }else{
            if(_language==0)
                cell.textLabel.text = @"STA";
            else
                cell.textLabel.text = @"路由模式";
            if(_isStation==YES){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        return cell;
    }

    if(indexPath.row==0){
        if(indexPath.section==0)
        {
            if(_language==0)
                _APNameField.textLabel.text = @"Device Name";
            else
                _APNameField.textLabel.text= @"设备名称";

            _APNameField.content.text= _AP_SSID;
            return _APNameField;
        }
        else if(indexPath.section==1)
        {
            if(_language==0)
                _STANameField.textLabel.text = @"Device Name";
            else
                _STANameField.textLabel.text= @"设备名称";
            
            _STANameField.content.text= _STA_SSID;
            return _STANameField;
        }
    }
    else if(indexPath.row==1)
    {
        if(indexPath.section==0)
        {
            if(_AP_PASSWD!=nil){
                _APPasswdField.content.text = _AP_PASSWD;
            }
            
            if(_language==0)
                _APPasswdField.textLabel.text = @"Password";
            else
                _APPasswdField.textLabel.text= @"密码";
            return _APPasswdField;
        }
        else
        {
            if(_STA_PASSWD!=nil)
            {
                _STAPasswdField.content.text = _STA_PASSWD;
            }
            
            if(_language==0)
                _STAPasswdField.textLabel.text = @"Password";
            else
                _STAPasswdField.textLabel.text= @"密码";
            return _STAPasswdField;
        }
    }

    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if(section==0){
        if(_language==1){
            return @"用户将使用此密码加入您设备共享的“无线局域网”网络。\n密码必须包含至少8个字符。";
        }else {
            return @"Users will use this password to join your device to share the “wireless network” network.Password must contain at least 8 characters.";
        }
        
    }else if(section==1){
        if(_language==1){
            return @"用户将使用此密码加入您设备共享的“无线局域网”网络。\n密码必须包含至少8个字符。";
        }else {
            return @"Users will use this password to join your device to share the “wireless network” network.Password must contain at least 8 characters.";
        }
    }else{
        if(_language==1){
            return @"更改连接模式将可能导致您的设备和应用程序重启！";
        }else {
            return @"Changing the connection mode will cause your device and application to restart!";
        }
        
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0){
        if(_language==0)
            return @"AP Setting";
        else
            return @"热点设置";
    }else if(section==1){
        if(_language==0)
            return @"STA Setting";
        else
            return @"路由设置";
    }else{
        if(_language==0)
            return @"STA Setting";
        else
            return @"模式选择";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section== 2 ){
        if(indexPath.row==0){
//            if(_isStation!= NO){
                NSString * Title;
                NSString * action1;
                NSString * action2;
                if(_language==1){
                    Title = @"本次操作将导致您的设备重启，是否继续？";
                    action1 = @"确定";
                    action2 = @"取消";
                }else{
                    Title = @"This operation will cause your device to restart, do you want to continue?";
                    action1 = @"YES";
                    action2 = @"NO";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:Title preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                {	
                    NSString * cmd = [NSString stringWithFormat:@"CMD_SETWIFIWIFINAME:%@PASSWD:%@",_AP_SSID,_AP_PASSWD];
                    [[WiFiConnection sharedSingleton] SendMsgToCVR:cmd];
                    _isStation= NO;
                    [self.tableView reloadData];
                    [_userDefaults setBool:NO forKey:@"ConnectMode"];
                    
                    NSString * Title;
                    NSString * action1;
                    NSString * action2;
                    if(_language==1){
                        Title = @"应用程序即将退出，是否继续？";
                        action1 = @"立即退出";
                        action2 = @"取消";
                    }else{
                        Title = @"Application is about to exit, do you want to continue?";
                        action1 = @"Exit immediately";
                        action2 = @"NO";
                    }
                    
                    UIAlertController *subalert = [UIAlertController alertControllerWithTitle:nil message:Title preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:subalert animated:YES completion:nil];
                    [subalert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                    {
                        exit(0);
                    }]];
                    [subalert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
                }]];

                [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
//            }
        }else{
//            if(_isStation!= YES){

                NSString * Title;
                NSString * action1;
                NSString * action2;
                if(_language==1){
                    Title = @"切换模式并重启设备";
                    action1 = @"确定";
                    action2 = @"取消";
                }else{
                    Title = @"Switch mode and restart device";
                    action1 = @"YES";
                    action2 = @"NO";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:Title preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:nil];
            
                [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                {
                    NSString * cmd = [NSString stringWithFormat:@"CMD_APPEARSSID:%@PASSWD:%@",_STA_SSID,_STA_PASSWD];
                    [[WiFiConnection sharedSingleton] SendMsgToCVR:cmd];
                    _isStation= YES;
                    [self.tableView reloadData];
                    [_userDefaults setBool:YES forKey:@"ConnectMode"];
                
                    NSString * Title;
                    NSString * action1;
                    NSString * action2;
                    if(_language==1){
                        Title = @"请确认设备是否重启？";
                        action1 = @"是";
                        action2 = @"否";
                    }else{
                        Title = @"Make sure the device is restarted?";
                        action1 = @"YES";
                        action2 = @"NO";
                    }
       
                    UIAlertController *subalert = [UIAlertController alertControllerWithTitle:nil message:Title preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:subalert animated:YES completion:nil];
                   
                    [subalert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
                        exit(0);
                    }]];
                    
                    [subalert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        NSString * cmd = [NSString stringWithFormat:@"CMD_APPEARSSID:%@PASSWD:%@",_STA_SSID,_STA_PASSWD];
                        [[WiFiConnection sharedSingleton] SendMsgToCVR:cmd];
                        [self presentViewController:subalert animated:YES completion:nil];
                    }]];
                }]];
            
                [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
            }
        }
        
        [self.tableView reloadData];
//    }
}

- (BOOL)shouldAutorotate
{
    //是否支持转屏
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
