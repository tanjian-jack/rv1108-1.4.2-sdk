//
//  SettingView.m
//  UI
//
//  Created by leiqibin on 16-4-11.
//  Copyright (c) 2016年 Rockchip. All rights reserved.
//

#import "SettingView.h"
#import "APConfigViewTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import "WhiteBalanceConfigController.h"
#import "ExposureConfigController.h"
#import "LanguageConfigController.h"
#import "FrequencyConfigController.h"
#import "BrightConfigController.h"
#import "USBMODEConfigController.h"
#import "FontCameraConfigController.h"
#import "BackCameraConfigController.h"
#import "VideoLengthConfigController.h"
#import "RecordModeConfigController.h"
#import "DebugTableViewController.h"
#import "PhotoResolutionController.h"
#import "TimeLapseConfigController.h"
#import "QuaityConfigController.h"
#import "CollisionConfigController.h"
#import "TableCellField.h"
#import "PhotoQualityTableViewController.h"

@interface SettingView () <UITableViewDelegate , UITableViewDataSource, GetSettingDelegate, UITextFieldDelegate, UIApplicationDelegate , UIGestureRecognizerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UISwitch * switch_3dnr;
@property (strong, nonatomic) UISwitch * switch_adas_ldw;
@property (strong, nonatomic) UISwitch * switch_motion_detection;
@property (strong, nonatomic) UISwitch * switch_date_stamp;
@property (strong, nonatomic) UISwitch * switch_record_audio;
@property (strong, nonatomic) UISwitch * switch_boot_record;
@property (strong, nonatomic) UISwitch * switch_idc;
@property (strong, nonatomic) UISwitch * switch_flip;
@property (strong, nonatomic) UISwitch * switch_autooff_screen;
@property (strong, nonatomic) UISwitch * switch_dvs;

@property (strong, nonatomic) UITextField * time;
@property (strong, nonatomic) UIDatePicker * picker;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@property (assign, nonatomic) UIAlertAction * AlertAction;
@property (strong, nonatomic) UIAlertController *alert;

@property (strong, nonatomic) UIBarButtonItem *doneBtn;
@property (strong, nonatomic) UIBarButtonItem *cancelBtn;
@property (assign, nonatomic) NSInteger format;
@property (assign, nonatomic) NSInteger calibration;
@end

@implementation SettingView

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView= [self.tableView initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    
    _userDefaults = [NSUserDefaults standardUserDefaults];

    _switch_3dnr = [[UISwitch alloc] init];
    [_switch_3dnr addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_adas_ldw = [[UISwitch alloc] init];
    [_switch_adas_ldw addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_motion_detection = [[UISwitch alloc] init];
    [_switch_motion_detection addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_date_stamp = [[UISwitch alloc] init];
    [_switch_date_stamp addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_record_audio = [[UISwitch alloc] init];
    [_switch_record_audio addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_boot_record = [[UISwitch alloc] init];
    [_switch_boot_record addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_idc = [[UISwitch alloc] init];
    [_switch_idc addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_flip = [[UISwitch alloc] init];
    [_switch_flip addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_autooff_screen = [[UISwitch alloc] init];
    [_switch_autooff_screen addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_dvs = [[UISwitch alloc] init];
    [_switch_dvs addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _time = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    
    _time.text = [_userDefaults objectForKey:@"Time:"];;
    _time.delegate = self;
    _time.textAlignment = UITextAlignmentRight;
    _time.textColor = [UIColor lightGrayColor];
    
    _picker = [[UIDatePicker alloc] init];
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
        _picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
        _doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(fixedBtnClick)];
        _cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnClick)];
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        _language = 0;
        _picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        _doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(fixedBtnClick)];
        _cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnClick)];
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
    
    _time.inputView = _picker;
    
    [_picker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.backgroundColor = [UIColor lightGrayColor];

    // 添加UIToolbar里面的按钮
    toolbar.items = @[_doneBtn,_cancelBtn];
    
    // 设置textfield的辅助工具条
    _time.inputAccessoryView = toolbar;
    
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    toolbar.bounds = CGRectMake(0, 0, screenW, 44);
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(onFormatStatusChange:) name:@"FORMAT_STATUS" object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(onCalibrationStatusChange:) name:@"DVS_CALIB" object:nil];
    
    CGFloat R = (CGFloat) 0;
    CGFloat G = (CGFloat) 89/255.0;
    CGFloat B = (CGFloat) 84/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    self.navigationController.navigationBar.tintColor = myColorRGB;
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)post
{
    NSString * path = [NSString stringWithFormat: @"%@/Documents/%@", NSHomeDirectory(), @"Firmware.img"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [fileManager fileExistsAtPath:path];
    if(result==YES){
        NSLog(@"固件存在开始升级\n");
    }else{
        NSLog(@"固件需要手动拷贝到应用根目录\n");
    }
    
    [[WiFiConnection sharedSingleton] UploadFile:path];
}

- (void)onWiFiConnectionDisconnect:(NSError *)err
{
    NSString * Title;
    NSString * action1;
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"连接已断开!ps:ios exit退出不干净，必须通过奔溃的方式让app强制结束。但是苹果审核对奔溃0容忍，提示审核时屏蔽奔溃方法。上架以后改回来。";
        action1 = @"退出";
    }else {
        Title = @"connection dropped! ps:ios exit退出不干净，必须通过奔溃的方式让app强制结束。但是苹果审核对奔溃零容忍，需要审核时屏蔽奔溃方法。上架以后改回来。";
        action1 = @"exit";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        exit(0);
    }]];
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        if(_picker!=nil)
            _picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
        if(_doneBtn!=nil)
            _doneBtn.title = @"确定";
        if(_cancelBtn!=nil)
            _cancelBtn.title = @"取消";
        
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        if(_picker!=nil)
            _picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        if(_doneBtn!=nil)
            _doneBtn.title = @"Done";
        if(_cancelBtn!=nil)
            _cancelBtn.title = @"Cancel";
        
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)onCalibrationStatusChange:(NSNotification*) notification
{
    NSString * status= [notification object];
    
    NSString * Title;
    NSString * Title1;
    NSString * Title2;
    NSString * action1;

    if(_language==1){
        Title   = @"请保持DV水平静止放置";
        Title1  = @"成功";
        Title2  = @"失败";
        action1 = @"确定";
    }else{
        Title = @"Please keep DV level and still?";
        Title1  = @"Success";
        Title2  = @"Fail";
        action1 = @"YES";
    }
    
    if( [status isEqualToString:@"START"] ){
        _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:_alert animated:YES completion:nil];
        
        _AlertAction= [UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){}];
        [_alert addAction:_AlertAction];
        
        _AlertAction.enabled = NO;
    }
    else if( [status isEqualToString:@"SUCCESS"] ){
        [_alert setTitle:Title1];
        _AlertAction.enabled = YES;
    }else if( [status isEqualToString:@"FAULT"] ){
        [_alert setTitle:Title2];
        _AlertAction.enabled = YES;
    }else{
        NSLog(@"other\n");
    }
}

- (void)onFormatStatusChange:(NSNotification*) notification
{
    NSNumber * status= [notification object];
    NSInteger istatus = [status integerValue];
    
    if( _format==1 ){
        if(istatus==0){
            _AlertAction.enabled = YES;
            if(_language==1){
                [_alert setTitle:@"格式化完成"];
            }else{
                [_alert setTitle:@"Finish"];
            }
            _format= 0;
            return;
        }
        return ;
    }
    
    switch (istatus) {
        case 0:
        {
            NSString * Title;
            NSString * action1;
            NSString * action2;
            if(_language==1){
                Title = @"是否要格式化？";
                action1 = @"确定";
                action2 = @"取消";
            }else{
                Title = @"Do you want to format it?";
                action1 = @"YES";
                action2 = @"NO";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
                [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGFormat"];

                NSString * Title;
                NSString * action2;
                if(_language==1){
                    Title = @"正在格式化...";
                    action2 = @"完成";
                }else{
                    Title = @"Formatting...";
                    action2 = @"Finish";
                }
                                  
                _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
                                  
                [self presentViewController:_alert animated:YES completion:nil];
                                  
                _AlertAction = [UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                _AlertAction.enabled = NO;
                [_alert addAction:_AlertAction];
                
                _format = 1;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_FORMAT_STATUS"];
                });
            }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        }
        break;
            
        case 1:
        {
            NSString * Title;
            NSString * action2;
            if(_language==1){
                Title = @"正在格式化...";
                action2 = @"完成";
            }else{
                Title = @"Formatting...";
                action2 = @"Finish";
            }
            
            _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:_alert animated:YES completion:nil];
            
            _AlertAction = [UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
            _AlertAction.enabled = NO;
            [_alert addAction:_AlertAction];
            
            _format = 1;
        }
        break;
        
        default:
            break;
    }
}
- (void)cancelBtnClick
{
    [_time resignFirstResponder];
}

- (void)fixedBtnClick
{
    NSString * msg = [NSString stringWithFormat:@"CMD_ARGSETTINGDateSet:%@",_time.text];
    
    [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_ARGSETTING"];
    [_time resignFirstResponder];
}

- (void)FoldupKeyboard:(UITapGestureRecognizer *)tap
{
    [_time resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_ARGSETTING"];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [WiFiConnection sharedSingleton].Vc = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dateChange:(UIDatePicker *)dataPicker
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString * dateString = [fmt stringFromDate:dataPicker.date];
    _time.text = dateString;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 2;
    }else if (section== 1){
//        return 17;
        return 15;
    }else if(section==2){
        return 1;
    }else if(section==3){
        return 1;
    }else if(section==4){
        return 1;
    }else if(section==5){
        return 1;
    }else if(section==6){
        return 1;
    }else if(section==7){
        return 1;
    }else if(section==8){
        return 6;
    }else if(section==9){
        return 1;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 8;
//    return 10;
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    if(switchButton==_switch_3dnr)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTING3DNR:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTING3DNR:OFF"];
        }
    }
    else if(switchButton==_switch_adas_ldw)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGADAS:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGADAS:OFF"];
        }
    }
    else if(switchButton==_switch_motion_detection)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGMotionDetection:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGMotionDetection:OFF"];
        }
    }
    else if(switchButton==_switch_date_stamp)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDataStamp:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDataStamp:OFF"];
        }
    }
    else if(switchButton==_switch_record_audio)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordAudio:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordAudio:OFF"];
        }
    }
    else if(switchButton==_switch_boot_record)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBootRecord:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBootRecord:OFF"];
        }
    }
    else if(switchButton==_switch_idc)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGIDC:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGIDC:OFF"];
        }
    }
    else if(switchButton==_switch_flip)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGFLIP:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGFLIP:OFF"];
        }
    }
    else if(switchButton==_switch_autooff_screen)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGAutoOffScreen:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGAutoOffScreen:OFF"];
        }
    }else if(switchButton==_switch_dvs)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDVS:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDVS:OFF"];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            
            if(_language==0)
                cell.textLabel.text= @"Front_camera";
            else if(_language==1)
                cell.textLabel.text= @"前置摄像头";
            
        }else if(indexPath.row== 1){
            if(_language==0)
                cell.textLabel.text= @"Back_camera";
            else if(_language==1)
                cell.textLabel.text= @"后置摄像头";
        }
    }
    else if(indexPath.section==1){
        if(indexPath.row== 0){
            cell.accessoryView = _switch_3dnr;
            NSString * str = [_userDefaults objectForKey:@"3DNR:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_3dnr setOn:YES];
            }else{
                [_switch_3dnr setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"3DNR";
            else if(_language==1)
                cell.textLabel.text= @"夜视增强";
        }else if(indexPath.row== 1){
            cell.accessoryView = _switch_adas_ldw;
            NSString * str = [_userDefaults objectForKey:@"ADAS:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_adas_ldw setOn:YES];
            }else{
                [_switch_adas_ldw setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"ADAS_LDW";
            else if(_language==1)
                cell.textLabel.text= @"车道偏离预警";
        }else if(indexPath.row== 2){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"WhiteBalance:"];
            if(_language==1){
                if([str isEqualToString:@"auto"]==YES){
                    str = @"自动";
                }else if([str isEqualToString:@"Daylight"]==YES){
                    str = @"日光";
                }else if([str isEqualToString:@"fluocrescence"]==YES){
                    str = @"荧光";
                }else if([str isEqualToString:@"cloudysky"]==YES){
                    str = @"多云";
                }else if([str isEqualToString:@"tungsten"]==YES){
                    str = @"灯光";
                }
            }
            
            cell.detailTextLabel.text =str;
            
            if(_language==0)
                cell.textLabel.text= @"White Balance";
            else if(_language==1)
                cell.textLabel.text= @"白平衡";
        }else if(indexPath.row== 3){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Exposure:"];
            if(_language==0)
                cell.textLabel.text= @"Exposure";
            else if(_language==1)
                cell.textLabel.text= @"曝光度";
        }else if(indexPath.row== 4){
            cell.accessoryView = _switch_motion_detection;
            NSString * str = [_userDefaults objectForKey:@"MotionDetection:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_motion_detection setOn:YES];
            }else{
                [_switch_motion_detection setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"Motion Detection";
            else if(_language==1)
                cell.textLabel.text= @"移动侦测";
        }else if(indexPath.row== 5){
            cell.accessoryView = _switch_date_stamp;
            NSString * str = [_userDefaults objectForKey:@"DataStamp:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_date_stamp setOn:YES];
            }else{
                [_switch_date_stamp setOn:NO];
            }
            
            if(_language==0)
                cell.textLabel.text= @"Date Stamp";
            else if(_language==1)
                cell.textLabel.text= @"日期标签";
        }else if(indexPath.row== 6){
            cell.accessoryView = _switch_record_audio;
            NSString * str = [_userDefaults objectForKey:@"RecordAudio:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_record_audio setOn:YES];
            }else{
                [_switch_record_audio setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"Record Audio";
            else if(_language==1)
                cell.textLabel.text= @"录像音频";
        }else if(indexPath.row== 7){
            cell.accessoryView = _switch_boot_record;
            NSString * str = [_userDefaults objectForKey:@"BootRecord:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_boot_record setOn:YES];
            }else{
                [_switch_boot_record setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"Boot record";
            else if(_language==1)
                cell.textLabel.text= @"开机录像";
        }else if(indexPath.row== 8){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"Language:"];;
            if(_language==1){
                if([str isEqualToString:@"Chinese"]==YES){
                    str = @"简体中文";
                }
            }
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"language";
            else if(_language==1)
                cell.textLabel.text= @"语言选择";
        }else if(indexPath.row== 9){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Frequency:"];
            if(_language==0)
                cell.textLabel.text= @"Frequency";
            else if(_language==1)
                cell.textLabel.text= @"频率";
        }else if(indexPath.row== 10){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"Bright:"];
            if(_language==1){
                if([str isEqualToString:@"low"]==YES){
                    str = @"低";
                }else if([str isEqualToString:@"middle"]==YES){
                    str = @"中";
                }else if([str isEqualToString:@"hight"]==YES){
                    str = @"高";
                }
            }
            
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"Bright";
            else if(_language==1)
                cell.textLabel.text= @"亮度";
        }else if(indexPath.row== 11){
            cell.accessoryType = UITableViewCellAccessoryNone;
            if(_language==0)
                cell.textLabel.text= @"Format";
            else if(_language==1)
                cell.textLabel.text= @"格式化";
        }else if(indexPath.row== 12){
            if(_language==0)
                cell.textLabel.text= @"Date Set";
            else if(_language==1)
                cell.textLabel.text= @"日期设置";
            cell.accessoryView = _time;
        }else if(indexPath.row== 13){
            cell.accessoryType = UITableViewCellAccessoryNone;
            if(_language==0)
                cell.textLabel.text= @"Recovery";
            else if(_language==1)
                cell.textLabel.text= @"恢复出厂设置";
        }else if(indexPath.row== 14){
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Version:"];
            if(_language==0)
                cell.textLabel.text= @"Version";
            else if(_language==1)
                cell.textLabel.text= @"固件版本";
        }else if(indexPath.row== 15){
            cell.accessoryView = _switch_dvs;
            NSString * str = [_userDefaults objectForKey:@"DVS:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_3dnr setOn:YES];
            }else{
                [_switch_3dnr setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"DVS";
            else if(_language==1)
                cell.textLabel.text= @"防抖算法";
        }else if(indexPath.row== 16){
            cell.accessoryType = UITableViewCellAccessoryNone;
            if(_language==0)
                cell.textLabel.text= @"gyrosensor calibration";
            else if(_language==1)
                cell.textLabel.text= @"陀螺仪防抖";
        }
    }else if(indexPath.section==2){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Video length";
            else if(_language==1)
                cell.textLabel.text= @"录像时长";
        }else if(indexPath.row== 1){
            if(_language==0)
                cell.textLabel.text= @"Photo Resolution";
            else if(_language==1)
                cell.textLabel.text= @"拍照清晰度";
        }
    }else if(indexPath.section==3){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Record Mode";
            else if(_language==1)
                cell.textLabel.text= @"录像模式";
        }
    }else if(indexPath.section==4){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Connect Setting";
            else if(_language==1)
                cell.textLabel.text= @"连接设置";
        }
    }else if(indexPath.section==5){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Debug Setting";
            else if(_language==1)
                cell.textLabel.text= @"拷机设置";
        }
    }else if(indexPath.section==6){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Firmware Update";
            else if(_language==1)
                cell.textLabel.text= @"固件升级";
        }
    }
    else if(indexPath.section==8){
        if(indexPath.row== 0){
            cell.accessoryView = _switch_idc;
            NSString * str = [_userDefaults objectForKey:@"IDC:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_idc setOn:YES];
            }else{
                [_switch_idc setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"IDC";
            else if(_language==1)
                cell.textLabel.text= @"畸变校正";
        }else if(indexPath.row== 1){
            cell.accessoryView = _switch_flip;
            NSString * str = [_userDefaults objectForKey:@"FLIP:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_flip setOn:YES];
            }else{
                [_switch_flip setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"FLIP";
            else if(_language==1)
                cell.textLabel.text= @"视频旋转";
        }else if(indexPath.row== 2){
            cell.accessoryView = _switch_autooff_screen;
            NSString * str = [_userDefaults objectForKey:@"AUTOOFF_SCREEN:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_autooff_screen setOn:YES];
            }else{
                [_switch_autooff_screen setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"auo off screen";
            else if(_language==1)
                cell.textLabel.text= @"自动灭屏";
        }else if(indexPath.row== 3){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"TIME_LAPSE:"];
            if(_language==1)
                if([str isEqualToString:@"OFF"]==YES)
                    str = @"关";

            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"TIME_LAPSE";
            else if(_language==1)
                cell.textLabel.text= @"缩时录影";
        }else if(indexPath.row== 4){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"QUAITY:"];
            if(_language==1)
                if([str isEqualToString:@"HIGH"]==YES)
                    str = @"高";
                else if([str isEqualToString:@"MID"]==YES)
                    str = @"中";
                else if([str isEqualToString:@"LOW"]==YES)
                    str = @"低";
            
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"QUAITY";
            else if(_language==1)
                cell.textLabel.text= @"录像质量";
        }else if(indexPath.row== 5){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"Collision:"];
            if(_language==1)
                if([str isEqualToString:@"CLOSE"]==YES)
                    str = @"关";
                else if([str isEqualToString:@"L"]==YES)
                    str = @"低";
                else if([str isEqualToString:@"M"]==YES)
                    str = @"中";
                else if([str isEqualToString:@"H"]==YES)
                    str = @"高";
            
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"Collision";
            else if(_language==1)
                cell.textLabel.text= @"碰撞灵敏度";
        }
    } else if(indexPath.section==7){
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        NSString * str= [_userDefaults objectForKey:@"LICENCE_PLATE:"];
        if(_language==0)
            cell.textLabel.text= @"licence plate";
        else if(_language==1)
            cell.textLabel.text= @"车牌";
        cell.detailTextLabel.text = str;
    }else if(indexPath.section==9){
        if(_language==0)
            cell.textLabel.text= @"photo quality";
        else if(_language==1)
            cell.textLabel.text= @"拍照质量";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row== 0 && indexPath.section==0 )
    {
        FontCameraConfigController * Vc = [[FontCameraConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 1 && indexPath.section==0 )
    {
        BackCameraConfigController * Vc = [[BackCameraConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 2 && indexPath.section==1 )
    {
        WhiteBalanceConfigController * Vc = [[WhiteBalanceConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 3 && indexPath.section==1 )
    {
        ExposureConfigController * Vc = [[ExposureConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 8 && indexPath.section==1 )
    {
        LanguageConfigController * Vc = [[LanguageConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 9 && indexPath.section==1 )
    {
        FrequencyConfigController * Vc = [[FrequencyConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 10 && indexPath.section==1 )
    {
        BrightConfigController * Vc = [[BrightConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 0 && indexPath.section==2 )
    {
        VideoLengthConfigController * Vc = [[VideoLengthConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 1 && indexPath.section==2 )
    {
        PhotoResolutionController * Vc = [[PhotoResolutionController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];        
    }
    else if( indexPath.row== 0 && indexPath.section==3 )
    {
        RecordModeConfigController * Vc = [[RecordModeConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if(indexPath.row== 11 && indexPath.section==1){
        _format= 0;
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_FORMAT_STATUS"];
    }else if(indexPath.row== 13 && indexPath.section==1){
        NSString * Title;
        NSString * action1;
        NSString * action2;
        if(_language==0){
            Title= @"Whether to restore the factory settings?";
            action1= @"YES";
            action2= @"NO";
        }
        else if(_language==1){
            Title= @"是否恢复出厂设置?";
            action1= @"确定";
            action2= @"取消";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
        {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecovery"];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    }else if(indexPath.row== 16 && indexPath.section==1){
        _calibration= 1;
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGCalibration"];
    }
    else if( indexPath.row== 0 && indexPath.section==4 )
    {
        APConfigViewTableViewController * Vc = [[APConfigViewTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 0 && indexPath.section==5 )
    {
        DebugTableViewController * Vc = [[DebugTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 0 && indexPath.section==6 )
    {
        NSString * Title;
        NSString * action1;
        NSString * action2;
        if(_language==0){
            Title= @"Whether upgrade firmware?";
            action1= @"YES";
            action2= @"NO";
        }
        else if(_language==1){
            Title= @"是否升级固件?";
            action1= @"确定";
            action2= @"取消";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            // 1. 检查固件完整性，固件需要手动拷贝到应用根目录
            [self post];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    }else if( indexPath.row== 3 && indexPath.section==8 )
    {
        TimeLapseConfigController * Vc = [[TimeLapseConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 4 && indexPath.section==8 )
    {
        QuaityConfigController * Vc = [[QuaityConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 5 && indexPath.section==8 )
    {
        CollisionConfigController * Vc = [[CollisionConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 0 && indexPath.section==7 )
    {
        NSString * Title;
        NSString * action1;
        NSString * action2;
        if(_language==0){
            Title= @"Please enter the license plate number";
            action1= @"YES";
            action2= @"NO";
        }
        else if(_language==1){
            Title= @"请输入车牌号码";
            action1= @"确定";
            action2= @"取消";
        }
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:Title message:nil delegate:self cancelButtonTitle:action2 otherButtonTitles:action1, nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert show];
    }else if( indexPath.row== 0 && indexPath.section==9 )
    {
        PhotoQualityTableViewController * Vc = [[PhotoQualityTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSString * action1;
    if(_language==0){
        action1= @"YES";
    }
    else if(_language==1){
        action1= @"确定";
    }
    
    if([buttonTitle isEqualToString:action1]){
        UITextField * tf = [alertView textFieldAtIndex:0];
        
        NSString * str = @"CMD_ARGSETTINGLicencePlate:";
        str= [str stringByAppendingString:tf.text];
        
        NSData * data = [str dataUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
        [[WiFiConnection sharedSingleton] SendDataToCVR:data];
    }
    
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    [self.tableView reloadData];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
