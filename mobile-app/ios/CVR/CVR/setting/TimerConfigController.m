//
//  TimerConfigController.m
//  CVR
//
//  Created by 雷起斌 on 10/13/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "TimerConfigController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"

@interface TimerConfigController ()<UITextFieldDelegate>
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (strong, nonatomic) UITextField * time;
@property (strong, nonatomic) UIDatePicker * picker;
@property (assign, nonatomic) NSInteger language;
@end

@implementation TimerConfigController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    _time = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    
    _time.text = @"yyyy-MM-dd HH:mm:ss";
    _time.delegate = self;
    _time.textAlignment = UITextAlignmentRight;
    
    _picker = [[UIDatePicker alloc] init];
    _picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
    _time.inputView = _picker;
    
    [_picker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dateChange:(UIDatePicker *)dataPicker
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString * dateString = [fmt stringFromDate:dataPicker.date];
    _time.text = dateString;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text= @"Date Set";
    cell.accessoryView = _time;
    
    return cell;
}

@end
