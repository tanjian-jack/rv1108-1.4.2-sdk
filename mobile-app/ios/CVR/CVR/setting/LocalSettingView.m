//
//  LocalSettingView.m
//  CVR
//
//  Created by 雷起斌 on 5/19/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "LocalSettingView.h"
#import "APConfigViewTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import "LocalLanguageController.h"

@interface LocalSettingView () <UITableViewDelegate , UITableViewDataSource>
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) UISwitch * switchButton;
@property (strong, nonatomic) UITextView *passwdView;
@property (strong, nonatomic) UITextField *passwdField;
//@property (nonatomic, assign) BOOL isSmartConfig;
@end

@implementation LocalSettingView

- (void)viewDidLoad {
    [super viewDidLoad];

    CGFloat R  = (CGFloat) 0;
    CGFloat G = (CGFloat) 167/255.0;
    CGFloat B = (CGFloat) 157/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R green:G blue:B  alpha:alpha];
    self.navigationController.navigationBar.barTintColor = myColorRGB;
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.leftBarButtonItem.title = @" ";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section== 0){
        return 2;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;

    cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];

    if(indexPath.row==0){
        NSString * str = [_userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            cell.textLabel.text= @"清除缓存";
        }else{
            cell.textLabel.text= @"Clear Cache";
        }
    }
    else if(indexPath.row== 1){
        NSString * str = [_userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            cell.textLabel.text= @"语言";
        }else{
            cell.textLabel.text= @"Language";
        }
    }else if(indexPath.row== 2){
        NSString * str = [_userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            cell.textLabel.text= @"离线地图";
        }else{
            cell.textLabel.text= @"offline map";
        }
    }
    
    return cell;
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.tableview reloadData];
}

- (void)rmDir:(NSString *)dirname
{
    // Do any additional setup after loading the view.
    NSFileManager * filemanager = [NSFileManager defaultManager];
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(), dirname];
    
    NSError * error;
    [filemanager removeItemAtPath:path error:&error];
    if(error!=nil){
        NSLog(@"delete %@ fault %@\n",path, error);
    }
    else{
        NSLog(@"delete %@ success\n",path);
    }
}
- (void)isDirExist:(NSString *)dirname
{
    // Do any additional setup after loading the view.
    NSFileManager * filemanager = [NSFileManager defaultManager];
    
    NSString * path = [NSString stringWithFormat: @"%@/Documents/%@/", NSHomeDirectory(), dirname];
    
    BOOL isDir = FALSE;
    BOOL isDirExist = [filemanager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        if([filemanager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil]!= TRUE )
        {
            NSLog(@"Create directories %@ fault\n",path);
        }
        else{
            NSLog(@"Create %@ success\n",path);
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row== 0 && indexPath.section==0 )
    {
        NSString * Title;
        NSString * action1;
        NSString * action2;
        NSString * str = [_userDefaults objectForKey:@"app_language"];
        if([str isEqualToString:@"Chinese"]==YES){
            Title   = @"是否需要清除所有缓存文件?";
            action1 = @"确定";
            action2 = @"取消";
        }else {
            Title = @"Do you need to clear all cached files?";
            action1 = @"YES";
            action2 = @"NO";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
        {
            //[self rmDir:@"video"];
            //[self rmDir:@"picture"];
            [self rmDir:@"lock"];
            [self rmDir:@"preview"];
            [self rmDir:@"cache"];
            [self rmDir:@"gps"];
            
            //[self isDirExist:@"video"];
            //[self isDirExist:@"picture"];
            [self isDirExist:@"lock"];
            [self isDirExist:@"preview"];
            [self isDirExist:@"cache"];
            [self isDirExist:@"gps"];
            
            NSString * Title;
            NSString * action1;
            NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
            NSString * str = [userDefaults objectForKey:@"app_language"];
            if([str isEqualToString:@"Chinese"]==YES){
                Title = @"清除成功";
                action1 = @"确定";
            }else {
                Title = @"SUCCESS";
                action1 = @"OK";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
                
            [self presentViewController:alert animated:YES completion:nil];
                
            [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
            {

            }]];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
        {
        }]];
    }
    else if( indexPath.row== 1 && indexPath.section==0 ){
        LocalLanguageController * Vc = [[LocalLanguageController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}
// viewcontroller支持哪些转屏方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
