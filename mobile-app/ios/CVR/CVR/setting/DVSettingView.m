//
//  DVSettingView.m
//  CVR
//
//  Created by rk on 5/2/17.
//  Copyright © 2017 雷起斌. All rights reserved.
//

#import "DVSettingView.h"
#import "APConfigViewTableViewController.h"
#import "NTFileObj.h"
#import "WiFiConnection.h"
#import "WhiteBalanceConfigController.h"
#import "ExposureConfigController.h"
#import "LanguageConfigController.h"
#import "FrequencyConfigController.h"
#import "BrightConfigController.h"
#import "USBMODEConfigController.h"
#import "FontCameraConfigController.h"
#import "BackCameraConfigController.h"
#import "VideoLengthConfigController.h"
#import "RecordModeConfigController.h"
#import "DebugTableViewController.h"
#import "PhotoResolutionController.h"
#import "PhotoQualityTableViewController.h"
#import "QuaityConfigController.h"
#import "KeyToneTableViewController.h"

@interface DVSettingView () <UITableViewDelegate , UITableViewDataSource, GetSettingDelegate, UITextFieldDelegate, UIApplicationDelegate , UIGestureRecognizerDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) UISwitch * DistortionCorrection;
@property (strong, nonatomic) UISwitch * switch_date_stamp;
@property (strong, nonatomic) UISwitch * switch_record_audio;
@property (strong, nonatomic) UISwitch * switch_boot_record;
@property (strong, nonatomic) UISwitch * switch_auto_shut;
@property (strong, nonatomic) UISwitch * GyroscopeStabilizatiob;
@property (strong, nonatomic) UITextField * time;
@property (strong, nonatomic) UIDatePicker * picker;
@property (strong, nonatomic) NSUserDefaults * userDefaults;
@property (assign, nonatomic) NSInteger language;
@property (assign, nonatomic) UIAlertAction * AlertAction;
@property (strong, nonatomic) UIAlertController *alert;

@property (strong, nonatomic) UIBarButtonItem *doneBtn;
@property (strong, nonatomic) UIBarButtonItem *cancelBtn;

@property (assign, nonatomic) NSInteger format;
@end

@implementation DVSettingView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView= [self.tableView initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    
    _DistortionCorrection = [[UISwitch alloc] init];
    [_DistortionCorrection addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _GyroscopeStabilizatiob = [[UISwitch alloc] init];
    [_GyroscopeStabilizatiob addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_date_stamp = [[UISwitch alloc] init];
    [_switch_date_stamp addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_record_audio = [[UISwitch alloc] init];
    [_switch_record_audio addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    _switch_boot_record = [[UISwitch alloc] init];
    [_switch_boot_record addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];

    _switch_auto_shut = [[UISwitch alloc] init];
    [_switch_auto_shut addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];

    _time = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    
    _time.text = [_userDefaults objectForKey:@"Time:"];;
    _time.delegate = self;
    _time.textAlignment = UITextAlignmentRight;
    _time.textColor = [UIColor lightGrayColor];
    
    _picker = [[UIDatePicker alloc] init];
    
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
        _picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
        _doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(fixedBtnClick)];
        _cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnClick)];
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        _language = 0;
        _picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        _doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(fixedBtnClick)];
        _cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnClick)];
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
    
    _time.inputView = _picker;
    
    [_picker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.backgroundColor = [UIColor lightGrayColor];
    
    // 添加UIToolbar里面的按钮
    toolbar.items = @[_doneBtn,_cancelBtn];
    
    // 设置textfield的辅助工具条
    _time.inputAccessoryView = toolbar;
    
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    toolbar.bounds = CGRectMake(0, 0, screenW, 44);
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(onFormatStatusChange:) name:@"FORMAT_STATUS" object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(onCalibrationStatusChange:) name:@"DVS_CALIB" object:nil];
    
    CGFloat R = (CGFloat) 0;
    CGFloat G = (CGFloat) 89/255.0;
    CGFloat B = (CGFloat) 84/255.0;
    CGFloat alpha = (CGFloat) 1.0;
    
    UIColor *myColorRGB = [UIColor colorWithRed:R  green:G  blue:B  alpha:alpha];
    self.navigationController.navigationBar.tintColor = myColorRGB;
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    if(switchButton==_switch_date_stamp)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDataStamp:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDataStamp:OFF"];
        }
    }
    else if(switchButton==_switch_record_audio)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordAudio:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecordAudio:OFF"];
        }
    }
    else if(switchButton==_switch_boot_record)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBootRecord:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGBootRecord:OFF"];
        }
    }
    else if(switchButton==_switch_auto_shut)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGAutoOffScreen:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGAutoOffScreen:OFF"];
        }
    }
    else if(switchButton==_GyroscopeStabilizatiob)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDVS:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGDVS:OFF"];
        }
    }
    else if(switchButton==_DistortionCorrection)
    {
        if (isButtonOn) {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGIDC:ON"];
        }else {
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGIDC:OFF"];
        }
    }
}

- (void)backclick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)post
{
    NSString * path = [NSString stringWithFormat: @"%@/Documents/%@", NSHomeDirectory(), @"Firmware.img"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [fileManager fileExistsAtPath:path];
    if(result==YES){
        NSLog(@"固件存在开始升级\n");
    }else{
        NSLog(@"固件需要手动拷贝到应用根目录\n");
    }
    
    [[WiFiConnection sharedSingleton] UploadFile:path];
}

- (void)onGetSettingCVR:(NSMutableDictionary *)dictM
{
    [self.tableView reloadData];
}

- (void)dateChange:(UIDatePicker *)dataPicker
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString * dateString = [fmt stringFromDate:dataPicker.date];
    _time.text = dateString;
}

- (void)cancelBtnClick
{
    [_time resignFirstResponder];
}

- (void)fixedBtnClick
{
    NSString * msg = [NSString stringWithFormat:@"CMD_ARGSETTINGDateSet:%@",_time.text];
    
    [[WiFiConnection sharedSingleton] SendMsgToCVR:msg];
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_ARGSETTING"];
    [_time resignFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section== 0){
        return 2;
    }else if (section== 1){
        return 5;
    }else if(section==2){
        return 14;
    }else if(section==3){
        return 1;
    }else if(section==4){
        return 1;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row== 0 && indexPath.section==0 )
    {
        FontCameraConfigController * Vc = [[FontCameraConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 1 && indexPath.section==0 )
    {
        PhotoQualityTableViewController * Vc = [[PhotoQualityTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if(indexPath.row== 0 && indexPath.section==2){
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGCalibration"];
    }else if( indexPath.row== 2 && indexPath.section==2 )
    {
        QuaityConfigController * Vc = [[QuaityConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 3 && indexPath.section==2 )
    {
        WhiteBalanceConfigController * Vc = [[WhiteBalanceConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }else if( indexPath.row== 4 && indexPath.section==2 )
    {
        ExposureConfigController * Vc = [[ExposureConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 5 && indexPath.section==2 )
    {
        LanguageConfigController * Vc = [[LanguageConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 6 && indexPath.section==2 )
    {
        FrequencyConfigController * Vc = [[FrequencyConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 7 && indexPath.section==2 )
    {
        BrightConfigController * Vc = [[BrightConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 8 && indexPath.section==2 )
    {
        VideoLengthConfigController * Vc = [[VideoLengthConfigController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 9 && indexPath.section==2 )
    {
        KeyToneTableViewController * Vc = [[KeyToneTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if(indexPath.row== 10 && indexPath.section==2){
        _format= 0;
        [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_FORMAT_STATUS"];
    }else if(indexPath.row== 12 && indexPath.section==2){
        NSString * Title;
        NSString * action1;
        NSString * action2;
        if(_language==0){
            Title= @"Whether to restore the factory settings?";
            action1= @"YES";
            action2= @"NO";
        }
        else if(_language==1){
            Title= @"是否恢复出厂设置?";
            action1= @"确定";
            action2= @"取消";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGRecovery"];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    }
    else if( indexPath.row== 0 && indexPath.section==3 )
    {
        APConfigViewTableViewController * Vc = [[APConfigViewTableViewController alloc] init];
        [self.navigationController pushViewController:Vc animated:YES];
    }
    else if( indexPath.row== 0 && indexPath.section==4 )
    {
        NSString * Title;
        NSString * action1;
        NSString * action2;
        if(_language==0){
            Title= @"Whether upgrade firmware?";
            action1= @"YES";
            action2= @"NO";
        }
        else if(_language==1){
            Title= @"是否升级固件?";
            action1= @"确定";
            action2= @"取消";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            // 1. 检查固件完整性，固件需要手动拷贝到应用根目录
            [self post];
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(indexPath.section==0){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Video Resolution";
            else if(_language==1)
                cell.textLabel.text= @"视频分辨率";
        }else if(indexPath.row== 1){
            if(_language==0)
                cell.textLabel.text= @"Photo Resolution";
            else if(_language==1)
                cell.textLabel.text= @"拍照分辨率";
        }
    }
    else if(indexPath.section==1){
        if(indexPath.row== 0){
            cell.accessoryView = _switch_date_stamp;
            NSString * str = [_userDefaults objectForKey:@"DataStamp:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_date_stamp setOn:YES];
            }else{
                [_switch_date_stamp setOn:NO];
            }
            
            if(_language==0)
                cell.textLabel.text= @"Watermark";
            else if(_language==1)
                cell.textLabel.text= @"水印标签";
        }else if(indexPath.row== 1){
            cell.accessoryView = _switch_record_audio;
            NSString * str = [_userDefaults objectForKey:@"RecordAudio:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_record_audio setOn:YES];
            }else{
                [_switch_record_audio setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"record audio";
            else if(_language==1)
                cell.textLabel.text= @"录像音频";
        }else if(indexPath.row==2){
            cell.accessoryView = _switch_boot_record;
            NSString * str = [_userDefaults objectForKey:@"BootRecord:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_boot_record setOn:YES];
            }else{
                [_switch_boot_record setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"boot record";
            else if(_language==1)
                cell.textLabel.text= @"开机录像";
        }else if(indexPath.row==3){
            cell.accessoryView = _switch_auto_shut;
            NSString * str = [_userDefaults objectForKey:@"AUTOOFF_SCREEN:"];
            if( [str isEqualToString:@"ON"] ){
                [_switch_auto_shut setOn:YES];
            }else{
                [_switch_auto_shut setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"Auto Off Screen";
            else if(_language==1)
                cell.textLabel.text= @"自动灭屏";
        }else if(indexPath.row==4){
            cell.accessoryView = _GyroscopeStabilizatiob;
            NSString * str = [_userDefaults objectForKey:@"DVS:"];
            if( [str isEqualToString:@"ON"] ){
                [_GyroscopeStabilizatiob setOn:YES];
            }else{
                [_GyroscopeStabilizatiob setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"DVS";
            else if(_language==1)
                cell.textLabel.text= @"防抖";
        }
    }else if(indexPath.section==2){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Gyroscope Stabilizatiob";
            else if(_language==1)
                cell.textLabel.text= @"陀螺仪校准";
        }else if(indexPath.row== 1){
            cell.accessoryView = _DistortionCorrection;
            NSString * str = [_userDefaults objectForKey:@"IDC:"];
            if( [str isEqualToString:@"ON"] ){
                [_DistortionCorrection setOn:YES];
            }else{
                [_DistortionCorrection setOn:NO];
            }
            if(_language==0)
                cell.textLabel.text= @"distortion correction";
            else if(_language==1)
                cell.textLabel.text= @"畸变校正";
        }else if(indexPath.row== 2){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str = [_userDefaults objectForKey:@"QUAITY:"];
            if(_language==0)
                cell.textLabel.text= @"Video Quality";
            else if(_language==1)
                cell.textLabel.text= @"视频质量";

            if(_language==1){
                if([str isEqualToString:@"LOW"]==YES){
                    str = @"低";
                }else if([str isEqualToString:@"MID"]==YES){
                    str = @"中";
                }else if([str isEqualToString:@"HIGH"]==YES){
                    str = @"高";
                }
            }
            cell.detailTextLabel.text =str;
        }else if(indexPath.row== 3){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"WhiteBalance:"];
            if(_language==1){
                if([str isEqualToString:@"auto"]==YES){
                    str = @"自动";
                }else if([str isEqualToString:@"Daylight"]==YES){
                    str = @"日光";
                }else if([str isEqualToString:@"fluocrescence"]==YES){
                    str = @"荧光";
                }else if([str isEqualToString:@"cloudysky"]==YES){
                    str = @"多云";
                }else if([str isEqualToString:@"tungsten"]==YES){
                    str = @"灯光";
                }
            }
            
            cell.detailTextLabel.text =str;
            
            if(_language==0)
                cell.textLabel.text= @"white balance";
            else if(_language==1)
                cell.textLabel.text= @"白平衡";
        }else if(indexPath.row== 4){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Exposure:"];
            if(_language==0)
                cell.textLabel.text= @"exposure";
            else if(_language==1)
                cell.textLabel.text= @"曝光度";
        }else if(indexPath.row== 5){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"Language:"];
            if(_language==1){
                if([str isEqualToString:@"Chinese"]==YES){
                    str = @"简体中文";
                }
            }
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"language";
            else if(_language==1)
                cell.textLabel.text= @"语言选择";
        }else if(indexPath.row== 6){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Frequency:"];
            if(_language==0)
                cell.textLabel.text= @"frequency";
            else if(_language==1)
                cell.textLabel.text= @"频率";
        }else if(indexPath.row== 7){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            NSString * str= [_userDefaults objectForKey:@"Bright:"];
            if(_language==1){
                if([str isEqualToString:@"low"]==YES){
                    str = @"低";
                }else if([str isEqualToString:@"middle"]==YES){
                    str = @"中";
                }else if([str isEqualToString:@"hight"]==YES){
                    str = @"高";
                }
            }
            cell.detailTextLabel.text = str;
            if(_language==0)
                cell.textLabel.text= @"bright";
            else if(_language==1)
                cell.textLabel.text= @"亮度";
        }else if(indexPath.row== 8){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            NSString * str= [_userDefaults objectForKey:@"Videolength:"];
            
            if(_language==0)
                cell.textLabel.text= @"Video Length";
            else if(_language==1)
                cell.textLabel.text= @"录像时长";
            
            if(_language==1){
                if([str isEqualToString:@"infinite"]==YES){
                    str = @"无限时长";
                }
            }
            
            cell.detailTextLabel.text = str;
        }else if(indexPath.row== 9){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            NSString * str= [_userDefaults objectForKey:@"KeyTone:"];
            
            if(_language==0)
                cell.textLabel.text= @"Volume Control";
            else if(_language==1)
                cell.textLabel.text= @"按键音量";
            
            if(_language==1){
                if([str isEqualToString:@"20"]==YES){
                    str = @"低";
                }else if([str isEqualToString:@"60"]==YES){
                    str = @"中";
                }else if([str isEqualToString:@"100"]==YES){
                    str = @"高";
                }
            }else{
                if([str isEqualToString:@"20"]==YES){
                    str = @"low";
                }else if([str isEqualToString:@"60"]==YES){
                    str = @"middle";
                }else if([str isEqualToString:@"100"]==YES){
                    str = @"high";
                }
            }
            
            cell.detailTextLabel.text = str;
        }else if(indexPath.row== 10){
            if(_language==0)
                cell.textLabel.text= @"format";
            else if(_language==1)
                cell.textLabel.text= @"格式化";
        }else if(indexPath.row== 11){
            if(_language==0)
                cell.textLabel.text= @"date set";
            else if(_language==1)
                cell.textLabel.text= @"日期设置";
            cell.accessoryView = _time;
        }else if(indexPath.row== 12){
            cell.accessoryType = UITableViewCellAccessoryNone;
            if(_language==0)
                cell.textLabel.text= @"recovery";
            else if(_language==1)
                cell.textLabel.text= @"恢复出厂设置";
        }else if(indexPath.row== 13){
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.detailTextLabel.text =[_userDefaults objectForKey:@"Version:"];
            if(_language==0)
                cell.textLabel.text= @"version";
            else if(_language==1)
                cell.textLabel.text= @"固件版本";
        }
    }else if(indexPath.section==3){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"connect setting";
            else if(_language==1)
                cell.textLabel.text= @"连接设置";
        }
    }else if(indexPath.section==4){
        if(indexPath.row== 0){
            if(_language==0)
                cell.textLabel.text= @"Firmware Update";
            else if(_language==1)
                cell.textLabel.text= @"固件升级";
        }
    }
    return cell;
}


- (void)onWiFiConnectionDisconnect:(NSError *)err
{
    NSString * Title;
    NSString * action1;
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        Title = @"连接已断开!ps:ios exit退出不干净，必须通过奔溃的方式让app强制结束。但是苹果审核对奔溃0容忍，提示审核时屏蔽奔溃方法。上架以后改回来。";
        action1 = @"退出";
    }else {
        Title = @"connection dropped! ps:ios exit退出不干净，必须通过奔溃的方式让app强制结束。但是苹果审核对奔溃零容忍，需要审核时屏蔽奔溃方法。上架以后改回来。";
        action1 = @"exit";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
    {
        exit(0);
    }]];
}

- (void)viewWillAppear:(BOOL)animated{
    [WiFiConnection sharedSingleton].Vc = self;
    
    _userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * str = [_userDefaults objectForKey:@"app_language"];
    if([str isEqualToString:@"Chinese"]==YES){
        _language = 1;
    }else{
        _language = 0;
    }
    
    if(_language==1){
        if(_picker!=nil)
            _picker.locale = [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
        if(_doneBtn!=nil)
            _doneBtn.title = @"确定";
        if(_cancelBtn!=nil)
            _cancelBtn.title = @"取消";
        
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }else{
        if(_picker!=nil)
            _picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        if(_doneBtn!=nil)
            _doneBtn.title = @"Done";
        if(_cancelBtn!=nil)
            _cancelBtn.title = @"Cancel";
        
        self.navigationItem.leftBarButtonItem= [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backclick)];
    }
    
    [WiFiConnection sharedSingleton].GetSettingDelegate = self;
    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_ARGSETTING"];
}

- (void)onCalibrationStatusChange:(NSNotification*) notification
{
    NSString * status= [notification object];
    
    NSString * Title;
    NSString * Title1;
    NSString * Title2;
    NSString * action1;
    
    if(_language==1){
        Title   = @"请保持DV水平静止放置";
        Title1  = @"成功";
        Title2  = @"失败";
        action1 = @"确定";
    }else{
        Title = @"Please keep DV level and still?";
        Title1  = @"Success";
        Title2  = @"Fail";
        action1 = @"YES";
    }
    
    if( [status isEqualToString:@"START"] ){
        _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:_alert animated:YES completion:nil];
        
        _AlertAction= [UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){}];
        [_alert addAction:_AlertAction];
        
        _AlertAction.enabled = NO;
    }
    else if( [status isEqualToString:@"SUCCESS"] ){
        [_alert setTitle:Title1];
        _AlertAction.enabled = YES;
    }else if( [status isEqualToString:@"FAULT"] ){
        [_alert setTitle:Title2];
        _AlertAction.enabled = YES;
    }else{
        NSLog(@"other\n");
    }
}

- (void)onFormatStatusChange:(NSNotification*) notification
{
    NSNumber * status= [notification object];
    NSInteger istatus = [status integerValue];
    
    if( _format==1 ){
        if(istatus==0){
            _AlertAction.enabled = YES;
            if(_language==1){
                [_alert setTitle:@"格式化完成"];
            }else{
                [_alert setTitle:@"Finish"];
            }
            _format= 0;
            return;
        }
        return ;
    }
    
    switch (istatus) {
        case 0:
        {
            NSString * Title;
            NSString * action1;
            NSString * action2;
            if(_language==1){
                Title = @"是否要格式化？";
                action1 = @"确定";
                action2 = @"取消";
            }else{
                Title = @"Do you want to format it?";
                action1 = @"YES";
                action2 = @"NO";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            [alert addAction:[UIAlertAction actionWithTitle:action1 style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
                [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_ARGSETTINGFormat"];
                
                NSString * Title;
                NSString * action2;
                if(_language==1){
                    Title = @"正在格式化...";
                    action2 = @"完成";
                }else{
                    Title = @"Formatting...";
                    action2 = @"Finish";
                }
                
                _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:_alert animated:YES completion:nil];
                
                _AlertAction = [UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
                _AlertAction.enabled = NO;
                [_alert addAction:_AlertAction];
                
                _format = 1;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[WiFiConnection sharedSingleton] SendMsgToCVR:@"CMD_GET_FORMAT_STATUS"];
                });
            }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}]];
        }
            break;
            
        case 1:
        {
            NSString * Title;
            NSString * action2;
            if(_language==1){
                Title = @"正在格式化...";
                action2 = @"完成";
            }else{
                Title = @"Formatting...";
                action2 = @"Finish";
            }
            
            _alert = [UIAlertController alertControllerWithTitle:Title message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:_alert animated:YES completion:nil];
            
            _AlertAction = [UIAlertAction actionWithTitle:action2 style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
            _AlertAction.enabled = NO;
            [_alert addAction:_AlertAction];
            
            _format = 1;
        }
            break;
            
        default:
            break;
    }
}

- (void)FoldupKeyboard:(UITapGestureRecognizer *)tap
{
    [_time resignFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [WiFiConnection sharedSingleton].Vc = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSString * action1;
    if(_language==0){
        action1= @"YES";
    }
    else if(_language==1){
        action1= @"确定";
    }
    
    if([buttonTitle isEqualToString:action1]){
        UITextField * tf = [alertView textFieldAtIndex:0];
        
        NSString * str = @"CMD_ARGSETTINGLicencePlate:";
        str= [str stringByAppendingString:tf.text];
        
        NSData * data = [str dataUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
        [[WiFiConnection sharedSingleton] SendDataToCVR:data];
    }
    
}

@end
