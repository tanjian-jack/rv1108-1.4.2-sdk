//
//  MapViewController.m
//  CVR
//
//  Created by rk on 11/25/16.
//  Copyright © 2016 雷起斌. All rights reserved.
//

#import "MapViewController.h"
#import <MAMapKit/MAMapKit.h>

@interface MapViewController () <MAMapViewDelegate>
@property (nonatomic, strong) MAMapView * map;
@property (nonatomic, strong) MAPointAnnotation * car;
@property (nonatomic, strong) NSMutableArray * coords;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.map];

    _coords = [[NSMutableArray alloc] init];
    
    [self initRoute];
    
    /* Step 1. */
    //show car
    self.car = [[MAPointAnnotation alloc] init];
    self.car.coordinate = CLLocationCoordinate2DMake(39.93563,  116.387358);
    self.car.title = @"Car";
    
    [self.map addAnnotation:self.car];
}

- (void)initRoute
{
    NSUInteger count = 14;
    CLLocationCoordinate2D * coords = malloc(count * sizeof(CLLocationCoordinate2D));
    
    coords[0] = CLLocationCoordinate2DMake(39.93563,  116.387358);
    coords[1] = CLLocationCoordinate2DMake(39.935564,   116.386414);
    coords[2] = CLLocationCoordinate2DMake(39.935646,  116.386038);
    coords[3] = CLLocationCoordinate2DMake(39.93586, 116.385791);
    coords[4] = CLLocationCoordinate2DMake(39.93586, 116.385791);
    coords[5] = CLLocationCoordinate2DMake(39.937983, 116.38474);
    coords[6] = CLLocationCoordinate2DMake(39.938616, 116.3846);
    coords[7] = CLLocationCoordinate2DMake(39.938888, 116.386971);
    coords[8] = CLLocationCoordinate2DMake(39.938855, 116.387047);
    coords[9] = CLLocationCoordinate2DMake(39.938172,  116.387132);
    coords[10] = CLLocationCoordinate2DMake(39.937604, 116.387218);
    coords[11] = CLLocationCoordinate2DMake(39.937489, 116.387132);
    coords[12] = CLLocationCoordinate2DMake(39.93614,  116.387283);
    coords[13] = CLLocationCoordinate2DMake(39.935622,  116.387347);
}

- (void)showRouteForCoords:(CLLocationCoordinate2D *)coords count:(NSUInteger)count
{
    //show route
    MAPolyline *route = [MAPolyline polylineWithCoordinates:coords count:count];
    [self.map addOverlay:route];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    /* Step 2. */
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        if ([annotation.title isEqualToString:@"Car"])
        {
            static NSString *pointReuseIndetifier = @"pointReuseIndetifier";

            UIImage *imge  =  [UIImage imageNamed:@"userPosition"];
            MAAnnotationView * annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                                                     reuseIdentifier:pointReuseIndetifier];
            annotationView.image =  imge;
//            CGPoint centerPoint= CGPointZero;
//            [annotation setCenterOffset:centerPoint];
            return annotationView;
        }
    }
    
    return nil;
}
    
- (MAMapView *)map
{
    if (!_map)
    {
        _map = [[MAMapView alloc] initWithFrame:self.view.frame];
        [_map setDelegate:self];
    }
    return _map;
}


@end
