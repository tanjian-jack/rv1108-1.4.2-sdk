package com.example.xng.rkcamera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import com.example.xng.rkcamera.Map.gps.GpsInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class SocketService{
    static final String TAG = "SocketService";

    public static final String ACTION_DOWNLOAD_FRAGMENT = "ACTION_DOWNLOAD_FRAGMENT";
    public static final String ACTION_IPCAMERA_SETTING = "ACTION_IPCAMERA_SETTING";
    public static final String ACTION_VIDEO_PLAYER_ACTIVITY = "ACTION_VIDEO_PLAYER_ACTIVITY";
    public static final String ACTION_CONNECT_SETTING = "ACTION_CONNECT_SETTING";
    public static final String ACTION_IPCAMERA_DEBUG = "ACTION_IPCAMERA_DEBUG";
    public static final String ACTION_GPS_FILE_LIST = "ACTION_GPS_FILE_LIST";
    //public static final String ACTION_CAMERA_FRAGMENT = "ACTION_CAMERA_FRAGMENT";
    //public static final String ACTION_VIDEO_PLAY = "ACTION_VIDEO_PLAY";

    private Context mContext = null;
    private String mOwner = "null";
    private ArrayList<String> mGpsFileList = new ArrayList<String>();

    private Socket mSocket = null;
    private InputStream mReader = null;
    private OutputStream mWriter = null;
    private boolean mReadDone = false;
    private String mLock = new String("lock");

    private static volatile SocketService mInstance = null;

    //tiantian, test
    private boolean mTest = false;
    private boolean mTakePhoto = false;

    public static SocketService getInstance() {
        if(mInstance == null) {
            synchronized(SocketService.class) {
                if(mInstance == null) {
                    mInstance = new SocketService();
                }
            }
        }

        return mInstance;
    }

    public void setContext(Context context) {
        if (mContext == null)
            mContext = context;
    }

    public void clearGpsFileList() {
        mGpsFileList.clear();
    }

    public void setOwner(String owner) {
        mOwner = owner;
    }

    public String getOwner() {
        Log.d(TAG, "mOwner: " + mOwner);
        return mOwner;
    }

    protected SocketService () {
        Log.d(TAG, "SocketService, new socket");
        connect();
    }

    private void connect() {
        new Thread() {
            @Override
            public void run() {
                try {
                    if(ConnectIP.IP == null){
                        //connectBuilder();
                        Log.d(TAG, "ConnectIP.IP == null");
                    } else {
                        if (mSocket == null){
                            mSocket = new Socket(ConnectIP.IP, 8888);
                            mSocket.setKeepAlive(true);
                            mReader = mSocket.getInputStream();
                            mWriter = mSocket.getOutputStream();
                            mReadDone = true;
                            revMsg();
                            sendUrgentDataThread();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void connectBuilder() {
        Looper.prepare();
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(mContext.getString(R.string.connect_confirm_msg));

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent =  new Intent(Settings.ACTION_WIFI_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        builder.setCancelable(false);
        builder.create().show();
        Looper.loop();
    }

    private void exitBuilder() {
        Log.d(TAG, mContext.getString(R.string.wifi_disconnected_hint));
        closeSocket();

        mContext.getMainLooper().prepare();
        //Looper.prepare();
        AlertDialog.Builder exit_builder = new AlertDialog.Builder(mContext);
        exit_builder.setMessage(mContext.getString(R.string.wifi_disconnected_hint));
        exit_builder.setPositiveButton(R.string.restart_msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //closeSocket();
                Intent intent = new Intent(mContext, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);

                // 杀掉进程
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });

        exit_builder.setCancelable(false);
        exit_builder.create().show();
        mContext.getMainLooper().loop();
        //Looper.loop();
    }

    private void photoTest() {
        new Thread() {
            @Override
            public void run() {
                while (mTest) {
                    try {
                        if (!mTakePhoto) {
                            sendMsg("CMD_Control_Photograph", 0);
                            mTakePhoto = true;
                        }
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }.start();
    }

    private void dealMsg(String msg) {
        Intent intent = new Intent();

        if (msg.equals("CMD_ALIVE"))
            return;

        Log.d(TAG, "dealMsg: " + msg);

        if (msg.startsWith(GpsInfo.CMD_ACK_GET_GPS_LIST)) {
            dealGpsListInfo(msg);
        } /*else if(msg.startsWith(VideoPlayerActivity.CMD_CB_STARTREC)) { //tiantian, test
            mTest = true;
            photoTest();
        } else if(msg.startsWith(VideoPlayerActivity.CMD_CB_STOPREC)) {
            mTest = false;
        } else if (msg.startsWith(VideoPlayerActivity.CMD_CB_PHOTO_END)) {
            mTakePhoto = false;
        } */ else {
            if (msg.startsWith(VideoPlayerActivity.CMD_RECORD_BUSY)
                    || msg.startsWith(VideoPlayerActivity.CMD_RECORD_IDLE)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_STARTREC)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_STOPREC)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_NO_SDCARD)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_GPS_UPDATA)
                    || msg.startsWith(VideoPlayerActivity.CMD_ACK_GET_CAMERA_LIST)
                    || msg.startsWith(VideoPlayerActivity.CMD_ACK_SET_LIVE_VIDEO)
                    || msg.startsWith(VideoPlayerActivity.CMD_ACK_GET_LIVE_VIDEO)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_ADD_VIDEO_LIST)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_DEL_VIDEO_LIST)
                    || msg.startsWith(VideoPlayerActivity.CMD_CB_PHOTO_END)
                    || (msg.startsWith(VideoPlayerActivity.CMD_CB_GET_MODE ) && getOwner().equals("VideoPlayerActivity"))) {
                Log.d(TAG, "sendBroadcast to  VideoPlayerActivity");
                intent.setAction(ACTION_VIDEO_PLAYER_ACTIVITY);
            } else if (msg.startsWith(DownloadFragment.CMD_ACK_GETCAMFILE_FINISH)
                    || msg.startsWith(DownloadFragment.CMD_GETCAMFILENAME)
                    || msg.startsWith(DownloadFragment.CMD_CB_GETCAMFILENAME)
                    || msg.startsWith(DownloadFragment.CMD_CB_DELETE)
                    || msg.startsWith(DownloadFragment.CMD_DELSUCCESS)
                    || msg.startsWith(DownloadFragment.CMD_DELFAULT)
                    || msg.startsWith(DownloadFragment.CMD_ACK_GETCAMFILE_STOP)) {
                Log.d(TAG, "sendBroadcast to DownloadFragment");
                intent.setAction(ACTION_DOWNLOAD_FRAGMENT);
            } else if (msg.startsWith(ConnectSetting.CMD_WIFI_INFOWIFINAME)) {
                Log.d(TAG, "sendBroadcast to  ConnectSetting");
                intent.setAction(ACTION_CONNECT_SETTING);
            } else if (msg.startsWith(IpCameraDebug.CMD_ACK_GET_DEBUG)) {
                Log.d(TAG, "sendBroadcast to IpCameraDebug");
                intent.setAction(ACTION_IPCAMERA_DEBUG);
            } else {
                //Log.d(TAG, "sendBroadcast to IpCameraSetting");
                intent.setAction(ACTION_IPCAMERA_SETTING);
            }

            intent.putExtra("msg", msg);
            mContext.sendBroadcast(intent);
        }
    }

    private void dealGpsListInfo(String msg) {
        String[] tmp = msg.split(":");
        if (tmp[0].equals(GpsInfo.CMD_ACK_GET_GPS_LIST)) {
            mGpsFileList.add(tmp[1]);
            sendMsg("CMD_NEXTFILE", 0);
            Log.d(TAG, "gps " + mGpsFileList.size() +" file name: " + tmp[1]);
        } else if (tmp[0].equals(GpsInfo.CMD_ACK_GET_GPS_LIST_END)) {
            Intent intent = new Intent(ACTION_GPS_FILE_LIST);
            intent.putExtra("mapGpsFile", tmp[1]);
            intent.putExtra("gpsFileList", mGpsFileList);
            mContext.sendBroadcast(intent);
        }
    }

    private void splitMsg(String msg) {
        int i;
        String tmp[] = msg.split("CMD_");

        for (i = 1; i < tmp.length; i++) {
            //Log.d(TAG, "tmp[" + i + "]: " + "CMD_" + tmp[i]);
            dealMsg("CMD_" + tmp[i]);
        }
    }

    private void sendUrgentDataThread() {
        new Thread() {
            @Override
            public void run() {
                String msg = "CMD_HEART"; //heart;
                while (mReadDone) {
                    try {
                        //mSocket.sendUrgentData(0xff);
                        mWriter.write(msg.getBytes("GB2312"));
                        mWriter.flush();
                        //Log.d(TAG, "-----send CMD_HEART");
                        Thread.sleep(5000);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(TAG, "sendUrgentData error");
                        break;
                    }
                }
                Log.d(TAG, "exit sendUrgentData thread");
            }
        }.start();
    }

    private void revMsg() {
        new Thread() {
            @Override
            public void run() {
                try {
                    byte[] mbyte = new byte[1024];
                    int readSize = -1;
                    String info = null;

                    mSocket.setSoTimeout(10000);
                    while (mReadDone) {
                        readSize = mReader.read(mbyte);
                        //Log.d(TAG, "readSize: " + readSize);
                        if (readSize > 0) {
                            if (mbyte[readSize - 2] == 13 && mbyte[readSize - 1] == 10) //去除gps数据的\r\n
                                readSize -= 2;

                            byte[] msg = new byte[readSize];
                            System.arraycopy(mbyte, 0, msg, 0, readSize);
                            info = new String(msg, "GB2312");

                            //Log.d(TAG, "readSize: " + readSize);

                            if (!info.equals("CMD_ALIVE")) {
                                Log.d(TAG, "revMsg: " + info);
                                splitMsg(info);
                            }

                            readSize = -1;
                        } else if (readSize == -1) {
                            Log.d(TAG, "readSize: " + readSize);
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //exitBuilder();
                } finally {
                    Log.d(TAG, "exit receive thread");
                    exitBuilder();
                }
            }
        }.start();
    }

    public void sendMsg(final String msg, int time) { //time: ms
        if (mWriter != null)
            new sendThread(msg, time).start();
    }

    private class sendThread extends Thread {
        private String msg;
        private int time;

        public sendThread(String msg, int time) {
            this.msg = msg;
            this.time = time;
        }

        public void run() {
            if (time > 0) {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //synchronized (mLock) {
                try {
                    Log.d(TAG, "send:" + msg);
                    mWriter.write(msg.getBytes("GB2312"));
                    mWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            //}
        }
    }

    public void closeSocket() {
        Log.d(TAG, "closeSocket");
        mReadDone = false;
        try {
            if (mWriter != null){
                mWriter.close();
                mWriter = null;
            }

            if (mReader != null){
                mReader.close();
                mReader = null;
            }

            if (mSocket != null){
                mSocket.close();
                mSocket = null;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
