package com.example.xng.rkcamera;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.Switch;

import com.example.xng.rkcamera.adapter.ArrayWheelAdapter;
import com.example.xng.rkcamera.widget.WheelView;

public class LicencePlateDialog {
    private String[] mLicenceZn = {"京", "津", "沪", "渝", "冀", "晋", "蒙", "辽", "吉", "黑", "苏",
            "浙", "皖", "闽", "赣", "鲁", "豫", "鄂", "湘", "粤", "桂", "琼", "川", "贵",
            "云", "藏", "青", "陕", "甘", "宁", "新", "港", "澳", "台"};

    private String[] mLicenceEn = {"-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "*"};

    private Activity mActivity;
    private FrameLayout mMainLayout;
    private Switch mLicenceSwitch;

    private WheelView mWv1, mWv2, mWv3, mWv4, mWv5, mWv6, mWv7, mWv8;
    private int mWvIndex[] = new int[8];
    private static final int mVisibleItems = 5;

    private boolean mIsSetLience = false;
    private int mScreenWidth;
    private PopupWindow popupWindow;
    private SocketService mSocketService = SocketService.getInstance();

    public LicencePlateDialog(Activity activity, FrameLayout layout, Switch licenceSwitch) {
        mActivity = activity;
        mMainLayout = layout;
        mLicenceSwitch = licenceSwitch;
        getScreenWidth();
    }

    public void SetLicencePlate(String licence) {
        showLicencePop(licence);
        makeWindowDark();
    }

    private void getScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenWidth = metrics.widthPixels;
    }

    private void showLicencePop(final String licence) {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupWindowView = inflater.inflate(R.layout.licence_plate_picker_layout, null);
        Button btn_ok = (Button) popupWindowView.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) popupWindowView.findViewById(R.id.btn_cancel);

        popupWindow = new PopupWindow(popupWindowView, mScreenWidth * 4 / 5, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setOutsideTouchable(true);

        initWheelView(popupWindowView, licence);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (licence.equals("OFF") && !mIsSetLience) {
                    mLicenceSwitch.setChecked(false);
                }

                mIsSetLience = false;
                makeWindowLight();
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String licence = mLicenceZn[mWv1.getCurrentItem()] + mLicenceEn[mWv2.getCurrentItem()]
                        + mLicenceEn[mWv3.getCurrentItem()] + mLicenceEn[mWv4.getCurrentItem()]
                        + mLicenceEn[mWv5.getCurrentItem()] + mLicenceEn[mWv6.getCurrentItem()]
                        + mLicenceEn[mWv7.getCurrentItem()] + mLicenceEn[mWv8.getCurrentItem()];

                mSocketService.sendMsg("CMD_ARGSETTINGLicencePlate:" + licence, 0);
                mIsSetLience = true;
                popupWindow.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(mMainLayout, Gravity.CENTER, 0, 0);
    }

    private void getLicenceIndex(String licence) {
        int i, j;

        if (licence.equals("OFF") || licence.equals("")) {
            for (i = 0; i < mWvIndex.length; i++)
                mWvIndex[i] = 0;
        } else {
            for (i = 0; i < mLicenceZn.length; i++) {
                if (mLicenceZn[i].equals(String.valueOf(licence.charAt(0)))) {
                    mWvIndex[0] = i;
                    break;
                }
            }

            for (i = 1; i < licence.length(); i++) {
                for (j = 0; j < mLicenceEn.length; j++) {
                    if (mLicenceEn[j].equals(String.valueOf(licence.charAt(i)))) {
                        mWvIndex[i] = j;
                        break;
                    }
                }
            }
        }
    }

    private void initWheelView(View view, String licence) {
        ArrayWheelAdapter arrayWheelAdapterZn = new ArrayWheelAdapter<String>(
                mActivity, mLicenceZn);

        ArrayWheelAdapter arrayWheelAdapterEn = new ArrayWheelAdapter<String>(
                mActivity, mLicenceEn);

        getLicenceIndex(licence);

        mWv1 = (WheelView) view.findViewById(R.id.wv1);
        mWv1.setViewAdapter(arrayWheelAdapterZn);
        mWv1.setCyclic(true);
        mWv1.setCurrentItem(mWvIndex[0]);
        mWv1.setVisibleItems(mVisibleItems);

        mWv2 = (WheelView) view.findViewById(R.id.wv2);
        mWv2.setViewAdapter(arrayWheelAdapterEn);
        mWv2.setCyclic(true);
        mWv2.setCurrentItem(mWvIndex[1]);
        mWv2.setVisibleItems(mVisibleItems);

        mWv3 = (WheelView) view.findViewById(R.id.wv3);
        mWv3.setViewAdapter(arrayWheelAdapterEn);
        mWv3.setCyclic(true);
        mWv3.setCurrentItem(mWvIndex[2]);
        mWv3.setVisibleItems(mVisibleItems);

        mWv4 = (WheelView) view.findViewById(R.id.wv4);
        mWv4.setViewAdapter(arrayWheelAdapterEn);
        mWv4.setCyclic(true);
        mWv4.setCurrentItem(mWvIndex[3]);
        mWv4.setVisibleItems(mVisibleItems);

        mWv5 = (WheelView) view.findViewById(R.id.wv5);
        mWv5.setViewAdapter(arrayWheelAdapterEn);
        mWv5.setCyclic(true);
        mWv5.setCurrentItem(mWvIndex[4]);
        mWv5.setVisibleItems(mVisibleItems);

        mWv6 = (WheelView) view.findViewById(R.id.wv6);
        mWv6.setViewAdapter(arrayWheelAdapterEn);
        mWv6.setCyclic(true);
        mWv6.setCurrentItem(mWvIndex[5]);
        mWv6.setVisibleItems(mVisibleItems);

        mWv7 = (WheelView) view.findViewById(R.id.wv7);
        mWv7.setViewAdapter(arrayWheelAdapterEn);
        mWv7.setCyclic(true);
        mWv7.setCurrentItem(mWvIndex[6]);
        mWv7.setVisibleItems(mVisibleItems);

        mWv8 = (WheelView) view.findViewById(R.id.wv8);
        mWv8.setViewAdapter(arrayWheelAdapterEn);
        mWv8.setCyclic(true);
        mWv8.setCurrentItem(mWvIndex[7]);
        mWv8.setVisibleItems(mVisibleItems);
    }

    /**
     * 让屏幕变暗
     */
    private void makeWindowDark() {
        Window window = mActivity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 0.5f;
        window.setAttributes(lp);
    }

    /**
     * 让屏幕变亮
     */
    private void makeWindowLight() {
        Window window = mActivity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 1f;
        window.setAttributes(lp);
    }
}