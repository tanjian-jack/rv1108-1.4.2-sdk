#! /bin/sh

if [ $# -gt 2 ] || [ $# -le 0 ]; then
    echo "usage: rndis_gdb.sh [bin name] [port num(default 7654)]"
    exit 1
fi

if [ `ps aux | grep gdbserver | grep -v grep` ]; then
    echo "another gdbserver is running, pls kill it"
    exit 2
fi

attach=y
pid=`ps aux | grep $1 | grep -v grep | grep -v log | grep -v rndis_gdb | busybox awk '{print \$1}'`
echo pid=$pid
if [ ! $pid ]; then
    echo "$1 is not running. run it by gdbserver"
    attach=n
else
    echo "attach to pid $pid"
fi

echo 1 >/tmp/RV_USB_STATE_MASK_DEBUG
echo RockChip > /sys/class/android_usb/android0/iManufacturer
echo RV1108 > /sys/class/android_usb/android0/iProduct 
echo 0 > /sys/class/android_usb/android0/enable
echo 2207 > /sys/class/android_usb/android0/idVendor
echo 0007 > /sys/class/android_usb/android0/idProduct
echo rndis > /sys/class/android_usb/android0/functions
echo 1 > /sys/class/android_usb/android0/enable

sleep 1
ifconfig lo 127.0.0.1 netmask 255.255.255.0
ifconfig rndis0 192.168.100.1 netmask 255.255.255.0 up
route add default gw 192.168.100.1 rndis0
dnsmasq_pid=`ps aux | grep rndis_gdb_dnsmasq.conf | grep -v grep | busybox awk '{print \$1}'`
sleep 1

if [ ! $dnsmasq_pid ]; then
echo "interface=rndis0" > /tmp/rndis_gdb_dnsmasq.conf
echo "user=root" >> /tmp/rndis_gdb_dnsmasq.conf
echo "listen-address=192.168.100.1" >> /tmp/rndis_gdb_dnsmasq.conf
echo "dhcp-range=192.168.100.167,192.168.100.167,24h" >> /tmp/rndis_gdb_dnsmasq.conf
echo "server=/google/8.8.8.8" >> /tmp/rndis_gdb_dnsmasq.conf
/usr/local/sbin/dnsmasq -O 6 -C /tmp/rndis_gdb_dnsmasq.conf
fi

#rm /tmp/RV_USB_STATE_MASK_DEBUG

port=7654
if [ $2 ]; then
    port=$2
fi

if [[ "$attach" == "y" ]]; then
    echo "gdbserver --attach :$port $pid"
    gdbserver --attach :$port $pid & 
else
    echo "gdbserver :$port $1"
    gdbserver :$port $1 & 
fi
