#ifndef __CVR_FFMEPG_SHARED_H__
#define __CVR_FFMEPG_SHARED_H__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>

/* encoder */
typedef struct {
    uint8_t *vir_addr;
    /* -1 means normal memory */
    int32_t phy_fd;
    void *handle;
    size_t buf_size;
}DataBuffer_t;

typedef DataBuffer_t* pdata_handle;

/* decoder */
typedef void (*free_mpp_mem)(void *ctx, void *mem);
typedef struct {
    DataBuffer_t image_buf;
    void *rkdec_ctx;
    free_mpp_mem free_func;
}DataBuffer_VPU_t;

typedef DataBuffer_VPU_t* image_handle;

#ifdef __cplusplus
}
#endif
#endif
