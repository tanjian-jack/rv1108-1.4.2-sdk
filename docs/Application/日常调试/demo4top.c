/*************************************************************************
        > File Name: main.c
        > Author:
        > Mail:
        > Created Time: Thu 28 Dec 2017 11:11:47 AM CST
 ************************************************************************/

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

int g_fd = -1;
int run = 1;

void *thread_write(void *arg) {
  int tt = 1 * 1000 * 1000;
  int i = 10;
  int fd = (*(int *)arg);
  char str[3] = {0};
  prctl(PR_SET_NAME, __func__, 0, 0, 0);
  while (--tt) {
    --i;
    snprintf(str, sizeof(str), "%d\n", i);
    printf("write content : %s", str);
    write(fd, str, 2);
    usleep(100 * 1000);
    if (i <= 0)
      i = 10;
  }
  printf("exit : %s\n", __func__);
  run = 0;
  pthread_exit(0);
}

void *thread_read(void *arg) {
  int fd = (*(int *)arg);
  char str[3] = {0};
  fd_set fdset;
  struct timeval timeout;
  prctl(PR_SET_NAME, __func__, 0, 0, 0);
  timeout.tv_sec = 0;
  timeout.tv_usec = 50 * 1000;
  while (run) {
    FD_ZERO(&fdset);
    FD_SET(fd, &fdset);
    int ret = select(fd + 1, &fdset, NULL, NULL, &timeout);
    if (ret < 0)
      break;
    else if (ret == 0) // timeout
      continue;
    if (FD_ISSET(fd, &fdset)) {
      ssize_t ret = read(fd, str, 2);
      if (ret != 2)
        printf("read error; ennor : %d\n", errno);
      else
        printf("read content: %s", str);
    }
  }
  printf("exit: %s\n", __func__);
  pthread_exit(0);
}

int OpenPipe(const char *pipe_path) {
  int fd = -1;
  if (pipe_path) {
    // If do not exist, create it.
    if (access(pipe_path, F_OK) == -1 && mkfifo(pipe_path, 0777) != 0) {
      printf("mkfifo %s failed, errno: %d\n", pipe_path, errno);
      return -1;
    }
    fd = open(pipe_path, O_RDWR);
    if (fd < 0)
      printf("open %s fail, errno: %d\n", pipe_path, errno);
  }
  return fd;
}

void ClosePipe(int fd) {
  if (fd >= 0 && close(fd))
    fprintf(stderr, "close fd <%d> failed, errno: %d\n", fd, errno);
}

int main() {
  g_fd = OpenPipe("/tmp/testfifo");
  if (g_fd < 0) {
    return -1;
  }
  pthread_t w_tid = 0;
  pthread_t r_tid = 0;
  int ret = pthread_create(&r_tid, NULL, thread_read, (void *)&g_fd);
  if (ret)
    goto err;
  ret = pthread_create(&w_tid, NULL, thread_write, (void *)&g_fd);
err:
  if (w_tid)
    pthread_join(w_tid, NULL);
  if (r_tid)
    pthread_join(r_tid, NULL);
  ClosePipe(g_fd);
  printf("exit: main\n");
  return 0;
}
