
#include <fcntl.h>
#include <sys/ioctl.h>

typedef		unsigned short	    uint16;
typedef		unsigned long	    uint32;
typedef		unsigned char	    uint8;

#define VENDOR_REQ_TAG		0x56524551
#define VENDOR_READ_IO		_IOW('v', 0x01, unsigned int)
#define VENDOR_WRITE_IO		_IOW('v', 0x02, unsigned int)

#define VENDOR_SN_ID		1
#define VENDOR_WIFI_MAC_ID	2
#define VENDOR_LAN_MAC_ID	3
#define VENDOR_BLUETOOTH_ID	4

struct rk_vendor_req {
	u32 tag;
	u16 id;
	u16 len;
	u8 data[1];
};

void rknand_print_hex_data(uint8 *s,uint32 * buf,uint32 len)
{
	uint32 i,j,count;

	ERROR("%s",s);
	for(i=0;i<len;i+=4)
		ERROR("%x %x %x %x",buf[i],buf[i+1],buf[i+2],buf[i+3]);
}

int vendor_storage_read_test(void)
{
	uint32 i;
	int ret ;
	uint8 p_buf[2048]; /* malloc req buffer or used extern buffer */
	struct rk_vendor_req *req;
	
	req = (struct rk_vendor_req *)p_buf;
	int sys_fd = open("/dev/vendor_storage",O_RDWR,0);
	if(sys_fd < 0){
		ERROR("vendor_storage open fail\n");
		return -1;
	}

	req->tag = VENDOR_REQ_TAG;
	req->id = VENDOR_SN_ID;
	req->len = 512; /* max read length to read*/

	ret = ioctl(sys_fd, VENDOR_READ_IO, req);
	rknand_print_hex_data("vendor read:", (uint32*)req, req->len + 8);
	/* return req->len is the real data length stored in the NV-storage */
	if(ret){
		ERROR("vendor read error\n");
		return -1;
	}

	return 0;
}

int vendor_storage_write_test(void)
{
	uint32 i;
	int ret ;
	uint8 p_buf[2048]; /* malloc req buffer or used extern buffer */
	struct rk_vendor_req *req;
	
	req = (struct rk_vendor_req *)p_buf;
	int sys_fd = open("/dev/vendor_storage",O_RDWR,0);
	if(sys_fd < 0){
		ERROR("vendor_storage open fail\n");
		return -1;
	}

	req->tag = VENDOR_REQ_TAG;
	req->id = VENDOR_SN_ID;
	req->len = 32; /* data len */
	for (i = 0; i < 32; i++)
		req->data[i] = i;
	rknand_print_hex_data("vendor write:", (uint32*)req, req->len + 8);
	ret = ioctl(sys_fd, VENDOR_WRITE_IO, req);
	if(ret){
		ERROR("vendor write error\n");
		return -1;
	}

	return 0;
}

void rknand_sys_storage_test(void)
{
	vendor_storage_write_test();
	vendor_storage_read_test();
}
