如果需要配置过编译工具或busybox，按照如下步奏：
1. make rockchip_rv1108_defconfig
2. make menuconfig 可以自行进行配置。单独配置busybox，可用命令：make busybox-menuconfig。
3. make -j4 进行编译。如果只是单独编译busybox，可用命令：make busybox。
4. 编译成功后，output/host会生成交叉编译工具。可将工程的交叉编译工具替换。在./output/target目录下，有生成相应的busybox和lib，将lib目录拷贝到工程目录<sdkroot>/common/system/lib目录下，将bin/busybox 拷贝到cvr工程目录<sdkroot>/common/root/bin目录下即可。
