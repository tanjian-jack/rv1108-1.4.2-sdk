################################################################################
# RV extra setting for cmake.
#
################################################################################

#$(warning $(TARGET_CFLAGS))

CMAKE := $(shell which cmake || type -p cmake)
CMAKE_VERSION := $(shell $(CMAKE) --version | \
	sed -n -r 's/^.* ([0-9]*)\.([0-9]*)\.([0-9]*)[ ]*.*/\1 \2/p')

ifeq ($(CMAKE_VERSION),)
$(error "CMake is not installed")
endif

$(BUILD_DIR)/toolchainfile.cmake:
	$(Q)mkdir -p $(@D)
	$(Q)sed \
		-e 's#@@STAGING_SUBDIR@@#$(call qstrip,$(STAGING_SUBDIR))#' \
		-e 's#@@TARGET_CFLAGS@@#$(call qstrip,$(TARGET_CFLAGS))#' \
		-e 's#@@TARGET_CXXFLAGS@@#$(call qstrip,$(TARGET_CXXFLAGS))#' \
		-e 's#@@TARGET_LDFLAGS@@#$(call qstrip,$(TARGET_LDFLAGS))#' \
		-e 's#@@TARGET_CC@@#$(call qstrip,$(TARGET_CC))#' \
		-e 's#@@TARGET_CXX@@#$(call qstrip,$(TARGET_CXX))#' \
		-e 's#@@CMAKE_SYSTEM_PROCESSOR@@#$(call qstrip,$(CMAKE_SYSTEM_PROCESSOR))#' \
		$(CONFIG_DIR)/toolchainfile.cmake.in \
		> $@

HOST_BIN_PATH = $(HOST_DIR)/usr/bin

host-cmake: $(BUILD_DIR)/toolchainfile.cmake
	$(Q)$(CMAKE) --version >/dev/null

host-cmake-show-build-order:
	$(info host-cmake)

$(HOST_BIN_PATH)/cmake:
	$(Q)mkdir -p $(@D) && ln -s $(CMAKE) $(@)

.PHONY: host-cmake host-cmake-show-build-order

# A generic same part cmake words for rv's libraries.
# $(1):pkg, $(2):PKG, $(3):pkgdir
define inner-rv-generic-cmake

$(call rv-generic-var-define,$(2),$(3))

$(2)_INSTALL_TARGET_OPTS += install/fast

# Some CMakeLists.txt in rv's libs take the CMAKE_INSTALL_PREFIX as the destination rather than
# $(DESTDIR)/$(INSTALL_PREFIX), so set CMAKE_INSTALL_PREFIX the realpath of destination.
define $(2)_CONFIGURE_CMDS
	$$($$(PKG)_MAKECLEAN_CMDS)
	$(Q)(mkdir -p $$($$(PKG)_BUILDDIR) && \
	cd $$($$(PKG)_BUILDDIR) && \
	rm -f CMakeCache.txt && \
	PATH=$$(BR_PATH) \
	$$($$(PKG)_CONF_ENV) $$(CMAKE) $$($$(PKG)_SITE) \
		-DCMAKE_TOOLCHAIN_FILE="$$(BUILD_DIR)/toolchainfile.cmake" \
		-DCMAKE_BUILD_TYPE=$$(if $$(RV_ENABLE_DEBUG),Debug,Release) \
		-DCMAKE_INSTALL_PREFIX=$$(TARGET_DIR) \
		-DCMAKE_COLOR_MAKEFILE=ON \
		$$(CMAKE_QUIET) \
		$$($$(PKG)_CONF_OPTS) \
	)
endef

define $(2)_MAKECLEAN_CMDS
	$(Q)$$(if $$(wildcard $$($$(PKG)_BUILDDIR)),\
	cat $$($$(PKG)_BUILDDIR)install_manifest.txt | xargs rm -rf)
	$(Q)rm -rf $$($$(PKG)_BUILDDIR)
endef
#	$$(TARGET_MAKE_ENV) $$($$(PKG)_MAKE_ENV) $$($$(PKG)_MAKE) clean -C $$($$(PKG)_BUILDDIR)

$(call inner-cmake-package,$(1),$(2),$(2),target)

$$($(2)_TARGET_CONFIGURE): $(pkgdir)CMakeLists.txt

endef

remove-last-slash = $(shell echo $(1) | sed "s/\/$$//")

rv-generic-cmake = $(call inner-rv-generic-cmake,$(pkgname),$(call UPPERCASE,$(pkgname)),\
	$(call remove-last-slash,$(pkgdir)))
