PKG_CONFIG_HOST_BINARY := $(shell which pkg-config || type -p pkg-config)
PKG_CONFIG_VERSION := $(shell $(PKG_CONFIG_HOST_BINARY) --version 2>/dev/null)

ifeq ($(PKG_CONFIG_VERSION),)
$(error "pkg-config is not installed")
endif

host-pkgconf:
	$(Q)$(PKG_CONFIG_HOST_BINARY) --version >/dev/null
host-pkgconf-show-build-order:
	$(info host-pkgconf)

.PHONY: host-pkgconf host-pkgconf-show-build-order

# Improve the pkgname as the name of rvmk, conveniently modify the pkgname for future,
# since the git folder name can not be changed optionally.
pkgname = $(basename $(notdir $(lastword $(MAKEFILE_LIST))))
rvmk_file_path = $(lastword $(MAKEFILE_LIST))

DISPABLECHARS = 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_

# $(1):PKG, $(2):pkgdir
define rv-generic-var-define
$(1)_VERSION := $(if $(wildcard $(RV_TOPDIR)/.git),NO_GIT_IN_SUBMODULE,\
	$(shell cd $(2) && git show --oneline --quiet HEAD | sed "s/[^$(DISPABLECHARS)]/_/g"))
#$$(info $$($(1)_VERSION))
$(1)_INSTALL_STAGING = NO
$(1)_SITE := $(2)
$(1)_SITE_METHOD = local

$(1)_BUILD_FROM_ORIGINAL_SOURCE = YES
$(1)_INSTALL_TARGET_OPTS ?=
# Some CMakeLists.txt in rv's libs take the CMAKE_INSTALL_PREFIX as the destination rather than
# $(DESTDIR)/$(INSTALL_PREFIX), so set DESTDIR empty.
$(1)_INSTALL_TARGET_OPTS += $(if $(QUIET),-s,) DESTDIR=
endef

# A generic same words for rv's libraries which be built by makefile.
# $(1):pkg, $(2):PKG, $(3):pkgdir, $(4): make opts
define inner-rv-generic-makefile

$(call rv-generic-var-define,$(2),$(3))

$(2)_MAKE_OPTS ?=
$(2)_MAKE_OPTS += $(4)
$(2)_MAKE_OPTS += $(if $(QUIET),-s,)
$(2)_INSTALL_TARGET_OPTS += install

ifndef $(2)_CONFIGURE_CMDS
define $(2)_CONFIGURE_CMDS
	$(Q)mkdir -p $$($$(PKG)_BUILDDIR)
	$$($$(PKG)_MAKECLEAN_CMDS)
endef
endif

ifndef $(2)_BUILD_CMDS
define $(2)_BUILD_CMDS
	$(Q)cd $$($$(PKG)_PKGDIR) && \
	$$($$(PKG)_MAKE_ENV) $$(MAKE) $$($$(PKG)_MAKE_OPTS)
endef
endif

ifndef $(2)_INSTALL_TARGET_CMDS
define $(2)_INSTALL_TARGET_CMDS
	$(Q)(cd $$($$(PKG)_PKGDIR) && \
		$$($$(PKG)_MAKE_ENV) $$(MAKE) $$($$(PKG)_MAKE_OPTS) $$($$(PKG)_INSTALL_TARGET_OPTS) \
	)
endef
endif

ifndef $(2)_MAKECLEAN_CMDS
define $(2)_MAKECLEAN_CMDS
	$(Q)(cd $$($$(PKG)_PKGDIR) && \
		! test -f Makefile || \
		! $$($$(PKG)_MAKE_ENV) $$(MAKE) $$($$(PKG)_MAKE_OPTS) $$($$(PKG)_CLEAN_OPTS) || true\
	)
endef
endif

$(call inner-generic-package,$(1),$(2),$(2),target)

endef

# A generic same words for rv's libraries which be built by Makefile.
# $(1):pkg, $(2):PKG, $(3):pkgdir
define inner-rv-generic-Makefile
$(2)_CLEAN_OPTS ?= clean
$(call inner-rv-generic-makefile,$(1),$(2),$(3),$(TARGET_CONFIGURE_OPTS))
ifneq ($(wildcard $(pkgdir)/Makefile),)
$$($(2)_TARGET_CONFIGURE): $(pkgdir)/Makefile
endif
endef

# A generic same words for rv's libraries which be built by auto-configure.
# $(1):pkg, $(2):PKG, $(3):pkgdir
define inner-rv-generic-configure

$(2)_CLEAN_OPTS ?= distclean

define REMOVE_TARGET_CONFIGURE
	$(Q)rm -f $$($$(PKG)_TARGET_CONFIGURE)
	$(Q)rm -f $$($$(PKG)_TARGET_INSTALL_TARGET)
endef

ifeq ($$($(2)_CLEAN_OPTS), distclean)
$(2)_POST_MAKECLEAN_TARGET_HOOKS = REMOVE_TARGET_CONFIGURE
endif

ifndef $(2)_CONFIGURE_CMDS
define $(2)_CONFIGURE_CMDS
	$(Q)(mkdir -p $$($$(PKG)_BUILDDIR) && \
		cd $$($$(PKG)_PKGDIR) && rm -rf config.cache && \
		$$(TARGET_CONFIGURE_OPTS) \
		$$(TARGET_CONFIGURE_ARGS) \
		$$($$(PKG)_CONF_ENV) \
		./configure $$($$(PKG)_CONF_OPTS) \
	)
endef
endif

$(call inner-rv-generic-makefile,$(1),$(2),$(3),)

endef

rv-generic-makefile = $(call inner-rv-generic-Makefile,$(pkgname),$(call UPPERCASE,$(pkgname)),\
	$(call remove-last-slash,$(pkgdir)))
rv-generic-configure = $(call inner-rv-generic-configure,$(pkgname),$(call UPPERCASE,$(pkgname)),\
	$(call remove-last-slash,$(pkgdir)))
