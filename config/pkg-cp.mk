# recursive wildcard
rwildcard=$(wildcard $(1)/$(2)) \
	$(foreach d,$(wildcard $(1)/*),$(call rwildcard,$(d),$(2)))

# A generic same words for rv's libraries which is binary, just copy the
# libs and headers into destdir.
# $(1):pkg, $(2):PKG, $(3):pkgdir
define inner-rv-generic-cp

$(call rv-generic-var-define,$(2),$(3))

# show the command of cp rm or not
SHOW_CP_RM = $(if $(QUIET),$(Q),)

$(2)_CUR_LIB_PATH ?=
$(2)_CUR_SBIN_PATH ?=
$(2)_CUR_HDR_PATH ?=

$(2)_TARGET_FILES ?= $$(if $$($(2)_LIBS),$$(addprefix $$($(2)_PKGDIR)$$($(2)_CUR_LIB_PATH)/,$$($(2)_LIBS)),) \
	$$(if $$($(2)_SBINS),$$(addprefix $$($(2)_PKGDIR)$$($(2)_CUR_SBIN_PATH)/,$$($(2)_SBINS)),) \
	$$(if $$($(2)_HDRS),$$(addprefix $$($(2)_PKGDIR)$$($(2)_CUR_HDR_PATH)/,$$($(2)_HDRS)),\
		$$(if $$($(2)_CUR_HDR_PATH),$$(call rwildcard,$$($(2)_PKGDIR)$$($(2)_CUR_HDR_PATH),*.*)))

# out/system/include/?
$(2)_INSTALL_TARGET_INC_DIRNAME ?=

ifndef $(2)_CONFIGURE_CMDS
define $(2)_CONFIGURE_CMDS
	$(Q)mkdir -p $$($$(PKG)_BUILDDIR)
	$(Q)$$(if $$(strip $$($(2)_TARGET_FILES)),touch $$($$(PKG)_TARGET_FILES))
endef
endif

ifndef $(2)_INSTALL_TARGET_CMDS
define $(2)_INSTALL_TARGET_CMDS
	$$(SHOW_CP_RM)$$(if $$($$(PKG)_LIBS),\
		cd $$($$(PKG)_PKGDIR)$$($$(PKG)_CUR_LIB_PATH) && \
		install -d $$(TARGET_DIR)/lib && \
		cp -d $$($$(PKG)_LIBS) $$(TARGET_DIR)/lib;) \
		 \
		$$(if $$($$(PKG)_SBINS),\
		cd $$($$(PKG)_PKGDIR)$$($$(PKG)_CUR_SBIN_PATH) && \
		install -d $$(TARGET_DIR)/sbin && \
		install -C $$($$(PKG)_SBINS) $$(TARGET_DIR)/sbin;) \
		 \
		$$(if $$($$(PKG)_CUR_HDR_PATH),\
		cd $$($$(PKG)_PKGDIR)$$($$(PKG)_CUR_HDR_PATH) && \
		install -d $$(TARGET_DIR)/include/$$($$(PKG)_INSTALL_TARGET_INC_DIRNAME) && \
		cp -r ./* $$(TARGET_DIR)/include/$$($$(PKG)_INSTALL_TARGET_INC_DIRNAME);)
endef
endif

ifndef $(2)_MAKECLEAN_CMDS
define $(2)_MAKECLEAN_CMDS
	$$(SHOW_CP_RM)$$(if $$($$(PKG)_LIBS),! test -d $$(TARGET_DIR)/lib || cd $$(TARGET_DIR)/lib && \
		rm -rf $$($$(PKG)_LIBS),)
	$$(SHOW_CP_RM)$$(if $$($$(PKG)_SBINS),! test -d $$(TARGET_DIR)/sbin || cd $$(TARGET_DIR)/sbin && \
		rm -rf $$($$(PKG)_SBINS),)
	$$(SHOW_CP_RM)$$(if $$($$(PKG)_INSTALL_TARGET_INC_DIRNAME),\
		rm -rf $$(TARGET_DIR)/include/$$($$(PKG)_INSTALL_TARGET_INC_DIRNAME),)
endef
endif

define remove_stamp_configured
	$(Q)rm -rf $$($$(PKG)_TARGET_CONFIGURE)
endef

$(2)_POST_MAKECLEAN_TARGET_HOOKS += remove_stamp_configured

$(call inner-generic-package,$(1),$(2),$(2),target)

endef

rv-generic-cp = $(call inner-rv-generic-cp,$(pkgname),$(call UPPERCASE,$(pkgname)),\
	$(call remove-last-slash,$(pkgdir)))
