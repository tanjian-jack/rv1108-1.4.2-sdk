pack-kernel:
	$(Q)$(if $(RV_BOARD_VERSION),,\
		$(error 'board version is null, forget make defconfig?'))
	@$(call MESSAGE,"Package kernel.img($(RV_BOARD_VERSION))")
	@echo "(rewrite kernel.img with dtb..."
	$(Q)cat $(KERNEL_PKGDIR)/arch/arm/boot/zImage $(KERNEL_PKGDIR)/arch/arm/boot/dts/$(call qstrip,$(RV_BOARD_VERSION)).dtb > $(KERNEL_PKGDIR)/zImage-dtb
	$(Q)$(RV_TOPDIR)/build/kernelimage --pack --kernel $(KERNEL_PKGDIR)/zImage-dtb $(KERNEL_PKGDIR)/kernel.img 0x62000000 > /dev/null
	$(Q)rm -f $(KERNEL_PKGDIR)/zImage-dtb
	@ls -sh $(KERNEL_PKGDIR)/kernel.img
	@echo " ...done)"

.PHONY: pack-kernel
