ROOTFS_SYS_LIBS_PATH := $(RV_TOPDIR)/common/system/lib
ROOTFS_SYS_LIBS := $(call rwildcard,$(ROOTFS_SYS_LIBS_PATH),*)
$(TARGET_SYS_LIBS): $(ROOTFS_SYS_LIBS_PATH) $(ROOTFS_SYS_LIBS)
	cp -rf $(ROOTFS_SYS_LIBS) $(@D)
	touch $(@)

ROOTFS_SHARE_PATH := $(RV_TOPDIR)/common/system/share
$(TARGET_SHARE): $(ROOTFS_SHARE_PATH) $(call rwildcard,$(ROOTFS_SHARE_PATH),*)
	rm -rf $(@D)/*
	cp -Rf $(ROOTFS_SHARE_PATH)/* $(@D)
	touch $(@)

ROOTFS_FINAL_BINS =
ROOTFS_FINAL_SBINS =
ROOTFS_FINAL_LIBS = $(patsubst $(ROOTFS_SYS_LIBS_PATH)/%,%,$(ROOTFS_SYS_LIBS))
ROOTFS_FINAL_ETCS =
ROOTFS_FINAL_SHARES =

CUSTOM_DIR := $(BASE_DIR)/custom
OUT_SYSROOT_DIR := $(BASE_DIR)/outImage
OUT_IMAGE_DIR := $(RV_TOPDIR)/rockimg/Image-cvr

# copy the targets to this dir
OUT_SYSROOT_USR_DIR = $(OUT_SYSROOT_DIR)/root/usr/local

ifeq ($(RV_TARGET_MAIN_APP),y)
ROOTFS_FINAL_BINS += video
endif

ifeq ($(RV_TARGET_WLAN_SERVICE),y)
ROOTFS_FINAL_BINS += wlan_service
endif

ifeq ($(RV_TARGET_LOGWRAP),y)
ROOTFS_FINAL_BINS += logwrapper
endif

ifeq ($(RV_TARGET_ADBD),y)
ROOTFS_FINAL_SBINS += adbd
endif

ifeq ($(RV_TARGET_FSCK_MSDOS),y)
ROOTFS_FINAL_SBINS += fsck_modos
endif

ifeq ($(RV_TARGET_IO),y)
ROOTFS_FINAL_SBINS += io
endif

ifeq ($(RV_TARGET_TINYALSA),y)
ROOTFS_FINAL_LIBS += libtinyalsa.so
ROOTFS_FINAL_BINS += tinyplay tinycap tinymix tinypcminfo
endif

ifeq ($(RV_TARGET_IPERF),y)
ROOTFS_FINAL_SBINS += iperf ethtool
ROOTFS_FINAL_LIBS += libnetutils.so
endif

ifeq ($(RV_TARGET_DEBUG_TOOLS_BLKPARSE),y)
ROOTFS_FINAL_SBINS += blkparse
endif

ifeq ($(RV_TARGET_DEBUG_TOOLS_BLKTRACE),y)
ROOTFS_FINAL_SBINS += blktrace
endif

ifeq ($(RV_TARGET_DEBUG_TOOLS_STRACE),y)
ROOTFS_FINAL_SBINS += strace
endif

ifeq ($(RV_TARGET_DEBUG_TOOLS_PERF),y)
ROOTFS_FINAL_SBINS += perf
ROOTFS_FINAL_LIBS += libdwfl.so  libdw.so  libebl.so  libelf.so  libperf.so
endif

ifeq ($(RV_TARGET_GDBSERVER),y)
ROOTFS_FINAL_SBINS += gdbserver
endif

ifeq ($(RV_TARGET_DNSMASQ),y)
ROOTFS_FINAL_SBINS += dnsmasq
endif

ifeq ($(RV_TARGET_LIGHTTPD),y)
ROOTFS_FINAL_SBINS += lighttpd
ROOTFS_FINAL_LIBS += libpcre.so libpcre.so.0 libpcre.so.0.0.1 mod_indexfile.so \
	mod_dirlisting.so mod_staticfile.so mod_access.so mod_cgi.so
ROOTFS_FINAL_ETCS += lighttpd.conf
endif

ifeq ($(RV_TARGET_HOSTAPD),y)
ROOTFS_FINAL_SBINS += hostapd hostapd_cli
endif

ifeq ($(RV_TARGET_WPA_SUPPLICANT),y)
ROOTFS_FINAL_SBINS += wpa_cli wpa_supplicant
endif

ifeq ($(RV_TARGET_OPENSSL),y)
ROOTFS_FINAL_LIBS += libcrypto.so libcrypto.so.1.0.0 libssl.so.1.0.0 libssl.so
endif

ifeq ($(RV_TARGET_LIBNL),y)
ROOTFS_FINAL_LIBS += libnl.so.1 libnl.so.1.1.4 libnl.so
endif

ifeq ($(RV_TARGET_LIBMINIGUI),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libminigui_ths.so libminigui_ths-3.0.so.12 libminigui_ths-3.0.so.12.0.0
endif
ROOTFS_FINAL_ETCS += MiniGUI.cfg
endif

ifeq ($(RV_TARGET_ZLIB),y)
ROOTFS_FINAL_LIBS += libz.so libz.so.1 libz.so.1.2.3
endif

ifeq ($(RV_TARGET_LIBION),y)
ROOTFS_FINAL_LIBS += libion.so
endif

ifeq ($(RV_TARGET_LIBPNG),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libpng.so libpng12.so libpng12.so.0 libpng12.so.0.37.0
endif
endif

ifeq ($(RV_TARGET_MPP),y)
ROOTFS_FINAL_LIBS += libmpp.so.0 libmpp.so.1 libvpu.so.0 libvpu.so.1
endif

ifeq ($(RV_TARGET_LIBJPEG),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libjpeg.so libjpeg.so.7 libjpeg.so.7.0.0
endif
endif

ifeq ($(RV_TARGET_LIBFDK_AAC),y)
ifneq ($(RV_TARGET_LIBFDK_AAC_STATIC_LIB),y)
ROOTFS_FINAL_LIBS += libfdk-aac.so libfdk-aac.so.1 libfdk-aac.so.1.0.0
endif
endif

ifeq ($(RV_TARGET_LIBALSA),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libsalsa.so.0.0.1 libsalsa.so.0 libsalsa.so
endif
endif

ifeq ($(RV_TARGET_LIBALSA_RKVOICE_PROCESS),y)
ROOTFS_FINAL_LIBS += libvoice_process.so
endif

ifeq ($(RV_TARGET_LIBFS_MANAGE),y)
ROOTFS_FINAL_LIBS += libfsmanage.so
endif

ifeq ($(RV_TARGET_DPP),y)
ROOTFS_FINAL_LIBS += libdpp.so
endif

ifeq ($(RV_TARGET_DPP_TEST),y)
ROOTFS_FINAL_BINS += dsp_test fast9_test histogram_test ccl_test gftt_test \
		image_warp_test image_warp_affine_test hog_test canny_test klt_test \
		orb_test pyramid_test pyramiddetect_test brief_matcher_test
endif

ifeq ($(RV_TARGET_LIBDVS),y)
ROOTFS_FINAL_LIBS += libdvs.so
endif

ifeq ($(RV_TARGET_LIBODT_ADAS),y)
ROOTFS_FINAL_LIBS += libodt_adas.so
endif

ifeq ($(RV_TARGET_LIBRK_BACKTRACE),y)
ROOTFS_FINAL_LIBS += librk_backtrace.so
endif

ifeq ($(RV_TARGET_LIBCAMERAHAL),y)
ROOTFS_FINAL_LIBS += libcam_hal.so
endif

ifeq ($(RV_TARGET_LIBIEP),y)
ROOTFS_FINAL_LIBS += libiep.so
endif

ifeq ($(RV_TARGET_LIBRKFB),y)
ROOTFS_FINAL_LIBS += librkfb.so
endif

ifeq ($(RV_TARGET_LIBRKRGA),y)
ROOTFS_FINAL_LIBS += librkrga.so
endif

ifeq ($(RV_TARGET_FREETYPE),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libfreetype.so libfreetype.so.6 libfreetype.so.6.12.3
endif
endif

ifeq ($(RV_TARGET_LIBTS),y)
ROOTFS_FINAL_LIBS += libts.so libts-1.3.so.0 libts-1.3.so.0.1.3
endif

ifeq ($(RV_TARGET_SPHINXBASE),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libsphinxbase.so.3.0.0 libsphinxbase.so.3 libsphinxbase.so libsphinxad.so.3.0.0 libsphinxad.so.3 libsphinxad.so
endif
endif

ifeq ($(RV_TARGET_POCKETSPHINX),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS +=  libpocketsphinx.so.3.0.0 libpocketsphinx.so.3 libpocketsphinx.so
endif
endif

ifeq ($(RV_TARGET_RKSTREAMER),y)
ROOTFS_FINAL_BINS += rk-streamer
endif

ifeq ($(RV_TARGET_ZS_GPSLIB),y)
ROOTFS_FINAL_BINS += martianFW.bin miniBL.bin zsgps.conf
endif

ifeq ($(RV_TARGET_LIBEDOG),y)
ROOTFS_FINAL_LIBS += libedog.so
endif

ifeq ($(RV_TARGET_TUTK),y)
ifeq ($(RV_SHARED_LIBS),y)
ROOTFS_FINAL_LIBS += libAVAPIs.so libAVAPIsT.so libIOTCAPIs.so libIOTCAPIsT.so \
		libP2PTunnelAPIs.so libP2PTunnelAPIsT.so libRDTAPIs.so libRDTAPIsT.so
endif
endif

ifeq ($(RV_TARGET_OPENCV),y)
ROOTFS_FINAL_LIBS += libopencv_core.so libopencv_core.so.3.2 libopencv_core.so.3.2.0 \
		libopencv_imgproc.so  libopencv_imgproc.so.3.2  libopencv_imgproc.so.3.2.0 \
		libopencv_objdetect.so libopencv_objdetect.so.3.2 libopencv_objdetect.so.3.2.0 \
		libopencv_highgui.so libopencv_highgui.so.3.2 libopencv_highgui.so.3.2.0 \
		libopencv_imgcodecs.so libopencv_imgcodecs.so.3.2 libopencv_imgcodecs.so.3.2.0 \
		libopencv_ml.so libopencv_ml.so.3.2 libopencv_ml.so.3.2.0 \
		libopencv_videoio.so libopencv_videoio.so.3.2 libopencv_videoio.so.3.2.0
endif

strip_files_under = find $(1) -type f -name "*" | xargs -r $(STRIPCMD) $(STRIP_STRIP_ALL) $(if $(Q),2>/dev/null) || true
rm_files_under = find $(1) -type f -name $(2) | xargs -r rm -f

define search_iq
	temp=`grep "cif_isp0" -A 6 $(1) | sed ':a;N;$$!ba;s/\n/ /g' | cut -d '{' -f2 | cut -d '}' -f1`; \
	iq_name=; \
	[[ $$temp =~ "okay" ]] && \
		camera=`echo "$$temp" | grep "rockchip,camera-modules-attached" | cut -d '<' -f2|cut -d '>' -f1 | sed "s/&camera/camera/g"`; \
		cam_dts=; \
		for i in $$camera; do \
			temp=`grep "$$i:" -A 35 $(1) | sed ':a;N;$$!ba;s/\n/ /g' | cut -d '{' -f2 | cut -d '}' -f1`; \
			if [ -z "$$temp" ]; then \
				[[ -z "$$cam_dts" ]] && cam_dts=`grep "&i2c1" -A 10 $(1) | grep "^#include" | cut -d '"' -f2|cut -d '"' -f1`; \
				temp=`grep "$$i:" -A 35 $(RV_TOPDIR)/kernel/arch/arm/boot/dts/$$cam_dts | sed ':a;N;$$!ba;s/\n/ /g' | cut -d '{' -f2 | cut -d '}' -f1`; \
				[[ $$temp =~ "okay" ]] && iq_name="$$iq_name `echo "$$temp" | awk -F 'rockchip,camera-module-name' '{print $$2}' | cut -d '"' -f2|cut -d '"' -f1`"; \
			else \
				[[ $$temp =~ "okay" ]] && iq_name="$$iq_name `echo "$$temp" | awk -F 'rockchip,camera-module-name' '{print $$2}' | cut -d '"' -f2|cut -d '"' -f1`"; \
			fi; \
		done; \
	temp=`ls $(2)`; \
	[[ -z "$$iq_name" ]] && iq_name="cam_default"; \
	echo "camera module name:$$iq_name"; \
	for i in $$iq_name; do \
		for j in $$temp; do [[ $$j =~ $$i ]] && temp=`echo "$$temp" | sed 's/'"$$j"'//g'`; done; \
	done; \
	cd $(2) && rm -f `echo "$$temp"`;
endef

pack-rootfs:
	@$(call MESSAGE,"Package rootfs.img")
	$(Q)rm -rf $(OUT_SYSROOT_DIR)
	$(Q)mkdir -p $(OUT_SYSROOT_DIR)
	$(Q)cp -R $(SYSROOT_DIR) $(OUT_SYSROOT_DIR)/
	$(Q)$(if $(wildcard $(CUSTOM_DIR)),cp -r $(CUSTOM_DIR)/* $(OUT_SYSROOT_DIR)/root)
	@$(call search_iq,$(call qstrip,$(RV_TOPDIR)/kernel/arch/arm/boot/dts/$(RV_BOARD_VERSION).dts),$(OUT_SYSROOT_DIR)/root/etc/cam_iq)
	$(Q)chmod -R a+x $(OUT_SYSROOT_DIR)/root/etc/
	$(Q)mkdir -p $(OUT_SYSROOT_USR_DIR)/bin
	$(Q)mkdir -p $(OUT_SYSROOT_USR_DIR)/sbin
	$(Q)mkdir -p $(OUT_SYSROOT_USR_DIR)/etc
	$(Q)mkdir -p $(OUT_SYSROOT_USR_DIR)/share
	$(Q)mkdir -p $(OUT_SYSROOT_USR_DIR)/etc/dsp
	$(Q)mkdir -p $(OUT_SYSROOT_DIR)/root/lib/firmware
	$(Q)mkdir -p $(OUT_SYSROOT_DIR)/root/lib/ts
	$(Q)cd $(TARGET_DIR)/bin && cp -r $(ROOTFS_FINAL_BINS) $(OUT_SYSROOT_USR_DIR)/bin/
	$(Q)cd $(TARGET_DIR)/sbin && cp -r $(ROOTFS_FINAL_SBINS) $(OUT_SYSROOT_USR_DIR)/sbin/
	$(Q)cd $(TARGET_DIR)/etc && cp -r $(ROOTFS_FINAL_ETCS) $(OUT_SYSROOT_USR_DIR)/etc/ || true
	$(Q)cd $(TARGET_DIR)/etc && cp -r dsp/ $(OUT_SYSROOT_DIR)/root/etc/dsp/
	$(Q)cd $(TARGET_DIR)/lib && cp -r $(ROOTFS_FINAL_LIBS) $(OUT_SYSROOT_DIR)/root/lib/
	$(Q)cd $(TARGET_DIR)/lib/firmware && cp -r * $(OUT_SYSROOT_DIR)/root/lib/firmware/
ifeq ($(RV_TARGET_LIBTS),y)
	$(Q)cd $(TARGET_DIR)/lib/ts && cp -r * $(OUT_SYSROOT_DIR)/root/lib/ts/
endif
	$(Q)mkdir $(OUT_SYSROOT_USR_DIR)/share/minigui && cp -R $(TARGET_DIR)/share/minigui/res $(OUT_SYSROOT_USR_DIR)/share/minigui/
	$(Q)mkdir $(OUT_SYSROOT_USR_DIR)/share/sounds && cp -R $(TARGET_DIR)/share/sounds/* $(OUT_SYSROOT_USR_DIR)/share/sounds/
ifneq ($(RV_PCBA_ENABLE),y)
	$(Q)rm -f $(OUT_SYSROOT_USR_DIR)/share/sounds/pcba.wav
endif
	$(Q)cp -Ra $(OUT_SYSROOT_DIR)/root $(OUT_SYSROOT_DIR)/symbols
	$(Q)$(call rm_files_under,$(OUT_SYSROOT_DIR)/root,".*")
	$(Q)$(call rm_files_under,$(OUT_SYSROOT_DIR)/root/lib/ts/,"*.a")
	$(Q)$(call rm_files_under,$(OUT_SYSROOT_DIR)/root/lib/ts/,"*.la")
	$(Q)chmod -R a+w $(OUT_SYSROOT_DIR)/root/bin/
	$(Q)chmod -R a+w $(OUT_SYSROOT_DIR)/root/sbin/
	$(Q)chmod -R a+w $(OUT_SYSROOT_DIR)/root/lib/
	$(Q)chmod -R a+w $(OUT_SYSROOT_USR_DIR)/bin/
	$(Q)chmod -R a+w $(OUT_SYSROOT_USR_DIR)/sbin/
# strip bin and libs
ifneq ($(STRIPCMD),)
	$(Q)$(call strip_files_under,$(OUT_SYSROOT_DIR)/root/bin/)
	$(Q)$(call strip_files_under,$(OUT_SYSROOT_DIR)/root/sbin/)
	$(Q)$(call strip_files_under,$(OUT_SYSROOT_DIR)/root/lib/)
	$(Q)$(call strip_files_under,$(OUT_SYSROOT_USR_DIR)/bin/)
	$(Q)$(call strip_files_under,$(OUT_SYSROOT_USR_DIR)/sbin/)
endif
	@echo "(create rootfs.img without kernel..."
	$(Q)mkdir -p $(OUT_IMAGE_DIR)
	$(Q)rm -f $(OUT_IMAGE_DIR)/rootfs.img
	$(Q)$(RV_TOPDIR)/build/mksquashfs \
		$(OUT_SYSROOT_DIR)/root $(OUT_IMAGE_DIR)/rootfs.tmp \
		-nopad -noappend \
		-root-owned \
		-comp gzip \
		-b 256k \
		-p '/dev/console c 600 0 0 5 1' >/dev/null
	$(Q)dd if=$(OUT_IMAGE_DIR)/rootfs.tmp \
		of=$(OUT_IMAGE_DIR)/rootfs.img \
		bs=128k conv=sync
	$(Q)rm -f $(OUT_IMAGE_DIR)/rootfs.tmp
	@ls -sh $(OUT_IMAGE_DIR)/rootfs.img
	@echo " ...done)"

.PHONY: pack-rootfs
