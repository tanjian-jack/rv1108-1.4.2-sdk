/**
 * Copyright (C) 2016 Fuzhou Rockchip Electronics Co., Ltd
 * author: ZhiChao Yu zhichao.yu@rock-chips.com
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef WLAN_SERVICE_CONSOLE_H_
#define WLAN_SERVICE_CONSOLE_H_

namespace rockchip {
namespace utils {

class Console {
 public:
  static bool Run(const char* cmdline, bool daemon = false);
};

}  // namespace utils
}  // namespace rockchip

#endif
