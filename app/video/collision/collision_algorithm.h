/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: cherry chen <cherry.chen@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "../gsensor.h"

#define COL_G 9.8
#define COL_PI 3.1415926
#define COL_M 30
#define COL_N 20
#define COL_FRAME 10

struct gsensor_axis_data {
    struct sensor_axis sensor_axis[COL_FRAME];
};

struct feature {
    float smv_avg;
    float smv_sigma;
    float smv_max;
    float smv_min;
    float angle_x;
    float angle_y;
    float angle_z;
};

enum collision_state {
    COLLISION_NOTHING = 0,
    COLLISION_HAPPEN,
    COLLISION_CONTINUE,
    COLLISION_TURNOVER,
    COLLISION_ERROR
};

struct col_state {
    int frame_count_in;
    int state_count;

    /* Not be used now, maybe add rollover detection then */
    int rollover_flag;
    int rollver_count;
};

enum collision_state collision_check(struct gsensor_axis_data *data_input, int level);
