/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __GPS_CTRL_H__
#define __GPS_CTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define GPS_DEBUG
#ifdef GPS_DEBUG
#define GPS_DBG(...) printf(__VA_ARGS__)
#else
#define GPS_DBG(...)
#endif

#define FILE_NAME_LEN 128
#define GPS_BUF_LEN 200
#define MAX_GPS_INFO_LEN 50

typedef void (* send_cb)(const char *, int, int);

struct gps_data {
    int len;
    char buffer[GPS_BUF_LEN];
};

struct gps_send_data {
    send_cb send_data;
    struct gps_send_data *pre;
    struct gps_send_data *next;
};

int gps_init(send_cb gps_send_callback);
int gps_deinit(void);

int gps_write_init(void);
void gps_write_deinit(void);

void gps_file_open(const char *filename);
void gps_file_close(void);

#if USE_GPS_MOVTEXT
void gps_set_encoderhandler(void * handler);
#endif

#ifdef __cplusplus
}
#endif
#endif
