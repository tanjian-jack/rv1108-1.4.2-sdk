/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "yuyv_process.hpp"

int convert_yuyv(int width,
                 int height,
                 void* srcbuf,
                 void* dstbuf,
                 int neadshownum)
{
    int *dstint_y, *dstint_uv, *srcint;
    int y_size = width * height;
    int i, j;

    dstint_y = (int*)dstbuf;
    srcint = (int*)srcbuf;
    dstint_uv = (int*)((char*)dstbuf + y_size);

    for (i = 0; i < height; i++) {
        for (j = 0; j<width >> 2; j++) {
            if (i % 2 == 0) {
                *dstint_uv++ =
                    (*(srcint + 1) & 0xff000000) | ((*(srcint + 1) & 0x0000ff00) << 8) |
                    ((*srcint & 0xff000000) >> 16) | ((*srcint & 0x0000ff00) >> 8);
            }
            *dstint_y++ = ((*(srcint + 1) & 0x00ff0000) << 8) |
                          ((*(srcint + 1) & 0x000000ff) << 16) |
                          ((*srcint & 0x00ff0000) >> 8) | (*srcint & 0x000000ff);
            srcint += 2;
        }
    }

    return 0;
}

