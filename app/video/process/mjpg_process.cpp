/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mjpg_process.hpp"

int mjpg_decode(struct Video* video,      void* srcbuf, unsigned int size, void* dstbuf, int dstfd)
{
    char* in_data = (char*)srcbuf;
    unsigned int in_size = size;
    int i = 0;
    int ret = 0;

    /* MJPEG buffer data should end of 0xFF 0xD9, the size should be 2 at least */
    if (in_size < 2) {
        printf("%s in_size error!\n", __func__);
        ret = -1;
        goto exit_mjpg_decode;
    }

    video->jpeg_dec.decoding = true;
    if (!video->pthread_run)
        goto exit_mjpg_decode;

    /* some USB camera input buffer data may not end of 0xFF 0xD9, it may align 16 Bytes */
    while (*(in_data + in_size - 2) != 0xFF || *(in_data + in_size - 1) != 0xD9) {
        in_size--;
        if (i++ > 16 || in_size < 2) {
            printf("err mjpg data.\n");
            ret = -1;
            goto exit_mjpg_decode;
        }
    }

    if (vpu_decode_jpeg_doing(video->jpeg_dec.decode, in_data, in_size,
                              dstfd, (char*)dstbuf)) {
        printf("vpu_decode_jpeg_doing err\n");
        ret = -1;
        goto exit_mjpg_decode;
    }

exit_mjpg_decode:
    video->jpeg_dec.decoding = false;
    return ret;
}

static int video_save_jpeg(struct Video* video,
                           char* filename,
                           void* srcbuf,
                           unsigned int size)
{
    return fs_picture_mjpg_write(filename, srcbuf, size);
}

int video_record_save_jpeg(struct Video* video,            void* srcbuf, unsigned int size)
{
    char filename[128];

    video_record_getfilename(filename, sizeof(filename),
                             fs_storage_folder_get_bytype(video->type, PICFILE_TYPE),
                             video->deviceid, "jpg", "");
    video_save_jpeg(video, filename, srcbuf, size);

    video_record_takephoto_end(video);

    return 0;
}

