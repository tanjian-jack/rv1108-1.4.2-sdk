/*
 * DSP process example
 *
 * Copyright (C) 2018 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>

#include <CameraHal/StrmPUBase.h>

#define DMA_1D_TRANSFER  0
#define DMA_2D_TRANSFER  1
#define TEST_MODE        DMA_1D_TRANSFER

// This example process uses DSP DDMA to copy buffer from src to dst.
struct dsp_memcpy_params {
    uint32_t src;
    uint32_t dst;

    uint32_t width;
    uint32_t height;

    uint32_t block_width;
    uint32_t block_height;

    uint32_t mode;
    uint32_t cost_cycles;
};

class DspMemcpy : public StreamPUBase {
 public:
    DspMemcpy() : StreamPUBase("DspMemcpy", true, false) {
        dsp_fd_ = open("/dev/dsp", O_RDWR);
        assert(dsp_fd_ >= 0);
    }

    ~DspMemcpy() {
        close(dsp_fd_);
        dsp_fd_ = -1;
    }

    bool processFrame(shared_ptr<BufferBase> inBuf,
                      shared_ptr<BufferBase> outBuf) {

        struct dsp_memcpy_params copy_params;
        struct dsp_user_work dsp_work;

        memset(&dsp_work, 0, sizeof(dsp_work));
        memset(&copy_params, 0, sizeof(copy_params));

        copy_params.src = (uint32_t)inBuf->getPhyAddr();
        copy_params.dst = (uint32_t)outBuf->getPhyAddr();
        copy_params.mode = TEST_MODE;
        copy_params.width = inBuf->getWidth();
        copy_params.height = inBuf->getHeight() * 3 / 2;
        copy_params.block_width = inBuf->getWidth() / 10;
        copy_params.block_height = inBuf->getHeight() / 10;

        dsp_work.magic = DSP_ALGORITHM_WORK_MAGIC;
        dsp_work.id = 0x78787878;
        dsp_work.algorithm.type = DSP_ALGORITHM_COPY;
        dsp_work.algorithm.packet_virt = (uint32_t)&copy_params;
        dsp_work.algorithm.size = sizeof(copy_params);

        ioctl(dsp_fd_, DSP_IOC_QUEUE_WORK, &dsp_work);
        ioctl(dsp_fd_, DSP_IOC_DEQUEUE_WORK, &dsp_work);
        return true;
    }

 private:
    int dsp_fd_;
};