/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __MJPG_PROCESS_HPP__
#define __MJPG_PROCESS_HPP__

#include "video.hpp"

int mjpg_decode(struct Video* video,      void* srcbuf, unsigned int size, void* dstbuf, int dstfd);
int video_record_save_jpeg(struct Video* video,            void* srcbuf, unsigned int size);

class MJPG_NV12 : public StreamPUBase
{
    struct Video* video;
    bool got_valid_input;
public:
    MJPG_NV12(struct Video* p) : StreamPUBase("MJPG_NV12", true, false),
        video(p), got_valid_input(false) {}
    ~MJPG_NV12() {}

    bool processFrame(shared_ptr<BufferBase> inBuf,
                      shared_ptr<BufferBase> outBuf) {
        if (video->pthread_run && inBuf.get() && outBuf.get()) {
            if (mjpg_decode(video, inBuf->getVirtAddr(), inBuf->getDataSize(),
                            outBuf->getVirtAddr(), (int)(outBuf->getFd()))) {
                if (!got_valid_input) {
                    // If have never got a valid input stream,
                    // do not call the children PU.
                    return false;
                }
                outBuf->setDataSize(0);
            } else {
                got_valid_input = true;
                outBuf->setDataSize(video->width * video->height * 3 / 2);
            }
        }
        return true;
    }
};

class MJPG_Photo : public StreamPUBase
{
    struct Video* video;

public:
    MJPG_Photo(struct Video* p) : StreamPUBase("MJPG_Photo", true, true) {
        video = p;
    }
    ~MJPG_Photo() {}

    bool processFrame(shared_ptr<BufferBase> inBuf,
                      shared_ptr<BufferBase> outBuf) {
        if (video->pthread_run && video->photo.state == PHOTO_ENABLE && inBuf.get()) {
            video->photo.state = PHOTO_BEGIN;
            video_record_save_jpeg(video, inBuf->getVirtAddr(), inBuf->getDataSize());
        }
        return true;
    }
};

#endif
