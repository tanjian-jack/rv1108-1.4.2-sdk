/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __VIDEO_ISP_HPP__
#define __VIDEO_ISP_HPP__

#include "video.hpp"
#include "nv12_process.hpp"
#include "nv12_3dnr.hpp"
#include "nv12_adas.hpp"
#include "nv12_dvs.hpp"

extern HAL_COLORSPACE color_space;
extern bool is_record_mode;

int video_isp_add_policy(struct Video* video);
void video_isp_remove_policy(struct Video* video);
int video_isp_add_mp_policy_fix(struct Video* video);
void video_isp_remove_mp_policy_fix(struct Video* video);
int video_isp_add_sp_policy_fix(struct Video* video);
void video_isp_remove_sp_policy_fix(struct Video* video);
int isp_video_init(struct Video* video,
                   int num,
                   unsigned int width,
                   unsigned int height,
                   unsigned int fps);
int isp_video_path(struct Video* video);
int isp_video_start(struct Video* video);
void isp_video_deinit(struct Video* video);

#if MAIN_APP_NEED_DOWNSCALE_STREAM
bool video_isp_get_enc_scale(struct Video* video);
void video_isp_set_enc_scale(struct Video* video, bool scale);
#endif

#endif
