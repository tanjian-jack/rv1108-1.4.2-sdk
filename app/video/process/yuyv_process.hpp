/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __YUYV_PROCESS_HPP__
#define __YUYV_PROCESS_HPP__

#include "video.hpp"

int convert_yuyv(int width,
                 int height,
                 void* srcbuf,
                 void* dstbuf,
                 int neadshownum);

class YUYV_NV12_Stream : public StreamPUBase
{
    struct Video* video;

public:
    YUYV_NV12_Stream(struct Video* p) : StreamPUBase("YUYV_NV12", true, false) {
        video = p;
    }
    ~YUYV_NV12_Stream() {}
    bool processFrame(shared_ptr<BufferBase> inBuf,
                      shared_ptr<BufferBase> outBuf) {
        if (video->pthread_run && inBuf.get() && outBuf.get()) {
            if (convert_yuyv(video->width, video->height, inBuf->getVirtAddr(),
                             outBuf->getVirtAddr(), parameter_get_video_mark())) {
                video_record_signal(video);
                return false;
            }
            outBuf->setDataSize(video->width * video->height * 3 / 2);
        }

        return true;
    }
};

#endif
