/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __DPP_PROCESS_H__
#define __DPP_PROCESS_H__

#include <CameraHal/StrmPUBase.h>
#include <dpp/dpp_frame.h>
#include <dpp/algo/algorithm_type.h>

using namespace rockchip;

#define DPP_OUT_BUFFER_NUM  3

class DppProcess : public StreamPUBase
{
public:
    DppProcess(struct Video *video, AlgorithmType type, bool shareInBuf);
    DppProcess(struct Video *video, const char* name, AlgorithmType type, bool shareInBuf);
    ~DppProcess();

    //from NewCameraBufferReadyNotifier
    virtual bool bufferReady(weak_ptr<BufferBase> buffer, int status);
    //need process result frame buffer
    virtual bool prepare(const frm_info_t& frmFmt, unsigned int numBuffers,
                         shared_ptr<CameraBufferAllocator> allocator);
    virtual bool start();
    virtual void stop();
protected:
    class ProcessThread : public CamThread
    {
    public:
        ProcessThread(DppProcess* dppProc): mDppProcess(dppProc) {};
        virtual bool threadLoop(void);
    private:
        DppProcess* mDppProcess;
    };
    virtual bool processFrame();
    virtual bool doSomeThing(shared_ptr<BufferBase> outBuf, DppFrame* frame);
    virtual int dppPacketProcess(shared_ptr<BufferBase>& inBuf);

    struct Video *mVideo;
    dpp::DppCore* mDppCore;
    AlgorithmType mAlgorithmType;

};

#endif
