/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __UVC_USER_H__
#define __UVC_USER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <unistd.h>
#include "uvc-gadget.h"

struct uvc_user {
    unsigned int width;
    unsigned int height;
    bool run;
    char* buffer;
    unsigned int size;
    unsigned int fcc;
};

extern struct uvc_user uvc_user;

#define UVC_BUFFER_NUM 3

int uvc_buffer_init();
void uvc_buffer_deinit();
bool uvc_buffer_process_state();
bool uvc_buffer_write_enable();
void uvc_buffer_write(void* extra_data,
                      size_t extra_size,
                      void* data,
                      size_t size,
                      unsigned int fcc);
int uvc_gadget_pthread_init(int id);
void uvc_gadget_pthread_exit();
void uvc_set_user_resolution(int width, int height);
void uvc_get_user_resolution(int* width, int* height);
bool uvc_check_user_resolution_limit(int width, int height);
bool uvc_get_user_run_state();
void uvc_set_user_run_state(bool state);
void uvc_set_user_fcc(unsigned int fcc);
unsigned int uvc_get_user_fcc();
int uvc_get_user_video_id();
void uvc_set_user_video_id(int video_id);
void uvc_user_fill_buffer(struct uvc_device *dev, struct v4l2_buffer *buf);

#ifdef __cplusplus
}

#endif
#endif
