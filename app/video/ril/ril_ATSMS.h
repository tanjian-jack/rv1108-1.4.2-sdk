/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Vicent Chi <vicent.chi@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef  __ATSMS_H__
#define __ATSMS_H__

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define PRINTF_FUN_LINE  //printf("vicent--%s--%d\n",__FUNCTION__, __LINE__);

#define MAX_PHONE_NUMBER 20
#define MAX_SMS_LENGTH 400

#define TABLE_NUM 7445
#define TABLE_EN_ARRAY_POS (0xA6 - 0x21)

#define SMS_HANDFRRE 1
#define SMS_NOR 0
#define SMS_NOCHANG 2

#define CHARSET_ASCII   0
#define CHARSET_UCS2    1

#define  SMS_HZ_SIZE   70
#define  SMS_ASC_SIZE  160
#define CHAR2HEX(x)   ((((x)>'9')?((x)-'A'+10):((x)-'0'))&0xF)

#define PDU_SMS_LEN     640     /*   4*160  */

typedef struct gsm_sms {
    int     index;
    char    coded;
    char    head;
    short   status;
    int     length;
    int     date;
    int     time;
    char    address[MAX_PHONE_NUMBER + 1];
    char    center[MAX_PHONE_NUMBER + 1];
    char    msg[MAX_SMS_LENGTH + 4];
} gsm_sms_t;

typedef struct {
    gsm_sms_t       *sms;
    unsigned char   tp_head;
    unsigned char   tp_serial;
    unsigned char   tp_pid;
    unsigned char   tp_dcs;
    unsigned char   tp_vp;
    unsigned char   tp_udl;
    unsigned char   handfree;
    unsigned char   blank2;
    char            pdu[PDU_SMS_LEN + 8];
} raw_sms_t;

int at_sms_read(char *raw_pdu, char* msg_buf);

#endif
