/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Vicent Chi <vicent.chi@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __RIL_LIB_H_
#define __RIL_LIB_H_

#define L710
//#define L810

enum RIL_CMD {
    RIL_INIT = 0,
    RIL_GET_IP,
    RIL_GET_DNS,
    RIL_CALL_TEL,
    RIL_HAND_UP,
    RIL_ANSWER_TEL,
    RIL_SEND_MESSAGE_EN,
    RIL_SEND_MESSAGE_CH,
    RIL_READ_MESSAGE,
    RIL_CLOSE_NET,
    RIL_CTRL_Z,
    RIL_OTHER_CMD,
    RIL_END,
};

enum {
    EVENT_READ_RIL_NOTHING = 0,
    EVENT_READ_RIL_RING,
    EVENT_READ_RIL_NO_CARRIER,
    EVENT_READ_RIL_CALL,
    EVENT_READ_RIL_HANDUP,
    EVENT_READ_RIL_RECV_MESSAGE,
    EVENT_READ_RIL_READ_MESSAGE,

};

typedef struct CmdData {
    int cmd_type;
    int cmd_num;
    char at_cmd_ch[40][80];
} cmd_data;

typedef struct ListenEvent {
    int event_type;
    void* event_msg0;
    void* event_msg1;
} listen_event_data;

void open_hot_share();
void close_hot_share();
int send_listen_riL_cmd(int listen_fd, int cmd_type, char* amendata);
int port_exist();

#endif
