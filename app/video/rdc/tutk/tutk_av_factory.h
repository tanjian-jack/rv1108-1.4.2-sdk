/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TUTK_AV_FACTORY_H
#define TUTK_AV_FACTORY_H

#include <mutex>

#include "rdc/rdc_object.h"

class TUTKAVFactory : public RDCSubFactory {
 public:
  FACTORY_IDENTIFIER_DEFINITION("tutk")
  FACTORY_INSTANCE_DEFINITION(TUTKAVFactory)
  std::shared_ptr<RDCAVServer> NewProduct();
  bool Valid() { return valid_; }
  bool MarkUID(const char* UID);
  void UnMarkUID(const char* UID);

 private:
  TUTKAVFactory();
  ~TUTKAVFactory();
  bool valid_;
  std::map<std::string, bool> uids_;
  std::mutex mtx_;
};

#endif  // TUTK_AV_FACTORY_H
