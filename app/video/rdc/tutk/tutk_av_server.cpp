/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <autoconfig/main_app_autoconf.h>
#ifdef TUTK

#include "tutk_av_server.h"

#include <stdio.h>
#include <sys/prctl.h>
#include <sys/time.h>
#include <unistd.h>

extern "C" {
#include "AVFRAMEINFO.h"
#include "AVIOCTRLDEFs.h"
}

#include "tutk_av_factory.h"

#include "tutk/AVAPIs.h"
#include "tutk/IOTCAPIs.h"

#include "video_common.h"

// test AVID/AVPASS
static char avID[] = "admin";
static char avPass[] = "888888";
static int AuthCallBackFn(char* viewAcc, char* viewPwd) {
  if (strcmp(viewAcc, avID) == 0 && strcmp(viewPwd, avPass) == 0)
    return 1;
  return 0;
}

void PrintErrHandling(int nErr) {
  switch (nErr) {
    case IOTC_ER_SERVER_NOT_RESPONSE:
      //-1 IOTC_ER_SERVER_NOT_RESPONSE
      printf("[Error code : %d]\n", IOTC_ER_SERVER_NOT_RESPONSE);
      printf("Master doesn't respond.\n");
      printf(
          "Please check the network wheather it could connect to the "
          "Internet.\n");
      break;
    case IOTC_ER_FAIL_RESOLVE_HOSTNAME:
      //-2 IOTC_ER_FAIL_RESOLVE_HOSTNAME
      printf("[Error code : %d]\n", IOTC_ER_FAIL_RESOLVE_HOSTNAME);
      printf("Can't resolve hostname.\n");
      break;
    case IOTC_ER_ALREADY_INITIALIZED:
      //-3 IOTC_ER_ALREADY_INITIALIZED
      printf("[Error code : %d]\n", IOTC_ER_ALREADY_INITIALIZED);
      printf("Already initialized.\n");
      break;
    case IOTC_ER_FAIL_CREATE_MUTEX:
      //-4 IOTC_ER_FAIL_CREATE_MUTEX
      printf("[Error code : %d]\n", IOTC_ER_FAIL_CREATE_MUTEX);
      printf("Can't create mutex.\n");
      break;
    case IOTC_ER_FAIL_CREATE_THREAD:
      //-5 IOTC_ER_FAIL_CREATE_THREAD
      printf("[Error code : %d]\n", IOTC_ER_FAIL_CREATE_THREAD);
      printf("Can't create thread.\n");
      break;
    case IOTC_ER_UNLICENSE:
      //-10 IOTC_ER_UNLICENSE
      printf("[Error code : %d]\n", IOTC_ER_UNLICENSE);
      printf("This UID is unlicense.\n");
      printf("Check your UID.\n");
      break;
    case IOTC_ER_NOT_INITIALIZED:
      //-12 IOTC_ER_NOT_INITIALIZED
      printf("[Error code : %d]\n", IOTC_ER_NOT_INITIALIZED);
      printf("Please initialize the IOTCAPI first.\n");
      break;
    case IOTC_ER_TIMEOUT:
      //-13 IOTC_ER_TIMEOUT
      break;
    case IOTC_ER_INVALID_SID:
      //-14 IOTC_ER_INVALID_SID
      printf("[Error code : %d]\n", IOTC_ER_INVALID_SID);
      printf("This SID is invalid.\n");
      printf("Please check it again.\n");
      break;
    case IOTC_ER_EXCEED_MAX_SESSION:
      //-18 IOTC_ER_EXCEED_MAX_SESSION
      printf("[Error code : %d]\n", IOTC_ER_EXCEED_MAX_SESSION);
      printf("[Warning]\n");
      printf("The amount of session reach to the maximum.\n");
      printf("It cannot be connected unless the session is released.\n");
      break;
    case IOTC_ER_CAN_NOT_FIND_DEVICE:
      //-19 IOTC_ER_CAN_NOT_FIND_DEVICE
      printf("[Error code : %d]\n", IOTC_ER_CAN_NOT_FIND_DEVICE);
      printf("Device didn't register on server, so we can't find device.\n");
      printf("Please check the device again.\n");
      printf("Retry...\n");
      break;
    case IOTC_ER_SESSION_CLOSE_BY_REMOTE:
      //-22 IOTC_ER_SESSION_CLOSE_BY_REMOTE
      printf("[Error code : %d]\n", IOTC_ER_SESSION_CLOSE_BY_REMOTE);
      printf("Session is closed by remote so we can't access.\n");
      printf("Please close it or establish session again.\n");
      break;
    case IOTC_ER_REMOTE_TIMEOUT_DISCONNECT:
      //-23 IOTC_ER_REMOTE_TIMEOUT_DISCONNECT
      printf("[Error code : %d]\n", IOTC_ER_REMOTE_TIMEOUT_DISCONNECT);
      printf(
          "We can't receive an acknowledgement character within a TIMEOUT.\n");
      printf("It might that the session is disconnected by remote.\n");
      printf("Please check the network wheather it is busy or not.\n");
      printf("And check the device and user equipment work well.\n");
      break;
    case IOTC_ER_DEVICE_NOT_LISTENING:
      //-24 IOTC_ER_DEVICE_NOT_LISTENING
      printf("[Error code : %d]\n", IOTC_ER_DEVICE_NOT_LISTENING);
      printf(
          "Device doesn't listen or the sessions of device reach to "
          "maximum.\n");
      printf(
          "Please release the session and check the device wheather it listen "
          "or not.\n");
      break;
    case IOTC_ER_CH_NOT_ON:
      //-26 IOTC_ER_CH_NOT_ON
      printf("[Error code : %d]\n", IOTC_ER_CH_NOT_ON);
      printf("Channel isn't on.\n");
      printf(
          "Please open it by IOTC_Session_Channel_ON() or "
          "IOTC_Session_Get_Free_Channel()\n");
      printf("Retry...\n");
      break;
    case IOTC_ER_SESSION_NO_FREE_CHANNEL:
      //-31 IOTC_ER_SESSION_NO_FREE_CHANNEL
      printf("[Error code : %d]\n", IOTC_ER_SESSION_NO_FREE_CHANNEL);
      printf("All channels are occupied.\n");
      printf("Please release some channel.\n");
      break;
    case IOTC_ER_TCP_TRAVEL_FAILED:
      //-32 IOTC_ER_TCP_TRAVEL_FAILED
      printf("[Error code : %d]\n", IOTC_ER_TCP_TRAVEL_FAILED);
      printf("Device can't connect to Master.\n");
      printf("Don't let device use proxy.\n");
      printf("Close firewall of device.\n");
      printf("Or open device's TCP port 80, 443, 8080, 8000, 21047.\n");
      break;
    case IOTC_ER_TCP_CONNECT_TO_SERVER_FAILED:
      //-33 IOTC_ER_TCP_CONNECT_TO_SERVER_FAILED
      printf("[Error code : %d]\n", IOTC_ER_TCP_CONNECT_TO_SERVER_FAILED);
      printf("Device can't connect to server by TCP.\n");
      printf("Don't let server use proxy.\n");
      printf("Close firewall of server.\n");
      printf("Or open server's TCP port 80, 443, 8080, 8000, 21047.\n");
      printf("Retry...\n");
      break;
    case IOTC_ER_NO_PERMISSION:
      //-40 IOTC_ER_NO_PERMISSION
      printf("[Error code : %d]\n", IOTC_ER_NO_PERMISSION);
      printf("This UID's license doesn't support TCP.\n");
      break;
    case IOTC_ER_NETWORK_UNREACHABLE:
      //-41 IOTC_ER_NETWORK_UNREACHABLE
      printf("[Error code : %d]\n", IOTC_ER_NETWORK_UNREACHABLE);
      printf("Network is unreachable.\n");
      printf("Please check your network.\n");
      printf("Retry...\n");
      break;
    case IOTC_ER_FAIL_SETUP_RELAY:
      //-42 IOTC_ER_FAIL_SETUP_RELAY
      printf("[Error code : %d]\n", IOTC_ER_FAIL_SETUP_RELAY);
      printf("Client can't connect to a device via Lan, P2P, and Relay mode\n");
      break;
    case IOTC_ER_NOT_SUPPORT_RELAY:
      //-43 IOTC_ER_NOT_SUPPORT_RELAY
      printf("[Error code : %d]\n", IOTC_ER_NOT_SUPPORT_RELAY);
      printf("Server doesn't support UDP relay mode.\n");
      printf("So session can't use UDP relay to connect to a device.\n");
      break;

    default:
      break;
  }
}

static void* thread_login(void* arg);
static void* thread_listen(void* arg);
static void* thread_send_video_frame(void* arg);
static void* thread_send_audio_frame(void* arg);
static void* thread_receive_audio(void* arg);

static void* thread_ctrl_session(void* arg);

class TUTKAVSession {
 public:
  static const int kMAX_SIZE_IOCTRL_BUF = 1024;
  TUTKAVSession(TUTKAVServer* server);
  ~TUTKAVSession();
  bool Active() { return active_; }
  void SetActive(bool val) { active_ = val; }
  void SetAVChannelID(int av_ch);
  bool Started();
  bool StartSession(int sid);
  void StopSession();
  void WRLock();    // write lock
  void RDLock();    // read lock
  void RWUnLock();  // unlock rw
  void EnableVideo(int av_ch) {
    assert(av_ch_id_ == av_ch);
    enable_video_ = true;
  }
  void DisableVideo() { enable_video_ = false; }
  bool VideoEnabled() { return enable_video_; }
  void EnableAudio(int av_ch) {
    assert(av_ch_id_ == av_ch);
    enable_audio_ = true;
  }
  void DisableAudio() { enable_audio_ = false; }
  bool AudioEnabled() { return enable_audio_; }
  bool StartSpeaker(unsigned int channel);
  void StopSpeaker();

  pthread_rwlock_t rw_lock_;
  TUTKAVServer* server_;
  std::atomic_bool active_;
  int SID_;
  pthread_t session_tid_;
  int av_ch_id_;  // av channel id
  bool enable_audio_;
  bool enable_video_;
  bool enable_speaker_;
  int speaker_ch_;
  pthread_t speaker_tid_;
};

#define LOCKGUARD(NAME, LOCKFN)                                             \
  class NAME {                                                              \
   public:                                                                  \
    NAME(TUTKAVSession* session) : session_(session) { session->LOCKFN(); } \
    ~NAME() { session_->RWUnLock(); }                                       \
                                                                            \
   private:                                                                 \
    TUTKAVSession* session_;                                                \
  };

LOCKGUARD(TUTKAVSessionRDLockGuard, RDLock)
LOCKGUARD(TUTKAVSessionWRLockGuard, WRLock)

TUTKAVSession::TUTKAVSession(TUTKAVServer* server)
    : server_(server),
      active_(false),
      SID_(-1),
      session_tid_(0),
      av_ch_id_(-1),
      enable_audio_(false),
      enable_video_(false),
      enable_speaker_(false),
      speaker_ch_(-1),
      speaker_tid_(0) {
  pthread_rwlock_init(&rw_lock_, NULL);
}

TUTKAVSession::~TUTKAVSession() {
  active_ = false;
  assert(session_tid_ == 0);
  StopSpeaker();
  pthread_rwlock_destroy(&rw_lock_);
}

inline bool TUTKAVSession::Started() {
  return SID_ >= 0;
}

void TUTKAVSession::SetAVChannelID(int av_ch) {
  TUTKAVSessionWRLockGuard lg(this);
  assert(av_ch_id_ < 0);
  av_ch_id_ = av_ch;
}

bool TUTKAVSession::StartSession(int sid) {
  SID_ = sid;
  active_ = true;
  if (StartThread(session_tid_, thread_ctrl_session, (void*)this))
    return false;

  return true;
}

void TUTKAVSession::StopSession() {
  {
    TUTKAVSessionWRLockGuard lg(this);
    enable_video_ = false;
    enable_audio_ = false;
    if (av_ch_id_ >= 0) {
      avServStop(av_ch_id_);
      av_ch_id_ = -1;
    }
  }
  if (SID_ >= 0) {
    IOTC_Session_Close(SID_);
    SID_ = -1;
  }
  StopThread(session_tid_);
}

void TUTKAVSession::WRLock() {
  int ret = pthread_rwlock_wrlock(&rw_lock_);
  if (ret)
    printf("Acquire SID %d wr lock error, ret = %d\n", SID_, ret);
}

void TUTKAVSession::RDLock() {
  int ret = pthread_rwlock_rdlock(&rw_lock_);
  if (ret)
    printf("Acquire SID %d rd lock error, ret = %d\n", SID_, ret);
}

void TUTKAVSession::RWUnLock() {
  int ret = pthread_rwlock_unlock(&rw_lock_);
  if (ret)
    printf("Release SID %d rwlock error, ret = %d\n", SID_, ret);
}

bool TUTKAVSession::StartSpeaker(unsigned int channel) {
  {
    TUTKAVSessionWRLockGuard lg(this);
    if (enable_speaker_) {
      printf("stop previous audio receive thread?\n");
      return false;
    }
    speaker_ch_ = channel;
    enable_speaker_ = true;
  }
  if (StartThread(speaker_tid_, thread_receive_audio, (void*)this)) {
    printf("start speaker thread failed!\n");
    speaker_ch_ = -1;
    enable_speaker_ = false;
    return false;
  }
  return true;
}

void TUTKAVSession::StopSpeaker() {
  {
    TUTKAVSessionWRLockGuard lg(this);
    speaker_ch_ = -1;
    enable_speaker_ = false;
  }
  StopThread(speaker_tid_);
}

TUTKAVServer::TUTKAVServer()
    : UID_(nullptr),
      process_run_(true),
      session_num_(0),
      login_tid_(0),
      listen_tid_(0) {}

TUTKAVServer::~TUTKAVServer() {
  process_run_ = false;
  for (int i = 0; i < kMAX_CLIENT_NUM; i++)
    sessions_[i]->SetActive(false);
  StopThread(listen_tid_);
  StopThread(login_tid_);
  send_audio_t_.Stop();
  send_video_t_.Stop();
  extradata_pkt_.resetRefcount();
  TUTKAVFactory::Instance().UnMarkUID(UID_);
  if (UID_)
    free(UID_);
  printf("exit tutk av server!\n");
}

bool TUTKAVServer::Init(char* UID) {
  assert(!UID_);
  UID_ = strdup(UID);
  if (!UID_)
    return false;
  if (!TUTKAVFactory::Instance().MarkUID(UID))
    return false;
  for (int i = 0; i < kMAX_CLIENT_NUM; i++) {
    sessions_[i] = std::make_shared<TUTKAVSession>(this);
    if (!sessions_[i])
      return false;
  }
  if (send_video_t_.Start(thread_send_video_frame, (void*)this))
    return false;
  if (send_audio_t_.Start(thread_send_audio_frame, (void*)this))
    return false;
  if (StartThread(login_tid_, thread_login, (void*)this))
    return false;
  if (StartThread(listen_tid_, thread_listen, (void*)this))
    return false;

  return true;
}

void TUTKAVServer::Queue(EncodedPacket* pkt) {
  if (session_num_ <= 0) {  // no session
    pkt->unref();
    return;
  }
  if (pkt->type == VIDEO_PACKET) {
    send_video_t_.Queue(pkt, &extradata_pkt_);
  } else if (pkt->type == AUDIO_PACKET) {
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    pkt->time_val = tv;
    send_audio_t_.Queue(pkt);
  }
}

EncodedPacket* TUTKAVServer::DequeueVideo() {
  return send_video_t_.Dequeue();
}

EncodedPacket* TUTKAVServer::DequeueAudio() {
  return send_audio_t_.Dequeue();
}

inline static unsigned int ToMS(struct timeval& tv) {
  return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

void TUTKAVServer::SendData(
    std::function<EncodedPacket*(TUTKAVServer*)> dequeue_fn,
    std::function<bool(TUTKAVSession*)> valid_fn,
    std::function<int(int, const char*, int, const void*, int)> tutk_send_fn,
    std::function<void(TUTKAVSession*)> disable_fn,
    FRAMEINFO_t* frame_info) {
  EncodedPacket* pkt = dequeue_fn(this);
  if (!pkt)
    return;
  frame_info->onlineNum = SessionNum();
  frame_info->timestamp = ToMS(pkt->time_val);
  if (pkt->type == VIDEO_INFO_PACKET)
    frame_info->flags = IPC_FRAME_FLAG_EXTRAINFO;
  else if (pkt->type == VIDEO_PACKET)
    frame_info->flags = (pkt->av_pkt.flags & AV_PKT_FLAG_KEY)
                            ? IPC_FRAME_FLAG_IFRAME
                            : IPC_FRAME_FLAG_PBFRAME;

  for (int i = 0; i < kMAX_CLIENT_NUM; i++) {
    std::shared_ptr<TUTKAVSession> session = SessionAt(i);
    TUTKAVSession* tutk_av_session = session.get();
    if (session->av_ch_id_ < 0)
      continue;
    int ret = AV_ER_NoERROR;
    {
      TUTKAVSessionRDLockGuard lg(tutk_av_session);
      if (session->av_ch_id_ < 0 || !valid_fn(tutk_av_session))
        continue;
      ret = tutk_send_fn(session->av_ch_id_, (const char*)pkt->av_pkt.data,
                         pkt->av_pkt.size, frame_info, sizeof(FRAMEINFO_t));
    }
    if (ret == AV_ER_EXCEED_MAX_SIZE) {
      // means data not write to queue, send too slow, I want to skip it
      printf("codec[%d] send too slow\n", frame_info->codec_id);
      usleep(10000);
    } else if (ret == AV_ER_SESSION_CLOSE_BY_REMOTE) {
      printf("codec[%d] AV_ER_SESSION_CLOSE_BY_REMOTE SID[%d]\n",
             frame_info->codec_id, i);
      disable_fn(tutk_av_session);
    } else if (ret == AV_ER_REMOTE_TIMEOUT_DISCONNECT) {
      printf("codec[%d] AV_ER_REMOTE_TIMEOUT_DISCONNECT SID[%d]\n",
             frame_info->codec_id, i);
      disable_fn(tutk_av_session);
    } else if (ret == IOTC_ER_INVALID_SID) {
      printf("codec[%d] Session cant be used anymore\n", frame_info->codec_id);
      disable_fn(tutk_av_session);
    } else if (ret < 0)
      printf("codec[%d] send ret: %d\n", frame_info->codec_id, ret);
  }
  pkt->unref();
}

TUTKAVServer::TUTKAVThread::TUTKAVThread() : tid_(0), run_(false) {}
TUTKAVServer::TUTKAVThread::~TUTKAVThread() {
  assert(tid_ == 0);
}

void TUTKAVServer::TUTKAVThread::Queue(EncodedPacket* pkt,
                                       EncodedPacket* extradata_pkt) {
  std::lock_guard<std::mutex> _lk(mtx_);
  if (!tid_) {
    pkt->unref();
    return;
  }
  if (extradata_pkt && (pkt->av_pkt.flags & AV_PKT_FLAG_KEY)) {
    extradata_pkt->ref();
    packets_.push(extradata_pkt);
  }
  packets_.push(pkt);
  cond_.notify_one();
}

EncodedPacket* TUTKAVServer::TUTKAVThread::Dequeue() {
  std::unique_lock<std::mutex> _lk(mtx_);
  if (packets_.empty() && run_) {
    cond_.wait(_lk);
    return nullptr;
  }
  EncodedPacket* pkt = packets_.front();
  packets_.pop();
  return pkt;
}

int TUTKAVServer::TUTKAVThread::Start(Runnable routine, void* arg) {
  run_ = true;
  return StartThread(tid_, routine, arg);
}

void TUTKAVServer::TUTKAVThread::Stop() {
  pthread_t tid = 0;
  do {
    std::lock_guard<std::mutex> _lk(mtx_);
    tid = tid_;
    tid_ = 0;
    run_ = false;
    while (!packets_.empty()) {
      EncodedPacket* pkt = packets_.front();
      packets_.pop();
      pkt->unref();
    }
    cond_.notify_one();
  } while (0);
  StopThread(tid);
}

static void* thread_login(void* arg) {
  TUTKAVServer* serv = (TUTKAVServer*)arg;
  PRSET_THREAD_NAME();
  while (serv->Running()) {
    int ret = IOTC_Device_Login(serv->UID(), NULL, NULL);
    // printf("IOTC_Device_Login() ret = %d\n", ret);
    if (ret == IOTC_ER_NoERROR) {
      break;
    } else {
      // PrintErrHandling(ret);
      sleep(5);
    }
  }
  pthread_detach(pthread_self());
  pthread_exit(0);
}

static void* thread_listen(void* arg) {
  TUTKAVServer* serv = (TUTKAVServer*)arg;
  PRSET_THREAD_NAME();
  while (serv->Running()) {
    // Accept connection only when IOTC_Listen() calling
    // timeout: 1000ms
    int SID = IOTC_Listen(1000);
    for (int i = 0; i < TUTKAVServer::kMAX_CLIENT_NUM; i++) {
      std::shared_ptr<TUTKAVSession>& session = serv->SessionAt(i);
      if (session->Started() && !session->Active())
        session->StopSession();
    }
    if (SID < 0) {
      PrintErrHandling(SID);
      if (SID == IOTC_ER_EXCEED_MAX_SESSION)
        sleep(5);
      continue;
    }
    std::shared_ptr<TUTKAVSession>& session = serv->SessionAt(SID);
    if (session->Started()) {
      fprintf(stderr, "SID[%d] started, tutk error??\n", SID);
      continue;
    }
    if (!session->StartSession(SID)) {
      fprintf(stderr, "start session for SID[%d] failed!\n", SID);
      session->StopSession();
    }
  }
  return nullptr;
}

static void handle_video_start(TUTKAVSession* session,
                               int av_ch_id,
                               char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_START, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  {
    TUTKAVSessionWRLockGuard lg(session);
    session->EnableVideo(av_ch_id);
  }
}

static void handle_video_stop(TUTKAVSession* session, int av_ch_id, char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_STOP, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  {
    TUTKAVSessionWRLockGuard lg(session);
    session->DisableVideo();
  }
}

static void handle_audio_start(TUTKAVSession* session,
                               int av_ch_id,
                               char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_AUDIOSTART, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  {
    TUTKAVSessionWRLockGuard lg(session);
    session->EnableAudio(av_ch_id);
  }
}

static void handle_audio_stop(TUTKAVSession* session, int av_ch_id, char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_AUDIOSTOP, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  {
    TUTKAVSessionWRLockGuard lg(session);
    session->DisableAudio();
  }
}

static void handle_speaker_start(TUTKAVSession* session,
                                 int av_ch_id,
                                 char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_SPEAKERSTART, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  session->StartSpeaker(p->channel);
}

static void handle_speaker_stop(TUTKAVSession* session,
                                int av_ch_id,
                                char* buf) {
  SMsgAVIoctrlAVStream* p = (SMsgAVIoctrlAVStream*)buf;
  printf("IOTYPE_USER_IPCAM_SPEAKERSTOP, ch:%d, av channel:%d\n\n", p->channel,
         av_ch_id);
  session->StopSpeaker();
}

// Thread - Start AV server and recv IOCtrl cmd for every new av idx
static void* thread_ctrl_session(void* arg) {
  TUTKAVSession* session = (TUTKAVSession*)arg;
  TUTKAVServer* serv = session->server_;
  unsigned int ioType;
  char ioCtrlBuf[TUTKAVSession::kMAX_SIZE_IOCTRL_BUF];
  struct st_SInfo Sinfo;

  PRSET_THREAD_NAME();
  printf("SID[%d], %s, OK\n", session->SID_, __func__);

  int sid = session->SID_;
  int nResend = -1;
  int av_ch_id =
      avServStart3(sid, AuthCallBackFn, 0, SERVTYPE_STREAM_SERVER, 0, &nResend);
  if (av_ch_id < 0) {
    printf("avServStart3 failed!! SID[%d] code[%d]!!!\n", sid, av_ch_id);
    printf("%s: exit!! SID[%d]\n", __func__, sid);
    pthread_exit(0);
  }
  session->SetAVChannelID(av_ch_id);
  session->SetActive(true);
  serv->IncSessionNum();
  if (IOTC_Session_Check(sid, &Sinfo) == IOTC_ER_NoERROR) {
    static const char* mode[3] = {"P2P", "RLY", "LAN"};
    // print session information(not a must)
    if (isdigit(Sinfo.RemoteIP[0]))
      printf(
          "Client is from[IP:%s, Port:%d] Mode[%s] VPG[%d:%d:%d] VER[%X] "
          "NAT[%d] AES[%d]\n",
          Sinfo.RemoteIP, Sinfo.RemotePort, mode[(int)Sinfo.Mode], Sinfo.VID,
          Sinfo.PID, Sinfo.GID, Sinfo.IOTCVersion, Sinfo.NatType,
          Sinfo.isSecure);
  }
  printf("avServStart3 OK, av channel[%d], Resend[%d]\n\n", av_ch_id, nResend);

  while (session->Active()) {
    int ret = avRecvIOCtrl(av_ch_id, &ioType, (char*)&ioCtrlBuf,
                           sizeof(ioCtrlBuf), 1000);
    if (ret >= 0) {
      printf("Handle CMD: ");
      switch (ioType) {
        case IOTYPE_USER_IPCAM_START:
          handle_video_start(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_STOP:
          handle_video_stop(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_AUDIOSTART:
          handle_audio_start(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_AUDIOSTOP:
          handle_audio_stop(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_SPEAKERSTART:
          handle_speaker_start(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_SPEAKERSTOP:
          handle_speaker_stop(session, av_ch_id, ioCtrlBuf);
          break;
        case IOTYPE_USER_IPCAM_LISTEVENT_REQ:
          printf("TODO: IOTYPE_USER_IPCAM_LISTEVENT_REQ\n\n");
          break;
        default:
          printf("av channel %d: non-handle type[%X]\n", av_ch_id, ioType);
          break;
      }
    } else if (ret != AV_ER_TIMEOUT) {
      printf("av channel[%d], avRecvIOCtrl error, code[%d]\n", av_ch_id, ret);
      break;
    }
  }

  printf("SID[%d], av channel[%d], %s exit!!\n", session->SID_, av_ch_id,
         __func__);
  serv->DecSessionNum();
  session->SetActive(false);
  pthread_exit(0);
}

// Thread - Send Video frames to all AV-idx
static void* thread_send_video_frame(void* arg) {
  TUTKAVServer* serv = (TUTKAVServer*)arg;
  FRAMEINFO_t frameInfo = {};
  frameInfo.codec_id = MEDIA_CODEC_VIDEO_H264;

  PRSET_THREAD_NAME();
  printf("%s start OK\n", __func__);

  TUTKAVServer::TUTKAVThread& vth = serv->VideoSendThread();
  while (vth.Run())
    serv->SendData(&TUTKAVServer::DequeueVideo, &TUTKAVSession::VideoEnabled,
                   avSendFrameData, &TUTKAVSession::DisableVideo, &frameInfo);

  printf("exit %s\n", __func__);
  return nullptr;
}

// Thread - Send Audio frames to all AV-idx
static void* thread_send_audio_frame(void* arg) {
  TUTKAVServer* serv = (TUTKAVServer*)arg;
  FRAMEINFO_t frameInfo = {};
  // *** set audio frame info here ***
  frameInfo.codec_id = MEDIA_CODEC_AUDIO_AAC;
  // TODO
  frameInfo.flags =
      (AUDIO_SAMPLE_16K << 2) | (AUDIO_DATABITS_16 << 1) | AUDIO_CHANNEL_STERO;

  PRSET_THREAD_NAME();
  printf("%s start OK\n", __func__);

  TUTKAVServer::TUTKAVThread& ath = serv->AudioSendThread();
  while (ath.Run())
    serv->SendData(&TUTKAVServer::DequeueAudio, &TUTKAVSession::AudioEnabled,
                   avSendAudioData, &TUTKAVSession::DisableAudio, &frameInfo);

  printf("[%s] exit\n", __func__);
  return nullptr;
}

static void audio_playback(const FRAMEINFO_t& frameInfo, char* buf, int size) {
  // TODO
  assert(frameInfo.codec_id == MEDIA_CODEC_AUDIO_PCM);
  assert(frameInfo.flags == ((AUDIO_SAMPLE_16K << 2) |
                             (AUDIO_DATABITS_16 << 1) | AUDIO_CHANNEL_STERO));
  printf("%s, size: %d\n", __func__, size);
}

static void* thread_receive_audio(void* arg) {
  TUTKAVSession* session = (TUTKAVSession*)arg;
  if (!session)
    return nullptr;
  PRSET_THREAD_NAME();
  int sid = session->SID_;
  int nResend = 0;
  unsigned int servType = 0;
  int av_ch_id = avClientStart2(sid, NULL, NULL, 30, &servType,
                                session->speaker_ch_, &nResend);
  printf("SID<%d> [%s] start ok idx[%d] servType[%d]\n", sid, __func__,
         av_ch_id, servType);

  if (av_ch_id >= 0) {
    char buf[TUTKAVServer::kAUDIO_BUF_SIZE];
    FRAMEINFO_t frameInfo;
    unsigned int frmNo = 0;

    avClientCleanAudioBuf(session->speaker_ch_);
    while (session->enable_speaker_) {
      int ret = avRecvAudioData(av_ch_id, buf, sizeof(buf), (char*)&frameInfo,
                                sizeof(FRAMEINFO_t), &frmNo);
      printf("avRecvAudioData return [%d]\n", ret);
      if (ret == AV_ER_SESSION_CLOSE_BY_REMOTE) {
        printf("%s AV_ER_SESSION_CLOSE_BY_REMOTE\n", __func__);
        break;
      } else if (ret == AV_ER_REMOTE_TIMEOUT_DISCONNECT) {
        printf("%s AV_ER_REMOTE_TIMEOUT_DISCONNECT\n", __func__);
        break;
      } else if (ret == IOTC_ER_INVALID_SID) {
        printf("%s Session[%d] cant be used anymore\n", __func__, sid);
        break;
      } else if (ret == AV_ER_LOSED_THIS_FRAME) {
        printf("frmNo [%d] Audio LOST\n", frmNo);
        continue;
      } else if (ret == AV_ER_DATA_NOREADY) {
        usleep(40000);
        continue;
      } else if (ret < 0) {
        printf("Other error[%d]!!!\n", ret);
        continue;
      }
      audio_playback(frameInfo, buf, ret);
    }
  }

  avClientStop(av_ch_id);
  printf("[%s] exit\n", __func__);
  pthread_exit(0);
}

#endif  // #ifdef TUTK
