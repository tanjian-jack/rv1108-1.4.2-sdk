/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <autoconfig/main_app_autoconf.h>
#ifdef TUTK

#include "camera_tutk_ts_muxer.h"

#include "rdc/rdc_factory.h"
#include "tutk_av_server.h"

DEFINITION_CAMERATSALLOC(CameraTutkTSAlloc, "tutk", CameraTutkTsMuxer)

CameraTutkTsMuxer::CameraTutkTsMuxer() {
  snprintf(format, sizeof(format), "%s", "h264");
  no_async = true;  // we push packet into the lists of tutk server
}
CameraTutkTsMuxer::~CameraTutkTsMuxer() {}

int CameraTutkTsMuxer::init_uri(char* uri, int rate) {
  char* uid = uri + sizeof("tutk:") - 1;
  std::shared_ptr<RDCAVServer> rdc_server =
      RDCFactory::Instance().Create("tutk");
  if (!rdc_server) {
    fprintf(stderr, "create tutk av server failed\n");
    return -1;
  }
  tutk_av_serv_ = std::dynamic_pointer_cast<TUTKAVServer>(rdc_server);
  if (!tutk_av_serv_) {
    fprintf(stderr, "tutk code is broken?\n");
    return -1;
  }
  if (!tutk_av_serv_->Init(uid)) {
    fprintf(stderr, "init tutk av server failed\n");
    return -1;
  }
  return CameraTsMuxer::init_uri(uri, rate);
}
int CameraTutkTsMuxer::muxer_write_header(AVFormatContext* oc, char* url) {
  for (unsigned int i = 0; i < oc->nb_streams; i++) {
    AVStream* st = oc->streams[i];
    AVCodecContext* codec = st ? st->codec : nullptr;
    if (codec && codec->codec_id == AV_CODEC_ID_H264) {
      tutk_av_serv_->SetExtradata(codec->extradata, codec->extradata_size);
      return 0;
    }
  }
  fprintf(stderr, "Error: could not get the sps pps info\n");
  return -1;
}

int CameraTutkTsMuxer::muxer_write_tailer(AVFormatContext* oc) {
  // do nothing
  return 0;
}
int CameraTutkTsMuxer::muxer_write_free_packet(MuxProcessor* process,
                                               EncodedPacket* pkt) {
  // do nothing
  return 0;
}

void CameraTutkTsMuxer::push_packet(EncodedPacket* pkt) {
  pkt->ref();
  tutk_av_serv_->Queue(pkt);
}

#endif  // #ifdef TUTK
