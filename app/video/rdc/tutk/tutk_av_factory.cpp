/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <autoconfig/main_app_autoconf.h>
#ifdef TUTK

#include "tutk_av_factory.h"

#include <stdio.h>

#include "rdc/rdc_factory.h"
#include "tutk_av_server.h"

#include "tutk/AVAPIs.h"
#include "tutk/IOTCAPIs.h"

FACTORY_REGISTER(TUTKAVFactory, RDCFactory)

// implement by tutk samples
static void LoginInfoCB(unsigned int nLoginInfo) {
  if ((nLoginInfo & 0x04))
    printf("I can be connected via Internet\n");
  else if ((nLoginInfo & 0x08))
    printf("I am be banned by IOTC Server because UID multi-login\n");
  else
    printf("%s: nLoginInfo<%u>\n", __FUNCTION__, nLoginInfo);
}

TUTKAVFactory::TUTKAVFactory() : valid_(false) {
  IOTC_Set_Max_Session_Number(TUTKAVServer::kMAX_CLIENT_NUM);
  // use which Master base on location, port 0 means to get a random port
  int ret = IOTC_Initialize2(0);
  if (ret != IOTC_ER_NoERROR) {
    printf("  [] IOTC_Initialize2(), ret=[%d]\n", ret);
    PrintErrHandling(ret);
    return;
  }
  // Versoin of IOTCAPIs & AVAPIs
  unsigned int iotcVer = 0;
  IOTC_Get_Version(&iotcVer);
  int avVer = avGetAVApiVer();
  unsigned char* p = (unsigned char*)&iotcVer;
  unsigned char* p2 = (unsigned char*)&avVer;
  char szIOTCVer[16], szAVVer[16];
  sprintf(szIOTCVer, "%d.%d.%d.%d", p[3], p[2], p[1], p[0]);
  sprintf(szAVVer, "%d.%d.%d.%d", p2[3], p2[2], p2[1], p2[0]);
  printf("IOTCAPI version[%s] AVAPI version[%s]\n", szIOTCVer, szAVVer);

  IOTC_Get_Login_Info_ByCallBackFn(LoginInfoCB);
  // alloc MAX_CLIENT_NUMBER*3 for every session av data/speaker/play back
  int max_av_chs = avInitialize(TUTKAVServer::kMAX_CLIENT_NUM * 3);
  if (max_av_chs < 0) {
    printf("avInitialize channel num error, ret : %d\n", max_av_chs);
    return;
  }
  valid_ = true;
}

TUTKAVFactory::~TUTKAVFactory() {
  avDeInitialize();
  IOTC_DeInitialize();
}

bool TUTKAVFactory::MarkUID(const char* UID) {
  std::lock_guard<std::mutex> _lk(mtx_);
  auto it = uids_.find(UID);
  if (it == uids_.end() || uids_[UID] == false) {
    uids_[UID] = true;
    return true;
  } else {
    fprintf(stderr, "UID [%s] has been using\n", UID);
    return false;
  }
}

void TUTKAVFactory::UnMarkUID(const char* UID) {
  std::lock_guard<std::mutex> _lk(mtx_);
  uids_[UID] = false;
}

std::shared_ptr<RDCAVServer> TUTKAVFactory::NewProduct() {
  if (!valid_)
    return nullptr;
  return std::make_shared<TUTKAVServer>();
}

#endif  // #ifdef TUTK
