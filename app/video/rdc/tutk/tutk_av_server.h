/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TUTK_AV_SERVER_H
#define TUTK_AV_SERVER_H

#include <pthread.h>

#include <array>
#include <atomic>
#include <queue>

#include "rdc/rdc_object.h"
#include "video_common.h"

#define SERVTYPE_STREAM_SERVER 0
class TUTKAVSession;
typedef struct _FRAMEINFO FRAMEINFO_t;
class TUTKAVServer : public RDCAVServer {
 public:
  static const int kMAX_CLIENT_NUM = 128;
  static const int kAUDIO_BUF_SIZE = 4096;

  class TUTKAVThread {
   public:
    TUTKAVThread();
    ~TUTKAVThread();
    void Queue(EncodedPacket* pkt, EncodedPacket* extradata_pkt = nullptr);
    EncodedPacket* Dequeue();
    int Start(Runnable routine, void* arg);
    void Stop();
    bool Run() { return run_; }

   private:
    pthread_t tid_;
    volatile bool run_;
    std::queue<EncodedPacket*> packets_;
    std::mutex mtx_;
    std::condition_variable cond_;
  };

  TUTKAVServer();
  ~TUTKAVServer();
  char* UID() { return UID_; }
  bool Running() { return process_run_; }
  void SetExtradata(uint8_t* data, int size) {
    extradata_pkt_.type = VIDEO_INFO_PACKET;
    extradata_pkt_.is_phy_buf = false;
    extradata_pkt_.av_pkt.flags |= AV_PKT_FLAG_KEY;
    extradata_pkt_.av_pkt.data = data;
    extradata_pkt_.av_pkt.size = size;
  }
  int SessionNum() { return session_num_; }
  void IncSessionNum() { session_num_++; }
  void DecSessionNum() { session_num_--; }
  std::shared_ptr<TUTKAVSession>& SessionAt(int index) {
    return sessions_[index];
  }
  bool Init(char* UID);
  void Queue(EncodedPacket*);
  EncodedPacket* DequeueVideo();
  EncodedPacket* DequeueAudio();

  TUTKAVThread& VideoSendThread() { return send_video_t_;}
  TUTKAVThread& AudioSendThread() { return send_audio_t_;}
  void SendData(
      std::function<EncodedPacket*(TUTKAVServer*)> dequeue_fn,
      std::function<bool(TUTKAVSession*)> valid_fn,
      std::function<int(int, const char*, int, const void*, int)> tutk_send_fn,
      std::function<void(TUTKAVSession*)> disable_fn,
      FRAMEINFO_t* frame_info);

 private:
  static const int kMAX_AV_CHANNEL_NUMBER = 16;

  char* UID_;
  std::array<std::shared_ptr<TUTKAVSession>, kMAX_CLIENT_NUM> sessions_;
  volatile bool process_run_;
  std::atomic_int session_num_;
  pthread_t login_tid_;
  pthread_t listen_tid_;

  TUTKAVThread send_video_t_;
  EncodedPacket extradata_pkt_;

  TUTKAVThread send_audio_t_;
};

// implement by tutk samples
void PrintErrHandling(int nErr);

#endif  // TUTK_AV_SERVER_H
