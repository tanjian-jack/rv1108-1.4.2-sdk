/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _AVFRAME_INFO_H_
#define _AVFRAME_INFO_H_

/* CODEC ID */
typedef enum {
  MEDIA_CODEC_UNKNOWN = 0x00,
  MEDIA_CODEC_VIDEO_MPEG4 = 0x4C,
  MEDIA_CODEC_VIDEO_H263 = 0x4D,
  MEDIA_CODEC_VIDEO_H264 = 0x4E,
  MEDIA_CODEC_VIDEO_MJPEG = 0x4F,

  // 2018-1-12 add DIVIDE VALUE between video and audio
  MEDIA_CODEC_AV_DIVIDE = 0x80,

  MEDIA_CODEC_AUDIO_AAC = 0x88,    // 2014-07-02 add AAC audio codec definition
  MEDIA_CODEC_AUDIO_G711U = 0x89,  // g711 u-law
  MEDIA_CODEC_AUDIO_G711A = 0x8A,  // g711 a-law
  MEDIA_CODEC_AUDIO_ADPCM = 0X8B,
  MEDIA_CODEC_AUDIO_PCM = 0x8C,
  MEDIA_CODEC_AUDIO_SPEEX = 0x8D,
  MEDIA_CODEC_AUDIO_MP3 = 0x8E,
  MEDIA_CODEC_AUDIO_G726 = 0x8F,

} ENUM_CODECID;

/* FRAME Flag */
typedef enum {
  IPC_FRAME_FLAG_PBFRAME = 0x00,    // A/V P/B frame..
  IPC_FRAME_FLAG_IFRAME = 0x01,     // A/V I frame.
  IPC_FRAME_FLAG_MD = 0x02,         // For motion detection.
  IPC_FRAME_FLAG_IO = 0x03,         // For Alarm IO detection.
  IPC_FRAME_FLAG_EXTRAINFO = 0x04,  // For h264, it is spspps
} ENUM_FRAMEFLAG;

typedef enum {
  AUDIO_SAMPLE_8K = 0x00,
  AUDIO_SAMPLE_11K = 0x01,
  AUDIO_SAMPLE_12K = 0x02,
  AUDIO_SAMPLE_16K = 0x03,
  AUDIO_SAMPLE_22K = 0x04,
  AUDIO_SAMPLE_24K = 0x05,
  AUDIO_SAMPLE_32K = 0x06,
  AUDIO_SAMPLE_44K = 0x07,
  AUDIO_SAMPLE_48K = 0x08,
} ENUM_AUDIO_SAMPLERATE;

typedef enum {
  AUDIO_DATABITS_8 = 0,
  AUDIO_DATABITS_16 = 1,
} ENUM_AUDIO_DATABITS;

typedef enum {
  AUDIO_CHANNEL_MONO = 0,
  AUDIO_CHANNEL_STERO = 1,
} ENUM_AUDIO_CHANNEL;

/* Audio Frame: flags =  (samplerate << 2) | (databits << 1) | (channel) */

/* Audio/Video Frame Header Info */
typedef struct _FRAMEINFO {
  unsigned short codec_id;  // Media codec type defined in sys_mmdef.h,
                            // MEDIA_CODEC_AUDIO_PCMLE16 for audio,
                            // MEDIA_CODEC_VIDEO_H264 for video.
  unsigned char flags;      // Combined with IPC_FRAME_xxx.
  unsigned char cam_index;  // 0 - n

  unsigned char onlineNum;  // number of client connected this device
  unsigned char reserve1[3];

  unsigned int reserve2;   //
  unsigned int timestamp;  // Timestamp of the frame, in milliseconds

  // unsigned int videoWidth;
  // unsigned int videoHeight;

} FRAMEINFO_t;
#endif
