/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Benjo Lei <benjo.lei@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __WIFI_SETTING_INTERFACE_H__
#define __WIFI_SETTING_INTERFACE_H__

int setting_get_cmd_resolve(int nfp, char *buffer);
int tcp_func_get_on_off_recording(int nfp, char *buffer);
int tcp_func_get_ssid_pw(int nfp, char *buffer);
int tcp_func_get_back_camera_apesplution(int nfp, char *buffer);
int tcp_func_get_front_camera_apesplution(int nfp, char *buffer);
int tcp_func_get_front_setting_apesplution(int nfp, char * buffer);
int tcp_func_get_back_setting_apesplution(int nfp, char * buffer);
int tcp_func_get_format_status(int nfp, char *buffer);
int tcp_func_get_photo_quality(int nfp, char *buffer);
int tcp_func_get_setting_photo_quality(int nfp, char *buffer);
int setting_cmd_resolve(int nfd, char *cmdstr);
int debug_setting_get_cmd_resolve(int nfp, char *buffer);
void reg_msg_manager_cb(void);
void unreg_msg_manager_cb(void);
#endif
