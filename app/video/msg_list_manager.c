/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Chad.ma <chad.ma@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "common.h"
#include "msg_list_manager.h"

static struct type_node *g_listHead = NULL;

struct type_node *createList(void)
{
    struct type_node *l = (struct type_node *)malloc(sizeof(struct type_node));

    if (NULL == l)
        return NULL;

    l->callback = NULL;
    l->next = NULL;

    return l;
}

static struct type_node *appendList(struct type_node *l, fun_cb cb)
{
    struct type_node *p = NULL;

    if (NULL == l)
        return NULL;

    p = (struct type_node *)malloc(sizeof(struct type_node));

    if (NULL == p)
        return NULL;

    memset(p, 0, sizeof(struct type_node));

    /* Fisrt node */
    if (l->next == NULL) {
        p->callback = cb;
        p->next = NULL;
        l->next = p;
        return p;
    } else {
        while (l->next != NULL)
            l = l->next;
    }

    p->callback = cb;
    p->next = NULL;
    l->next = p;

    return p;
}

static int deleteNode(struct type_node *node)
{
    struct type_node *p = NULL;
    struct type_node *pre = NULL;

    if (NULL == node)
        return 0;

    p = g_listHead;

    while (p != node) {
        pre = p;
        p = p->next;
    }

    pre->next = p->next;
    free(p);

    printf("deleteNode OK! \n");
    return 1;
}

static void destoryList(struct type_node *l)
{
    struct type_node *next;
    struct type_node *p = l;

    if (NULL == p)
        return;

    while (p != NULL) {
        next = p->next;
        free(p);
        p = next;
    }
}

struct type_node *init_list(void)
{
    g_listHead = createList();
    return g_listHead;
}

struct type_node *reg_entry(fun_cb cb)
{
    return appendList(g_listHead, cb);
}

int unreg_entry(fun_cb cb)
{
    struct type_node *node = NULL;

    if (cb != NULL) {
        node = g_listHead;
        while(node != NULL && node->callback != cb)
            node = node->next;
    }

    return deleteNode(node);
}

void deinit_list(void)
{
    if (NULL != g_listHead) {
        destoryList(g_listHead);
        g_listHead = NULL;
    }
}

void dispatch_msg(void *msg, void *prama0, void *param1)
{
    struct type_node *p = g_listHead;

    if (!p) {
        printf("list is NULL \n");
        return;
    }
    do {
        if (p->callback)
            p->callback(msg, prama0, param1);
        p = p->next;
    } while (p);
}
