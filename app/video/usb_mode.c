/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "usb_mode.h"

#include <stdio.h>
#include "parameter.h"
#include "uvc_user.h"
#include "common.h"
#include "wifi_management.h"

#include "ueventmonitor/usb_sd_ctrl.h"
#include "usb_audio_source.h"

void cmd_IDM_USB(void)
{
    ui_deinit_uvc();
    ui_deinit_uac();

    parameter_save_video_usb(0);
    android_usb_config_ums();
    rndis_management_stop();
    printf("msc mode\n");
}

void cmd_IDM_ADB(void)
{
    ui_deinit_uvc();
    ui_deinit_uac();

    parameter_save_video_usb(1);
    android_usb_config_adb();
    rndis_management_stop();
    printf("adb mode\n");
}

void cmd_IDM_UVC(void)
{
    if (parameter_get_video_usb() == 2)
        return ;
    parameter_save_video_usb(2);
    ui_deinit_uac();
    ui_init_uvc();
    rndis_management_stop();
    printf("uvc mode\n");
}

void cmd_IDM_RNDIS(void)
{
    ui_deinit_uvc();
    ui_deinit_uac();
    parameter_save_video_usb(3);
    android_usb_config_rndis();
    rndis_management_start();
    printf("rndis mode\n");
}

void cmd_IDM_UAC(void)
{
    if (parameter_get_video_usb() == 4)
        return;
    ui_deinit_uvc();
    parameter_save_video_usb(4);
    ui_init_uac();
    rndis_management_stop();
    printf("uac mode\n");
}

void ui_deinit_uac(void)
{
    if (parameter_get_video_usb() == 4)
        uac_play_off();
}

void ui_init_uac(void)
{
    if (parameter_get_video_usb() == 4)
        android_usb_config_uac();
}

void ui_deinit_uvc()
{
#if USE_USB_WEBCAM
    if (parameter_get_video_usb() == 2)
        uvc_gadget_pthread_exit();
#endif
}

int ui_init_uvc()
{
    int ret = 0;

#if USE_USB_WEBCAM
    if (parameter_get_video_usb() == 2)
        android_usb_config_uvc();
#endif
    return ret;
}

