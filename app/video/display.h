/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#define MAX_WIN_NUM 6

struct display_window {
    unsigned int x;
    unsigned int y;
    unsigned int w;
    unsigned int h;
};

typedef void (*set_out_window_callback)(struct display_window* out_window, int out_window_num,
                               int screen_w, int screen_h, int win_num);

void set_display_window(struct display_window* win, int num, bool* pos);
void display_the_window(int position, int src_fd, int src_w, int src_h,
                        int src_fmt, int vir_w, int vir_h);
void shield_the_window(int position, bool black);
int display_set_position();
void display_reset_position(int pos);
void display_switch_pos(int *pos1, int *pos2);
void display_set_display_callback(void (*callback)(int fd, int width, int height));
void reset_display_window(void);
int display_get_rotate_angle(void);
void display_set_out_window_callback(set_out_window_callback callback);

#ifdef __cplusplus
}
#endif

#endif
