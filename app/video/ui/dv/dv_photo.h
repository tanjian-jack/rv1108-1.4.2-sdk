/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Chad.ma <chad.ma@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __DV_PHOTO_H__
#define __DV_PHOTO_H__

#include <stdbool.h>
#include <minigui/common.h>

int get_photo_resolution(void);
int get_dv_photo_burst_num_index (void);
void set_dv_photo_burst_num(int sel);
bool get_takephoto_state(void);
void set_takephoto_state(bool takingphoto);
void dv_photo_proc(HWND hWnd);
void dv_photo_burst_proc(HWND hWnd);

void dv_photo_lapse_proc(HWND hWnd);

void dv_take_photo_lapse(HWND hWnd);
void dv_take_photo(HWND hWnd);
void dv_take_photo_burst(HWND hWnd);

void show_photo_normal_menu(HDC hdc, HWND hWnd);
void show_photo_lapse_menu(HDC hdc, HWND hWnd);
void show_photo_burst_menu(HDC hdc, HWND hWnd);
void show_preview_delete_menu(HDC hdc, HWND hWnd);
void show_photo_submenu(HDC hdc, HWND hWnd, int module_id);

#endif /* __DV_PHOTO_H__ */
