/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "display_sample.h"
#include "display.h"
#include "ui_resolution.h"

#define SAMPLE_WINDOW_NUM 5

static struct display_window sample_window[SAMPLE_WINDOW_NUM] = {
    { 0, 0, WIN_W, WIN_H },
    { WIN_W * 4 / 5, WIN_H * 4 / 5, WIN_W / 5, WIN_H / 5 },
    { WIN_W * 3 / 5, WIN_H * 4 / 5, WIN_W / 5, WIN_H / 5 },
    { WIN_W * 2 / 5, WIN_H * 4 / 5, WIN_W / 5, WIN_H / 5 },
    { WIN_W * 1 / 5, WIN_H * 4 / 5, WIN_W / 5, WIN_H / 5 },
};

static bool sample_position[SAMPLE_WINDOW_NUM];

void set_display_sample_window(void)
{
    set_display_window(sample_window, SAMPLE_WINDOW_NUM, sample_position);
}

#define MAIN_WINDOW_NUM 1

static struct display_window main_window[MAIN_WINDOW_NUM] = {
    { 0, 0, WIN_W, WIN_H },
};

static bool main_position[MAIN_WINDOW_NUM];

void set_display_main_window(void)
{
    set_display_window(main_window, MAIN_WINDOW_NUM, main_position);
}
