/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define open_ "open"
#define copy "copy"
#define delete_ "delete"
#define rename "rename"
#define properties_ "properties"
#define File_name "File name"
#define File_size "File size"
#define Category "Category"
#define Last_modified_time "Last modified time"
#define ListView_control "ListView control"
#define directory "directory"
#define regular_file "regular file"
#define are_you_really_want_to_delete_this_file \
  "are you really want to delete this file?"
#define warning "warning"
#define Are_you_really_want_to_open_this_file \
  "Are you really want to open this file?"
#define Question "Question"

#define OK "OK"
#define Cancel "Cancel"
#define SetDate "Set Date"
#define SetLicense "Set License Plate "
#define SetCallNum "Set Call Num "
#define ShowWifiInfo "Show Wifi Info "

