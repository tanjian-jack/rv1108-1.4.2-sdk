/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hogan.wang@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "display_cvr.h"
#include "display.h"
#include "ui_resolution.h"
#include "common.h"

#define CVR_WINDOW_NUM 6
#define CVR_WINWOD_WIDTH  ALIGN_CUT((WIN_W / CVR_WINDOW_NUM), 16)
#define CVR_WINDOW_HEIGHT ALIGN_CUT((WIN_H / CVR_WINDOW_NUM), 16)
#define CVR_WINDOW_Y(n) (WIN_H - (n) * CVR_WINDOW_HEIGHT)
#define CVR_WINDOW_X(n) (WIN_W - (n) * CVR_WINWOD_WIDTH)

static struct display_window cvr_window[CVR_WINDOW_NUM] = {
    { 0, 0, WIN_W, WIN_H },
    { CVR_WINDOW_X(1), CVR_WINDOW_Y(1), CVR_WINWOD_WIDTH, CVR_WINDOW_HEIGHT },
    { CVR_WINDOW_X(2), CVR_WINDOW_Y(1), CVR_WINWOD_WIDTH, CVR_WINDOW_HEIGHT },
    { CVR_WINDOW_X(3), CVR_WINDOW_Y(1), CVR_WINWOD_WIDTH, CVR_WINDOW_HEIGHT },
    { CVR_WINDOW_X(4), CVR_WINDOW_Y(1), CVR_WINWOD_WIDTH, CVR_WINDOW_HEIGHT },
    { CVR_WINDOW_X(5), CVR_WINDOW_Y(1), CVR_WINWOD_WIDTH, CVR_WINDOW_HEIGHT },
};

static bool cvr_position[CVR_WINDOW_NUM];

void set_display_cvr_window(void)
{
    set_display_window(cvr_window, CVR_WINDOW_NUM, cvr_position);
}

#define MAIN_WINDOW_NUM 1

static struct display_window main_window[MAIN_WINDOW_NUM] = {
    { 0, 0, WIN_W, WIN_H },
};

static bool main_position[MAIN_WINDOW_NUM];

void set_display_main_window(void)
{
    set_display_window(main_window, MAIN_WINDOW_NUM, main_position);
}
