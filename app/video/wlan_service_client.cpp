/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: ZhiChao Yu zhichao.yu@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "wlan_service_client.h"

#include <stdio.h>
#include <string.h>

#include <string>

#include "librkipc/socket_client.h"

const char* kWlanServiceSocket = "/tmp/socket/wlan_service.sock";

int WlanServiceSetPower(const char* power) {
  rockchip::ipc::SocketClient client;

  client.Connect(kWlanServiceSocket);

  std::string command = std::string("set_power/") + power;
  client.Send(command.c_str());

  client.Disconnect();
  return 0;
}

int WlanServiceSetMode(const char* mode,
                       const char* ssid, const char* password) {
  rockchip::ipc::SocketClient client;

  client.Connect(kWlanServiceSocket);

  if (strcmp(mode, "station") == 0 || strcmp(mode, "ap") == 0) {
    std::string command = std::string("set_mode/") + mode +
                          "/" + ssid + "/" + password + "/";
    client.Send(command.c_str());
  } else {
    printf("Unsupported wlan mode: %s.\n", mode);
  }

  client.Disconnect();
  return 0;
}
