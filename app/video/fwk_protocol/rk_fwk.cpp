/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Dayao Ji <jdy@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include "rk_fwk.h"

#ifdef MSG_FWK
#include "libfwk_glue/fwk_glue.h"
#include "libfwk_controller/fwk_controller.h"
#endif

int rk_fwk_glue_init(void)
{
#ifdef MSG_FWK
	printf("rk_fwk_glue_init \n");
	fwk_glue_init();
#endif
	return 0;
}

int rk_fwk_glue_destroy(void)
{
#ifdef MSG_FWK
	fwk_glue_destroy();
#endif
	return 0;
}


int rk_fwk_controller_init(void)
{
#ifdef MSG_FWK
	printf("rk_fwk_controller_init \n");
	fwk_controller_init();
#endif

	return 0;
}

int rk_fwk_controller_destroy(void)
{
#ifdef MSG_FWK
	fwk_controller_destroy();
#endif
	return 0;
}
