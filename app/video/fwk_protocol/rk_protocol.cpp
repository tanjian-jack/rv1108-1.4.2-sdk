/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Dayao Ji <jdy@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include "rk_protocol.h"

#ifdef PROTOCOL_IOTC
#include "iotc/rk_iotc.h"
#endif

#ifdef PROTOCOL_GB28181
#include "gb28181/rk_gb28181.h"
#endif

int protocol_rk_iotc_init(void)
{
#ifdef PROTOCOL_IOTC
	printf("protocol_rk_iotc_init \n");
	rk_iotc_init();
#endif
	return 0;
}

int protocol_rk_iotc_destroy(void)
{
#ifdef PROTOCOL_IOTC
	rk_iotc_destroy();
#endif
	return 0;
}

int protocol_rk_gb28181_init(void)
{
#ifdef PROTOCOL_GB28181
	printf("protocol_rk_gb28181_init \n");
	rk_gb28181_init();
#endif
	return 0;
}

int protocol_rk_gb28181_destroy(void)
{
#ifdef PROTOCOL_GB28181
	rk_gb28181_destroy();
#endif
	return 0;
}
