/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Cain Cai <cain.cai@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __THERMAL_H__
#define __THERMAL_H__

#include <stdbool.h>

#define THRESHOLD_MAX 5

enum {
    THERMAL_LEVEL0 = 0,
    THERMAL_LEVEL1,
    THERMAL_LEVEL2,
    THERMAL_LEVEL3,
    THERMAL_LEVEL4,
    THERMAL_LEVEL5,
};

struct thermal_dev_t {
    char *name;
    int temp;
    int ltemp;
    int htemp;
    int state;
    int old_state;
    int lthreshold[THRESHOLD_MAX];
    int hthreshold[THRESHOLD_MAX];
    bool stop_kernel_thermal;

    bool handle_brightness;
    int brightness;
    int cur_brightness;

    bool handle_wifi;
    bool off_wifi;

    bool handle_fb_fps;
    int fb_fps;
    int cur_fb_fps;

    bool handle_3dnr;
    bool off_3dnr;

    bool handle_ddr;
    bool ddr_low_power;
    char ddr_state;
};

int thermal_init(void);
void thermal_update_state(void);
int thermal_get_status(void);

#endif
