/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "power.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <pthread.h>

#include "public_interface.h"

pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_t standby_thread;
int run_state = 0;

void *standby_thread_function(void)
{
    while(run_state) {
        pthread_mutex_lock(&mut);
        pthread_cond_wait(&cond, &mut);
        pthread_mutex_unlock(&mut);

        if (run_state == 0)
            break;

        api_change_mode(MODE_SUSPEND);
        sleep(1);
    }

    return NULL;
}

void standby_run(void)
{
    pthread_mutex_lock(&mut);
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mut);
}

int power_init(void)
{
    int err;

    run_state = 1;
    if (pthread_mutex_init(&mut, NULL) != 0)
        printf("mutex init error\n");

    if (pthread_cond_init(&cond, NULL) != 0)
        printf("cond init error\n");

    err = pthread_create(&standby_thread,NULL,(void *)standby_thread_function,NULL);
    if (err != 0)
        printf("can't create thread:%s\n", strerror(err));

    return 0;
}

void power_deinit(void)
{
    pthread_mutex_lock(&mut);
    run_state = 0;
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mut);
    pthread_join(standby_thread, NULL);
    pthread_mutex_destroy(&mut);
    pthread_cond_destroy(&cond);
}