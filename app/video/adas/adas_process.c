/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Zain Wang<wzz@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "adas_process.h"
#include "parameter.h"
#include "ui_resolution.h"
#include <dpp/algo/adas/adas.h>
#include <stdio.h>
#include <string.h>
#include <video.h>
#include <sys/time.h>
#include <signal.h>
#include "gps/nmea_parse.h"
#include "public_interface.h"
#include "parking_monitor/kalman.h"

#define ADAS_KALMAN_FILTER_SENSITIVE_HEIGHT     0.0002
#define ADAS_KALMAN_FILTER_SENSITIVE_LOW        0.002

#define ADAS_LDW_RESTRICT_HIGH                  0.70
#define ADAS_LDW_RESTRICT_LOW                   0.90

struct adas_speed_detection {
        struct gps_info         gps;
        struct kalman           kalman;
        struct gsensor_data     gs_data;
        int                     gs_cnt;
        int                     car_status;
        int                     car_status_store;
        float                   speed;
        float                   speed_store;
};

#define ADAS_PRE_SET_LDW                        (1 << 0)
#define ADAS_PRE_SET_FCW                        (1 << 1)
#define ADAS_PRE_SET_MASK \
            (ADAS_PRE_SET_LDW | ADAS_PRE_SET_FCW)
struct adas_output_pre {
        struct adas_output  adas_in;
        char                flag;
};

static struct adas_parameter g_adas_parameter;
static struct adas_info g_adas_info;
static struct adas_process_output g_adas_output;
static struct adas_speed_detection g_adas_speed;
static struct adas_output_pre g_adas_in;

static struct itimerval g_timer_val;

void AdasGetParameter(struct adas_parameter *parameter)
{
    memcpy(parameter, &g_adas_parameter, sizeof(g_adas_parameter));
}

static void AdasLimitValue(char value, char max, char min)
{
    if (value > max)
        value = max;
    if (value < min)
        value = min;
}

static void AdasCheckParameter(struct adas_parameter *parameter)
{
    AdasLimitValue(parameter->adas_setting[0], 0, 100);
    AdasLimitValue(parameter->adas_setting[1], 0, 100);
    AdasLimitValue(parameter->adas_direction, 0, 100);
}

void AdasSaveParameter(struct adas_parameter *parameter)
{
    AdasCheckParameter(parameter);
    memcpy(&g_adas_parameter, parameter, sizeof(g_adas_parameter));
    parameter_save_video_adas_setting(g_adas_parameter.adas_setting);
    parameter_save_video_adas_alert_distance(g_adas_parameter.adas_alert_distance);
    parameter_save_video_adas_direction(g_adas_parameter.adas_direction);
    parameter_save_video_adas(g_adas_parameter.adas_state);
    dpp_adas_set_calibrate_line(
            (int)(ADAS_FCW_HEIGHT * ((float)parameter->adas_setting[0] / 100)),
            (int)(ADAS_FCW_HEIGHT * ((float)parameter->adas_setting[1] / 100)));
}

static int AdasCarStatusDetection(float speed)
{
    int car_status;

    if (g_adas_info.is_gps_support || g_adas_info.is_gs_support) {
        if (speed < ADAS_SPEED_SLOW)
            car_status = ADAS_CAR_STATUS_STOP;
        else if (speed < ADAS_SPEED_FAST)
            car_status = ADAS_CAR_STATUS_PACE;
        else
            car_status = ADAS_CAR_STATUS_RUN;
    } else {
        car_status = ADAS_CAR_STATUS_UNKNOWN;
    }

    return car_status;
}

/*
 *   return:
 *      -1  below ADAS_RESTRICT_LOW
 *      1   upon ADAS_RESTRICT_HEIGHT
 *      0   between ADAS_RESTRICT_HEIGHT and ADAS_RESTRICT_LOW
 */
static int AdasPointArea(struct adas_pos pos)
{
    if (pos.y < g_adas_info.ldw_restrict_high)
        return 1;
    else if (pos.y > g_adas_info.ldw_restrict_low)
        return -1;
    return 0;
}

static int AdasRestrictLineOutput(struct adas_pos *in, struct adas_pos *out)
{
    int pos_area[2];
    int high = g_adas_info.ldw_restrict_high;
    int low = g_adas_info.ldw_restrict_low;
    float a, b;

    pos_area[0] = AdasPointArea(in[0]);
    pos_area[1] = AdasPointArea(in[1]);

    if (((pos_area[0] == 1) && (pos_area[1] == 1)) ||
        ((pos_area[0] == -1) && (pos_area[1] == -1))) {
        return -1;
    } else if ((pos_area[0] == 0) && (pos_area[1] == 0)) {
        out[0] = in[0];
        out[1] = in[1];
    } else {
        if (pos_area[0] == 1) {
            if (pos_area[1] == 0)
                low = in[1].y;
        } else if (pos_area[0] == 0) {
            if (pos_area[1] == 1)
                high = in[0].y;
            else
                low = in[0].y;
        } else {
            if (pos_area[1] == 0)
                high = in[1].y;
        }
        out[0].y = high;
        out[1].y = low;
        if (in[0].x == in[1].x) {
            out[0].x = in[0].x;
            out[1].x = in[0].x;
        } else {
            a = (float)(in[0].y - in[1].y) / (in[0].x - in[1].x);
            b = in[0].y - (a * in[0].x);

            out[0].x = (out[0].y - b) / a;
            out[1].x = (out[1].y - b) / a;
        }
    }
    return 0;
}

static void AdasIsValid(struct adas_output *in, bool *odt_valid,
                        bool *ldw_valid)
{
    int i;

    *ldw_valid = false;
    *odt_valid = false;

    if (!memcmp(in, &g_adas_info.output_store, sizeof(struct adas_output)))
        return;

    memcpy(&g_adas_info.output_store, in, sizeof(struct adas_output));

    if (in->odt.valid && in->odt.count &&
        (g_adas_parameter.adas_state & ADAS_MODE_FCW)) {
        *odt_valid = true;
    }

    if (in->ldw.valid && (g_adas_parameter.adas_state & ADAS_MODE_LDW)) {
        for (i = 0; i < 4; i++) {
            if ((in->ldw.end_points[i][0] != 0 ||
                in->ldw.end_points[i][1] != 0) &&
                memcmp(&g_adas_info.ldw_store, &in->ldw,
                       sizeof(struct ldw_output))) {
                    *ldw_valid = true;
                    memcpy(&g_adas_info.ldw_store, &in->ldw,
                           sizeof(struct ldw_output));
            }
        }
    }
}

static void AdasProcessFCW(struct adas_output *in)
{
    int i;
    struct odt_object *obj;
    struct adas_fcw *fcw;
    int direction = (int)(g_adas_info.width *
                         ((float)g_adas_parameter.adas_direction / 100));

    /* FCW process */
    pthread_mutex_lock(&g_adas_info.process_mutex);
    if (g_adas_speed.car_status != g_adas_speed.car_status_store) {
        g_adas_output.car_status = g_adas_speed.car_status;
        g_adas_speed.car_status_store = g_adas_speed.car_status;
    }

    g_adas_output.fcw_count = in->odt.count;
    for (i = 0; i < in->odt.count; i++) {
        obj = &in->odt.objects[i];
        fcw = &g_adas_output.fcw_out[i];
        fcw->img.x = (obj->x * g_adas_info.width) / ADAS_FCW_WIDTH;
        fcw->img.y = (obj->y * g_adas_info.height) / ADAS_FCW_HEIGHT;
        fcw->img.width = (obj->width * g_adas_info.width) / ADAS_FCW_WIDTH;
        fcw->img.height = (obj->height* g_adas_info.height) / ADAS_FCW_HEIGHT;
        fcw->distance = obj->distance;
        fcw->alert = 0;

        if (g_adas_output.car_status == ADAS_CAR_STATUS_RUN) {
            int alert = (g_adas_output.speed - ADAS_SPEED_FAST) *
                        g_adas_parameter.coefficient + g_adas_parameter.base;

            if ((obj->distance < alert) &&
                (fcw->img.x < direction &&
                 (fcw->img.x + fcw->img.width) > direction))
                fcw->alert = 1;
        } else if (g_adas_output.car_status == ADAS_CAR_STATUS_STOP) {
            if ((obj->distance > g_adas_parameter.adas_alert_distance) &&
                (fcw->img.x < direction &&
                 (fcw->img.x + fcw->img.width) > direction))
                fcw->alert = 1;
        }
        g_adas_output.fcw_alert += fcw->alert;
    }
    pthread_mutex_unlock(&g_adas_info.process_mutex);

    if (g_adas_output.fcw_alert) {
        if (g_adas_output.car_status == ADAS_CAR_STATUS_RUN)
            g_adas_output.fcw_alert = ADAS_ALERT_RUN;
        else if (g_adas_output.car_status == ADAS_CAR_STATUS_STOP)
            g_adas_output.fcw_alert = ADAS_ALERT_STOP;
    }
}

static void AdasProcessLDW(struct adas_output *in)
{
    struct adas_pos pos[4];
    struct ldw_output *ldw;
    int i;

    g_adas_output.ldw_flag = in->ldw.turn_flag + 1;
    ldw = &in->ldw;
    for (i = 0; i < 4; i++) {
        pos[i].x =
            ldw->end_points[i][0] * g_adas_info.width / ADAS_LDW_WIDTH;
        pos[i].y =
            ldw->end_points[i][1] * g_adas_info.height / ADAS_LDW_HEIGHT;
    }
    AdasRestrictLineOutput(&pos[0], &g_adas_output.ldw_pos[0]);
    AdasRestrictLineOutput(&pos[2], &g_adas_output.ldw_pos[2]);
}

static void AdasProcess(struct adas_output *in)
{
    bool ldw_valid = false, odt_valid = false, speed_valid = false;

    g_adas_output.fcw_count = 0;
    g_adas_output.fcw_alert = 0;
    g_adas_output.ldw_flag = 0;

    AdasIsValid(in, &odt_valid, &ldw_valid);
    /* Speed process */
    pthread_mutex_lock(&g_adas_info.process_mutex);
    if (g_adas_speed.speed != g_adas_speed.speed_store) {
        g_adas_speed.speed_store = g_adas_speed.speed;
        if (g_adas_speed.car_status != g_adas_speed.car_status_store) {
            g_adas_output.car_status = g_adas_speed.car_status;
            g_adas_speed.car_status_store = g_adas_speed.car_status;
        }
        speed_valid = true;
    }
    g_adas_output.speed = g_adas_speed.speed;
    pthread_mutex_unlock(&g_adas_info.process_mutex);

    if (odt_valid)
        AdasProcessFCW(in);

    /* LDW process */
    if (ldw_valid)
        AdasProcessLDW(in);

    if ((ldw_valid || odt_valid || speed_valid) && g_adas_info.callback)
        g_adas_info.callback(0, (void *)&g_adas_output, NULL);
}

static void AdasProcessPre(int cmd, void *msg0, void *msg1)
{
    struct adas_output *in = (struct adas_output *)msg0;

    if (in->ldw.valid) {
        if (g_adas_in.flag & ADAS_PRE_SET_LDW)
            AdasProcess(&g_adas_in.adas_in);
        else
            g_adas_in.flag |= ADAS_PRE_SET_LDW;
        memcpy(&g_adas_in.adas_in.ldw, &in->ldw,
               sizeof(g_adas_in.adas_in.ldw));
    }

    if (in->odt.valid) {
        if (g_adas_in.flag & ADAS_PRE_SET_FCW)
            AdasProcess(&g_adas_in.adas_in);
        else
            g_adas_in.flag |= ADAS_PRE_SET_FCW;
        memcpy(&g_adas_in.adas_in.odt, &in->odt,
               sizeof(g_adas_in.adas_in.odt));
    }

    if ((g_adas_in.flag & ADAS_PRE_SET_MASK) == ADAS_PRE_SET_MASK) {
        AdasProcess(&g_adas_in.adas_in);
        memset(&g_adas_in.adas_in, 0, sizeof(g_adas_in.adas_in));
        g_adas_in.flag = 0;
    }
}

int AdasEventRegCallback(void (*call)(int cmd, void *msg0, void *msg1))
{
    g_adas_info.callback = call;
    return 0;
}

void AdasGpsUpdate(void *gps)
{
    if(gps == NULL)
        return;

    pthread_mutex_lock(&g_adas_info.data_mutex);
    memcpy(&g_adas_speed.gps, gps, sizeof(struct gps_info));
    pthread_mutex_unlock(&g_adas_info.data_mutex);
}

static int adas_get_gsdata(struct sensor_axis data)
{
    pthread_mutex_lock(&g_adas_info.data_mutex);
    if (g_adas_speed.gs_cnt < MAX_BUF_LEN) {
        memcpy(&g_adas_speed.gs_data.sa[g_adas_speed.gs_cnt], &data,
               sizeof(data));
        g_adas_speed.gs_cnt++;
    }
    pthread_mutex_unlock(&g_adas_info.data_mutex);

    return 0;
}

static void adas_timer_handle(int sigo)
{
    pthread_mutex_lock(&g_adas_info.process_mutex);
    pthread_mutex_lock(&g_adas_info.data_mutex);

    g_adas_speed.speed = 0;

    if (g_adas_info.is_gps_support) {
        if (g_adas_speed.gps.status == 'A') {
            g_adas_speed.speed = g_adas_speed.gps.speed;
            goto BACK;
        }
    }

    if (g_adas_info.is_gs_support) {
        if (g_adas_speed.gs_cnt) {
            if (sliding_filter(&g_adas_speed.kalman, &g_adas_speed.gs_data,
                g_adas_speed.gs_cnt,
                g_adas_output.car_status == ADAS_CAR_STATUS_STOP ?
                (double)ADAS_KALMAN_FILTER_SENSITIVE_LOW :
                (double)ADAS_KALMAN_FILTER_SENSITIVE_HEIGHT))
                /*
                 * If GPS unable to work, we can use GS instead GPS.
                 * But GS unsupport speed data, we can only know whether the
                 * the car in moved or stopped.
                 * For less mistake alrams, let's defined the default
                 * speed(30 Km/h) for GS moved. In this speed, It always in
                 * ADAS_CAR_STATUS_PACE which can't meet any alarms.
                 */
                g_adas_speed.speed = 30;
            else
                g_adas_speed.speed = 0;
        }
    }

    g_adas_speed.car_status = AdasCarStatusDetection(g_adas_speed.speed);
BACK:
    g_adas_speed.gs_cnt = 0;
    pthread_mutex_unlock(&g_adas_info.data_mutex);
    pthread_mutex_unlock(&g_adas_info.process_mutex);
}

static void adas_timer_init(void)
{
    struct sigaction act;

    act.sa_handler = adas_timer_handle;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    sigaction(SIGALRM, &act, NULL);

    g_timer_val.it_value.tv_sec = 1;
    g_timer_val.it_value.tv_usec = 0;
    g_timer_val.it_interval.tv_sec = 1;
    g_timer_val.it_interval.tv_usec = 0;

    setitimer(ITIMER_REAL, &g_timer_val, NULL);
}

static void adas_timer_deinit(void)
{
    g_timer_val.it_value.tv_sec = 0;
    g_timer_val.it_interval.tv_sec = 0;

    setitimer(ITIMER_REAL, &g_timer_val, NULL);
}

int AdasInit(int width, int height)
{
    memset(&g_adas_parameter, 0, sizeof(g_adas_parameter));
    memset(&g_adas_speed, 0, sizeof(g_adas_speed));
    memset(&g_adas_in, 0, sizeof(g_adas_in));
    parameter_get_video_adas_setting(g_adas_parameter.adas_setting);
    g_adas_parameter.adas_alert_distance = parameter_get_video_adas_alert_distance();
    g_adas_parameter.adas_direction = parameter_get_video_adas_direction();
    g_adas_parameter.adas_state = parameter_get_video_adas();
    g_adas_info.width = width;
    g_adas_info.height = height;

    g_adas_parameter.coefficient = 0.75;
    g_adas_parameter.base = 10;
    g_adas_info.is_gps_support = false;
    g_adas_info.is_gs_support = false;

    if (g_adas_parameter.adas_state & ADAS_MODE_FCW) {
        if (api_gps_init() < 0)
            printf("[ADAS] Warning: %s: Can't open GPS\n", __func__);
        else
            g_adas_info.is_gps_support = true;

        g_adas_info.gsensor_reg = gsensor_regsenddatafunc(adas_get_gsdata);
        if (g_adas_info.gsensor_reg <= 0) {
            printf("[ADAS] Warning: %s: Can't open GS\n", __func__);
        } else {
            g_adas_info.is_gs_support = true;
            kalman_init(&g_adas_speed.kalman);
        }

        if (g_adas_info.is_gps_support || g_adas_info.is_gs_support)
            adas_timer_init();
    }

    g_adas_info.ldw_restrict_high = height * ADAS_LDW_RESTRICT_HIGH;
    g_adas_info.ldw_restrict_low = height * ADAS_LDW_RESTRICT_LOW;

    dpp_adas_set_calibrate_line(
            (int)(ADAS_FCW_HEIGHT * ((float)g_adas_parameter.adas_setting[0] / 100)),
            (int)(ADAS_FCW_HEIGHT * ((float)g_adas_parameter.adas_setting[1] / 100)));
    ADAS_RegEventCallback(AdasProcessPre);
    g_adas_speed.gs_cnt = 0;
    pthread_mutex_init(&g_adas_info.data_mutex, NULL);
    pthread_mutex_init(&g_adas_info.process_mutex, NULL);

    return 0;
}

void AdasDeinit(void)
{
    if (g_adas_parameter.adas_state) {
        adas_timer_deinit();
        if (g_adas_info.is_gs_support) {
            gsensor_unregsenddatafunc(g_adas_info.gsensor_reg);
            g_adas_info.is_gs_support = false;
        }
        if (g_adas_info.is_gps_support) {
            api_gps_deinit();
            g_adas_info.is_gps_support = false;
        }
        AdasSaveParameter(&g_adas_parameter);
        pthread_mutex_destroy(&g_adas_info.data_mutex);
        pthread_mutex_destroy(&g_adas_info.process_mutex);
    }
}

int AdasOn(int mode)
{
    if (g_adas_parameter.adas_state != mode &&
        mode != ADAS_MODE_NONE) {
        if (mode & ADAS_MODE_FCW) {
            if (!g_adas_info.is_gps_support) {
                if (api_gps_init() < 0)
                    printf("[ADAS] Warning: %s: Can't open GPS\n", __func__);
                else
                    g_adas_info.is_gps_support = true;
            }

            if (!g_adas_info.is_gs_support) {
                g_adas_info.gsensor_reg =
                                gsensor_regsenddatafunc(adas_get_gsdata);
                if (g_adas_info.gsensor_reg <= 0) {
                    printf("[ADAS] Warning: %s: Can't open GS\n", __func__);
                } else {
                    g_adas_info.is_gs_support = true;
                    kalman_init(&g_adas_speed.kalman);
                }
            }

            if (g_adas_info.is_gps_support || g_adas_info.is_gs_support)
                adas_timer_init();
        } else if (mode == ADAS_MODE_LDW) {
            adas_timer_deinit();
            if (g_adas_info.is_gs_support)
                gsensor_unregsenddatafunc(g_adas_info.gsensor_reg);
            if (g_adas_info.is_gps_support)
                api_gps_deinit();
            g_adas_info.is_gps_support = false;
            g_adas_info.is_gs_support = false;
        }
        g_adas_parameter.adas_state = mode;
        parameter_save_video_adas(g_adas_parameter.adas_state);
    }

    return 0;
}

int AdasOff(void)
{
    if (g_adas_parameter.adas_state) {
        if (g_adas_parameter.adas_state & ADAS_MODE_FCW) {
            adas_timer_deinit();
            if (g_adas_info.is_gs_support)
                gsensor_unregsenddatafunc(g_adas_info.gsensor_reg);
            if (g_adas_info.is_gps_support)
                api_gps_deinit();
            g_adas_info.is_gps_support = false;
            g_adas_info.is_gs_support = false;
        }
        g_adas_parameter.adas_state = 0;
        parameter_save_video_adas(g_adas_parameter.adas_state);
    }
    return 0;
}

void AdasSetWH(int width, int height)
{
    g_adas_info.width = width;
    g_adas_info.height = height;
}
