/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: Zain Wang<wzz@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __ADAS_PROCESS_H_
#define __ADAS_PROCESS_H_

#include <stdbool.h>
#include <dpp/algo/adas/adas.h>
#include <pthread.h>

#define ADAS_MODE_NONE                  0
#define ADAS_MODE_LDW                   1
#define ADAS_MODE_FCW                   2
#define ADAS_MODE_LDW_FCW               3

#define ADAS_ALERT_STOP                 1
#define ADAS_ALERT_RUN                  2

#define ADAS_CAR_STATUS_UNKNOWN         0
#define ADAS_CAR_STATUS_STOP            1
#define ADAS_CAR_STATUS_PACE            2
#define ADAS_CAR_STATUS_RUN             3

#define ADAS_SPEED_SLOW                 2
#define ADAS_SPEED_FAST                 50

struct adas_parameter {
    char adas_state;
    char adas_setting[2];
    char adas_alert_distance;
    char adas_direction;

    float coefficient;
    char base;
};

struct adas_info {
    int width;
    int height;
    int gsensor_reg;
    pthread_mutex_t data_mutex;
    pthread_mutex_t process_mutex;
    void (*callback)(int cmd, void *msg0, void *msg1);
    int ldw_restrict_high;
    int ldw_restrict_low;
    bool is_gps_support;
    bool is_gs_support;
    struct adas_output output_store;
    struct ldw_output ldw_store;
};

struct adas_img {
    int x;
    int y;
    int width;
    int height;
};

struct adas_fcw {
    int alert;
    float distance;
    struct adas_img img;
};

struct adas_pos {
    int x;
    int y;
};

struct adas_process_output {
    int fcw_count;
    int fcw_alert;
    struct adas_fcw fcw_out[ODT_MAX_OBJECT];
    /* 0: LDW invalid 1: turn left 2: normal 3 turn right */
    int ldw_flag;
    struct adas_pos ldw_pos[4];
    float speed;
    int car_status;
};

int AdasInit(int width, int height);
void AdasDeinit(void);
int AdasOn(int mode);
int AdasOff(void);
void AdasSetWH(int width, int height);
void AdasGetParameter(struct adas_parameter *parameter);
void AdasSaveParameter(struct adas_parameter *parameter);
int AdasEventRegCallback(void (*call)(int cmd, void *msg0, void *msg1));
void AdasGpsUpdate(void *gps);

#endif
