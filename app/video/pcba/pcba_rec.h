/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * Author: benjo.lei <benjo.lei@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _PCBA_REC_H
#define _PCBA_REC_H

#define CMD_SPEECH_REC            0

enum pcbaRec {
    SPEECH_START_WAKEUP         = 0,
    SPEECH_START_RECORD,
    SPEECH_STOP_RECORD,
    SPEECH_TAKE_PHOTOS,
    SPEECH_LOCK_FILE,
    SPEECH_BACK_CAR,
    SPEECH_CANCEL_BACK_CAR,
    SPEECH_OPENSCREEN,
    SPEECH_CLOSESCREEN,
    SPEECH_END,
    SPEECH_UNKNOW
};

#ifdef __cplusplus
extern "C" {
#endif

int pcba_register(int nfd);
int pcba_unregister(void);

#ifdef __cplusplus
}
#endif

#endif
