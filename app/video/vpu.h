/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __VPU_ENCODE_H__
#define __VPU_ENCODE_H__

#include <memory.h>
#include <stdint.h>
#include <stdio.h>

#include <mpp/mpp_log.h>
#include <mpp/rk_mpi.h>
#include <libavutil/pixfmt.h>

#include "common.h"

#define MPP_ALIGN(x, a) (((x) + (a)-1) & ~((a)-1))

struct vpu_encode {
  MppCtx mpp_ctx;
  MppApi* mpi;
  int width;
  int height;
  struct video_ion jpeg_enc_out;
  RK_U8* enc_out_data;
  RK_U32 enc_out_length;
  MppPacket packet;
};

struct vpu_decode {
  int in_width;
  int in_height;
  RK_S32 pkt_size;
  MppCtx mpp_ctx;
  MppApi* mpi;
  MppBufferGroup memGroup;
  MppFrameFormat fmt;
};

int vpu_nv12_encode_jpeg_init_ext(struct vpu_encode* encode,
                              int width,
                              int height,
                              int quant);

int vpu_nv12_encode_jpeg_init(struct vpu_encode* encode, int width, int height);

int vpu_nv12_encode_jpeg_doing(struct vpu_encode* encode,
                               void* srcbuf,
                               int src_fd,
                               size_t src_size);

void vpu_nv12_encode_jpeg_done(struct vpu_encode* encode);

int vpu_decode_jpeg_init(struct vpu_decode* decode, int width, int height);

int vpu_decode_jpeg_doing(struct vpu_decode* decode,
                          char* in_data,
                          RK_S32 in_size,
                          int out_fd,
                          char* out_data);

int vpu_decode_jpeg_done(struct vpu_decode* decode);

inline enum AVPixelFormat vpu_color_space2ffmpeg(MppFrameFormat mpp_fmt);

#endif
