/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __UEVENTMONITOR_H__
#define __UEVENTMONITOR_H__

#include <stdio.h>
#include <stdbool.h>

struct _uevent {
    char *strs[30];
    int size;
};

enum battery_event_cmd {
    CMD_UPDATE_CAP,
    CMD_LOW_BATTERY,
    CMD_DISCHARGE
};

enum gpios_name_id {
    CAR_REVERSE_ID,
    VEHICLE_EXIT_ID,
    MIX_CIF0_PLUG_ID,
    MIX_CIF1_PLUG_ID,
    MIX_CIF2_PLUG_ID,
    MIX_CIF3_PLUG_ID,
    CVBSIN_PLUG_ID,
    GPIOS_NAME_ID_MAX
};
char *gpios_name_str[GPIOS_NAME_ID_MAX];

bool charging_status_check(void);
int batt_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
int hdmi_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
int cvbsout_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
int camera_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
int uevent_monitor_run(void);
int gpios_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
void cvr_check_reverse_system(void);
int sd_hotplug_timer(void);
void sd_hotplug_insert(void);
void sd_hotplug_reset(void);

#endif
