/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __USB_SD_CTRL_H__
#define __USB_SD_CTRL_H__

#include <stdio.h>
#include "ueventmonitor.h"

#define SD_CHANGE   0
#define SD_MOUNT_FAIL   1

enum usb_sd_type {
    SD_CTRL,
    USB_CTRL,
    USB_STORAGE_CTRL
};

enum sd_state_type {
    SD_STATE_OUT,
    SD_STATE_IN,
    SD_STATE_ERR,
    SD_STATE_SUS,
    SD_STATE_FORMATING
};

int sd_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
int usb_reg_event_callback(void (*call)(int cmd, void *msg0, void *msg1));
void cvr_sd_ctl(char mount);
void android_usb_init(void);
void cvr_android_usb_ctl(const struct _uevent *event);
void cvr_audio_source_ctl(const struct _uevent *event);
void cvr_usb_sd_ctl(enum usb_sd_type type, char state);
void selct_disk_charging(int disk);
void android_usb_config_ums(void);
void android_usb_config_adb(void);
void android_usb_config_rndis(void);
void android_usb_config_uvc(void);
void set_sd_mount_state(int state);
int get_sd_mount_state(void);
bool cvr_usb_mode_switch(const struct _uevent *event);
void android_adb_keeplive(void);
void android_usb_config_uac(void);

#endif
