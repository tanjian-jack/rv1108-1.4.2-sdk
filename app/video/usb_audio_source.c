/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Zain Wang <wzz@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "usb_audio_source.h"

#include <pthread.h>

#include "audio/playback/pcmreplay.h"
#include "ueventmonitor/usb_sd_ctrl.h"

#include <autoconfig/main_app_autoconf.h>

#define REPLAY_HW_DEV       "hw:1,0"

struct uac_dev {
	void *uac_replay;
	bool alt;
	unsigned int sample_rate;
};

static struct uac_dev uac;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static void uac_play_refresh(void)
{
    pthread_mutex_lock(&mutex);
    if ((uac.sample_rate == MAIN_APP_AUDIO_SAMPLE_RATE) && uac.alt) {
        if (!uac.uac_replay)
            uac.uac_replay = start_replay_from_mic(REPLAY_HW_DEV);
    } else {
        if (uac.uac_replay)
            stop_replay_from_mic(uac.uac_replay);
    }
    pthread_mutex_unlock(&mutex);
}

void uac_play_on(void)
{
    uac.alt = true;
    uac_play_refresh();
}

void uac_play_off(void)
{
    uac.alt = false;
    uac.sample_rate = 0;
    uac_play_refresh();
    uac.uac_replay = NULL;
}

void uac_set_sample_rate(unsigned int sample_rate)
{
    uac.sample_rate = sample_rate;
    printf("[UAC] set sample_rate %d\n", sample_rate);
    uac_play_refresh();
}

#if 0
void uac_init(void)
{
    android_usb_config_uac();
    uac.uac_replay = NULL;
    uac.alt = false;
    uac.sample_rate = 0;
}

void uac_uvc_deinit(void)
{
    uac_play_off();
}
#endif

