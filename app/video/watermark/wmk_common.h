/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _WATER_MARK_COMMON_H
#define _WATER_MARK_COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#ifndef BOOL
typedef int BOOL;
#define TRUE  1
#define FALSE 0
#endif

/* MAX/MIN/ABS macors */
#ifndef MAX
#define MAX(x, y)           (((x) > (y))?(x):(y))
#endif

#ifndef MIN
#define MIN(x, y)           (((x) < (y))?(x):(y))
#endif

#define WMK_HAS_64BIT_TYPE    long long
typedef uint32_t          pixel_t;

#define get_r_value(rgba)      ((unsigned char)(rgba))
#define get_g_value(rgba)      ((unsigned char)(((unsigned long)(rgba)) >> 8))
#define get_b_value(rgba)      ((unsigned char)((unsigned long)(rgba) >> 16))
#define get_a_value(rgba)      ((unsigned char)((unsigned long)(rgba) >> 24))

#define WMK_LIL_ENDIAN  1234
#define WMK_BIG_ENDIAN  4321
#define WMK_BYTEORDER   WMK_LIL_ENDIAN

#define BYTES_PER_PIXEL 2
#define BITS_PER_PIXEL 16
#endif
