/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __WMK_RECT_H__
#define __WMK_RECT_H__

#include "ui_resolution.h"
#include "wmk_glyph.h"

/* watermark rect, must 16-byte alignment */
/* 3840x2160 */
#define WATERMARK4K_IMG_W       288
#define WATERMARK4K_IMG_H       96

/* 2560x1440 */
#define WATERMARK1440p_IMG_W       192
#define WATERMARK1440p_IMG_H       64

/* 1920x1080 */
#define WATERMARK1080p_IMG_W       144
#define WATERMARK1080p_IMG_H       48

/* 1280x720 */
#define WATERMARK720p_IMG_W        96
#define WATERMARK720p_IMG_H        32

/* 640x480 */
#define WATERMARK480p_IMG_W        72
#define WATERMARK480p_IMG_H        24

/* 480x320 */
#define WATERMARK360p_IMG_W        48
#define WATERMARK360p_IMG_H        16

/* 320x240 */
#define WATERMARK240p_IMG_W        48
#define WATERMARK240p_IMG_H        16

#ifdef MULTI_COLOR_FONT
#define WATERMARK_TIME_FONTX        MAX_TIME_LEN * WMK_FONTX
#define WATERMARK_TIME_FONTY        WMK_FONTY
#define WATERMARK_LICENSE_FONTX     (WMK_CHINESE_FONTX + (MAX_LICN_NUM - 1) * WMK_FONTX)
#define WATERMARK_LICENSE_FONTY     WMK_CHINESE_FONTY
#else
#define WATERMARK_TIME_FONTX        WATERMARK_TIME_W
#define WATERMARK_TIME_FONTY        WATERMARK_TIME_H
#define WATERMARK_LICENSE_FONTX     WATERMARK_LICN_W
#define WATERMARK_LICENSE_FONTY     WATERMARK_LICN_Y
#endif

/* 3840x2160 */
#define WATERMARK4K_TIME_W          WATERMARK_TIME_FONTX * 4
#define WATERMARK4K_TIME_H          WATERMARK_TIME_FONTY * 4
#define WATERMARK4K_LICENSE_W       WATERMARK_LICENSE_FONTX * 4
#define WATERMARK4K_LICENSE_H       WATERMARK_LICENSE_FONTY * 4

/* 2560x1440 */
#define WATERMARK1440p_TIME_W       WATERMARK_TIME_FONTX * 3
#define WATERMARK1440p_TIME_H       WATERMARK_TIME_FONTY * 3
#define WATERMARK1440p_LICENSE_W    WATERMARK_LICENSE_FONTX * 3
#define WATERMARK1440p_LICENSE_H    WATERMARK_LICENSE_FONTY * 3

/* 1920x1080 */
#define WATERMARK1080p_TIME_W       WATERMARK_TIME_FONTX * 2
#define WATERMARK1080p_TIME_H       WATERMARK_TIME_FONTY * 2
#define WATERMARK1080p_LICENSE_W    WATERMARK_LICENSE_FONTX * 2
#define WATERMARK1080p_LICENSE_H    WATERMARK_LICENSE_FONTY * 2

/* 1280x720 */
#define WATERMARK720p_TIME_W        WATERMARK_TIME_FONTX * 3 / 2
#define WATERMARK720p_TIME_H        WATERMARK_TIME_FONTY * 3 / 2
#define WATERMARK720p_LICENSE_W     WATERMARK_LICENSE_FONTX * 3 / 2
#define WATERMARK720p_LICENSE_H     WATERMARK_LICENSE_FONTY * 3 / 2

/* 640x480 */
#define WATERMARK480p_TIME_W        WATERMARK_TIME_FONTX
#define WATERMARK480p_TIME_H        WATERMARK_TIME_FONTY
#define WATERMARK480p_LICENSE_W     WATERMARK_LICENSE_FONTX
#define WATERMARK480p_LICENSE_H     WATERMARK_LICENSE_FONTY

/* 480x320 */
#define WATERMARK360p_TIME_W        WATERMARK_TIME_FONTX
#define WATERMARK360p_TIME_H        WATERMARK_TIME_FONTY
#define WATERMARK360p_LICENSE_W     WATERMARK_LICENSE_FONTX
#define WATERMARK360p_LICENSE_H     WATERMARK_LICENSE_FONTY

/* 320x240 */
#define WATERMARK240p_TIME_W        WATERMARK_TIME_FONTX
#define WATERMARK240p_TIME_H        WATERMARK_TIME_FONTY
#define WATERMARK240p_LICENSE_W     WATERMARK_LICENSE_FONTX
#define WATERMARK240p_LICENSE_H     WATERMARK_LICENSE_FONTY

#endif