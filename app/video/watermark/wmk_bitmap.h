/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef WATER_MARK_BITMAP_H
#define WATER_MARK_BITMAP_H

#include <stdint.h>
#include "wmk_endianrw.h"
#include "wmk_readbmp.h"

/* rgb565 start */
#define RGB_FORMAT_RLOSS    3
#define RGB_FORMAT_GLOSS    2
#define RGB_FORMAT_BLOSS    3
#define RGB_FORMAT_ALOSS    8

#define RGB_FORMAT_RSHIFT   11
#define RGB_FORMAT_GSHIFT   5
#define RGB_FORMAT_BSHIFT   0
#define RGB_FORMAT_ASHIFT   0

#define RGB_FORMAT_RMASK    0xf800
#define RGB_FORMAT_GMASK    0x07e0
#define RGB_FORMAT_BMASK    0x001f
#define RGB_FORMAT_AMASK    0
/* rgb565 end */

/* Transparency definitions: These define alpha as the opacity of a surface */
#define WMK_ALPHA_OPAQUE        255
#define WMK_ALPHA_TRANSPARENT   0

#define wmk_fp_getc(fp)     rw_getc(fp)
#define wmk_fp_igetw(fp)    read_le16(fp)
#define wmk_fp_igetl(fp)    read_le32(fp)
#define wmk_fp_mgetw(fp)    read_be16(fp)
#define wmk_fp_mgetl(fp)    read_be32(fp)

void *init_bmp(struct mg_rw_ops *fp, struct mybitmap *bmp, struct rgb_t *pal);
void cleanup_bmp(void *init_info);
int load_bmp(struct mg_rw_ops *fp, void *init_info, struct mybitmap *bmp,
	     _cb_one_scanline cb, void *context);
BOOL check_bmp(struct mg_rw_ops *fp);
#endif
