/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef REFLECTOR_H
#define REFLECTOR_H

#include <map>
#include <memory>
#include <string>

#define DECLARE_REFLECTOR(PRODUCT, FACTORY, REFLECTOR)              \
  class PRODUCT;                                                    \
  class FACTORY;                                                    \
  class REFLECTOR {                                                 \
   public:                                                          \
    static REFLECTOR& Instance();                                   \
    std::shared_ptr<PRODUCT> Create(const char* request);           \
    void RegisterFactory(std::string identifier, FACTORY* factory); \
                                                                    \
   private:                                                         \
    REFLECTOR() = default;                                          \
    ~REFLECTOR() = default;                                         \
    REFLECTOR(const REFLECTOR&) = delete;                           \
    REFLECTOR& operator=(const REFLECTOR&) = delete;                \
                                                                    \
    std::map<std::string, FACTORY*> factorys_;                      \
  };

#define DEFINE_REFLECTOR(PRODUCT, FACTORY, REFLECTOR)                         \
  REFLECTOR& REFLECTOR::Instance() {                                          \
    static REFLECTOR _reflector;                                              \
    return _reflector;                                                        \
  }                                                                           \
                                                                              \
  void REFLECTOR::RegisterFactory(std::string identifier, FACTORY* factory) { \
    auto it = factorys_.find(identifier);                                     \
    if (it == factorys_.end())                                                \
      factorys_[identifier] = factory;                                        \
    else                                                                      \
      fprintf(stderr, "repeated identifier : %s\n", identifier.c_str());      \
  }                                                                           \
                                                                              \
  std::shared_ptr<PRODUCT> REFLECTOR::Create(const char* request) {           \
    const char* identifier = FACTORY::Parse(request);                         \
    if (!identifier)                                                          \
      return nullptr;                                                         \
                                                                              \
    auto it = factorys_.find(identifier);                                     \
    if (it != factorys_.end()) {                                              \
      FACTORY* f = it->second;                                                \
      return f->NewProduct();                                                 \
    }                                                                         \
    return nullptr;                                                           \
  }

#define DECLARE_FACTORY(PRODUCT, FACTORY)              \
  class PRODUCT;                                       \
  class FACTORY {                                      \
   public:                                             \
    virtual const char* Identifier() = 0;              \
    static const char* Parse(const char* request);     \
    virtual std::shared_ptr<PRODUCT> NewProduct() = 0; \
                                                       \
   protected:                                          \
    FACTORY() = default;                               \
    virtual ~FACTORY() = default;                      \
                                                       \
   private:                                            \
    FACTORY(const FACTORY&) = delete;                  \
    FACTORY& operator=(const FACTORY&) = delete;       \
  };

#define FACTORY_IDENTIFIER_DEFINITION(IDENTIFIER) \
  const char* Identifier() override { return IDENTIFIER; }

#define FACTORY_INSTANCE_DEFINITION(FACTORY) \
  static FACTORY& Instance() {               \
    static FACTORY object;                   \
    return object;                           \
  }

#define FACTORY_REGISTER(FACTORY, REFLECTOR)                         \
  class Register_##FACTORY {                                         \
   public:                                                           \
    Register_##FACTORY() {                                           \
      FACTORY& obj = FACTORY::Instance();                            \
      REFLECTOR::Instance().RegisterFactory(obj.Identifier(), &obj); \
    }                                                                \
  };                                                                 \
  Register_##FACTORY reg_##FACTORY;

#endif  // REFLECTOR_H
