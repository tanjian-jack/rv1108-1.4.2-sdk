/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MOTION_DETECT_HANDLER_H
#define MOTION_DETECT_HANDLER_H

#include "scale_encode_handler.h"
#include "motion_detection/mv_dispatcher.h"
#include "motion_detection/md_processor.h"
#include "encoder_muxing/encoder/mpp_h264_encoder.h"

class MotionDetectHandler : public ScaleEncodeHandler<MPPH264Encoder> {
 public:
  // Make these user configurable ?
  static const int kMDWidth = 320;
  MotionDetectHandler();
  virtual ~MotionDetectHandler();
  int Init(MediaConfig config);
  virtual void DeInit() override;
  void Process(int src_fd, const VideoConfig& src_config);
  int AddMdProcessor(VPUMDProcessor* processor, MDAttributes* attr);
  int RemoveMdProcessor(VPUMDProcessor* processor);
  inline bool IsRunning() { return pid_ != 0; }

 protected:
  virtual void GetSrcConfig(const MediaConfig& src_config,
                            int& src_w,
                            int& src_h,
                            PixelFormat& src_fmt) final;
  virtual int PrepareBuffers(MediaConfig& src_config,
                             const int dst_numerator,
                             const int dst_denominator,
                             int& dst_w,
                             int& dst_h) override;
  virtual void Work() final;

 private:
  MVDispatcher mv_dispatcher_;
  int md_processor_num_;
  struct video_ion mv_buf_;
  BufferData mv_data_;
};

#endif  // MOTION_DETECT_HANDLER_H
