/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SCALEENCODETSHANDLER_H
#define SCALEENCODETSHANDLER_H

#include <memory>

#include "scale_encode_handler.h"
#include "encoder_muxing/packet_dispatcher.h"
#include "encoder_muxing/encoder/mpp_h264_encoder.h"
#include "encoder_muxing/encoder/ffmpeg_wrapper/ff_context.h"

#define SCALED_WIDTH 640
#define SCALED_HEIGHT 384
#define SCALED_BIT_RATE 800000

class FFAudioEncoder;
class CameraTsMuxer;
class ScaleEncodeTSHandler : public ScaleEncodeHandler<MPPH264Encoder>
{
public:
    ScaleEncodeTSHandler(MediaConfig& config);
    virtual ~ScaleEncodeTSHandler();
    int Init(MediaConfig src_config);
    virtual void DeInit() override;
    void Process(int src_fd, const VideoConfig& src_config, const timeval& time,
                 int src_w_align, int src_h_align);
    int StartTransferStream(char* uri,
                            pthread_attr_t* global_attr,
                            FFAudioEncoder* audio_enc = nullptr);
    int StopTransferStream();
    bool Valid() { return valid_; }
    int GetCommuFd() { return commu_fd_; }
    CameraTsMuxer* GetTsMuxer() { return ts_.get(); }
    MPPH264Encoder* GetVideoEncoder() { return encoder_; }
    PacketDispatcher* GetPacketDispatcher() { return &pkt_dispatcher_; }

protected:
    virtual void GetSrcConfig(const MediaConfig& src_config,
                              int& src_w,
                              int& src_h,
                              PixelFormat& src_fmt) final;
    virtual int PrepareBuffers(MediaConfig& src_config,
                               const int dst_numerator,
                               const int dst_denominator,
                               int& dst_w,
                               int& dst_h) override;
    virtual void Work() final;

private:
    PacketDispatcher pkt_dispatcher_;
    std::shared_ptr<CameraTsMuxer> ts_;
    MediaConfig scale_config_;  // bit_rate, gop, width, height
    struct timeval time_val_;
    FFContext ff_ctx_;  // ffmpeg muxer help context

    // For pipe rtsp.
    static const char* kCommuPath;
    int commu_fd_;
    pthread_t commu_tid_;
    bool valid_;
};

#endif  // SCALEENCODETSHANDLER_H
