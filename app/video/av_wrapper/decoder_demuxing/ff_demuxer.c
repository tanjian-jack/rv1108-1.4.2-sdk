/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ff_demuxer.h"

#include <libavformat/avformat.h>

int get_file_duration(char* file_path, int64_t* duration) {
  AVFormatContext* fmt_ctx = NULL;
  av_register_all();
  *duration = -1;
  if (avformat_open_input(&fmt_ctx, file_path, NULL, NULL) < 0) {
    fprintf(stderr, "Could not open source file %s\n", file_path);
    return -1;
  }
  if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
    fprintf(stderr, "Could not find stream information\n");
    avformat_close_input(&fmt_ctx);
    return -1;
  }
  if (fmt_ctx->duration != AV_NOPTS_VALUE) {
#ifdef DEBUG
    int hours, mins, secs, us;
    int64_t time_len = fmt_ctx->duration + 5000;
    secs = time_len / AV_TIME_BASE;
    us = time_len % AV_TIME_BASE;
    mins = secs / 60;
    secs %= 60;
    hours = mins / 60;
    mins %= 60;
    fprintf(stderr, "%s time length: %02d:%02d:%02d.%02d\n", file_path, hours,
            mins, secs, (100 * us) / AV_TIME_BASE);
#endif
    *duration = fmt_ctx->duration + 5000;
  }
  avformat_close_input(&fmt_ctx);
  return 0;
}
