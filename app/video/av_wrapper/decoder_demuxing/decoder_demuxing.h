/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef DECODER_DEMUXING_H
#define DECODER_DEMUXING_H

#include "../video_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <cvr_ffmpeg_shared.h>
#include <libavformat/avformat.h>

int decode_init_context(void** ctx, char* filepath);
void decode_deinit_context(void** ctx);
int decode_one_video_frame(void* ctx,
                           image_handle image_buf,
                           int* width,
                           int* height,
                           int* coded_width,
                           int* coded_height,
                           enum AVPixelFormat* pix_fmt);
int64_t decode_get_duration(void* ctx);
#ifdef __cplusplus
}
#endif

class DecoderDemuxer {
 private:
  AVFormatContext* fmt_ctx;
  media_info info;

  int video_stream_idx;
  AVStream* video_stream;
  AVCodecContext* video_dec_ctx;
  int video_frame_count;

  int audio_stream_idx;
  AVStream* audio_stream;
  AVCodecContext* audio_dec_ctx;
  int audio_frame_count;

#define AUDIO_DISABLE_FLAG (1 << 0)
#define VIDEO_DISABLE_FLAG (1 << 1)
  uint32_t disable_flag;
  bool finished;
  int decode_packet(AVPacket* pkt, AVFrame* frame, int* got_frame);

 public:
  DecoderDemuxer();
  ~DecoderDemuxer();
  int init(char* src_filepath, uint32_t disable_flag);
  int read_and_decode_one_frame(image_handle image_buf,  // in&out
                                AVFrame* out_frame,
                                int* got_frame,  // out
                                int* isVideo);
  media_info& get_media_info() { return info; }
  int rewind();  // rewind read
  bool is_finished() { return finished; }
  int get_video_coded_width();
  int get_video_coded_height();
  void set_audio_disable() { disable_flag |= AUDIO_DISABLE_FLAG; }
  int64_t get_duration();
};

#endif
