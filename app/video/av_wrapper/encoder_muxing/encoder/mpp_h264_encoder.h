/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MPP_H264_ENCODER_H
#define MPP_H264_ENCODER_H

#include "base_encoder.h"
#include "mpp_inc.h"
#include <mpp/rk_mpi.h>

// A encoder which call the mpp interface directly.
// Not thread-safe.
class MPPH264Encoder : public BaseVideoEncoder {
 public:
#ifndef MPP_PACKET_FLAG_INTRA
#define MPP_PACKET_FLAG_INTRA (0x00000008)
#endif
  typedef struct {
    MppCodingType video_type;
    MppCtx ctx;
    MppApi* mpi;
    MppPacket packet;
    MppFrame frame;
    MppBuffer osd_data;
  } RkMppEncContext;

  MPPH264Encoder();
  ~MPPH264Encoder();

  virtual int InitConfig(MediaConfig& config);
  // Change configs which are not contained in sps/pps.
  int CheckConfigChange();
  // Encode the raw srcbuf to dstbuf
  virtual int EncodeOneFrame(Buffer* src, Buffer* dst, Buffer* mv);
  // Control before encoding.
  int EncodeControl(int cmd, void* param);
  inline void GetExtraData(void*& extra_data, size_t& extra_data_size) {
    extra_data = extra_data_;
    extra_data_size = extra_data_size_;
  }

 private:
  RkMppEncContext mpp_enc_ctx_;
  void* extra_data_;
  size_t extra_data_size_;
};

#endif  // MPP_H264_ENCODER_H
