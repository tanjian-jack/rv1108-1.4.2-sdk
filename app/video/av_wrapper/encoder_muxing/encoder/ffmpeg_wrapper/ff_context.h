/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ENCODER_CODECS_FF_CONTEXT_H_
#define ENCODER_CODECS_FF_CONTEXT_H_

extern "C" {
#include <libavformat/avformat.h>
}

#include "encoder_muxing/encoder/base_encoder.h"

class FFContext {
 public:
  FFContext();
  virtual ~FFContext();
  int InitConfig(VideoConfig& vconfig, bool ff_open_codec = true);
  int InitConfig(AudioConfig& aconfig);
  inline AVFormatContext* GetAVFmtContext() { return av_fmt_ctx_; }
  inline AVCodec* GetAVCodec() { return av_codec_; }
  inline AVStream* GetAVStream() { return av_stream_; }
  inline AVCodecContext* GetAVCodecContext() {
    return av_stream_ ? av_stream_->codec : nullptr;
  }
  inline void SetCodecName(char* codec_name) { codec_name_ = codec_name; }
  inline void SetCodecId(AVCodecID codec_id) { codec_id_ = codec_id; }

  static enum AVPixelFormat ConvertToAVPixFmt(const PixelFormat& fmt);
  static enum AVSampleFormat ConvertToAVSampleFmt(const SampleFormat& fmt);

  // Pack the encoded data to avpacket,
  // default do not copy content to avpacket.
  static int PackEncodedDataToAVPacket(const Buffer& buf,
                                       AVPacket& out_pkt,
                                       const bool copy = false);

 private:
  AVFormatContext* av_fmt_ctx_;
  AVCodec* av_codec_;
  AVStream* av_stream_;
  AVCodecID codec_id_;
  char* codec_name_;

  char* GetCodecName();
  int Init(bool ff_open_codec);
};

#endif  // ENCODER_CODECS_FF_CONTEXT_H_
