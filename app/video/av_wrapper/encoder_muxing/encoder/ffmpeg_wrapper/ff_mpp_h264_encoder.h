/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef FF_MPP_H264_ENCODER_H
#define FF_MPP_H264_ENCODER_H

#include "ff_base_encoder.h"

extern "C" {
#include <cvr_ffmpeg_shared.h>
}

#include <mpp/rk_mpi.h>

// The share data struct between ffmpeg and mpp.
typedef DataBuffer_t FFMppShareBuffer;
typedef FFMppShareBuffer* PFFMppShareBuffer;

// A encoder which call the ffmpeg interface,
// finally towards to mpp interface.
class FFMPPH264Encoder : public BaseVideoEncoder, public FFBaseEncoder {
 public:
  FFMPPH264Encoder();
  virtual ~FFMPPH264Encoder();
  // Must be called before encoding.
  virtual int InitConfig(MediaConfig& config) override;
  // Encode the raw srcbuf to dstbuf.
  virtual int EncodeOneFrame(Buffer* src, Buffer* dst, Buffer* mv) override;
  // Control before encoding.
  int EncodeControl(int cmd, void* param);
  virtual void* GetHelpContext() override;

 private:
  FFContext ff_ctx_;
  AVFrame* video_frame_;
  // Return the encoded pkt size.
  int EncodeOneFrame(PFFMppShareBuffer src_buf,
                     PFFMppShareBuffer dst_buf,
                     uint32_t& dst_flag,
                     PFFMppShareBuffer mv_buf);
};

#endif  // FF_MPP_H264_ENCODER_H
