/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef FF_AUDIO_ENCODER_H
#define FF_AUDIO_ENCODER_H

#include "ff_base_encoder.h"

class FFAudioEncoder : public BaseEncoder, public FFBaseEncoder {
 public:
  FFAudioEncoder();
  virtual ~FFAudioEncoder();
  virtual int InitConfig(MediaConfig& config) override;
  void GetAudioBuffer(void** buf,
                      int* nb_samples,
                      int* channels,
                      enum AVSampleFormat* format);
  int EncodeAudioFrame(AVPacket* out_pkt);
  // return if there is no remaining audio data
  void EncodeFlushAudioData(AVPacket* out_pkt, bool& finish);
  virtual void* GetHelpContext() override;

 private:
  AVFrame* audio_frame_;
  AVFrame* audio_in_frame_;
  int samples_count_;
  struct SwrContext* swr_ctx_;
};

#endif  // FF_AUDIO_ENCODER_H
