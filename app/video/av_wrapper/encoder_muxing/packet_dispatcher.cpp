/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>

#include "muxing/camera_muxer.h"
#include "packet_dispatcher.h"

static void _Dispatch(std::mutex& list_mutex,
                      EncodedPacket* pkt,
                      std::list<CameraMuxer*>* muxers,
                      std::list<CameraMuxer*>* except_muxers) {
  assert(muxers);
  std::lock_guard<std::mutex> lk(list_mutex);
  if (pkt->is_phy_buf && !pkt->release_cb) {
    bool sync = true;
    for (CameraMuxer* muxer : *muxers) {
      if (except_muxers &&
          std::find(except_muxers->begin(), except_muxers->end(), muxer) !=
              except_muxers->end())
        continue;
      if (muxer->use_data_async()) {
        sync = false;
        break;
      }
    }
    if (!sync)
      pkt->copy_av_packet();  // need copy buffer
  }

  for (CameraMuxer* muxer : *muxers) {
    if (except_muxers &&
        std::find(except_muxers->begin(), except_muxers->end(), muxer) !=
            except_muxers->end())
      continue;
    muxer->push_packet(pkt);
  }
}

void PacketDispatcher::Dispatch(EncodedPacket* pkt) {
  _Dispatch(list_mutex, pkt, &handlers, &special_muxers_);
}

void PacketDispatcher::DispatchToSpecial(EncodedPacket* pkt) {
  _Dispatch(list_mutex, pkt, &special_muxers_, nullptr);
}

void PacketDispatcher::AddSpecialMuxer(CameraMuxer* muxer) {
  std::lock_guard<std::mutex> lk(list_mutex);
  special_muxers_.push_back(muxer);
}
void PacketDispatcher::RemoveSpecialMuxer(CameraMuxer* muxer) {
  std::lock_guard<std::mutex> lk(list_mutex);
  special_muxers_.remove(muxer);
}
