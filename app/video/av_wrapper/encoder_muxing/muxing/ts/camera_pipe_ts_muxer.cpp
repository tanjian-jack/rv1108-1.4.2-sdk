/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "camera_pipe_ts_muxer.h"

#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

#include "encoder_muxing/encoder/base_encoder.h"

DEFINITION_CAMERATSALLOC(CameraPipeTSAlloc, "pipe", CameraPipeTsMuxer)

static inline ssize_t pipe_write(int fd, void* buf, size_t buf_size) {
  size_t remain_size = buf_size;
  do {
    ssize_t ret = write(fd, buf, remain_size);
    if (ret != static_cast<ssize_t>(remain_size) && errno != EAGAIN) {
      printf(
          "write pipe failed, expectly write %u bytes, really write %u "
          "bytes\n",
          buf_size, buf_size - remain_size);
      return -1;
    }
    assert(ret >= 0);
    remain_size -= ret;
  } while (remain_size > 0);
  return buf_size;
}

CameraPipeTsMuxer::CameraPipeTsMuxer() : ext_video_fd_(-1), ext_audio_fd_(-1) {
#ifdef DEBUG
  snprintf(class_name, sizeof(class_name), "CameraPipeTsMuxer");
#endif
}

CameraPipeTsMuxer::~CameraPipeTsMuxer() {
  ClosePipe(ext_video_fd_);
  ClosePipe(ext_audio_fd_);
}

int CameraPipeTsMuxer::init_uri(char* uri, int rate) {
  char* pipe_path = uri + sizeof("pipe://") - 1;  // 7
  if (OpenLivePipe(pipe_path) < 0)
    return -1;
  sprintf(uri, "pipe:%d&%d", ext_video_fd_, ext_audio_fd_);
  snprintf(format, sizeof(format), "h264");
  state_ = ENTER;

  return CameraTsMuxer::init_uri(uri, rate);
}

int CameraPipeTsMuxer::OpenLivePipe(const char* pipe_path) {
  ext_video_fd_ = OpenPipe(pipe_path);
  ext_audio_fd_ = OpenPipe(AUDIO_FIFO_NAME);
  if (ext_video_fd_ >= 0 && ext_audio_fd_ >= 0) {
    return 0;
  } else {
    state_ = EXIT;
    ClosePipe(ext_video_fd_);
    ClosePipe(ext_audio_fd_);
    return -1;
  }
}

int CameraPipeTsMuxer::PipeWriteSpsPps(const uint8_t* spspps,
                                       size_t spspps_size,
                                       const struct timeval& time,
                                       const int start_len /* 00 00 01 */) {
  assert(spspps_size > 0);
  if (!spspps || spspps_size <= 0)
    return -1;
  const uint8_t* p = spspps;
  const uint8_t* end = p + spspps_size;
  const uint8_t *nal_start = nullptr, *nal_end = nullptr;
  nal_start = find_h264_startcode(p, end);
  for (;;) {
    // while (nal_start < end && !*(nal_start++));
    if (nal_start == end)
      break;
    nal_start += start_len;
    nal_end = find_h264_startcode(nal_start, end);
    unsigned size = nal_end - nal_start + start_len;
    uint8_t nal_type = (*nal_start) & 0x1F;
    if (nal_type == 7 || nal_type == 8) {
      if (pipe_write(ext_video_fd_, (void*)&time, sizeof(time)) < 0)
        return -1;
      if (pipe_write(ext_video_fd_, &size, sizeof(size)) < 0)
        return -1;
      if (pipe_write(ext_video_fd_, (void*)(nal_start - start_len), size) < 0)
        return -1;
    }
    nal_start = nal_end;
  }
  return 0;
}

int CameraPipeTsMuxer::muxer_write_header(AVFormatContext* oc, char* url) {
  // Do the writting by ourself.
  for (uint32_t i = 0; i < oc->nb_streams; i++) {
    AVStream* st = oc->streams[i];
    AVCodecContext* codec = st ? st->codec : nullptr;
    if (codec && codec->codec_id == AV_CODEC_ID_H264) {
      struct timeval time = {0, 0};
      gettimeofday(&time, nullptr);
      if (!PipeWriteSpsPps(codec->extradata, codec->extradata_size, time, 3))
        return 0;
    }
  }
  fprintf(stderr, "Error: could not get the sps pps info\n");
  return -1;
}

int CameraPipeTsMuxer::muxer_write_tailer(AVFormatContext* oc) {
  return 0;  // Do nothing.
}

int CameraPipeTsMuxer::muxer_write_free_packet(MuxProcessor* process,
                                               EncodedPacket* pkt) {
  int fd = -1;
  struct timeval time_val = pkt->time_val;
  if (pkt->type == VIDEO_PACKET) {
    fd = ext_video_fd_;
  } else if (pkt->type == AUDIO_PACKET) {
    fd = ext_audio_fd_;
    gettimeofday(&time_val, nullptr);
  }

  if (fd >= 0) {
    if (pipe_write(fd, &time_val, sizeof(time_val)) < 0)
      return -1;
    unsigned size = pkt->av_pkt.size;
    if (pipe_write(fd, &size, sizeof(size)) < 0)
      return -1;
    if (pipe_write(fd, pkt->av_pkt.data, size) < 0)
      return -1;
  }

  pkt->unref();

  return 0;
}

int OpenPipe(const char* pipe_path) {
  int fd = -1;
  if (pipe_path) {
    // If do not exist, create it.
    if (access(pipe_path, F_OK) == -1 && mkfifo(pipe_path, 0777) != 0) {
      printf("mkfifo %s failed, errno: %d\n", pipe_path, errno);
      return -1;
    }
    fd = open(pipe_path, O_RDWR);
    if (fd < 0)
      printf("open %s fail, errno: %d\n", pipe_path, errno);
  }
  return fd;
}

void ClosePipe(int& fd) {
  if (fd >= 0 && close(fd))
    fprintf(stderr, "close fd <%d> failed, errno: %d\n", fd, errno);
  fd = -1;
}
