/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAMERA_TS_H
#define CAMERA_TS_H

#include "encoder_muxing/muxing/camera_muxer.h"
#include "reflector.h"

DECLARE_REFLECTOR(CameraTsMuxer, CameraTsAlloc, CameraTs)
DECLARE_FACTORY(CameraTsMuxer, CameraTsAlloc)

#define DEFINITION_CAMERATSALLOC(CAMERATSALLOC, IDENTIFIER, CAMERATSMUXER) \
  class CAMERATSALLOC : public CameraTsAlloc {                             \
   public:                                                                 \
    FACTORY_IDENTIFIER_DEFINITION(IDENTIFIER)                              \
    std::shared_ptr<CameraTsMuxer> NewProduct() override {                 \
      return std::shared_ptr<CAMERATSMUXER>(CAMERATSMUXER::create());      \
    }                                                                      \
    FACTORY_INSTANCE_DEFINITION(CAMERATSALLOC)                             \
                                                                           \
   private:                                                                \
    CAMERATSALLOC() = default;                                             \
    ~CAMERATSALLOC() = default;                                            \
  };                                                                       \
                                                                           \
  FACTORY_REGISTER(CAMERATSALLOC, CameraTs)

class CameraTsMuxer : public CameraMuxer {
 public:
  bool Running() { return state_ == RUNNING; }
  void SetRunning() { state_ = RUNNING; }
  void SetPausing() { state_ = PAUSING; }
  virtual bool StartByOutside() { return false; }
  int Error() { return fatal_error_; }
  virtual int init_uri(char* uri, int rate);
  virtual void stop_current_job();
  virtual int muxer_write_free_packet(MuxProcessor* process,
                                      EncodedPacket* pkt);

 protected:
  enum { ENTER, RUNNING, PAUSING, EXIT };
  volatile int state_;
  int fatal_error_;
  CameraTsMuxer() : state_(RUNNING), fatal_error_(0) {
    exit_id = MUXER_IMMEDIATE_EXIT;
  }
  virtual ~CameraTsMuxer() { state_ = EXIT; }
};

#endif  // CAMERA_TS_H
