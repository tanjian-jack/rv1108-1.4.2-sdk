/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "camera_ts.h"

#include <stdio.h>
#include <string.h>

DEFINE_REFLECTOR(CameraTsMuxer, CameraTsAlloc, CameraTs)

const char* CameraTsAlloc::Parse(const char* request) {
  // extra path without ffmpeg
  static const char* protocols[] = {"pipe", "tutk"};
  if (!request)
    return nullptr;

  for (size_t i = 0; i < sizeof(protocols) / sizeof(protocols[0]); i++) {
    const char* p = protocols[i];
    if (!strncmp(request, p, strlen(p)))
      return p;
  }

  return "ff";  // default ffmpeg's muxer
}

int CameraTsMuxer::init_uri(char* uri, int rate) {
  int ret = CameraMuxer::init_uri(uri, rate);
  max_video_num = rate;  // 1 seconds
  return ret;
}
void CameraTsMuxer::stop_current_job() {
  CameraMuxer::stop_current_job();
  free_packet_list();
}
int CameraTsMuxer::muxer_write_free_packet(MuxProcessor* process,
                                           EncodedPacket* pkt) {
  int ret = CameraMuxer::muxer_write_free_packet(process, pkt);
  if (ret != 0)
    fatal_error_ = ret;
  return ret;
}
