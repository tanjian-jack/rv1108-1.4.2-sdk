/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "camera_ff_ts_muxer.h"

#include <stdio.h>

DEFINITION_CAMERATSALLOC(CameraFFTSAlloc, "ff", CameraFFTsMuxer)

CameraFFTsMuxer::CameraFFTsMuxer() {
#ifdef DEBUG
  snprintf(class_name, sizeof(class_name), "CameraFFTsMuxer");
#endif
}

int CameraFFTsMuxer::init_uri(char* uri, int rate) {
  // av_log_set_level(AV_LOG_TRACE);
  if (!strncmp(uri, "rtsp", 4))
    snprintf(format, sizeof(format), "rtsp");
  else if (!strncmp(uri, "rtp", 3))
    snprintf(format, sizeof(format), "rtp_mpegts");
  else if (!strncmp(uri, "rtmp", 4))
    snprintf(format, sizeof(format), "flv");
  else
    fprintf(stderr, "warning, unrecognized uri: %s!\n", uri);

  return CameraTsMuxer::init_uri(uri, rate);
}
