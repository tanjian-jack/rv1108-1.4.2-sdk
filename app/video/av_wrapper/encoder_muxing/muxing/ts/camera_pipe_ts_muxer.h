/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAMERA_PIPE_TS_MUXER_H
#define CAMERA_PIPE_TS_MUXER_H

#include "camera_ts.h"

#define AUDIO_FIFO_NAME "/tmp/rv110x_audio_fifo"

class CameraPipeTsMuxer : public CameraTsMuxer {
 public:
  ~CameraPipeTsMuxer();
  int init_uri(char* uri, int rate) override;
  int muxer_write_header(AVFormatContext* oc, char* url) override;
  int muxer_write_tailer(AVFormatContext* oc) override;
  int muxer_write_free_packet(MuxProcessor* process,
                              EncodedPacket* pkt) override;
  bool StartByOutside() override { return true; }
  CREATE_FUNC(CameraPipeTsMuxer)

 private:
  CameraPipeTsMuxer();
  int ext_video_fd_;
  int ext_audio_fd_;

  int OpenLivePipe(const char* pipe_path);
  int PipeWriteSpsPps(const uint8_t* spspps,
                      size_t spspps_size,
                      const timeval& time,
                      const int start_len);
};

int OpenPipe(const char* pipe_path);
void ClosePipe(int& fd);

#endif  // CAMERA_PIPE_TS_MUXER_H
