/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAMERA_FILE_MUXER_H
#define CAMERA_FILE_MUXER_H

#include "../camera_muxer.h"

// For file storage.
class CameraFileMuxer : public CameraMuxer {
 private:
  CameraFileMuxer();

#define MODE_NORMAL (0 << 0)
#define MODE_ONLY_KEY_FRAME (1 << 0)
#define MODE_NORMAL_FIX_TIME (1 << 1)
#define MODE_NORMAL_RELATIVE (1 << 2)

  uint32_t low_frame_rate_mode;

  struct timeval lbr_pre_timeval;
  struct timeval real_pre_timeval;  // actual time

 public:
  ~CameraFileMuxer();
  bool is_low_frame_rate();
  bool set_low_frame_rate(uint32_t val, const struct timeval& start_timeval);
  void reset_lbr_time();
  void set_real_time(const struct timeval& start_timeval);
  void push_packet(EncodedPacket* pkt);
  CREATE_FUNC(CameraFileMuxer)
  bool init_subtitle_stream(AVStream *stream);
};

#endif  // CAMERA_FILE_MUXER_H
