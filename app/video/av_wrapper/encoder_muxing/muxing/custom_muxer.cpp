/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "custom_muxer.h"
#include "encoder_muxing/encoder/ffmpeg_wrapper/ff_context.h"

CustomMuxer::CustomMuxer() {
#ifdef DEBUG
  snprintf(class_name, sizeof(class_name), "custom_muxer");
#endif
  exit_id = MUXER_NORMAL_EXIT;
  no_async = true;  // no cache packet list and no thread handle
  data_cb = NULL;
}

int CustomMuxer::init_muxer_processor(MuxProcessor* process) {
  UNUSED(process);
  return 0;
}

void CustomMuxer::deinit_muxer_processor(AVFormatContext* oc) {
  UNUSED(oc);
}

int CustomMuxer::muxer_write_header(AVFormatContext* oc, char* url) {
  UNUSED(oc);
  UNUSED(url);
  return 0;
}

int CustomMuxer::muxer_write_tailer(AVFormatContext* oc) {
  UNUSED(oc);
  return 0;
}

int CustomMuxer::muxer_write_free_packet(MuxProcessor* process,
                                         EncodedPacket* pkt) {
  if (data_cb && pkt->type == VIDEO_PACKET) {
    BaseEncoder * venc = get_video_encoder();
    assert(venc);
    FFContext* ff_ctx = static_cast<FFContext*>(venc->GetHelpContext());
    AVCodecContext* codec_ctx = ff_ctx->GetAVStream()->codec;
    assert(codec_ctx);
    AVPacket* avpkt = &pkt->av_pkt;
    VEncStreamInfo info = {
        .frm_type = (avpkt->flags & AV_PKT_FLAG_KEY) ? I_FRAME : P_FRAME,
        .buf_addr = avpkt->data,
        .buf_size = avpkt->size,
        .time_val = pkt->time_val,
        .ExtraInfo = {
            .sps_pps_info = {.data = codec_ctx->extradata,
                             .data_size = codec_ctx->extradata_size}}};
    data_cb(&info);
  }
  pkt->unref();
  return 0;
}
