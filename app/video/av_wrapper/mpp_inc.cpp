/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mpp_inc.h"

MppFrameFormat ConvertToMppPixFmt(const PixelFormat& fmt) {
  // static const MppFrameFormat invalid_mpp_fmt = static_cast<MppFrameFormat>(-1);
  static_assert(PIX_FMT_YUV420P == 0, "The index should greater than 0\n");
  static MppFrameFormat mpp_fmts[PIX_FMT_NB] =
      {[PIX_FMT_YUV420P] = MPP_FMT_YUV420P,
       [PIX_FMT_NV12] = MPP_FMT_YUV420SP,
       [PIX_FMT_NV21] = MPP_FMT_YUV420SP_VU,
       [PIX_FMT_YUV422P] = MPP_FMT_YUV422P,
       [PIX_FMT_NV16] = MPP_FMT_YUV422SP,
       [PIX_FMT_NV61] = MPP_FMT_YUV422SP_VU,
       [PIX_FMT_YVYU422] = MPP_FMT_YUV422_YUYV,
       [PIX_FMT_UYVY422] = MPP_FMT_YUV422_UYVY,
       [PIX_FMT_RGB565LE] = MPP_FMT_RGB565,
       [PIX_FMT_BGR565LE] = MPP_FMT_BGR565,
       [PIX_FMT_RGB24] = MPP_FMT_RGB888,
       [PIX_FMT_BGR24] = MPP_FMT_BGR888,
       [PIX_FMT_RGB32] = MPP_FMT_ARGB8888,
       [PIX_FMT_BGR32] = MPP_FMT_ABGR8888};
  assert(fmt >= 0 && fmt < PIX_FMT_NB);
  return mpp_fmts[fmt];
}

PixelFormat ConvertToPixFmt(const MppFrameFormat& mfmt) {
  switch (mfmt) {
    case MPP_FMT_YUV420P:
      return PIX_FMT_YUV420P;
    case MPP_FMT_YUV420SP:
      return PIX_FMT_NV12;
    case MPP_FMT_YUV420SP_VU:
      return PIX_FMT_NV21;
    case MPP_FMT_YUV422P:
      return PIX_FMT_YUV422P;
    case MPP_FMT_YUV422SP:
      return PIX_FMT_NV16;
    case MPP_FMT_YUV422SP_VU:
      return PIX_FMT_NV61;
    case MPP_FMT_YUV422_YUYV:
      return PIX_FMT_YVYU422;
    case MPP_FMT_YUV422_UYVY:
      return PIX_FMT_UYVY422;
    case MPP_FMT_RGB565:
      return PIX_FMT_RGB565LE;
    case MPP_FMT_BGR565:
      return PIX_FMT_BGR565LE;
    case MPP_FMT_RGB888:
      return PIX_FMT_RGB24;
    case MPP_FMT_BGR888:
      return PIX_FMT_BGR24;
    case MPP_FMT_ARGB8888:
      return PIX_FMT_RGB32;
    case MPP_FMT_ABGR8888:
      return PIX_FMT_BGR32;
    default:
      printf("unsupport for pixel fmt: %d\n", mfmt);
      return PIX_FMT_NONE;
  }
}
