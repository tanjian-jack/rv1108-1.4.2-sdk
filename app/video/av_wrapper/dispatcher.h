/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <functional>

#include "video_common.h"

template <class T, typename DATA>
class Dispatcher  // DataDispatcher
{
 private:
  void CallBackIfEmpty() {
    if (handlers.empty()) {
      for (std::function<void(void)>& func : cb_when_empty)
        func();
    }
  }

 protected:
  std::mutex list_mutex;
  std::list<T*> handlers;
  std::list<std::function<void(void)>> cb_when_empty;

 public:
  Dispatcher() {}
  virtual ~Dispatcher() { assert(handlers.size() == 0); }
  void AddHandler(T* handler) {
    std::lock_guard<std::mutex> lk(list_mutex);
    UNUSED(lk);
    handlers.push_back(handler);
  }
  void RemoveHandler(T* handler) {
    std::lock_guard<std::mutex> lk(list_mutex);
    UNUSED(lk);
    handlers.remove(handler);
    CallBackIfEmpty();
  }
  // return if remove successfully
  void RemoveHandler(T* handler, bool& exist) {
    std::lock_guard<std::mutex> lk(list_mutex);
    exist = false;
    for (T* element : handlers) {
      if (element == handler) {
        handlers.remove(handler);
        CallBackIfEmpty();
        exist = true;
        return;
      }
    }
  }
  virtual void Dispatch(DATA* pkt) {}
  bool Empty() {
    std::lock_guard<std::mutex> lk(list_mutex);
    return handlers.empty();
  }
  void RegisterCbWhenEmpty(std::function<void(void)>& func) {
    cb_when_empty.push_back(std::ref(func));
  }
  void UnRegisterCbWhenEmpty() { cb_when_empty.pop_front(); }
};

#endif  // DISPATCHER_H
