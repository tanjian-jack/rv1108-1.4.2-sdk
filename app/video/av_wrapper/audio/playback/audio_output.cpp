﻿/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define AUDIO_OUTPUT_CPP_
#include "audio_output.h"

#include <stdlib.h>
#include <sys/prctl.h>

#include <algorithm>

#include "video_common.h"

#include <autoconfig/main_app_autoconf.h>

#define ARRAY_SIZE(arry) (sizeof(arry) / sizeof(arry[0]))

static void notify_if_need(AudioFrame* in_frame) {
  AVFrame* frame = in_frame->av_frame;
  if (frame && frame->user_reserve_buf[0]) {
    std::mutex* mtx = (std::mutex*)frame->user_reserve_buf[0];
    std::condition_variable* cond =
        (std::condition_variable*)frame->user_reserve_buf[1];
    std::unique_lock<std::mutex> _lk(*mtx);
    // printf("notify on mtx: %p\n", mtx);
    cond->notify_one();
  }
}

WantedAudioParams AudioPlayHandler::kWantedAudioParams = {
    AV_SAMPLE_FMT_S16, AV_CH_LAYOUT_STEREO, MAIN_APP_AUDIO_SAMPLE_RATE,
    MAIN_APP_AUDIO_SAMPLE_NUM};

bool AudioPlayHandler::compare(WantedAudioParams& params) {
  return params.fmt != kWantedAudioParams.fmt ||
         params.channel_layout != kWantedAudioParams.channel_layout ||
         params.sample_rate != kWantedAudioParams.sample_rate;
}

AudioPlayHandler::AudioPlayHandler(const char* device)
    : tid_(0), run_(true), detached_(false) {
  memcpy(dev_, device, strlen(device) + 1);
  for (size_t i = 0; i < sizeof(play_chs) / sizeof(play_chs[0]); i++)
    play_chs[i].id_ = i;
  run_ = !StartThread(tid_, audio_play_routine, this);
  assert(run_);
}
AudioPlayHandler::~AudioPlayHandler() {
  {
    std::lock_guard<std::mutex> _lk(mutex_);
    run_ = false;
    cond_.notify_one();
  }
  StopThread(tid_);
  while (!frames_.empty()) {
    AudioFrame* frame = PopDataFrame();
    notify_if_need(frame);
    av_frame_free(&frame->av_frame);
    free(frame);
  }
}

AudioFrame* AudioPlayHandler::PopDataFrame() {
  if (frames_.empty())
    return nullptr;
  AudioFrame* frame = frames_.front();
  frames_.pop_front();
  return frame;
}

void AudioPlayHandler::PushDataFrame(AudioFrame* frame) {
  if (!frames_.empty() && frames_.size() >= 30) {
    fprintf(stderr, "warning: too much audio frames waiting for playing!\n");
    AudioFrame* wait_remove_frame = nullptr;
    std::list<AudioFrame*>::iterator wait_remove_it;
    for (auto it = frames_.begin(); it != frames_.end(); it++) {
      AudioFrame* f = *it;
      if (frame->play_id == f->play_id) {
        wait_remove_frame = f;
        wait_remove_it = it;
        break;
      }
      if (!wait_remove_frame) {
        wait_remove_frame = f;
        wait_remove_it = it;
      }
    }
    if (wait_remove_frame) {
      frames_.erase(wait_remove_it);
      notify_if_need(wait_remove_frame);
      av_frame_free(&wait_remove_frame->av_frame);
      free(wait_remove_frame);
    }
  }
  frames_.push_back(frame);
}

bool AudioPlayHandler::LPushDataFrame(AudioFrame* frame) {
  AVFrame* avframe = frame->av_frame;
  assert(avframe->format == kWantedAudioParams.fmt &&
         avframe->channel_layout == kWantedAudioParams.channel_layout &&
         avframe->sample_rate == kWantedAudioParams.sample_rate);
  std::lock_guard<std::mutex> _lk(mutex_);
  PlayChannel& ch = play_chs[frame->play_id];
  bool ret = ch.Fill(this, frame->av_frame);
  cond_.notify_one();
  return ret;
}

void AudioPlayHandler::PushFlushReq(int playid) {
  PlayChannel& ch = play_chs[playid];
  ch.Flush(this);
}

void AudioPlayHandler::LPushFlushReq(int playid) {
  std::lock_guard<std::mutex> _lk(mutex_);
  PushFlushReq(playid);
  cond_.notify_one();
}

bool AudioPlayHandler::LPushEventFrame(AudioFrame* frame) {
  std::lock_guard<std::mutex> _lk(mutex_);
  if (event_frame_.empty()) {
    event_frame_.push_back(frame);
    cond_.notify_one();
    return true;
  }
  return false;
}

AudioFrame* AudioPlayHandler::PopEventFrame() {
  if (event_frame_.empty())
    return nullptr;
  AudioFrame* frame = event_frame_.front();
  event_frame_.pop_front();
  return frame;
}

int AudioPlayHandler::GetAvailablePlayId() {
  for (size_t i = 0; i < ARRAY_SIZE(play_chs); i++) {
    bool except = false;
    if (play_chs[i].enabled_.compare_exchange_strong(except, !except))
      return (int)i;
  }
  fprintf(stderr, "exceed the max available play id.\n");
  return -1;
}

void AudioPlayHandler::ReturnPlayId(int play_id) {
  play_chs[play_id].enabled_ = false;
}

AudioPlayHandler::PlayChannel::PlayChannel()
    : id_(-1),
      enabled_(false),
      cache_buffer_(nullptr),
      cache_sum_samples_(0),
      cache_samples_(0) {
  memset(&fake_frame_, 0, sizeof(fake_frame_));
  fake_frame_.format = kWantedAudioParams.fmt;
  fake_frame_.sample_rate = kWantedAudioParams.sample_rate;
  fake_frame_.channel_layout = kWantedAudioParams.channel_layout;
  fake_frame_.channels =
      av_get_channel_layout_nb_channels(fake_frame_.channel_layout);
  fake_frame_.nb_samples = kWantedAudioParams.nb_samples;
  fake_frame_.extended_data = fake_frame_.data;
  fake_frame_.extended_data[0] = nullptr;
  cache_buffer_ = (uint8_t*)malloc(get_buffer_size(&fake_frame_));
  if (cache_buffer_)
    cache_sum_samples_ = kWantedAudioParams.nb_samples;
}

AudioPlayHandler::PlayChannel::~PlayChannel() {
  assert(cache_samples_ == 0);
  if (cache_buffer_)
    free(cache_buffer_);
}

static AudioFrame* alloc_new_audio_frame(AVFrame* src_frame, int playid) {
  AudioFrame* af = (AudioFrame*)malloc(sizeof(AudioFrame));
  if (unlikely(!af)) {
    printf("<%s:%d> no memory.", __func__, __LINE__);
    return nullptr;
  }
  AVFrame* new_frame = av_frame_clone(src_frame);
  if (unlikely(!new_frame)) {
    printf("<%s:%d> no memory.", __func__, __LINE__);
    free(af);
    return nullptr;
  }
  af->av_frame = new_frame;
  af->play_id = playid;
  return af;
}

// As call PushDataFrame in this function, ensure lock the list
// Return whether cache_buffer was full ever.
bool AudioPlayHandler::PlayChannel::Fill(AudioPlayHandler* handler,
                                         AVFrame* in_frame) {
  bool filled_full = false;
  AVFrame* last_avframe = nullptr;
  int nb_samples = in_frame->nb_samples;
  uint8_t* input_buffer = in_frame->extended_data[0];
  const int frame_size =
      av_get_bytes_per_sample((enum AVSampleFormat)fake_frame_.format) *
      fake_frame_.channels;
  assert(frame_size == 4);
  while (nb_samples > 0) {
    // printf("%s, process nb_samples: %d\n", __func__, nb_samples);
    int cache_free_samples = cache_sum_samples_ - cache_samples_;
    int copy_samples = 0;
    int processed_samples = nb_samples;
    uint8_t* stream = nullptr;
    if (processed_samples < cache_free_samples) {
      copy_samples = processed_samples;
    } else {
      if (cache_samples_ == 0) {
        stream = input_buffer;
      } else {
        stream = cache_buffer_;
        copy_samples = cache_free_samples;
      }
      processed_samples = cache_free_samples;
    }

    if (copy_samples) {
      // printf("%s , copy_samples : %d\n", __func__, copy_samples);
      memcpy(cache_buffer_ + cache_samples_ * frame_size, input_buffer,
             copy_samples * frame_size);
      cache_samples_ += copy_samples;
      if (cache_samples_ == cache_sum_samples_)
        cache_samples_ = 0;
    }

    if (stream) {
      fake_frame_.extended_data[0] = stream;
      AudioFrame* af = alloc_new_audio_frame(&fake_frame_, id_);
      if (unlikely(!af))
        break;
      last_avframe = af->av_frame;
      handler->PushDataFrame(af);
      filled_full = true;
    }
    // printf("%s processed_samples: %d\n\n", __func__, processed_samples);
    nb_samples -= processed_samples;
    input_buffer += (processed_samples * frame_size);
  }
  if (last_avframe) {
    // mutex, cond
    last_avframe->user_reserve_buf[0] = in_frame->user_reserve_buf[0];
    last_avframe->user_reserve_buf[1] = in_frame->user_reserve_buf[1];
  }
  return filled_full;
}

void AudioPlayHandler::PlayChannel::Flush(AudioPlayHandler* handler) {
  int frame_size =
      av_get_bytes_per_sample((enum AVSampleFormat)fake_frame_.format) *
      fake_frame_.channels;
  if (cache_samples_ > 0) {
    memset(cache_buffer_ + cache_samples_ * frame_size, 0,
           (cache_sum_samples_ - cache_samples_) * frame_size);
    cache_samples_ = 0;
    fake_frame_.extended_data[0] = cache_buffer_;
    AudioFrame* af = alloc_new_audio_frame(&fake_frame_, id_);
    if (af)
      handler->PushDataFrame(af);
  }
}

static AVFrame* convert_frame(struct SwrContext* swr,
                              AudioParams* audio_hw_params,
                              AVFrame& in_frame,
                              AVFrame& out_frame) {
  if (!swr)
    return &in_frame;
  // AVFrame should set the right member value before call this convert function
  // memset(&out_frame, 0, sizeof(out_frame));
  out_frame.format = audio_hw_params->fmt;
  out_frame.channel_layout = audio_hw_params->channel_layout;
  out_frame.channels = audio_hw_params->channels;
  out_frame.sample_rate = audio_hw_params->sample_rate;
  if (swr_convert_frame(swr, &out_frame, &in_frame)) {
    fprintf(stderr, "audio_frame_convert failed!\n");
    av_frame_unref(&out_frame);
    return nullptr;
  }
  return &out_frame;
}

static int out_buffer_to_dev(AudioParams* audio_hw_params,
                             uint8_t* buffer,
                             snd_pcm_sframes_t samples) {
  assert(buffer);
  assert(samples > 0);
  while (samples > 0) {
    int status = audio_dev_write(audio_hw_params, buffer, &samples);
    if (status < 0)
      return -1;
    else if (status == 0)
      continue;
    buffer += (status * audio_hw_params->frame_size);
  }
  return 0;
}

#if 0
static int flush_cache_to_dev(AudioParams* audio_hw_params,
                              uint8_t* const cache_buffer,
                              int& cache_offset) {
  int ret = 0;
  if (cache_offset > 0) {
    memset(cache_buffer + cache_offset, 0,
           audio_hw_params->audio_hw_buf_size - cache_offset);
    ret = out_buffer_to_dev(audio_hw_params, cache_buffer,
                            audio_hw_params->pcm_samples);
    cache_offset = 0;
  }
  return ret;
}
#endif

// fill cache, and output if full
// return size of processed data
static int fill_cache(AudioParams* audio_hw_params,
                      uint8_t* input_buffer,
                      int nb_samples,
                      uint8_t* const cache_buffer,
                      int& cache_offset) {
  int cache_free_samples = audio_hw_params->pcm_samples -
                           (cache_offset / audio_hw_params->frame_size);
  int copy_samples = 0;
  uint8_t* stream = nullptr;
  if (nb_samples < cache_free_samples) {
    copy_samples = nb_samples;
  } else {
    if (cache_offset == 0) {
      stream = input_buffer;
    } else {
      stream = cache_buffer;
      copy_samples = cache_free_samples;
    }
    nb_samples = cache_free_samples;
  }

  if (copy_samples) {
    // printf("copy_samples : %d\n", copy_samples);
    int copy_length = copy_samples * audio_hw_params->frame_size;
    memcpy(cache_buffer + cache_offset, input_buffer, copy_length);
    cache_offset += copy_length;
    if (cache_offset == audio_hw_params->audio_hw_buf_size)
      cache_offset = 0;
  }

  if (stream &&
      out_buffer_to_dev(audio_hw_params, stream, audio_hw_params->pcm_samples))
    return -1;

  return nb_samples;
}

static int out_to_dev(AVFrame* frame,
                      AudioParams* audio_hw_params,
                      uint8_t* const cache_buffer,
                      int& cache_offset) {
  uint8_t* input_buffer = frame->extended_data[0];
  int nb_samples = frame->nb_samples;
  while (nb_samples > 0) {
    // printf("process nb_samples: %d\n", nb_samples);
    int processed_samples = fill_cache(audio_hw_params, input_buffer,
                                       nb_samples, cache_buffer, cache_offset);
    // printf("fill_cache processed_samples: %d\n\n", processed_samples);
    if (processed_samples < 0) {
      // reset
      cache_offset = 0;
      return processed_samples;
    }
    nb_samples -= processed_samples;
    input_buffer += (processed_samples * audio_hw_params->frame_size);
  }
  return 0;
}

// Be called in locking of handler->mutex_.
static void pick_mix_source_frames(std::list<AudioFrame*>& src_frames,
                                   std::list<AudioFrame*>& mix_frames) {
  bool picked_play_ids[AudioPlayHandler::MAX_PLAY_PARALLEL_NUM];
  for (size_t i = 0; i < ARRAY_SIZE(picked_play_ids); i++)
    picked_play_ids[i] = false;
  for (auto it = src_frames.begin(), e = src_frames.end(); it != e;) {
    AudioFrame* f = *it;
    int play_id = f->play_id;
    if (picked_play_ids[play_id]) {
      ++it;
      continue;
    }
    picked_play_ids[play_id] = true;
    mix_frames.push_back(f);
    AVFrame* avframe = f->av_frame;
    assert(avframe->nb_samples ==
           AudioPlayHandler::kWantedAudioParams.nb_samples);
    it = src_frames.erase(it);
  }
}

static void notify_if_need_each_frame(std::list<AudioFrame*>& frames) {
  for (AudioFrame* f : frames)
    notify_if_need(f);
}

static void free_frame_list(std::list<AudioFrame*>& frames) {
  for (AudioFrame* f : frames) {
    av_frame_free(&f->av_frame);
    free(f);
  }
  frames.clear();
}

static AVFrame* mix_each_frame(std::list<AudioFrame*>& frames) {
  AVFrame* frame = nullptr;
  AVFrame* tmp_frame = nullptr;
  int ret = 0;
  int num = frames.size();
  assert(num > 1 && num <= AudioPlayHandler::MAX_PLAY_PARALLEL_NUM);
  int index = 0, offset = 0, length = 0;
  int16_t* dst = nullptr;
  int16_t* datas[AudioPlayHandler::MAX_PLAY_PARALLEL_NUM]{};
  for (AudioFrame* f : frames) {
    tmp_frame = f->av_frame;
    // printf("-- frame[%d] nb_frames: %d\n", index, f->nb_samples);
    datas[index++] = (int16_t*)tmp_frame->extended_data[0];
  }
  assert(tmp_frame->format == AV_SAMPLE_FMT_S16);
  frame = av_frame_alloc();
  if (!frame)
    goto end;
  frame->format = tmp_frame->format;
  frame->sample_rate = tmp_frame->sample_rate;
  frame->channel_layout = tmp_frame->channel_layout;
  frame->channels =
      av_get_channel_layout_nb_channels(tmp_frame->channel_layout);
  frame->nb_samples = tmp_frame->nb_samples;
  frame->extended_data = frame->data;
  ret = av_frame_get_buffer(frame, 1);
  if (ret < 0) {
    char str[AV_ERROR_MAX_STRING_SIZE] = {0};
    av_strerror(ret, str, AV_ERROR_MAX_STRING_SIZE);
    fprintf(stderr, "Error av_frame_get_buffer (%s)\n", str);
    av_frame_free(&frame);
    goto end;
  }
  length = frame->channels * frame->nb_samples;
  dst = reinterpret_cast<int16_t*>(frame->extended_data[0]);
  while (offset < length) {
    int32_t value = 0;
    for (index = 0; index < num; index++) {
      int32_t data_value = *(datas[index] + offset);
      value += data_value;
    }
    value /= num;
    *dst++ = (int16_t)value;
    // if (value >= INT16_MAX || value <= INT16_MIN)
    //  printf("~~~~value : %d\n", value);
    offset++;
  }

end:
  free_frame_list(frames);
  return frame;
}

static void close_audio_play(AudioParams& audio_hw_params,
                             struct SwrContext*& swr,
                             uint8_t*& cache_buffer,
                             int& cache_offset) {
  audio_dev_close(&audio_hw_params);
  audio_deinit_swr_context(swr);
  if (cache_buffer) {
    free(cache_buffer);
    cache_buffer = nullptr;
    cache_offset = 0;
  }
}

static void* audio_play_routine(void* arg) {
  AudioPlayHandler* handler = static_cast<AudioPlayHandler*>(arg);
  prctl(PR_SET_NAME, __FUNCTION__, 0, 0, 0);
  if (!handler)
    return nullptr;
  AudioParams audio_hw_params = {AV_SAMPLE_FMT_S16};
  uint8_t* cache_buffer = nullptr;
  int cache_offset = 0;

  struct SwrContext* swr = nullptr;
  while (handler->run_) {
    std::list<AudioFrame*> mix_frames;
    {
      static const std::chrono::milliseconds msec(3000);
      std::unique_lock<std::mutex> _lk(handler->mutex_);
      if (handler->frames_.empty() && handler->event_frame_.empty()) {
        if (handler->cond_.wait_for(_lk, msec) == std::cv_status::timeout) {
          // printf("no audio play request in %lld ms!\n", msec.count());
          close_audio_play(audio_hw_params, swr, cache_buffer, cache_offset);
        }
        continue;
      }
      AudioFrame* event = handler->PopEventFrame();
      if (event) {
        close_audio_play(audio_hw_params, swr, cache_buffer, cache_offset);
        notify_if_need(event);
        continue;
      }
      pick_mix_source_frames(handler->frames_, mix_frames);
    }
    assert(!mix_frames.empty());
    notify_if_need_each_frame(mix_frames);
    if (audio_dev_open(AudioPlayHandler::kWantedAudioParams.fmt,
                       AudioPlayHandler::kWantedAudioParams.channel_layout,
                       AudioPlayHandler::kWantedAudioParams.sample_rate,
                       &audio_hw_params, handler->dev_)) {
      fprintf(stderr, "open %s failed!!\n", handler->dev_);
      free_frame_list(mix_frames);
      break;
    }
    if (!cache_buffer) {
      uint8_t* buffer =
          (uint8_t*)realloc(cache_buffer, audio_hw_params.audio_hw_buf_size);
      if (!buffer) {
        fprintf(stderr, "no memory for cache_buffer!!\n");
        free_frame_list(mix_frames);
        break;
      } else {
        cache_buffer = buffer;
      }
      cache_offset = 0;
    }
    if (!swr) {
      WantedAudioParams params = {audio_hw_params.fmt,
                                  audio_hw_params.channel_layout,
                                  audio_hw_params.sample_rate};
      if (AudioPlayHandler::compare(params)) {
        swr = audio_init_swr_context(AudioPlayHandler::kWantedAudioParams,
                                     params);
        if (!swr) {
          free_frame_list(mix_frames);
          break;
        }
      }
    }
    AVFrame* in_frame = nullptr;
    if (mix_frames.size() == 1) {
      AudioFrame* frame = mix_frames.front();
      in_frame = frame->av_frame;
      free(frame);
    } else {
      in_frame = mix_each_frame(mix_frames);
    }
    if (unlikely(!in_frame))
      continue;
    AVFrame out_frame = {0};
    AVFrame* frame = convert_frame(swr, &audio_hw_params, *in_frame, out_frame);
    if (frame &&
        out_to_dev(frame, &audio_hw_params, cache_buffer, cache_offset)) {
      fprintf(stderr, "write frame error!\n");
      // close device?
      // assert(0);
    }
    av_frame_free(&in_frame);
    av_frame_unref(&out_frame);
  }
  audio_dev_close(&audio_hw_params);
  if (cache_buffer)
    free(cache_buffer);
  audio_deinit_swr_context(swr);
  handler->run_ = false;
  return nullptr;
}

AudioPlayHandler g_audio_play_handler("hw:0,0");

extern "C" {
#include <libavutil/time.h>
}

static void push_play_frame(AudioPlayHandler* player,
                            AVFrame* frame,
                            int playid,
                            bool wait_if_full = true) {
  if (unlikely(!player->IsRunning())) {
    av_usleep(frame->nb_samples * 1000000LL / frame->sample_rate);
    return;
  }
  AVFrame f = *frame;
  AudioFrame af = {&f, playid};
  if (wait_if_full) {
    std::mutex mtx;
    std::condition_variable cond;
    assert(sizeof(&mtx) <= sizeof(uint32_t));
    f.user_reserve_buf[0] = (uint32_t)&mtx;
    assert(sizeof(&cond) <= sizeof(uint32_t));
    f.user_reserve_buf[1] = (uint32_t)&cond;
    std::unique_lock<std::mutex> _lk(mtx);
    if (player->LPushDataFrame(&af)) {
      // printf("wait on mtx: %p\n", &mtx);
      cond.wait(_lk);
    }
  } else {
    f.user_reserve_buf[0] = 0;
    f.user_reserve_buf[1] = 0;
    player->LPushDataFrame(&af);
  }
}

void push_play_buffer(AudioPlayHandler* player,
                      uint8_t* buffer,
                      size_t size,
                      WantedAudioParams* w_params,
                      int nb_samples,
                      int playid,
                      bool wait_if_full) {
  AVFrame frame = {};
  frame.format = w_params->fmt;
  frame.sample_rate = w_params->sample_rate;
  frame.channel_layout = w_params->channel_layout;
  frame.channels = av_get_channel_layout_nb_channels(frame.channel_layout);
  frame.nb_samples = nb_samples;
  frame.extended_data = frame.data;
  frame.extended_data[0] = buffer;
  push_play_frame(player, &frame, playid, wait_if_full);
}

extern "C" int default_get_play_id() {
  return g_audio_play_handler.GetAvailablePlayId();
}

extern "C" void default_return_play_id(int playid) {
  return g_audio_play_handler.ReturnPlayId(playid);
}

extern "C" void default_push_play_frame(AVFrame* frame,
                                        int playid,
                                        int wait_if_full) {
  push_play_frame(&g_audio_play_handler, frame, playid, !!wait_if_full);
}

extern "C" void default_push_play_buffer(uint8_t* buffer,
                                         size_t size,
                                         WantedAudioParams* w_params,
                                         int nb_samples,
                                         int playid,
                                         int wait_if_full) {
  push_play_buffer(&g_audio_play_handler, buffer, size, w_params, nb_samples,
                   playid, !!wait_if_full);
}

extern "C" void default_flush_play(int playid) {
  g_audio_play_handler.LPushFlushReq(playid);
}

// TODO: internal specail use
extern "C" void default_wait_close_play() {
  AVFrame f = {};
  AudioFrame af = {&f, -1};
  std::mutex mtx;
  std::condition_variable cond;
  assert(sizeof(&mtx) <= sizeof(uint32_t));
  f.user_reserve_buf[0] = (uint32_t)&mtx;
  assert(sizeof(&cond) <= sizeof(uint32_t));
  f.user_reserve_buf[1] = (uint32_t)&cond;
  std::unique_lock<std::mutex> _lk(mtx);
  if (g_audio_play_handler.LPushEventFrame(&af)) {
    cond.wait(_lk);
  }
}

extern "C" WantedAudioParams* default_wanted_audio_params() {
  return &AudioPlayHandler::kWantedAudioParams;
}

extern "C" void default_audio_params(AudioParams* params) {
  WantedAudioParams* wap = default_wanted_audio_params();
  params->fmt = wap->fmt;
  params->channel_layout = wap->channel_layout;
  params->sample_rate = wap->sample_rate;
  params->channels = av_get_channel_layout_nb_channels(wap->channel_layout);
  params->frame_size =
      av_samples_get_buffer_size(NULL, params->channels, 1, params->fmt, 1);
  params->bytes_per_sec = av_samples_get_buffer_size(
      NULL, params->channels, params->sample_rate, params->fmt, 1);
  params->wanted_params = *wap;
  params->pcm_handle = nullptr;
  params->pcm_samples = wap->nb_samples;
  params->pcm_format = audio_convert_fmt(wap->fmt);
  params->audio_hw_buf_size =
      (params->channels * params->pcm_samples *
       snd_pcm_format_physical_width(params->pcm_format) / 8);
  params->period_size = 0;
}
