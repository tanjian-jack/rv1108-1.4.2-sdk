/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef AUDIO_OUTPUT_H_
#define AUDIO_OUTPUT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "audio_dev.h"

// Ffmpeg is enable
#include <libavutil/frame.h>

typedef struct {
  AVFrame* av_frame;
  int play_id;
} AudioFrame;

int default_get_play_id();
void default_return_play_id(int playid);
void default_push_play_frame(AVFrame* frame, int playid, int wait_if_full);
void default_push_play_buffer(uint8_t* buffer,
                              size_t size,
                              WantedAudioParams* w_params,
                              int nb_samples,
                              int playid,
                              int wait_if_full);
void default_flush_play(int playid);
void default_wait_close_play();
WantedAudioParams* default_wanted_audio_params();
void default_audio_params(AudioParams* params);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include <atomic>
#include <condition_variable>
#include <list>
#include <memory>
#include <mutex>

#ifdef AUDIO_OUTPUT_CPP_
static void* audio_play_routine(void* arg);
#endif

class AudioPlayHandler {
 public:
  static const int MAX_PLAY_PARALLEL_NUM = 16;
  static WantedAudioParams kWantedAudioParams;

  static bool compare(WantedAudioParams& params);
  AudioPlayHandler(const char* device);
  ~AudioPlayHandler();
  inline bool IsRunning() { return run_; }
  bool Detached() { return detached_; }
  void SetDetach(bool val) { detached_ = val; }
  AudioFrame* PopDataFrame();
  void PushDataFrame(AudioFrame*);
  bool LPushDataFrame(AudioFrame*);  // with locking, return whether wait
  void LPushFlushReq(int playid);
  bool LPushEventFrame(AudioFrame*);
  AudioFrame* PopEventFrame();
  int GetAvailablePlayId();
  void ReturnPlayId(int play_id);

  friend void* audio_play_routine(void* arg);

 private:
  char dev_[32];
  pthread_t tid_;
  std::mutex mutex_;
  std::condition_variable cond_;
  // AVFrame in stack could not be pass into container,
  // as extended_data may equal data whose lifecyle is same to AVFrame.
  // Using AVFrame in heap.
  std::list<AudioFrame*> frames_;
  volatile bool run_;
  bool detached_;
  std::list<AudioFrame*> event_frame_;

  class PlayChannel {
   public:
    PlayChannel();
    ~PlayChannel();
    bool Fill(AudioPlayHandler* handler, AVFrame* in_frame);
    void Flush(AudioPlayHandler* handler);
    int id_;
    std::atomic_bool enabled_;
    uint8_t* cache_buffer_;
    int cache_sum_samples_;
    int cache_samples_;
    AVFrame fake_frame_;
  };
  PlayChannel play_chs[MAX_PLAY_PARALLEL_NUM];

  void PushFlushReq(int playid);
};

void push_play_buffer(AudioPlayHandler* player,
                      uint8_t* buffer,
                      size_t size,
                      WantedAudioParams* w_params,
                      int nb_samples,
                      int playid,
                      bool wait_if_full = true);
#endif

#endif  // #ifndef AUDIO_OUTPUT_H_
