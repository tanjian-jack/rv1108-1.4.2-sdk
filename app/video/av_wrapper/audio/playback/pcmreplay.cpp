/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "pcmreplay.h"

PCMReplay::PCMReplay(AudioPlayHandler* target_player)
    : PCMReceiver(), player_(target_player), play_id_(-1) {}

PCMReplay::~PCMReplay() {
  if (play_id_ >= 0)
    player_->ReturnPlayId(play_id_);
  if (player_ && player_->Detached())
    delete player_;
}

void PCMReplay::AttachToSender() {
  play_id_ = player_->GetAvailablePlayId();
  if (play_id_ < 0)
    return;
  PCMReceiver::AttachToSender();
  GetSourceParams(w_params_.fmt, w_params_.channel_layout,
                  w_params_.sample_rate);
}

void PCMReplay::Receive(void* ptr, size_t size) {
  int nb_samples =
      size / av_samples_get_buffer_size(
                 NULL,
                 av_get_channel_layout_nb_channels(w_params_.channel_layout), 1,
                 w_params_.fmt, 1);
  push_play_buffer(player_, (uint8_t*)ptr, size, &w_params_, nb_samples,
                   play_id_, false);
}

extern AudioPlayHandler g_audio_play_handler;

extern "C" void* start_replay_from_mic(const char* dev) {
  AudioPlayHandler* target_player = nullptr;
  if (!strcmp(dev, "hw:0,0")) {
    target_player = &g_audio_play_handler;
  } else {
    target_player = new AudioPlayHandler(dev);
    if (!target_player)
      return nullptr;
    else
      target_player->SetDetach(true);
  }

  PCMReplay* replay = new PCMReplay(target_player);
  if (!replay && target_player->Detached()) {
    delete target_player;
    return nullptr;
  }
  replay->AttachToSender();
  return replay;
}

extern "C" void stop_replay_from_mic(void* replay) {
  if (!replay)
    return;
  PCMReplay* pcm_replay = (PCMReplay*)replay;
  pcm_replay->DetachFromSender();
  delete pcm_replay;
}
