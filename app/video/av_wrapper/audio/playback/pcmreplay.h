/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PCMREPLAY_H
#define PCMREPLAY_H

#ifdef __cplusplus
#include "audio/pcm_receiver.h"
#include "audio_output.h"

class PCMReplay : public PCMReceiver {
 public:
  PCMReplay(AudioPlayHandler* target_player);
  ~PCMReplay();
  virtual void AttachToSender() override;
  // Unlikely speechrec,
  // capture thread push the data to player directly.
  virtual void Receive(void* ptr, size_t size) override;

 private:
  AudioPlayHandler* player_;
  WantedAudioParams w_params_;
  int play_id_;
};

extern "C" {
#endif

// dev: pattern like hw:0,0
void* start_replay_from_mic(const char* dev);
void stop_replay_from_mic(void* replay);

#ifdef __cplusplus
}
#endif

#endif  // PCMREPLAY_H
