/*
 * Rockchip App
 *
 * Copyright (C) 2017 Rockchip Electronics Co., Ltd.
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL), available from the file
 * COPYING in the main directory of this source tree, or the
 * OpenIB.org BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PCM_RECEIVER_H
#define PCM_RECEIVER_H

#include <condition_variable>
#include <mutex>

extern "C" {
#include <libavutil/samplefmt.h>
}

#include "av_wrapper/raw_data_receiver.h"

class PCMReceiver : public RawDataReceiver {
 public:
  PCMReceiver();
  virtual ~PCMReceiver() {}
  virtual void AttachToSender() override;
  virtual void DetachFromSender() override;
  virtual void Receive(void* ptr, size_t size) override;

  // for reading. timeout: millisecond
  void ObtainBuffer(void*& ptr, size_t& size, int timeout);
  void ReleaseBuffer();

  void GetSourceParams(enum AVSampleFormat& fmt,
                       uint64_t& channel_layout,
                       int& sample_rate);

 private:
  enum State { WRITEABLE, READABLE, STATESUMS };
  class InternalBuffer {
   public:
    InternalBuffer();
    ~InternalBuffer();
    bool Alloc(size_t size);
    void* ptr_;
    size_t size_;
    State state_;
  };
  static const int loop_num = 2;
  InternalBuffer cycle_buffers_[loop_num];
  // [0]: available write index, [1]: available read index
  int idx_[STATESUMS];
  InternalBuffer* reading_buffer;
  std::mutex mtx_;
  std::condition_variable cond_;
};

#endif
