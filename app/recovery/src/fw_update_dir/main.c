/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

#include "common.h"
#include "fw_partition.h"
#include "fw_update.h"
#include "fw_ui.h"

#define FW_NAME                    "fw_update"

#define FW_DEFAULT_TEMP_PATH        "/tmp/fw_update_dir"

#define FW_DEFAULT_MOUNT_PATH       "/mnt/sdcard"
#define FW_DEFAULT_MOUNT_FULLNAME   "/mnt/sdcard/Firmware.img"
#define FW_DEFAULT_MOUNT_DEVICE     "/dev/mmcblk0"

#define PATH_MAX_LEN                256

#define CRU_BASE                    0x20200000
#define MMAP_CRU_SIZE               0x1f0

#define SDCARD_DELAY                (500 * 1000) /* us */

static int stop_draw = 0;

static void usage(void)
{
    fprintf(stderr,
            "Rockchip firmware update tool\n\n"
            "%s [-p <fw_path>] [-d <temp_dir>] [-r <is_reboot>]\n"
            "   [-L <log_mask>] [-s <show_process>]\n"
            "Examples:\n"
            "    # just update kernel at /tmp/mydir, but don't reboot after update:\n"
            "    %s -L 2 -r 0 -p /mnt/sdcard/Firmware.img\n"
            "\n",
            FW_NAME, FW_NAME);

    exit(1);
}

static bool check_mounted(const char *path)
{
    FILE *fp;
    char device[256];
    char mounted_path[256];
    char rest[256];
    char line[1024];

    fw_logi("check path: %s\n", path);
    if (!(fp = fopen("/proc/mounts", "r"))) {
        fw_logi("Error opening /proc/mounts (%s)", strerror(errno));
        return false;
    }

    while (fgets(line, sizeof(line), fp)) {
        line[strlen(line) - 1] = '\0';
        sscanf(line, "%255s %255s %255s\n", device, mounted_path, rest);
        fw_logi("device=%s,mounted_path=%s,rest=%s\n",
                device, mounted_path, rest);

        if (!strcmp(mounted_path, path)) {
            fclose(fp);
            return true;
        }
    }

    fclose(fp);

    return false;
}

static char *check_sdcard_device(void)
{
    int part = 0;
    char *dev_path = NULL;
    char cur_path[PATH_MAX_LEN];
    /* Wait for all partitions are mounted */
    usleep(SDCARD_DELAY);
    /* Get path from /dev/mmcblk0p1 */
    for (part = 99; part > 0 ; part--) {
        sprintf(cur_path, "%sp%d\0", FW_DEFAULT_MOUNT_DEVICE, part);
        if (access(cur_path, F_OK) == 0) {
            dev_path = strdup(cur_path);
            break;
        }
    }

    if (part == 0) {
        sprintf(cur_path, "%s\0", FW_DEFAULT_MOUNT_DEVICE);
        if (access(cur_path, F_OK) == 0) {
            dev_path = strdup(cur_path);
        }
    }
    return dev_path;
}

static int check_and_mount(const char *mounted_path)
{
    int retry = 20;
    int ret;
    char *dev_path = NULL;

    if (!check_mounted(mounted_path)) {
        fw_logi("Try to mount mounted_path: %s\n", mounted_path);

        while (--retry) {
            if (!access("/dev/mmcblk0", F_OK))
                break;
            usleep(100 * 1000);
        }

        if (!retry)
            fw_loge("Does mmc is initialized failed? (%d)\n", errno);
        dev_path = check_sdcard_device();
        if (dev_path) {
            ret = mount(dev_path,
                        mounted_path,
                        "vfat",
                        MS_NOATIME | MS_NOSUID,
                        0);
            free(dev_path);
            if (!ret)
                fw_logi("SD card is mounted\n");
            else if (retry == 0)
                goto err;
        } else {
            goto err;
        }
    }

    return ret;
err:
    fw_loge("SD card mount fail: (%d / %d)\n", ret, errno);
    exit(1);
}

static void check_and_umount(const char *mounted_path)
{
    if (check_mounted(mounted_path)) {
        fw_logi("will umount path: %s\n", mounted_path);
        umount(mounted_path);
    }
}

static void reboot_forcedly(void)
{
    uint32_t mmap_va;
    uint32_t *p;

    fw_logi("reboot system forcedly...\n");

    common_mmap(CRU_BASE, MMAP_CRU_SIZE, &mmap_va);

    /* APLL switch to slow mode */
    p = (uint32_t *)(mmap_va + 0xc);
    *p = 0x01000000;

    /* GPLL switch to slow mode */
    p = (uint32_t *)(mmap_va + 0x4c);
    *p = 0x01000000;

    /*
     * Write the magic 0xfdb9 to glb_srst_fst_value and
     * software reset system.
     */
    p = (uint32_t *)(mmap_va + 0x1c0);
    *p = 0xfdb9;

    common_unmap();

    fw_loge("reboot: shouldn't come here!\n");

    while (1)
        sleep(1);
}

void *ui_thread(void *param)
{
    unsigned int length = 0;

    while (!stop_draw) {
        fw_ui_entry(length++);
        sleep(1);
    }

    pthread_exit(NULL);
}

int main(int argc, char **argv)
{
    const char *mounted_path;
    char fw_path[PATH_MAX_LEN + 1];
    char temp_dir[PATH_MAX_LEN + 1];
    char *endptr;
    int ret = 0;
    int show_process = 1;
    int reboot = 1;
    int keep_fw = 0;
    int update_type = UPDATE_TYPE_ALL;
    int len;
    int opt;
    unsigned int log_mask = LOG_DEBUG;
    pthread_t tid_ui;

    memset(fw_path, 0 , sizeof(fw_path));
    memset(temp_dir, 0, sizeof(temp_dir));
    opterr = 0;

    stpcpy(fw_path, FW_DEFAULT_MOUNT_FULLNAME);
    stpcpy(temp_dir, FW_DEFAULT_TEMP_PATH);

    while ((opt = getopt(argc, argv, "hkp:d:t:r:s:L:")) > 0) {
        switch (opt) {
        case 'h':
            usage();
            break;
        case 'k':
            keep_fw = 1;
            break;
            break;
        case 'p':
            len = strlen(optarg);
            if (len > PATH_MAX_LEN) {
                fw_loge("Bad fw img path: '%s'\n", optarg);
                exit(1);
            }
            memset(fw_path, 0, sizeof(fw_path));
            stpcpy(fw_path, optarg);
            break;
        case 'd':
            len = strlen(optarg);
            if (len > PATH_MAX_LEN) {
                fw_loge("Bad temp dir path: '%s'\n", optarg);
                exit(1);
            }
            memset(temp_dir, 0, sizeof(temp_dir));
            stpcpy(temp_dir, optarg);
            break;
        case 'r':
            reboot = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                fw_loge("Bad <size> value '%s'\n", optarg);
                exit(1);
            }
            break;
        case 's':
            show_process = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                fw_loge("Bad <size> value '%s'\n", optarg);
                exit(1);
            }
            break;
        case 't':
            update_type = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                fw_loge("Bad <size> value '%s'\n", optarg);
                exit(1);
            }

            if (update_type < UPDATE_TYPE_ALL ||
                update_type > UPDATE_TYPE_ROOTFS) {
                fw_loge("parameter error, resset update_type = %d\n",
                        UPDATE_TYPE_ALL);
                update_type = UPDATE_TYPE_ALL;
            }
            break;
        case 'L':
            log_mask = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                fw_loge("Bad <size> value '%s'\n", optarg);
                exit(1);
            }

            if (log_mask < LOG_ERR || log_mask > LOG_DEBUG) {
                fw_loge("parameter %s error %d", "L", log_mask);
                exit(1);
            }
            break;
        default:
            fw_loge("Unknown option: %c\n", opt);
            usage();
            exit(1);
        }
    }

    fw_set_logmask(log_mask);

    mounted_path = split_path(fw_path);
    if (!mounted_path) {
        fw_loge("The mounted_path is NULL!\n");
        exit(1);
    }

    check_and_mount(mounted_path);

    if (show_process) {
        if (pthread_create(&tid_ui, NULL, ui_thread, NULL)) {
            fw_loge("create display pthread failed\n");
            ret = -1;
        }
    }

    if (fw_update_check_img(fw_path, MAGIC_TYPE_RKFP) < 0) {
        fw_loge("%s format error!\n", fw_path);
        exit(1);
    }

    switch (update_type) {
    case UPDATE_TYPE_KERNEL:
        fw_logi("Just update kernel\n");
        ret = fw_update_kernel(fw_path);
        if (ret != 0) {
            fw_loge("type: %d update kernel fail\n",
                    update_type);
            exit(2);
        }
        break;
    case UPDATE_TYPE_ROOTFS:
        fw_logi("Just update rootfs\n");
        ret = fw_update_rootfs(fw_path);
        if (ret != 0) {
            fw_loge("type: %d update kernel fail\n",
                    update_type);
            exit(2);
        }
        break;
    case UPDATE_TYPE_ALL:
    default:
        fw_logi("Update kernel and rootfs\n");
        ret = fw_update_kernel(fw_path);
        if (ret != 0) {
            fw_loge("type: %d update kernel fail\n",
                    update_type);
            exit(2);
        }
        ret = fw_update_rootfs(fw_path);
        if (ret != 0) {
            fw_loge("type: %d update kernel fail\n",
                    update_type);
            exit(2);
        }
        break;
    }

    if (!keep_fw) {
        if (!remove(fw_path))
            fw_logi("remove fw: %s success\n", fw_path);
        else
            fw_loge("remove fw: %s failed\n", fw_path);
    }

    check_and_umount(mounted_path);

    stop_draw = 1;

    fw_update_finish();

    /* The rootfs may be not used now, we need reboot via operate CRU */
    if (reboot)
        reboot_forcedly();

    fw_logi("Not reboot and will exit fw_update\n");

    return 0;
}
