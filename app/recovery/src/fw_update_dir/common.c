/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#include "common.h"

#define MMAP_MEM_PAGEALIGN      (PAGE_SIZE - 1)

struct mmap_ctrl {
    volatile unsigned int *mmap_va;
    unsigned int mmap_addr;
    unsigned int mmap_size;
    unsigned int mmap_offset;
    unsigned int mmap_devfd;
};

static struct mmap_ctrl mc;

unsigned int fw_logmask = LOG_ERR;

int common_mmap(unsigned int phy_addr, unsigned int mmap_size,
                unsigned int *vir_addr)
{
    mc.mmap_devfd = open("/dev/mem", O_RDWR | O_SYNC);
    if (mc.mmap_devfd < 0) {
        fw_loge(" ERROR: /dev/mem open failed !!!\n");
        return -1;
    }

    mc.mmap_offset = phy_addr & MMAP_MEM_PAGEALIGN;
    mc.mmap_addr = phy_addr - mc.mmap_offset;
    mc.mmap_size = mmap_size + mc.mmap_offset;

    mc.mmap_va = mmap((void *)mc.mmap_addr,
                      mc.mmap_size,
                      PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED,
                      mc.mmap_devfd,
                      mc.mmap_addr);
    if (mc.mmap_va == NULL) {
        fw_loge(" ERROR: mmap() failed !!!\n");
        return -1;
    }

    *vir_addr = (unsigned int)((unsigned int)mc.mmap_va + mc.mmap_offset);

    return 0;
}

int common_unmap(void)
{
    if (mc.mmap_va)
        munmap((void *)mc.mmap_va, mc.mmap_size);

    if (mc.mmap_devfd >= 0)
        close(mc.mmap_devfd);

    return 0;
}

void fw_set_logmask(unsigned int log_mask)
{
    fw_logmask = log_mask;
}

const char *split_basename(const char *path)
{
    const char *last_slash = strrchr(path, '/');

    if (last_slash)
        return last_slash + 1;

    return path;
}

const char *split_path(const char *path)
{
    static char str[256];
    char *last_slash;

    memset(str, 0, sizeof(str));
    strncpy(str, path, strlen(path));

    last_slash = strrchr(str, '/');

    if (last_slash) {
        last_slash[0] = '\0';
        return str;
    }

    return path;
}
