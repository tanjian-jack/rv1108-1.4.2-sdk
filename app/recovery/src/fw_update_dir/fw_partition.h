/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FW_PARTITION_H__
#define __FW_PARTITION_H__

typedef enum {
    PART_VENDOR = 1 << 0,
    PART_IDBLOCK = 1 << 1,
    PART_KERNEL = 1 << 2,
    PART_BOOT = 1 << 3,
    PART_USER = 1 << 31
} ENUM_PARTITION_TYPE;

typedef struct {
    uint16_t    year;
    uint8_t     month;
    uint8_t     day;
    uint8_t     hour;
    uint8_t     min;
    uint8_t     sec;
    uint8_t     reserve;
} STRUCT_DATETIME, *PSTRUCT_DATETIME;

typedef struct {
    uint8_t   uiFwTag[4];               /* "RKFP" */
    STRUCT_DATETIME dtReleaseDataTime;
    uint32_t    uiFwVer;
    uint32_t    uiSize;     /* size of sturct, unit of uint8_t */
    uint32_t    uiPartEntryOffset;      /* unit of sector */
    uint32_t    uiBackupPartEntryOffset;
    uint32_t    uiPartEntrySize;        /* unit of uint8_t */
    uint32_t    uiPartEntryCount;
    uint32_t    uiFwSize;           /* unit of uint8_t */
    uint8_t     reserved[464];
    uint32_t    uiPartEntryCrc;
    uint32_t    uiHeaderCrc;
} STRUCT_FW_HEADER, *PSTRUCT_FW_HEADER;

typedef struct {
    uint8_t szName[32];
    ENUM_PARTITION_TYPE emPartType;
    uint32_t    uiPartOffset;           /* unit of sector */
    uint32_t    uiPartSize;         /* unit of sector */
    uint32_t    uiDataLength;           /* unit of uint8_t */
    uint32_t    uiPartProperty;
    uint8_t reserved[76];
} STRUCT_PART_ENTRY, *PSTRUCT_PART_ENTRY;

typedef struct {
    STRUCT_FW_HEADER hdr;               /* 0.5KB */
    STRUCT_PART_ENTRY part[12];         /* 1.5KB */
} STRUCT_PART_INFO, *PSTRUCT_PART_INFO;

#endif /* __FW_PARTITION_H__ */
