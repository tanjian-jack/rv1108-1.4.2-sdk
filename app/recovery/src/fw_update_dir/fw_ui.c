/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <pthread.h>

#include "common.h"
#include "fw_font.h"

struct fb_info {
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    int fd;
    char *mem_va;
    unsigned size;
};

static struct fb_info fi;

static void ui_draw_point(int x, int y, int color)
{
    int offset;

    offset = x * (fi.vinfo.bits_per_pixel * 4 / 8) +
             y * fi.vinfo.xres * (fi.vinfo.bits_per_pixel * 4 / 8);

    *(fi.mem_va + offset) = color & 0xff;
    *(fi.mem_va + offset + 1) = (color >> 8) & 0xff;
    *(fi.mem_va + offset + 2) = (color >> 16) & 0xff;
    *(fi.mem_va + offset + 3) = 0;
}

static void ui_write_cn(int x, int y, unsigned char *codes, int color)
{
    int i;

    for (i = 0; i < 16; i ++) {
        int j = 0;

        for ( j = 0; j < 2; j ++) {
            int k = 0;

            x += 8 * (j + 1);
            for ( k = 0; k < 8; k ++) {
                -- x;
                if ((codes[2 * i + j] >> k) & 0x1)
                    ui_draw_point(x, y, color);
            }
        }
    }
    x -= 8;
    ++ y;
}

static void ui_write_en(int x, int y, unsigned char *codes, int color)
{
    int i = 0;

    for (i = 0; i < 16; ++ i) {
        int j = 0;

        x += 8;
        for (j = 0; j < 8; ++j) {
            --x;

            if ((codes[i] >> j) & 0x1)
                ui_draw_point(x , y, color);
        }
        ++y;
    }
}

static void ui_draw_string(int x, int y, char *str, int color)
{
    unsigned char *ptr;
    unsigned int ch;
    unsigned int cl;
    unsigned int offset;

    while (*str) {
        ch = (unsigned int)str[0];
        cl = (unsigned int)str[1];

        if (( ch >= 0xa1) && (ch < 0xf8) && (cl >= 0xa1) && (cl < 0xff)) {
            offset = ((ch - 0xa1) * 94 + (cl - 0xal)) * 32;
            ptr = __ASCII8X16__ + offset;
            ui_write_cn(x, y, ptr, color);
            x += 16;
            str += 2;
        } else {
            ptr = __ASCII8X16__ + 16 * ch;
            ui_write_en(x, y + 4, ptr, color);
            x += 8;
            str += 1;
        }
    }
}

static uint16_t ui_conv24to16(uint8_t r, uint8_t g, uint8_t b)
{
    return ((((r >> 3) & 31) << 11) |
            (((g >> 2) & 63) << 5) |
            ((b >> 3) & 31));
}

void fw_ui_entry(int length)
{
    int x = 0, y = 0;
    int step, start_pos;
    long location = 0;
    int screen_w = 0;

    /* Open the file for reading and writing */
    fi.fd = open("/dev/fb0", O_RDWR);
    if (!fi.fd) {
        fw_loge("Error: cannot open framebuffer device.\n");
        exit(1);
    }

    /* Get fixed fi information */
    if (ioctl(fi.fd, FBIOGET_FSCREENINFO, &fi.finfo)) {
        fw_loge("Error reading fixed information.\n");
        exit(2);
    }

    /* Get variable fi information */
    if (ioctl(fi.fd, FBIOGET_VSCREENINFO, &fi.vinfo)) {
        fw_loge("Error reading variable information.\n");
        exit(3);
    }

    /* Figure out the size of */
    fi.size = fi.vinfo.xres * fi.vinfo.yres * fi.vinfo.bits_per_pixel / 8;

    /* Map the device to memory */
    fi.mem_va = (char *)mmap(0, fi.size, PROT_READ | PROT_WRITE, MAP_SHARED, fi.fd, 0);
    if ((int)fi.mem_va == -1) {
        fw_loge("Error: failed to map framebuffer device to memory.\n");
        exit(4);
    }

    length %= 8;
    step = fi.vinfo.yres / 20;

    /* Fill black color first */
    if (length == 0) {
        memset(fi.mem_va, 0, fi.size);

        /* Draw rectangle, and try to put the pixel. */
        y = fi.vinfo.yres / 2 - 4 * step;
        for (x = fi.vinfo.xres / 2 - fi.vinfo.xres / 10; x < fi.vinfo.xres / 2 + fi.vinfo.xres / 10; x++) {
            location = (x + fi.vinfo.xoffset) * (fi.vinfo.bits_per_pixel / 8) +
                       (y + fi.vinfo.yoffset) * fi.finfo.line_length;
            *((unsigned short int*)(fi.mem_va + location)) = 255;
        }
        y = fi.vinfo.yres / 2 + 4 * step;
        for (x = fi.vinfo.xres / 2 - fi.vinfo.xres / 10; x < fi.vinfo.xres / 2 + fi.vinfo.xres / 10; x++) {
            location = (x + fi.vinfo.xoffset) * (fi.vinfo.bits_per_pixel / 8) +
                       (y + fi.vinfo.yoffset) * fi.finfo.line_length;
            *((unsigned short int*)(fi.mem_va + location)) = 255;
        }
        x = fi.vinfo.xres / 2 - fi.vinfo.xres / 10;
        for (y = fi.vinfo.yres / 2 - fi.vinfo.yres / 5; y < fi.vinfo.yres / 2 + fi.vinfo.yres / 5; y++) {
            location = (x + fi.vinfo.xoffset) * (fi.vinfo.bits_per_pixel / 8) +
                       (y + fi.vinfo.yoffset) * fi.finfo.line_length;
            *((unsigned short int*)(fi.mem_va + location)) = 255;
        }
        x = fi.vinfo.xres / 2 + fi.vinfo.xres / 10;
        for (y = fi.vinfo.yres / 2 - fi.vinfo.yres / 5; y < fi.vinfo.yres / 2 + fi.vinfo.yres / 5; y++) {
            location = (x + fi.vinfo.xoffset) * (fi.vinfo.bits_per_pixel / 8) +
                       (y + fi.vinfo.yoffset) * fi.finfo.line_length;
            *((unsigned short int*)(fi.mem_va + location)) = 255;
        }
    }

    start_pos = fi.vinfo.yres / 2 - fi.vinfo.yres / 5 + length * step;

    for (y = start_pos; y < start_pos + step; y++) {
        for (x = fi.vinfo.xres / 2 - fi.vinfo.xres / 10 ;
             x < fi.vinfo.xres / 2 + fi.vinfo.xres / 10;
             x++) {
            location = (x + fi.vinfo.xoffset) *
                       (fi.vinfo.bits_per_pixel / 8) +
                       (y + fi.vinfo.yoffset) *
                       fi.finfo.line_length;
            if (fi.vinfo.bits_per_pixel == 32 ) {
                *(fi.mem_va + location) = 100;        // Some blue
                *(fi.mem_va + location + 1) = 15 + (x - 100) / 2; // A little green
                *(fi.mem_va + location + 2) = fi.vinfo.yres / 5 - (y - 100) / 5; // A lot of red
                *(fi.mem_va + location + 3) = 0;      // No transparency
            } else { //assume 16bpp
                unsigned char b = 255 * x / (fi.vinfo.xres - 10);
                unsigned char g = 255;     // (x - 100)/6 A little green
                unsigned char r = 255;    // A lot of red
                unsigned short int t = ui_conv24to16(r, g, b);
                *((unsigned short int*)(fi.mem_va + location)) = t;
            }
        }
    }
    munmap(fi.mem_va, fi.size);
    close(fi.fd);
    fi.fd = -1;
}
