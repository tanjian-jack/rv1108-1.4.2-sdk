/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __COMMON_H__
#define __COMMON_H__

#ifndef PAGE_SIZE
#define PAGE_SIZE           (2 << 12)
#endif

#define LOG_ERR             0
#define LOG_INFO            1
#define LOG_DEBUG           2

extern unsigned int fw_logmask;

#define fw_loge(fmt, ...)               \
do {                            \
    if (fw_logmask >= LOG_ERR)          \
        printf("FWLOGE: " fmt, ##__VA_ARGS__);  \
} while (0)

#define fw_logi(fmt, ...)               \
do {                            \
    if (fw_logmask >= LOG_INFO)         \
        printf("FWLOGI: " fmt, ##__VA_ARGS__);  \
} while (0)

#define fw_logd(fmt, ...)               \
do {                            \
    if (fw_logmask >= LOG_DEBUG)            \
        printf("FWLOGD: " fmt, ##__VA_ARGS__);  \
} while (0)

void fw_set_logmask(unsigned int log_mask);

const char *split_basename(const char *path);
const char *split_path(const char *path);

#endif /* __COMMON_H__ */
