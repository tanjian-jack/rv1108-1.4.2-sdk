/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fw_update.h"
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "common.h"
#include "fw_partition.h"

#define FW_UPDATE_ALIGN_SIZE    (64 * 1024)
#define IMG_BUF_SIZE            (64 * 1024)
#define IMG_CHK_SIZE            512

#define FW_KERNEL_PART_PATH     "/dev/mtdblock2"
#define FW_ROOTFS_PART_PATH     "/dev/mtdblock3"

struct image_info {
    STRUCT_PART_INFO *fw_head;
    int is_init;
    int fd;
    int offset;
    int part_size;
    int size;
    int from;
};

static struct image_info ii;

int fw_update_check_img(char *image_path, int magic_type)
{
    int ret = 0;
    int size;
    int fd;
    char buf[IMG_CHK_SIZE];

    fw_logd("%s, %d: image_path: %s\n", __func__, __LINE__, image_path);

    fd = open(image_path, O_RDONLY);
    if (fd < 0) {
        fw_loge("can not open %s\n", image_path);
        ret = -1;
        goto err1;
    }

    size = read(fd, buf, IMG_CHK_SIZE);

    if (magic_type == MAGIC_TYPE_RKFP) {
        if (memcmp(buf, "RKFP", 4) != 0) {
            fw_loge("RKFP fw format error\n");
            ret = -1;
            goto err1;
        }
    } else if (magic_type == MAGIC_TYPE_KERN) {
        if (memcmp(buf, "KERN", 4) != 0) {
            fw_loge("KERN fw format error\n");
            ret = -1;
            goto err1;
        }
    } else if (magic_type == MAGIC_TYPE_HSQS) {
        if (memcmp(buf, "hsqs", 4) != 0) {
            fw_loge("hsqs fw format error\n");
            ret = -1;
            goto err1;
        }
    }

err1:
    if (fd)
        close(fd);

    return ret;
}

static int fw_config_image_info(struct image_info* imginfo,
                                const char *parse_name)
{
    STRUCT_PART_INFO *fw_head;
    int ret = 0;
    int i;

    if (!imginfo) {
        fw_loge("The image info pointer is NULL!\n");
        ret = -1;
        goto out;
    }

    fw_head = imginfo->fw_head;

    if (memcmp(fw_head->hdr.uiFwTag, "RKFP", 4) != 0) {
        fw_loge("fw format error\n");
        ret = -1;
        goto out;
    }

    for (i = 0; i < fw_head->hdr.uiPartEntryCount; i++) {
        if (memcmp(fw_head->part[i].szName , parse_name,
                   strlen(parse_name)) == 0) {
            break;
        }
    }

    if (i == fw_head->hdr.uiPartEntryCount) {
        fw_loge("can't find d_img part\n");
        ret = -1;
        goto out;
    }

    imginfo->offset = fw_head->part[i].uiPartOffset * 512;
    imginfo->part_size = fw_head->part[i].uiPartSize * 512;
    imginfo->size = (imginfo->part_size + FW_UPDATE_ALIGN_SIZE - 1) &
                    (~(FW_UPDATE_ALIGN_SIZE - 1));

    fw_logd("%s: %d - imginfo: offset = %d, part_size = %d, size = %d\n",
            __func__, __LINE__,
            imginfo->offset, imginfo->part_size, imginfo->size);

    lseek(imginfo->fd, imginfo->offset, SEEK_SET);

out:
    return ret;
}

static void fw_put_image_info(struct image_info *imginfo)
{
    imginfo->is_init = 0;

    if (imginfo) {
        close(imginfo->fd);

        if (imginfo->fw_head)
            free(imginfo->fw_head);
    }
}

static struct image_info* fw_get_image_info(const char *image_path)
{
    STRUCT_PART_INFO *fw_head;
    int ret = 0;
    int read_size;

    fw_logd("%s: %d, image_path: %s\n", __func__, __LINE__, image_path);

    /* Now, we just choice one image info, from file or from ram. */
    if (ii.is_init == 1) {
        fw_loge("image info has been initialized.\n");
        return &ii;
    }

    ii.fw_head = NULL;

    fw_head = malloc(sizeof(STRUCT_PART_INFO));
    if (fw_head == NULL) {
        fw_loge("fw_head malloc fail\n");
        ret = -1;
        goto err1;
    }

    /* Get image from image file */
    ii.fd = open(image_path, O_RDONLY);
    if (ii.fd < 0) {
        fw_loge("can not open %s\n", image_path);
        ret = -1;
        goto err2;
    }
    read_size = read(ii.fd, fw_head, sizeof(STRUCT_PART_INFO));

    if (read_size != sizeof(STRUCT_PART_INFO))
        fw_loge("%s: %d, fw_update read fail\n", __func__, __LINE__);

    ii.fw_head = fw_head;
    ii.is_init = 1;

    return &ii;

err2:
    if (fw_head)
        free(fw_head);
err1:
    return (void*)ret;
}

static int fw_update_parse_img(const char *image_path, const char *parse_name,
                               char *output)
{
    struct image_info *imginfo;
    char *buf;
    int ret = 0;
    int fd;
    int read_size;
    int write_size;
    int image_size;

    fw_logd("%s: %d, image_path: %s, parse_name: %s\n",
            __func__, __LINE__, image_path, parse_name);

    imginfo = fw_get_image_info(image_path);
    if (imginfo < 0) {
        fw_loge("%s get image info failed: %d\n", __func__, imginfo);
        ret = -1;
        goto err1;
    }

    ret = fw_config_image_info(imginfo, parse_name);
    if (ret < 0) {
        fw_loge("%s: %d, fw_config_image_info failed, ret: %d\n",
                __func__, __LINE__, ret);
        fw_put_image_info(imginfo);
        goto err1;
    }

    buf = malloc(IMG_BUF_SIZE);
    if (!buf) {
        ret = -1;
        goto err1;
    }

    fd = open(output, O_RDWR | O_CREAT | O_SYNC);
    if (fd < 0) {
        fw_loge("can not open output: %s\n", output);
        ret = -1;
        goto err2;
    }

    image_size = imginfo->size;

    while (image_size) {
        read_size = read(imginfo->fd, buf, IMG_BUF_SIZE);

        if (read_size > 0) {
            write_size = write(fd, buf, read_size);
            if (write_size != read_size) {
                fw_loge("fw_update_d_img disk write dev err\n");
                ret = -1;
                goto err3;
            }

            image_size -= read_size;
        }
    }

err3:
    if (fd > 0)
        close(fd);
err2:
    if (buf)
        free(buf);
err1:
    return ret;
}

static int fw_update_find_img(const char *image_path, const char *parse_name,
                              int *img_size)
{
    struct image_info *imginfo;
    int ret = 0;

    fw_logd("%s, %d: - image_path: %s, parse_name: %s\n",
            __func__, __LINE__, image_path, parse_name);

    imginfo = fw_get_image_info(image_path);
    if (imginfo < 0) {
        fw_loge("%s get image info failed: %d\n", __func__, imginfo);
        ret = -1;
        goto err;
    }

    ret = fw_config_image_info(imginfo, parse_name);
    if (ret < 0) {
        fw_loge("%s: %d, fw_config_image_info failed, ret: %d\n",
                __func__, __LINE__, ret);
        fw_put_image_info(imginfo);
        goto err;
    }

    *img_size = imginfo->size;

    return imginfo->fd;

err:
    return ret;
}

static int fw_update_image(const char *image_path, const char *parse_name)
{
    int ret = 0;
    int img_fd;
    int dst_fd;
    int size;
    int image_total;
    int image_size;
    int read_size;
    int write_size;
    int process_val_pre = 0;
    int process_val;
    char cmd_buf[IMG_BUF_SIZE];
    char *buf;

    fw_logd("%s, %d, cmd_buf: %s, parse_name: %s\n",
            __func__, __LINE__, cmd_buf, parse_name);

    img_fd = fw_update_find_img(image_path, parse_name, &image_size);

    image_total = image_size;

    fw_logd("img_fd = %d, parse_name: %s, image_size: %d\n",
            img_fd, parse_name, image_size);

    memset(cmd_buf, 0, sizeof(cmd_buf));

    if (!strcmp(parse_name, "kernel")) {
        sprintf(cmd_buf, FW_KERNEL_PART_PATH);
    } else if (!strcmp(parse_name, "rootfs")) {
        sprintf(cmd_buf, FW_ROOTFS_PART_PATH);
    } else {
        fw_loge("ERR: invalid parse_name: %s\n", parse_name);
        ret = -1;
        goto err;
    }

    fw_logi("%s partition path: %s\n", parse_name, cmd_buf);

    buf = malloc(IMG_BUF_SIZE);
    if (buf == NULL) {
        ret = -1;
        goto err;
    }

    dst_fd = open(cmd_buf, O_RDWR | O_SYNC);
    if (dst_fd < 0) {
        ret = -1;
        fw_loge("open %s fail\n", cmd_buf);
        goto err;
    }

    while (image_size) {
        read_size = read(img_fd, buf, IMG_BUF_SIZE);
        write_size = write(dst_fd, buf, read_size);
        if (read_size != write_size) {
            ret = -1;
            fw_loge("write %s fail\n", cmd_buf);
            break;
        }
        image_size -= read_size;

        process_val = 100 - (long long)image_size * 100 / image_total;
        if (process_val > process_val_pre + 9) {
            process_val_pre = process_val;
            fw_logi("%s doing: %d%%\n", parse_name, process_val);
        }
    }

    fw_logi("Well, %s update OK!\n", parse_name);

    close(dst_fd);
    free(buf);
err:
    return ret;
}

int fw_update_kernel(const char *image_path)
{
    return fw_update_image(image_path, "kernel");
}

int fw_update_rootfs(const char *image_path)
{
    return fw_update_image(image_path, "rootfs");
}

int fw_update_finish(void)
{
    fw_put_image_info(&ii);

    return 0;
}
