/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FW_UPDATE_H__
#define __FW_UPDATE_H__

#define MAGIC_TYPE_RKFP         0x1000
#define MAGIC_TYPE_KERN         0x1001
#define MAGIC_TYPE_HSQS         0x1002

enum {
    UPDATE_TYPE_ALL = 0,
    UPDATE_TYPE_KERNEL,
    UPDATE_TYPE_ROOTFS,
};

int fw_update_kernel(const char *image_path);
int fw_update_rootfs(const char *image_path);
int fw_update_finish(void);

#endif /* __FW_UPDATE_H__ */
