/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __INIT_HOOK_H__
#define __INIT_HOOK_H__

/*
 * The BOOT flags
 *
 * Note that: the BOOT_RECOVERY is reserved and used for debug.
 */
enum {
    BOOT_ROOTFS = 0,
    BOOT_FLASHED,
    BOOT_FWUPDATE,
    BOOT_RECOVERY,
    BOOT_FWUPDATE_TEST_START = 0x10,
    BOOT_FWUPDATE_TEST_DOING,
    BOOT_FWUPDATE_TEST_CONTINUE,
};

#endif /* __INIT_HOOK_H__ */
