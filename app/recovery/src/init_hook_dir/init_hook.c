/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: Xing Zheng <zhengxing@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <linux/fs.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/reboot.h>
#include <sys/mman.h>
#include <unistd.h>

#include "init_hook.h"

#define ih_log(fmt, ...)                \
do {                                    \
    printf("IH: " fmt, ##__VA_ARGS__);  \
} while (0)

/* Vendor infos */
#define VENDOR_REQ_TAG                  0x56524551
#define VENDOR_READ_IO                  _IOW('v', 0x01, unsigned int)
#define VENDOR_WRITE_IO                 _IOW('v', 0x02, unsigned int)

/* Change the id when flash firmware */
#define VENDOR_FW_UPDATE_ID             6
#define VENDOR_FW_TEST_COUNT_ID         7

/* Dup from flash_vendor_storage.c in kernel */
#define VENDOR_DATA_SIZE                1024
#define VERDOR_DEVICE                   "/dev/vendor_storage"

#define DEF_BOOT_ROOTFS                 "/rootfs"

#define FW_UPDATE_TEST_MAX              UINT_MAX

/*
 * It looks like that nobody will accesss /tmp at squashfs
 * when early startup. Let's try to use it as old root dir.
 * temporarily.
 */
#define TEMP_OLDROOT                    "tmp"

#define ARG_NUM                         16

/*
 * Umount options
 * It looks like will casuse compile error if we include <linux/fs.h> or
 * <sys/mount.h>. As busybox umount.c, we just define MNT_XXX in here.
 */
#define MNT_FORCE   0x00000001  /* Attempt to forcibily umount */
#define MNT_DETACH  0x00000002  /* Just detach from the tree */
#define MNT_EXPIRE  0x00000004  /* Mark for expiry */

/* Defined in app/video/parameter.c */
struct flash_param {
    int flashed_flag;
};

typedef struct _RK_VERDOR_REQ {
    uint32_t tag;
    uint16_t id;
    uint16_t len;
    uint8_t data[VENDOR_DATA_SIZE];
} RK_VERDOR_REQ;

static void wait_and_sync(void)
{
    int count = 5000;

    while (--count)
        usleep(100);

    sync();
}

static void do_reboot(void)
{
    ih_log("call reboot system ...\n");

    reboot(RB_AUTOBOOT);

    ih_log("ERR: reboot: shouldn't come here!\n");

    while (1)
        sleep(1);
}

static int vendor_read(uint8_t *buffer, int buffer_size, uint16_t vendor_id)
{
    RK_VERDOR_REQ req;
    int ret;
    int sys_fd = open(VERDOR_DEVICE, O_RDWR, 0);

    if (sys_fd < 0) {
        ih_log("ERR: vendor_storage open fail\n");
        return -2;
    }

    req.tag = VENDOR_REQ_TAG;
    req.id = vendor_id;

    /* Read length, the max size <= 1024 Bytes */
    req.len =
        buffer_size > VENDOR_DATA_SIZE ? VENDOR_DATA_SIZE : buffer_size;

    /*
     * The returned req->len is the real data length stored
     * in the NV-storage
     */
    ret = ioctl(sys_fd, VENDOR_READ_IO, &req);
    if (ret) {
        ih_log("ERR: vendor read error ret = %d\n", ret);
        close(sys_fd);
        return -1;
    }

    memcpy(buffer, req.data, req.len);
    close(sys_fd);

    return 0;
}

static int vendor_write(uint8_t *buffer, int buffer_size, uint16_t vendor_id)
{
    RK_VERDOR_REQ req;
    int ret;
    int sys_fd = open(VERDOR_DEVICE, O_RDWR, 0);

    if (sys_fd < 0) {
        ih_log("ERR: vendor_write open fail\n");
        return -1;
    }

    req.tag = VENDOR_REQ_TAG;
    req.id = vendor_id;
    req.len =
        buffer_size > VENDOR_DATA_SIZE ? VENDOR_DATA_SIZE : buffer_size;
    memcpy(req.data, buffer, req.len);

    ret = ioctl(sys_fd, VENDOR_WRITE_IO, &req);
    if (ret) {
        ih_log("ERR: vendor write error\n");
        close(sys_fd);
        return -1;
    }

    close(sys_fd);

    return 0;
}

static int flash_flag_show(int vendor_id)
{
    struct flash_param param;
    int ret;

    if (vendor_id < 0) {
        ih_log("ERR: %s: invalid vendor_id: %d\n", __func__, vendor_id);
        return vendor_id;
    }

    ret = vendor_read((uint8_t *)&param, sizeof(param), vendor_id);
    if (ret < 0) {
        ih_log("ERR: %s: vendor_read fail: %d\n", __func__, ret);
        return ret;
    }

    ih_log("%s: vendor_id: %d, flashed_flag: %d, ret: %d\n",
           __func__, vendor_id, param.flashed_flag, ret);

    return ret;
}

static int flash_flag_store(int vendor_id, int boot_val)
{
    struct flash_param param;
    int ret;

    if (vendor_id < 0) {
        ih_log("ERR: %s: invalid vendor_id: %d\n", __func__, vendor_id);
        return vendor_id;
    }

    param.flashed_flag = boot_val;

    ret = vendor_write((uint8_t *)&param, sizeof(param), vendor_id);
    if (ret < 0) {
        ih_log("ERR: %s: vendor_write fail: %d\n", __func__, ret);
        return ret;
    }

    ih_log("%s: vendor_id: %d, flashed_flag: %d, ret: %d\n",
           __func__, vendor_id, param.flashed_flag, ret);

    return ret;
}

static int check_boot_flag(void)
{
    struct flash_param param;
    int ret;

    ret = vendor_read((uint8_t *)&param, sizeof(param), VENDOR_FW_UPDATE_ID);
    if (ret < 0) {
        ih_log("ERR: vendor_read fail: %d\n", ret);
        return ret;
    }

    ih_log("%s: flashed_flag: %d, ret: %d\n",
           __func__, param.flashed_flag, ret);

    return param.flashed_flag;
}

static int fork_process(char *cmd, int *exit_status)
{
    pid_t pid;
    int ret = 0;

    pid = fork();

    if (pid < 0) {
        ih_log("err: fork failed!\n");
        return pid;
    } else if (pid == 0) {
        /* Child process. */
        char *argv[ARG_NUM];
        char str[BUFSIZ] = { 0 };
        char *delimiter = " ";
        char *save = NULL;
        int i, num = 0;
        int fd = -1;

        ih_log("Fork child process\n");

        /* Redirect STD streams */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        /* Re-opened fd should be STDIN_FILENO (0), don't close it. */
        fd = open("/dev/console", O_RDWR);
        if (fd < 0) {
            ih_log("ERR: Open /dev/console failed!\n");
            return fd;
        }

        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);

        /* To parse commands */
        strcpy(str, cmd);

        argv[num] = strtok_r(str, delimiter, &save);
        while (argv[num++])
            argv[num] = strtok_r(NULL, delimiter, &save);

        argv[num] = NULL;

        execvp(argv[0], argv);

        ih_log("process(%d) shouldn't come here!\n", getpid());
    } else {
        int status;

        /* Parent process. */
        ret = wait(&status);
        if (ret < 0) {
            ih_log("ERR: wait child failed: (%d)\n", ret);
            return ret;
        }

        ih_log("process(%d) wait child(%d) status: %d\n",
               getpid(), ret, status);

        if (WIFEXITED(status) && exit_status)
            *exit_status = WEXITSTATUS(status);
    }

    return ret;
}

static int get_rootfs_mounted_point(char *point)
{
    int fd;
    int rsize;
    char buf[128] = {0};
    char *save = NULL;
    char *temp;

    /* Get mounted point for rootfs from command line */
    fd = open("/proc/cmdline", O_RDONLY, 0);
    if (fd < 0) {
        ih_log("ERR: /proc/cmdline open fail\n");
        return -1;
    }

    rsize = read(fd, buf, sizeof(buf));
    if (rsize > 0) {
        temp = strstr(buf, "root=");
        temp = strtok_r(temp, "=", &save);
        temp = strtok_r(NULL, "=", &save);
        temp = strtok_r(temp, " ", &save);
        strcpy(point, temp);
    }

    if (fd)
        close(fd);

    return 0;
}

static int prepare_rootfs(void)
{
    int ret, exit_status;
    char point[128] = {0};

    if (access(DEF_BOOT_ROOTFS, F_OK) < 0) {
        if (mkdir(DEF_BOOT_ROOTFS, 0644) < 0) {
            ih_log("mkdir %s failed!\n", DEF_BOOT_ROOTFS);
            ret = -1;
            goto err;
        }
    }

    ret = get_rootfs_mounted_point(point);
    if (ret < 0) {
        ih_log("ERR: get_rootfs_mounted_point failed: %d\n", ret);
        goto err;
    }

    ih_log("get point: %s OK\n", point);

    ret = mount(point, DEF_BOOT_ROOTFS, "squashfs",
                MS_NOATIME | MS_NOSUID, 0);
    if (ret < 0) {
        ih_log("ERR: mount %s failed! (%d)\n", DEF_BOOT_ROOTFS, ret);
        ret = -1;
        goto err;
    }

    return 0;

err:
    return ret;
}

static int prepare_fw_update(void)
{
    int exit_status;
    int ret;

    ret = fork_process("/bin/mkdir -p /mnt/sdcard", &exit_status);
    if (ret < 0 || exit_status != 0) {
        ih_log("ERR: mkdir failed! (%d, %d)\n", ret, exit_status);
        ret = -1;
    }

    return ret;
}

static int fw_update_stress_test(int boot_from, unsigned int test_max)
{
    unsigned int test_count;
    int exit_status;
    int ret;

    ret = prepare_fw_update();
    if (ret < 0) {
        ih_log("ERR: %s prepare_fw_update failed: (%d)\n",
               __func__, ret);
        return ret;
    }

    if (boot_from == BOOT_FWUPDATE_TEST_START) {
        ih_log("First time, clean the test_count\n");
        flash_flag_store(VENDOR_FW_TEST_COUNT_ID, 0);
    }

    ret = vendor_read((uint8_t *)&test_count, sizeof(test_count),
                      VENDOR_FW_TEST_COUNT_ID);
    if (ret < 0) {
        ih_log("ERR: %s: vendor_read fail: %d\n", __func__, ret);
        return ret;
    }

    ih_log("start to update firmware: (%lu / %lu)...\n", test_count, test_max);

    /* Switch 2 firmware image for updating */
    if (test_count % 2) {
        ih_log("update Firmware_B.img\n");
        ret = fork_process("/bin/fw_update -k -L 2 -r 0 -p /mnt/sdcard/Firmware_B.img",
                           &exit_status);
    } else {
        ih_log("update Firmware_A.img\n");
        ret = fork_process("/bin/fw_update -k -L 2 -r 0 -p /mnt/sdcard/Firmware_A.img",
                           &exit_status);
    }

    if (ret < 0 || exit_status != 0) {
        ih_log("ERR: count: (%d) failed! pid: %d, exit_code: %d\n",
               test_count, ret, exit_status);

        /* Try to enter rootfs again. */
        flash_flag_store(VENDOR_FW_UPDATE_ID, BOOT_ROOTFS);
        return -exit_status;
    }

    ++test_count;
    ih_log("finished and passed count: (%lu / %lu) cycles, exit_status: %d\n",
           test_count, test_max, exit_status);

    /* Increase and store test count */
    ret = vendor_write((uint8_t *)&test_count, sizeof(test_count),
                       VENDOR_FW_TEST_COUNT_ID);
    if (ret < 0) {
        ih_log("ERR: %s: vendor_write fail: %d\n", __func__, ret);
        return ret;
    }

    /* Recover flash flag, boot from rootfs at next boot */
    flash_flag_store(VENDOR_FW_UPDATE_ID, BOOT_FWUPDATE_TEST_DOING);

    if (test_count >= test_max) {
        ih_log("Good, passed [%lu / %lu] cycles!\n", test_count);
        flash_flag_store(VENDOR_FW_UPDATE_ID, BOOT_ROOTFS);
    }

    wait_and_sync();
    do_reboot();
    return 0;
}

int main(int argc, char **argv)
{
    int boot_from;
    int opt;
    int ret;
    int boot_val;
    int exit_status;
    int vendor_id = VENDOR_FW_UPDATE_ID;
    int test_max = FW_UPDATE_TEST_MAX;
    char *endptr;

    ih_log("==== %s enter ====\n", argv[0]);

    while ((opt = getopt(argc, argv, "hrw:i:n:")) > 0) {
        switch (opt) {
        case 'r':
            flash_flag_show(vendor_id);
            exit(1);
        case 'w':
            boot_val = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                ih_log("Bad: (%d) <size> value '%s'\n",
                       __LINE__, optarg);
                exit(1);
            }

            flash_flag_store(vendor_id, boot_val);
            exit(1);
        case 'i':
            vendor_id = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                ih_log("Bad: (%d) <size> value '%s'\n",
                       __LINE__, optarg);
                exit(1);
            }

            ih_log("NOTICE: vendor_id is changed: %d\n", vendor_id);
            break;
        case 'n':
            test_max = strtoul(optarg, &endptr, 0);
            if (*endptr) {
                ih_log("Bad: (%d) <size> value '%s'\n",
                       __LINE__, optarg);
                exit(1);
            }
            break;
        default:
            ih_log("Unknown option: %c\n", opt);
            exit(1);
            break;
        }
    }

    boot_from = check_boot_flag();
    if (boot_from < 0) {
        ih_log("ERR: check_boot_flag fail: %d\n", boot_from);
        boot_from = BOOT_ROOTFS;
    }

    switch (boot_from) {
    case BOOT_ROOTFS:
    case BOOT_FLASHED:
    case BOOT_FWUPDATE_TEST_DOING:
        ret = prepare_rootfs();
        if (ret < 0) {
            ih_log("ERR: prepare_rootfs fail: %d\n", ret);
            exit(3);
        }

        ret = chdir(DEF_BOOT_ROOTFS);
        if (ret < 0) {
            ih_log("ERR: chdir fail: %d\n", ret);
            exit(3);
        }

        /* The TEMP_OLDROOT exists in squashfs */
        ret = pivot_root(".", TEMP_OLDROOT);
        if (ret < 0) {
            ih_log("ERR: pivot_root fail: %d\n", ret);
            exit(3);
        }

        /* Force umount TEMP_OLDROOT */
        ret = umount2(TEMP_OLDROOT, MNT_FORCE | MNT_DETACH);
        if (ret < 0) {
            ih_log("ERR: umount2 fail: %d\n", ret);
            exit(3);
        }

        chdir("/");

        ih_log("chroot and enter rootfs\n");

        ret = fork_process("/linuxrc", &exit_status);
        if (ret < 0 || exit_status != 0)
            ih_log("ERR: fw_update failed! (%d, %d)\n",
                   ret, exit_status);

        ih_log("ERR: should not come here, chroot failed?\n");
        break;
    case BOOT_FWUPDATE:
        ih_log("start to update firmware...\n");

        ret = prepare_fw_update();
        if (ret < 0) {
            ih_log("ERR: %s prepare_fw_update failed: (%d)\n",
                   __func__, ret);
            break;
        }

        ret = fork_process("/bin/fw_update -L 2 -r 0", &exit_status);
        if (ret < 0 || exit_status != 0) {
            ih_log("ERR: fw_update failed! (%d, %d)\n",
                   ret, exit_status);
            break;
        }

        ih_log("firmware update finish, exit_status: %d\n", exit_status);

        /* Recover flash flag, boot from rootfs at next boot */
        flash_flag_store(VENDOR_FW_UPDATE_ID, BOOT_ROOTFS);
        wait_and_sync();
        do_reboot();
        break;
    case BOOT_FWUPDATE_TEST_START:
    case BOOT_FWUPDATE_TEST_CONTINUE:
        fw_update_stress_test(boot_from, test_max);
        break;
    case BOOT_RECOVERY:
    default:
        ih_log("stay on the tsh\n");
        break;
    }

    ih_log("==== %s exit ====\n", argv[0]);

    return 0;
}
