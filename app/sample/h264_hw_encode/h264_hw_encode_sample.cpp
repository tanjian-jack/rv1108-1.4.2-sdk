extern "C" {
#include "../../video/video_ion_alloc.c"  // for alloc ion memory
}

#include <pthread.h>

#include "../../video/av_wrapper/video_common.cpp"

#include "../../video/av_wrapper/mpp_inc.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/buffer.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/base_encoder.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/mpp_h264_encoder.cpp"  // for call h264 hw encoder

// pc:
//.....cd <project root path>/app/sample/h264_hw_encode
//     cp test_320_240.nv12 <sdcard>
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ \
      -I../../video \
      -I../../video/av_wrapper \
      -I../../video/watermark \
      -I../../../out/system/include \
      -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -std=c++11 -fstack-protector-all  -g \
      -L../../../out/system/lib \
      -o h264_hw_encode_sample h264_hw_encode_sample.cpp \
      -lavformat -lavcodec -lavutil -lpthread -lion -lmpp
//     cp h264_hw_encode_sample <sdcard>

// board:
//        mount /dev/mmcblk0p1 /mnt/sdcard/
//        cd /mnt/sdcard
//        ./h264_hw_encode_sample

int main() {
  int w = 320, h = 240;
  int frame_size = w * h * 3 / 2;
  MediaConfig h264_config;
  struct video_ion nv12_ion_buffer = {}, h264_ion_buffer = {};
  nv12_ion_buffer.client = -1;
  nv12_ion_buffer.fd = -1;
  h264_ion_buffer.client = -1;
  h264_ion_buffer.fd = -1;
  if (video_ion_alloc_rational(&nv12_ion_buffer, w, h, 3, 2) == -1)
    assert(0);
  if (video_ion_alloc_rational(&h264_ion_buffer, w, h, 3, 2) == -1)
    assert(0);
  BufferData input_data;
  input_data.vir_addr_ = nv12_ion_buffer.buffer;
  input_data.ion_data_.fd_ = nv12_ion_buffer.fd;
  input_data.ion_data_.handle_ = nv12_ion_buffer.handle;
  input_data.mem_size_ = nv12_ion_buffer.size;

  BufferData dst_data;
  dst_data.vir_addr_ = h264_ion_buffer.buffer;
  dst_data.ion_data_.fd_ = h264_ion_buffer.fd;
  dst_data.ion_data_.handle_ = h264_ion_buffer.handle;
  dst_data.mem_size_ = h264_ion_buffer.size;

  VideoConfig& vconfig = h264_config.video_config;
  vconfig.width = w;
  vconfig.height = h;
  vconfig.fmt = PIX_FMT_NV12;
  vconfig.bit_rate = w * h * 7;
  if (vconfig.bit_rate > 1000000) {
    vconfig.bit_rate /= 1000000;
    vconfig.bit_rate *= 1000000;
  }
  vconfig.frame_rate = 30;
  vconfig.level = 51;
  vconfig.gop_size = vconfig.frame_rate;
  vconfig.profile = 100;
  vconfig.quality = MPP_ENC_RC_QUALITY_BEST;
  vconfig.qp_init = 26;
  vconfig.qp_step = 8;
  vconfig.qp_min = 4;
  vconfig.qp_max = 48;
  vconfig.rc_mode = MPP_ENC_RC_MODE_CBR;
  MPPH264Encoder *encoder = new MPPH264Encoder();
  //init H264Encoder
  if (encoder->InitConfig(h264_config))
    assert(0);

  int read_size = 0, write_size = 0;
  int nv12_file_fd = open("/mnt/sdcard/test_320_240.nv12", O_RDONLY);
  assert(nv12_file_fd >= 0);
  int h264_file_fd =
      open("/mnt/sdcard/test_320_240.h264", O_WRONLY | O_CREAT | O_TRUNC);
  assert(h264_file_fd);
  // write spspps
  void* h264_extra_data = nullptr;
  size_t h264_extra_data_size = 0;
  encoder->GetExtraData(h264_extra_data, h264_extra_data_size);
  write_size = write(h264_file_fd, h264_extra_data, h264_extra_data_size);
  assert(write_size == h264_extra_data_size);
  while ((read_size = read(nv12_file_fd, nv12_ion_buffer.buffer, frame_size)) >
         0) {
    assert(read_size == frame_size);
    Buffer input_buf(input_data);
    Buffer dst_buf(dst_data);
    int encode_ret = encoder->EncodeOneFrame(&input_buf, &dst_buf, nullptr);
    assert(!encode_ret);
    assert(dst_buf.GetValidDataSize() > 0);
    // hw encoder on 1108 only output I/P frame type
    if (dst_buf.GetUserFlag() & MPP_PACKET_FLAG_INTRA)
      printf("Got I frame\n");
    else
      printf("Got P frame\n");
    write_size = write(h264_file_fd, h264_ion_buffer.buffer, dst_buf.GetValidDataSize());
    assert(write_size == dst_buf.GetValidDataSize());
  }

  close(nv12_file_fd);
  close(h264_file_fd);
  encoder->unref();
  video_ion_free(&nv12_ion_buffer);
  video_ion_free(&h264_ion_buffer);

  return 0;
}
