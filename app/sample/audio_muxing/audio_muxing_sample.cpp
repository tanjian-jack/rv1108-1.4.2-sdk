extern "C" {
#include <alsa/asoundlib.h>
#include "../../video/alsa_capture.h"
const char* parameter_get_audio_mux_format();
}

#include "../../video/av_wrapper/encoder_muxing/encoder/base_encoder.cpp"
#include "../../video/av_wrapper/encoder_muxing/encoder/ffmpeg_wrapper/ff_audio_encoder.cpp"
#include "../../video/av_wrapper/encoder_muxing/encoder/ffmpeg_wrapper/ff_base_encoder.cpp"
#include "../../video/av_wrapper/encoder_muxing/encoder/ffmpeg_wrapper/ff_context.cpp"
#include "../../video/av_wrapper/video_common.cpp"

// pc:
//.....cd <project root path>/app/sample/audio_muxing_sample
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-gcc -c \
       -I../../video \
       -I../../video/av_wrapper \
       -I../../../out/system/include \
       -ffunction-sections -fdata-sections \
       -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -g \
       audio_capture.c -o audio_capture.o
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ -c \
       -I../../video \
       -I../../video/av_wrapper \
       -I../../../out/system/include \
       -ffunction-sections -fdata-sections \
       -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -std=c++11 -g \
       audio_muxing_sample.cpp -o audio_muxing_sample.o
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ \
      -L../../../out/system/lib -Wl,--gc-sections \
      -o audio_muxing_sample audio_muxing_sample.o \
      -lswresample -lavformat -lavcodec -lavutil \
      audio_capture.o -lsalsa \
      -lfdk-aac -lmpp -lpthread
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-strip \
       audio_muxing_sample
//     cp audio_muxing_sample <sdcard>

// board:
//        mount /dev/mmcblk0p1 /mnt/sdcard/
//        cd /mnt/sdcard
//        ./audio_play_muxing test.xxx

#define WRITE_TO_BUFFER 1

static volatile bool capture = true;

void* timer(void*) {
  sleep(3);
  capture = false;
}

static int write_frame(AVFormatContext* fmt_ctx,
                       const AVRational* time_base,
                       AVStream* st,
                       AVPacket* pkt) {
  /* rescale output packet timestamp values from codec to stream timebase */
  av_packet_rescale_ts(pkt, *time_base, st->time_base);
  pkt->stream_index = st->index;

  /* Write the compressed frame to the media file. */
  return av_write_frame(fmt_ctx, pkt);
}

static int write_packet(AVFormatContext* ctx, AVStream* st, AVPacket* pkt) {
  AVCodecContext* c = st->codec;
  int ret = write_frame(ctx, &c->time_base, st, pkt);
  if (ret < 0) {
    char str[AV_ERROR_MAX_STRING_SIZE] = {0};
    av_strerror(ret, str, AV_ERROR_MAX_STRING_SIZE);
    fprintf(stderr, "Error while writing frame: %s\n", str);
  }
  return ret;
}

extern "C" {
typedef struct DynBuffer {
  int pos, size, allocated_size;
  uint8_t* buffer;
  int io_buffer_size;
  uint8_t io_buffer[1];
} DynBuffer;
}

static void dump_dynbuffer(DynBuffer* d) {
  printf(
      "DynBuffer; pos, size, allocated_size, buffer, io_buffer_size: "
      "%d, %d, %d, %p, %d\n",
      d->pos, d->size, d->allocated_size, d->buffer, d->io_buffer_size);
}

static int flush_dyn_buf(AVIOContext* s, uint8_t** pbuffer) {
  DynBuffer* d;
  int size;
  static const unsigned char padbuf[AV_INPUT_BUFFER_PADDING_SIZE] = {0};
  int padding = 0;

  if (!s) {
    *pbuffer = NULL;
    return 0;
  }
  avio_flush(s);
  d = (DynBuffer*)s->opaque;
  *pbuffer = d->buffer;
  size = d->size;
  d->size = 0;
  return size - padding;
}

static void write_dyn_buf(int fd, AVIOContext* s) {
  uint8_t* pbuffer = nullptr;
  int written_size = flush_dyn_buf(s, &pbuffer);
  if (written_size > 0) {
    // printf("write size: %d\n", written_size);
    write(fd, pbuffer, written_size);
    avio_seek(s, 0, SEEK_SET);
  }
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "missing out file!\n");
    return -1;
  }
  char out_file[64];
  char* file_path = argv[1];
  int i = sprintf(out_file, "%s", file_path);
  int len = i;
  while (i-- > 0) {
    if (out_file[i] == '.') {
      i++;
      break;
    }
  }
  if (i < 0)
    i = len;

  const char* mux_format = parameter_get_audio_mux_format();
  sprintf(out_file + i, "%s", mux_format);

  pthread_t pid = 0;

  int ret = StartThread(pid, timer, nullptr);
  assert(ret >= 0);

  struct alsa_capture cap = {0};
  MediaConfig mconfig;
  AudioConfig& aconfig = mconfig.audio_config;
  FFAudioEncoder* aencoder = new FFAudioEncoder();
  assert(aencoder);

  cap.sample_rate = 48000;
  cap.format = SND_PCM_FORMAT_S32_LE;  // libfdk-aac only support s16le
  cap.dev_id = 0;

  ret = alsa_capture_open(&cap, 0);
  assert(!ret);
  switch (cap.format) {
    case SND_PCM_FORMAT_S16_LE:
      aconfig.fmt = SAMPLE_FMT_S16;
      printf("shit1 : %d\n", cap.format);
      break;
    case SND_PCM_FORMAT_S32_LE:
      aconfig.fmt = SAMPLE_FMT_S32;
      break;
    default:
      printf("TODO: unsupport snd format : %d\n", cap.format);
      assert(0);
  }
  aconfig.bit_rate = 48000;
  aconfig.nb_samples = 1024;
  aconfig.sample_rate = cap.sample_rate;
  aconfig.nb_channels = cap.channel;
  assert(cap.channel == 1 || cap.channel == 2);
  aconfig.channel_layout = av_get_default_channel_layout(cap.channel);

  mconfig.audio_config = aconfig;
  ret = aencoder->InitConfig(mconfig);
  assert(!ret);

  FFContext* ffctx = (FFContext*)aencoder->GetHelpContext();
  AVFormatContext* oc = ffctx->GetAVFmtContext();
  oc->oformat = av_guess_format(mux_format, NULL, NULL);
  assert(oc->oformat);
  if (oc->oformat->priv_data_size > 0) {
    oc->priv_data = av_mallocz(oc->oformat->priv_data_size);
    assert(oc->priv_data);
    if (oc->oformat->priv_class) {
      *(const AVClass**)oc->priv_data = oc->oformat->priv_class;
      av_opt_set_defaults(oc->priv_data);
    }
  } else
    oc->priv_data = NULL;

#if WRITE_TO_BUFFER
  int out_fd = creat(out_file, 777);
  assert(out_fd > 0);
  ret = avio_open_dyn_buf(&oc->pb);
  assert(ret >= 0);
#else
  ret = avio_open(&oc->pb, out_file, AVIO_FLAG_WRITE);
  assert(ret >= 0);
#endif

  ret = avformat_write_header(oc, NULL);
  assert(ret >= 0);
#if WRITE_TO_BUFFER
  write_dyn_buf(out_fd, oc->pb);
#endif

  uint64_t audio_idx = 0;
  while (capture) {
    void* audio_buf = nullptr;
    int nb_samples = 0;
    int channels = 0;
    enum AVSampleFormat fmt;
    aencoder->GetAudioBuffer(&audio_buf, &nb_samples, &channels, &fmt);
    assert(audio_buf && nb_samples * channels > 0);
    cap.buffer_frames = nb_samples;
    int capture_samples = alsa_capture_doing(&cap, audio_buf);
    assert(capture_samples > 0);
    audio_idx++;
    AVPacket av_pkt;
    av_init_packet(&av_pkt);
    aencoder->EncodeAudioFrame(&av_pkt);
    av_pkt.pts = audio_idx * aconfig.nb_samples;
    av_pkt.dts = av_pkt.pts;
    write_packet(oc, ffctx->GetAVStream(), &av_pkt);
#if WRITE_TO_BUFFER
    write_dyn_buf(out_fd, oc->pb);
#endif
    av_packet_unref(&av_pkt);
  }

  alsa_capture_done(&cap);

  ret = av_write_trailer(oc);
  assert(ret >= 0);
#if WRITE_TO_BUFFER
  write_dyn_buf(out_fd, oc->pb);
  close(out_fd);
  uint8_t* pbuffer = nullptr;
  avio_close_dyn_buf(oc->pb, &pbuffer);
  av_free(pbuffer);
#else
  avio_closep(&oc->pb);
#endif
  StopThread(pid);
  aencoder->unref();
  return 0;
}
