// RK's AEC_AGC_ANR_ALGORITHM only support 8k/16k 16bits. Disable the following
// macro if you need other pcm configure.

// #define AEC_AGC_ANR_ALGORITHM

#define _GNU_SOURCE 1
#include "../../video/alsa_capture.c"

// Set the encoder.
const char* parameter_get_audio_enc_format() {
  return "aac";
}

// Set the muxer.
const char* parameter_get_audio_mux_format() {
  return "adts";  // add "--enable-muxer=adts" in external/ffmpeg/ffmpeg.rvmk
}

#include <sys/types.h>
#include <sys/stat.h>
// Do not need real fsmanage, replace them by linux API
int64_t fs_manage_lseek(int fd, int64_t offset, int whence) {
  return lseek(fd, offset, whence);
}
ssize_t fs_manage_read(int fd, void* buf, size_t count) {
  return read(fd, buf, count);
}
ssize_t fs_manage_write(int fd, const void* buf, size_t count) {
  return write(fd, buf, count);
}
int fs_manage_open(const char* filename, int flags, ...) {
  unsigned int mode = 0;
  va_list ap;

  va_start(ap, flags);
  if (flags & O_CREAT)
    mode = va_arg(ap, unsigned int);
  va_end(ap);

  flags |= O_CLOEXEC;
  return open(filename, flags, mode);
}
int fs_manage_close(int fd) {
  return close(fd);
}
int fs_manage_writetrail(int fd) {
  return 0;
}
int fs_manage_get_tailoff(int fd) {
  return 0;
}
int fs_manage_fstat(int fd, struct stat* filestat) {
  return fstat(fd, filestat);
}
