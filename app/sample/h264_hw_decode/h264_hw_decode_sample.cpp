#include <assert.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern "C" {
#include <cvr_ffmpeg_shared.h>
#include <libavcodec/avcodec.h>
#include <mpp/vpu.h>
#include <mpp/vpu_api.h>
#include <rk_fb/rk_fb.h>
#include <rk_rga/rk_rga.h>
}

// pc:
//.....cd <project root path>/app/sample/h264_hw_decode
//     cp *.h264_frame<sdcard>
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ \
      -I../../video \
      -I../../../out/system/include \
      -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -std=c++11 \
      -fstack-protector-all  -g \
      -L../../../out/system/lib \
      -o h264_hw_decode_sample h264_hw_decode_sample.cpp \
      -lavformat -lavcodec -lavutil -lfdk-aac -lpthread -lion -lmpp -lrkrga \
      -lrkfb
//     cp h264_hw_decode_sample <sdcard>

// board:
//        mount /dev/mmcblk0p1 /mnt/sdcard/
//        cd /mnt/sdcard
//        ./h264_hw_decode_sample

#define DECODE_THROUGH_FFMPEG 1

#if DECODE_THROUGH_FFMPEG

#define VIDEO_DISPLAY_ROTATE_ANGLE 90

#define COLOR_KEY_R 0
#define COLOR_KEY_G 0
#define COLOR_KEY_B 1

static void ui_fill_colorkey(void) {
  struct win* ui_win;
  struct color_key color_key;
  unsigned short rgb565_data;
  unsigned short* ui_buff;
  int i;
  int w, h;

  ui_win = rk_fb_getuiwin();
  ui_buff = (unsigned short*)ui_win->buffer;

  /* enable and set color key */
  color_key.enable = 1;
  color_key.red = (COLOR_KEY_R & 0x1f) << 3;
  color_key.green = (COLOR_KEY_G & 0x3f) << 2;
  color_key.blue = (COLOR_KEY_B & 0x1f) << 3;
  rk_fb_set_color_key(color_key);

  rk_fb_get_out_device(&w, &h);

  /* set ui win color key */
  rgb565_data = (COLOR_KEY_R & 0x1f) << 11 | ((COLOR_KEY_G & 0x3f) << 5) |
                (COLOR_KEY_B & 0x1f);
  for (i = 0; i < w * h; i++) {
    ui_buff[i] = rgb565_data;
  }
}

static void display(int rga_fd,
                    int fd,
                    int w,
                    int h,
                    int stride_w,
                    int stride_h,
                    enum AVPixelFormat fmt) {
  int ret;
  int out_w, out_h;
  int outdevice;
  struct win* win = NULL;
  int rotate_angle = 0;
  int src_fmt = RGA_FORMAT_YCBCR_420_SP;
  switch (fmt) {
    case AV_PIX_FMT_NV12:
      src_fmt = RGA_FORMAT_YCBCR_420_SP;
      break;
    case AV_PIX_FMT_NV16:
      src_fmt = RGA_FORMAT_YCBCR_422_SP;
      break;
    default:
      printf("%s, unsupport pixel fmt : %d\n", __FILE__, fmt);
      break;
  }

  win = rk_fb_getvideowin();
  if (!win) {
    printf("!rk_fb_getvideowin return NULL\n");
  }

  outdevice = rk_fb_get_out_device(&out_w, &out_h);
  printf("display w,h,stridew,strideh [%d, %d, %d, %d]; out w,h: [%d, %d]\n", w,
         h, stride_w, stride_h, out_w, out_h);
  rotate_angle =
      (outdevice == OUT_DEVICE_HDMI ? 0 : VIDEO_DISPLAY_ROTATE_ANGLE);

  ret = rk_rga_ionfd_to_ionfd_rotate(rga_fd, fd, w, h, src_fmt, stride_w,
                                     stride_h, win->video_ion.fd, out_w, out_h,
                                     RGA_FORMAT_YCBCR_420_SP, rotate_angle);
  printf("rk_rga_ionfd_to_ionfd_rotate ret : %d\n", ret);
  if ((0 == ret) && (rk_fb_video_disp(win) == -1))
    printf("rk_fb_video_disp err\n");
}

static int decode_frame(AVCodecContext* codecContext,
                        AVPacket* pkt,
                        AVFrame* frame) {
  int got_frame = 0;
  while (pkt->size > 0) {
    got_frame = 0;
    int result = avcodec_decode_video2(codecContext, frame, &got_frame, pkt);
    if (result < 0) {
      char errbuf[128];
      av_strerror(result, errbuf, sizeof(errbuf));
      fprintf(stderr, "avcodec_decode_video2 error: %s", errbuf);
      break;
    }
    printf("decoded %d bytes\n", result);
    pkt->size -= result;
    pkt->data += result;

    if (got_frame) {
      printf("Got frame\n");
    }
  }

  return got_frame;
}

int main() {
  AVCodec* codec = nullptr;
  AVCodecContext* codecContext = nullptr;
  // in this sample, the resolution of test frame is 320x240 / nv12
  uint8_t h264_data[320 * 240 * 3 / 2];

  // 1. init ffmpeg avcodec.
  avcodec_register_all();

  // ffmpeg should --enable-decoder=h264_mpp for h264 decoding without other
  // decoders.
  codec = avcodec_find_decoder(AV_CODEC_ID_H264);
  if (!codec) {
    fprintf(stderr, "avcodec_find_decoder failed\n");
    return -1;
  }

  codecContext = avcodec_alloc_context3(codec);
  if (!codecContext) {
    fprintf(stderr, "avcodec_alloc_context3 failed\n");
    return -1;
  }

  // set spspps
  int fd = open("/mnt/sdcard/0.h264_frame", O_RDONLY);
  if (fd < 0) {
    printf("missing /mnt/sdcard/0.h264_frame\n");
    return -1;
  }
  int size = read(fd, h264_data, sizeof(h264_data));
  assert(size > 0);
  close(fd);
  uint8_t* sps = (uint8_t*)malloc(size);
  assert(sps);
  memcpy(sps, h264_data, size);
  codecContext->extradata = sps;
  codecContext->extradata_size = size;

  // Open codec
  if (avcodec_open2(codecContext, codec, NULL) < 0) {
    fprintf(stderr, "avcodec_open2 failed\n");
    return -1;
  }

  // 2. init fb. Donot run the app/video when runing this sample.
  rk_fb_init(FB_FORMAT_RGB_565);
  ui_fill_colorkey();

  // 3. rga do the copy, scale and rotation
  int rga_fd = rk_rga_open();
  if (rga_fd < 0) {
    printf("rk_rga_open failed\n");
    return -1;
  }

  // 4. read the frame from file and decode it.
  DataBuffer_VPU_t vpu_data_buf;
  image_handle image_buf = &vpu_data_buf;

  int frame_num = 0;
  char frame_file_path[64];
  do {
    memset(&vpu_data_buf, 0, sizeof(vpu_data_buf));
    // 4.1 read frame data
    sprintf(frame_file_path, "/mnt/sdcard/%d.h264_frame", frame_num++);
    int file_fd = open(frame_file_path, O_RDONLY);
    if (file_fd < 0)
      break;
    int read_size = read(file_fd, h264_data, sizeof(h264_data));
    if (read_size <= 0) {
      printf("read failed, errno : %d\n", errno);
      close(file_fd);
      break;
    }
    printf("\nread size: %d\n", read_size);
    AVPacket packet;
    av_init_packet(&packet);
    packet.data = h264_data;
    packet.size = read_size;

    AVFrame* out_frame = av_frame_alloc();
    if (!out_frame) {
      close(file_fd);
      break;
    }
    // out_frame->format = video_dec_ctx->pix_fmt;
    // out_frame->width = info.width;
    // out_frame->height = info.height;
    // out_frame->linesize[0] = out_frame->linesize[1] = info.width;
    // out_frame->linesize[2] = 0;
    out_frame->user_flags |= VPU_MEM_ALLOC_EXTERNAL;
    out_frame->data[0] = out_frame->data[1] = out_frame->data[2] = 0;
    out_frame->data[3] = (uint8_t*)image_buf;

    // 4.2 decode the frame
    int got_frame = decode_frame(codecContext, &packet, out_frame);
    if (got_frame) {
      // 4.3 display the image
      display(rga_fd, vpu_data_buf.image_buf.phy_fd, out_frame->width,
              out_frame->height, codecContext->coded_width,
              codecContext->coded_height, codecContext->pix_fmt);
      vpu_data_buf.free_func(vpu_data_buf.rkdec_ctx, &vpu_data_buf.image_buf);
    }

    av_frame_free(&out_frame);
    close(file_fd);
  } while (true);

  rk_rga_close(rga_fd);
  avcodec_free_context(&codecContext);
  // rk_fb_deinit();
  return 0;
}
#endif
