#include "../../video/video_ion_alloc.c"  // for alloc ion memory
#include "mpp/mpp_log.h"
#include "mpp/rk_mpi.h"

#include <sys/time.h>

struct vpu_decode {
  int in_width;
  int in_height;
  MppCtx mpp_ctx;
  MppApi* mpi;
  MppFrameFormat fmt;
};

int vpu_decode_jpeg_init(struct vpu_decode* decode, int width, int height) {
  int ret;
  decode->in_width = width;
  decode->in_height = height;

  ret = mpp_create(&decode->mpp_ctx, &decode->mpi);
  if (MPP_OK != ret) {
    mpp_err("mpp_create failed\n");
    return -1;
  }

  ret = mpp_init(decode->mpp_ctx, MPP_CTX_DEC, MPP_VIDEO_CodingMJPEG);
  if (MPP_OK != ret) {
    mpp_err("mpp_init failed\n");
    return -1;
  }

  MppApi* mpi = decode->mpi;
  MppCtx mpp_ctx = decode->mpp_ctx;
  MppFrame frame;
  ret = mpp_frame_init(&frame);
  if (!frame || (MPP_OK != ret)) {
    mpp_err("failed to init mpp frame!");
    return MPP_ERR_NOMEM;
  }

  mpp_frame_set_fmt(frame, MPP_FMT_YUV420SP);
  mpp_frame_set_width(frame, decode->in_width);
  mpp_frame_set_height(frame, decode->in_height);
  mpp_frame_set_hor_stride(frame, ALIGN(decode->in_width, 16));
  mpp_frame_set_ver_stride(frame, ALIGN(decode->in_height, 16));

  ret = mpi->control(mpp_ctx, MPP_DEC_SET_FRAME_INFO, (MppParam)frame);
  mpp_frame_deinit(&frame);

  return 0;
}

int vpu_decode_jpeg_doing(struct vpu_decode* decode,
                          int in_fd,
                          char* in_data,
                          RK_S32 in_size,
                          int out_fd,
                          char* out_data,
                          RK_S32 out_size) {
  MPP_RET ret = MPP_OK;
  MppTask task = NULL;

  /* try import input buffer and output buffer */
  RK_U32 width = decode->in_width;
  RK_U32 height = decode->in_height;
  MppFrame frame = NULL;
  MppPacket packet = NULL;
  MppBuffer str_buf = NULL; /* input */
  MppBuffer pic_buf = NULL; /* output */
  MppCtx mpp_ctx = decode->mpp_ctx;
  MppApi* mpi = decode->mpi;

  mpp_assert(in_fd > 0);
  mpp_assert(out_fd > 0);
  ret = mpp_frame_init(&frame);
  if (MPP_OK != ret) {
    mpp_err_f("mpp_frame_init failed\n");
    goto DECODE_OUT;
  }

  MppBufferInfo inputCommit;
  memset(&inputCommit, 0, sizeof(inputCommit));
  inputCommit.type = MPP_BUFFER_TYPE_ION;
  inputCommit.fd = in_fd;
  inputCommit.size = in_size;
  inputCommit.ptr = (void*)in_data;

  ret = mpp_buffer_import(&str_buf, &inputCommit);
  if (ret) {
    mpp_err_f("import input stream buffer failed\n");
    goto DECODE_OUT;
  }

  MppBufferInfo outputCommit;
  memset(&outputCommit, 0, sizeof(outputCommit));
  outputCommit.type = MPP_BUFFER_TYPE_ION;
  outputCommit.fd = out_fd;
  outputCommit.size = out_size;
  outputCommit.ptr = (void*)out_data;

  ret = mpp_buffer_import(&pic_buf, &outputCommit);
  if (ret) {
    mpp_err_f("import output stream buffer failed\n");
    goto DECODE_OUT;
  }

  mpp_packet_init_with_buffer(&packet, str_buf); /* input */
  mpp_frame_set_buffer(frame, pic_buf);          /* output */

  ret = mpi->poll(mpp_ctx, MPP_PORT_INPUT, MPP_POLL_BLOCK);
  if (ret) {
    mpp_err("mpp input poll failed\n");
    goto DECODE_OUT;
  }

  ret = mpi->dequeue(mpp_ctx, MPP_PORT_INPUT, &task); /* input queue */
  if (ret) {
    mpp_err("mpp task input dequeue failed\n");
    goto DECODE_OUT;
  }

  mpp_assert(task);

  mpp_task_meta_set_packet(task, KEY_INPUT_PACKET, packet);
  mpp_task_meta_set_frame(task, KEY_OUTPUT_FRAME, frame);

  ret = mpi->enqueue(mpp_ctx, MPP_PORT_INPUT, task); /* input queue */
  if (ret) {
    mpp_err("mpp task input enqueue failed\n");
    goto DECODE_OUT;
  }

  /* poll and wait here */
  ret = mpi->poll(mpp_ctx, MPP_PORT_OUTPUT, MPP_POLL_BLOCK);
  if (ret) {
    mpp_err("mpp output poll failed\n");
    goto DECODE_OUT;
  }

  ret = mpi->dequeue(mpp_ctx, MPP_PORT_OUTPUT, &task); /* output queue */
  if (ret) {
    mpp_err("mpp task output dequeue failed\n");
    goto DECODE_OUT;
  }

  mpp_assert(task);

  if (task) {
    RK_U32 err_info = 0;
    MppFrame frame_out = NULL;
    decode->fmt = MPP_FMT_YUV420SP;

    mpp_task_meta_get_frame(task, KEY_OUTPUT_FRAME, &frame_out);
    mpp_assert(frame_out == frame);

    err_info = mpp_frame_get_errinfo(frame_out);
    if (!err_info) {
      decode->fmt = mpp_frame_get_fmt(frame_out);
      if (MPP_FMT_YUV422SP != decode->fmt && MPP_FMT_YUV420SP != decode->fmt)
        printf("No support USB JPEG decode format!\n");
    }

    ret = mpi->enqueue(mpp_ctx, MPP_PORT_OUTPUT, task);
    if (ret) {
      mpp_err("mpp task output enqueue failed\n");
      goto DECODE_OUT;
    }
    task = NULL;

    if (err_info)
      ret = MPP_NOK;
  }

DECODE_OUT:
  if (str_buf) {
    mpp_buffer_put(str_buf);
    str_buf = NULL;
  }

  if (pic_buf) {
    mpp_buffer_put(pic_buf);
    pic_buf = NULL;
  }

  if (frame)
    mpp_frame_deinit(&frame);

  if (packet)
    mpp_packet_deinit(&packet);

  return ret;
}

int vpu_decode_jpeg_done(struct vpu_decode* decode) {
  MPP_RET ret = MPP_OK;
  ret = mpp_destroy(decode->mpp_ctx);
  if (ret != MPP_OK) {
    mpp_err("something wrong with mpp_destroy! ret:%d\n", ret);
  }
  return ret;
}

inline int64_t difftime(struct timeval* start, struct timeval* end) {
  return (end->tv_sec - start->tv_sec) * 1000000LL +
         (end->tv_usec - start->tv_usec);
}

int main() {
  int w = 1920, h = 1080;
  int read_size = 0, write_size = 0;
  char path[64] = {0};
  struct timeval t1, t2;
  int64_t total_time = 0;
  // prepare ion buffer
  struct video_ion nv12_ion_buffer, jpeg_ion_buffer;
  memset(&nv12_ion_buffer, 0, sizeof(nv12_ion_buffer));
  memset(&jpeg_ion_buffer, 0, sizeof(jpeg_ion_buffer));
  nv12_ion_buffer.client = -1;
  nv12_ion_buffer.fd = -1;
  jpeg_ion_buffer.client = -1;
  jpeg_ion_buffer.fd = -1;
  if (video_ion_alloc_rational(&nv12_ion_buffer, w, h, 2, 1) == -1)
    assert(0);
  if (video_ion_alloc_rational(&jpeg_ion_buffer, w, h, 3, 2) == -1)
    assert(0);

  snprintf(path, sizeof(path), "/mnt/sdcard/test.jpg");
  int jpg_file_fd = open(path, O_RDONLY);
  assert(jpg_file_fd >= 0);
  read_size = read(jpg_file_fd, jpeg_ion_buffer.buffer, jpeg_ion_buffer.size);
  close(jpg_file_fd);
  assert(read_size > 0);
  jpeg_ion_buffer.size = read_size;

  int i = 0;
  struct vpu_decode decode;
  memset(&decode, 0, sizeof(decode));
  int ret = vpu_decode_jpeg_init(&decode, w, h);
  assert(!ret);
  while (i++ < 1) {
    int64_t cur_consume = 0;
    gettimeofday(&t1, NULL);
    ret = vpu_decode_jpeg_doing(&decode, jpeg_ion_buffer.fd,
                                jpeg_ion_buffer.buffer, jpeg_ion_buffer.size,
                                nv12_ion_buffer.fd, nv12_ion_buffer.buffer,
                                nv12_ion_buffer.size);
    gettimeofday(&t2, NULL);
    cur_consume = difftime(&t1, &t2);
    total_time += cur_consume;
    printf("decode jpeg consume : %lld us, avg consume: %lld us\n", cur_consume,
           total_time / i);
    assert(!ret);
  }

  snprintf(path, sizeof(path), "/mnt/sdcard/test_%d_%d.nv12", w, h);
  int nv12_file_fd = open(path, O_WRONLY | O_CREAT | O_TRUNC);
  assert(nv12_file_fd >= 0);
  uint8_t* out_buffer = (uint8_t*)nv12_ion_buffer.buffer;
  write_size =
      write(nv12_file_fd, out_buffer, ALIGN(w,16) * h);
  write_size =
      write(nv12_file_fd, out_buffer + ALIGN(w,16) * ALIGN(h, 16), ALIGN(w,16) * h / 2);
  close(nv12_file_fd);

  vpu_decode_jpeg_done(&decode);
  video_ion_free(&nv12_ion_buffer);
  video_ion_free(&jpeg_ion_buffer);
  return 0;
}
