extern "C" {
#include <rk_fb/rk_fb.h>
#include <rk_rga/rk_rga.h>
}

#include "../../video/av_wrapper/decoder_demuxing/decoder_demuxing.cpp"

// pc:
//.....cd <project root path>/app/sample/ff_demux_decode
//     cp test_320_240.h264 <sdcard>
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ \
      -I../../video \
      -I../../video/av_wrapper \
      -I../../../out/system/include \
      -fPIC -O2 -g2 -DDEBUG -fstack -protector -all -std=c++11 \
      -fstack-protector-all -g \
      -L../../../out/system/lib \
      -o ff_demux_decode_sample ff_demux_decode_sample.cpp \
      -lavformat -lavcodec -lavutil -lfdk-aac -lfsmanage \
      -lpthread -lion -lmpp -lrkrga -lrkfb
//     cp ff_demux_decode <sdcard>

// board:
//        mount /dev/mmcblk0p1 /mnt/sdcard/
//        cd /mnt/sdcard
//        ./ff_demux_decode_sample

// mpp is the framework of hw decoding
#define MPP_ENABLE 1

#if !MPP_ENABLE
extern "C" {
#include "../../video/video_ion_alloc.c"  // for alloc ion memory
}
#endif

#define VIDEO_DISPLAY_ROTATE_ANGLE 90

#define COLOR_KEY_R 0
#define COLOR_KEY_G 0
#define COLOR_KEY_B 1

static void ui_fill_colorkey(void) {
  struct win* ui_win;
  struct color_key color_key;
  unsigned short rgb565_data;
  unsigned short* ui_buff;
  int i;
  int w, h;

  ui_win = rk_fb_getuiwin();
  ui_buff = (unsigned short*)ui_win->buffer;

  /* enable and set color key */
  color_key.enable = 1;
  color_key.red = (COLOR_KEY_R & 0x1f) << 3;
  color_key.green = (COLOR_KEY_G & 0x3f) << 2;
  color_key.blue = (COLOR_KEY_B & 0x1f) << 3;
  rk_fb_set_color_key(color_key);

  rk_fb_get_out_device(&w, &h);

  /* set ui win color key */
  rgb565_data = (COLOR_KEY_R & 0x1f) << 11 | ((COLOR_KEY_G & 0x3f) << 5) |
                (COLOR_KEY_B & 0x1f);
  for (i = 0; i < w * h; i++) {
    ui_buff[i] = rgb565_data;
  }
}

static void display(int rga_fd,
                    int fd,
                    int w,
                    int h,
                    int stride_w,
                    int stride_h,
                    enum AVPixelFormat fmt) {
  int ret;
  int out_w, out_h;
  int outdevice;
  struct win* win = NULL;
  int rotate_angle = 0;
  int src_fmt = RGA_FORMAT_YCBCR_420_SP;
  switch (fmt) {
    case AV_PIX_FMT_NV12:
      src_fmt = RGA_FORMAT_YCBCR_420_SP;
      break;
    case AV_PIX_FMT_NV16:
      src_fmt = RGA_FORMAT_YCBCR_422_SP;
      break;
    case AV_PIX_FMT_YUV420P:
      src_fmt = RGA_FORMAT_YCBCR_420_P;
      break;
    default:
      printf("%s, unsupport pixel fmt : %d\n", __FILE__, fmt);
      break;
  }

  win = rk_fb_getvideowin();
  if (!win) {
    printf("!rk_fb_getvideowin return NULL\n");
  }

  outdevice = rk_fb_get_out_device(&out_w, &out_h);
  printf("display w,h,stridew,strideh [%d, %d, %d, %d]; out w,h: [%d, %d]\n", w,
         h, stride_w, stride_h, out_w, out_h);
  rotate_angle =
      (outdevice == OUT_DEVICE_HDMI ? 0 : VIDEO_DISPLAY_ROTATE_ANGLE);

  ret = rk_rga_ionfd_to_ionfd_rotate(rga_fd, fd, w, h, src_fmt, stride_w,
                                     stride_h, win->video_ion.fd, out_w, out_h,
                                     RGA_FORMAT_YCBCR_420_SP, rotate_angle);
  printf("rk_rga_ionfd_to_ionfd_rotate ret : %d\n", ret);
  if ((0 == ret) && (rk_fb_video_disp(win) == -1))
    printf("rk_fb_video_disp err\n");
}

int main() {
#if MPP_ENABLE
  // Be sure you have add the proper demuxer configure when compile ffmpeg,
  // for example, add "--enable-demuxer=h264" in external/ffmpeg/ffmpeg.rvmk for
  // demuxing the following h264 file.
  static char filepath[] = "/mnt/sdcard/test_320_240.h264";
#else
  static char filepath[] = "/mnt/sdcard/VGA_yuv420p.mp4";
#endif
  int w = 0, h = 0;
  int coded_w = 0, coded_h = 0;
  enum AVPixelFormat pix_fmt = AV_PIX_FMT_NV12;
  DataBuffer_VPU_t vpu_data_buf;
  void* ctx = nullptr;
  // 1. init fb. Donot run the app/video when runing this sample.
  rk_fb_init(FB_FORMAT_RGB_565);
  ui_fill_colorkey();
  // 2. init rga fd, rga do the copy, scale and rotation.
  // 1 and 2 is not necessary if you only need demux-decode.
  int rga_fd = rk_rga_open();
  if (rga_fd < 0) {
    printf("rk_rga_open failed\n");
    return -1;
  }

  // 3. demux and decode
  int ret = decode_init_context(&ctx, filepath);
  assert(!ret);

#if !MPP_ENABLE
  DecoderDemuxer* dd = (DecoderDemuxer*)ctx;
  printf("dd->get_media_info().out_sample_fmt: %d\n",
         dd->get_media_info().out_sample_fmt);
  // assert(dd->get_media_info().out_sample_fmt == AV_PIX_FMT_YUV420P);
  struct video_ion nv12_ion_buffer = {};
  nv12_ion_buffer.client = -1;
  nv12_ion_buffer.fd = -1;
  if (video_ion_alloc_rational(&nv12_ion_buffer, dd->get_media_info().width,
                               dd->get_media_info().height, 3, 2) == -1)
    assert(0);
  int64_t total_consume = 0;
  int frame_num = 0;
#endif

  do {
#if MPP_ENABLE
    memset(&vpu_data_buf, 0, sizeof(vpu_data_buf));
    ret = decode_one_video_frame(ctx, &vpu_data_buf, &w, &h, &coded_w, &coded_h,
                                 &pix_fmt);
    if (!ret) {
      // 4. display
      display(rga_fd, vpu_data_buf.image_buf.phy_fd, w, h, coded_w, coded_h,
              pix_fmt);
      vpu_data_buf.free_func(vpu_data_buf.rkdec_ctx, &vpu_data_buf.image_buf);
    }
#else
    // software decode
    AVFrame* frame = av_frame_alloc();
    if (frame) {
      int got_frame = 0;
      int is_video = 0;
      struct timeval start, end;
      dd->set_audio_disable();
      while (!got_frame) {
        gettimeofday(&start, NULL);
        ret = dd->read_and_decode_one_frame(NULL, frame, &got_frame, &is_video);
        if (dd->is_finished()) {
          ret = -1;
          break;
        }
      }
      if (got_frame && is_video) {
        gettimeofday(&end, NULL);
        int64_t diff_time = difftime(start, end);
        total_consume += diff_time;
        frame_num++;
        printf("cur frame[%d], consume <%lld> ms, average <%lld> ms\n",
               frame_num, diff_time / 1000LL,
               total_consume / frame_num / 1000LL);
        w = frame->width;
        h = frame->height;
        coded_w = dd->get_video_coded_width();
        coded_h = dd->get_video_coded_height();
        pix_fmt = dd->get_media_info().pixel_fmt;
        ret = 0;
#if 1
        uint8_t* p = (uint8_t*)nv12_ion_buffer.buffer;
        memcpy(p, frame->data[0], frame->linesize[0] * h);
        p += (frame->linesize[0] * h);
        // printf("frame->linesize[]: %d, %d, %d, %d\n",
        //       frame->linesize[0], frame->linesize[1],
        //       frame->linesize[2], frame->linesize[3]);
        memcpy(p, frame->data[1], frame->linesize[1] * h / 2);
        p += (frame->linesize[1] * h / 2);
        memcpy(p, frame->data[2], frame->linesize[2] * h / 2);
        display(rga_fd, nv12_ion_buffer.fd, w, h, coded_w, coded_h, pix_fmt);
#endif
      }
      av_frame_free(&frame);
    }
#endif
  } while (!ret);

#if !MPP_ENABLE
  video_ion_free(&nv12_ion_buffer);
#endif
  if (ctx)
    decode_deinit_context(&ctx);
  rk_rga_close(rga_fd);
  return 0;
}
