extern "C" {
#include "../../video/video_ion_alloc.c"  // for alloc ion memory
}

#include "../../video/av_wrapper/video_common.cpp"

#include "../../video/av_wrapper/mpp_inc.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/buffer.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/base_encoder.cpp"

#include "../../video/av_wrapper/encoder_muxing/encoder/mpp_jpeg_encoder.cpp"  // for call jpeg hw encoder

void* jpeg_encode_routine(void* arg) {
  int thread_idx = *((int*)arg);
  int w = 640, h = 480;
  int frame_size = w * h * 3 / 2;
  void* buffer = malloc(frame_size);
  assert(buffer);
  MediaConfig config;
  JpegEncConfig& jconfig = config.jpeg_config;
  jconfig.width = w;
  jconfig.height = h;
  jconfig.fmt = PIX_FMT_NV12;
  jconfig.qp = 10;  // best quality [1-10]

  int read_size = 0, write_size = 0;
  char path[64] = {0};
  snprintf(path, sizeof(path), "/mnt/sdcard/test_%d_%d.nv12", w, h);
  int nv12_file_fd = open(path, O_RDONLY);
  assert(nv12_file_fd >= 0);
  read_size = read(nv12_file_fd, buffer, frame_size);
  close(nv12_file_fd);
  assert(read_size == frame_size);
  int i = 1500;
  while (i-- > 0) {
    // prepare ion buffer
    struct video_ion nv12_ion_buffer = {}, jpeg_ion_buffer = {};
    nv12_ion_buffer.client = -1;
    nv12_ion_buffer.fd = -1;
    jpeg_ion_buffer.client = -1;
    jpeg_ion_buffer.fd = -1;
    if (video_ion_alloc_rational(&nv12_ion_buffer, w, h, 3, 2) == -1)
      assert(0);
    if (video_ion_alloc_rational(&jpeg_ion_buffer, w, h, 3, 2) == -1)
      assert(0);
    BufferData input_data;
    input_data.vir_addr_ = nv12_ion_buffer.buffer;
    input_data.ion_data_.fd_ = nv12_ion_buffer.fd;
    input_data.ion_data_.handle_ = nv12_ion_buffer.handle;
    input_data.mem_size_ = nv12_ion_buffer.size;

    BufferData dst_data;
    dst_data.vir_addr_ = jpeg_ion_buffer.buffer;
    dst_data.ion_data_.fd_ = jpeg_ion_buffer.fd;
    dst_data.ion_data_.handle_ = jpeg_ion_buffer.handle;
    dst_data.mem_size_ = jpeg_ion_buffer.size;
    Buffer input_buf(input_data);
    Buffer dst_buf(dst_data);

    memcpy(nv12_ion_buffer.buffer, buffer, frame_size);
    MPPJpegEncoder* encoder = new MPPJpegEncoder();
    // init jpegEncoder
    if (encoder->InitConfig(config))
      assert(0);
    int encode_ret = encoder->Encode(&input_buf, &dst_buf);
    assert(!encode_ret);
    assert(dst_buf.GetValidDataSize() > 0);
    encoder->unref();
    snprintf(path, sizeof(path), "/mnt/sdcard/%d-test_%d_%d.jpg", thread_idx, w,
             h);
    int jpeg_file_fd = open(path, O_WRONLY | O_CREAT | O_TRUNC);
    if (jpeg_file_fd < 0) {
      printf("open jpg failed, errno: %d\n", errno);
      video_ion_free(&nv12_ion_buffer);
      video_ion_free(&jpeg_ion_buffer);
      break;
    }
    write_size =
        write(jpeg_file_fd, jpeg_ion_buffer.buffer, dst_buf.GetValidDataSize());
    close(jpeg_file_fd);

    video_ion_free(&nv12_ion_buffer);
    video_ion_free(&jpeg_ion_buffer);
    if (write_size != dst_buf.GetValidDataSize()) {
      printf("sd card may be full filled\n");
      unlink(path);
      break;
    }
  }

  free(buffer);
  return nullptr;
}

int main() {
  const static int thread_num = 10;
  int thread_idx[thread_num]{};
  pthread_t tid[thread_num] = {};
  for (int i = 0; i < thread_num; i++) {
    thread_idx[i] = i;
    if (StartThread(tid[i], jpeg_encode_routine, (void*)(thread_idx + i))) {
      printf("start thread <%d> failed\n", i);
      break;
    }
  }

  for (int i = 0; i < thread_num; i++) {
    if (tid[i])
      StopThread(tid[i]);
  }
  return 0;
}
