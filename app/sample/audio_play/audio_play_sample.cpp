extern "C" {
#include "../../video/av_wrapper/audio/playback/audio_dev.h"
}

#include "../../video/av_wrapper/video_common.cpp"
#include "../../video/av_wrapper/decoder_demuxing/decoder_demuxing.cpp"
#include "../../video/av_wrapper/audio/playback/audio_output.cpp"
#include "../../video/av_wrapper/audio/playback/audioplay.cpp"

// pc:
//.....cd <project root path>/app/sample/audio_play_sample
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-gcc -c \
       -I../../video \
       -I../../video/av_wrapper \
       -I../../../out/system/include \
       -ffunction-sections -fdata-sections \
       -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -g \
       audio_device.c -o audio_device.o
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ -c \
       -I../../video \
       -I../../video/av_wrapper \
       -I../../../out/system/include \
       -ffunction-sections -fdata-sections \
       -fPIC -O2 -g2 -DDEBUG -fstack-protector-all -std=c++11 -g \
       audio_play_sample.cpp -o audio_play_sample.o
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-g++ \
      -L../../../out/system/lib -Wl,--gc-sections \
      -o audio_play_sample audio_device.o audio_play_sample.o \
      -lsalsa -lswresample -lavformat -lavcodec -lavutil \
      -lfsmanage -lfdk-aac -lmpp -lpthread
//     <project root path>/prebuilts/toolschain/usr/bin/arm-linux-strip \
       audio_play_sample
//     cp audio_play_sample <sdcard>

// board:
//        mount /dev/mmcblk0p1 /mnt/sdcard/
//        cd /mnt/sdcard
//        ./audio_play_sample test.wav

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "missing input file!\n");
    return -1;
  }
  int i = argc - 1;
  while (i) {
    char* file_path = argv[argc - i];
    audio_sync_play(file_path);
    i--;
  }
  return 0;
}
