#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path
cd build

if [ "$rebuild" == "yes"  ]; then
    rm libvoice_process.so
fi

if [ "$MICROPHONE_TYPE" == "single" ];then
    AUDIO_LIB="RK_SseProcess.a"
elif [ "$MICROPHONE_TYPE"  == "dual" ];then
    AUDIO_LIB="RK_DseProcess.a"
else
    AUDIO_LIB="libvoice_process.a"
fi

echo $1" do make"
CC="arm-linux-gcc"
CFLAGS="-fPIC -mcpu=cortex-a7"
$CC -shared $CFLAGS -Wl,--whole-archive $AUDIO_LIB -o libvoice_process.so -Wl,--no-whole-archive

cd ..
SYSTEM_DIR=$(pwd)/../../out/system
SYSTEM_LIB_DIR=$SYSTEM_DIR/lib
SYSTEM_INC_DIR=$SYSTEM_DIR/include
SYSTEM_BIN_DIR=$SYSTEM_DIR/bin
[ ! -d $SYSTEM_LIB_DIR ] && mkdir -p $SYSTEM_LIB_DIR
[ ! -d $SYSTEM_INC_DIR ] && mkdir -p $SYSTEM_INC_DIR
[ ! -d $SYSTEM_BIN_DIR ] && mkdir -p $SYSTEM_BIN_DIR
cp build/libvoice_process.so $SYSTEM_LIB_DIR
cp -r out/inc/* $SYSTEM_INC_DIR
cp -r out/lib/* $SYSTEM_LIB_DIR
cp -r out/bin/* $SYSTEM_BIN_DIR

cd $current_path
