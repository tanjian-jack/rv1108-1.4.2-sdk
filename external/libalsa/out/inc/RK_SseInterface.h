/************************************************************************************
文件名称：
文件功能：
函数列表：
修改日期：
*************************************************************************************/
#ifndef RK_SSEINTERFACE_H
#define RK_SSEINTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

extern int   RK_SseInit(char  *pcParaName, int swFrmLen);
extern void  RK_SseDestroy();
extern void  RK_SseProcess(short int  *pshwSigIn, short int *pshwSigOut, int  swFrmLen);

#ifdef __cplusplus
}
#endif

#endif
