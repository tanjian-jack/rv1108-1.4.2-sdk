#ifndef __ZSGPSLIB_H__
#define __ZSGPSLIB_H__

/*
* @brief open GPS driver
*
* @param pGpsConfFilePath - zsgps.conf file path.
*		(note: this file path is an absolute path, if this file path is
*NULL, default use the relative path "./zsgps.conf")
* 		(eg. "/usr/local/bin/zsgps.conf")
*
* @return ret 0: open gps success, else ret -1: open gps fail
*/
extern int ZS_GPSOpen(const char* pGpsConfFilePath);

/*
* @brief close GPS driver
*
* @return void
*/
extern void ZS_GPSClose();

/*
* @brief read GPS NMEA0183 data
*
* @param pBuf - read data into pBuf
* @param len - attempts to read data len
*
* @return - the numbers of bytes read is returned
*
* @return void
*/
extern int ZS_ReadNmea(void* pBuf, unsigned int len);

/*
* @brief clean the NMEA0183 data
*
* @return void
*/
void ZS_CleanNmea();

typedef void (*GpsNmeaCallBack)(const char* pNmea, int len);

/*
* @brief open GPS and register callback function for read NMEA0183 data
*
* @param pGpsConfFilePath - zsgps.conf file path.
*		(note: this file path is an absolute path, if this file path is
*NULL, default use the relative path "./zsgps.conf")
* 		(eg. "/usr/local/bin/zsgps.conf")
*
* @param nmeaCB - read nmea callback function
*
* @return ret 0: open gps success, else ret -1: open gps fail
*/
extern int ZS_GPSOpenAndRegNmeaCB(const char* pGpsConfFilePath,
                                  GpsNmeaCallBack nmeaCB);

/*
* @brief register callback function for read NMEA0183 data, call this interface
*when need start read nmea
*
* @param nmeaCB - read nmea callback function
*
* @return void
*/
extern void ZS_RegNmeaCB(GpsNmeaCallBack nmeaCB);

/*
* @brief deregister callback function for read NMEA0183 data, call this
*interface when need stop read nmea
*
* @return void
*/
extern void ZS_DeRegNmeaCB();

#endif
