#! /bin/bash

current_path=$(pwd)

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
echo $bash_path

SYSTEM_DIR=$(pwd)/../../out/system
SYSTEM_LIB_DIR=$SYSTEM_DIR/lib
SYSTEM_INC_DIR=$SYSTEM_DIR/include
SYSTEM_BIN_DIR=$SYSTEM_DIR/bin

[ ! -d $SYSTEM_LIB_DIR ] && mkdir -p $SYSTEM_LIB_DIR
[ ! -d $SYSTEM_INC_DIR ] && mkdir -p $SYSTEM_INC_DIR
[ ! -d $SYSTEM_BIN_DIR ] && mkdir -p $SYSTEM_BIN_DIR

cp -r zs_gpslib/*.h $SYSTEM_INC_DIR
cp -r zs_gpslib/*.a $SYSTEM_LIB_DIR
cp -r zs_gpslib/bin/* $SYSTEM_BIN_DIR

cd $current_path
