/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#define MODULE_TAG "fs_buffer"

#include <string.h>

#include "fs_log.h"
#include "fs_env.h"
#include "fs_err.h"

#include "fs_buffer_impl.h"

#define BUFFER_OPS_MAX_COUNT 1024

typedef enum FsBufOps_e {
	GRP_CREATE,
	GRP_RELEASE,
	GRP_DESTROY,

	GRP_OPS_BUTT = GRP_DESTROY,
	BUF_COMMIT,
	BUF_CREATE,
	BUF_REF_INC,
	BUF_REF_DEC,
	BUF_DESTROY,
	BUF_OPS_BUTT,
} FsBufOps;


unsigned int fs_buffer_debug = 0;
FsBufferGroupImpl *mgroup = NULL;
FsBufferGroupImpl *fs_buffer_get_group(const char *tag);
void fs_buffer_put_group(FsBufferGroupImpl *p);
void fs_buffer_destroy_group(FsBufferGroupImpl *group);

FsBufferGroupImpl *fs_buffer_get_buffergroup(FsBufferImpl *buffer)
{
	return (FsBufferGroupImpl *)buffer->buffergroup;
}

FsBufferGroupImpl *fs_buffer_set_buffergroup(FsBufferImpl *buffer,
                                             FsBufferGroupImpl *group)
{
	buffer->buffergroup = (void *)group;
}

void fs_buffer_lock(pthread_mutex_t *grouplock)
{
	pthread_mutex_lock(grouplock);
	return;
}

void fs_buffer_unlock(pthread_mutex_t *grouplock)
{
	pthread_mutex_unlock(grouplock);
	return;
}

FS_RET deinit_buffer_no_lock(FsBufferImpl *buffer)
{
	FS_BUF_FUNCTION_ENTER();
	fs_assert(buffer->ref_count == 0);
	fs_assert(buffer->used == 0);

	list_del_init(&buffer->list_status);
	FsBufferGroupImpl *group = fs_buffer_get_buffergroup(buffer);
	char *test = NULL;
	free(buffer->info.ptr);
	group->usage -= buffer->info.size;
	group->buffer_count--;

	free(buffer);
	FS_BUF_FUNCTION_LEAVE();
	return FS_OK;
}

static FS_RET inc_buffer_ref_no_lock(FsBufferImpl *buffer)
{
	FS_BUF_FUNCTION_ENTER();
	FS_RET ret = FS_OK;
	FsBufferGroupImpl *group = fs_buffer_get_buffergroup(buffer);
	if (!buffer->used) {
		fs_assert(group);
		buffer->used = 1;
		if (group) {
			list_del_init(&buffer->list_status);
			list_add_tail(&buffer->list_status, &group->list_used);
			group->count_used++;
			group->count_unused--;
		} else {
			fs_err_f("unused buffer without group\n");
			ret = FS_NOK;
		}
	}
	buffer->ref_count++;

	FS_BUF_FUNCTION_LEAVE();
	return ret;
}

void fs_buffer_info_dump(FsBufferImpl *buffer)
{
	fs_log("buffer %p size %10d ref_count %3d discard %d\n",
	       buffer,  buffer->info.size,
	       buffer->ref_count, buffer->discard);
}

FS_RET fs_buffer_create(FsBufferGroupImpl *group,
                        const char *tag, FsBufferInfo *info)
{
	FS_BUF_FUNCTION_ENTER();
	fs_buffer_lock(&group->grouplock);

	FS_RET ret = FS_OK;
	FsBufferImpl *p = NULL;

	if (NULL == group) {
		fs_err_f("can not create buffer without group\n");
		ret = FS_NOK;
		goto RET;
	}

	if (group->limit_count && group->buffer_count >= group->limit_count) {
		fs_err_f("reach group count limit %d\n", group->limit_count);
		ret = FS_NOK;
		goto RET;
	}

	if (group->limit_size && info->size > group->limit_size) {
		fs_err_f("required size %d reach group size limit %d\n",
		         info->size, group->limit_size);
		ret = FS_NOK;
		goto RET;
	}

	p = calloc(sizeof(FsBufferImpl), 1);
	if (NULL == p) {
		fs_err_f("failed to allocate context\n");
		ret = FS_ERR_MALLOC;
		goto RET;
	}

	info->ptr = malloc(info->size);
	if (info->ptr == NULL) {
		fs_err_f("failed to create buffer with size %d\n", info->size);
		free(p);
		ret = FS_ERR_MALLOC;
		goto RET;
	}

	p->info = *info;

	if (NULL == tag)
		tag = group->tag;

	strncpy(p->tag, tag, sizeof(p->tag));
	p->buffer_id = group->buffer_id;
	fs_buffer_set_buffergroup(p, group);
	INIT_LIST_HEAD(&p->list_status);
	list_add_tail(&p->list_status, &group->list_unused);
	group->buffer_id++;
	group->usage += info->size;
	group->buffer_count++;
	group->count_unused++;

	//fs_err("alloc buffer in group=%d, size=%d\n", group_id, info->size);
RET:
	fs_buffer_unlock(&group->grouplock);
	FS_BUF_FUNCTION_LEAVE();
	return ret;
}

FS_RET fs_buffer_destroy(FsBufferImpl *buffer)
{
	FS_BUF_FUNCTION_ENTER();
	FsBufferGroupImpl *group = fs_buffer_get_buffergroup(buffer);
	fs_buffer_lock(&group->grouplock);

	FS_RET ret = deinit_buffer_no_lock(buffer);

	fs_buffer_unlock(&group->grouplock);
	FS_BUF_FUNCTION_LEAVE();
	return ret;
}

FS_RET fs_buffer_ref_inc(FsBufferImpl *buffer)
{
	FS_BUF_FUNCTION_ENTER();
	FsBufferGroupImpl *group = fs_buffer_get_buffergroup(buffer);
	fs_buffer_lock(&group->grouplock);
	FS_RET ret = inc_buffer_ref_no_lock(buffer);
	fs_buffer_unlock(&group->grouplock);

	FS_BUF_FUNCTION_LEAVE();
	return ret;
}

FS_RET fs_buffer_ref_dec(FsBufferImpl *buffer)
{
	FS_BUF_FUNCTION_ENTER();

	FS_RET ret = FS_OK;
	FsBufferGroupImpl *group = fs_buffer_get_buffergroup(buffer);
	if (group == NULL) {
		fs_err_f("get mgroup == NULL\n");
		ret = FS_NOK;
	}

	fs_buffer_lock(&group->grouplock);
	if (buffer->ref_count <= 0) {
		fs_err_f("found non-positive, ref_count=%d\n", buffer->ref_count);
		ret = FS_NOK;
	} else {
		buffer->ref_count--;
		if (0 == buffer->ref_count) {
			buffer->used = 0;
			list_del_init(&buffer->list_status);

			if (buffer->discard) { //release the buffer
				deinit_buffer_no_lock(buffer);
			} else {
				list_add_tail(&buffer->list_status, &group->list_unused);
				group->count_unused++;
				//fs_err("group=%d, return to unused=%d, size=%d\n",
				//  group->group_id, group->count_unused, buffer->info.size);
			}
			group->count_used--;
		}
	}

	fs_buffer_unlock(&group->grouplock);
	FS_BUF_FUNCTION_LEAVE();
	return ret;
}

FsBufferImpl *fs_buffer_get_unused(FsBufferGroupImpl *p, size_t size)
{
	FsBufferImpl *buffer = NULL;
	FS_BUF_FUNCTION_ENTER();
	fs_buffer_lock(&p->grouplock);

	if (!list_empty(&p->list_unused)) {
		FsBufferImpl *pos, *n;
		list_for_each_entry_safe(pos, n, &p->list_unused, FsBufferImpl,
		                        list_status) {
			//fs_log("[RKV_DEBUG]pos->info.size=%d, size=%d \n",
			//pos->info.size, size);
			if (pos->info.size >= size) {
				buffer = pos;
				inc_buffer_ref_no_lock(buffer);
				//fs_err("group=%d, get from unused=%d, size=%d\n",
				//p->group_id, p->count_unused, size);
				break;
			}
		}
	}

	fs_buffer_unlock(&p->grouplock);
	FS_BUF_FUNCTION_LEAVE();
	return buffer;
}

FS_RET fs_buffer_group_limit_config(FsBufferGroupImpl *group,
                                    size_t size, int count)
{
	fs_buffer_lock(&(group)->grouplock);

	if (NULL == group) {
		fs_err_f("input invalid group %p\n", group);
		return -1;
	}

	group->limit_size = size;
	group->limit_count = count;

	fs_buffer_unlock(&(group)->grouplock);
	return 0;
}


FS_RET fs_buffer_group_init(FsBufferGroupImpl **group, const char *tag)
{
	FS_BUF_FUNCTION_ENTER();
	*group = fs_buffer_get_group(tag);
	FS_BUF_FUNCTION_LEAVE();
	return ((*group) ? (FS_OK) : (FS_NOK));
}

FS_RET fs_buffer_group_deinit(FsBufferGroupImpl *p)
{
	if (NULL == p) {
		fs_err_f("found NULL pointer\n");
		return FS_ERR_NULL_PTR;
	}

	FS_BUF_FUNCTION_ENTER();

	fs_buffer_put_group(p);

	FS_BUF_FUNCTION_LEAVE();
	return FS_OK;
}

FS_RET fs_buffer_group_reset(FsBufferGroupImpl *p)
{
	if (NULL == p) {
		fs_err_f("found NULL pointer\n");
		return FS_ERR_NULL_PTR;
	}

	fs_buffer_lock(&p->grouplock);
	FS_BUF_FUNCTION_ENTER();

	if (!list_empty(&p->list_used)) {
		FsBufferImpl *pos, *n;
		list_for_each_entry_safe(pos, n, &p->list_used, FsBufferImpl,
		                         list_status) {
			// fs_buffer_ref_dec(pos);
			pos->discard = 1;
		}
	}

	/* remove unused list */
	if (!list_empty(&p->list_unused)) {
		FsBufferImpl *pos, *n;
		list_for_each_entry_safe(pos, n, &p->list_unused, FsBufferImpl,
		                         list_status) {
			deinit_buffer_no_lock(pos);
			p->count_unused--;
		}
	}

	fs_buffer_unlock(&p->grouplock);
	FS_BUF_FUNCTION_LEAVE();
	return FS_OK;
}


void fs_buffer_group_dump(FsBufferGroupImpl *group)
{
	fs_log("limit size %d count %d\n", group->limit_size, group->limit_count);

	fs_log("used buffer count %d\n", group->count_used);
	fs_log("used size  = %d\n", group->usage);
	FsBufferImpl *pos, *n;
	list_for_each_entry_safe(pos, n, &group->list_used, FsBufferImpl,
	                         list_status)
		fs_buffer_info_dump(pos);

	fs_log("unused buffer count %d\n", group->count_unused);
	list_for_each_entry_safe(pos, n, &group->list_unused, FsBufferImpl,
	                         list_status) {
		fs_buffer_info_dump(pos);
	}

}

FsBufferGroupImpl *fs_buffer_get_group(const char *tag)
{
	FS_BUF_FUNCTION_ENTER();
	FsBufferGroupImpl *p = calloc(sizeof(FsBufferGroupImpl), 1);
	if (NULL == p) {
		fs_err("FsBufferService failed to allocate group context\n");
		return NULL;
	}

	if (pthread_mutex_init(&p->grouplock, NULL) != 0) {
		printf("grouplock init fail \n");
		exit(0);
	}

	INIT_LIST_HEAD(&p->list_used);
	INIT_LIST_HEAD(&p->list_unused);
	if (!fs_buffer_debug)
		fs_env_get_u32("fs_buffer_debug", &fs_buffer_debug, 0);
	p->log_runtime_en = (fs_buffer_debug & FS_BUF_DBG_OPS_RUNTIME) ? (1) : (0);
	p->log_history_en = (fs_buffer_debug & FS_BUF_DBG_OPS_HISTORY) ? (1) : (0);

	if (tag) {
		snprintf(p->tag, sizeof(p->tag), "%s", tag);
	} else {
		snprintf(p->tag, sizeof(p->tag), "unknown");
	}
	p->limit = BUFFER_GROUP_SIZE_DEFAULT;

	FS_BUF_FUNCTION_LEAVE();
	return p;
}

void fs_buffer_put_group(FsBufferGroupImpl *p)
{
	/* emove unused list */
	FS_BUF_FUNCTION_ENTER();
	if (!list_empty(&p->list_unused)) {
		FsBufferImpl *pos, *n;
		list_for_each_entry_safe(pos, n, &p->list_unused, FsBufferImpl,
		                         list_status) {
			deinit_buffer_no_lock(pos);
			p->count_unused--;
		}
	}
	if (list_empty(&p->list_used)) {
		fs_buffer_destroy_group(p);
	} else {
		fs_err("fs_group %p tag %s  deinit with %d bytes not released\n",
		       p, p->tag, p->usage);

		fs_buffer_group_dump(p);

		/* if clear on exit we need to release remaining buffer */
		if (p->clear_on_exit) {
			FsBufferImpl *pos, *n;

			fs_err("force release all remaining buffer\n");

			list_for_each_entry_safe(pos, n, &p->list_used, FsBufferImpl,
			                         list_status) {
				fs_err("clearing buffer %p pos\n");
				pos->ref_count = 0;
				pos->used = 0;
				pos->discard = 0;
				deinit_buffer_no_lock(pos);
				p->count_used--;
			}
			fs_buffer_destroy_group(p);
		}
	}
	FS_BUF_FUNCTION_LEAVE();
}

void fs_buffer_destroy_group(FsBufferGroupImpl *group)
{
	FS_BUF_FUNCTION_ENTER();
	fs_assert(group->count_used == 0);
	fs_assert(group->count_unused == 0);
	if (group->count_unused || group->count_used) {
		fs_err("fs_buffer_group_deinit mismatch counter"
		       "used %4d unused %4dfound\n",
		       group->count_used, group->count_unused);
		group->count_unused = 0;
		group->count_used   = 0;
	}
	if (pthread_mutex_destroy(&group->grouplock) != 0) {
		fs_err("grouplock init fail \n");
		exit(0);
	}
	free(group);
	FS_BUF_FUNCTION_LEAVE();
}


