/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "fs_env.h"

#define ENV_BUF_SIZE_LINUX  1024


unsigned int fs_env_get_u32(const char *name,
                            unsigned int *value,
                            unsigned int default_value)
{
	char *ptr = getenv(name);
	if (NULL == ptr) {
		*value = default_value;
	} else {
		char *endptr;
		int base = (ptr[0] == '0' && ptr[1] == 'x') ? (16) : (10);
		errno = 0;

		*value = strtoul(ptr, &endptr, base);
		if (errno || (ptr == endptr)) {
			errno = 0;
			*value = default_value;
		}
	}
	return 0;
}

unsigned int fs_env_get_str(const char *name, char **value, char *default_value)
{
	*value = getenv(name);
	if (NULL == *value)
		*value = default_value;
	return 0;
}

unsigned int fs_env_set_u32(const char *name, unsigned int value)
{
	char buf[ENV_BUF_SIZE_LINUX];

	snprintf(buf, sizeof(buf), "%u", value);
	return setenv(name, buf, 1);
}

unsigned int fs_env_set_str(const char *name, char *value)
{
	return setenv(name, value, 1);
}


