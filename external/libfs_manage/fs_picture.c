/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/types.h>
#include <malloc.h>

#include "fs_cache.h"
#include "fs_file.h"
#include "fs_log.h"
#include "fs_picture.h"
#include "huffman.h"

#define UPPER_LEN 2048

int has_huffman(unsigned char *buf, unsigned int buf_size)
{
	unsigned int i = 0;
	unsigned char cur_byte;

	while (i < buf_size - 1) {
		cur_byte = buf[i++];
		if (cur_byte == 0xFF) {
			cur_byte = buf[i++];
			if (cur_byte == DHT)
				return 1;
			else if (cur_byte == SOS)
				break;
		}
		if (i > UPPER_LEN)
			break;
	}
	return 0;
}

int fs_picture_open(const char *filename, int flags, int mode)
{
	return fs_file_open(filename, flags, mode);
}

int fs_picture_close(int fd)
{
	struct timeval t1;
	struct timeval t2;
	struct timeval t3;

	int delay = 0;
	int delay1 = 0;
	int test_fd = 0;
	gettimeofday(&t1, NULL);
	//fsync(fd);
	gettimeofday(&t2, NULL);
	int ret = close(fd);

	gettimeofday(&t3, NULL);

	delay = (t2.tv_sec - t1.tv_sec) * 1000000 + (t2.tv_usec - t1.tv_usec);
	delay1 = (t3.tv_sec - t2.tv_sec) * 1000000 + (t3.tv_usec - t2.tv_usec);

	printf("picture close use= %duS, fsync use= %duS, all use =%duS\n",
	       delay1, delay, delay + delay1);
	return ret;
}

ssize_t fs_picture_write(int fd, const void *buf, size_t count)
{
	size_t real_size = FS_ALIGN(count, FS_ALIGN_VALUE);
	char *buffer = NULL;
	int ret = posix_memalign((void **)(&buffer), FS_ALIGN_VALUE, real_size);

	if (buffer == NULL)
		return -1;
	memcpy(buffer, buf, count);
	int size = fs_file_write(fd, buffer, real_size);
	free(buffer);
	return size;
}

ssize_t fs_picture_read(int fd, void *buf, size_t count)
{
	return fs_file_read(fd, buf, count);
}

ssize_t fs_picture_mjpg_write(char *filename, const void *srcbuf, size_t size)
{
	int ret;
	int size_start = 0;
	size_t real_size = 0;
	char *buffer = NULL;

	unsigned char *in_buf = (unsigned char *)srcbuf;
	unsigned int buf_size = size;

	if(size <= 2) {
		fs_err_f("mjpeg size to small = %d\n", size);
		return -1;
	}
	while (*(in_buf + buf_size - 2) != 0xFF || *(in_buf + buf_size - 1) != 0xD9)
		buf_size--;

	if (has_huffman(in_buf, buf_size)) {
		real_size = FS_ALIGN(buf_size, FS_ALIGN_VALUE);
		posix_memalign((void **)(&buffer), FS_ALIGN_VALUE, real_size);
		if (buffer == NULL)
			goto error;
		memcpy(buffer, in_buf, buf_size);
	} else {
		int dht_size = sizeof(dht_data);
		unsigned char *pdeb = in_buf;
		unsigned char *pcur = in_buf;
		unsigned char *plimit = in_buf + buf_size;

		/* find the SOF0(Start Of Frame 0) of JPEG */
		while ((((pcur[0] << 8) | pcur[1]) != 0xffc0) && (pcur < plimit)) {
			pcur++;
		}
		/* SOF0 of JPEG exist */
		if (pcur < plimit) {
			/* insert huffman table after SOF0 */
			size_start = pcur - pdeb;
			real_size = FS_ALIGN(buf_size + dht_size, FS_ALIGN_VALUE);
			posix_memalign((void **)(&buffer), FS_ALIGN_VALUE, real_size);
			if (buffer == NULL)
				goto error;
			memcpy(buffer, in_buf, size_start);
			memcpy(buffer + size_start, dht_data, dht_size);
			memcpy(buffer + size_start + dht_size, pcur, buf_size - size_start);
			fs_log("no huffman\n");
		}
	}
	return fs_cache_picture_write(filename, buffer, real_size);
error:
	fs_err_f("--------error---------\n");
	return -1;
}

