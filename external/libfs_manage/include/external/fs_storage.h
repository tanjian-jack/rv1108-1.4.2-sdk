/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_STORAGE_H__
#define __FS_STORAGE_H__

#include <sys/types.h>
#include "fs_file.h"

typedef enum _FILETYPE {
	VIDEOFILE_TYPE = 0,
	PICFILE_TYPE,
	LOCKFILE_TYPE,
	THUMBFILE_TYPE,
	GPS_TYPE,
	UNKNOW
} FILETYPE;

typedef enum _FSINITSTAT {
	FS_NORMAL     =  0,
	FS_UNFORMAT   = -1,
	FS_INITFAIL   = -2,
}FSINITSTAT;

struct file_list;
struct file_node {
	char *name;
	time_t time;
	size_t filesize;
	struct file_list *file_list;
	struct file_node *pre;
	struct file_node *next;
};

typedef enum _FS_ERROR {
	FS_REPAIR = 0,
	FS_LOW_CAPACITY
} FS_ERROR;

typedef enum _FS_PATH_ATTRI {
	FS_PATH_NORMAL = 0,
	FS_PATH_HIDE
} FS_PATH_ATTRI;

struct file_list {
	char *path;
	char *format;
	char recing_filename[FILENAME_LEN];
	FILETYPE filetype;
	int video_type;
	size_t filesize;
	size_t filesize_align;
	struct file_node *file_head;
	struct file_node *file_tail;
	struct file_node *folder_head;
	struct file_node *folder_tail;
	int filenum_lim;
	volatile int filenum;
	volatile long long all_space;
	volatile long long use_space;
	volatile long long free_space;
	//int len;
};

typedef enum _FS_STORAGE_TYPE {
	PHOTO_REAR = 0,
	COLLI_REAR,
#ifdef CACHE_ENCODEDATA_IN_MEM
	THUMB_COLLI_REAR,
#endif
	THUMB_REAR,
	PHOTO_CIF,
	COLLI_CIF,
#ifdef CACHE_ENCODEDATA_IN_MEM
	THUMB_COLLI_CIF,
#endif
	THUMB_CIF,
	PHOTO_FRONT,
	COLLI_FRONT,
#ifdef CACHE_ENCODEDATA_IN_MEM
	THUMB_COLLI_FRONT,
#endif
	THUMB_FRONT,
	VIDEO_CIF,
	VIDEO_REAR,
	VIDEO_FRONT,
} FS_STORAGE_TYPE;

typedef struct _FS_STORAGE_PATH {
	char storage_path[FILENAME_LEN];
	char colli_path[FILENAME_LEN];
	char thumb_path[FILENAME_LEN];
	char storage_format[FILEFORMAT_LEN];
	char file_sfx[FILEFORMAT_LEN];
	float storage_percent; /* storage percent, the units is "%" */
	int filesize;       /* file size, the units is "Byte" */
	int filesize_align;
	int filenum;
	int video_type;
	FILETYPE filetype;
	FS_PATH_ATTRI path_attri;
	struct file_list *filelist;
} FS_STORAGE_PATH;

#define RK_FILEOPERA_MSGKEY 1021
#define FS_PREPARE_FORMAT ".pre"
#define FS_COLLISION_TAG "_lock"

#define VIDEO_REPAIR 0

#define THUMB_FILE_SIZE (128 * FS_SZ_1K)
#define ISP_FILE_ALIGN (0x2000000)
#define USB_FILE_ALIGN (0x1000000)
#define CIF_FILE_ALIGN (0x1000000)
#define PIC_FILE_ALIGN (0x100000)
#define GPS_FILE_ALIGN (0x20000)

//#define FS_ENABLE_COLLISION_FILE
#if VIDEO_REPAIR
#define REPAIR_CHECK_NUMBER 6
#define REPAIR_FILE_MINSIZE (10 * FS_SZ_1K)
#define REPAIR_READ_BUFFER_LEN (FS_SZ_1K)
#define REPAIR_FILE_PATH "/catch/repair_video/"
#endif

struct file_list *fs_storage_filelist_get(const char *fullpath_filename);
int fs_storage_filelist_init(FS_STORAGE_PATH *storage_path, struct file_list **file_list);
void fs_manage_free_filelist(struct file_list **list);
void fs_manage_getsdcardcapacity(long long *free_size, long long *total_size);
int fs_storage_remove(const char *fullpath_videoname, int type);
int fs_storage_add(const char *fullpath_videoname, size_t filesize);
int fs_storage_init(int (*callback)(void *, int), void *arg);
int fs_storage_deinit(void);

struct file_node *fs_get_tail_filenode(struct file_list *list);
struct file_node *fs_get_head_filenode(struct file_list *list);
struct file_node *fs_get_next_filenode(struct file_node *node);
struct file_node *fs_get_pre_filenode(struct file_node *node);

struct file_list *fs_manage_getmediafilelist(void);
void fs_manage_free_mediafilelist(void);
int fs_storage_get_medialist_bytype(int videotype,
                                    FILETYPE filetype,
                                    struct file_list *list);
void fs_storage_free_mediafilelist(struct file_list **list);

int fileopera_ipcmsg_send_cmd(int fd, void *param1, void *param2, int command);
int fs_manage_insert_file(const char *filename);
int fs_manage_insert_filepath(const char *path, const char *filename);

int fs_storage_sizealign_get_bytype(int videotype, FILETYPE filetype);
char *fs_storage_folder_get_bytype(int videotype, FILETYPE filetype);
char *fs_storage_folder_get_bynode(struct file_node *filenode);
int fs_storage_filetype_get_bynode(struct file_node *filenode);
char *fs_storage_filename_get_bynode(struct file_node *filenode);

size_t fs_storage_filesize_get_bynode(struct file_node *filenode);

char *fs_storage_format_get_bynode(struct file_node *filenode);
int fs_storage_videotype_get_bynode(struct file_node *filenode);
int fs_storage_update_filelist(struct file_node *filenode, char *newpath);
int fs_storage_thumbname_get(const char *fullpath_name, char *thumbname);
int fs_storage_fileinfo_get_bypath(char *path, FILETYPE *filetype, int *videotype);
int *fs_storage_filetypetbl_get();
int fs_storage_filetypenum_get();
void fs_storage_set_recing_filename(char *filename);
char *fs_storage_get_recing_filename(char *path);
int fs_storage_file_prepare(const char *filename);
void *fs_storage_pthread(void *arg);
int string_to_time(const char *strDateStr, time_t *timeData);
FS_STORAGE_PATH *fs_storage_get_bythumb(const char *pathname);
inline long long  fs_storage_get_free_cap(long long total);
inline long long fs_storage_get_use_cap(long long total);
int fs_sdcard_set_newfile();

extern FS_STORAGE_PATH fs_storage_tbl[];
extern int storage_path_num;

#define fs_storage_foreach_path(number, fs_storage_path) \
        for (number = 0, fs_storage_path = &fs_storage_tbl[number]; \
            number < storage_path_num; \
            number++, fs_storage_path = &fs_storage_tbl[number]) \

#endif
