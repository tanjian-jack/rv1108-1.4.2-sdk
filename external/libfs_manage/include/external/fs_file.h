/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_FILE_H__
#define __FS_FILE_H__

#include <sys/types.h>
#include <time.h>

#define FILENAME_LEN 128
#define FILEFORMAT_LEN 10

#define FS_ALIGN_VALUE 0x1000
#define FS_ALIGN(x, a) (((x) + (a) - 1) &~ ((a) - 1))

#define MP4_MEDIA_OFFSET 40
#define MOV_MEDIA_OFFSET 28

#ifndef FS_SZ_1K
#define FS_SZ_1K (1024)
#endif

#ifndef FS_SZ_1M
#define FS_SZ_1M  (FS_SZ_1K * FS_SZ_1K)
#endif

typedef enum _SPEED_MODE{
	FAST = 0,
	MIDDLE,
	LOW,
	FREEZE
} SPEED_MODE;

void fs_file_set_hide_byname(char *filename);
void fs_file_set_hide(int fd);
void fs_file_set_visible(int fd);
int fs_file_open_normal(const char *filename, int flags, int mode);
int fs_file_close_normal(int fd);
int fs_file_open(const char *filename, int flags, int mode);
int fs_file_close(int fd);
ssize_t fs_file_write(int fd, const void *buf, size_t count);
ssize_t fs_file_read(int fd, void *buf, size_t count);
int64_t fs_file_lseek(int fd, int64_t offset ,int whence);
void fs_file_get_filesize_byname(const char* filename,
                                 size_t *filesize,
                                 size_t *filespace);
void fs_file_get_fullpath_name(char *path, char *filename,
                               char *fullpath_name, int len);
int fs_file_create_path(char *path, int hide);
int fs_file_media_offset(const char *filename);


#endif
