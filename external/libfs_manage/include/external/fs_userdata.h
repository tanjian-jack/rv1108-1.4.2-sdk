/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_USERDATA_H__
#define __FS_USERDATA_H__

#define USERDATA_DEVICE "/dev/userdata"

int fs_userdata_exist(); /* 1 exist 0 none */
int fs_userdata_get_filesize(char *filename);
int fs_userdata_write(char *filename, char *buffer, int buffer_size);
int fs_userdata_read(char *filename, char *buffer, int buffer_size);
int fs_userdata_init();
int fs_userdata_deinit();
char *fs_userdata_next_filename(char *filename);
int fs_userdata_deletefile(char *filename);

#endif
