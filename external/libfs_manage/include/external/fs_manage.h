/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_MANAGE_H__
#define __FS_MANAGE_H__

int64_t fs_manage_lseek(int fd, int64_t offset, int whence);
ssize_t fs_manage_read(int fd, void *buf, size_t count);
ssize_t fs_manage_write(int fd, const void *buf, size_t count);
int fs_manage_open(const char *filename, int flags, ...);
int fs_manage_close(int fd);
FILE *fs_manage_fopen(const char *file_name, const char *mode);
int fs_manage_fclose(FILE *fp);
int fs_manage_writetrail(int fd);
int fs_manage_get_tailoff(int fd);
int fs_manage_blocknotify(int prev_num, int later_num, char *filename);
int fs_manage_fstat(int fd, struct stat *filestat);

#endif
