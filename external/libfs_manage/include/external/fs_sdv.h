/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_SDV_H__
#define __FS_SDV_H__

typedef enum _FS_MODE {
	FS_CVR = 0,
	FS_SDV,
	FS_DVR
} FS_MODE;

FS_MODE fs_get_mode();
void fs_set_mode(FS_MODE mode);
int fs_manage_init(int (*callback)(void *, int), void *arg);
int fs_manage_deinit(void);

#endif
