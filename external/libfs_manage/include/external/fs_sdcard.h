/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_SDCARD_H__
#define __FS_SDCARD_H__

#define SDCARD_PATH "/mnt/sdcard"
#define SDCARD_DEVICE "/dev/mmcblk1"
#define SDCARD_PARTITION "/dev/mmcblk1p1"

//#define SUPPORT_EXFAT
typedef enum _FILESYSTYPE {
	VFAT = 0,
	EXFAT,
	EXT2,
	EXT3,
	EXT4,
	UNSUPPROT
} FILESYSTYPE;

typedef enum _FORMATTYPE {
	FORMAT_NORMAL = 0,
	FORMAT_SPECICAL
} FORMATTYPE;

typedef enum _SDMOUNTSTAT {
	SD_UMOUNT = 0,
	SD_MOUNTED
} SDMOUNTSTAT;

typedef void (*FS_MOUNT_CALLBACK)(int);

int fs_manage_sd_exist(const char *sdcard_path);
int fs_manage_sdcard_mount(FS_MOUNT_CALLBACK callback);
int fs_manage_sdcard_unmount();
int fs_manage_sdcard_force_unmount(void);

int fs_manage_format_sdcard(int (*callback)(void *, int), void *arg, int type);
int fs_sdcard_check_capacity(long long *free_size, long long *total_size,
                             int *fblock_size);
void fs_manage_set_device(const char *sdcard_device);
char *fs_manage_get_device();
inline SDMOUNTSTAT fs_sdcard_get_mount();

#endif

