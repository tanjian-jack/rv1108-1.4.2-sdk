/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_MSG_H__
#define __FS_MSG_H__

typedef enum _FILE_NOTIFY{
	FILE_NEW = 0, /* Recording now */
	FILE_END, /* Record end */
	FILE_DEL, /* Delete file */
	FILE_COLLI, /* Add collision file */
	FILE_FULL, /* Storage space full */
	FILE_OVERLONG, /* File size overlong */
	CARD_COM,
	CARD_BAD,
	CARD_SLOW,
	FS_BAD,
	FS_NOSPC,
	SPEED_ADJ,
}FILE_NOTIFY;

typedef void (*FS_EVENT_CALLBACK)(int cmd, void *msg0, void *msg1);

void fs_msg_file_reg_callback(FS_EVENT_CALLBACK call);
void fs_msg_notify_file(FILE_NOTIFY notify, const char *filename, int arg);


#endif

