/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FS_CACHE_H_
#define _FS_CACHE_H_

#include <unistd.h>
#include <stdint.h>
#include "fs_err.h"
#define TIMEOUT 3
#define TIMEOUT_MIN_SIZE (1 * 1024)
//#define FS_PERFORMANCE 1

typedef enum _FS_COMMAND {
	FS_CMD_OPEN = 0,
	FS_CMD_CLOSE,
	FS_CMD_WRITE,
	FS_CMD_SEEK,
	FS_CMD_READ,
	FS_CMD_TAIL,
	FS_CMD_REPAIR,
	FS_CMD_NOMSG, /* The file io don't enter cache operation */
	FS_CMD_EXIT,
	FS_CMD_BLOCK,
	FS_CMD_PIC,
	FS_CMD_WRITE_SIZE,
	FS_CMD_RENAME_CUR,
	FS_CMD_RENAME_PRE,
	FS_CMD_UNKNOW
} FS_COMMAND;

typedef struct _FS_WRITE_ARG {
	void *buf;
	size_t count;
} FS_WRITE_ARG;

typedef struct _FS_OPEN_ARG {
	int flag;
	int mode;
} FS_OPEN_ARG;

typedef struct _FS_SEEK_ARG {
	int offset; /* Only support 32bit */
	int whence;
} FS_SEEK_ARG;

typedef struct _FS_BLOCK_ARG {
	int prev_num; /*Only support 32bit */
	int later_num;
} FS_BLOCK_ARG;

typedef struct _FS_CACHE_CTL {
	int filesize;
	int filespace;
	/* Cache length */
	int cache_size;
	/* Cache begin write to file */
	int cache_write;
	/* If data leng greater than (cache_size - cache_limit) will block */
	int cache_limit;
	/* Current wait copy into cache size */
	volatile int cache_count;
	int cache_count_max;

	size_t buffer_offset;
	size_t buffer_offset_end;
	char *buffer;

	size_t file_offset_start;
	size_t file_offset_end;
	size_t file_offset_last;

	int mediasize_flag;
	char mediasize[4];
	int media_offset;
	int mediatrail_flag;
	int block_flag;
	int delet_flag;
#ifdef FS_PERFORMANCE
	int write_max_time;
	int write_total_time;
	int write_total_num;
#endif
	struct timeval timeout;
	int filepre_flag;
	int trail_flag;
	int tail_offset;
	int mediasize_offset;
	int delay_num;
} FS_CACHE_CTL;

typedef struct _FS_MSG {
	struct _FS_MSG *pre;
	struct _FS_MSG *next;
	struct _FS_MSG *root_msg;
	int fake_fd;
	int real_fd;
	char *filename;
	FS_COMMAND command;
	FS_WRITE_ARG write_arg;
	int buffer_size;
	/*control buffer size*/
	FS_CACHE_CTL *cache_ctl;
} FS_MSG;

int64_t fs_cache_file_seek(int fd, int64_t offset, int whence);
ssize_t fs_cache_file_write(int fd, const void *buf, size_t count);
ssize_t fs_cache_file_read(int fd, const void *buf, size_t count);
int fs_cache_file_open(const char *filename, int flags, int mode);
int fs_cache_file_close(int fd);
FS_RET fs_fs_cache_init();
FS_RET fs_fs_cache_deinit();
int fs_cache_file_writetrail(int fd);
ssize_t fs_cache_picture_write(char *filename, void *buf, size_t count);

#endif
