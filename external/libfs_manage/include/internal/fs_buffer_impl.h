/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef __FS_BUFFER_IMPL_H__
#define __FS_BUFFER_IMPL_H__

#include "fs_list.h"
#include "fs_err.h"
#include "fs_buffer.h"
#include <pthread.h>

extern unsigned int fs_buffer_debug;

#define FS_BUF_DBG_FUNCTION            (0x00000001)
#define FS_BUF_DBG_OPS_RUNTIME         (0x00000002)
#define FS_BUF_DBG_OPS_HISTORY         (0x00000004)
#define FS_BUF_DBG_CLR_ON_EXIT         (0x00000008)

#define fs_buf_dbg(flag, fmt, ...)     _fs_dbg(fs_buffer_debug, flag, fmt, ## __VA_ARGS__)
#define fs_buf_dbg_f(flag, fmt, ...)   _fs_dbg_f(fs_buffer_debug, flag, fmt, ## __VA_ARGS__)

#define FS_BUF_FUNCTION_ENTER()        fs_buf_dbg_f(FS_BUF_DBG_FUNCTION, "enter\n")
#define FS_BUF_FUNCTION_LEAVE()        fs_buf_dbg_f(FS_BUF_DBG_FUNCTION, "leave\n")
#define FS_BUF_FUNCTION_LEAVE_OK()     fs_buf_dbg_f(FS_BUF_DBG_FUNCTION, "success\n")
#define FS_BUF_FUNCTION_LEAVE_FAIL()   fs_buf_dbg_f(FS_BUF_DBG_FUNCTION, "failed\n")

#define FS_TAG_SIZE            32
#define FS_TAG_NAME            "rockchip"

/* use index instead of pointer to avoid invalid pointer */
typedef struct _FsBufferImpl {
	char                tag[FS_TAG_SIZE];
	unsigned int group_id;
	int buffer_id;

	FsBufferInfo info;

	/*
	 * Used for buf on group reset mode set disard value to 1 when frame
	 * refcount no zero, we will delay relesase buffer after refcount to zero,
	 * not put this buf to unused list int discard.
	 */

	/* used flag is for used/unused list detection */
	unsigned int used;
	unsigned int internal;
	int ref_count;
	void *buffergroup;
	struct list_head list_status;
} FsBufferImpl;

typedef struct _FsBufferGroupImpl {

	/* used in limit mode only */
	char                tag[FS_TAG_SIZE];
	size_t              limit_size;
	signed int          limit_count;
	pthread_mutex_t grouplock;
	/* status record */
	size_t              limit;
	size_t              usage;
	signed int          buffer_id;
	signed int          buffer_count;
	signed int          count_used;
	signed int          count_unused;

	/* buffer force clear mode flag */
	unsigned int        clear_on_exit;

	/* buffer log function */
	unsigned int        log_runtime_en;
	unsigned int        log_history_en;
	unsigned int        log_count;

	/* link to list_status in FsBufferImpl */
	struct list_head    list_used;
	struct list_head    list_unused;
} FsBufferGroupImpl;

/*
 *  fs_buffer_create       : create a unused buffer with parameter tag/size/data
 *                            buffer will be register to unused list
 *
 *  fs_buffer_destroy      : destroy a buffer, it must be on unused status
 *
 *  fs_buffer_get_unused   : get unused buffer with size. it will first search
 *                            the unused list. if failed it will create on from
 *                            group allocator.
 *
 *  fs_buffer_ref_inc      : increase buffer's reference counter. if it is unused
 *                            then it will be moved to used list.
 *
 *  fs_buffer_ref_dec      : decrease buffer's reference counter. if the reference
 *                            reduce to zero buffer will be moved to unused list.
 *
 * normal call flow will be like this:
 *
 * fs_buffer_create        - create a unused buffer
 * fs_buffer_get_unused    - get the unused buffer
 * fs_buffer_ref_inc/dec   - use the buffer
 * fs_buffer_destory       - destroy the buffer
 */
FS_RET fs_buffer_create(FsBufferGroupImpl *group, const char *tag,
                        FsBufferInfo *info);
FS_RET fs_buffer_destroy(FsBufferImpl *buffer);
FS_RET fs_buffer_ref_inc(FsBufferImpl *buffer);
FS_RET fs_buffer_ref_dec(FsBufferImpl *buffer);
FsBufferImpl *fs_buffer_get_unused(FsBufferGroupImpl *p, size_t size);

FS_RET fs_buffer_group_init(FsBufferGroupImpl **group, const char *tag);
FS_RET fs_buffer_group_deinit(FsBufferGroupImpl *p);
FS_RET fs_buffer_group_reset(FsBufferGroupImpl *p);
/* fs_buffer_group helper function */
void fs_buffer_group_dump(FsBufferGroupImpl *p);
FS_RET fs_buffer_group_limit_config(FsBufferGroupImpl *group, size_t size,
                                    signed int count);
void fs_buffer_info_dump(FsBufferImpl *buffer);

#endif /*__FS_BUFFER_IMPL_H__*/
