/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FS_LOG_H__
#define __FS_LOG_H__

#include <stdio.h>
#include <stdlib.h>

/*
 * fs runtime log system usage:
 * fs_err is for error status message, it will print for sure.
 * fs_log is for important message like open/close/reset/flush, it will print too.
 * fs_dbg is for all optional message. it can be controlled by debug and flag.
 */

#define fs_log(fmt, ...)   _fs_log(MODULE_TAG, fmt, NULL, ## __VA_ARGS__)
#define fs_err(fmt, ...)   _fs_err(MODULE_TAG, fmt, NULL, ## __VA_ARGS__)

#define _fs_dbg(debug, flag, fmt, ...) \
             do { \
                if (debug & flag) \
                    fs_log(fmt, ## __VA_ARGS__); \
             } while (0)

#define fs_dbg(flag, fmt, ...) _fs_dbg(fs_debug, flag, fmt, ## __VA_ARGS__)

/*
 * _f function will add function name to the log
 */
#define fs_log_f(fmt, ...)  _fs_log(MODULE_TAG, fmt, __FUNCTION__, ## __VA_ARGS__)
#define fs_err_f(fmt, ...)  _fs_err(MODULE_TAG, fmt, __FUNCTION__, ## __VA_ARGS__)
#define _fs_dbg_f(debug, flag, fmt, ...) \
            do { \
               if (debug & flag) \
                   fs_log_f(fmt, ## __VA_ARGS__); \
            } while (0)

#define fs_dbg_f(flag, fmt, ...) _fs_dbg_f(fs_debug, flag, fmt, ## __VA_ARGS__)


#define FS_TIMING                      (0x00000001)
#define FS_ABORT                       (0x10000000)


#define fs_abort() do {                \
    if (fs_debug & FS_ABORT) {        \
        abort();                        \
    }                                   \
} while (0)

#define FS_STRINGS(x)      FS_TO_STRING(x)
#define FS_TO_STRING(x)    #x

#define fs_assert(cond) do {                                           \
    if (!(cond)) {                                                      \
        fs_err("Assertion %s failed at %s:%d\n",                       \
               FS_STRINGS(cond), __FUNCTION__, __LINE__);              \
        fs_abort();                                                    \
    }                                                                   \
} while (0)

extern unsigned int fs_debug;


#endif /*__FS_LOG_H__*/
