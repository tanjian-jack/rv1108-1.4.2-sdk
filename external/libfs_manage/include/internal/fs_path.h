/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FS_PATH_H_
#define _FS_PATH_H_

#include <autoconfig/libfs_manage_autoconf.h>

#include "fs_conf.h"

/* get in from app/video/video.cpp */
#define USB_TYPE_YUYV 0
#define USB_TYPE_MJPEG 1
#define USB_TYPE_H264 2
#define VIDEO_TYPE_ISP 3
#define VIDEO_TYPE_CIF 4
#define VIDEO_TYPE_USB 5
#define VIDEO_TYPE_CIF0 4
#define VIDEO_TYPE_CIF1 6
#define VIDEO_TYPE_CIF2 7
#define VIDEO_TYPE_CIF3 8

#define VIDEO_TYPE_ISP_LOCK 33
#define VIDEO_TYPE_CIF_LOCK 34
#define VIDEO_TYPE_USB_LOCK 35

#if ENABLE_SDV
#define PATH_NUM 0
#else
#define PATH_NUM 3
#endif

#if (PATH_NUM == 3)
FS_STORAGE_PATH fs_storage_tbl[] = {
	{
		.storage_path	 = "/mnt/sdcard/VIDEO-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 57,
		.filesize		 = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype		 = VIDEOFILE_TYPE,
		.video_type 	 = VIDEO_TYPE_ISP,
		.colli_path 	 = "/mnt/sdcard/LOCK-FRONT",
		.thumb_path 	 = "/mnt/sdcard/.MISC/.THUMB-FRONT",
	},
	{
		.storage_path	 = "/mnt/sdcard/PHOTO-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 2,
		.filesize		 = 3 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype		 = PICFILE_TYPE,
		.video_type 	 = VIDEO_TYPE_ISP,
	},
#ifdef CACHE_ENCODEDATA_IN_MEM
	{
		.storage_path	 = "/mnt/sdcard/LOCK-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 6,
		.filesize		 = 24 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype		 = LOCKFILE_TYPE,
		.video_type 	 = VIDEO_TYPE_ISP,
		.thumb_path 	 = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
	},
	{
		.storage_path	 = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize		 = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#else
	{
		.storage_path    = "/mnt/sdcard/LOCK-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 6,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.thumb_path 	 = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
	},
	{
		.storage_path	 = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
		.colli_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO-REAR",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 11,
		.filesize        = 24 * FS_SZ_1M,
		.filesize_align  = USB_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.colli_path      = "/mnt/sdcard/LOCK-REAR",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-REAR",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0.67,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
	},
#ifdef CACHE_ENCODEDATA_IN_MEM
	{
		.storage_path    = "/mnt/sdcard/LOCK-REAR",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 2.25,
		.filesize        = 8 * FS_SZ_1M,
		.filesize_align  = USB_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-REAR",
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-LOCK-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#else
	{
		.storage_path    = "/mnt/sdcard/LOCK-REAR",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 2.25,
		.filesize        = 24 * FS_SZ_1M,
		.filesize_align  = USB_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-REAR",
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-LOCK-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.path_attri      = FS_PATH_HIDE,
		.colli_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-REAR",
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO-CIF",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 11,
		.filesize        = 24 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF,
		.colli_path       = "/mnt/sdcard/LOCK-CIF",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-CIF",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO-CIF",
		.storage_format  = ".jpg",
		.storage_percent = 0.67,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF,
	},
#ifdef CACHE_ENCODEDATA_IN_MEM
	{
		.storage_path    = "/mnt/sdcard/LOCK-CIF",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 2.25,
		.filesize        = 8 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-CIF",
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-LOCK-CIF",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#else
	{
		.storage_path    = "/mnt/sdcard/LOCK-CIF",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 2.25,
		.filesize        = 24 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-CIF",
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-LOCK-CIF",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF_LOCK,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-CIF",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF,
		.path_attri      = FS_PATH_HIDE,
		.colli_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-CIF",
	},
#ifdef GPS
	{
		.storage_path    = "/mnt/sdcard/.MISC/.GPS",
		.storage_format  = ".txt",
		.storage_percent = 0.1,
		.filesize        = GPS_FILE_ALIGN,
		.filetype        = GPS_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.filesize_align  = GPS_FILE_ALIGN,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
};
#elif (PATH_NUM == 1)
FS_STORAGE_PATH fs_storage_tbl[] = {
	{
		.storage_path    = "/mnt/sdcard/VIDEO-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 80,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.colli_path      = "/mnt/sdcard/LOCK-FRONT",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-FRONT",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 7,
		.filesize        = 3 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-PHOTO-FRONT",
	},
#ifdef CACHE_ENCODEDATA_IN_MEM
	{
		.storage_path    = "/mnt/sdcard/LOCK-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7,
		.filesize        = 24 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-LOCK-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
	},
#else
	{
		.storage_path    = "/mnt/sdcard/LOCK-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
	},
#endif
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-PHOTO-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
	},
#ifdef GPS
	{
		.storage_path    = "/mnt/sdcard/.MISC/.GPS",
		.storage_format  = ".txt",
		.storage_percent = 0.1,
		.filesize        = GPS_FILE_ALIGN,
		.filetype        = GPS_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.filesize_align  = GPS_FILE_ALIGN,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
};
// Just for sport dv
#elif (PATH_NUM == 0)
FS_STORAGE_PATH fs_storage_tbl[] = {
	{
		.storage_path    = "/mnt/sdcard/VIDEO",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.thumb_path      = "/mnt/sdcard/.MISC",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO",
		.storage_format  = ".jpg",
		.filesize        = 3 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC",
		.storage_format  = ".jpg",
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
	},
#ifdef GPS
	{
		.storage_path    = "/mnt/sdcard/.MISC/.GPS",
		.storage_format  = ".txt",
		.storage_percent = 0.1,
		.filesize        = GPS_FILE_ALIGN,
		.filetype        = GPS_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.filesize_align  = GPS_FILE_ALIGN,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
};
#elif (PATH_NUM == 6)
FS_STORAGE_PATH fs_storage_tbl[] = {
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 40,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.colli_path      = "/mnt/sdcard/LOCK/LOCK-FRONT",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-FRONT",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 2,
		.filesize        = 3 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-FRONT",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 6,
		.filesize        = 80 * FS_SZ_1M,
		.filesize_align  = ISP_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-FRONT",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-REAR",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7.5,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = USB_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.colli_path      = "/mnt/sdcard/LOCK/LOCK-REAR",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-REAR",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0.5,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-REAR",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 1,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = USB_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-REAR",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_USB,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-CIF0",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7.5,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF0,
		.colli_path      = "/mnt/sdcard/LOCK/LOCK-CIF0",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-CIF0",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-CIF0",
		.storage_format  = ".jpg",
		.storage_percent = 0.5,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF0,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-CIF0",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 1,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF0,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-CIF0",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF0,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-CIF1",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7.5,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF1,
		.colli_path       = "/mnt/sdcard/LOCK/LOCK-CIF1",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-CIF1",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-CIF1",
		.storage_format  = ".jpg",
		.storage_percent = 0.5,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF1,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-CIF1",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 1,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF1,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-CIF1",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF1,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-CIF2",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7.5,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF2,
		.colli_path       = "/mnt/sdcard/LOCK/LOCK-CIF2",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-CIF2",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-CIF2",
		.storage_format  = ".jpg",
		.storage_percent = 0.5,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF2,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-CIF2",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 1,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF2,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-CIF2",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF2,
		.path_attri      = FS_PATH_HIDE,
	},
	{
		.storage_path    = "/mnt/sdcard/VIDEO/VIDEO-CIF3",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 7.5,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = VIDEOFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF3,
		.colli_path       = "/mnt/sdcard/LOCK/LOCK-CIF3",
		.thumb_path      = "/mnt/sdcard/.MISC/.THUMB-CIF3",
	},
	{
		.storage_path    = "/mnt/sdcard/PHOTO/PHOTO-CIF3",
		.storage_format  = ".jpg",
		.storage_percent = 0.5,
		.filesize        = 1 * FS_SZ_1M,
		.filesize_align  = PIC_FILE_ALIGN,
		.filetype        = PICFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF3,
	},
	{
		.storage_path    = "/mnt/sdcard/LOCK/LOCK-CIF3",
		.storage_format  = "."MAIN_APP_RECORD_FORMAT,
		.storage_percent = 1,
		.filesize        = 16 * FS_SZ_1M,
		.filesize_align  = CIF_FILE_ALIGN,
		.filetype        = LOCKFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF3,
	},
	{
		.storage_path    = "/mnt/sdcard/.MISC/.THUMB-CIF3",
		.storage_format  = ".jpg",
		.storage_percent = 0,
		.filesize        = 128 * FS_SZ_1K,
		.filetype        = THUMBFILE_TYPE,
		.video_type      = VIDEO_TYPE_CIF3,
		.path_attri      = FS_PATH_HIDE,
	},
#ifdef GPS
	{
		.storage_path    = "/mnt/sdcard/.MISC/.GPS",
		.storage_format  = ".txt",
		.storage_percent = 0.1,
		.filesize        = GPS_FILE_ALIGN,
		.filetype        = GPS_TYPE,
		.video_type      = VIDEO_TYPE_ISP,
		.filesize_align  = GPS_FILE_ALIGN,
		.path_attri      = FS_PATH_HIDE,
	},
#endif
};
#endif

#define STORAGE_PATH_NUM (sizeof(fs_storage_tbl) / sizeof(FS_STORAGE_PATH))
int storage_path_num = (sizeof(fs_storage_tbl) / sizeof(FS_STORAGE_PATH));

int fs_current_filetype_tbl[] = {
	VIDEO_TYPE_ISP,
#if (PATH_NUM > 1)
	VIDEO_TYPE_USB,
#endif
#if (PATH_NUM > 2)
	VIDEO_TYPE_CIF,
#endif
};

#define ENABLE_CAP_ADJ
#ifdef  ENABLE_CAP_ADJ

/*
 * The cap_readj is percent of capacity, will readjust capacity.
 * Such as there are 3 path:isp,usb,cif. Isp video is  112MB,
 * usb video is 16MB, cif vide is 32MB, so isp video capacity
 * precent will adjust to 80 * (112 / (112 + 16 + 32)).
 */
#define FS_CAP_READJ_VIDEO  (80)
#define FS_CAP_READJ_PHOTO  (5)
#define FS_CAP_READJ_LOCK   (10)

//#define FS_CAP_CHANGE_PER
#define FS_CAP_PER_FROM (30000) /* 32G */
#endif

#endif
