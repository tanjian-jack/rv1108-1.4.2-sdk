/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <sys/prctl.h>

#include "fs_sdv.h"
#include "fs_storage.h"
#include "fs_err.h"
#include "fs_cache.h"
#include "fs_log.h"
#include "fs_conf.h"

#if ENABLE_SDV
FS_MODE g_fs_mode = FS_SDV;
#else
FS_MODE g_fs_mode = FS_CVR;
#endif

int g_sdv_pthread = 0;
extern long long tf_free_size;
extern long long tf_total_size;
extern pthread_mutex_t filelistlock;
extern pthread_t fs_manage_tid;
static pthread_t fs_sdv_tid;
extern int m_fileopera_msgid;

FS_MODE fs_get_mode()
{
	return g_fs_mode;
}

void fs_set_mode(FS_MODE mode)
{
	if ((mode != FS_CVR) && (mode != FS_SDV) && (mode != FS_DVR))
		fs_assert(0);
	g_fs_mode = mode;
}

void *fs_sdv_pthread(void *arg)
{
	int n = 0;

	g_sdv_pthread = 1;
	prctl(PR_SET_NAME, "fs_sdv", 0, 0, 0);

	while(g_sdv_pthread) {
		for(n = 0; n < 20 && g_sdv_pthread; n++)
			sleep(1);
		fs_sdcard_check_capacity(&tf_free_size, &tf_total_size, NULL);
	}
	pthread_exit(0);
}

void fs_sdv_check_folder(void)
{
	int number;
	char *path;
	FS_STORAGE_PATH *fs_storage_path;

	fs_storage_foreach_path(number, fs_storage_path) {
		path = fs_storage_path->storage_path;
		if (*path == 0)
			continue;
		fs_file_create_path(fs_storage_path->storage_path,
		                    fs_storage_path->path_attri);
	}
}

void fs_sdv_create_list(FS_MODE mode)
{
	int number;
	struct file_list **filelist;
	FS_STORAGE_PATH *fs_storage_path;

	fs_storage_foreach_path(number, fs_storage_path) {
		filelist = &fs_storage_path->filelist;
		fs_storage_filelist_init(fs_storage_path, filelist);
		if (mode == FS_DVR) {
			pthread_mutex_lock(&filelistlock);
			creat_filelist(*filelist);
			pthread_mutex_unlock(&filelistlock);
		}
	}
}

int fs_sdv_init(FS_MODE mode)
{
	FS_RET ret;

	fs_sdcard_check_capacity(&tf_free_size, &tf_total_size, NULL);
	if (fs_manage_sd_exist(NULL) == 0) {
		fs_err_f("No SD Card\n");
		ret = -3;
		goto RET4;
	}
	fs_sdv_check_folder();

	if (pthread_mutex_init(&filelistlock, NULL) != 0) {
		fs_err_f("filelistlock init fail \n");
		ret = -3;
		goto RET4;
	}

	ret = fileopera_ipcmsg_init(RK_FILEOPERA_MSGKEY, &m_fileopera_msgid);
	if (ret != FS_OK) {
		fs_err_f("fileopera_ipcmsg_init fail \n");
		ret = -3;
		goto RET3;
	}

	if (pthread_create(&fs_manage_tid, NULL, fs_storage_pthread, NULL)) {
		fs_err_f("pthread_create err\n");
		ret = -3;
		goto RET2;
	}

	fs_sdv_create_list(mode);

	if (fs_sdv_tid == 0) {
		if (pthread_create(&fs_sdv_tid, NULL, fs_sdv_pthread, NULL)) {
			fs_err_f("pthread_create err\n");
			ret = -3;
			goto RET1;
		}
	}

	ret = fs_cache_init();
	if (ret != FS_OK) {
		ret = -3;
		goto RET0;
	}

	return 0;

RET0:
	fileopera_ipcmsg_send_cmd(0, NULL, NULL, FS_CMD_EXIT);
	pthread_join(fs_manage_tid, NULL);
	fs_manage_tid = 0;
RET1:
	g_sdv_pthread = 0;
	pthread_join(fs_sdv_tid, NULL);
	fs_sdv_tid = 0;
RET2:
	fileopera_ipcmsg_deinit(RK_FILEOPERA_MSGKEY, &m_fileopera_msgid);
RET3:
	pthread_mutex_destroy(&filelistlock);
RET4:
	return ret;
}

int fs_sdv_deinit(void)
{
	fs_cache_deinit();
	if (fs_sdv_tid) {
		g_sdv_pthread = 0;
		pthread_join(fs_sdv_tid, NULL);
		fs_sdv_tid = 0;
	}

	if (fs_manage_tid) {
		fileopera_ipcmsg_send_cmd(0, NULL, NULL, FS_CMD_EXIT);
		pthread_join(fs_manage_tid, NULL);
		fs_manage_tid = 0;
	}
	pthread_mutex_destroy(&filelistlock);
	fs_manage_sdcard_unmount();

	return 0;
}

int fs_manage_init(int (*callback)(void *, int), void *arg)
{
	int ret;
	FS_MODE mode;

	mode = fs_get_mode();
	if (mode == FS_CVR)
		ret = fs_storage_init(callback, arg);
	else
		ret = fs_sdv_init(mode);

	return ret;
}

int fs_manage_deinit(void)
{
	FS_MODE mode;

	mode = fs_get_mode();
	if (mode == FS_CVR)
		fs_storage_deinit();
	else if (mode == FS_SDV)
		fs_sdv_deinit();

	return 0;
}
