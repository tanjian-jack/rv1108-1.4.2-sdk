/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#define MODULE_TAG "fs_buffer"

#include <string.h>

#include "fs_buffer.h"
#include "fs_buffer_impl.h"
#include "fs_log.h"
#include "fs_err.h"

FsBufferGroup mbuffergroup = NULL;

void fs_buffer_set_mgroup(FsBufferGroup buffer_group)
{
	mbuffergroup = buffer_group;
}

FsBufferGroup fs_buffer_get_mgroup()
{
	return mbuffergroup;
}

/* just prepare buffer, but no use */
FS_RET fs_buffer_commit_with_tag(const char *tag, FsBufferGroup group, size_t size)
{
	if (NULL == group) {
		fs_err("fs_buffer_commit input null pointer group %p size %d\n",
		       group, size);
		return FS_ERR_NULL_PTR;
	}

	FsBufferGroupImpl *p = (FsBufferGroupImpl *)group;
	FsBufferInfo info = {
		size,
		NULL,
	};
	return fs_buffer_create(p, tag, &info);
}

FS_RET fs_buffer_get_with_tag(const char *tag,  FsBufferGroup group, FsBuffer *buffer,
                              size_t size)
{
	if (NULL == buffer || 0 == size) {
		fs_err("fs_buffer_get invalid input: group %p buffer %p size %u\n",
		      group, buffer, size);
		return FS_ERR_NULL_PTR;
	}

	if (NULL == group) {
		// deprecated, only for libvpu support
		// todo
		//group = fs_buffer_legacy_group();
		return FS_ERR_NULL_PTR;
	}

	fs_assert(group);

	FsBufferGroupImpl *p = (FsBufferGroupImpl *)group;
	/* try unused buffer first */
	FsBufferImpl *buf = fs_buffer_get_unused(p, size);
	if (NULL == buf) {
		FsBufferInfo info = {
			size,
			NULL,
		};
		/* if failed try init a new buffer */
		fs_buffer_create(p, tag, &info);
		buf = fs_buffer_get_unused(p, size);
	}
	*buffer = buf;
	return (buf) ? (FS_OK) : (FS_NOK);
}

FS_RET fs_buffer_put(FsBuffer buffer)
{
	if (NULL == buffer) {
		fs_err("fs_buffer_put invalid input: buffer %p\n", buffer);
		return FS_ERR_UNKNOW;
	}

	return fs_buffer_ref_dec((FsBufferImpl *)buffer);
}

FS_RET fs_buffer_inc_ref(FsBuffer buffer)
{
	if (NULL == buffer) {
		fs_err("fs_buffer_inc_ref invalid input: buffer %p\n", buffer);
		return FS_ERR_UNKNOW;
	}

	return fs_buffer_ref_inc((FsBufferImpl *)buffer);
}

FS_RET fs_buffer_dec_ref(FsBuffer buffer)
{
	if (NULL == buffer) {
		fs_err("fs_buffer_dec_ref invalid input: buffer %p\n", buffer);
		return FS_ERR_UNKNOW;
	}

	return fs_buffer_ref_dec((FsBufferImpl *)buffer);
}

FS_RET fs_buffer_read(FsBuffer buffer, size_t offset, void *data, size_t size)
{
	if (NULL == buffer || NULL == data) {
		fs_err_f("invalid input: buffer %p data %p\n", buffer, data);
		return FS_ERR_UNKNOW;
	}

	if (0 == size)
		return FS_OK;

	FsBufferImpl *p = (FsBufferImpl *)buffer;
	void *src = p->info.ptr;
	fs_assert(src != NULL);
	memcpy(data, (char *)src + offset, size);
	return FS_OK;
}

FS_RET fs_buffer_write(FsBuffer buffer, size_t offset, const char *data, size_t size)
{
	if (NULL == buffer || NULL == data) {
		fs_err_f("invalid input: buffer %p data %p\n", buffer, data);
		return FS_ERR_UNKNOW;
	}

	if (0 == size)
		return FS_OK;

	FsBufferImpl *p = (FsBufferImpl *)buffer;
	void *dst = p->info.ptr;
	fs_assert(dst != NULL);
	memcpy((char *)dst + offset, data, size);
	return FS_OK;
}

void *fs_buffer_get_ptr(FsBuffer buffer)
{
	if (NULL == buffer) {
		fs_err_f("invalid NULL input\n");
		return NULL;
	}

	FsBufferImpl *p = (FsBufferImpl *)buffer;
	return p->info.ptr;
}

size_t fs_buffer_get_size(FsBuffer buffer)
{
	if (NULL == buffer) {
		fs_err_f("invalid NULL input\n");
		return -1;
	}

	FsBufferImpl *p = (FsBufferImpl *)buffer;
	return p->info.size;
}

FS_RET fs_buffer_info_get(FsBuffer buffer, FsBufferInfo **info)
{
	if (NULL == buffer || NULL == info) {
		fs_err_f("invalid input: buffer %p info %p\n", buffer, info);
		return FS_ERR_NULL_PTR;
	}

	*info = (FsBufferInfo *)&(((FsBufferImpl *)buffer)->info);
	return FS_OK;
}

FS_RET fs_buffer_group_get(const char *tag, FsBufferGroup *group)
{
	return fs_buffer_group_init((FsBufferGroupImpl **)group, tag);
}

FS_RET fs_buffer_group_put(FsBufferGroup group)
{
	if (NULL == group) {
		fs_err_f("input invalid group %p\n", group);
		return FS_ERR_NULL_PTR;
	}

	fs_buffer_set_mgroup(NULL);
	return fs_buffer_group_deinit((FsBufferGroupImpl *)group);
}

FS_RET fs_buffer_group_clear(FsBufferGroup group)
{
	if (NULL == group) {
		fs_err_f("input invalid group %p\n", group);
		return FS_ERR_NULL_PTR;
	}

	return fs_buffer_group_reset((FsBufferGroupImpl *)group);
}

int fs_buffer_group_unused(FsBufferGroup group)
{
	if (NULL == group) {
		fs_err_f("input invalid group %p\n", group);
		return FS_ERR_NULL_PTR;
	}

	FsBufferGroupImpl *p = (FsBufferGroupImpl *)group;
	unsigned int no_creat_count = p->limit_count ?
	                    (p->limit_count - p->count_unused - p->count_used) : 0;

	return  p->count_unused;
}

FS_RET fs_buffer_group_config(FsBufferGroup group, size_t size, int count)
{
	if (NULL == group || size < 0 || count < 0) {
		fs_err_f("input invalid group %p\n", group);
		return FS_NOK;
	}

	fs_buffer_group_limit_config((FsBufferGroupImpl *)group, size, count);

	return FS_OK;
}

FS_RET fs_buffer_prepare_buffer(FsBufferGroup group, size_t size, int count)
{
	int      number;
	FS_RET   ret;
	FsBuffer fs_buffer_get;
	fs_buffer_group_dump((FsBufferGroupImpl *)group);
	for (number = 0; number < count; number++) {
		//ret = fs_buffer_get_with_tag(FS_TAG_NAME, group, &fs_buffer_get, size);
		//if (ret != FS_OK)
		//	break;
		//fs_buffer_info_dump((FsBufferImpl *)fs_buffer_get);
		//fs_buffer_dec_ref(fs_buffer_get);
		ret = fs_buffer_commit_with_tag(FS_TAG_NAME, group, size);
		if (ret != FS_OK)
			break;
	}

	fs_log("prepare number = %d\n", number);
	fs_buffer_group_dump((FsBufferGroupImpl *)group);

}

