/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include "fs_manage.h"
#include "fs_cache.h"
#include "fs_log.h"

FILE *fs_manage_fopen(const char *file_name, const char *mode)
{
	fs_err_f("Don't support\n");
	return NULL;
}

int fs_manage_open(const char *filename, int flags, ...)
{
	unsigned int mode = 0;
	va_list ap;

	va_start(ap, flags);
	if (flags & O_CREAT) {
		mode = va_arg(ap, unsigned int);
		va_end(ap);
	}
	return fs_cache_file_open(filename, flags, mode);
}

int fs_manage_fclose(FILE *fp)
{
	fs_err_f("Don't support\n");
	return 0;
}

int fs_manage_close(int fd)
{
	return fs_cache_file_close(fd);
}

ssize_t fs_manage_write(int fd, const void *buf, size_t count)
{
	return fs_cache_file_write(fd, buf, count);
}

ssize_t fs_manage_read(int fd, void *buf, size_t count)
{
	return fs_cache_file_read(fd, buf, count);
}

int64_t fs_manage_lseek(int fd, int64_t offset , int whence)
{
	return fs_cache_file_seek(fd, offset, whence);
}

int fs_manage_writetrail(int fd)
{
	return fs_cache_file_writetrail(fd);
}

int fs_manage_get_tailoff(int fd)
{
	return fs_cache_file_get_tailoff(fd);
}

int fs_manage_blocknotify(int prev_num, int later_num, char *filename)
{
	return fs_cache_file_blocknotify(prev_num, later_num, filename);
}

int fs_manage_fstat(int fd, struct stat *filestat)
{
	return fs_cache_file_fstat(fd, filestat);
}

