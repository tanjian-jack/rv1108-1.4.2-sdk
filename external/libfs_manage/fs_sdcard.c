/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_sdcard"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/statfs.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <linux/msdos_fs.h>

#include "fs_sdcard.h"
#include "fs_storage.h"
#include "fs_file.h"
#include "fs_err.h"
#include "fs_log.h"
#include "fs_msg.h"

#define COMMAND_BUFFER_SIZE 125
#define FORMAT_CAPACITY (16 * 1024)
#define BEST_BLOCK_SIZE (128 * 512)
#define PATITION_SIZE (2000)
#define PATTITION_MAX (99)

typedef struct _FORMAT_CALLBACK {
	int (*format_callback_func)(void *, int);
	void *format_callback_param;
	int type;
} FORMAT_CALLBACK;
FORMAT_CALLBACK msdcard_format_callback;

char m_sdcard_device[FILENAME_LEN];
static SDMOUNTSTAT g_mount_state = 0;

int fs_manage_checksdcardattri() ;
inline void fs_sdcard_set_mount(SDMOUNTSTAT state);
extern FS_RET fs_sdcard_create_newfile();
extern int fs_cache_param_set();

int fs_manage_runapp(char *cmd)
{
	int ret;
	char buffer[COMMAND_BUFFER_SIZE];
	FILE *read_fp;
	int chars_read;

	memset(buffer, 0, COMMAND_BUFFER_SIZE);
	read_fp = popen(cmd, "r");
	if (read_fp != NULL) {
		chars_read = fread(buffer, sizeof(char), COMMAND_BUFFER_SIZE - 1,
		                   read_fp);
		if (chars_read > 0)
			ret = 1;
		else
			ret = -1;
		pclose(read_fp);
	} else {
		ret = -1;
	}
	return ret;
}

int fs_sdcard_command_result(char *cmd, char *buffer, int buffer_size)
{
	int ret;
	FILE *read_fp;
	int chars_read;

	memset(buffer, 0, buffer_size);
	read_fp = popen(cmd, "r");
	if (read_fp != NULL) {
		chars_read = fread(buffer, sizeof(char), buffer_size - 1, read_fp);
		if (chars_read > 0)
			ret = 1;
		else
			ret = -1;
		pclose(read_fp);
	} else {
		ret = -1;
	}
	return ret;
}

char *fs_sdcard_get_device()
{
	return m_sdcard_device;
}

void fs_manage_set_device(const char *sdcard_device)
{
	char *cur_sdcard_device = fs_sdcard_get_device();
	char sdcard_device_path[FILENAME_LEN] = {0};
	char path[FILENAME_LEN] = {0};

	/* if the sdcard_device are full path,then will don't need path. */
	if (!strstr(sdcard_device, "/dev/"))
		strcpy(path, "/dev/");
	sprintf(sdcard_device_path, "%s%s\0", path, sdcard_device);
	if (sdcard_device == NULL)
		return;
	if (strcmp(cur_sdcard_device, sdcard_device_path) == 0)
		return;

	memset(cur_sdcard_device, 0, FILENAME_LEN);
	strcpy(cur_sdcard_device, sdcard_device_path);

	return;
}

int fs_sdcard_get_parnum()
{
	int parnum = 0;
	int partition = 0;
	char partition_path[FILENAME_LEN] = {0};

	for (partition = PATTITION_MAX; partition > 0 ; partition--) {
		sprintf(partition_path, "%sp%d\0", SDCARD_DEVICE, partition);
		if (access(partition_path, F_OK) == 0) {
			parnum++;
		}
	}

	return parnum;
}

/* old function api*/
int fs_manage_sd_exist(const char *sdcard_path)
{
	const char *path = NULL;

	if (sdcard_path)
		path = sdcard_path;
	else
		path = fs_sdcard_get_device();

	if (*path == 0) {
		int partition = 0;
		char partition_path[FILENAME_LEN] = {0};
		/*get path from /dev/mmcblk0p1 */
		for (partition = PATTITION_MAX; partition > 0 ; partition--) {
			sprintf(partition_path, "%sp%d\0", SDCARD_DEVICE, partition);
			if (access(partition_path, F_OK) == 0) {
				path = partition_path;
				break;
			}
		}

		if (partition == 0) {
			sprintf(partition_path, "%s\0", SDCARD_DEVICE);
			if (access(partition_path, F_OK) == 0) {
				path = partition_path;
			}
		}

		if (*path != 0)
			fs_manage_set_device(path);
	}
	return !access(path, F_OK);
}

int fs_sdcard_check_status(char *device)
{
	int ret = 0;
	FILESYSTYPE file_type = UNSUPPROT;
	char *sdcard_device = NULL;
	char blkdi_command[COMMAND_BUFFER_SIZE] = {0};
	char blkid_result[COMMAND_BUFFER_SIZE] = {0};

	if (device == NULL)
		sdcard_device = fs_sdcard_get_device();
	else
		sdcard_device = device;

	sprintf(blkdi_command, "fdisk -l %s\0", sdcard_device);
	return fs_sdcard_command_result(blkdi_command, blkid_result,
	                                COMMAND_BUFFER_SIZE);
}

FILESYSTYPE fs_sdcard_filesys_type(char *device)
{
	int ret = 0;
	FILESYSTYPE file_type = UNSUPPROT;
	char *sdcard_device = NULL;
	char blkdi_command[COMMAND_BUFFER_SIZE] = {0};
	char blkid_result[COMMAND_BUFFER_SIZE] = {0};

	if (device == NULL)
		sdcard_device = fs_sdcard_get_device();
	else
		sdcard_device = device;

	sprintf(blkdi_command, "blkid  %s\0", sdcard_device);
	ret = fs_sdcard_command_result(blkdi_command, blkid_result,
	                               COMMAND_BUFFER_SIZE);
	if (ret == -1)
		return file_type;

	if (strstr(blkid_result, "vfat") != NULL)
		file_type = VFAT;
	else if (strstr(blkid_result, "exfat") != NULL)
		file_type = EXFAT;
	else if (strstr(blkid_result, "ext2") != NULL)
		file_type = EXT2;
	else if (strstr(blkid_result, "ext3") != NULL)
		file_type = EXT3;
	else if (strstr(blkid_result, "ext4") != NULL)
		file_type = EXT4;
	else
		file_type = UNSUPPROT;
	return file_type;
}

int fs_sdcard_fsck(void)
{
	int ret = 0;
	char fsck_command[COMMAND_BUFFER_SIZE] = {0};
	char *sdcard_device = fs_sdcard_get_device();
	struct timeval tv1, tv2;
	int tm;

	gettimeofday(&tv1, NULL);

	if (fs_manage_sd_exist(NULL) == 0) {
		fs_err_f("No SD Card\n");
		return -1;
	}
	fs_manage_sdcard_unmount();
	FILESYSTYPE sdcard_type = UNSUPPROT;
	sdcard_type = fs_sdcard_filesys_type(NULL);
	if (sdcard_type == UNSUPPROT)
		ret = -1;
	else if (sdcard_type == VFAT) {
		sprintf(fsck_command, "./usr/local/sbin/fsck_modos -p %s\0", sdcard_device);
		fs_manage_runapp(fsck_command);
	} else if (sdcard_type == EXFAT) {
		sprintf(fsck_command, "fsck_exfat %s\0", sdcard_device);
		fs_manage_runapp(fsck_command);
	} else {
		sprintf(fsck_command, "fsck %s\0", sdcard_device);
		fs_manage_runapp(fsck_command);
	}

	gettimeofday(&tv2, NULL);
	tm = (tv2.tv_usec - tv1.tv_usec) / 1000 + (tv2.tv_sec - tv1.tv_sec) * 1000;
	fs_log("fs_manage_sdcard_fsck use %dmS sdcard_type = %d\n", tm, sdcard_type);
	return ret;
}

volatile int fs_sdcard_formating = 0;
int fs_sdcard_mount_type(int type)
{
	int ret = -1;
	long long free_size = 0;
	long long total_size = 0;
	char *sdcard_device = NULL;
	FILESYSTYPE sdcard_type = UNSUPPROT;

	if (fs_manage_sd_exist(NULL) == 0) {
		fs_err_f("No SD Card\n");
		return -2;
	}
	/* mount in external */
	if (fs_sdcard_formating == 1 && type == 0)
		return -3;
	fs_manage_sdcard_unmount();
	sdcard_device = fs_sdcard_get_device();
	sdcard_type = fs_sdcard_filesys_type(NULL);
	if (sdcard_type == VFAT)
		ret = mount(sdcard_device, SDCARD_PATH, "vfat", MS_NOATIME | MS_NOSUID,
                    "force_fallocate");
#ifdef SUPPORT_EXFAT
	else if (sdcard_type == EXFAT)
		ret = mount(sdcard_device, SDCARD_PATH, "exfat", MS_NOATIME | MS_NOSUID,
                    "force_fallocate");
#endif
	else {
		/* retry to get sdcard whether exist */
		ret = fs_sdcard_check_status(NULL);
		if (ret == -1)
			return -2;
		else
			return -1;
	}
	if (ret == 0) {
		fs_sdcard_check_capacity(&free_size, &total_size, NULL);
		/* if the partition more than two, will return error. */
		if (total_size < PATITION_SIZE) {
			fs_manage_sdcard_unmount();
			ret = -1;
		}
	} else {
		/* device be used now, the errno will be busy. */
		if (errno == EBUSY)
			ret = 0;
	}
	if (ret == 0)
		fs_sdcard_set_mount(SD_MOUNTED);
	return ret;
}

static void* fs_manage_sdcard_mount_thd(void *arg)
{
	int ret;
	FS_MOUNT_CALLBACK callback;

	fs_cache_param_set();
	callback = (FS_MOUNT_CALLBACK)arg;
	ret = fs_sdcard_mount_type(0);
	if (callback)
		callback(ret);
	pthread_detach(pthread_self());
	return NULL;
}

int fs_manage_sdcard_mount(FS_MOUNT_CALLBACK callback)
{
	pthread_t tid;

	if (pthread_create(&tid, NULL, fs_manage_sdcard_mount_thd,
	                   (void *)(callback))) {
		fs_log("%s pthread_create err\n", __func__);
		return -1;
	}
	return 0;
}

int fs_manage_sdcard_unmount(void)
{
	fs_sdcard_set_mount(SD_UMOUNT);
	return umount2(SDCARD_PATH, MNT_DETACH);
}

inline void fs_sdcard_set_mount(SDMOUNTSTAT state)
{
	g_mount_state = state;
}

inline SDMOUNTSTAT fs_sdcard_get_mount()
{
	return 	g_mount_state;
}

int fs_manage_format(int type)
{
	int ret;

	if (fs_manage_sd_exist(SDCARD_DEVICE) == 0) {
		fs_err_f("No SD Card\n");
		return -1;
	}

	if (type == FORMAT_SPECICAL) {
		fs_manage_runapp("mkfs.vfat /dev/mmcblk1");
		fs_manage_runapp("fdisk -t /dev/mmcblk1");
		fs_manage_runapp("mkfs.vfat -t /dev/mmcblk1p1");
		 /* judge the partition if noexist, format fail */
		if (access(SDCARD_PARTITION, F_OK) != 0)
			return -1;
	} else {
		char mkfs_command[COMMAND_BUFFER_SIZE] = {0};
		char *sdcard_device = fs_sdcard_get_device();
		sprintf(mkfs_command, "mkfs.vfat -t %s\0", sdcard_device);
		fs_manage_runapp(mkfs_command);
	}

	return 0;
}

void *fs_sdcard_format_thread(void *arg)
{
	int ret;
	FORMAT_CALLBACK *format_callback = (FORMAT_CALLBACK *)arg;

	prctl(PR_SET_NAME, "fs_sdcard_format", 0, 0, 0);

	fs_sdcard_formating = 1;
	fs_manage_deinit();
	fs_manage_sdcard_unmount();
	/* Create prepare file in mkfs.vfat */
	ret = fs_sdcard_set_newfile();
	if (ret < 0)
		goto RET;
	ret = fs_manage_format(format_callback->type);
	if (ret < 0) {
		fs_err_f("fs_manage_format fail errno = %d\n", errno);
		goto RET;
	}

	ret = fs_sdcard_mount_type(1);
	if (ret < 0) {
		fs_err_f("mount fail ret = %d, errno = %d\n", ret, errno);
		goto RET;
	}

RET:
	if (ret < 0)
		fs_file_sd_abn(CARD_BAD);
	fs_sdcard_formating = 0;
	if (format_callback->format_callback_func) {
		format_callback->format_callback_func(
		                           format_callback->format_callback_param, ret);
		format_callback->format_callback_func = NULL;
	}
	pthread_detach(pthread_self());
	pthread_exit(NULL);
	return NULL;
}

int fs_manage_format_sdcard(int (*callback)(void *, int), void *arg, int type)
{
	pthread_t tid;

	FORMAT_CALLBACK *sdcard_format_callback = &msdcard_format_callback;
	sdcard_format_callback->format_callback_func = callback;
	sdcard_format_callback->format_callback_param = arg;
	sdcard_format_callback->type = type;
	if (pthread_create(&tid, NULL,
                  fs_sdcard_format_thread, (void *)(sdcard_format_callback))) {
		fs_log("%s pthread_create err\n", __func__);
		return -1;
	}
	return 0;
}

int fs_sdcard_check_capacity(long long *free_size, long long *total_size,
                               int *fblock_size)
{
	struct statfs mystatfs;
	if (statfs(SDCARD_PATH, &mystatfs) == -1)
		return -1;

	*free_size = (((long long)mystatfs.f_bsize * (long long)mystatfs.f_bfree) /
                 (long long) 1024 / (long long) 1024);
	*total_size = (((long long)mystatfs.f_bsize * (long long)mystatfs.f_blocks) /
                 (long long) 1024 / (long long) 1024);
	if (fblock_size)
		*fblock_size = mystatfs.f_bsize;

	return 0;
}

#define SD_CHECK_FILE "/mnt/sdcard/rktest.file"
#define SD_CHECK_LEN 128 * 1024
int fs_manage_sd_check()
{
	int fd;
	int tm;
	int ret;
	struct timeval tv1, tv2;
	char check[SD_CHECK_LEN];

	gettimeofday(&tv1, NULL);

	for (ret = 0; ret < SD_CHECK_LEN; ret++)
		*(check + ret) = ret % 256;

	fd = fs_file_open_normal(SD_CHECK_FILE, O_RDWR | O_CREAT, 0777);
	if (fd < 0) {
		fs_log("func = %s, open fail, errno = %d\n", __func__, errno);
		return -1;
	}

	ret = fs_file_write(fd, check, SD_CHECK_LEN);
	if (ret != SD_CHECK_LEN) {
		fs_file_close_normal(fd);
		fs_log("func = %s, write fail, errno = %d\n", __func__, errno);
		return -1;
	}
	fs_file_lseek(fd, SEEK_SET, 0);
	ret = fs_file_read(fd, check, SD_CHECK_LEN);
	if (ret != SD_CHECK_LEN) {
		fs_file_close_normal(fd);
		fs_log("func = %s, read fail, errno = %d\n", __func__, errno);
		return -1;
	}

	for (ret = 0; ret < SD_CHECK_LEN; ret++) {
		if (*(check + ret) != ret % 256)
			break;
	}

	if (ret < SD_CHECK_LEN) {
		fs_file_close_normal(fd);
		fs_log("func = %s, data check fail, errno = %d\n", __func__, errno);
		return -1;
	}
	fs_file_close_normal(fd);
	gettimeofday(&tv2, NULL);
	tm = (tv2.tv_usec - tv1.tv_usec) / 1000 + (tv2.tv_sec - tv1.tv_sec) * 1000;
	fs_log("%s complete, use %dmS\n", __func__, tm);

	return 0;
}

