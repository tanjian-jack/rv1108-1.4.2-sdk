/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/prctl.h>
#include <errno.h>
#include <assert.h>
#include <dirent.h>
#include <malloc.h>
#include <sched.h>
#include <sys/sysinfo.h>

#include "fs_cache.h"
#include "fs_file.h"
#include "fs_list.h"
#include "fs_storage.h"
#include "fs_userdata.h"
#include "fs_sdcard.h"
#include "fs_log.h"
#include "fs_msg.h"
#include "fs_conf.h"

#define FD_MAX 10000

FS_MSG *m_fs_msg = NULL;
static pthread_mutex_t fs_msg_lock;

int m_filesize = 0;
int m_cache_size = 0;
/* cache begin write to file */
int m_cache_write_size = 0;
/* when data length greater than (cache_size - cache_limit) will block */
int m_cache_limit = 0;
int m_cache_count_max = 0;
int g_cur_speed_mode = FAST;
int g_cur_bitrate, g_ori_bitrate;

#define FILESIZE_DEFAULT (8 * FS_SZ_1M)
#define CACHE_GPS_SIZE_DEFAULT (128 * FS_SZ_1K)
#define CACHE_WRITE_SIZE_DEFAULT (1024 * FS_SZ_1K)
#define CACHE_BUFFER_SIZE_DEFAULT (1200 * FS_SZ_1K)

/* Video Box size */
#if ENABLE_SDV
#define CACHE_BOX_SIZE_DEFAULT (4 * FS_SZ_1M)
#else
#define CACHE_BOX_SIZE_DEFAULT (1 * FS_SZ_1M)
#endif

/* The size for 128M mem, if mem greater, double in fs_cache_kernelparam_set */
#ifdef CACHE_ENCODEDATA_IN_MEM
#define CACHE_COUNT_MAX_DEFAULT (20 * FS_SZ_1M)
#elif ENABLE_SDV
#define CACHE_COUNT_MAX_DEFAULT (50 * FS_SZ_1M)
#else
#define CACHE_COUNT_MAX_DEFAULT (10 * FS_SZ_1M)
#endif

#define CACHE_PREPARE_TIME (2)
#define MAX_FILE_SIZE (1800 * FS_SZ_1M)
/* limit = buffer_size - write_size, the echo write data len limit */
#define CACHE_LIMIT_DEFAULT (138 * FS_SZ_1K)

int dirty_background_bytes = 2 * FS_SZ_1M;
int dirty_bytes = 4 * FS_SZ_1M;
int dirty_writeback_centisecs  = 50000; /* wake up thread 1s */
int dirty_expire_centisecs = 1000000; /* centiseconds  5s */
int mmc_max_block = FS_SZ_1K; /* write mmc once */

#define DIRTYNODEPATH "/proc/sys/vm/"
#define DIRTY_BACKGROUND_BYTES DIRTYNODEPATH "dirty_background_bytes"
#define DIRTY_BYTES DIRTYNODEPATH "dirty_bytes"
#define DIRTY_WRITEBACK_CENTISECS DIRTYNODEPATH "dirty_writeback_centisecs"
#define DIRTY_EXPIRE_CENTISECS DIRTYNODEPATH "dirty_expire_centisecs"
#define MMC_MAX_BLOCK "/sys/block/mmcblk1/queue/max_sectors_kb"

#define DOWN_SPEED_RELA (1.5)
#define UP_SPEED_RELA (2)
#define SPEED_LIST_NUM (CACHE_COUNT_MAX_DEFAULT / CACHE_WRITE_SIZE_DEFAULT  / 2)

pthread_t cache_manage_tid = 0;
int m_cache_ipcmsgid = 0;
volatile int cache_all_count = 0;
int m_cache_msgid = -1;

#define BUFFER_TAG "fs_manage"
#define RK_IPCMSGKEY 1029
#define SPEED_MONI

typedef struct _IPC_MSG {
	long msg_type;
	char fs_msg[sizeof(FS_MSG)];
} IPC_MSG;

extern int fs_manage_runapp(char *cmd);

void fs_cache_time_printf(int line)
{
	int64_t time;
	struct timeval time_current;
	gettimeofday(&time_current, NULL);
	fs_log("line = %d, msec = %d.%dmS\n", line , time_current.tv_sec,
	       time_current.tv_usec / 1000);
}

void fs_cache_time_printf_f(const char *func)
{
	int64_t time;
	struct timeval time_current;
	gettimeofday(&time_current, NULL);
	time = time_current.tv_sec * 1000 + time_current.tv_usec / 1000;
	fs_log("fun=%s, t=%ld mS\n", func , time);
}

void fs_cache_timeout_init(struct timeval *timeout)
{
	gettimeofday(timeout, NULL);
	return;
}

void fs_cache_timeout_reinit(struct timeval *timeout)
{
	gettimeofday(timeout, NULL);
	return;
}

int fs_cache_timeout_getvalue(struct timeval *timeout)
{
	int64_t time;
	struct timeval time_current;
	gettimeofday(&time_current, NULL);
	time = time_current.tv_sec - timeout->tv_sec;
	return time;
}

int fs_cache_ipcmsg_getnum(int msgid)
{
	int ret;
	struct msqid_ds msg_info;

	if (msgid < 0) {
		fs_err_f(" msgid = %d, exit\n", msgid);
		return -1;
	}
	ret = msgctl(msgid, IPC_STAT, &msg_info);
	if (ret < 0) {
		fs_err_f("failed to get info | errno=%d [%s]/n", errno, strerror(errno));
		return -1;
	}
	return msg_info.msg_qnum;
}

int fs_cache_ipcmsg_setnum(int msgid, int number)
{
	int ret;
	struct msqid_ds msg_info;

	if (msgid < 0)
		return -1;

	ret = msgctl(msgid, IPC_STAT, &msg_info);
	if (ret < 0) {
		fs_err_f("failed to get info | errno=%d [%s]/n", errno, strerror(errno));
		return -1;
	}
	msg_info.msg_qnum = msg_info.msg_qnum * number;
	msg_info.msg_qbytes = msg_info.msg_qbytes * number;
	ret = msgctl(msgid, IPC_SET, &msg_info);
	if (ret < 0) {
		fs_err_f("failed to get info | errno=%d [%s]/n", errno, strerror(errno));
		return -1;
	}
	ret = msgctl(msgid, IPC_STAT, &msg_info);
	if (ret < 0) {
		fs_err_f("failed to get info | errno=%d [%s]/n", errno, strerror(errno));
		return -1;
	}

	return msg_info.msg_qnum;

}

FS_RET fs_cache_ipcmsg_init(int msg_key, int *m_msgid)
{
	int msgid;
	int ret;
	int retry_num = 0;

RETRY:
	msgid = msgget(msg_key, IPC_EXCL);  /*check msg*/
	if (msgid < 0) {
		msgid = msgget(msg_key, IPC_CREAT | 0666); /*create msg*/
		if (msgid < 0) {
			fs_err("failed to create msq | errno=%d [%s]/n", errno, strerror(errno));
			return FS_ERR_UNKNOW;
		}
	}

	ret = fs_cache_ipcmsg_getnum(msgid);
	if (ret > 0) {
		ret = msgctl(msgid, IPC_RMID, NULL);
		if (ret < 0)
			assert(0);
		retry_num++;
		if (retry_num > 3) /*retry, but the msg can't delete*/
			assert(0);
		goto RETRY;
	}
	*m_msgid = msgid;
	return FS_OK;
}

void fs_cache_ipcmsg_deinit(int msg_key, int *m_msgid)
{
	int msgid;

	/* if ipcmsg init fail, the m_msgid will set to -1, so don't deinit again */
	if (*m_msgid < 0)
		return;

	msgid = msgget(msg_key, IPC_EXCL);  /*check ipc msg*/
	if (msgid < 0) {
		fs_log("failed to get id | errno=%d [%s]/n", errno, strerror(errno));
		return;
	}

	msgctl(msgid, IPC_RMID, 0); /*remove ipc msg*/

	*m_msgid = -1;
	return;
}

FS_RET fs_cache_ipcmsg_rec(int msgid, FS_MSG *fs_msg)
{
	IPC_MSG ipc_msg_recv;

	if (fs_msg == NULL)
		return FS_NOK;
	if (msgid < 0)
		return FS_NOK;

	int ret = msgrcv(msgid, &ipc_msg_recv, sizeof(FS_MSG), 0, 0);
	if (ret < 0) {
		fs_err_f("msgsnd() read msg failed,errno=%d[%s]\n",
		         errno, strerror(errno));
		return FS_NOK;
	}
	memcpy((char *)fs_msg, ipc_msg_recv.fs_msg, sizeof(FS_MSG));

	return FS_OK;
}

FS_RET fs_cache_ipcmsg_send(int msgid, FS_MSG *fs_msg)
{
	IPC_MSG ipc_msg_send;

	if (fs_msg == NULL)
		return FS_NOK;
	if (msgid < 0)
		return FS_NOK;

	ipc_msg_send.msg_type = 1;
	memcpy(ipc_msg_send.fs_msg, (char *)fs_msg, sizeof(FS_MSG));
	int ret = msgsnd(msgid, (void *)&ipc_msg_send, sizeof(FS_MSG), IPC_NOWAIT);
	if (ret < 0) {
		fs_err_f("msgsnd() write msg failed,ret = %d errno=%d[%s]\n", ret, errno,
		         strerror(errno));
		fs_err_f("msgsnd() write msg failed,msg number = %d\n",
		         fs_cache_ipcmsg_getnum(msgid));
		return FS_NOK;
	}

	return FS_OK;
}

int fs_cache_create_fake_fd(FS_MSG *fs_msg_list)
{
	int fake_fd;
	FS_MSG *fs_msg_current;

	if (fs_msg_list == NULL)
		return -1;

rand:
	fake_fd = 1 + rand() % 10000;
	for (fs_msg_current = fs_msg_list; fs_msg_current->next != NULL;
		 fs_msg_current = fs_msg_current->next) {
		if (fs_msg_current->fake_fd == fake_fd) {
			fake_fd = 0;
			break;
		}
	}
	/* the last one have no check */
	if (fs_msg_current->next == NULL) {
		if (fs_msg_current->fake_fd == fake_fd)
			fake_fd = 0;
	}

	if (fake_fd == 0)
		goto rand;
	return fake_fd;
}

FS_CACHE_CTL *fs_cache_create_ctl()
{
	FS_CACHE_CTL *cache_ctl;

	cache_ctl = calloc(1, sizeof(FS_CACHE_CTL));
	if (cache_ctl == NULL)
		return NULL;

	cache_ctl->cache_size =
	    m_cache_size > 0 ? m_cache_size : CACHE_BUFFER_SIZE_DEFAULT;
	cache_ctl->cache_write =
	    m_cache_write_size > 0 ? m_cache_write_size : CACHE_WRITE_SIZE_DEFAULT;
	cache_ctl->cache_limit =
	    m_cache_limit > 0 ? m_cache_limit : CACHE_LIMIT_DEFAULT;
	cache_ctl->cache_count_max =
	    m_cache_count_max > 0 ? m_cache_count_max : CACHE_COUNT_MAX_DEFAULT;
	cache_ctl->filesize = 0;
	cache_ctl->filespace = 0;
	return cache_ctl;
}

FS_RET fs_cache_msg_printf(FS_MSG *fs_msg_list)
{
	FS_MSG *fs_msg_current;

	if (fs_msg_list == NULL)
		return FS_NOK;

	pthread_mutex_lock(&fs_msg_lock);
	for (fs_msg_current = fs_msg_list; fs_msg_current->next != NULL;
		 fs_msg_current = fs_msg_current->next) {
		fs_log("fd = %d, filename = %s\n",
		       fs_msg_current->fake_fd, fs_msg_current->filename);
		if (fs_msg_current->cache_ctl != NULL) {
			fs_log("filesize=%d,cache_size=%d,cache_write=%d,"
			       "cache_limit=%d cache_count=%d\n",
			       fs_msg_current->cache_ctl->filesize,
			       fs_msg_current->cache_ctl->cache_size,
			       fs_msg_current->cache_ctl->cache_write,
			       fs_msg_current->cache_ctl->cache_limit,
			       fs_msg_current->cache_ctl->cache_count);
		}
	}
	if (fs_msg_current) {
		fs_log("fd = %d, filename = %s\n",
		        fs_msg_current->fake_fd, fs_msg_current->filename);
		if (fs_msg_current->cache_ctl != NULL) {
			fs_log("filesize=%d,cache_size=%d,cache_write=%d,"
			       "cache_limit=%d cache_count=%d\n",
			       fs_msg_current->cache_ctl->filesize,
			       fs_msg_current->cache_ctl->cache_size,
			       fs_msg_current->cache_ctl->cache_write,
			       fs_msg_current->cache_ctl->cache_limit,
			       fs_msg_current->cache_ctl->cache_count);
		}
	}
	pthread_mutex_unlock(&fs_msg_lock);
	return 1;
}

void fs_cache_msg_setfd(FS_MSG *fs_msg_list, int fd)
{
	FS_MSG *fs_msg_current;

	if (fs_msg_list == NULL)
		return;

	pthread_mutex_lock(&fs_msg_lock);
	for (fs_msg_current = fs_msg_list; fs_msg_current->next != NULL;
		 fs_msg_current = fs_msg_current->next) {
		if (fs_msg_current->command != FS_CMD_NOMSG)
			fs_msg_current->real_fd = fd;
	}
	if (fs_msg_current && fs_msg_current->command != FS_CMD_NOMSG) {
		fs_msg_current->real_fd = fd;
	}
	pthread_mutex_unlock(&fs_msg_lock);
}

FS_MSG *fs_cache_msg_get_byfd(FS_MSG *fs_msg_list, int fd)
{
	FS_MSG *fs_msg_current = NULL;

	if (fs_msg_list == NULL)
		goto ret;
	if (fd <= 0)
		goto ret;

	for (fs_msg_current = fs_msg_list; fs_msg_current->next != NULL;
		 fs_msg_current = fs_msg_current->next) {
		if (fs_msg_current->fake_fd == fd)
			break;
	}
	if (fs_msg_current->next == NULL) {
		/* the last one have no check */
		if (fs_msg_current->fake_fd != fd)
			fs_msg_current = NULL;
	}

ret:
	return fs_msg_current;
}

FS_MSG *fs_cache_msg_get_byfilename(FS_MSG *fs_msg_list, char *filename)
{
	FS_MSG *fs_msg_current = NULL;

	if (fs_msg_list == NULL)
		goto ret;
	if (filename == NULL)
		goto ret;

	for (fs_msg_current = fs_msg_list; fs_msg_current->next != NULL;
		 fs_msg_current = fs_msg_current->next) {
		if (strcmp(filename, fs_msg_current->filename) == 0)
			break;
	}
	/* the last one have no check */
	if (fs_msg_current->next == NULL) {
		if (strcmp(filename, fs_msg_current->filename) != 0)
			fs_msg_current = NULL;
	}

ret:
	return fs_msg_current;
}

FS_RET fs_cache_msg_insert(FS_MSG **fs_msg_list, FS_MSG *fs_msg_node)
{
	FS_MSG *fs_msg_current;

	if (fs_msg_node == NULL)
		return FS_NOK;

	if (*fs_msg_list == NULL) {
		*fs_msg_list = fs_msg_node;
		goto out;
	}

	for (fs_msg_current = *fs_msg_list; fs_msg_current->next != NULL;
	     fs_msg_current = fs_msg_current->next);
	cg_list_add((CgList *)(fs_msg_current), (CgList *)fs_msg_node);

out:
	return FS_OK;
}

void fs_cache_msg_free(FS_MSG *fs_msg)
{
	if (fs_msg == NULL)
		return;

	if (fs_msg->cache_ctl != NULL) {
		free(fs_msg->cache_ctl);
		fs_msg->cache_ctl = NULL;
	}

	if (fs_msg->filename != NULL)
		free(fs_msg->filename);
	fs_msg->next = fs_msg->pre = NULL;

	free(fs_msg);
	return;
}

FS_RET fs_cache_msg_delete(FS_MSG **fs_msg_list, int fd)
{
	FS_MSG *fs_msg;

	fs_msg = fs_cache_msg_get_byfd(*fs_msg_list , fd);
	if (fs_msg == NULL)
		return FS_NOK;

	if (fs_msg->next == NULL && fs_msg->pre == NULL) {
		fs_cache_msg_free(fs_msg);
		*fs_msg_list = NULL;
	} else if (fs_msg->next == NULL) {
		fs_msg->pre->next = NULL;
		fs_cache_msg_free(fs_msg);
	} else if (fs_msg->pre == NULL) {
		*fs_msg_list = fs_msg->next;
		fs_msg->next->pre = NULL;
		fs_cache_msg_free(fs_msg);
	} else {
		cg_list_remove((CgList *)fs_msg);
		fs_cache_msg_free(fs_msg);
	}

	return FS_OK;
}

FS_MSG *fs_cache_msg_create(int create_flag)
{
	FS_MSG *fs_msg;

	fs_msg = (FS_MSG *)malloc(sizeof(FS_MSG));
	if (fs_msg == NULL) {
		fs_err_f("cache_create_msg fail\n");
		return NULL;
	}

	if (create_flag)
		fs_msg->command = FS_CMD_UNKNOW;
	else
		fs_msg->command = FS_CMD_NOMSG;
	fs_msg->fake_fd = -1;
	fs_msg->real_fd = -1;
	fs_msg->write_arg.buf = NULL;
	fs_msg->write_arg.count = 0;
	fs_msg->next = NULL;
	fs_msg->pre = NULL;
	fs_msg->filename = NULL;
	fs_msg->cache_ctl = NULL;
	fs_msg->root_msg = fs_msg;
	return fs_msg;
}

ssize_t fs_cache_picture_write(char *filename, void *buf, size_t count)
{
	int fd = -1;
	FS_MSG *fs_msg;

	fs_msg = fs_cache_msg_create(O_CREAT);
	if (fs_msg == NULL)
		goto RET1;

	pthread_mutex_lock(&fs_msg_lock);
	fs_cache_msg_insert(&m_fs_msg, fs_msg);
	fd = fs_cache_create_fake_fd(m_fs_msg);
	pthread_mutex_unlock(&fs_msg_lock);

	fs_msg->fake_fd = fd;
	fs_msg->filename = strdup(filename);
	fs_msg->write_arg.buf = buf;
	fs_msg->write_arg.count = count;
	fs_msg->command = FS_CMD_PIC;

	int ret = fs_cache_ipcmsg_send(m_cache_msgid, fs_msg);
	if (ret != FS_OK) {
		fs_err_f("fs_cache_ipcmsg_send fail, errno = %d\n", errno);
		goto RET;
	}
	return count;
RET:
	pthread_mutex_lock(&fs_msg_lock);
	fs_cache_msg_delete(&m_fs_msg, fs_msg->fake_fd);
	pthread_mutex_unlock(&fs_msg_lock);
RET1:
	if (buf != NULL)
		free(buf);
	fs_err_f("--------error---------\n");
	return -1;
}

void fs_cache_picture_flush(FS_MSG *fs_msg)
{
	int delay1, delay2, delay3;
	struct timeval t1, t2, t3, t4;

	gettimeofday(&t1, NULL);
	int fd = fs_file_open(fs_msg->filename, O_CREAT | O_RDWR | O_DIRECT, S_IRWXU);
	if (fd < 0) {
		fs_err_f("fs_file_open fail, errno = %d\n", errno);
		goto RET;
	}

	gettimeofday(&t2, NULL);
	int ret = fs_file_write(fd, fs_msg->write_arg.buf, fs_msg->write_arg.count);
	if (ret < 0)
		fs_err_f("fs_file_write fail, errno = %d\n", errno);

	gettimeofday(&t3, NULL);
	fs_file_close(fd);
	gettimeofday(&t4, NULL);

	delay1 = (t2.tv_sec - t1.tv_sec) * 1000000 + (t2.tv_usec - t1.tv_usec);
	delay2 = (t3.tv_sec - t2.tv_sec) * 1000000 + (t3.tv_usec - t2.tv_usec);
	delay3 = (t4.tv_sec - t3.tv_sec) * 1000000 + (t4.tv_usec - t3.tv_usec);
	fs_log("picture  Write all = %d uS\n", delay1 + delay2 + delay3);
RET:
	free(fs_msg->write_arg.buf);
	pthread_mutex_lock(&fs_msg_lock);
	fs_cache_msg_delete(&m_fs_msg, fs_msg->fake_fd);
	pthread_mutex_unlock(&fs_msg_lock);
	return;
}

FS_RET fs_cache_buffer_get(char **buffer, int *size, volatile int *buffer_count,
                           int flag, int def_size)
{
	int ret = -1;

	if (flag == 1)
		*size = CACHE_BOX_SIZE_DEFAULT;
	else
		*size = def_size;

	ret = posix_memalign((void **)buffer, FS_ALIGN_VALUE, *size);
	if (ret < 0) {
		fs_err_f("malloc fail, errno = %d\n", errno);
		return FS_NOK;
	}
	*buffer_count += *size;
	cache_all_count += *size;
	return FS_OK;
}

void fs_cache_buffer_put(char **buffer, int size, volatile int *buffer_count)
{
	if (*buffer) {
		free(*buffer);
		*buffer = NULL;
		*buffer_count -= size;
		cache_all_count -= size;
	} else {
		fs_err_f("the cache buffer is NULL\n");
	}
	return;
}

FS_RET fs_cache_file_send_exit()
{
	FS_MSG  fs_msg;

	fs_msg.command = FS_CMD_EXIT;
	FS_RET ret = fs_cache_ipcmsg_send(m_cache_msgid, &fs_msg);
	if (ret != FS_OK)
		goto RET1;
	return FS_OK;
RET1:
	fs_err_f("--------error---------\n");
	return FS_NOK;
}

int fs_cache_file_fstat(int fd, struct stat *filestat)
{
	int ret;
	FS_MSG *fs_msg;

	return -1;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto error;
	if (fs_msg->real_fd < 0)
		return -1;

	return fstat(fs_msg->real_fd, filestat);
error:
	fs_err_f("--------error---------\n");
	return -1;
}

int fs_cache_file_blocknotify(int prev_num, int later_num, char *filename)
{
/* If enable cache encode data, return 0 right now */
#ifdef CACHE_ENCODEDATA_IN_MEM
	return 0;
#endif

	int ret;
	FS_MSG       *fs_msg;
	FS_BLOCK_ARG *fs_block_arg;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfilename(m_fs_msg, filename);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto error;
	 /* The operation only for cache to write*/
	if (fs_msg->command == FS_CMD_NOMSG) {
		fs_assert(0);
	}

	fs_block_arg = (FS_BLOCK_ARG *)(&fs_msg->write_arg);
	fs_block_arg->prev_num = prev_num;
	fs_block_arg->later_num = later_num;
	fs_msg->command = FS_CMD_BLOCK;
	ret = fs_cache_ipcmsg_send(m_cache_msgid, fs_msg);
	if (ret != FS_OK)
		goto error;

	return 0;
error:
	fs_err_f("--------error---------\n");
	return -1;
}

FS_RET fs_cache_file_write_send(FS_MSG *fs_msg, int command)
{
	FS_RET ret;
	FS_CACHE_CTL *cache_ctl;
	cache_ctl = fs_msg->cache_ctl;

	if (cache_ctl->buffer_offset_end == 0)
		return FS_OK;
	if (cache_ctl->buffer == NULL)
		return FS_ERR_NULL_PTR;
	/* if sd don't exsit  */
	if (fs_manage_sd_exist(NULL) != 0 || command == FS_CMD_REPAIR) {
		fs_msg->write_arg.buf = cache_ctl->buffer;
		fs_msg->command = command;
		fs_msg->write_arg.count = cache_ctl->buffer_offset_end;
		if (command == FS_CMD_TAIL || command == FS_CMD_CLOSE) {
			fs_msg->write_arg.count = FS_ALIGN(cache_ctl->buffer_offset_end, FS_ALIGN_VALUE);
			/* Set tail flag for box write */
			if (command == FS_CMD_TAIL) {
				cache_ctl->trail_flag = 1;
				cache_ctl->tail_offset = fs_msg->write_arg.count - cache_ctl->buffer_offset_end;
				fs_log("tail offset = %d\n", cache_ctl->tail_offset);
			}
			/* Just get the mediasize offset. */
			if (fs_msg->write_arg.count != cache_ctl->buffer_offset_end && command == FS_CMD_TAIL)
				cache_ctl->media_offset = fs_msg->write_arg.count - cache_ctl->buffer_offset_end;
			fs_msg->command = FS_CMD_WRITE;
		}
		ret = fs_cache_ipcmsg_send(m_cache_msgid, fs_msg);
		if (ret != FS_OK) {
			fs_err_f("fs_cache_ipcmsg_send fail, errno = %d\n", errno);
			fs_cache_buffer_put(&cache_ctl->buffer, fs_msg->buffer_size,
			                    &cache_ctl->cache_count);
		}
	} else {
		fs_cache_buffer_put(&cache_ctl->buffer, fs_msg->buffer_size,
		                    &cache_ctl->cache_count);
	}

	cache_ctl->file_offset_start += cache_ctl->buffer_offset_end;
	cache_ctl->buffer = NULL;
	cache_ctl->buffer_offset = 0;
	cache_ctl->buffer_offset_end = 0;
	return ret;
}

int fs_cache_file_writetrail(int fd)
{
	FS_MSG *fs_msg = NULL;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET1;
	if (fs_msg->command == FS_CMD_NOMSG) //the operation only for cache
		fs_assert(0);

	FS_RET ret = fs_cache_file_write_send(fs_msg, FS_CMD_TAIL);
	if (ret != FS_OK)
		goto RET1;
	return 0;
RET1:
	fs_err_f("--------error---------\n");
	return -1;
}

int fs_cache_file_get_tailoff(int fd)
{
	FS_MSG *fs_msg = NULL;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET1;
	return fs_msg->cache_ctl->tail_offset;
RET1:
	fs_err_f("--------error---------\n");
	return -1;
}

int64_t fs_cache_file_seek(int fd, int64_t offset, int whence)
{
	FS_RET ret;
	int64_t file_seek_position = 0;
	FS_MSG *fs_msg = NULL;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET2;
	if (fs_msg->command == FS_CMD_NOMSG)
		return fs_file_lseek(fs_msg->real_fd, offset, whence);

	/* Insert buffer */
	FS_CACHE_CTL *cache_ctl = fs_msg->cache_ctl;
	if (cache_ctl->buffer == NULL) {
		ret = fs_cache_buffer_get(&cache_ctl->buffer, &fs_msg->buffer_size,
		                          &cache_ctl->cache_count, cache_ctl->trail_flag,
		                          cache_ctl->cache_write);
		if (ret != FS_OK)
			goto RET1;
	}

	/* Get the offset to the buffer */
	if (whence == SEEK_SET)
		file_seek_position = offset;
	else if (whence == SEEK_CUR)
		file_seek_position = cache_ctl->file_offset_end + offset;
	else if (whence == SEEK_END)
		file_seek_position = cache_ctl->file_offset_last + offset;
	else
		fs_assert(0);
	 /* This is the media size */
	if (file_seek_position == cache_ctl->mediasize_offset
		&& file_seek_position != 0) {
		cache_ctl->mediasize_flag = 1;
		goto RET1;
	} else if (file_seek_position > cache_ctl->file_offset_last) {
		fs_err("Opera:Seek Fail. Seek position = %d, the last offset = %d\n",
		       file_seek_position, cache_ctl->file_offset_last);
		goto RET2;
	} else if (file_seek_position < cache_ctl->file_offset_start) {
		fs_err("Opera:Seek Fail. Seek position = %d, the begin offset = %d\n",
		       file_seek_position, cache_ctl->file_offset_start);
		goto RET2;
	} else {
		cache_ctl->file_offset_end = file_seek_position;
		cache_ctl->buffer_offset =
		    cache_ctl->file_offset_end - cache_ctl->file_offset_start;
	}
RET1:
	return file_seek_position;
RET2:
	fs_err_f("--------error---------\n");
	return 0;
}

ssize_t fs_cache_file_read(int fd, const void *buf, size_t count)
{
	FS_MSG *fs_msg = NULL;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg= fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET1;
	if (fs_msg->command != FS_CMD_NOMSG)
		fs_assert(0);

	return read(fs_msg->real_fd, (void *)buf, count);
RET1:
	fs_err_f("--------error---------\n");
	return -1;
}

ssize_t fs_cache_file_write(int fd, const void *buf, size_t count)
{
	FS_RET ret;
	int delay_num = 0;
	FS_MSG *fs_msg;
	char *buffer;
	int remain_size;
	FS_CACHE_CTL *cache_ctl;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET1;
	if (fs_msg->command == FS_CMD_NOMSG) //the operation only for cache
		fs_assert(0);

	if (count > fs_msg->cache_ctl->cache_limit) {
		fs_err("The file data size is too large. Limit = %d Byle\n",
		       fs_msg->cache_ctl->cache_limit);
		goto RET1;
	}

	cache_ctl = fs_msg->cache_ctl;
	while (cache_all_count > cache_ctl->cache_count_max) {
		if (delay_num == 0) {
			fs_log("%s write is blocking, Count=%d Byte, all_count=%d Byte\n",
			       fs_msg->filename, cache_ctl->cache_count, cache_all_count);
		}
		usleep(30 * 1000);
		delay_num++;
	}
	if (delay_num > 0) {
		fs_log("File write is block, delay time = %d ms\n\n\n", delay_num * 30);
#ifdef SPEED_MONI
		fs_msg_notify_file(SPEED_ADJ, NULL, 1);
#endif
	}

	/* get the head and don't do operation */
	if (cache_ctl->mediasize_flag == 1) {
		char *mediasize = (char *)(buf);
		cache_ctl->mediasize_flag = 0;
		memcpy(cache_ctl->mediasize, buf, 4);
		//printf("Opera:Write.Mp4 size %2x%2x%2x%2x\n",
		//cache_ctl->mediasize[0], cache_ctl->mediasize[1],
		//cache_ctl->mediasize[2], cache_ctl->mediasize[3]);
		return 4;
	}

	if (cache_ctl->trail_flag != 1 && cache_ctl->file_offset_last > MAX_FILE_SIZE)
		fs_msg_notify_file(FILE_OVERLONG, fs_msg->filename, 0);

	if (cache_ctl->buffer == NULL) {
		ret = fs_cache_buffer_get(&cache_ctl->buffer, &fs_msg->buffer_size,
		                          &cache_ctl->cache_count, cache_ctl->trail_flag,
		                          cache_ctl->cache_write);
		if (ret != FS_OK)
			goto RET1;
	}

	remain_size = cache_ctl->buffer_offset + count - fs_msg->buffer_size;
	if (remain_size < 0) {
		memcpy(cache_ctl->buffer + cache_ctl->buffer_offset, (void *)buf, count);
		cache_ctl->buffer_offset += count;
	} else {
		memcpy(cache_ctl->buffer + cache_ctl->buffer_offset,
		       (void *)buf, count - remain_size);
		cache_ctl->buffer_offset = fs_msg->buffer_size;
	}
	if (cache_ctl->buffer_offset > cache_ctl->buffer_offset_end)
		cache_ctl->buffer_offset_end = cache_ctl->buffer_offset;
	/* send write package */
	if (remain_size >= 0) {
		ret = fs_cache_file_write_send(fs_msg, FS_CMD_WRITE);
		if (ret != FS_OK)
			goto RET1;
	}
	if (remain_size > 0) {
		ret = fs_cache_buffer_get(&cache_ctl->buffer, &fs_msg->buffer_size,
		                          &cache_ctl->cache_count, cache_ctl->trail_flag,
		                          cache_ctl->cache_write);
		if (ret != FS_OK)
			goto RET1;
		memcpy(cache_ctl->buffer + cache_ctl->buffer_offset,
		      (void *)buf + count - remain_size, remain_size);
		cache_ctl->buffer_offset += remain_size;
		cache_ctl->buffer_offset_end = cache_ctl->buffer_offset;
	}
	cache_ctl->file_offset_end += count;
	if (cache_ctl->file_offset_end > cache_ctl->file_offset_last)
		cache_ctl->file_offset_last = cache_ctl->file_offset_end;
	return count;
RET1:
	fs_err_f("--------error---------\n");
	return -1;
}

int fs_cache_file_open(const char *filename, int flags, int mode)
{
	int fd;
	int ret;
	FS_MSG *fs_msg;

	if (filename == NULL)
		goto RET2;

	fs_msg = fs_cache_msg_create(flags & O_CREAT);
	if (fs_msg == NULL)
		goto RET2;

	pthread_mutex_lock(&fs_msg_lock);
	fs_cache_msg_insert(&m_fs_msg, fs_msg);
	pthread_mutex_unlock(&fs_msg_lock);

	if (fs_msg->command == FS_CMD_NOMSG) {
		pthread_mutex_lock(&fs_msg_lock);
		fs_msg->fake_fd = fs_cache_create_fake_fd(m_fs_msg);
		pthread_mutex_unlock(&fs_msg_lock);
		fs_msg->real_fd = open(filename, flags, mode);
		fs_msg->filename = strdup(filename);
		return fs_msg->fake_fd;
	}

	pthread_mutex_lock(&fs_msg_lock);
	fd = fs_cache_create_fake_fd(m_fs_msg);
	pthread_mutex_unlock(&fs_msg_lock);

	fs_msg->filename = strdup(filename);
	fs_msg->fake_fd = fd;
	((FS_OPEN_ARG *)(&fs_msg->write_arg))->flag = flags;
	((FS_OPEN_ARG *)(&fs_msg->write_arg))->mode = mode;
	fs_msg->command = FS_CMD_OPEN;

	/*create cache_ctl;*/
	fs_msg->cache_ctl = fs_cache_create_ctl();
	if (fs_msg->cache_ctl == NULL)
		goto RET1;
	fs_msg->cache_ctl->mediasize_offset = fs_file_media_offset(filename);
	if(strcmp(strrchr(filename, '.') + 1, "txt") == 0)
		fs_msg->cache_ctl->cache_size = CACHE_GPS_SIZE_DEFAULT;
	ret = fs_cache_ipcmsg_send(m_cache_msgid, fs_msg);
	if (ret < 0)
		goto RET1;

	return fd;
RET1:
	pthread_mutex_lock(&fs_msg_lock);
	fs_cache_msg_delete(&m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
RET2:
	fs_err_f("--------error---------\n");
	return -1;
}

int fs_cache_file_close(int fd)
{
	int ret;
	FS_MSG *fs_msg = NULL;

	pthread_mutex_lock(&fs_msg_lock);
	fs_msg = fs_cache_msg_get_byfd(m_fs_msg, fd);
	pthread_mutex_unlock(&fs_msg_lock);
	if (fs_msg == NULL)
		goto RET2;

	if (fs_msg->command == FS_CMD_NOMSG) {
		ret = fs_file_close_normal(fs_msg->real_fd);
		pthread_mutex_lock(&fs_msg_lock);
		fs_cache_msg_delete(&m_fs_msg, fs_msg->fake_fd);
		pthread_mutex_unlock(&fs_msg_lock);
		return ret;
	}
	/* sd exist */
	if (fs_manage_sd_exist(NULL) != 0)
		fs_cache_file_write_send(fs_msg, FS_CMD_CLOSE);
	else
		fs_cache_file_write_send(fs_msg, FS_CMD_REPAIR);
	fs_msg->command = FS_CMD_CLOSE;
	ret = fs_cache_ipcmsg_send(m_cache_msgid, fs_msg);
	if (ret != FS_OK)
		goto RET2;
	return 0;
RET2:
	fs_err_f("--------error---------\n");
	return -1;
}

void fs_cache_write_mediasize(char *filename, char *size_buffer,
                              int media_size)
{
	int fd;
	int64_t ret;
	int64_t offset = 0;
	char new_media_buffer[4] = {0};
	int new_media = 0;

	new_media = size_buffer[0] * 0x1000000 + size_buffer[1] * 0x10000 +
	            size_buffer[2] * 0x100 + size_buffer[3];
	new_media += media_size;
	new_media_buffer[0] = (new_media >> 24) & 0xff;
	new_media_buffer[1] = (new_media >> 16) & 0xff;
	new_media_buffer[2] = (new_media >> 8) & 0xff;
	new_media_buffer[3] = new_media & 0xff;

	offset = fs_file_media_offset(filename);

	fd = fs_file_open_normal(filename, O_RDWR, S_IRWXU);
	if (fd < 0) {
		fs_err_f("open %s fail, errno = %d\n", filename, errno);
		return;
	}
	ret = pwrite(fd, new_media_buffer, 4, offset);
	if (ret <= 0)
		fs_err_f("fail errno = %d\n", errno);
	//fsync(fd);
	fs_file_close_normal(fd);
	return;
}

#if VIDEO_REPAIR
void fs_cache_video_repair(FS_MSG *fs_msg, char *cache, int cache_size)
{
	int fd;
	int ret;
	char *video_file_name;

	ret = fs_userdata_init();
	if (ret == -1)
		return;

	video_file_name = strrchr(fs_msg->filename, '/') + 1;
	fs_userdata_write(video_file_name, cache, cache_size);
	fs_log("storage cache_video_repair name = %s\n", video_file_name);
	fs_userdata_deinit();

	return;
}
#endif

void fs_cache_set_bitrate(int cur_bitrate, int ori_bitrate)
{
	g_cur_bitrate = cur_bitrate / 1024 / 8;
	g_ori_bitrate = ori_bitrate / 1024 / 8;
	if (g_cur_bitrate > g_ori_bitrate)
		g_ori_bitrate = g_cur_bitrate;
	fs_log("curbite = %d, ori_bitrate = %d\n", g_cur_bitrate, g_ori_bitrate);
}

void fs_cache_get_bitrate(int *cur_bitrate, int *ori_bitrate)
{
	*cur_bitrate = g_cur_bitrate;
	*ori_bitrate = g_ori_bitrate;
}

#ifdef SPEED_MONI
void fs_cache_speed_moni(int time, int count)
{
	int ret = 0;
	int speed;
	int speed_mode = FREEZE;
	int cur_bitrate, ori_bitrate;
	static int start = 0;
	static int times = 0;
	static int cur_speed_mode = FAST;
	static int speed_lst[SPEED_LIST_NUM] = {0};
	static int min_mode = FREEZE;
	static int max_mode = FAST;
	static int lowst = 10000;

	fs_cache_get_bitrate(&cur_bitrate, &ori_bitrate);
	speed = count * 1.024 / time;
	if (speed < lowst)
		lowst = speed;

	start++;
	if (start >= SPEED_LIST_NUM)
		start = 0;
	speed_lst[start] = speed;

	if (times < SPEED_LIST_NUM) {
		times++;
		return;
	}
	int cur_speed = 0;
	cur_speed = speed;
	speed = 0;
	for (ret = 0; ret < SPEED_LIST_NUM; ret++)
		speed += speed_lst[ret];
	speed = speed / SPEED_LIST_NUM;

	ret = 0;
	if (cur_bitrate == 0 || ori_bitrate == 0)
		return;
	if (speed > UP_SPEED_RELA * cur_bitrate) {
		ret = -speed / cur_bitrate / UP_SPEED_RELA;
		//fs_log("ret = %d speed = %d, cur = %d\n", ret, speed, cur_bitrate);
	}
	else if (speed < DOWN_SPEED_RELA * cur_bitrate) {
		ret = cur_bitrate * DOWN_SPEED_RELA / speed;
	//	fs_log("ret = %d speed = %d, cur = %d\n", ret, speed, cur_bitrate);
	}
//	printf("fs_cache_speed_moni speed = %2.1f MB/s, all speed = %2.1fMB/s, count = %d, time = %d, \n",
//	(float)cur_speed / 1024, (float)speed / 1024, count, time);
	if (ret == 0)
		return;
	speed_mode = cur_speed_mode + ret;
	if (speed_mode < FAST)
		speed_mode = FAST;
	if (speed_mode > FREEZE)
		speed_mode = FREEZE;
	ret = speed_mode - cur_speed_mode;
	/* Only reduce bitrate, don't up bitrate now */
	if (ret > 0) {
		cur_speed_mode = speed_mode;
		fs_msg_notify_file(SPEED_ADJ, NULL, ret);
		fs_log("speed_moni ret = %d, cur_speed_mode = %d\n", ret, cur_speed_mode);
	}

	if (speed_mode < min_mode)
		min_mode = speed_mode;
	if (speed_mode > max_mode)
		max_mode = speed_mode;
}
#endif
void fs_cache_flush_cache(FS_MSG *fs_msg, FS_CACHE_CTL *cache_ctl)
{
	int ret;
	FS_WRITE_ARG *fs_write_arg;
	struct timeval t1, t2, t3, t4;
	int mdelay1;

	fs_write_arg = &fs_msg->write_arg;
	void *write_buf = (char *)fs_write_arg->buf;

	if (fs_manage_sd_exist(NULL) != 0 &&
		fs_msg->command == FS_CMD_WRITE) {
		/* Wait the fd are change */
		if (fs_msg->root_msg->real_fd < 0) {
			fs_err_f("The fd is invalid, fd = %d\n", fs_msg->root_msg->real_fd);
			goto RET;
		}
		gettimeofday(&t1, NULL);
		ret = fs_file_write(fs_msg->root_msg->real_fd, write_buf, fs_write_arg->count);
		gettimeofday(&t2, NULL);
		mdelay1 = (t2.tv_sec - t1.tv_sec) * 1000 + (t2.tv_usec - t1.tv_usec) / 1000;
		if (mdelay1 > 1000) {
			fs_cache_time_printf(__LINE__);
			fs_log("%s, cost-time=%dms,write-size=%d Byte,filesize=%d Byte\n",
			       fs_msg->filename, mdelay1,
			       fs_write_arg->count, cache_ctl->filesize);
		}
		/* If timeout 8s and errno is EIO/EINVAL, Sd card is bad */
		if (mdelay1 >= 8000 && (errno == EIO || errno == EINVAL))
			fs_cache_msg_setfd(m_fs_msg, -1);
		if (ret < 0) {
			fs_err_f("Write fail errno = %d ret = %d, count = %d\n",
			          errno, ret, fs_write_arg->count);
			goto RET;
		}
#ifdef SPEED_MONI
		fs_cache_speed_moni(mdelay1, fs_write_arg->count);
#endif
		cache_ctl->filesize += fs_write_arg->count;

		/* Send command for prepare file. */
		if (cache_ctl->filepre_flag == 0) {
			ret = fs_cache_timeout_getvalue(&cache_ctl->timeout);
			if (ret >= CACHE_PREPARE_TIME) {
				cache_ctl->filepre_flag = 1;
				fileopera_ipcmsg_send_cmd(0, fs_msg->filename, NULL, FS_CMD_OPEN);
			}
		}
	}
#if VIDEO_REPAIR
	else if (fs_msg->command == FS_CMD_REPAIR) {
		fs_cache_video_repair(fs_msg, write_buf, fs_write_arg->count);
	}
#endif
RET:
	fs_cache_buffer_put((char **)&fs_write_arg->buf, fs_msg->buffer_size,
	                    &cache_ctl->cache_count);
	return;
}

void *fs_cache_manage_pthread(void *arg)
{
	fs_log("cache_manage_pthread init \n");

	FS_RET ret;
	int msgid;
	FS_MSG fs_msg;
	FS_OPEN_ARG  *fs_open_arg;
	FS_SEEK_ARG  *fs_seek_arg;
	FS_WRITE_ARG *fs_write_arg;
	FS_BLOCK_ARG *fs_block_arg;
	FS_CACHE_CTL *cache_ctl;
	FS_MSG       *fs_root_msg;
	int collision_prev_number = 0;
	int collision_later_number = 0;
	int video_number = 0;
	char new_filename[FILENAME_LEN];
	static int clr_cache = 0;

	prctl(PR_SET_NAME, "fs_cache_manage", 0, 0, 0);

	while (1) {
		ret = fs_cache_ipcmsg_rec(m_cache_msgid, &fs_msg);
		if (ret < 0) {
			fs_err_f("fail cache_ipcmsg_rec | errno=%d\n", errno);
			continue;
		}

		cache_ctl = fs_msg.cache_ctl;
		if (fs_msg.command == FS_CMD_OPEN) {
			fs_root_msg = fs_msg.root_msg;
			fs_open_arg = (FS_OPEN_ARG *)(&fs_msg.write_arg);
			fs_root_msg->real_fd = fs_file_open(fs_root_msg->filename,
			                             fs_open_arg->flag, fs_open_arg->mode);
			if (fs_root_msg->real_fd < 0) {
				fs_err("fs_root_msg->real_fd get fail errno = %d\n", errno);
				continue;
			}
			fs_cache_timeout_init(&cache_ctl->timeout);
			if (!clr_cache) {
				fs_manage_runapp("echo 3 > /proc/sys/vm/drop_caches");
				clr_cache = 1;
			}
			if (collision_later_number > 0) {
				cache_ctl->block_flag = 1;
				collision_later_number--;
			}
		} else if (fs_msg.command == FS_CMD_CLOSE) {
			fs_log("filename = %s, fileszie = %d\n",
			       fs_msg.filename, cache_ctl->filesize);
			fileopera_ipcmsg_send_cmd(fs_msg.root_msg->real_fd,
			                          NULL, NULL, FS_CMD_CLOSE);
			if (fs_manage_sd_exist(NULL) != 0) {
				if (cache_ctl->media_offset)
					fileopera_ipcmsg_send_cmd(cache_ctl->media_offset, fs_msg.filename,
					         (void *)(cache_ctl->mediasize), FS_CMD_WRITE_SIZE);
				/* rename the current file */
				if (cache_ctl->block_flag == 1) {
					fileopera_ipcmsg_send_cmd(0, fs_msg.filename, NULL,
					                          FS_CMD_RENAME_CUR);
					cache_ctl->block_flag = 0;
				} else if (cache_ctl->block_flag == 0 &&
				           collision_later_number > 0) {
					collision_later_number--;
					fileopera_ipcmsg_send_cmd(0, fs_msg.filename, NULL,
					                          FS_CMD_RENAME_CUR);
				}
			}
			fs_storage_clr_recing_filename(fs_msg.filename);
			pthread_mutex_lock(&fs_msg_lock);
			fs_cache_msg_delete(&m_fs_msg, fs_msg.fake_fd);
			pthread_mutex_unlock(&fs_msg_lock);
		} else if (fs_msg.command == FS_CMD_WRITE) {
			fs_cache_flush_cache(&fs_msg, cache_ctl);
		} else if (fs_msg.command == FS_CMD_SEEK) {
			fs_err_f("Opera:Seek. The seek opera don't support\n");
		} else if (fs_msg.command == FS_CMD_READ) {
			fs_err_f("Opera:Read. The read opera don't support\n");
		} else if (fs_msg.command == FS_CMD_REPAIR) {
			fs_cache_flush_cache(&fs_msg, cache_ctl);
		} else if (fs_msg.command == FS_CMD_BLOCK) {
			fs_log("Opera:FS_CMD_BLOCK.\n");
			fs_block_arg = (FS_BLOCK_ARG *)(&fs_msg.write_arg);
			if (fs_block_arg->prev_num < 0 || fs_block_arg->later_num < 0) {
				cache_ctl->block_flag = 0;
				collision_later_number = 0;
				continue;
			}
			if (cache_ctl->block_flag >= 1)
				continue;
			cache_ctl->block_flag = 1;
			collision_later_number += fs_block_arg->later_num;
			if (fs_block_arg->prev_num)
				fileopera_ipcmsg_send_cmd(0,
				                          fs_msg.filename,
				                          (void *)&fs_block_arg->prev_num,
				                          FS_CMD_RENAME_PRE);
		} else if (fs_msg.command == FS_CMD_PIC) {
			fs_cache_picture_flush(&fs_msg);
		} else if (fs_msg.command == FS_CMD_EXIT) {
			fs_log("Opera: Cache pthread will exit \n");
			break;
		} else {
			fs_err("Opera: Unknow.\n");
			continue;
		}
	}
	return;
}

unsigned int fs_cache_totalmem_get()
{
	int ret;
	struct sysinfo s_info;

	ret = sysinfo(&s_info);
	if (ret < 0)
		return ret;

	return s_info.totalram;
}

int fs_cache_param_set()
{
	unsigned int total_mem;
	char system_command[FILENAME_LEN];
	/* set dirty_background_bytes */
	sprintf(system_command, "echo %d > %s\0",
	        dirty_background_bytes, DIRTY_BACKGROUND_BYTES);
	fs_manage_runapp(system_command);
	/* set dirty_bytes */
	sprintf(system_command, "echo %d > %s\0", dirty_bytes, DIRTY_BYTES);
	fs_manage_runapp(system_command);
	// set dirty_writeback_interval
	//sprintf(system_command, "echo %d > %s\0",
	//        dirty_writeback_centisecs, DIRTY_WRITEBACK_CENTISECS);
	//fs_manage_runapp(system_command);
	//set dirty_expire_interval
	//sprintf(system_command, "echo %d > %s\0",
	//        dirty_expire_centisecs, DIRTY_EXPIRE_CENTISECS);
	//fs_manage_runapp(system_command);
	//clear buffer

	sprintf(system_command, "echo %d > %s\0", mmc_max_block, MMC_MAX_BLOCK);
	fs_manage_runapp(system_command);

	total_mem = fs_cache_totalmem_get();
	if (total_mem > 128 * FS_SZ_1M)
		m_cache_count_max = CACHE_COUNT_MAX_DEFAULT * 2;
	return 0;
}

FS_RET fs_cache_init()
{
	FS_RET ret;

	fs_cache_param_set();
	if (pthread_mutex_init(&fs_msg_lock, NULL) != 0) {
		fs_err_f("fs_msg_lock init fail \n");
		goto ERT3;
	}

	ret = fs_cache_ipcmsg_init(RK_IPCMSGKEY, &m_cache_msgid);
	if (ret != FS_OK) {
		fs_err_f("cache_ipcmsg_init fail\n");
		goto RET2;
	}

	if (cache_manage_tid == 0) {
		if (pthread_create(&cache_manage_tid, NULL,
		                   fs_cache_manage_pthread, NULL)) {
			fs_err_f("pthread_create err\n");
			goto RET1;
		}

	}

	return FS_OK;

RET1:
	fs_cache_ipcmsg_deinit(RK_IPCMSGKEY, &m_cache_msgid);
RET2:
	pthread_mutex_destroy(&fs_msg_lock);
ERT3:
	return FS_NOK;
}

FS_RET fs_cache_deinit()
{
	if (cache_manage_tid) {
		fs_cache_file_send_exit();
		pthread_join(cache_manage_tid, NULL);
		cache_manage_tid = 0;
	}

	fs_cache_ipcmsg_deinit(RK_IPCMSGKEY, &m_cache_msgid);
	pthread_mutex_destroy(&fs_msg_lock);
	return FS_OK;
}

