/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_log"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "fs_log.h"

#define FS_LOG_MAX_LEN     256
#define LINE_SZ 1024

/* level low, wil printf the log */
#define FS_LOG_ERR 1
#define FS_LOG 2
#define FS_LOG_LEVEL 2
#define FS_ERR_TAG "fs_err"

typedef void (*fs_log_callback)(const char *, const char *, va_list);

unsigned int fs_debug = 0;

static const char *msg_log_warning = "log message is long\n";
static const char *msg_log_nothing = "\n";

void static os_log(const char *tag, const char *msg, va_list list)
{
	char line[LINE_SZ] = {0};

	snprintf(line, sizeof(line), "%s: %s", tag, msg);
	vfprintf(stdout, line, list);
}

void static os_err(const char *tag, const char *msg, va_list list)
{
	char line[LINE_SZ] = {0};

	snprintf(line, sizeof(line), "%s: %s: %s", FS_ERR_TAG, tag, msg);
	vfprintf(stderr, line, list);
}

void __fs_log(fs_log_callback func, const char *tag, const char *fmt,
              const char *fname, va_list args)
{
	char msg[FS_LOG_MAX_LEN + 1];
	char *tmp = msg;
	const char *buf = fmt;

	size_t len_fmt  = strnlen(fmt, FS_LOG_MAX_LEN);
	size_t len_name = (fname) ? (strnlen(fname, FS_LOG_MAX_LEN)) : (0);
	size_t buf_left = FS_LOG_MAX_LEN;
	size_t len_all  = len_fmt + len_name;

	if (NULL == tag)
		tag = MODULE_TAG;

	if (len_name) {
		buf = msg;
		buf_left -= snprintf(msg, buf_left, "%s ", fname);
		tmp += len_name + 1;
	}

	if (len_all == 0) {
		buf = msg_log_nothing;
	} else if (len_all >= FS_LOG_MAX_LEN) {
		buf_left -= snprintf(tmp, buf_left, "%s", msg_log_warning);
		buf = msg;
	} else {
		snprintf(tmp, buf_left, "%s", fmt);
		if (fmt[len_fmt - 1] != '\n') {
			tmp[len_fmt]    = '\n';
			tmp[len_fmt + 1]  = '\0';
		}
		buf = msg;
	}

	func(tag, buf, args);
}

#if (FS_LOG_LEVEL >= FS_LOG)
void _fs_log(const char *tag, const char *fmt, const char *fname, ...)
{
	va_list args;
	va_start(args, fname);
	__fs_log(os_log, tag, fmt, fname, args);
	va_end(args);
}
#else
void _fs_log(const char *tag, const char *fmt, const char *fname, ...)
{
	/* DO NOTHING */
}
#endif

#if (FS_LOG_LEVEL >= FS_LOG_ERR)
void _fs_err(const char *tag, const char *fmt, const char *fname, ...)
{
	va_list args;
	va_start(args, fname);
	__fs_log(os_err, tag, fmt, fname, args);
	va_end(args);
}
#else
void _fs_err(const char *tag, const char *fmt, const char *fname, ...)
{
	/* DO NOTHING */
}
#endif


