/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/ioctl.h>
#include <linux/msdos_fs.h>

#include "fs_storage.h"
#include "fs_file.h"
#include "fs_err.h"
#include "fs_log.h"
#include "fs_sdv.h"
#include "fs_msg.h"

int g_cycle_flag = 0; /* enable=0,disable=1 */

int fs_file_sd_abn(int flag)
{
	int mflag = flag;

	if (mflag == CARD_COM) {
		if (errno == EIO || errno == EINVAL && fs_manage_sd_exist(NULL) != 0)
			mflag = CARD_BAD;
		else if (errno == EROFS && fs_manage_sd_exist(NULL) != 0)
			mflag = FS_BAD;
		else if (errno == ENOSPC && fs_manage_sd_exist(NULL) != 0)
			mflag = FS_NOSPC;
	}
	switch (mflag) {
	case CARD_COM:
		break;
	case CARD_SLOW:
		fs_msg_notify_file(mflag, NULL, 0);
		fs_log("sdcard slow now \n");
		break;
	case CARD_BAD:
	case FS_BAD:
	case FS_NOSPC:
		usleep(500 * 1000);
		if(fs_manage_sd_exist(NULL) != 0) {
			fs_log("sdcard bad now errno = %d\n", errno);
			fs_msg_notify_file(mflag, NULL, 0);
		}
		break;
	default:
		break;
	}

	return 0;
}

inline int fs_file_get_cycle(void)
{
	return g_cycle_flag;
}

inline int fs_file_set_cycle(int val)
{
	g_cycle_flag = val;
}

int fs_file_media_offset(const char *filename)
{
	char *suffix = NULL;
	int offset = 0;

	if (filename == NULL)
		return 0;
	suffix = (strrchr(filename, '.') + 1);
	if (strcmp(suffix, "mov") == 0)
		offset = MOV_MEDIA_OFFSET;
	else if (strcmp(suffix, "mp4") == 0)
		offset = MP4_MEDIA_OFFSET;
	return offset;
}

/* get the file size and space , the units is K */
void fs_file_get_filesize(int fd, off_t *filesize, int *filespace)
{
	struct stat file_stat;

	if (fstat(fd, &file_stat) == -1)
		fs_assert(0);
	if (filesize != NULL)
		*filesize = file_stat.st_size;
	if (filespace != NULL)
		*filespace = file_stat.st_blocks * 512;
}

void fs_file_get_filesize_byname(const char* filename,
                                 size_t *filesize,
                                 size_t *filespace)
{
	struct stat file_stat;

	if (stat(filename, &file_stat) == -1)
		return;
	if (filesize != NULL)
		*filesize = file_stat.st_size;
	if (filespace != NULL)
		*filespace = file_stat.st_blocks * 512;
}

void fs_file_get_fullpath_name(char *path, char *filename,
                               char *fullpath_name, int len)
{
	snprintf(fullpath_name, len, "%s/%s", path, filename);
}

void fs_file_set_hide_path(char *filename)
{
	int fd;
	int attr;

	fd = open(filename, O_RDWR | O_PATH | O_DIRECTORY, S_IRWXU);
	if (fd < 0) {
		fs_err_f("open filename = %s fail, errno = %d\n", filename, errno);
		return;
	}

	ioctl(fd, FAT_IOCTL_GET_ATTRIBUTES, &attr);
	attr = attr | ATTR_HIDDEN;
	ioctl(fd, FAT_IOCTL_SET_ATTRIBUTES, &attr);

	close(fd);
}

void fs_file_set_hide_byname(char *filename)
{
	int fd;
	int attr;

	fd = open(filename, O_RDWR, S_IRWXU);
	if (fd < 0) {
		fs_err_f("open filename = %s fail\n", filename);
		return;
	}

	ioctl(fd, FAT_IOCTL_GET_ATTRIBUTES, &attr);
	attr = attr | ATTR_HIDDEN;
	ioctl(fd, FAT_IOCTL_SET_ATTRIBUTES, &attr);

	close(fd);
}

int fs_file_is_hide(int fd)
{
	int attr;

	ioctl(fd, FAT_IOCTL_GET_ATTRIBUTES, &attr);
	attr = attr & ATTR_HIDDEN;
	return attr;
}

void fs_file_set_hide(int fd)
{
	int attr;

	ioctl(fd, FAT_IOCTL_GET_ATTRIBUTES, &attr);
	attr = attr | ATTR_HIDDEN;
	ioctl(fd, FAT_IOCTL_SET_ATTRIBUTES, &attr);
}

void fs_file_set_visible(int fd)
{
	int attr;

	ioctl(fd, FAT_IOCTL_GET_ATTRIBUTES, &attr);
	attr = attr & (~ATTR_HIDDEN);
	ioctl(fd, FAT_IOCTL_SET_ATTRIBUTES, &attr);
}

inline void fs_file_enable_cycle()
{
	fs_file_set_cycle(0);
}

inline void fs_file_disable_cycle()
{
	fs_file_set_cycle(1);
}

static FS_RET create_path(char *path, int hide)
{
	char *p_path = NULL;
	char parent_path[FILENAME_LEN] = {0};

	memcpy(parent_path, path, FILENAME_LEN);
	p_path = (char *)dirname(parent_path);
	if (access (p_path, 0) == -1)
		create_path(p_path, hide);

	if (mkdir(path, 0777)) {
		fs_file_sd_abn(CARD_COM);
		if (errno != EEXIST)
			return FS_NOK;
	}
	if (hide)
		fs_file_set_hide_path(path);

	return FS_OK;
}

int fs_file_create_path(char *path, int hide)
{
	if (access(path, 0) == -1)
		return create_path(path, hide);
}

int fs_file_open_normal(const char *filename, int flags, int mode)
{
	int ret = 0;

	ret = open(filename, flags, mode);
	if (ret < 0)
		fs_file_sd_abn(CARD_COM);
	return ret;
}

int fs_file_close_normal(int fd)
{
	int ret = 0;

	ret = close(fd);
	if (ret < 0)
		fs_file_sd_abn(CARD_COM);
	return ret;
}

int fs_file_open(const char *filename, int flags, int mode)
{
	FS_RET ret;
	FS_MODE fs_mode;
	int fd = -1;
	int tm, tm1;
	char *recingname = NULL;
	struct timeval tv1, tv2, tv3;

	fs_mode = fs_get_mode();
	if (flags & O_CREAT) {
		gettimeofday(&tv1, NULL);
		if (fs_mode != FS_SDV) {
			if (fs_file_get_cycle()) {
				ret = fs_storage_check_head_name(filename);
				if (ret == FS_NOK) {
					fs_msg_notify_file(FILE_FULL, filename, 0);
					return -1;
				}
			}
			if (fs_mode == FS_CVR && strstr(filename, "mp4") != NULL)
				fs_storage_file_prepare(filename);
			if (fs_mode == FS_CVR || fs_mode != FS_CVR && strstr(filename, "mp4") != NULL) {
				ret = fs_manage_rename_head(filename);
				if (ret != FS_OK) {
					fs_err_f("rename fail,filename = %s\n", filename);
					return -1;
				}
				ret = fs_manage_insert_file(filename);
				if (ret != FS_OK) {
					fs_err_f("rename fail,filename = %s\n", filename);
					return -1;
				}
				fs_storage_set_recing_filename((char *)filename);
				recingname = fs_storage_get_recing_filename((char *)filename);
			}
		}
		gettimeofday(&tv2, NULL);
		int new_flag = O_CREAT | O_RDWR | O_NOATIME | O_DIRECT;
		fd = open(filename, new_flag, mode);
		if (fd < 0) {
			printf("Warning to open, errno = %d, fd = %d\n", errno, fd);
			fs_file_sd_abn(CARD_COM);
			return FS_NOK;
		}
		//reset hide file
		if (mode != FS_SDV) {
			if (fs_file_is_hide(fd))
				fs_file_set_visible(fd);
		}
		fs_msg_notify_file(FILE_NEW, filename, 0);
		gettimeofday(&tv3, NULL);
		tm = (tv2.tv_usec - tv1.tv_usec) + (tv2.tv_sec - tv1.tv_sec) * 1000000;
		tm1 = (tv3.tv_usec - tv2.tv_usec) + (tv3.tv_sec - tv2.tv_sec) * 1000000;
		printf("Rename file use %duS, Open use %duS all use %duS\n",
		        tm, tm1, tm + tm1);

	} else {
		fd = open(filename, flags, mode);
		if (fd < 0) {
			printf("Warning to open, errno = %d\n", errno);
		}
	}
	return fd;
}

int fs_file_close(int fd)
{
	struct timeval t1;
	struct timeval t2;
	struct timeval t3;

	off_t filesize = 0;
	int filespace = 0;

	int delay = 0;
	int delay1 = 0;
	int test_fd = 0;
	gettimeofday(&t1, NULL);
	//fsync(fd);
	gettimeofday(&t2, NULL);
	int ret = close(fd);
	if (ret < 0)
		fs_file_sd_abn(CARD_COM);

	gettimeofday(&t3, NULL);

	delay = (t2.tv_sec - t1.tv_sec) * 1000000 + (t2.tv_usec - t1.tv_usec);
	delay1 = (t3.tv_sec - t2.tv_sec) * 1000000 + (t3.tv_usec - t2.tv_usec);

	printf("close use= %duS, fsync use= %duS, all use =%duS\n",
	       delay1, delay, delay + delay1);
	return ret;
}

ssize_t fs_file_write(int fd, const void *buf, size_t count)
{
	ssize_t ret;

	ret = write(fd, buf, count);
	if (ret < 0)
		fs_file_sd_abn(CARD_COM);
	return ret;
}

ssize_t fs_file_read(int fd, void *buf, size_t count)
{
	return read(fd, buf, count);
}

int64_t fs_file_lseek(int fd, int64_t offset , int whence)
{
	return lseek(fd, offset, whence);
}
