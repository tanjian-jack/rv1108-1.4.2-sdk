/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MODULE_TAG "fs_buffer"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/prctl.h>
#include <assert.h>

#include "fs_storage.h"
#include "fs_cache.h"
#include "fs_log.h"
#include "fs_file.h"
#include "fs_sdv.h"
#include "fs_sdcard.h"
#include "fs_path.h"
#include "fs_msg.h"

pthread_mutex_t filelistlock;
pthread_t fs_manage_tid = 0;
float fs_free_per;
float fs_use_per;
long long tf_free_size;
long long tf_total_size;

struct file_list *mvideo_file_list = NULL;
struct file_list *mlock_file_list = NULL;
struct file_list *mpicture_file_list = NULL;
struct file_list *mmedia_file_list = NULL;

#define SMALL_SPACE_RENAMEFILE  (1600)  //1.6G
#define SMALL_SPACE_REMOVERFILE  (1500)  //1.5G

#define FALLOCATE_FIRST_SIZE (16 * FS_SZ_1M)
#define FALLOCATE_INCRE_SIZE (16 * FS_SZ_1M)
#define FTRUNCATE_SZIE (64 * FS_SZ_1K) //KB
#define MAX_DELAY_TIME (50 * 1000) //us

int m_fileopera_msgid = -1;

typedef struct _FILEOPERA_INFO {
	int real_fd;
	int file_size;
	int file_space;
	char *filename;
	char mediasize[4];
	FS_COMMAND command;
} FILEOPERA_INFO;

typedef struct _FILEOPERA_MSG {
	long msg_type;
	char fs_msg[sizeof(FILEOPERA_INFO)];
} FILEOPERA_MSG;

typedef struct _VIDEO_REPAIR_CALLBACK {
	int (*repair_callback_func)(void *, int);
	void *repair_callback_param;
} VIDEO_REPAIR_CALLBACK;
VIDEO_REPAIR_CALLBACK mvideo_repair_callback;

void fs_manage_get_filesize(int fd, off_t *filesize, int *filespace);
void fs_manage_fallocate_manager(int fd, int filesize, off_t filespace);
int file_manage_collision_rename(char *fullpath_name,
                                 char *fullpath_newname,
                                 char *path,
                                 char *format);
extern void cache_time_printf(int line);
extern void cache_time_printf_f(const char *func);
struct file_node *fs_find_filenode_byname(struct file_list *list,
                                          const char *filename);

int fileopera_ipcmsg_getnum(int msgid)
{
	int ret;
	struct msqid_ds msg_info;

	if (msgid < 0) {
		printf("%s, msgid = %d, exit\n", __func__, msgid);
		return -1;
	}

	ret = msgctl(msgid, IPC_STAT, &msg_info);
	if (ret < 0) {
		printf("failed to get info | errno=%d [%s]/n", errno, strerror(errno));
		return -1;
	}

	return msg_info.msg_qnum;
}

FS_RET fileopera_ipcmsg_init(int msg_key, int *m_msgid)
{
	int ret;
	int msgid;
	int retry_num = 0;

RETRY:
	msgid = msgget(msg_key, IPC_EXCL);  /*check msg*/
	if (msgid < 0) {
		msgid = msgget(msg_key, IPC_CREAT | 0666); /*create msg*/
		if (msgid < 0) {
			printf("failed to create msq | errno=%d [%s]/n", errno, strerror(errno));
			return FS_NOK;
		}
	}
	ret = fileopera_ipcmsg_getnum(msgid);
	if (ret > 0) {
		ret = msgctl(msgid, IPC_RMID, NULL);
		if (ret < 0)
			assert(0);
		retry_num++;
		if (retry_num > 3) //retry, but the msg can't delete
			assert(0);
		goto RETRY;
	}
	*m_msgid = msgid;
	return FS_OK;
}

void fileopera_ipcmsg_deinit(int msg_key, int *m_msgid)
{
	int msgid;
	/* if ipcmsg init fail, the m_msgid will be set to  -1, so don't deinit again */
	if (*m_msgid < 0)
		return;

	msgid = msgget(msg_key, IPC_EXCL);  /*check ipc msg*/
	if (msgid < 0) {
		printf("failed to get id | errno=%d [%s]/n", errno, strerror(errno));
		return;
	}

	msgctl(msgid, IPC_RMID, 0); //remove ipc msg

	*m_msgid = -1;
	return;
}

int fileopera_ipcmsg_rec(int msgid, FILEOPERA_INFO *fs_msg)
{
	int ret;
	FILEOPERA_MSG ipc_msg_recv;

	if (fs_msg == NULL)
		return -1;
	if (msgid < 0)
		return -1;

	ret = msgrcv(msgid, &ipc_msg_recv, sizeof(FILEOPERA_INFO), 0, 0);
	if (ret < 0) {
		printf("msgsnd() read msg failed,errno=%d[%s]\n", errno, strerror(errno));
		return -1;
	}
	memcpy((char *)fs_msg, ipc_msg_recv.fs_msg, sizeof(FILEOPERA_INFO));

	return 0;
}

int fileopera_ipcmsg_send(int msgid, FILEOPERA_INFO *fs_msg)
{
	int ret;
	FILEOPERA_MSG ipc_msg_send;

	if (fs_msg == NULL)
		return -1;
	if (msgid < 0)
		return -1;

	ipc_msg_send.msg_type = 1;
	memcpy(ipc_msg_send.fs_msg, (char *)fs_msg, sizeof(FILEOPERA_INFO));
	ret = msgsnd(msgid, (void *)&ipc_msg_send, sizeof(FILEOPERA_INFO), IPC_NOWAIT);
	if (ret < 0) {
		printf("msgsnd() write msg failed,ret = %d errno=%d[%s]\n", ret, errno, strerror(errno));
		printf("msgsnd() write msg failed,msg number = %d\n", fileopera_ipcmsg_getnum(msgid));
		return -1;
	}

	return 0;
}

int fileopera_ipcmsg_send_cmd(int fd, void *param1, void *param2, int command)
{
	FILEOPERA_INFO ipc_msg_send;
	char *filename = NULL;
	int *filesize = NULL;
	int *filespace = NULL;
	int block_prev_number= 0;

	ipc_msg_send.command = command;
	if (command == FS_CMD_OPEN) {
		filename = (char *)param1;
		ipc_msg_send.filename = strdup(filename);
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else if (command == FS_CMD_WRITE) {
		filesize = (int *)param1;
		filespace = (int *)param2;
		if (*filespace == 0) {
			*filespace = FALLOCATE_FIRST_SIZE;
			return 0;
		}
		/* fize small to 1/4 */
		if ((*filespace - *filesize) < FALLOCATE_INCRE_SIZE / 2) {
			ipc_msg_send.command = FS_CMD_WRITE;
			ipc_msg_send.real_fd = fd;
			ipc_msg_send.file_size = *filesize;
			ipc_msg_send.file_space = *filespace;
			fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
			//fs_manage_fallocate_manager(fd, *filesize, *filespace);
			*filespace += FALLOCATE_INCRE_SIZE;
		}
	} else if (command == FS_CMD_CLOSE) {
		ipc_msg_send.real_fd = fd;
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else if (command == FS_CMD_WRITE_SIZE) {
		filename = (char *)param1;
		memcpy(ipc_msg_send.mediasize, (char *)param2, 4);
		ipc_msg_send.real_fd = fd;
		ipc_msg_send.filename = strdup(filename);
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else if (command == FS_CMD_RENAME_CUR) {
		filename = (char *)param1;
		ipc_msg_send.filename = strdup(filename);
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else if (command == FS_CMD_RENAME_PRE) {
		filename = (char *)param1;
		block_prev_number = *(int *)param2;
		ipc_msg_send.file_size = block_prev_number;
		ipc_msg_send.filename = strdup(filename);
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else if (command == FS_CMD_EXIT) {
		fileopera_ipcmsg_send(m_fileopera_msgid, &ipc_msg_send);
	} else
		fs_log("Fileopera_ipcmsg_send_cmd cmd = %d, unknow\n", command);

	return 0;
}

const char *filename_get(const char *fullpath_name)
{
	if (fullpath_name == NULL)
		return NULL;

	return (strrchr(fullpath_name, '/') + 1);
}

int string_to_time(const char *strDateStr, time_t *timeData)
{
	char year[5];
	char mouth[3];
	char day[3];
	char hour[3];
	char min[3];
	char sec[3];
	struct tm sourcedate;
	int stroffset = 0;

	if (*strDateStr == '.')
		stroffset = 1;

	strncpy(year, strDateStr + stroffset, 4);
	year[4] = 0;

	strncpy(mouth, strDateStr + stroffset + 4, 2);
	mouth[2] = 0;

	strncpy(day, strDateStr + stroffset + 6, 2);
	day[2] = 0;

	strncpy(hour, strDateStr + stroffset + 9, 2);
	hour[2] = 0;

	strncpy(min, strDateStr + stroffset + 11, 2);
	min[2] = 0;

	strncpy(sec, strDateStr + stroffset + 13, 2);
	sec[2] = 0;

	memset(&sourcedate, 0, sizeof(sourcedate));
	sourcedate.tm_year = atoi(year) - 1900;
	sourcedate.tm_mon = atoi(mouth) - 1;
	sourcedate.tm_mday = atoi(day);
	sourcedate.tm_hour = atoi(hour);
	sourcedate.tm_min = atoi(min);
	sourcedate.tm_sec = atoi(sec);

	/* If time such ".2016", set the time to older then normal video */
	if (stroffset != 0 && sourcedate.tm_year > 70)
		sourcedate.tm_year = 71;

	*timeData = mktime(&sourcedate);

	return 0;
}

void time_to_string(char *filename, int len, time_t time,
                          char *path, char *format)
{
	struct tm *tm;

	tm = localtime(&time);
	snprintf(filename, len, "%s/.%04d%02d%02d_%02d%02d%02d_%s",
	                   path, tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
	                   tm->tm_hour, tm->tm_min, tm->tm_sec, format);
}

void dump_filelist(struct file_list *list)
{
	struct file_node *node_tmp;
	int i = 0;

	if (list == NULL)
		return;

	node_tmp = list->folder_head;
	i = 0;
	while (node_tmp) {
		printf("folder%d %s\n", i, node_tmp->name);
		i++;
		node_tmp = node_tmp->next;
	}

	node_tmp = list->file_head;
	i = 0;
	while (node_tmp) {
		printf("file%d %s\n", i, node_tmp->name);
		i++;
		node_tmp = node_tmp->next;
	}
}

void fs_update_space(struct file_node *node, int update_type)
{
	int filesize = 0;
	struct file_list *list = NULL;

	filesize = node->filesize;
	list = node->file_list;

	/*
	 * type == -1, remove a file
	 * type == 1, add the a file
	 */
	if (update_type == 1) {
		list->use_space += filesize;
		list->free_space -= filesize;
	} else if (update_type == -1) {
		list->use_space -= filesize;
		list->free_space += filesize;
	} else {
		fs_assert(0);
	}
}

struct file_node *new_filenode(const char *name, struct file_list *list, size_t size)
{
	struct file_node *node_new = NULL;

	node_new = calloc(1, sizeof(struct file_node));
	if (node_new == NULL)
		return NULL;
	node_new->name = strdup(name);
	node_new->file_list = list;
	string_to_time(name, &node_new->time);
	node_new->filesize = size;
	return node_new;
}

void free_filenode(struct file_node *node)
{
	if (node) {
		if (node->name)
			free(node->name);
		free(node);
	}
}

void fs_delete_filenode(struct file_list *list, struct file_node *node)
{
	if (list == NULL || node == NULL)
		return;

	if (node->pre)
		node->pre->next = node->next;
	else
		list->file_head = node->next;
	if (node->next)
		node->next->pre = node->pre;
	else
		list->file_tail = node->pre;

	fs_update_space(node, -1);
	free_filenode(node);
	list->filenum--;
}

void fs_delete_filenode_byname(struct file_list *list, const char *filename)
{
	struct file_node *filenode = NULL;

	if (list == NULL)
		return;

	pthread_mutex_lock(&filelistlock);
	filenode = fs_find_filenode_byname(list, filename);
	if (filenode == NULL)
		goto RET;
	fs_delete_filenode(list, filenode);
RET:
	pthread_mutex_unlock(&filelistlock);
}

void fs_delete_last_node(struct file_list *list)
{
	struct file_node *filenode = NULL;

	filenode = fs_get_head_filenode(list);
	if (filenode == NULL)
		return;

	pthread_mutex_lock(&filelistlock);
	fs_delete_filenode(list, filenode);
	pthread_mutex_unlock(&filelistlock);
}

int insert_filenode(struct file_list *list, struct file_node *node)
{
	struct file_node *node_tmp = NULL;

	if (list == NULL || node == NULL)
		return -1;

	if (list->file_head == 0) {
		list->file_head = node;
		list->file_tail = node;
	} else {
		node_tmp = list->file_head;
		while (node_tmp) {
			if (node->time == node_tmp->time) {
				if (strcmp(node->name, node_tmp->name) == 0) {
					fs_err_f("path = %s, node->name = %s, node_tmp->name = %s\n",
					         list->path, node->name, node_tmp->name);
					return -1;
				}
			}

			if (node->time > node_tmp->time)
				node_tmp = node_tmp->next;
			else
				break;
		}
		if (node_tmp == NULL) {
			list->file_tail->next = node;
			node->pre = list->file_tail;
			list->file_tail = node;
		} else {
			if (list->file_head == node_tmp) {
				list->file_head = node;
			} else {
				node_tmp->pre->next = node;
				node->pre = node_tmp->pre;
			}
			node->next = node_tmp;
			node_tmp->pre = node;
		}
	}
	list->filenum++;
	return 0;
}

int insert_foldernode(struct file_list *list, struct file_node *node)
{
	struct file_node *node_tmp = NULL;

	if (list == NULL || node == NULL)
		return -1;

	if (list->folder_head == 0) {
		list->folder_head = node;
		list->folder_tail = node;
	} else {
		node_tmp = list->folder_head;
		while (node_tmp) {
			if (node->time > node_tmp->time)
				node_tmp = node_tmp->next;
			else
				break;
		}
		if (node_tmp == NULL) {
			list->folder_tail->next = node;
			node->pre = list->folder_tail;
			list->folder_tail = node;
		} else {
			if (list->folder_head == node_tmp) {
				list->folder_head = node;
			} else {
				node_tmp->pre->next = node;
				node->pre = node_tmp->pre;
			}
			node->next = node_tmp;
			node_tmp->pre = node;
		}
	}

	return 0;
}

struct file_node *fs_get_tail_filenode(struct file_list *list)
{
	if (list == NULL)
		return NULL;

	return list->file_tail;
}

struct file_node *fs_get_head_filenode(struct file_list *list)
{
	if (list == NULL)
		return NULL;

	return list->file_head;
}

struct file_node *fs_get_next_filenode(struct file_node *node)
{
	if (node == NULL)
		return NULL;

	return node->next;
}

struct file_node *fs_get_pre_filenode(struct file_node *node)
{
	if (node == NULL)
		return NULL;

	return node->pre;
}

void fs_get_node_fullpath_name(struct file_node *node, char *name, int len)
{
	snprintf(name, len, "%s/%s", node->file_list->path, node->name);
}

struct file_node *fs_find_filenode_byname(struct file_list *list,
                                          const char *filename)
{
	struct file_node *cur_node = NULL;
	struct file_node *node = NULL;
	time_t time = 0;

	if (list == NULL || filename == NULL)
		return NULL;

	string_to_time(filename, &time);
	cur_node = fs_get_head_filenode(list);

	while (cur_node) {
		if (time == cur_node->time && strcmp(cur_node->name, filename) == 0) {
			fs_log("fs_find_filenode_byname, name = %s\n", cur_node->name);
			node = cur_node;
			break;
		}
		cur_node = cur_node->next;
	}
	return node;
}

struct file_node *fs_find_filenode_byfullpathname(char *fullpath_name)
{
	const char *filename = NULL;
	struct file_node *filenode = NULL;
	struct file_list *filelist = NULL;

	if (fullpath_name == NULL)
		return NULL;
	filelist = fs_storage_filelist_get(fullpath_name);
	if (filelist == NULL)
		return NULL;

	filename = filename_get(fullpath_name);
	filenode = fs_find_filenode_byname(filelist, filename);
	return filenode;
}

int fs_storage_get_early_name(struct file_list *list, char *filename)
{
	struct file_node *node = NULL;

	node = fs_get_head_filenode(list);
	if (node == NULL)
		return -1;

	fs_get_node_fullpath_name(node, filename, FILENAME_LEN);
	return 0;
}

int fs_storage_get_last_name(struct file_list *list, char *filename)
{
	struct file_node *node = NULL;

	node = fs_get_tail_filenode(list);
	if (node == NULL)
		return -1;

	sprintf(filename, "%s/%s\0", list->path, node->name);
	return 0;
}

FS_RET fs_manage_rename_head(const char *fullpath_filename)
{
	char rename_name[FILENAME_LEN] = {0};
	struct file_node *filenode = NULL;
	struct file_list *filelist = NULL;

	filelist = fs_storage_filelist_get(fullpath_filename);
	if (filelist == NULL)
		return FS_NOK;

	if (!filelist->filenum)
		return FS_OK;

	if (filelist->filenum_lim && filelist->filenum_lim > filelist->filenum)
		return FS_OK;

	pthread_mutex_lock(&filelistlock);
	filenode = fs_get_head_filenode(filelist);
	if (filenode == NULL) {
		pthread_mutex_unlock(&filelistlock);
		return FS_NOK;
	}
	fs_get_node_fullpath_name(filenode, rename_name, FILENAME_LEN);
	rename(rename_name, fullpath_filename);
	fs_msg_notify_file(FILE_DEL, rename_name, 0);
	fs_delete_filenode(filelist, filenode);
	pthread_mutex_unlock(&filelistlock);

	fs_log("old_name = %s, new name = %s\n", rename_name, fullpath_filename);
	return FS_OK;
}

FS_RET fs_storage_check_head(const char *fullpath_filename)
{
	size_t filesize = 0;
	struct file_node *filenode = NULL;
	struct file_list *filelist = NULL;

	filelist = fs_storage_filelist_get(fullpath_filename);
	if (filelist == NULL)
		return FS_NOK;

	filenode = fs_get_head_filenode(filelist);
	if (filenode == NULL)
		return FS_NOK;

	filesize = fs_storage_filesize_get_bynode(filenode);
	if (filesize != filelist->filesize)
		return FS_NOK;
	return FS_OK;
}

FS_RET fs_storage_check_head_name(const char *filename)
{
	char *name;
	struct file_node *filenode = NULL;
	struct file_list *filelist = NULL;

	filelist = fs_storage_filelist_get(filename);
	if (filelist == NULL)
		return FS_NOK;

	filenode = fs_get_head_filenode(filelist);
	if (filenode == NULL)
		return FS_NOK;

	name = fs_storage_filename_get_bynode(filenode);
	if (strstr(name, FS_PREPARE_FORMAT) == NULL)
		return FS_NOK;
	return FS_OK;
}

struct file_node *check_filenodebyname(struct file_list *list,
                                       const char *name,
                                       int number)
{
	int ret = 0;
	struct file_node *node_tmp;

	if (list == NULL)
		return;

	node_tmp = list->file_tail;

	while (node_tmp) {
		if (strcmp(node_tmp->name, name) == 0)
			break;
		ret++;
		if (number != 0 && ret >= number) {
			node_tmp = NULL;
			break;
		}
		node_tmp = node_tmp->pre;
	}

	return node_tmp;
}

FS_RET creat_filelist(struct file_list *list)
{
	int i = 0;
	DIR *dp = NULL;
	struct dirent *dirp = NULL;
	size_t file_size = 0;
	size_t file_space = 0;
	struct file_node *node_new = NULL;
	char fullpath_name[FILENAME_LEN] = {0};

	if (list == NULL)
		return FS_NOK;
	if ((dp = opendir(list->path)) == NULL) {
		perror("opendir");
		return FS_NOK;
	}

	while (((dirp = readdir(dp)) != NULL)) {
		if (fs_sdcard_get_mount() == SD_UMOUNT)
			return FS_INITFAIL;
		if (strstr(dirp->d_name, list->format) == NULL &&
			strstr(dirp->d_name, FS_PREPARE_FORMAT) == NULL)
			continue;
		fs_file_get_fullpath_name(list->path, dirp->d_name,
		                          fullpath_name, FILENAME_LEN);
		if (list->filetype != THUMBFILE_TYPE)
			fs_file_get_filesize_byname(fullpath_name, &file_size, &file_space);
		node_new = new_filenode(dirp->d_name,
		                        list,
		                        file_size);
		if (node_new == NULL)
			break;

		if (dirp->d_type == 4) {
			insert_foldernode(list, node_new);
		} else {
			if (insert_filenode(list, node_new) == -1)
				free_filenode(node_new);
			else
				fs_update_space(node_new, 1);
			i++;
		}
	}
	closedir(dp);
	list->filenum = i;

	return FS_OK;
}

void free_filelist(struct file_list *list)
{
	struct file_node *node_tmp;

	if (list == NULL)
		return;

	node_tmp = list->folder_head;
	while (node_tmp) {
		list->folder_head = node_tmp->next;
		free_filenode(node_tmp);
		node_tmp = list->folder_head;
	}
	list->folder_tail = NULL;
	node_tmp = list->file_head;
	while (node_tmp) {
		list->file_head = node_tmp->next;
		free_filenode(node_tmp);
		node_tmp = list->file_head;
	}
	list->file_tail = NULL;
	free(list);
}

void fs_manage_getsdcardcapacity(long long *free_size, long long *total_size)
{
	*free_size = tf_free_size;
	*total_size = tf_total_size;
}

int fs_manage_insert_file(const char *fullpath_name)
{
	size_t file_size = 0;
	size_t file_space = 0;
	struct file_node *node_new;
	struct file_list *filelist;

	filelist = fs_storage_filelist_get(fullpath_name);

	if (filelist) {
		fs_file_get_filesize_byname(fullpath_name, &file_size, &file_space);
		node_new = new_filenode(filename_get(fullpath_name),
		                        filelist,
		                        file_size);
		if (node_new == NULL)
			return -1;
		pthread_mutex_lock(&filelistlock);
		if (insert_filenode(filelist, node_new) == -1)
			free_filenode(node_new);
		else
			fs_update_space(node_new, 1);
		pthread_mutex_unlock(&filelistlock);
	}
	return 0;
}

int fs_manage_insert_filepath(const char *path, const char *filename)
{
	char fullpath_name[FILENAME_LEN] = {0};
	snprintf(fullpath_name, FILENAME_LEN, "%s/%s", path, filename);
	return fs_manage_insert_file(fullpath_name);
}

int fs_storage_filelist_init(FS_STORAGE_PATH *storage_path, struct file_list **file_list)
{
	long long tf_space = 0;
	long long tf_free = 0;

	fs_sdcard_check_capacity(&tf_free, &tf_space, NULL);
	tf_space = tf_space * FS_SZ_1M;
	if (*file_list == NULL) {
		*file_list = calloc(1, sizeof(struct file_list));
		if (*file_list == NULL) {
			fs_log("creat video file list err\n");
			return FS_NOK;
		}
		(*file_list)->path = storage_path->storage_path;
		(*file_list)->format = storage_path->storage_format;
		(*file_list)->filetype = storage_path->filetype;
		(*file_list)->video_type = storage_path->video_type;
		(*file_list)->filesize = storage_path->filesize;
		(*file_list)->filesize_align = storage_path->filesize_align;
		(*file_list)->all_space = (storage_path->storage_percent * tf_space / 100);
		(*file_list)->free_space = (storage_path->storage_percent * tf_space / 100);
		(*file_list)->filenum_lim = storage_path->filenum;
	}

	if (*file_list == NULL)
		fs_log("fs_storage_filelist_init == NULL\n");
	return FS_OK;
}

void fs_manage_free_filelist(struct file_list **list)
{
	if (*list == NULL)
		return;

	free_filelist(*list);
	*list = NULL;

	return;
}

int fs_storage_create_list()
{
	int num;
	int ret;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		pthread_mutex_lock(&filelistlock);
		fs_storage_filelist_init(&fs_storage_tbl[num],
		                         &fs_storage_tbl[num].filelist);
		ret = creat_filelist(fs_storage_tbl[num].filelist);
		pthread_mutex_unlock(&filelistlock);
		if (ret)
			return ret;
	}
	return 0;
}

void fs_storage_free_list()
{
	int num;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		pthread_mutex_lock(&filelistlock);
		fs_manage_free_filelist(&fs_storage_tbl[num].filelist);
		pthread_mutex_unlock(&filelistlock);
	}
}

struct file_list *fs_storage_colli_video_get(const char *colli_name)
{
	int num;
	char *colli_path = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		colli_path = fs_storage_tbl[num].colli_path;
		if (*colli_path != 0 && strstr(colli_name, colli_path) != NULL) {
			return fs_storage_tbl[num].filelist;
		}
	}
	return NULL;
}

struct file_list *fs_storage_filelist_get(const char *fullpath_filename)
{
	int num;
	struct file_list *filelist = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(fullpath_filename, fs_storage_tbl[num].storage_path) != NULL) {
			filelist = fs_storage_tbl[num].filelist;
			break;
		}
	}
	return filelist;
}

struct file_list *fs_storage_collisionlist_get(const char *fullpath_filename)
{
	int num;
	char *collipath = NULL;
	struct file_list *filelist = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(fullpath_filename, fs_storage_tbl[num].storage_path) != NULL) {
			collipath = fs_storage_tbl[num].colli_path;
			break;
		}
	}
	if (collipath == NULL)
		return NULL;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(collipath, fs_storage_tbl[num].storage_path) != NULL) {
			filelist = fs_storage_tbl[num].filelist;
			break;
		}
	}

	return filelist;
}

FS_RET fs_storage_check_formated()
{
	int num;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (access(fs_storage_tbl[num].storage_path, F_OK) == -1) {
			fs_err_f("path: access fail\n", fs_storage_tbl[num].storage_path);
			break;
		}
	}

	return num >= STORAGE_PATH_NUM ? FS_OK : FS_NOK;
}

FS_RET fs_storage_check_file()
{
	FS_RET ret = FS_OK;
	int number = 0;
	int filenum = 0;
	long long need_space = 0;
	long long free_size = 0;
	long long total_size = 0;
	struct file_list *file_list = NULL;
	FS_STORAGE_PATH *path_node = NULL;

	fs_storage_foreach_path(number, path_node) {
		if (path_node->filetype == THUMBFILE_TYPE
		    || path_node->filetype == GPS_TYPE)
			continue;

		file_list = path_node->filelist;
		if (file_list == NULL)
			continue;
		fs_assert(file_list->filesize != 0);

		fs_sdcard_check_capacity(&free_size, &total_size, NULL);
		if (free_size < fs_storage_get_free_cap(total_size)) {
			if (file_list->use_space == 0) {
				fs_err_f("Ff_total leak of size = %lld\n", free_size);
				return FS_ERR_UNKNOW;
			} else {
				continue;
			}
		}
		free_size -= fs_storage_get_free_cap(total_size);
		need_space = free_size * FS_SZ_1M;
		if (need_space > file_list->free_space)
			need_space = file_list->free_space;
		if (need_space < file_list->filesize)
			continue;
		filenum = need_space / file_list->filesize;
		fs_err_f("Create file, num=%d, space=%lld\n", filenum, need_space);
		ret = fs_storage_create_mediafile(filenum, 1, path_node);
		if (ret != FS_OK)
			break;
	}

	return ret;
}

int fs_storage_sizealign_get_bytype(int videotype, FILETYPE filetype)
{
	int num;
	int filesize_align = 0;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (fs_storage_tbl[num].video_type == videotype &&
			fs_storage_tbl[num].filetype == filetype) {
			filesize_align = fs_storage_tbl[num].filesize_align;
			break;
		}
	}

	return filesize_align;
}

char *fs_storage_folder_get_bytype(int videotype, FILETYPE filetype)
{
	int num;
	char *folder = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (fs_storage_tbl[num].video_type == videotype &&
			fs_storage_tbl[num].filetype == filetype) {
			folder = fs_storage_tbl[num].storage_path;
			break;
		}
	}

	return folder;
}

int fs_storage_align_get_bynode(struct file_node *filenode)
{
	return filenode->file_list->filesize_align;
}

char *fs_storage_folder_get_bynode(struct file_node *filenode)
{
	return filenode->file_list->path;
}

int fs_storage_filetype_get_bynode(struct file_node *filenode)
{
	return filenode->file_list->filetype;
}

char *fs_storage_filename_get_bynode(struct file_node *filenode)
{
	return filenode->name;
}

size_t fs_storage_filesize_get_bynode(struct file_node *filenode)
{
	return filenode->filesize;
}

char *fs_storage_format_get_bynode(struct file_node *filenode)
{
	return filenode->file_list->format;
}

int fs_storage_videotype_get_bynode(struct file_node *filenode)
{
	return filenode->file_list->video_type;
}

FS_STORAGE_PATH *fs_storage_node_get_bypath(const char *pathname)
{
	int num;
	FS_STORAGE_PATH *current_path = NULL;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(pathname, fs_storage_tbl[num].storage_path) != NULL) {
			current_path = &fs_storage_tbl[num];
			break;
		}
	}

	return current_path;
}

int fs_storage_fileinfo_get_bypath(char *path, FILETYPE *filetype, int *videotype)
{
	int num;
	struct file_list *current_file_list = NULL;

	if (path == NULL)
		return -1;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(path, fs_storage_tbl[num].storage_path) != NULL) {
			current_file_list = fs_storage_tbl[num].filelist;
			break;
		}
	}
	if (current_file_list) {
		*filetype = current_file_list->filetype;
		*videotype = current_file_list->video_type;
	}
	return 0;
}

struct file_list *fs_storage_filelist_get_bypath(char *path)
{
	int num;
	struct file_list *current_file_list = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(path, fs_storage_tbl[num].storage_path) != NULL) {
			current_file_list = fs_storage_tbl[num].filelist;
			break;
		}
	}

	return current_file_list;
}

struct file_list *fs_storage_filelist_get_bytype(int videotype,
                                                 FILETYPE filetype)
{
	int number = 0;
	struct file_list *current_file_list = NULL;
	FS_STORAGE_PATH *path_node = NULL;

	fs_storage_foreach_path(number, path_node) {
		if (path_node->filetype == filetype &&
		    path_node->video_type == videotype) {
			current_file_list = path_node->filelist;
			break;
		}
	}
	return current_file_list;
}

FS_STORAGE_PATH *fs_storage_thumb_get_bypath(const char *pathname)
{
	int num;
	FS_STORAGE_PATH *current_path;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(pathname, fs_storage_tbl[num].storage_path) != NULL) {
			current_path = &fs_storage_tbl[num];
			break;
		}
	}

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strcmp(current_path->thumb_path, fs_storage_tbl[num].storage_path) == 0) {
			return &fs_storage_tbl[num];
		}
	}

	return NULL;
}

FS_STORAGE_PATH *fs_storage_get_bythumb(const char *pathname)
{
	int num = -1;
	FS_STORAGE_PATH *current_path = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (*fs_storage_tbl[num].thumb_path == 0)
			continue;
		if (strstr(pathname, fs_storage_tbl[num].thumb_path) != NULL)
			return &fs_storage_tbl[num];
	}

	return NULL;
}

int fs_storage_update_filelist(struct file_node *filenode, char *newpath)
{
	struct file_list *current_file_list = NULL;

	current_file_list = fs_storage_filelist_get_bypath(newpath);
	if (current_file_list)
		filenode->file_list = current_file_list;

	return current_file_list ? 0 : -1;
}

int fs_storage_thumbname_get(const char *fullpath_name, char *thumbname)
{
	FS_STORAGE_PATH *video_path = fs_storage_node_get_bypath(fullpath_name);
	FS_STORAGE_PATH *thumb_path = fs_storage_thumb_get_bypath(fullpath_name);

	char *filename = strrchr(fullpath_name, '/') + 1;
	sprintf(thumbname, "%s/%s", thumb_path->storage_path, filename);
	filename = strstr(thumbname, video_path->storage_format);
	if (filename != NULL) {
		memset(filename, 0, strlen(video_path->storage_format));
		strcat(thumbname, thumb_path->storage_format);
	}

	return 0;
}

int *fs_storage_filetypetbl_get()
{
	return fs_current_filetype_tbl;
}

int fs_storage_filetypenum_get()
{
	return sizeof(fs_current_filetype_tbl) / sizeof(int);
}

void fs_storage_clr_recing_filename(char *path)
{
	int num;
	FS_STORAGE_PATH *current_path = NULL;
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(path, fs_storage_tbl[num].storage_path) != NULL) {
			current_path= &fs_storage_tbl[num];
			break;
		}
	}
	if (current_path == NULL)
		return;
	memset(current_path->filelist->recing_filename, FILENAME_LEN, 0);
}

void fs_storage_set_recing_filename(char *fullpath_name)
{
	int num;
	FS_STORAGE_PATH *current_path = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(fullpath_name, fs_storage_tbl[num].storage_path) != NULL) {
			current_path = &fs_storage_tbl[num];
			break;
		}
	}

	if (current_path == NULL)
		return;

	snprintf(current_path->filelist->recing_filename, FILENAME_LEN,
	         "%s", fullpath_name);
}

char *fs_storage_get_recing_filename(char *path)
{
	int num;
	FS_STORAGE_PATH *current_path = NULL;

	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		if (strstr(path, fs_storage_tbl[num].storage_path) != NULL) {
			current_path = &fs_storage_tbl[num];
			break;
		}
	}

	if (current_path == NULL)
		return NULL;

	return current_path->filelist->recing_filename;
}

int fs_storage_get_medialist_bytype(int videotype,
                                    FILETYPE filetype,
                                    struct file_list *list)
{
	int i = 0, num = 0;
	struct file_list *filelist = NULL;
	struct file_node *current_node = NULL;
	struct file_node *node_new = NULL;

	if (list == NULL)
		return -1;

	filelist = fs_storage_filelist_get_bytype(videotype, filetype);
	if (filelist == NULL)
		return -1;
	if (filelist->filenum <= 0)
		return -1;

	for (current_node = filelist->file_head; current_node != NULL;
	     current_node = current_node->next) {
		if (current_node == NULL)
			break;
		if (strstr(current_node->name, FS_PREPARE_FORMAT) != NULL)
			continue;
		node_new = new_filenode(current_node->name,
		                        filelist,
		                        current_node->filesize);
		if (node_new == NULL)
			break;
		pthread_mutex_lock(&filelistlock);
		if (insert_filenode(list, node_new) == -1)
			free_filenode(node_new);
		else
			i++;
		pthread_mutex_unlock(&filelistlock);
	}
	return i;
}

FS_RET fs_storage_get_medialist_bypath(struct file_list *parent_list,
                                       struct file_list *list)
{
	int i = 0;
	DIR *dp = NULL;
	char *path = NULL;
	char *format = NULL, *format_4k = "_S.mp4";
	struct dirent *dirp = NULL;
	size_t file_size = 0;
	size_t file_space = 0;
	struct file_node *node_new = NULL;
	char fullpath_name[FILENAME_LEN] = {0};

	if (list == NULL || parent_list == NULL)
		return FS_NOK;

	path = parent_list->path;
	format = parent_list->format;

	if ((dp = opendir(path)) == NULL) {
		fs_err_f("opendir");
		return FS_NOK;
	}

	while (((dirp = readdir(dp)) != NULL)) {
		if (strstr(dirp->d_name, format) == NULL)
			continue;
		fs_file_get_fullpath_name(path, dirp->d_name,
                                  fullpath_name, FILENAME_LEN);

		/* Find whether a video file have small video file */
		if (strstr(dirp->d_name, ".mp4") != NULL &&
		    strstr(dirp->d_name, format_4k) == NULL) {
			char file_4k_name[FILENAME_LEN] = {0};
			char tmp[FILENAME_LEN] = {0};
			int size = strlen(dirp->d_name) - 4;    /* not inlude ".mp4" */

			strcpy(tmp, dirp->d_name);
			tmp[size] = '\0';

			sprintf(file_4k_name, "%s%s", tmp, format_4k);
			snprintf(fullpath_name, FILENAME_LEN, "%s/%s", path, file_4k_name);

			/* Not process if exist small vedio file.*/
			if (access(fullpath_name, F_OK) == 0)
				continue;
		}

		node_new = new_filenode(dirp->d_name, parent_list, 0);
		if (node_new == NULL)
			break;

		if (dirp->d_type == 4) {
			insert_foldernode(list, node_new);
		} else {
			if (insert_filenode(list, node_new) == -1)
				free_filenode(node_new);
			i++;
		}
	}
	closedir(dp);
	return FS_OK;
}

void fs_storage_free_mediafilelist(struct file_list **list)
{
	pthread_mutex_lock(&filelistlock);
	fs_manage_free_filelist(list);
	pthread_mutex_unlock(&filelistlock);
	return;
}

struct file_list *fs_manage_getmediafilelist(void)
{
	FS_MODE mode;
	int i = 0, number = 0;
	FS_STORAGE_PATH *path_node = NULL;
	struct file_node *node_new = NULL;
	struct file_list **file_list = &mmedia_file_list;

	if (*file_list != NULL)
		return *file_list;

	mode = fs_get_mode();
	(*file_list) = calloc(1, sizeof(struct file_list));
	if ((*file_list) == NULL) {
		fs_err_f("creat video file list err\n");
		return NULL;
	}
	fs_storage_foreach_path(number, path_node) {
		if (path_node->filetype == THUMBFILE_TYPE
		    || path_node->filetype == GPS_TYPE)
			continue;
		if (mode == FS_CVR)
			fs_storage_get_medialist_bytype(path_node->video_type,
			                                path_node->filetype,
			                                *file_list);
		else
			fs_storage_get_medialist_bypath(path_node->filelist, *file_list);

		fs_log("path = %s, num = %d\n", path_node->storage_path,
		                                (*file_list)->filenum);
	}

	return (*file_list);
}

void fs_manage_free_mediafilelist(void)
{
	struct file_list **file_list = &mmedia_file_list;

	fs_storage_free_mediafilelist(file_list);
}

void fs_storage_rm_collision(char *colli_name, char *video_name, int size)
{
	int ret;
	char *filename;
	char new_colli_name[FILENAME_LEN];
	char new_video_name[FILENAME_LEN];
	struct file_list *video_list = NULL;
	struct file_list *colli_list = NULL;

	colli_list = fs_storage_filelist_get(colli_name);
	video_list = fs_storage_colli_video_get(colli_name);
	if (access(colli_name, 0) == -1)
		return;
	memset(new_colli_name, 0, FILENAME_LEN);
	memset(new_video_name, 0, FILENAME_LEN);
	fs_storage_video_rename(colli_name,
	                        new_video_name,
	                        video_list->path,
	                        video_list->format);
	filename = strrchr(colli_name, '/') + 1;//get filename

	fs_delete_filenode_byname(colli_list, filename);
	rename(colli_name, new_video_name);
	fs_manage_insert_file(new_video_name);
	if (video_name && size > 0)
		snprintf(video_name, size, "%s", new_video_name);

	memset(new_colli_name, 0, FILENAME_LEN);
	memset(new_video_name, 0, FILENAME_LEN);
	file_manage_hide_rename(colli_name, new_colli_name);
	fs_storage_get_early_name(video_list, new_video_name);
	fs_delete_filenode_byname(video_list, strrchr(new_video_name, '/') + 1);
	rename(new_video_name, new_colli_name);
	fs_file_set_hide_byname(new_colli_name);
	fs_manage_insert_file(new_colli_name);
	sync();

	return;
}

void file_manage_collision_rename_cur(char *fullpath_video_name,
                                      char *collision_name,
                                      int buffer_size)
{
	int ret;
	char *filename;
	char fullpath_collision_name[FILENAME_LEN];
	char new_fullpath_video_name[FILENAME_LEN];
	struct file_list *video_file_list = NULL;
	struct file_list *collision_file_list = NULL;

	video_file_list = fs_storage_filelist_get(fullpath_video_name);
	collision_file_list = fs_storage_collisionlist_get(fullpath_video_name);
	if (collision_file_list == NULL)
		return;
	if (access(fullpath_video_name, 0) == -1)
		return;
	memset(new_fullpath_video_name, 0, FILENAME_LEN);
	memset(fullpath_collision_name, 0, FILENAME_LEN);
	file_manage_collision_rename(fullpath_video_name,
	                             fullpath_collision_name,
	                             collision_file_list->path,
	                             collision_file_list->format);
	filename = strrchr(fullpath_video_name, '/') + 1;//get filename

	fs_delete_filenode_byname(video_file_list, filename);
	rename(fullpath_video_name, fullpath_collision_name);
	fs_msg_notify_file(FILE_DEL, fullpath_video_name, 0);
	fs_msg_notify_file(FILE_COLLI, fullpath_collision_name, 0);
	fs_manage_insert_file(fullpath_collision_name);
	if (collision_name && buffer_size > 0)
		snprintf(collision_name, buffer_size, "%s", fullpath_collision_name);

	memset(new_fullpath_video_name, 0, FILENAME_LEN);
	memset(fullpath_collision_name, 0, FILENAME_LEN);
	file_manage_hide_rename(fullpath_video_name, new_fullpath_video_name);
	fs_storage_get_early_name(collision_file_list, fullpath_collision_name);
	fs_delete_filenode_byname(collision_file_list,
	                      strrchr(fullpath_collision_name, '/') + 1);
	rename(fullpath_collision_name, new_fullpath_video_name);
	fs_msg_notify_file(FILE_DEL, fullpath_collision_name, 0);
	fs_file_set_hide_byname(new_fullpath_video_name);
	fs_manage_insert_file(new_fullpath_video_name);
	sync();

	return;
}

void file_manage_collision_rename_all(char *fullpath_video_name,
                                     char *collision_name,
                                     int buffer_size)
{
	FS_STORAGE_PATH *current_path;
	char thumb_filename[FILENAME_LEN] = {0};

	file_manage_collision_rename_cur(fullpath_video_name, collision_name, buffer_size);

	current_path = fs_storage_node_get_bypath(fullpath_video_name);
	if (current_path == NULL || *current_path->thumb_path == 0)
		return;
	current_path = fs_storage_thumb_get_bypath(fullpath_video_name);
	if (current_path == NULL)
		return;

	fs_storage_thumbname_get(fullpath_video_name, thumb_filename);
	file_manage_collision_rename_cur(thumb_filename, NULL, buffer_size);
}


void file_manage_collision_rename_last(char *filename, int number)
{
	int ret = 0;
	struct file_list *list = NULL;
	struct file_node *cur_node = NULL;
	char *cur_filename = NULL;
	char *recing_filename = NULL;
	char fullpath_video_name[FILENAME_LEN] = {0};
	static time_t colli_earlytime = 0;

	list = fs_storage_filelist_get(filename);
	if (list == NULL)
		return;

	for (; ret < number; ret++) {
		cur_node = fs_find_filenode_byname(list, filename_get(filename));
		cur_node = fs_get_pre_filenode(cur_node);
		if (cur_node == NULL)
			break;

		cur_filename = cur_node->name;
		if (strstr(cur_filename, FS_PREPARE_FORMAT) != NULL)
			break;

		recing_filename = fs_storage_get_recing_filename(list->path);
		if (recing_filename != NULL &&
		    strstr(recing_filename, cur_filename) != NULL) {
			ret--;
			continue;
		}

		/* Save collision early time, if file time earlier it, don't save */
		if (colli_earlytime == 0)
			colli_earlytime = cur_node->time;
		else if (colli_earlytime > cur_node->time)
			break;
		colli_earlytime = cur_node->time;

		fs_get_node_fullpath_name(cur_node, fullpath_video_name, FILENAME_LEN);
		file_manage_collision_rename_all(fullpath_video_name, NULL, 0);
	}
}

int fs_storage_video_rename(char *fullpath_name,
                                 char *fullpath_newname,
                                 char *path,
                                 char *format)
{
	char *ret;

	sprintf(fullpath_newname, "%s/%s", path, strrchr(fullpath_name, '/') + 1);
	ret = strstr(fullpath_newname, FS_COLLISION_TAG);
	if (ret != NULL) {
		memset(ret, 0, strlen(FS_COLLISION_TAG) + strlen(format) + 1);
		strcat(fullpath_newname, format);
	}

	return 0;
}

int file_manage_collision_rename(char *fullpath_name,
                                 char *fullpath_newname,
                                 char *path,
                                 char *format)
{
	char *ret;

	sprintf(fullpath_newname, "%s/%s\0", path, strrchr(fullpath_name, '/') + 1);
	ret = strstr(fullpath_newname, format);
	if (ret != NULL) {
		memset(ret, 0, strlen(format));
		strcat(fullpath_newname, FS_COLLISION_TAG);
		strcat(fullpath_newname, format);
	}

	return 0;
}

int file_manage_hide_rename(char *fullpath_name, char *fullpath_newname)
{
	char *ret;
	strcpy(fullpath_newname, fullpath_name);
	sprintf(strrchr(fullpath_newname, '/') + 1, ".%s\0", strrchr(fullpath_name, '/') + 1);
	ret = strrchr(fullpath_newname, '.') - 1;
	if (ret != NULL) {
		memset(ret, 0, strlen(FS_PREPARE_FORMAT) + 1);
		strcat(fullpath_newname, FS_PREPARE_FORMAT);
	}

	return 0;
}

FS_RET fs_storage_hide_file(const char *fullpath_videoname, int type)
{
	char newfullpath_videoname[FILENAME_LEN] = {0};

	if (access(fullpath_videoname, 0) == -1)
		return FS_NOK;
	struct file_list *filelist = fs_storage_filelist_get(fullpath_videoname);

	char *filename = strrchr(fullpath_videoname, '/') + 1;//get filename
	if (*filename == '.')
		return FS_OK;

	file_manage_hide_rename((char *)fullpath_videoname, newfullpath_videoname);
	if (filelist)
		fs_delete_filenode_byname(filelist, filename);
	/* Type=1, is thumb file, don't need remove media_file */
	if (mmedia_file_list && !type)
		fs_delete_filenode_byname(mmedia_file_list, filename);
	rename(fullpath_videoname, newfullpath_videoname);
	fs_file_set_hide_byname(newfullpath_videoname);
	fs_manage_insert_file(newfullpath_videoname);
	return FS_OK;
}

FS_RET fs_storage_remove_file(const char *fullpath_videoname)
{
	const char *filename = NULL;
	char *format_4k = "_S.mp4";
	struct file_list *filelist = NULL;

	filelist = fs_storage_filelist_get(fullpath_videoname);
	filename = filename_get(fullpath_videoname);
	if (filelist)
		fs_delete_filenode_byname(filelist, filename);
	if (mmedia_file_list)
		fs_delete_filenode_byname(mmedia_file_list, filename);

	if (access(fullpath_videoname, 0) == -1)
		return FS_NOK;

	/* Remove 4K files */
	if (strstr(fullpath_videoname, format_4k) != NULL) {
		char *tmp = NULL;
		char newfullpath[FILENAME_LEN] = {0};
		int size = strlen(fullpath_videoname) - strlen(format_4k);

		/* Remove samll 4K file first*/
		remove(fullpath_videoname);

		/* Remove large 4K file later*/
		tmp = (char *)fullpath_videoname;
		tmp[size] = '\0';
		sprintf(newfullpath, "%s%s", tmp, strrchr(format_4k, '.'));
		remove(newfullpath);
	} else {
		remove(fullpath_videoname);
	}

	return FS_OK;
}

int fs_storage_remove(const char *fullpath_videoname, int type)
{
	FS_RET ret;
	FS_STORAGE_PATH *current_path;
	char thumb_filename[FILENAME_LEN] = {0};

	/* If type == 0, hide the file only, type !=0, remove the file. */
	if (!type)
		ret = fs_storage_hide_file(fullpath_videoname, 0);
	else
		ret = fs_storage_remove_file(fullpath_videoname);
	if (ret < 0)
		return -1;

	fs_msg_notify_file(FILE_DEL, (char *)fullpath_videoname, 0);

	current_path = fs_storage_node_get_bypath(fullpath_videoname);
	if (current_path == NULL || *current_path->thumb_path == 0)
		goto RET;

	current_path = fs_storage_thumb_get_bypath(fullpath_videoname);
	if (current_path == NULL)
		goto RET;

	fs_storage_thumbname_get(fullpath_videoname, thumb_filename);

	if (type)
		remove(thumb_filename);

	fs_storage_hide_file(thumb_filename, 1);
RET:
	return 0;
}

int fs_storage_add(const char *fullpath_videoname, size_t filesize)
{
	FS_RET ret;
	FS_STORAGE_PATH *current_path;
	char thumb_filename[FILENAME_LEN] = {0};

	if (access(fullpath_videoname, 0) == -1)
		fs_storage_create_onefile(fullpath_videoname, filesize);
	fs_manage_insert_file(fullpath_videoname);

	current_path = fs_storage_node_get_bypath(fullpath_videoname);
	if (current_path == NULL || *current_path->thumb_path == 0)
		goto RET;

	current_path = fs_storage_thumb_get_bypath(fullpath_videoname);
	if (current_path == NULL)
		goto RET;

	fs_storage_thumbname_get(fullpath_videoname, thumb_filename);
	if (access(thumb_filename, 0) == -1)
		fs_storage_create_onefile(thumb_filename, current_path->filesize);
	fs_manage_insert_file(thumb_filename);
RET:
	sync();
	return 0;
}


#if VIDEO_REPAIR
int fs_video_repair(char *repair_filename, char *video_filename)
{
	int ret;
	int fd_video;
	int fd_repair;
	int64_t file_size;
	off_t filesize = 0;
	int filespace = 0;
	char media_size[5] = {0};
	char *file_buffer;
	int64_t offset = 0;

	fd_video = open(video_filename, O_RDWR, 0666);
	if (fd_video < 0)
		fs_err_f("Open %s fail, errno = %d\n", video_filename, errno);

	fs_manage_get_filesize(fd_video, &filesize, &filespace);
	if (filesize < REPAIR_FILE_MINSIZE) {
		ret = 0;
		fs_err_f("The file is too small\n");
		goto error;
	}

	if (ftruncate(fd_video, filesize) != 0)
		fs_err_f("ftruncate fail errno = %d\n", errno);

	offset = fs_file_media_offset(video_filename);
	lseek(fd_video, offset, SEEK_SET);/* seek to the mediasize */
	read(fd_video, media_size, 4);
	if (media_size[0] == 0 && media_size[1] == 0 && media_size[2] == 0 &&
		media_size[3] == 0) {/* the file will be repair */
		int repair_filesize = 0;
		char *repair_buffer;
		repair_filesize = fs_userdata_get_filesize(repair_filename);
		if (repair_filesize <= 0)
			return -1;
		repair_buffer = calloc(repair_filesize + 1, 1);
		if (repair_buffer == NULL)
			return -1;
		fs_userdata_read(repair_filename, repair_buffer, repair_filesize);

		file_size = lseek(fd_video, 0, SEEK_END);
		file_size -= offset;
		media_size[0] = (char)(file_size >> 24);
		media_size[1] = (char)(file_size >> 16);
		media_size[2] = (char)(file_size >> 8);
		media_size[3] = (char)(file_size);
		fs_err_f("file size = %lld media isze=%2x%2x%2x%2x\n", file_size,
		          media_size[0], media_size[1], media_size[2], media_size[3]);
		lseek(fd_video, offset, SEEK_SET);
		ret = write(fd_video, media_size, 4);

		lseek(fd_video, 0, SEEK_END);
		write(fd_video, repair_buffer, repair_filesize);
		free(repair_buffer);

		fs_log("vedio repair complete\n");
		ret = 0;
	} else
		ret = -1;

error:
	fsync(fd_video);
	close(fd_video);
	return ret;
}

void fs_manage_video_repair(VIDEO_REPAIR_CALLBACK *video_repair_callback)
{
	int ret = 0;
	int filenum = 0;
	char video_filename[FILENAME_LEN];
	char *repair_filename = NULL;
	char *current_filename = NULL;
	ret = fs_userdata_init();
	if (ret < 1) {
		printf("fs_userdata get file NULL\n");
		return;
	}

	while ((repair_filename = (char *)fs_userdata_next_filename(repair_filename))
	        != NULL) {
		printf("Storaged file name = %s\n", repair_filename);
		filenum++;
		if (filenum > ret)
			break;
		if (check_filenodebyname(video_file_list, repair_filename,
		    REPAIR_CHECK_NUMBER) != NULL) {
			sprintf(video_filename, "%s/%s\0", VIDEOFILE_PATH, repair_filename);
			ret = fs_video_repair(repair_filename, video_filename);
			printf("Repairing file name = %s\n", repair_filename);
			if (ret == 0) {//remove the repaired head
				fs_userdata_deletefile(repair_filename);
				if (video_repair_callback->repair_callback_func) {
					video_repair_callback->repair_callback_func(
						video_repair_callback->repair_callback_param, FS_REPAIR);
					video_repair_callback->repair_callback_func = NULL;
				}
			}
		}
	}
	fs_userdata_deinit();

	return;
}
#endif

int fs_storage_file_manage(int delay_time)
{
	int i = 0;
	char remove_name[FILENAME_LEN];
	struct timeval t1, t2, t3;
	if (delay_time)
		sleep(delay_time);
	gettimeofday(&t1, NULL);
	fs_sdcard_check_capacity(&tf_free_size, &tf_total_size, NULL);
	if (tf_free_size < fs_storage_get_free_cap(tf_total_size)) {
		/* default filelist is tlb[0] list */
		struct file_list *video_file_list = fs_storage_tbl[0].filelist;
		while (video_file_list && tf_free_size < fs_storage_get_free_cap(tf_total_size)) {
			if (video_file_list->file_head) {
				struct file_node *node_tmp;
				sprintf(remove_name, "%s/%s\0", video_file_list->path,
				        video_file_list->file_head->name);
				remove(remove_name);

				pthread_mutex_lock(&filelistlock);
				node_tmp = video_file_list->file_head;
				if (node_tmp == NULL) {/* the file have been remove all */
					pthread_mutex_unlock(&filelistlock);
					break;
				}
				video_file_list->file_head = node_tmp->next;
				if (video_file_list->file_head)
					video_file_list->file_head->pre = 0;
				else
					video_file_list->file_tail = 0;

				free_filenode(node_tmp);
				pthread_mutex_unlock(&filelistlock);
				i++;
			} else {
				break;
			}
			fs_log("remove file name = %s\n", remove_name);
			fs_sdcard_check_capacity(&tf_free_size, &tf_total_size, NULL);
		}
	}
	if (i) {
		gettimeofday(&t2, NULL);
		//fs_manage_runapp("sync");
		gettimeofday(&t3, NULL);
		fs_log("delete %d file sync time = %d us All us = %d\n", i,
		       (t3.tv_sec - t2.tv_sec) * 1000000 + (t3.tv_usec - t2.tv_usec),
		       (t3.tv_sec - t1.tv_sec) * 1000000 + (t3.tv_usec - t1.tv_usec));
	}
	return tf_free_size;
}

void *fs_storage_pthread(void *arg)
{
	int ret = 0;
	int tf_free = 0;
	FILEOPERA_INFO ipc_msg;
	VIDEO_REPAIR_CALLBACK *video_repair_callback = (VIDEO_REPAIR_CALLBACK *)arg;

	prctl(PR_SET_NAME, "fs_storage", 0, 0, 0);

#if VIDEO_REPAIR
	fs_manage_video_repair(video_repair_callback);
#endif

	while (1) {
		ret = fileopera_ipcmsg_rec(m_fileopera_msgid, &ipc_msg);
		if (ret < 0) {
			fs_log("fail cache_ipcmsg_rec m_fileopera_msgid = %d | errno=%d [%s]\n",
			       m_fileopera_msgid, errno, strerror(errno));
			continue;
		}

		if (ipc_msg.command == FS_CMD_OPEN) {
			fs_storage_file_prepare(ipc_msg.filename);
			free(ipc_msg.filename);
		}
		else if (ipc_msg.command == FS_CMD_CLOSE)
			fs_file_close(ipc_msg.real_fd);
		else if (ipc_msg.command == FS_CMD_WRITE_SIZE) {
			fs_cache_write_mediasize(ipc_msg.filename,
			                         ipc_msg.mediasize,
			                         ipc_msg.real_fd);
			fs_msg_notify_file(FILE_END, ipc_msg.filename, 0);
			free(ipc_msg.filename);
		} else if (ipc_msg.command == FS_CMD_RENAME_CUR) {
			fs_log_f("Rename cur, filename = %s\n", ipc_msg.filename);
			file_manage_collision_rename_all(ipc_msg.filename, NULL, 0);
			free(ipc_msg.filename);
		} else if (ipc_msg.command == FS_CMD_RENAME_PRE) {
			/* The pre storage filename in the file size */
			fs_log_f("Rename pre, filename = %s\n", ipc_msg.filename);
			file_manage_collision_rename_last(ipc_msg.filename,
			                                  ipc_msg.file_size);
			free(ipc_msg.filename);
		} else if (ipc_msg.command == FS_CMD_EXIT)
			break;
		else
			fs_log("The commnd = %d, unknow\n", ipc_msg.command);
	}
	//dump_filelist(video_file_list);
out:
	pthread_exit(NULL);
}

FS_RET fs_storage_create_onefile(char *filename, int filesize)
{
	int fd = fs_file_open_normal(filename, O_CREAT | O_RDWR | O_NOATIME |
	                             O_TRUNC, S_IRWXU);
	if (fd < 0) {
		fs_err("open fail, filename = %s, errno = %d\n", filename, errno);
		return FS_NOK;
	}

	if (fallocate(fd, 0x01, 0, filesize) != 0) {
		fs_err("fallocate fail fd=%d, size=%d errno=%d\n", fd, filesize, errno);
		return FS_NOK;
	}
	fs_file_set_hide(fd);
	fs_file_close_normal(fd);

	return FS_OK;
}

FS_RET fs_storage_create_mediafile(int filenum,
                                   int insert,
                                   FS_STORAGE_PATH *storage_path)
{
	int ret = 0;
	int number = 0;
	int cur_number = 0;
	char *path = NULL;
	int filesize = 0;
	char filename[FILENAME_LEN] = {0};
	time_t time = 0;

	path = storage_path->storage_path;
	filesize = storage_path->filesize;
	if (filenum <= 0)
		return FS_NOK;

	fs_log("Create path=%s, filenum=%d\n", path, filenum);
	fs_file_create_path(path, storage_path->path_attri);
	string_to_time("19700101_000000", &time);
	cur_number = filenum;
	for (number = 0; number < cur_number; number++) {
		time_to_string(filename, FILENAME_LEN,
		               time + number, path, FS_PREPARE_FORMAT);
		if (insert && access(filename, 0) == 0) {
			cur_number++;
			continue;
		}
		ret = fs_storage_create_onefile(filename, filesize);
		if (ret != FS_OK)
			break;

		if (insert)
			fs_manage_insert_file(filename);
	}
	return ret;
}

FS_RET fs_sdcard_create_newfile()
{
	int num;
	int filesize = 0;
	float percent = 0;
	int filenum = 0;
	int align = 0;
	int ret = FS_NOK;
	FS_MODE mode;
	struct timeval t1, t2, t3, t4;
	long long mtf_free_size;
	long long mtf_total_size;
	int delay1, delay2, delay3, all_delay;
	FS_STORAGE_PATH *current_node = NULL;

	mode = fs_get_mode();
	if (mode != FS_CVR)
		return FS_OK;

	fs_sdcard_check_capacity(&mtf_free_size, &mtf_total_size, NULL);

	gettimeofday(&t1, NULL);
	for (num = 0; num < STORAGE_PATH_NUM; num++) {
		current_node = &fs_storage_tbl[num];
		if (fs_storage_tbl[num].filetype == THUMBFILE_TYPE) {
			current_node = fs_storage_get_bythumb(fs_storage_tbl[num].storage_path);
			if (current_node == NULL)
				fs_assert(0);
		}
		percent = current_node->storage_percent;
		if (fs_storage_tbl[num].filetype == THUMBFILE_TYPE
		    && current_node->filetype == VIDEOFILE_TYPE) {
			align = current_node->filesize_align;
			filenum = mtf_total_size * percent * FS_SZ_1M / 100 / align;
		} else {
			filesize = current_node->filesize;
			filenum = mtf_total_size * percent * FS_SZ_1M / 100 / filesize;
		}
		ret = fs_storage_create_mediafile(filenum, 0, &fs_storage_tbl[num]);
		if (ret != FS_OK)
			goto out;
	}
	sync();
	gettimeofday(&t2, NULL);

	delay1 = (t2.tv_sec - t1.tv_sec) * 1000 + (t2.tv_usec - t1.tv_usec) / 1000;
	fs_log("create all file use %d mS\n", delay1);
	return ret;
out:
	fs_err("create_newfile fail errno = %d\n", errno);
	return ret;
}

void fs_storage_cap_adj();
int fs_sdcard_set_newfile()
{
	int fd = 0;
	fs_storage_cap_adj();
	fd = open("/tmp/fsconf.conf",  O_CREAT | O_RDWR, 0666);
	write(fd, fs_storage_tbl, sizeof(fs_storage_tbl));
	close(fd);
	return 0;
}

FS_RET fs_storage_file_prepare(const char *filename)
{
	long long free_space = 0;
	long long use_space = 0;
	long long all_space = 0;
	long long sd_free_space = 0;
	long long sd_total_space = 0;
	int headsize = 0;
	int dstsize = 0;
	int alignsize = 0;
	FS_MODE mode;
	time_t head_time = 0;
	char *path = NULL;
	char new_name[FILENAME_LEN] = {0};
	struct file_node *filenode = NULL;
	struct file_list *filelist = NULL;

	mode = fs_get_mode();
	if (mode != FS_CVR)
		return FS_OK;

	filelist = fs_storage_filelist_get(filename);
	if (filelist == NULL)
		return FS_NOK;
	alignsize = filelist->filesize_align;
	dstsize = filelist->filesize;
	while(1) {
		filenode = fs_get_head_filenode(filelist);
		if (filenode == NULL)
			return FS_NOK;
		headsize = fs_storage_filesize_get_bynode(filenode);
		if (headsize == filelist->filesize)
			break;
		free_space = filelist->free_space;
		all_space = filelist->all_space;
		path = filelist->path;
		fs_log("Update space,path =%s free = %lld, all space = %lld\n", path,
		                                                 free_space, all_space);
		if (dstsize < free_space) {
			fs_sdcard_check_capacity(&sd_free_space, &sd_total_space, NULL);
			sd_free_space += dstsize / FS_SZ_1M;
			if (sd_free_space < fs_storage_get_free_cap(sd_total_space)) {
				fs_err_f("Ff_total leak of size = %lld\n", sd_free_space);
				sprintf(new_name, "%s/%s\0", path, filenode->name);
				fs_storage_remove(new_name, 1);
				continue;
			}
			head_time = filenode->time;
			time_to_string(new_name, FILENAME_LEN,
			               head_time - 1, path, FS_PREPARE_FORMAT);
			fs_storage_create_onefile(new_name, dstsize);
			fs_manage_insert_file(new_name);
			fs_log("Add filename = %s, dstsize =%d\n", new_name, dstsize);
			break;
		} else {
			sprintf(new_name, "%s/%s\0", path, filenode->name);
			fs_storage_remove(new_name, 1);
			fs_log("Remove filename = %s, dstsize =%d\n", new_name, headsize);
		}
	}

	return FS_OK;
}

#ifdef ENABLE_CAP_ADJ
void fs_storage_cap_adj_path(int filetype, float total)
{
	int number = 0;
	float percent = 0;
	int filesize = 0;
	FS_STORAGE_PATH *path_node = NULL;

	switch (filetype) {
	case VIDEOFILE_TYPE:
		percent = (float)FS_CAP_READJ_VIDEO;
		break;
	case PICFILE_TYPE:
		percent = (float)FS_CAP_READJ_PHOTO;
		break;
	case LOCKFILE_TYPE:
		percent = (float)FS_CAP_READJ_LOCK;
		break;
	default:
		return;
	}

	if (total)
		percent = percent * total;
	/* Calculate filesize total. */
	fs_storage_foreach_path(number, path_node) {
		if (path_node->filetype == filetype)
			filesize += path_node->filesize;
	}
	/* Readjust filesize. */
	fs_storage_foreach_path(number, path_node) {
		if (path_node->filetype == filetype && path_node->filesize > 0)
			path_node->storage_percent = (float)path_node->filesize /
			                             (float)filesize * percent;
	}
}
#endif

float fs_storage_get_cap()
{
	int number = 0;
	float total = 0;
	FS_STORAGE_PATH *path_node = NULL;

	fs_storage_foreach_path(number, path_node) {
		if (path_node->storage_percent)
			total += path_node->storage_percent;
	}
	/* Add 0.5, just for some thumbnail and some extra space */
	return total + 1;
}

inline long long fs_storage_get_free_cap(long long total)
{
	return total * fs_free_per / 100;
}

inline long long fs_storage_get_use_cap(long long total)
{
	return total * fs_use_per / 100;
}

inline void fs_storage_set_free_per(float free_per)
{
	fs_free_per = free_per;
}

inline int fs_storage_set_use_per(float use_per)
{
	fs_use_per = use_per;
}

void fs_storage_cap_adj()
{
	int total = 0;
	float total_add = 0;

	fs_sdcard_check_capacity(&tf_free_size, &tf_total_size, NULL);
#ifdef FS_CAP_CHANGE_PER
	total = FS_CAP_READJ_VIDEO + FS_CAP_READJ_PHOTO + FS_CAP_READJ_LOCK;
	if (tf_total_size > FS_CAP_PER_FROM) {
		total_add = total + tf_total_size /  FS_CAP_PER_FROM;
		if (total_add > 98)
			total_add = 98;
		total_add = total_add / (float)total;
	}
#endif
#ifdef ENABLE_CAP_ADJ
	fs_storage_cap_adj_path(VIDEOFILE_TYPE, total_add);
	fs_storage_cap_adj_path(PICFILE_TYPE, total_add);
	fs_storage_cap_adj_path(LOCKFILE_TYPE, total_add);
#endif
	total_add = fs_storage_get_cap();
	fs_storage_set_use_per(total_add);
	fs_storage_set_free_per(100 - total_add);
}

int fs_storage_init(int (*callback)(void *, int), void *arg)
{
	FS_RET ret;
	int tf_free;

	if (fs_manage_sd_exist(NULL) == 0) {
		fs_err_f("No SD Card\n");
		ret = FS_INITFAIL;
		goto RET3;
	}

	ret = fs_storage_check_formated();
	if (ret != FS_OK) {
		ret = FS_UNFORMAT;
		goto RET3;
	}

	if (pthread_mutex_init(&filelistlock, NULL) != 0) {
		fs_err_f("filelistlock init fail \n");
		ret = FS_INITFAIL;
		goto RET3;
	}

	VIDEO_REPAIR_CALLBACK *video_repair_callback = &mvideo_repair_callback;
	if (callback != NULL && arg != NULL) {
		video_repair_callback->repair_callback_func = callback;
		video_repair_callback->repair_callback_param = arg;
	}
	fs_storage_cap_adj();
	ret = fs_storage_create_list();
	if (ret < 0) {
		fs_err_f("fs_create_list fail\n");
		goto RET2;
	}

	tf_free = fs_storage_file_manage(0);
	if (tf_free < fs_storage_get_free_cap(tf_total_size)) {
		fs_err_f("tf_free_size=%d MB, too small to open file\n", tf_free);
		ret = FS_UNFORMAT;
		goto RET2;
	}

	ret = fs_storage_check_file();
	if (ret == FS_ERR_UNKNOW) {
		ret = FS_UNFORMAT;
		goto RET2;
	}

	ret = fileopera_ipcmsg_init(RK_FILEOPERA_MSGKEY, &m_fileopera_msgid);
	if (ret != FS_OK) {
		fs_err_f("fileopera_ipcmsg_init fail \n");
		ret = FS_INITFAIL;
		goto RET2;
	}

	if (fs_manage_tid == 0) {
		if (pthread_create(&fs_manage_tid, NULL, fs_storage_pthread,
		                   video_repair_callback)) {
			fs_err_f("pthread_create err\n");
			ret = FS_INITFAIL;
			goto RET1;
		}
	}

	ret = fs_cache_init();
	if (ret != FS_OK) {
		ret = FS_INITFAIL;
		goto RET0;
	}

	return 0;

RET0:
	/* send open msg for capital manage */
	fileopera_ipcmsg_send_cmd(0, NULL, NULL, FS_CMD_EXIT);
	pthread_join(fs_manage_tid, NULL);
	fs_manage_tid = 0;
RET1:
	fileopera_ipcmsg_deinit(RK_FILEOPERA_MSGKEY, &m_fileopera_msgid);
RET2:
	pthread_mutex_destroy(&filelistlock);
RET3:
	return ret;
}

int fs_storage_deinit(void)
{
	fs_cache_deinit();
	if (fs_manage_tid) {
		fileopera_ipcmsg_send_cmd(0, NULL, NULL, FS_CMD_EXIT); //send open msg for capital manage
		pthread_join(fs_manage_tid, NULL);
		fs_manage_tid = 0;
	}
	fileopera_ipcmsg_deinit(RK_FILEOPERA_MSGKEY, &m_fileopera_msgid);
	fs_storage_free_list();
	pthread_mutex_destroy(&filelistlock);
	fs_manage_sdcard_unmount();
}
