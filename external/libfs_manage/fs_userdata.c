/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

#include "fs_userdata.h"
#include "fs_list.h"
#include "fs_file.h"

#define USERDATA_FLAG (0x56531234)
#define USERDATA_SIZE (512 * 1024)
#define BLOCK_SIZE (64 * 1024)
#define FILE_NUM (2)
#define FILE_MAX_SIZE (USERDATA_SIZE / FILE_NUM)
#define FILE_DATA_OFFSET 4
typedef struct _USERDATA_FILE {
	int userdata_flag;
	int userdata_filesize;
	int userdata_head;
	int usetdata_trail;
	char userdata_filename[FILENAME_LEN];
} USERDATA_FILE;

typedef struct _USERDATA_STORAGE {
	struct _USERDATA_STORAGE *pre;
	struct _USERDATA_STORAGE *next;
	USERDATA_FILE *file_node;
} USERDATA_STORAGE;

USERDATA_STORAGE *g_userdata_file = NULL;
int g_storage_file_num = 0;

int fs_userdata_align_64k(int size)
{
	int size_64k;
	int align_64k = BLOCK_SIZE;

	size_64k = (size / align_64k) * align_64k;

	return size_64k;
}

int fs_userdata_get_offset(USERDATA_STORAGE *userdata_storage_list,
                           USERDATA_STORAGE **userdata_node)
{
	int file_num = 0;
	int offset = 0;
	USERDATA_STORAGE *early_userdata = NULL;
	USERDATA_STORAGE *userdata_storage_current;

	if (userdata_storage_list != NULL) {
		for (userdata_storage_current = userdata_storage_list;
		     userdata_storage_current->next != NULL;
		     userdata_storage_current = userdata_storage_current->next) {
			file_num++;
		}
		if (userdata_storage_current->next == NULL)
			file_num++;
	}
	if (file_num >= FILE_NUM) {
		for (userdata_storage_current = userdata_storage_list;
			 userdata_storage_current->next != NULL;
			 userdata_storage_current = userdata_storage_current->next) {
			if (early_userdata == NULL)
				early_userdata = userdata_storage_current;
			else if (strcmp(early_userdata->file_node->userdata_filename,
                    userdata_storage_current->file_node->userdata_filename) > 0)
				early_userdata = userdata_storage_current;
		}
		if (userdata_storage_current->next == NULL) {
			if (early_userdata == NULL)
				early_userdata = userdata_storage_current;
			else if (strcmp(early_userdata->file_node->userdata_filename,
                    userdata_storage_current->file_node->userdata_filename) > 0)
				early_userdata = userdata_storage_current;
		}
		offset = early_userdata->file_node->userdata_head +
		         (sizeof(USERDATA_FILE) + FILE_DATA_OFFSET);
		*userdata_node = early_userdata;
	} else {
		offset = file_num * FILE_MAX_SIZE;
	}

	return offset;
}

USERDATA_STORAGE *fs_userdata_get_byfilename(
                                        USERDATA_STORAGE *userdata_storage_list,
                                        char *filename)
{
	USERDATA_STORAGE *userdata_storage_current;

	if (userdata_storage_list == NULL)
		return NULL;

	if (filename == NULL)
		return NULL;

	for (userdata_storage_current = userdata_storage_list;
		 userdata_storage_current->next != NULL;
		 userdata_storage_current = userdata_storage_current->next) {
		if (strcmp(filename, userdata_storage_current->file_node->userdata_filename) == 0)
			break;
	}
	if (userdata_storage_current->next == NULL) {
		if (strcmp(filename, userdata_storage_current->file_node->userdata_filename) != 0)
			return NULL;
	}

	return userdata_storage_current;
}


int fs_userdata_insert(USERDATA_STORAGE **userdata_storage_list,
                       USERDATA_STORAGE *userdata_storage_node)
{
	USERDATA_STORAGE *userdata_storage_current;

	if (userdata_storage_node == NULL)
		return -1;

	if (*userdata_storage_list == NULL) {
		*userdata_storage_list = userdata_storage_node;
		goto out;
	}

	for (userdata_storage_current = *userdata_storage_list;
		 userdata_storage_current->next != NULL;
		 userdata_storage_current = userdata_storage_current->next);

	cg_list_add((CgList *)(userdata_storage_current),
	            (CgList *)userdata_storage_node);

out:
	return 0;
}

void fs_userdata_free(USERDATA_STORAGE *userdata_storage)
{
	if (userdata_storage == NULL)
		return;

	if (userdata_storage->file_node != NULL)
		free(userdata_storage->file_node);
	free(userdata_storage);

	return;
}

int fs_userdata_delete(USERDATA_STORAGE **userdata_storage_list,
                        char *filename)
{
	USERDATA_STORAGE *userdata_storage;

	userdata_storage = fs_userdata_get_byfilename(*userdata_storage_list, filename);
	if (userdata_storage == NULL)
		return -1;

	if (userdata_storage->next == NULL && userdata_storage->pre == NULL) {
		fs_userdata_free(userdata_storage);
		*userdata_storage_list = NULL;
	} else if (userdata_storage->next == NULL) {
		userdata_storage->pre->next = NULL;
		fs_userdata_free(userdata_storage);
	} else if (userdata_storage->pre == NULL) {
		*userdata_storage_list = userdata_storage->next;
		userdata_storage->next->pre = NULL;
		fs_userdata_free(userdata_storage);
	} else {
		cg_list_remove((CgList *)userdata_storage);
		fs_userdata_free(userdata_storage);
	}

	return 0;
}

int fs_userdata_delete_node(USERDATA_STORAGE **userdata_storage_list,
                            USERDATA_STORAGE *userdata_storage)
{
	if (userdata_storage == NULL)
		return -1;

	if (userdata_storage->next == NULL && userdata_storage->pre == NULL) {
		fs_userdata_free(userdata_storage);
		*userdata_storage_list = NULL;
	} else if (userdata_storage->next == NULL) {
		userdata_storage->pre->next = NULL;
		fs_userdata_free(userdata_storage);
	} else if (userdata_storage->pre == NULL) {
		*userdata_storage_list = userdata_storage->next;
		userdata_storage->next->pre = NULL;
		fs_userdata_free(userdata_storage);
	} else {
		cg_list_remove((CgList *)userdata_storage);
		fs_userdata_free(userdata_storage);
	}

	return 0;
}


int fs_userdata_delete_list(USERDATA_STORAGE **userdata_storage_list)
{
	USERDATA_STORAGE *userdata_storage_current;
	USERDATA_STORAGE *userdata_storage_del;

	if (*userdata_storage_list == NULL)
		return -1;

	userdata_storage_del = *userdata_storage_list;
	for (userdata_storage_current = (*userdata_storage_list)->next ;
	     userdata_storage_current != NULL;
	     userdata_storage_current = userdata_storage_current->next) {
		fs_userdata_free(userdata_storage_del);
		userdata_storage_del = userdata_storage_current;
	}
	fs_userdata_free(userdata_storage_del);

	*userdata_storage_list = NULL;
	return 0;
}

USERDATA_STORAGE *fs_userdata_create()
{
	USERDATA_FILE *userdata_file;
	USERDATA_STORAGE *userdata_storage;

	userdata_storage = (USERDATA_STORAGE *)calloc(1, sizeof(USERDATA_STORAGE));
	if (userdata_storage == NULL) {
		printf("userdata_storage fail\n");
		return NULL;
	}

	userdata_file = (USERDATA_FILE *)calloc(1, sizeof(USERDATA_FILE));
	if (userdata_file == NULL) {
		printf("userdata_file fail\n");
		free(userdata_storage);
		return NULL;
	}

	userdata_storage->file_node = userdata_file;

	return userdata_storage;
}

int fs_userdata_create_list(USERDATA_STORAGE **userdata_file_list)
{
	int fd;
	int ret;
	int offset;
	int file_num = 0;
	char buffer[BLOCK_SIZE];
	USERDATA_FILE *userdata_file;
	USERDATA_STORAGE *userdata_storage;

	fd = open(USERDATA_DEVICE, O_RDWR, 0666);
	if (fd < 0) {
		printf("open fail, errno = %d\n", errno);
		return -1;
	}

	for (offset = 0; offset < USERDATA_SIZE; offset += FILE_MAX_SIZE) {
		lseek(fd, offset, SEEK_SET);
		ret = read(fd, buffer, BLOCK_SIZE);
		if (ret != BLOCK_SIZE) {
			printf("read fail, ret = %d, errno = %d\n", ret, errno);
			continue;
		}
		userdata_file = (USERDATA_FILE *)buffer;
		if (userdata_file->userdata_flag != USERDATA_FLAG)
			continue;

		userdata_storage = fs_userdata_create();
		if (userdata_storage == NULL) {
			printf("userdata_storage create fail, errno = %d\n", errno);
			continue;
		}
		memcpy(userdata_storage->file_node, buffer, sizeof(USERDATA_FILE));
		fs_userdata_insert(userdata_file_list, userdata_storage);
		file_num++;
	}
	close(fd);
	if (file_num)
		printf("Storage file number %d\n", file_num);
	return file_num;
}

int printf_data(char *buffer, int file_size)
{
	int i;

	printf("lhp printf data begin\n");
	for (i = 0; i < file_size; i++) {
		printf("%2x ", *(buffer + i));
		if (i % 32 == 0)
			printf("\n");
		if (i % 1024 == 0)
			printf("lhp buffer len = %d\n", i);
	}
}

int fs_userdata_storage_read(int fd, char *buffer, int head, int filesize)
{
	int sum;
	int ret;
	int file_offset;
	int buffer_offset;
	int buffer_size;
	int real_len;
	char *buffer_head;
	char file_buffer[BLOCK_SIZE];

	file_offset = fs_userdata_align_64k(head);
	real_len = file_offset + filesize;

	for (buffer_offset = 0; file_offset < real_len;
	     file_offset += BLOCK_SIZE, buffer_offset += buffer_size) {
		lseek(fd, file_offset, SEEK_SET);
		ret = read(fd, file_buffer, BLOCK_SIZE);
		if (ret < BLOCK_SIZE)
			printf("%s read error, errno = %d\n", __func__, errno);

		buffer_size = BLOCK_SIZE;
		buffer_head = file_buffer;
		/* head offset */
		if (file_offset < head) {
			buffer_head = buffer_head + (head - file_offset);
			buffer_size = buffer_size - (head - file_offset);
		}
		/* trail offset */
		if (file_offset + BLOCK_SIZE > real_len)
			buffer_size = buffer_size - (file_offset + BLOCK_SIZE - filesize - head);
		memcpy(buffer + buffer_offset, buffer_head, buffer_size);
	}

	return filesize;
}

int fs_userdata_storage_write(int fd, char *data_buffer, int head, int filesize,
                              char *filename, USERDATA_STORAGE **userdata_file_list)
{
	int sum;
	int ret;
	int file_offset;
	int buffer_size;
	int buffer_head;
	int real_len;
	int data_offset;
	int buffer_offset;
	char write_buffer[BLOCK_SIZE];
	USERDATA_FILE userdata_file;
	USERDATA_STORAGE *userdata_storage;

	memset(&userdata_file, 0, sizeof(USERDATA_FILE));
	file_offset = fs_userdata_align_64k(head);
	userdata_file.userdata_flag = USERDATA_FLAG;
	userdata_file.userdata_filesize = filesize;
	userdata_file.userdata_head = file_offset + sizeof(USERDATA_FILE) + FILE_DATA_OFFSET;
	userdata_file.usetdata_trail = userdata_file.userdata_filesize +
	                               userdata_file.userdata_head;
	strcpy(userdata_file.userdata_filename, filename);
	memcpy(write_buffer, &userdata_file, sizeof(USERDATA_FILE));

	data_offset = 0;
	real_len = userdata_file.userdata_head + filesize;

	buffer_offset = sizeof(USERDATA_FILE) + FILE_DATA_OFFSET;
	buffer_size = BLOCK_SIZE - (sizeof(USERDATA_FILE) + FILE_DATA_OFFSET);
	for (; file_offset < filesize + head; file_offset += BLOCK_SIZE) {
		lseek(fd, file_offset, SEEK_SET);
		if (file_offset + BLOCK_SIZE > real_len) { /* trail offset */
			buffer_size = buffer_size - (file_offset + BLOCK_SIZE - filesize - head);
		}

		memcpy(buffer_offset + write_buffer, data_offset + data_buffer, buffer_size);
		ret = write(fd, write_buffer, BLOCK_SIZE);
		if (ret < BLOCK_SIZE)
			printf("%s write error, errno = %d, ret = %d\n", __func__, errno, ret);
		data_offset += buffer_size;
		buffer_offset = 0;
		buffer_size = BLOCK_SIZE;
	}

	userdata_storage = fs_userdata_create();
	if (userdata_storage == NULL) {
		printf("userdata_storage create fail, errno = %d\n", errno);
		return filesize;
	}
	memcpy(userdata_storage->file_node, &userdata_file, sizeof(USERDATA_FILE));
	fs_userdata_insert(userdata_file_list, userdata_storage);

	return filesize;
}

char *fs_userdata_next_filename(char *filename)
{
	char *newfilename = NULL;
	USERDATA_STORAGE *userdata_storage_current;
	USERDATA_STORAGE *userdata_storage_list = g_userdata_file;

	if (userdata_storage_list == NULL)
		return NULL;

	if (filename == NULL)
		return userdata_storage_list->file_node->userdata_filename;

	for (userdata_storage_current = userdata_storage_list;
		 userdata_storage_current->next != NULL; \
		 userdata_storage_current = userdata_storage_current->next) {
		/* the filename == inputfilname, and the next != NULL */
		if (strcmp(filename, userdata_storage_current->file_node->userdata_filename) != 0)
			return userdata_storage_current->next->file_node->userdata_filename;
	}
	/* the next == NULL, this is the last one */
	if (userdata_storage_current->next == NULL) {
		if (strcmp(filename, userdata_storage_current->file_node->userdata_filename) != 0)
			return userdata_storage_current->file_node->userdata_filename;
	}

	return NULL;
}

/* 1 exist 0 none */
int fs_userdata_exist()
{
	return !access(USERDATA_DEVICE, F_OK);
}

int fs_userdata_get_filesize(char *filename)
{
	int filesize;
	USERDATA_STORAGE *userdata_storage;

	userdata_storage = fs_userdata_get_byfilename(g_userdata_file, filename);
	filesize = (userdata_storage == NULL) ? 0 :
	           userdata_storage->file_node->userdata_filesize;
	return filesize;
}

int fs_userdata_deletefile(char *filename)
{
	int fd;
	int offset;
	char filenull[sizeof(USERDATA_FILE)] = {0};
	USERDATA_STORAGE *userdata_storage = NULL;

	userdata_storage = fs_userdata_get_byfilename(g_userdata_file, filename);
	if (userdata_storage == NULL)
		return -1;

	fd = open(USERDATA_DEVICE, O_RDWR, 0666);
	if (fd < 0) {
		printf("open fail, errno = %d\n", errno);
		return -1;
	}

	lseek(fd, fs_userdata_align_64k(userdata_storage->file_node->userdata_head), SEEK_SET);
	write(fd, filenull, sizeof(USERDATA_FILE));
	fsync(fd);
	close(fd);

	fs_userdata_delete_node(&g_userdata_file, userdata_storage);

	return 0;
}


int fs_userdata_write(char *filename, char *buffer, int buffer_size)
{
	int fd;
	int offset;
	USERDATA_STORAGE *storage_node = NULL;

	fd = open(USERDATA_DEVICE, O_RDWR, 0666);
	if (fd < 0) {
		printf("open fail, errno = %d\n", errno);
		return -1;
	}
	offset = fs_userdata_get_offset(g_userdata_file, &storage_node);
	if (storage_node)
		fs_userdata_delete_node(&g_userdata_file, storage_node); 
	fs_userdata_storage_write(fd, buffer, offset, buffer_size, filename, &g_userdata_file);

	fsync(fd);
	close(fd);

	return buffer_size;
}

int fs_userdata_read(char *filename, char *buffer, int buffer_size)
{
	int fd;
	int ret;
	USERDATA_STORAGE *userdata_storage;

	userdata_storage = fs_userdata_get_byfilename(g_userdata_file, filename);
	if (userdata_storage == NULL)
		return -1;
	if (buffer_size < userdata_storage->file_node->userdata_filesize)
		return -1;

	fd = open(USERDATA_DEVICE, O_RDWR, 0666);
	if (fd < 0) {
		printf("open fail, errno = %d\n", errno);
		return -1;
	}

	fs_userdata_storage_read(fd, buffer,
	                         userdata_storage->file_node->userdata_head,
	                         userdata_storage->file_node->userdata_filesize);
	close(fd);

	return buffer_size;
}

/*
 * return value
 * value = -1 : userdata noexit
 * value = 0: userdata_file = NULL
 * value = 1:normal
 */
int fs_userdata_init()
{
	int filenum = 0;

	if (fs_userdata_exist() == 0)
		return -1;
	if (g_userdata_file != NULL)
		fs_userdata_delete_list(&g_userdata_file);

	filenum = fs_userdata_create_list(&g_userdata_file);
	if (g_userdata_file == NULL)
		return 0;

	return filenum;
}

int fs_userdata_deinit()
{
	if (fs_userdata_exist() == 0)
		return -1;

	fs_userdata_delete_list(&g_userdata_file);
	return 0;
}
