/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fs_list.h"
#include <stdlib.h>

void cg_free(void *str)
{
	if (NULL != str)
		free(str);
}

void cg_list_header_init(CgList *list)
{
	if (NULL == list)
		return;
	list->prev = list->next = NULL;
}

void cg_list_remove(CgList *list)
{
	if(list == NULL)
		return;

	list->next->prev = list->prev;
	list->prev->next = list->next;
	list->prev = list->next = list;
}

void cg_list_add(CgList *listprev, CgList *list)
{
	if (list == NULL || listprev == NULL) {
		return;
	}
	if (listprev->next == NULL) {

		listprev->next = list;
		list->prev = listprev;
		return;
	}
	list->prev = listprev;
	list->next = listprev->next;
	listprev->next->prev = list;
	listprev->next = list;
}

void cg_list_clear(CgList *headlist, CG_LIST_DESTRUCTORFUNC desfuc)
{
	CgList *list = NULL;
	CgList *list_ent = NULL;

	if (headlist == NULL)
		return ;
	list = headlist->next;
	if (list == NULL)
		return;
	while (list->next != NULL) {
		list_ent = list->next;
		cg_list_remove(list);
		if (NULL != desfuc)
			desfuc(list);
		else
			cg_free(list);
		list = list_ent;
	}

	if (NULL != desfuc)
		desfuc(list);
	else
		cg_free(list);
}

