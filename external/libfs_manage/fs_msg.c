/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: Huaping Liao <huaping.liao@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>

#include "fs_storage.h"
#include "fs_msg.h"

FS_EVENT_CALLBACK fs_msg_file_call;

void fs_msg_file_reg_callback(FS_EVENT_CALLBACK call)
{
 	fs_msg_file_call = call;
}

void fs_msg_file_callback(int cmd, void *msg0, void *msg1)
{
 	if (fs_msg_file_call)
		return (*fs_msg_file_call)(cmd, msg0, msg1);
}

void fs_msg_notify_file(FILE_NOTIFY notify, const char *filename, int arg)
{
	FILE_NOTIFY m_notify = notify;

	if (filename == NULL) {
		if (notify >= CARD_BAD && notify <= SPEED_ADJ)
			fs_msg_file_callback(m_notify, NULL, (void *)arg);
		return;
	}
	if (strstr(filename, FS_PREPARE_FORMAT) != NULL)
		return;
	if (fs_storage_get_bythumb(filename) != NULL)
		return;
	if (m_notify == FILE_NEW && strstr(filename, ".jpg") != NULL)
		m_notify = FILE_END;

	fs_msg_file_callback(m_notify, (void *)filename, NULL);
}
