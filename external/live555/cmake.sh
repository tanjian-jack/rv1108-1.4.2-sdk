#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path

if [ "$rebuild" == "yes"  ]; then
    make distclean
    rm -f Makefile
    cd rk-streamer && make clean
    cd ..
fi

#for  project config
result=$(find ./ -maxdepth 1 -name Makefile)
if [ "$result" = "" ];then
	echo $1" do configure"
    ./genMakefiles rk110x
fi

cpu_cores=`cat /proc/cpuinfo | grep processor | wc -l`
make -j$cpu_cores
cd rk-streamer
make -B -j$cpu_cores
cd ..
[ ! -d $bash_path/../../out/system/bin ] && mkdir -p $bash_path/../../out/system/bin
cp rk-streamer/rk-streamer ../../out/system/bin/

cd $current_path
