/**
 * Copyright (C) 2016 Fuzhou Rockchip Electronics Co., Ltd
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "MediaInput.hh"
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>

#include <algorithm>

// A common "FramedSource" subclass, used for reading from an open pipe:

class OpenPipeSource : public FramedSource {
 protected:
  OpenPipeSource(UsageEnvironment& env, MediaInput& input, int fileNo);
  virtual ~OpenPipeSource();

  virtual bool readFromPipe(bool flush = false) = 0;
  virtual void flush();

 private:  // redefined virtual functions:
  virtual void doGetNextFrame();
  virtual void doStopGettingFrames();

 private:
  static void incomingDataHandler(OpenPipeSource* source, int mask);
  void incomingDataHandler1();

 protected:
  MediaInput& fInput;
  int fFileNo;
};

#define VIDEO_FIFO_NAME "/tmp/rv110x_video_fifo"
class VideoOpenPipeSource : public OpenPipeSource {
 public:
  VideoOpenPipeSource(UsageEnvironment& env, MediaInput& input);
  virtual ~VideoOpenPipeSource();

 protected:  // redefined virtual functions:
  virtual bool readFromPipe(bool flush = false);
};

#define AUDIO_FIFO_NAME "/tmp/rv110x_audio_fifo"
class AudioOpenPipeSource : public OpenPipeSource {
 public:
  AudioOpenPipeSource(UsageEnvironment& env, MediaInput& input);
  virtual ~AudioOpenPipeSource();

 protected:  // redefined virtual functions:
  virtual bool readFromPipe(bool flush = false);
};

const char* MediaInput::kCommuPath = "/tmp/live_commu_fifo";

MediaInput::MediaInput(UsageEnvironment& env)
    : Medium(env),
      fOurVideoFileNo(-1),
      fOurVideoSource(nullptr),
      fOurAudioFileNo(-1),
      fOurAudioSource(nullptr),
      commu_fd_(-1) {}

MediaInput::~MediaInput() {
  fprintf(stderr, "ENTER %s\n", __FUNCTION__);
  if (fOurVideoFileNo >= 0)
    closePipe(fOurVideoFileNo);
  //  if (fOurVideoSource)
  //    delete fOurVideoSource;

  if (fOurAudioFileNo >= 0)
    closePipe(fOurAudioFileNo);
  //  if (fOurAudioSource)
  //    delete fOurAudioSource;
  if (commu_fd_ >= 0)
    closePipe(commu_fd_);
  fprintf(stderr, "EXIT %s\n", __FUNCTION__);
}

static void Write(int fd, const char* content, size_t content_len) {
  if (fd >= 0) {
    ssize_t ret = write(fd, content, content_len);
    if ((size_t)ret != content_len)
      fprintf(stderr, "write %s failed, errno: %s\n", content, strerror(errno));
  }
}

void MediaInput::Start() {
  static const char* notify_str = "live:start_stream";
  Write(commu_fd_, notify_str, strlen(notify_str) + 1);
}

void MediaInput::Stop() {
  static const char* notify_str = "live:stop_stream";
  Write(commu_fd_, notify_str, strlen(notify_str) + 1);
}

MediaInput* MediaInput::createNew(UsageEnvironment& env) {
  MediaInput* mi = new MediaInput(env);
  if (mi && !mi->initialize(env)) {
    delete mi;
    return nullptr;
  }
  return mi;
}

FramedSource* MediaInput::videoSource() {
  if (fOurVideoSource == NULL) {
    fOurVideoSource = new VideoOpenPipeSource(envir(), *this);
  }
  return fOurVideoSource;
}

FramedSource* MediaInput::audioSource() {
  if (fOurAudioSource == NULL) {
    fOurAudioSource = new AudioOpenPipeSource(envir(), *this);
  }
  return fOurAudioSource;
}

Boolean MediaInput::initialize(UsageEnvironment& env) {
  do {
    if (!openFiles(env))
      break;
    if (!initAudio(env))
      break;
    if (!initVideo(env))
      break;

    return True;
  } while (0);

  // An error occurred
  return False;
}

static void printErr(UsageEnvironment& env, char const* str = NULL) {
  if (str != NULL)
    env << str;
  env << ": " << strerror(env.getErrno()) << "\n";
}

Boolean MediaInput::openPipe(UsageEnvironment& env, const char* path, int& fd) {
  if (access(path, F_OK) == -1) {
    env << "Debug: " << path << " is not exist, try creating it.\n";
    int res = mkfifo(path, 0777);
    if (res != 0) {
      env << "Debug: Create fifo " << path << " failed.\n";
      printErr(env);
      return False;
    }
  }
  fd = open(path, O_RDWR);
  if (fd < 0) {
    env << "Unable to open \"" << path << "\"";
    printErr(env);
    return False;
  }
  return True;
}

Boolean MediaInput::closePipe(int& fd) {
  if (fd < 0 || ::close(fd)) {
    fprintf(stderr, "close fd <%d> failed, errno: %s\n", fd, strerror(errno));
    return False;
  }
  fd = -1;
  return True;
}

Boolean MediaInput::openFiles(UsageEnvironment& env) {
  do {
    // Open it:
    if (openPipe(env, VIDEO_FIFO_NAME, fOurVideoFileNo) == False)
      break;

    if (openPipe(env, AUDIO_FIFO_NAME, fOurAudioFileNo) == False)
      break;

    // Open communication with app/video
    if (openPipe(env, kCommuPath, commu_fd_) == False)
      break;

    return True;
  } while (0);

  // An error occurred:
  return False;
}

Boolean MediaInput::initAudio(UsageEnvironment& env) {
  return True;
}

Boolean MediaInput::initVideo(UsageEnvironment& env) {
  return True;
}

OpenPipeSource::OpenPipeSource(UsageEnvironment& env,
                               MediaInput& input,
                               int fileNo)
    : FramedSource(env), fInput(input), fFileNo(fileNo) {}

OpenPipeSource::~OpenPipeSource() {
  envir().taskScheduler().turnOffBackgroundReadHandling(fFileNo);
}

void OpenPipeSource::doGetNextFrame() {
  // Await the next incoming data on our FID:
  envir().taskScheduler().turnOnBackgroundReadHandling(
      fFileNo, (TaskScheduler::BackgroundHandlerProc*)&incomingDataHandler,
      this);
}

void OpenPipeSource::doStopGettingFrames() {
  FramedSource::doStopGettingFrames();
  flush();
}

void OpenPipeSource::incomingDataHandler(OpenPipeSource* source, int /*mask*/) {
  source->incomingDataHandler1();
}

void OpenPipeSource::incomingDataHandler1() {
  // Read the data from our file into the client's buffer:
  readFromPipe();

  // Stop handling any more input, until we're ready again:
  envir().taskScheduler().turnOffBackgroundReadHandling(fFileNo);

  // Tell our client that we have new data:
  afterGetting(this);
}

bool OpenPipeSource::readFromPipe(bool flush) {
  return false;
}

void OpenPipeSource::flush() {
  if (fFileNo < 0)
    return;
  while (1) {
    struct pollfd clientfds;
    clientfds.fd = fFileNo;
    clientfds.events = POLLIN | POLLERR;
    clientfds.revents = 0;
    int p_ret = poll(&clientfds, 1, 200);
#ifdef DEBUG_SEND
    fprintf(stderr, "in %s, p_ret: %d, clientfds.revents: %d\n", __func__,
            p_ret, clientfds.revents);
#endif
    if (p_ret < 0 || !(clientfds.revents & POLLIN))
      break;
    readFromPipe(true);
  }
  fFrameSize = 0;
  fNumTruncatedBytes = 0;
}

////////// VideoOpenPipeSource implementation //////////

VideoOpenPipeSource::VideoOpenPipeSource(UsageEnvironment& env,
                                         MediaInput& input)
    : OpenPipeSource(env, input, input.fOurVideoFileNo) {}

VideoOpenPipeSource::~VideoOpenPipeSource() {
  fInput.fOurVideoSource = NULL;
}

bool VideoOpenPipeSource::readFromPipe(bool flush) {
  assert(fFileNo > 0);
#ifdef DEBUG_SEND
  fprintf(stderr, "$$$$ %s, %d\n", __func__, __LINE__);
#endif
  do {
    // Note the timestamp and size:
    ssize_t read_size = (ssize_t)sizeof(struct timeval);
    ssize_t ret = read(fFileNo, &fPresentationTime, read_size);
    if (ret != read_size) {
      envir() <<  __LINE__ << " read from pipe error: " << errno << "\n";
      break;
    }
#ifdef DEBUG_SEND
    envir() << "video read pipe frame time: " << (int)fPresentationTime.tv_sec
            << "s, " << (int)fPresentationTime.tv_usec << "us. \n";
#endif
    assert(sizeof(fFrameSize) >= sizeof(unsigned int));
    read_size = sizeof(unsigned int);
    ret = read(fFileNo, &fFrameSize, read_size);
    if (ret != read_size) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
    assert(fFrameSize > 0);
#ifdef DEBUG_SEND
    envir() << "video read pipe size: " << fFrameSize << "\n";
#endif
    char startcode[3];
    read_size = sizeof(startcode);
    ret = read(fFileNo, startcode, read_size);
    if (ret != read_size) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
    assert(startcode[0] == 0);
    assert(startcode[1] == 0);
    if (startcode[2] == 0) {
      if (read(fFileNo, startcode, 1) != 1) {
        envir() << __LINE__ << "read from pipe error: " << errno << "\n";
        break;
      }
      read_size += 1;
      assert(startcode[0] == 0x1);
    } else {
      assert(startcode[2] == 0x1);
    }
    fFrameSize -= read_size;
    if (flush) {
      char tmp[512];
      while (fFrameSize > 0) {
        read_size = std::min(sizeof(tmp), fFrameSize);
        ret = read(fFileNo, tmp, read_size);
        if (ret != read_size && errno != EAGAIN) {
          envir() << __LINE__ << "read from pipe error: " << errno << "\n";
          break;
        }
        fFrameSize -= ret;
      }
      break;
    }
    if (fFrameSize > fMaxSize) {
      fNumTruncatedBytes = fFrameSize - fMaxSize;
      fFrameSize = fMaxSize;
    } else {
      fNumTruncatedBytes = 0;
    }
    ret = read(fFileNo, fTo, fFrameSize);
    if ((unsigned)ret != fFrameSize) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
#ifdef DEBUG_SEND
    // printf("get 0x%02x\n", fTo[0]);
    fprintf(stderr, "$$$$ %s, %d\n", __func__, __LINE__);
#endif
    return true;
  } while (0);
  fFrameSize = 0;
  fNumTruncatedBytes = 0;
  return false;
}

////////// AudioOpenPipeSource implementation //////////

AudioOpenPipeSource::AudioOpenPipeSource(UsageEnvironment& env,
                                         MediaInput& input)
    : OpenPipeSource(env, input, input.fOurAudioFileNo) {}

AudioOpenPipeSource::~AudioOpenPipeSource() {
  fInput.fOurAudioSource = NULL;
}

bool AudioOpenPipeSource::readFromPipe(bool flush) {
  assert(fFileNo > 0);
#ifdef DEBUG_SEND
  fprintf(stderr, "$$$$ %s, %d\n", __func__, __LINE__);
#endif
  do {
    // Note the timestamp and size:
    ssize_t read_size = (ssize_t)sizeof(struct timeval);
    ssize_t ret = read(fFileNo, &fPresentationTime, read_size);
    if (ret != read_size) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
#ifdef DEBUG_SEND
    envir() << "audio read pipe frame time: " << (int)fPresentationTime.tv_sec
            << "s, " << (int)fPresentationTime.tv_usec << "us. \n";
#endif
    assert(sizeof(fFrameSize) >= sizeof(unsigned int));
    read_size = sizeof(unsigned int);
    ret = read(fFileNo, &fFrameSize, read_size);
    if (ret != read_size) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
    assert(fFrameSize > 0);
#ifdef DEBUG_SEND
    envir() << "audio read pipe size: " << fFrameSize << "\n";
#endif
    if (flush) {
      char tmp[512];
      while (fFrameSize > 0) {
        read_size = std::min(sizeof(tmp), fFrameSize);
        ret = read(fFileNo, tmp, read_size);
        if (ret != read_size && errno != EAGAIN) {
          envir() << __LINE__ << "read from pipe error: " << errno << "\n";
          break;
        }
        fFrameSize -= ret;
      }
      break;
    }
    if (fFrameSize > fMaxSize) {
      fNumTruncatedBytes = fFrameSize - fMaxSize;
      fFrameSize = fMaxSize;
    } else {
      fNumTruncatedBytes = 0;
    }
    ret = read(fFileNo, fTo, fFrameSize);
    if ((unsigned)ret != fFrameSize) {
      envir() << __LINE__ << "read from pipe error: " << errno << "\n";
      break;
    }
#ifdef DEBUG_SEND
    fprintf(stderr, "$$$$ %s, %d\n", __func__, __LINE__);
#endif
    return true;
  } while (0);
  fFrameSize = 0;
  fNumTruncatedBytes = 0;
  return false;
}
