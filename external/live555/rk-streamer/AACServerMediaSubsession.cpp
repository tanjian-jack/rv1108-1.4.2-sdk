#include "AACServerMediaSubsession.hh"

#include "FramedFilter.hh"
#include "MPEG4GenericRTPSink.hh"

AACServerMediaSubsession* AACServerMediaSubsession::createNew(
    UsageEnvironment& env,
    MediaInput& mediaInput) {
  return new AACServerMediaSubsession(env, mediaInput);
}

AACServerMediaSubsession::AACServerMediaSubsession(UsageEnvironment& env,
                                                   MediaInput& mediaInput)
    : OnDemandServerMediaSubsession(env, True),
      fMediaInput(mediaInput),
      fEstimatedKbps(64),
      fAudioSamplingFrequency(44100),
      fAudioNumChannels(2) {}

AACServerMediaSubsession::~AACServerMediaSubsession() {}

FramedSource* AACServerMediaSubsession::createNewStreamSource(
    unsigned clientSessionId,
    unsigned& estBitrate) {
  estBitrate = fEstimatedKbps;
  return fMediaInput.audioSource();
}

RTPSink* AACServerMediaSubsession::createNewRTPSink(
    Groupsock* rtpGroupsock,
    unsigned char rtpPayloadTypeIfDynamic,
    FramedSource* inputSource) {
  if (!inputSource)
    return NULL;
  char const* encoderConfigStr = fAudioNumChannels == 2 ? "1210" : "1208";
  RTPSink* audioSink = MPEG4GenericRTPSink::createNew(
      envir(), rtpGroupsock, rtpPayloadTypeIfDynamic, fAudioSamplingFrequency,
      "audio", "AAC-hbr", encoderConfigStr, fAudioNumChannels);
  return audioSink;
}
