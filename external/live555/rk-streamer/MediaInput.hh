/**
 * Copyright (C) 2016 Fuzhou Rockchip Electronics Co., Ltd
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef MEDIAINPUT_H
#define MEDIAINPUT_H

#include <MediaSink.hh>

class MediaInput : public Medium {
 public:
  static MediaInput* createNew(UsageEnvironment& env);

  FramedSource* videoSource();
  FramedSource* audioSource();
  void Start();
  void Stop();

 private:
  MediaInput(UsageEnvironment& env);
  virtual ~MediaInput();
  Boolean initialize(UsageEnvironment& env);
  Boolean openPipe(UsageEnvironment& env, const char* path, int& fd);
  Boolean closePipe(int& fd);
  Boolean openFiles(UsageEnvironment& env);
  Boolean initAudio(UsageEnvironment& env);
  Boolean initVideo(UsageEnvironment& env);

 private:
  static const char* kCommuPath;
  friend class VideoOpenPipeSource;
  friend class AudioOpenPipeSource;
  int fOurVideoFileNo;
  FramedSource* fOurVideoSource;
  int fOurAudioFileNo;
  FramedSource* fOurAudioSource;

  int commu_fd_;  // communication fd
};

// Functions to set the optimal buffer size for RTP sink objects.
// These should be called before each RTPSink is created.
#define AUDIO_MAX_FRAME_SIZE 204800
#define VIDEO_MAX_FRAME_SIZE (1920 * 1080 * 3 / 2)
inline void setAudioRTPSinkBufferSize() {
  OutPacketBuffer::maxSize = AUDIO_MAX_FRAME_SIZE;
}
inline void setVideoRTPSinkBufferSize() {
  OutPacketBuffer::maxSize = VIDEO_MAX_FRAME_SIZE;
}

#endif  // MEDIAINPUT_H
