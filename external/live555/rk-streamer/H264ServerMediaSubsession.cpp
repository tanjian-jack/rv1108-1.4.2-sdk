#include "H264ServerMediaSubsession.hh"
#include <H264VideoRTPSink.hh>
#include <H264VideoStreamDiscreteFramer.hh>

H264ServerMediaSubsession* H264ServerMediaSubsession::createNew(
    UsageEnvironment& env,
    MediaInput& wisInput) {
  return new H264ServerMediaSubsession(env, wisInput);
}

H264ServerMediaSubsession::H264ServerMediaSubsession(UsageEnvironment& env,
                                                     MediaInput& mediaInput)
    : OnDemandServerMediaSubsession(env, True /*reuse the first source*/),
      fMediaInput(mediaInput),
      fEstimatedKbps(1000),
      fDoneFlag(0),
      fDummyRTPSink(NULL),
      fGetSdpTimeOut(1000 * 10),
      sdpState(INITIAL) {}

H264ServerMediaSubsession::~H264ServerMediaSubsession() {}

std::mutex H264ServerMediaSubsession::kMutex;
std::list<unsigned int> H264ServerMediaSubsession::kSessionIdList;

void H264ServerMediaSubsession::startStream(
    unsigned clientSessionId,
    void* streamToken,
    TaskFunc* rtcpRRHandler,
    void* rtcpRRHandlerClientData,
    unsigned short& rtpSeqNum,
    unsigned& rtpTimestamp,
    ServerRequestAlternativeByteHandler* serverRequestAlternativeByteHandler,
    void* serverRequestAlternativeByteHandlerClientData) {
  OnDemandServerMediaSubsession::startStream(
      clientSessionId, streamToken, rtcpRRHandler, rtcpRRHandlerClientData,
      rtpSeqNum, rtpTimestamp, serverRequestAlternativeByteHandler,
      serverRequestAlternativeByteHandlerClientData);
  kMutex.lock();
  if (kSessionIdList.empty())
    fMediaInput.Start();
  fprintf(stderr, "%s - clientSessionId: 0x%08x\n", __func__, clientSessionId);
  kSessionIdList.push_back(clientSessionId);
  kMutex.unlock();
}
void H264ServerMediaSubsession::deleteStream(unsigned clientSessionId,
                                             void*& streamToken) {
  kMutex.lock();
  fprintf(stderr, "%s - clientSessionId: 0x%08x\n", __func__, clientSessionId);
  kSessionIdList.remove(clientSessionId);
  if (kSessionIdList.empty())
    fMediaInput.Stop();
  kMutex.unlock();
  OnDemandServerMediaSubsession::deleteStream(clientSessionId, streamToken);
}

static void afterPlayingDummy(void* clientData) {
  H264ServerMediaSubsession* subsess = (H264ServerMediaSubsession*)clientData;
  fprintf(stderr, "%s, set done.\n", __FUNCTION__);
  // Signal the event loop that we're done:
  subsess->setDoneFlag();
}

static void checkForAuxSDPLine(void* clientData) {
  H264ServerMediaSubsession* subsess = (H264ServerMediaSubsession*)clientData;
  subsess->checkForAuxSDPLine1();
}

void H264ServerMediaSubsession::checkForAuxSDPLine1() {
  fprintf(stderr, "** fDoneFlag: %d, fGetSdpTimeOut: %d **\n", fDoneFlag,
          fGetSdpTimeOut);
  if (fDummyRTPSink->auxSDPLine() != NULL) {
    // Signal the event loop that we're done:
    fprintf(stderr, "%s, set done.\n", __FUNCTION__);
    setDoneFlag();
  } else {
    if (fGetSdpTimeOut <= 0) {
      fprintf(stderr, "get sdp time out, set done.\n");
      sdpState = GET_SDP_LINES_TIMEOUT;
      setDoneFlag();
    } else {
      // try again after a brief delay:
      int uSecsToDelay = 100000;  // 100 ms
      nextTask() = envir().taskScheduler().scheduleDelayedTask(
          uSecsToDelay, (TaskFunc*)checkForAuxSDPLine, this);
      fGetSdpTimeOut -= uSecsToDelay;
    }
  }
}

char const* H264ServerMediaSubsession::getAuxSDPLine(
    RTPSink* rtpSink,
    FramedSource* inputSource) {
  // Note: For MPEG-4 video buffer, the 'config' information isn't known
  // until we start reading the Buffer.  This means that "rtpSink"s
  // "auxSDPLine()" will be NULL initially, and we need to start reading
  // data from our buffer until this changes.
  sdpState = GETTING_SDP_LINES;
  fDoneFlag = 0;
  fDummyRTPSink = rtpSink;
  fGetSdpTimeOut = 100000 * 10;
  // Start reading the buffer:
  fDummyRTPSink->startPlaying(*inputSource, afterPlayingDummy, this);
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  // Check whether the sink's 'auxSDPLine()' is ready:
  checkForAuxSDPLine(this);

  envir().taskScheduler().doEventLoop(&fDoneFlag);
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  char const* auxSDPLine = fDummyRTPSink->auxSDPLine();
  fprintf(stderr, "++ auxSDPLine: %p\n", auxSDPLine);
  if (auxSDPLine)
    sdpState = GOT_SDP_LINES;
  return auxSDPLine;
}

char const* H264ServerMediaSubsession::sdpLines() {
  if (!fSDPLines)
    sdpState = INITIAL;
  char const* ret = OnDemandServerMediaSubsession::sdpLines();
  if (sdpState == GET_SDP_LINES_TIMEOUT) {
    if (fSDPLines) {
      delete[] fSDPLines;
      fSDPLines = NULL;
    }
    ret = NULL;
  }
  return ret;
}

FramedSource* H264ServerMediaSubsession::createNewStreamSource(
    unsigned /*clientSessionId*/,
    unsigned& estBitrate) {
  estBitrate = fEstimatedKbps;
  if (sdpState == GETTING_SDP_LINES || sdpState == GET_SDP_LINES_TIMEOUT) {
    fprintf(stderr, "sdpline is not ready, can not create new stream source\n");
    return NULL;
  }
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  // Create a framer for the Video Elementary Stream:
  FramedSource* source = H264VideoStreamDiscreteFramer::createNew(
      envir(), fMediaInput.videoSource());
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  return source;
}

RTPSink* H264ServerMediaSubsession::createNewRTPSink(
    Groupsock* rtpGroupsock,
    unsigned char rtpPayloadTypeIfDynamic,
    FramedSource* inputSource) {
  if (!inputSource) {
    fprintf(stderr, "inputSource is not ready, can not create new rtp sink\n");
    return NULL;
  }
  setVideoRTPSinkBufferSize();
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  RTPSink* rtp_sink = H264VideoRTPSink::createNew(envir(), rtpGroupsock,
                                                  rtpPayloadTypeIfDynamic);
  fprintf(stderr, "<%s> %s: %d\n", __FILE__, __FUNCTION__, __LINE__);
  return rtp_sink;
}
