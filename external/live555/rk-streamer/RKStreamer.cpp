/**
 * Copyright (C) 2016 Fuzhou Rockchip Electronics Co., Ltd
 * author: hertz.wang hertz.wong@rock-chips.com
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <assert.h>
#include <BasicUsageEnvironment.hh>
#ifndef _RTSP_SERVER_HH
#include "RTSPServer.hh"
#endif

#include "MediaInput.hh"
#include "AACServerMediaSubsession.hh"
#include "H264ServerMediaSubsession.hh"

int main(int argc, char** argv) {
  // Begin by setting up our usage environment:
  TaskScheduler* scheduler = BasicTaskScheduler::createNew();
  assert(scheduler);
  UsageEnvironment* env = BasicUsageEnvironment::createNew(*scheduler);
  assert(env);
  UserAuthenticationDatabase* authDB = NULL;
  *env << "Initializing...\n";
  MediaInput* input = MediaInput::createNew(*env);
  if (!input) {
    *env << "Error: Failed to create WIS input device\n";
    exit(1);
  }
  portNumBits rtspServerPortNum = 554;
  RTSPServer* rtspServer =
      RTSPServer::createNew(*env, rtspServerPortNum, authDB, 1000);
  if (!rtspServer) {
    rtspServerPortNum = 8554;
    rtspServer = RTSPServer::createNew(*env, rtspServerPortNum, authDB, 1000);
  }
  if (!rtspServer) {
    *env << "Error: Failed to create RTSP server: " << env->getResultMsg()
         << "\n";
    exit(1);
  }

  char* url = nullptr;
  ServerMediaSession* sms = ServerMediaSession::createNew(
      *env, "stream0", "h264-aac", "rv110x camera rtsp stream",
      False /*UNICAST*/);
  if (!sms) {
    *env << "Error: Failed to create ServerMediaSession: "
         << env->getResultMsg() << "\n";
    goto err;
  }
  rtspServer->addServerMediaSession(sms);
  url = rtspServer->rtspURL(sms);
  *env << "Play this stream using the URL:\n\t" << url << "\n";
  if (url)
    delete[] url;

  sms->addSubsession(
      H264ServerMediaSubsession::createNew(sms->envir(), *input));
  sms->addSubsession(AACServerMediaSubsession::createNew(sms->envir(), *input));
  *env << "...done initializing\n";

  // Begin the LIVE555 event loop:
  env->taskScheduler().doEventLoop();  // does not return

err:
  *env << "rk-streamer exit\n";
  // will also reclaim "sms" and its "ServerMediaSubsession"s
  Medium::close(rtspServer);
  env->reclaim();
  delete scheduler;
  return 0;
}
