#ifndef AACSERVERMEDIASUBSESSION_H
#define AACSERVERMEDIASUBSESSION_H

#include <OnDemandServerMediaSubsession.hh>

#include "MediaInput.hh"

class AACServerMediaSubsession : public OnDemandServerMediaSubsession {
 public:
  static AACServerMediaSubsession* createNew(UsageEnvironment& env,
                                             MediaInput& mediaInput);

 protected:
  AACServerMediaSubsession(UsageEnvironment& env, MediaInput& mediaInput);
  virtual ~AACServerMediaSubsession();

 protected:
  MediaInput& fMediaInput;
  unsigned fEstimatedKbps;
  unsigned fAudioSamplingFrequency;
  unsigned fAudioNumChannels;

 private:
  virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
                                              unsigned& estBitrate);
  virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
                                    unsigned char rtpPayloadTypeIfDynamic,
                                    FramedSource* inputSource);
};

#endif  // AACSERVERMEDIASUBSESSION_H
