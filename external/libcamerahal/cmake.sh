#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path

BUILD_DIR=$(pwd)/out
SYSTEM_DIR=$(pwd)/../../out/system
SYSTEM_INC_DIR=$SYSTEM_DIR/include/
CAMERA_HAL_DIR=$(pwd)/CameraHal10_Release/CameraHal
CAMERA_HAL_IQ_DIR=$(pwd)/CameraHal10_Release/IQ

[ ! -d "$SYSTEM_INC_DIR"  ] && mkdir -p $SYSTEM_INC_DIR

if [ "$rebuild" == "yes"   ]; then
cd $CAMERA_HAL_DIR && ./make.sh
cd $CAMERA_HAL_IQ_DIR && ./make.sh
fi

cd $BUILD_DIR/inc/ && cp -r * $SYSTEM_INC_DIR

cd $current_path
