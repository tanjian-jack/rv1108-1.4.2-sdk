#! /bin/sh
SYSTEM_DIR=$(pwd)/../../../../out/system
BUILD_OUT_DIR=$(pwd)/../../out
SYSTEM_LIB_DIR=$SYSTEM_DIR/lib
[ ! -d "$SYSTEM_LIB_DIR" ] && mkdir $SYSTEM_LIB_DIR
[ -d "$BUILD_OUT_DIR" ] && rm -rf $BUILD_OUT_DIR
mkdir -p $BUILD_OUT_DIR/inc/CameraHal/

cp -r HAL/include/*          ../../out/inc/CameraHal/
cp -r include/CamHalVersion.h                     ../../out/inc/CameraHal/
cp -r include/shared_ptr.h   ../../out/inc/
cp -r include/ebase          ../../out/inc/
cp -r include/oslayer        ../../out/inc/
cp -r include/linux          ../../out/inc/CameraHal/

make clean

make

cp ./build/lib/libcam_hal.so $SYSTEM_LIB_DIR

echo "Camera HAL build success!"
