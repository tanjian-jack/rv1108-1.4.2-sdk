#include <ebase/types.h>
#include <ebase/trace.h>
#include <ebase/builtins.h>

#include <calib_xml/calibdb.h>
#include <utils/Log.h>

#include "cam_ia10_engine.h"
#include "cam_ia10_engine_isp_modules.h"
#ifdef RK_ISP10
#include "linux/v4l2-controls.h"
#include "linux/media/rk-isp10-config.h"
#endif
#ifdef RK_ISP11
#include "linux/v4l2-controls.h"
#include "linux/media/rk-isp11-config.h"
#endif
#include <common/cam_types.h>
#include <HAL/cam_types.h>

CamIA10Engine::CamIA10Engine():
  hCamCalibDb(NULL) {
  memset(&dCfg, 0x00, sizeof(struct CamIA10_DyCfg));
  memset(&dCfgShd, 0x00, sizeof(struct CamIA10_DyCfg));
  memset(&lastAwbResult, 0, sizeof(lastAwbResult));
  memset(&curAwbResult, 0, sizeof(curAwbResult));
  memset(&curAecResult, 0, sizeof(curAecResult));
  memset(&lastAecResult, 0, sizeof(lastAecResult));
  memset(&curAfcResult, 0, sizeof(curAfcResult));
  memset(&lastAfcResult, 0, sizeof(lastAfcResult));
  memset(&adpfCfg, 0, sizeof(adpfCfg));
  memset(&awbcfg, 0, sizeof(awbcfg));
  memset(&aecCfg, 0, sizeof(aecCfg));
  memset(&mStats, 0, sizeof(mStats));
  memset(&awdrCfg, 0, sizeof(awdrCfg));
  hCamCalibDb = NULL;
  hAwb =  NULL;
  hAdpf = NULL;
  hAf = NULL;
  hAwdr = NULL;
  lastAecResult.regIntegrationTime = -1;
  lastAecResult.regGain = -1;
  lastAecResult.coarse_integration_time = -1;
  lastAecResult.analog_gain_code_global = -1;
  mLightMode = LIGHT_MODE_DAY;
}

CamIA10Engine::~CamIA10Engine() {
  //ALOGD("%s: E", __func__);

  if (hAwb) {
    AwbStop(hAwb);
    AwbRelease(hAwb);
    hAwb = NULL;
  }

  if (hAf) {
    AfStop(hAf);
    AfRelease(hAf);
    hAf = NULL;
  }

  AecStop();
  AecRelease();
  if (hAdpf) {
    AdpfRelease(hAdpf);
    hAdpf = NULL;
  }

  if (hAwdr) {
    AwdrRelease(hAwdr);
    hAwdr = NULL;
  }

  hCamCalibDb = NULL;

  //ALOGD("%s: x", __func__);
}

RESULT CamIA10Engine::initStatic
(
    char* aiqb_data_file
) {
  RESULT result = RET_FAILURE;
  if (!hCamCalibDb) {
    if (calidb.CreateCalibDb(aiqb_data_file)) {
      ALOGD("load tunning file success.");
      hCamCalibDb = calidb.GetCalibDbHandle();
    } else {
      ALOGD("load tunning file failed..");
      goto init_fail;
    }
  }

  result = initAEC();
  if (result != RET_SUCCESS)
    goto init_fail;

  result = initAWB();
  if (result != RET_SUCCESS)
    goto init_fail;

  result = initAF();
  if (result != RET_SUCCESS)
    goto init_fail;

  return RET_SUCCESS;
init_fail:
  return result;
}

RESULT CamIA10Engine::initDynamic(struct CamIA10_DyCfg* cfg) {
  RESULT result = RET_SUCCESS;
  //init awb static
  if (!hAwb) {
    AwbInstanceConfig_t awbInstance;
    result = AwbInit(&awbInstance);
    if (cfg->awb_cfg.mode != HAL_WB_AUTO) {
      char prfName[10];
      int i, no;
      CamIlluProfile_t* pIlluProfile = NULL;
      awbcfg.Mode = AWB_MODE_MANUAL;
      if (cfg->awb_cfg.mode == HAL_WB_INCANDESCENT) {
        strcpy(prfName, "A");
      } else if (cfg->awb_cfg.mode == HAL_WB_DAYLIGHT) {
        strcpy(prfName, "D65");
      } else if (cfg->awb_cfg.mode == HAL_WB_FLUORESCENT) {
        strcpy(prfName, "F11_TL84");
      } else if (cfg->awb_cfg.mode == HAL_WB_SUNSET) {
        strcpy(prfName, "HORIZON"); //not support now
      } else if (cfg->awb_cfg.mode == HAL_WB_CLOUDY_DAYLIGHT) {
        strcpy(prfName, "F2_CWF");
      } else if (cfg->awb_cfg.mode == HAL_WB_CANDLE) {
        strcpy(prfName, "U30"); //not support now
      } else
        LOGE("%s:not support this awb mode %d !", __func__, cfg->awb_cfg.mode);

      // get number of availabe illumination profiles from database
      result = CamCalibDbGetNoOfIlluminations(hCamCalibDb, &no);
      // run over all illumination profiles
      for (i = 0; i < no; i++) {
        // get an illumination profile from database
        result = CamCalibDbGetIlluminationByIdx(hCamCalibDb, i, &pIlluProfile);
        if (strstr(pIlluProfile->name, prfName)) {
          awbcfg.idx = i;
          break;
        }
      }

      if (i == no)
        LOGE("%s:can't find %s profile!", __func__, prfName);
    }
    if (result != RET_SUCCESS)
      goto initDynamic_end;
    else
      hAwb = awbInstance.hAwb;

    awbcfg.width = cfg->sensor_mode.isp_input_width;
    awbcfg.height = cfg->sensor_mode.isp_input_height;
    awbcfg.awbWin.h_offs = 0;
    awbcfg.awbWin.v_offs = 0;
    awbcfg.awbWin.h_size = HAL_WIN_REF_WIDTH;
    awbcfg.awbWin.v_size = HAL_WIN_REF_HEIGHT;

    result = AwbConfigure(hAwb, &awbcfg);
    if (result != RET_SUCCESS) {
      LOGE("%s:awb config failure!", __func__);
      AwbRelease(hAwb);
      hAwb = NULL;
      goto initDynamic_end;

    }
    //start awb
    result = AwbStart(hAwb, &awbcfg);
    if (result != RET_SUCCESS) {
      LOGE("%s:awb start failure!", __func__);
      AwbRelease(hAwb);
      hAwb = NULL;
      goto initDynamic_end;
    }
    AwbRunningOutputResult_t outresult = {0};
    result =  AwbRun(hAwb, NULL, &outresult);
    if (result == RET_SUCCESS) {
      //convert result
      memset(&curAwbResult, 0, sizeof(curAwbResult));
      convertAwbResult2Cameric(&outresult, &curAwbResult);
    } else {
      LOGE("%s:awb run failure!", __func__);
      AwbStop(hAwb);
      AwbRelease(hAwb);
      hAwb = NULL;
      goto initDynamic_end;
    }
  } else {
    if (cfg->awb_cfg.win.right_width && cfg->awb_cfg.win.bottom_height) {
      awbcfg.awbWin.h_offs = cfg->awb_cfg.win.left_hoff;
      awbcfg.awbWin.v_offs = cfg->awb_cfg.win.top_voff;
      awbcfg.awbWin.h_size = cfg->awb_cfg.win.right_width;
      awbcfg.awbWin.v_size = cfg->awb_cfg.win.bottom_height;
    } else {
      awbcfg.awbWin.h_offs = 0;
      awbcfg.awbWin.v_offs = 0;
      awbcfg.awbWin.h_size = HAL_WIN_REF_WIDTH;
      awbcfg.awbWin.v_size = HAL_WIN_REF_HEIGHT;
    }
    //mode change ?
    if (cfg->awb_cfg.mode != dCfg.awb_cfg.mode) {
	  memset(&lastAwbResult, 0x00, sizeof(lastAwbResult));
      if (cfg->awb_cfg.mode != HAL_WB_AUTO) {
        char prfName[10];
        int i, no;
        CamIlluProfile_t* pIlluProfile = NULL;
        AwbStop(hAwb);
        awbcfg.Mode = AWB_MODE_MANUAL;
        //get index
        //A:3400k
        //D65:6500K artificial daylight
        //CWF:4150K cool whtie fluorescent
        //TL84: 4000K
        //D50: 5000k
        //D75:7500K
        //HORIZON:2300K
        //SNOW :6800k
        //CANDLE:1850K
        if (cfg->awb_cfg.mode == HAL_WB_INCANDESCENT) {
          strcpy(prfName, "A");
        } else if (cfg->awb_cfg.mode == HAL_WB_DAYLIGHT) {
          strcpy(prfName, "D65");
        } else if (cfg->awb_cfg.mode == HAL_WB_FLUORESCENT) {
          strcpy(prfName, "F11_TL84");
        } else if (cfg->awb_cfg.mode == HAL_WB_SUNSET) {
          strcpy(prfName, "HORIZON"); //not support now
        } else if (cfg->awb_cfg.mode == HAL_WB_CLOUDY_DAYLIGHT) {
          strcpy(prfName, "F2_CWF");
        } else if (cfg->awb_cfg.mode == HAL_WB_CANDLE) {
          strcpy(prfName, "U30"); //not support now
        } else
          LOGE("%s:not support this awb mode %d !", __func__, cfg->awb_cfg.mode);

        // get number of availabe illumination profiles from database
        result = CamCalibDbGetNoOfIlluminations(hCamCalibDb, &no);
        // run over all illumination profiles
        for (i = 0; i < no; i++) {
          // get an illumination profile from database
          result = CamCalibDbGetIlluminationByIdx(hCamCalibDb, i, &pIlluProfile);
          if (strstr(pIlluProfile->name, prfName)) {
            awbcfg.idx = i;
            break;
          }
        }

        if (i == no)
          LOGE("%s:can't find %s profile!", __func__, prfName);
        else
          AwbStart(hAwb, &awbcfg);
      } else {
        //from manual to auto
        AwbStop(hAwb);
        initAWB();
        AwbStart(hAwb, &awbcfg);
      }
    }

    //awb locks
    if ((cfg->aaa_locks & HAL_3A_LOCKS_WB)
        && (dCfg.awb_cfg.mode == HAL_WB_AUTO)) {
      AwbTryLock(hAwb);
    } else if (dCfg.aaa_locks & HAL_3A_LOCKS_WB)
      AwbUnLock(hAwb);
  }

  if (!hAdpf) {
    adpfCfg.data.db.width = cfg->sensor_mode.isp_input_width;
    adpfCfg.data.db.height = cfg->sensor_mode.isp_input_height;
    adpfCfg.data.db.hCamCalibDb  = hCamCalibDb;
	adpfCfg.LightMode = cfg->LightMode;
    result = AdpfInit(&hAdpf, &adpfCfg);
  } else {
    result = AdpfConfigure(hAdpf, &adpfCfg);
    if (result != RET_SUCCESS)
      goto initDynamic_end;
  }
  //mLightMode = cfg->LightMode;
  
  //ALOGE("%s: 222 lightmode:%d!", __func__, cfg->LightMode);

  if (!hAwdr) {
    awdrCfg.hCamCalibDb = hCamCalibDb;
    result = AwdrInit(&hAwdr, &awdrCfg);
    if (result != RET_SUCCESS)
      goto initDynamic_end;
  } else {
    result = AwdrConfigure(hAwdr, &awdrCfg);
    if (result != RET_SUCCESS)
      goto initDynamic_end;
  }

  dCfg = *cfg;

  struct HAL_AecCfg* set, *shd;
  int meter_coeff_flg;
  set = &dCfg.aec_cfg;
  shd = &dCfgShd.aec_cfg;

  meter_coeff_flg = 0;
  for (int i = 0; i < sizeof(set->meter_coeff); i++) {
	if (set->meter_coeff[i] != shd->meter_coeff[i]) {
	  meter_coeff_flg = 1;
	  break;
	}
  }
  
  if ((set->win.left_hoff != shd->win.left_hoff) ||
      (set->win.top_voff != shd->win.top_voff) ||
      (set->win.right_width != shd->win.right_width) ||
      (set->win.bottom_height != shd->win.bottom_height) ||
      (set->meter_mode != shd->meter_mode) ||
      (set->mode != shd->mode) ||
      (set->flk != shd->flk) ||
      (shd->meter_mode == HAL_AE_METERING_MODE_USER && meter_coeff_flg == 1)||
      (set->ae_bias != shd->ae_bias)||
      (set->set_point != shd->set_point)||
      (mLightMode != cfg->LightMode)||
      (set->aeMaxExpGain != shd->aeMaxExpGain) ||
      (set->aeMaxExpTime != shd->aeMaxExpTime) || 
      (set->api_set_fps != shd->api_set_fps) ||
      (aecCfg.LinePeriodsPerField != dCfg.sensor_mode.line_periods_per_field) ||
      (aecCfg.PixelPeriodsPerLine != dCfg.sensor_mode.pixel_periods_per_line)) {
    uint16_t step_width, step_height;
    //cifisp_histogram_mode mode = CIFISP_HISTOGRAM_MODE_RGB_COMBINED;
    cam_ia10_map_hal_win_to_isp(
        set->win.right_width,
        set->win.bottom_height,
        cfg->sensor_mode.isp_input_width,
        cfg->sensor_mode.isp_input_height,
        &step_width,
        &step_height
    );
    cam_ia10_isp_hst_update_stepSize(
        aecCfg.HistMode,
        aecCfg.GridWeights.uCoeff,
        step_width,
        step_height,
        &(aecCfg.StepSize));

    //ALOGD("aec set win:%dx%d",
    //  set->win.right_width,set->win.bottom_height);

    aecCfg.LinePeriodsPerField = dCfg.sensor_mode.line_periods_per_field;
    aecCfg.PixelPeriodsPerLine = dCfg.sensor_mode.pixel_periods_per_line;
    aecCfg.PixelClockFreqMHZ = dCfg.sensor_mode.pixel_clock_freq_mhz;
    if (aecCfg.ApiSetFps != 0)
      aecCfg.PixelClockFreqMHZ = aecCfg.LinePeriodsPerField *
        aecCfg.PixelPeriodsPerLine * aecCfg.ApiSetFps / 1000000.0f;

    aecCfg.ApiSetMaxExpTime = dCfg.aec_cfg.aeMaxExpTime;
    aecCfg.ApiSetMaxGain = dCfg.aec_cfg.aeMaxExpGain;
    aecCfg.ApiSetFps = dCfg.aec_cfg.api_set_fps;

    if (set->flk == HAL_AE_FLK_OFF)
      aecCfg.EcmFlickerSelect = AEC_EXPOSURE_CONVERSION_FLICKER_OFF;
    else if (set->flk == HAL_AE_FLK_50)
      aecCfg.EcmFlickerSelect = AEC_EXPOSURE_CONVERSION_FLICKER_100HZ;
    else if (set->flk == HAL_AE_FLK_60)
      aecCfg.EcmFlickerSelect = AEC_EXPOSURE_CONVERSION_FLICKER_120HZ;
    else
      aecCfg.EcmFlickerSelect = AEC_EXPOSURE_CONVERSION_FLICKER_100HZ;

    if (set->meter_mode == HAL_AE_METERING_MODE_CENTER) {
#if 0
      unsigned char gridWeights[25];
      //cifisp_histogram_mode mode = CIFISP_HISTOGRAM_MODE_RGB_COMBINED;

      gridWeights[0] = 0x00;    //weight_00to40
      gridWeights[1] = 0x00;
      gridWeights[2] = 0x01;
      gridWeights[3] = 0x00;
      gridWeights[4] = 0x00;

      gridWeights[5] = 0x00;    //weight_01to41
      gridWeights[6] = 0x02;
      gridWeights[7] = 0x02;
      gridWeights[8] = 0x02;
      gridWeights[9] = 0x00;

      gridWeights[10] = 0x00;    //weight_02to42
      gridWeights[11] = 0x04;
      gridWeights[12] = 0x08;
      gridWeights[13] = 0x04;
      gridWeights[14] = 0x00;

      gridWeights[15] = 0x00;    //weight_03to43
      gridWeights[16] = 0x02;
      gridWeights[17] = 0x00;
      gridWeights[18] = 0x02;
      gridWeights[19] = 0x00;

      gridWeights[20] = 0x00;    //weight_04to44
      gridWeights[21] = 0x00;
      gridWeights[22] = 0x00;
      gridWeights[23] = 0x00;
      gridWeights[24] = 0x00;
      memcpy(aecCfg.GridWeights.uCoeff, gridWeights, sizeof(gridWeights));
#else
      //do nothing ,just use the xml setting
#endif
    } else if (set->meter_mode == HAL_AE_METERING_MODE_AVERAGE) {
      memset(aecCfg.GridWeights.uCoeff, 0x01, sizeof(aecCfg.GridWeights.uCoeff));
    } else if (set->meter_mode == HAL_AE_METERING_MODE_USER) {
      memcpy(aecCfg.GridWeights.uCoeff, set->meter_coeff, sizeof(set->meter_coeff));
    } else
      ALOGE("%s:not support %d metering mode!", __func__, set->meter_mode);

    //set ae bias
    {
      CamCalibAecGlobal_t* pAecGlobal;
      CamCalibDbGetAecGlobal(hCamCalibDb, &pAecGlobal);
	  float SetPoint = pAecGlobal->SetPoint ;
	  if(cfg->LightMode == LIGHT_MODE_NIGHT && pAecGlobal->NightSetPoint != 0)
	  	SetPoint = pAecGlobal->NightSetPoint;

	  if (set->set_point != 0) {
		SetPoint = set->set_point;
	  }

      aecCfg.SetPoint = (SetPoint) +
                        (set->ae_bias / 100.0f) * MAX(10, SetPoint / (1 - pAecGlobal->ClmTolerance / 100.0f) / 10.0f) ;
	  aecCfg.ae_bias = set->ae_bias;
	  aecCfg.LightMode = cfg->LightMode;
    }

    AecStop();
    if ((set->mode != HAL_AE_OPERATION_MODE_MANUAL) &&
        (shd->mode == HAL_AE_OPERATION_MODE_MANUAL)) {
      lastAecResult.regIntegrationTime = -1;
      lastAecResult.regGain = -1;
      lastAecResult.coarse_integration_time = -1;
      lastAecResult.analog_gain_code_global = -1;
    }
    if ((set->mode != HAL_AE_OPERATION_MODE_MANUAL) &&
        !(cfg->aaa_locks & HAL_3A_LOCKS_EXPOSURE)) {
      AecUpdateConfig(&aecCfg);
      AecStart();
      //get init result
      AecRun(NULL,NULL, NULL);
    }

    *shd = *set;

	if(set->aeMaxExpGain != 0.0){
		shd->aeMaxExpGain = set->aeMaxExpGain;
	}

	if(set->aeMaxExpTime != 0.0){
		shd->aeMaxExpTime = set->aeMaxExpTime;
	}
  }

  mLightMode = cfg->LightMode;

  // AF
  struct HAL_AfcCfg* afset;
  struct HAL_AfcCfg* afshd;
  unsigned int isp_ref_width;
  unsigned int isp_ref_height;

  afset = &dCfg.afc_cfg;
  afshd = &dCfgShd.afc_cfg;
  if (afset->mode == HAL_AF_MODE_NOT_SET) {
    goto initDynamic_end;
  }

  if (cfg->sensor_mode.isp_output_width != 0) {
    isp_ref_width = cfg->sensor_mode.isp_output_width;
    isp_ref_height = cfg->sensor_mode.isp_output_height;
  } else {
    isp_ref_width = cfg->sensor_mode.isp_input_width;
    isp_ref_height = cfg->sensor_mode.isp_input_height;
  }

  //init af
  {
    //start af
    if (afset->mode != afshd->mode) {
      if (afshd->mode != HAL_AF_MODE_NOT_SET &&
          afshd->mode != HAL_AF_MODE_FIXED)
        AfStop(hAf);

      if (afset->mode == HAL_AF_MODE_CONTINUOUS_VIDEO ||
        afset->mode == HAL_AF_MODE_CONTINUOUS_PICTURE) {
        result = AfStart(hAf, AFM_FSS_ADAPTIVE_RANGE);
        if (result != RET_SUCCESS) {
          ALOGE("%s: AfStart failure!", __func__);
          goto initDynamic_end;
        }
      } else if (afset->mode == HAL_AF_MODE_AUTO) {
        result = AfOneShot(hAf, AFM_FSS_ADAPTIVE_RANGE);
        if (result != RET_SUCCESS) {
          ALOGE("%s: AfOneShot failure!", __func__);
          goto initDynamic_end;
        }
      } else if (afset->mode == HAL_AF_MODE_FIXED ||
                 afset->mode == HAL_AF_MODE_NOT_SET) {
        result = AfStop(hAf);
        if (result != RET_SUCCESS) {
          ALOGE("%s: AfStop failure!", __func__);
          goto initDynamic_end;
        }
      }

      afshd->mode = afset->mode;
    } else if ((afset->mode == HAL_AF_MODE_AUTO) &&
              (afset->oneshot_trigger == BOOL_TRUE)) {
      afset->oneshot_trigger = BOOL_FALSE;
      result = AfOneShot(hAf, AFM_FSS_ADAPTIVE_RANGE);
      if (result != RET_SUCCESS) {
        ALOGE("%s: AfOneShot failure!", __func__);
        goto initDynamic_end;
      }
    }

    if ((afset->win_num != afshd->win_num) ||
        (memcmp(&afset->win_a, &afshd->win_a, sizeof(HAL_Window))) ||
        (memcmp(&afset->win_b, &afshd->win_b, sizeof(HAL_Window))) ||
        (memcmp(&afset->win_c, &afshd->win_c, sizeof(HAL_Window)))) {
      AfConfig_t afcCfg;

      afcCfg.Window_Num = afset->win_num;
      if (afset->win_num >= 1) {
        mapHalWinToIsp(afset->win_a.right_width - afset->win_a.left_hoff,
                    afset->win_a.bottom_height - afset->win_a.top_voff,
                    afset->win_a.left_hoff,
                    afset->win_a.top_voff,
                    isp_ref_width,
                    isp_ref_height,
                    afcCfg.WindowA.h_size,
                    afcCfg.WindowA.v_size,
                    afcCfg.WindowA.h_offs,
                    afcCfg.WindowA.v_offs);
      }
      if (afset->win_num >= 2) {
        mapHalWinToIsp(afset->win_b.right_width - afset->win_b.left_hoff,
                    afset->win_b.bottom_height - afset->win_b.top_voff,
                    afset->win_b.left_hoff,
                    afset->win_b.top_voff,
                    isp_ref_width,
                    isp_ref_height,
                    afcCfg.WindowB.h_size,
                    afcCfg.WindowB.v_size,
                    afcCfg.WindowB.h_offs,
                    afcCfg.WindowB.v_offs);
      }
      if (afset->win_num >= 3) {
        mapHalWinToIsp(afset->win_c.right_width - afset->win_c.left_hoff,
                    afset->win_c.bottom_height - afset->win_c.top_voff,
                    afset->win_c.left_hoff,
                    afset->win_c.top_voff,
                    isp_ref_width,
                    isp_ref_height,
                    afcCfg.WindowC.h_size,
                    afcCfg.WindowC.v_size,
                    afcCfg.WindowC.h_offs,
                    afcCfg.WindowC.v_offs);
      }

      result = AfReConfigure(hAf, &afcCfg);
      if (result != RET_SUCCESS) {
        ALOGE("%s: AfReConfigure failure! result %d", __func__, result);
        goto initDynamic_end;
      }

      afshd->win_num = afset->win_num;
      afshd->win_a = afset->win_a;
      afshd->win_b = afset->win_b;
      afshd->win_c = afset->win_c;
    }
  }
initDynamic_end:
  return result;
}

RESULT CamIA10Engine::setStatistics(struct CamIA10_Stats* stats) {
  mStats = *stats;
  return RET_SUCCESS;
}

RESULT CamIA10Engine::initAWB() {
  //init awbcfg
  const CamerIcAwbMeasuringConfig_t MeasConfig = {
    .MaxY           = 200U,
    .RefCr_MaxR     = 128U,
    .MinY_MaxG      =  30U,
    .RefCb_MaxB     = 128U,
    .MaxCSum        =  20U,
    .MinC           =  20U
  };
  awbcfg.framerate = 0;
  awbcfg.Mode = AWB_MODE_AUTO;
  awbcfg.idx = 1;
  awbcfg.damp = BOOL_TRUE;
  awbcfg.MeasMode = CAMERIC_ISP_AWB_MEASURING_MODE_YCBCR;

  awbcfg.fStableDeviation  = 0.1f;     // 10 %
  awbcfg.fRestartDeviation = 0.2f;     // 20 %

  awbcfg.MeasMode          = CAMERIC_ISP_AWB_MEASURING_MODE_YCBCR;
  awbcfg.MeasConfig        = MeasConfig;
  awbcfg.Flags             = (AWB_WORKING_FLAG_USE_DAMPING | AWB_WORKING_FLAG_USE_CC_OFFSET);
  awbcfg.hCamCalibDb = hCamCalibDb;

  return RET_SUCCESS;
}

RESULT CamIA10Engine::initAF() {
  //ALOGD("%s:%d", __func__, __LINE__);
  AfInstanceConfig_t afInstance;
  AfConfig_t afcCfg;
  RESULT result = RET_SUCCESS;
  
  result = AfInit(&afInstance);
  if (result != RET_SUCCESS) {
    ALOGE("%s: AfConfigure failure! result %d", __func__, result);
    goto end;
  }
  hAf = afInstance.hAf;

  dCfg.afc_cfg.mode = HAL_AF_MODE_AUTO;
  dCfg.afc_cfg.type.contrast_af = 1;
  dCfg.afc_cfg.type.laser_af = 0;
  dCfg.afc_cfg.type.pdaf = 0;
  dCfg.afc_cfg.win_num = 1;
  dCfg.afc_cfg.win_a.left_hoff = 512;
  dCfg.afc_cfg.win_a.right_width = 1024;
  dCfg.afc_cfg.win_a.top_voff = 512;
  dCfg.afc_cfg.win_a.bottom_height = 1024;

  afcCfg.AfType.contrast_af = 1;
  afcCfg.AfType.laser_af = 0;
  afcCfg.AfType.pdaf = 0;
  afcCfg.Afss = AFM_FSS_ADAPTIVE_RANGE;
  afcCfg.Window_Num = 1;
  mapHalWinToIsp(dCfg.afc_cfg.win_a.right_width,
    dCfg.afc_cfg.win_a.bottom_height,
    dCfg.afc_cfg.win_a.left_hoff,
    dCfg.afc_cfg.win_a.top_voff,
    640,
    480,
    afcCfg.WindowA.h_size,
    afcCfg.WindowA.v_size,
    afcCfg.WindowA.h_offs,
    afcCfg.WindowA.v_offs);
  result = AfConfigure(hAf, &afcCfg);
  if (result != RET_SUCCESS) {
    ALOGE("%s: AfConfigure failure! result %d", __func__, result);
    goto end;
  }

  result = AfReset(hAf, 0, afcCfg.Afss);
  if (result != RET_SUCCESS) {
    ALOGE("%s: AfReset failure! result %d", __func__, result);
    goto end;
  }

  dCfgShd.afc_cfg = dCfg.afc_cfg;
end:
  return result;

}

void CamIA10Engine::mapSensorExpToHal
(
    int sensorGain,
    int sensorInttime,
    float& halGain,
    float& halInttime
) {

  float gainRange[] = {
    1,  1.9375, 16,  16, 1, 0, 15,
    2,  3.875,  8,  0, 1, 16, 31,
    4,  7.75,   4,  -32, 1, 48, 63,
    8,  15.5,   2,  -96, 1, 112, 127
  };

  float* pgainrange = NULL;
  uint32_t size = 0;

  if (aecCfg.pGainRange == NULL || aecCfg.GainRange_size <= 0) {
    pgainrange = gainRange;
	size = sizeof(gainRange) / sizeof(float);
  } else {
    pgainrange = aecCfg.pGainRange;
	size = aecCfg.GainRange_size;
  }

  int *revert_gain_array = (int *)malloc((size/7*2) * sizeof(int));
  if(revert_gain_array == NULL){
	ALOGE("%s: malloc fail", __func__);
	return;
  }
  
  for(uint32_t i=0; i<(size/7); i++){
	revert_gain_array[i*2 + 0] = (int)((pgainrange[i*7 + 0] * pgainrange[i*7 + 2] - pgainrange[i*7 + 3]) / pgainrange[i*7 + 4] + 0.5);
	revert_gain_array[i*2 + 1] = (int)((pgainrange[i*7 + 1] * pgainrange[i*7 + 2] - pgainrange[i*7 + 3]) / pgainrange[i*7 + 4] + 0.5);
  }


  // AG = (((C1 * analog gain - C0) / M0) + 0.5f
  float C1, C0, M0, minReg, maxReg, minHalGain, maxHalGain;
  float ag = sensorGain;
  uint32_t i = 0;
  for(i=0; i<(size/7); i++){
	if (ag >= revert_gain_array[i*2+0] && ag <= revert_gain_array[i*2+1]) {
      C1 = pgainrange[i*7 + 2];
      C0 = pgainrange[i*7 + 3];
      M0 = pgainrange[i*7 + 4];
      minReg = pgainrange[i*7 + 5];
      maxReg = pgainrange[i*7 + 6];
	  #if 0
	  ALOGD("%s: gain(%f) c1:%f c0%f m0:%f min:%f max:%f", 
	  	__func__, ag, C1, C0, M0, minReg, maxReg);
	  #endif
	  break;
  	} 
  }

 if(i > (size/7)){
    ALOGE("GAIN OUT OF RANGE: lasttime-gain: %d-%d", sensorInttime, sensorGain);
    C1 = 16;
    C0 = 0;
    M0 = 1;
    minReg = 16;
    maxReg = 255;
  }

  halGain = ((float)sensorGain * M0 + C0) / C1;
  minHalGain = ((float)minReg * M0 + C0) / C1;
  maxHalGain = ((float)maxReg * M0 + C0) / C1;
  if (halGain < minHalGain)
    halGain = minHalGain;
  if (halGain > maxHalGain)
    halGain = maxHalGain;

#if 0
  halInttime = sensorInttime * aecCfg.PixelPeriodsPerLine /
               (aecCfg.PixelClockFreqMHZ * 1000000);
#else
  float timeC0 = aecCfg.TimeFactor[0];
  float timeC1 = aecCfg.TimeFactor[1];
  float timeC2 = aecCfg.TimeFactor[2];
  float timeC3 = aecCfg.TimeFactor[3];

  halInttime = ((sensorInttime - timeC0 * aecCfg.LinePeriodsPerField - timeC1) / timeC2 - timeC3) *
               aecCfg.PixelPeriodsPerLine / (aecCfg.PixelClockFreqMHZ * 1000000);

  if(revert_gain_array != NULL){
  	free(revert_gain_array);
	revert_gain_array = NULL;
  }
  
#endif

}
void CamIA10Engine::mapHalExpToSensor
(
    float hal_gain,
    float hal_time,
    int& sensor_gain,
    int& sensor_time
) {

  float gainRange[] = {
    1,  1.9375, 16,  16, 1, 0, 15,
    2,  3.875,  8,  0, 1, 16, 31,
    4,  7.75,   4,  -32, 1, 48, 63,
    8,  15.5,   2,  -96, 1, 112, 127
  };

  float* pgainrange;
  uint32_t size = 0;
  
  if (aecCfg.pGainRange == NULL || aecCfg.GainRange_size <= 0) {
    pgainrange = gainRange;
    size = sizeof(gainRange) / sizeof(float);
  } else {
    pgainrange = aecCfg.pGainRange;
    size = aecCfg.GainRange_size;
  }
  
  // AG = (((C1 * analog gain - C0) / M0) + 0.5f
  float C1 = -1, C0, M0, minReg, maxReg;
  float ag = hal_gain;
  unsigned int i;
  
  for (i=0; i<size; i+=7) {
    if (ag >= pgainrange[i] && ag <= pgainrange[i+1]) {
      C1 = pgainrange[i+2];
      C0 = pgainrange[i+3];
      M0 = pgainrange[i+4];
      minReg = pgainrange[i+5];
      maxReg = pgainrange[i+6];
      break;
    }
  }
  
  if (C1 == -1) {
    ALOGE("GAIN OUT OF RANGE: lasttime-gain: %f-%f", hal_time, hal_gain);
    C1 = 16;
    C0 = 0;
    M0 = 1;
    minReg = 16;
    maxReg = 255;
  }
  
  sensor_gain = (int)((C1 * hal_gain - C0) / M0 + 0.5f);
  if (sensor_gain < minReg)
    sensor_gain = minReg;
  if (sensor_gain > maxReg)
    sensor_gain = maxReg;

#if 0
  sensor_time = (int)(hal_time * aecCfg.PixelClockFreqMHZ * 1000000 /
                      aecCfg.PixelPeriodsPerLine + 0.5);
#else
  float timeC0 = aecCfg.TimeFactor[0];
  float timeC1 = aecCfg.TimeFactor[1];
  float timeC2 = aecCfg.TimeFactor[2];
  float timeC3 = aecCfg.TimeFactor[3];
  sensor_time = (int)(timeC0 * aecCfg.LinePeriodsPerField + timeC1 +
                      timeC2 * ((hal_time * aecCfg.PixelClockFreqMHZ * 1000000 / aecCfg.PixelPeriodsPerLine) + timeC3));
#endif

}

RESULT CamIA10Engine::initAEC() {
  RESULT ret = RET_FAILURE;

  CamCalibAecGlobal_t* pAecGlobal;
  ret = CamCalibDbGetAecGlobal(hCamCalibDb, &pAecGlobal);
  if (ret != RET_SUCCESS) {
    ALOGD("fail to get pAecGlobal, ret: %d", ret);
    return ret;
  }

  aecCfg.SetPoint       = pAecGlobal->SetPoint;
  aecCfg.ClmTolerance   = pAecGlobal->ClmTolerance;
  aecCfg.DampOverStill  = pAecGlobal->DampOverStill;
  aecCfg.DampUnderStill = pAecGlobal->DampUnderStill;
  aecCfg.DampOverVideo  = pAecGlobal->DampOverVideo;
  aecCfg.DampUnderVideo = pAecGlobal->DampUnderVideo;
  aecCfg.DampingMode    = AEC_DAMPING_MODE_STILL_IMAGE;
  aecCfg.SemMode        = AEC_SCENE_EVALUATION_DISABLED;
  aecCfg.AOE_Enable     = pAecGlobal->AOE_Enable;
  aecCfg.AOE_Max_point  = pAecGlobal->AOE_Max_point;
  aecCfg.AOE_Min_point  = pAecGlobal->AOE_Min_point;
  aecCfg.AOE_Y_Max_th   = pAecGlobal->AOE_Y_Max_th;
  aecCfg.AOE_Y_Min_th   = pAecGlobal->AOE_Y_Min_th;
  aecCfg.AOE_Step_Dec   = pAecGlobal->AOE_Step_Dec;
  aecCfg.AOE_Step_Inc   = pAecGlobal->AOE_Step_Inc;
  aecCfg.DON_Night_Trigger        = pAecGlobal->DON_Night_Trigger;
  aecCfg.DON_Night_Mode           = pAecGlobal->DON_Night_Mode;
  aecCfg.DON_Day2Night_Fac_th     = pAecGlobal->DON_Day2Night_Fac_th;
  aecCfg.DON_Night2Day_Fac_th     = pAecGlobal->DON_Night2Day_Fac_th;
  aecCfg.DON_Bouncing_th          = pAecGlobal->DON_Bouncing_th;
  aecCfg.meas_mode    = (AecMeasuringMode_t)(pAecGlobal->CamerIcIspExpMeasuringMode);
  aecCfg.FpsSetEnable = pAecGlobal->FpsSetEnable;
  aecCfg.isFpsFix = pAecGlobal->isFpsFix;
  aecCfg.GainRange_size = pAecGlobal->GainRange.array_size;
  aecCfg.pGainRange = pAecGlobal->GainRange.pGainRange;
  aecCfg.IntervalAdjStgy.enable = pAecGlobal->InterAdjustStrategy.enable;
  aecCfg.IntervalAdjStgy.dluma_high_th = pAecGlobal->InterAdjustStrategy.dluma_high_th;
  aecCfg.IntervalAdjStgy.dluma_low_th = pAecGlobal->InterAdjustStrategy.dluma_low_th;
  aecCfg.IntervalAdjStgy.trigger_frame = pAecGlobal->InterAdjustStrategy.trigger_frame;
  

  memcpy(aecCfg.TimeFactor, pAecGlobal->TimeFactor, sizeof(pAecGlobal->TimeFactor));
  memcpy(aecCfg.GridWeights.uCoeff, pAecGlobal->GridWeights.uCoeff, sizeof(pAecGlobal->GridWeights.uCoeff));
  memcpy(aecCfg.NightGridWeights.uCoeff, pAecGlobal->NightGridWeights.uCoeff, sizeof(pAecGlobal->NightGridWeights.uCoeff));
  memcpy(aecCfg.EcmTimeDot.fCoeff, pAecGlobal->EcmTimeDot.fCoeff, sizeof(pAecGlobal->EcmTimeDot.fCoeff));
  memcpy(aecCfg.EcmGainDot.fCoeff, pAecGlobal->EcmGainDot.fCoeff, sizeof(pAecGlobal->EcmGainDot.fCoeff));
  memcpy(aecCfg.FpsFixTimeDot.fCoeff, pAecGlobal->FpsFixTimeDot.fCoeff, sizeof(pAecGlobal->FpsFixTimeDot.fCoeff));
  memcpy(&aecCfg.NLSC_config, &pAecGlobal->NLSC_config, sizeof(pAecGlobal->NLSC_config));
  memcpy(&aecCfg.backLightConf, &pAecGlobal->backLightConf, sizeof(pAecGlobal->backLightConf));
  memcpy(&aecCfg.hist2Hal, &pAecGlobal->hist2Hal, sizeof(pAecGlobal->hist2Hal));

  aecCfg.StepSize = 0;
  aecCfg.HistMode = (CamerIcIspHistMode_t)(pAecGlobal->CamerIcIspHistMode);

  int no_DySetpoint = 0;
  ret = CamCalibDbGetNoOfDySetpoint(hCamCalibDb, pAecGlobal, &no_DySetpoint);
  if (ret != RET_SUCCESS) {
    ALOGD("%s: Getting number of DySetpoint profile from calib database failed (%d)\n",
          __FUNCTION__, ret);
    return (ret);
  }
  
  if(no_DySetpoint){
  	for(int i=0; i<no_DySetpoint && i<LIGHT_MODE_MAX ; i++){
  		CamCalibAecDynamicSetpoint_t* pDySetpointProfile = NULL;
  		ret = CamCalibDbGetDySetpointByIdx(hCamCalibDb, pAecGlobal, i, &pDySetpointProfile);
  		if (ret != RET_SUCCESS) {
  	      ALOGD("%s: Getting idx(%d) DySetpoint profile from calib database failed (%d)\n",
  	            __FUNCTION__, i, ret);
  	      return (ret);
  	    }
  	    DCT_ASSERT(NULL != pDySetpointProfile);
  		aecCfg.pDySetpoint[i] = pDySetpointProfile;
  	}
  }

  int no_ExpSeparate = 0;
  ret = CamCalibDbGetNoOfExpSeparate(hCamCalibDb, pAecGlobal, &no_ExpSeparate);
  if (ret != RET_SUCCESS) {
    ALOGD("%s: Getting number of DySetpoint profile from calib database failed (%d)\n",
          __FUNCTION__, ret);
    return (ret);
  }
  
  if(no_ExpSeparate){
  	for(int i=0; i<no_ExpSeparate && i<LIGHT_MODE_MAX ; i++){
  		CamCalibAecExpSeparate_t* pExpSeparateProfile = NULL;
  		ret = CamCalibDbGetExpSeparateByIdx(hCamCalibDb, pAecGlobal, i, &pExpSeparateProfile);
  		if (ret != RET_SUCCESS) {
  	      ALOGD("%s: Getting idx(%d) ExpSeparate profile from calib database failed (%d)\n",
  	            __FUNCTION__, i, ret);
  	      return (ret);
  	    }
  	    DCT_ASSERT(NULL != pExpSeparateProfile);
  		aecCfg.pExpSeparate[i] = pExpSeparateProfile;
  	}
  }

  aecCfg.LightMode = mLightMode;
  ret = AecInit(&aecCfg);

  return ret;
}

RESULT CamIA10Engine::runAEC() {
  RESULT ret = RET_SUCCESS;
  AwbRunningInputParams_t MeasResult = {0};
  CamerIcAwbMeasure2AwbMeasure(&(mStats.awb), &(MeasResult.MesureResult));

  int lastTime = lastAecResult.regIntegrationTime;
  int lastGain = lastAecResult.regGain;
  TRACE_D(1, "runAEC - cur time-gain: %d-%d, sensor: %d-%d", lastTime, dCfg.sensor_mode.exp_time, lastGain, dCfg.sensor_mode.gain);
  if ((lastTime == -1 && lastGain == -1)
      || (lastTime == dCfg.sensor_mode.exp_time && lastGain == dCfg.sensor_mode.gain)) {
    ret = AecRun(&mStats.aec, &(MeasResult.MesureResult), NULL);
  }
  return ret;
}

RESULT CamIA10Engine::getAECResults(AecResult_t* result) {
  struct HAL_AecCfg* set, *shd;

  set = &dCfg.aec_cfg;
  shd = &dCfgShd.aec_cfg;

  /*if ((set->win.h_offs != shd->win.h_offs) ||
    (set->win.v_offs != shd->win.v_offs) ||
    (set->win.h_size != shd->win.h_size) ||
    (set->win.v_size != shd->win.v_size)) {*/
  //if (true) {
  AecGetResults(result);

  if (lastAecResult.coarse_integration_time != result->coarse_integration_time
      || lastAecResult.analog_gain_code_global != result->analog_gain_code_global
      || lastAecResult.LinePeriodsPerField != result->LinePeriodsPerField) {
    result->actives |= CAMIA10_AEC_MASK;
    lastAecResult.coarse_integration_time = result->coarse_integration_time;
    lastAecResult.analog_gain_code_global = result->analog_gain_code_global;
    lastAecResult.regIntegrationTime = result->regIntegrationTime;
    lastAecResult.regGain = result->regGain;
    lastAecResult.gainFactor = result->gainFactor;
    lastAecResult.gainBias = result->gainBias;
    lastAecResult.LinePeriodsPerField = result->LinePeriodsPerField;
    //*shd = *set;
  }

  result->actives |= CAMIA10_HST_MASK;
  result->meas_mode = aecCfg.meas_mode;
  result->meas_win.h_offs = set->win.left_hoff;
  result->meas_win.v_offs = set->win.top_voff;
  result->meas_win.h_size = set->win.right_width - set->win.left_hoff;
  result->meas_win.v_size = set->win.bottom_height - set->win.top_voff;
  if (lastAecResult.meas_win.h_offs != result->meas_win.h_offs ||
      lastAecResult.meas_win.v_offs != result->meas_win.v_offs ||
      lastAecResult.meas_win.h_size != result->meas_win.h_size ||
      lastAecResult.meas_win.v_size != result->meas_win.v_size) {
    result->actives |= CAMIA10_AEC_MASK;
    lastAecResult.meas_win.h_offs = set->win.left_hoff;
    lastAecResult.meas_win.v_offs = set->win.top_voff;
    lastAecResult.meas_win.h_size = set->win.right_width - set->win.left_hoff;
    lastAecResult.meas_win.v_size = set->win.bottom_height - set->win.top_voff;
  }

  if(lastAecResult.auto_adjust_fps != result->auto_adjust_fps || (result->actives & AEC_AFPS_MASK) ){
	lastAecResult.auto_adjust_fps = result->auto_adjust_fps;
	result->actives |= CAMIA10_AEC_AFPS_MASK;
  }

  lastAecResult.MeanLuma = result->MeanLuma;
  lastAecResult.overHistPercent = result->overHistPercent;
  //ALOGD("set offset: %d-%d, size: %d-%d", set->win.left_hoff, set->win.top_voff, 
  //             set->win.right_width - set->win.left_hoff, set->win.bottom_height - set->win.top_voff);
  //ALOGD("ret offset: %d-%d, size: %d-%d", result->meas_win.h_offs, result->meas_win.v_offs, result->meas_win.h_size, result->meas_win.v_size);
  //ALOGD("sensor_mode size: %d-%d", dCfg.sensor_mode.isp_input_width, dCfg.sensor_mode.isp_input_height);
  //ALOGD("AEC time: %f, gain:%f", result->coarse_integration_time, result->analog_gain_code_global);
  //for (int i = 0; i < 25; i++)
  //	ALOGD("AEC GridWeights[%d] = %d", i, result->GridWeights[i]);

  return RET_SUCCESS;
}

void CamIA10Engine::convertAwbResult2Cameric
(
    AwbRunningOutputResult_t* awbResult,
    CamIA10_AWB_Result_t* awbCamicResult
) {
  if (!awbResult || !awbCamicResult)
    return;
  awbCamicResult->actives = awbResult->validParam;
  AwbGains2CamerIcGains(
      &awbResult->WbGains,
      &awbCamicResult->awbGains
  );

  AwbXtalk2CamerIcXtalk
  (
      &awbResult->CcMatrix,
      &awbCamicResult->CcMatrix
  );

  AwbXTalkOffset2CamerIcXTalkOffset
  (
      &awbResult->CcOffset,
      &awbCamicResult->CcOffset
  );
  awbCamicResult->LscMatrixTable = awbResult->LscMatrixTable;
  awbCamicResult->SectorConfig   = awbResult->SectorConfig;
  awbCamicResult->MeasMode    =  awbResult->MeasMode;
  awbCamicResult->MeasConfig    =  awbResult->MeasConfig;
  awbCamicResult->awbWin      =  awbResult->awbWin;
  awbCamicResult->DoorType    =  awbResult->DoorType;
  strlcpy(awbCamicResult->IllName,awbResult->IllName,sizeof(awbCamicResult->IllName));  //yamasaki
}

void CamIA10Engine::updateAwbConfigs
(
    CamIA10_AWB_Result_t* old,
    CamIA10_AWB_Result_t* newCfg,
    CamIA10_AWB_Result_t* update
) {
  if (!old || !newCfg || !update)
    return;
  //LOGD("%s:%d,awb config actives,old->new:%d->%d \n",__func__,__LINE__,old->actives,newCfg->actives );
  if (newCfg->actives & AWB_RECONFIG_GAINS) {
    if ((newCfg->awbGains.Blue != old->awbGains.Blue)
        || (newCfg->awbGains.Red != old->awbGains.Red)
        || (newCfg->awbGains.GreenR != old->awbGains.GreenR)
        || (newCfg->awbGains.GreenB != old->awbGains.GreenB)) {
      update->actives |= AWB_RECONFIG_GAINS;
      update->awbGains = newCfg->awbGains;
    }
  }

  //TODO:AWB_RECONFIG_CCMATRIX & AWB_RECONFIG_CCOFFSET should
  //update concurrently,now alogorithm ensure this
  if (newCfg->actives & AWB_RECONFIG_CCMATRIX) {
    int i = 0;

    for (; ((i < 9) &&
            (newCfg->CcMatrix.Coeff[i] == old->CcMatrix.Coeff[i]))
         ; i++);
    if (i != 9) {
      update->actives |= AWB_RECONFIG_CCMATRIX;
      update->CcMatrix = newCfg->CcMatrix;
    } else
      update->CcMatrix = newCfg->CcMatrix;
  } else
    update->CcMatrix = newCfg->CcMatrix;

  if (newCfg->actives & AWB_RECONFIG_CCOFFSET) {
    if ((newCfg->CcOffset.Blue) != (old->CcOffset.Blue)
        || (newCfg->CcOffset.Red) != (old->CcOffset.Red)
        || (newCfg->CcOffset.Green) != (old->CcOffset.Green)) {
      update->actives |= AWB_RECONFIG_CCOFFSET;
      update->CcOffset = newCfg->CcOffset;
    } else
      update->CcOffset = newCfg->CcOffset;
  } else
    update->CcOffset = newCfg->CcOffset;

  //TODO:AWB_RECONFIG_LSCMATRIX & AWB_RECONFIG_LSCSECTOR should
  //update concurrently,now alogorithm ensure this
  if (newCfg->actives & AWB_RECONFIG_LSCMATRIX) {
    update->actives |= AWB_RECONFIG_LSCMATRIX;
    update->LscMatrixTable = newCfg->LscMatrixTable;
  } else
    update->LscMatrixTable = newCfg->LscMatrixTable;

  if (newCfg->actives & AWB_RECONFIG_LSCSECTOR) {
    update->actives |= AWB_RECONFIG_LSCSECTOR;
    update->SectorConfig = newCfg->SectorConfig;
  } else
    update->SectorConfig = newCfg->SectorConfig;

  //TODO:AWB_RECONFIG_MEASMODE & AWB_RECONFIG_MEASCFG & AWB_RECONFIG_AWBWIN
  //should update concurrently,now alogorithm ensure this
  if (newCfg->actives & AWB_RECONFIG_MEASMODE) {
    update->actives |= AWB_RECONFIG_MEASMODE;
    update->MeasMode = newCfg->MeasMode;
  } else
    update->MeasMode = newCfg->MeasMode;

  if (newCfg->actives & AWB_RECONFIG_MEASCFG) {
    update->actives |= AWB_RECONFIG_MEASCFG;
    update->MeasConfig = newCfg->MeasConfig;
  } else
    update->MeasConfig = newCfg->MeasConfig;

  if (newCfg->actives & AWB_RECONFIG_AWBWIN) {
    update->actives |= AWB_RECONFIG_AWBWIN;
    update->awbWin = newCfg->awbWin;
  } else
    update->awbWin = newCfg->awbWin;
  /* fixed */
  update->DoorType = newCfg->DoorType;
  strlcpy(update->IllName,newCfg->IllName,sizeof(update->IllName));//yamasaki
  //LOGD("%s:%d,update awb config actives %d \n",__func__,__LINE__,update->actives );
}

RESULT CamIA10Engine::runAWB() {
  //convert statics to awb algorithm
  AwbRunningInputParams_t MeasResult = {0};
  AwbRunningOutputResult_t result = {0};
  CamerIcAwbMeasure2AwbMeasure(&(mStats.awb), &(MeasResult.MesureResult));
  for (int i = 0; i < AWB_HIST_NUM_BINS; i++)
    MeasResult.HistBins[i] = mStats.aec.hist_bins[i];
  MeasResult.fGain = lastAecResult.analog_gain_code_global;
  MeasResult.fIntegrationTime = lastAecResult.coarse_integration_time;
  RESULT ret =  AwbRun(hAwb, &MeasResult, &result);
  if (ret == RET_SUCCESS) {
    //convert result
    memset(&curAwbResult, 0, sizeof(curAwbResult));
    convertAwbResult2Cameric(&result, &curAwbResult);
  }
  curAwbResult.err_code = result.err_code;
  
  return ret;
}

RESULT CamIA10Engine::getAWBResults(CamIA10_AWB_Result_t* result) {
  updateAwbConfigs(&lastAwbResult, &curAwbResult, result);
  lastAwbResult = curAwbResult;
  result->err_code = curAwbResult.err_code;
  return RET_SUCCESS;
}

uint32_t CamIA10Engine::calcAfmTenengradShift(const uint32_t MaxPixelCnt) {
  uint32_t tgrad  = MaxPixelCnt;
  uint32_t tshift = 0U;

  while ( tgrad > (128*128) )
  {
    ++tshift;
    tgrad >>= 1;
  }

  return ( tshift );
}

uint32_t CamIA10Engine::calcAfmLuminanceShift(const uint32_t MaxPixelCnt) {
  uint32_t lgrad  = MaxPixelCnt;
  uint32_t lshift = 0U;

  while ( lgrad > 65793)
  {
    ++lshift;
    lgrad >>= 1;
  }

  return ( lshift );
}

RESULT CamIA10Engine::runAF() {
  RESULT result = RET_SUCCESS;
  struct HAL_AfcCfg* set;
  struct HAL_AfcCfg* shd;

  set = &dCfg.afc_cfg;
  shd = &dCfgShd.afc_cfg;
  if (set->mode == HAL_AF_MODE_NOT_SET) {
    return RET_FAILURE;
  }

  if (shd->mode != HAL_AF_MODE_NOT_SET &&
    shd->mode != HAL_AF_MODE_FIXED) {
    if (hAf != NULL) {
      result = AfProcessFrame(hAf, &mStats.af);
      if ((result != RET_SUCCESS) && (result != RET_CANCELED))
        ALOGE( "%s AfProcessFrame: %d", __func__, result );
    }
  }
  return result;
}

void CamIA10Engine::mapHalWinToIsp(
    uint16_t in_width, uint16_t in_height,
    uint16_t in_hOff, uint16_t in_vOff,
    uint16_t drvWidth, uint16_t drvHeight,
    uint16_t& out_width, uint16_t& out_height,
    uint16_t& out_hOff, uint16_t& out_vOff
) {
  out_hOff = in_hOff * drvWidth / HAL_WIN_REF_WIDTH;
  out_vOff = in_vOff * drvHeight / HAL_WIN_REF_HEIGHT;
  out_width = in_width * drvWidth / HAL_WIN_REF_WIDTH;
  out_height = in_height * drvHeight / HAL_WIN_REF_HEIGHT;
}

RESULT CamIA10Engine::getAFResults(CamIA10_AFC_Result_t* result) {
  uint32_t max_pixel_cnt;
  uint32_t tshift = 0U;
  uint32_t lshift = 0U;
  struct HAL_AfcCfg* set;
  CamerIcAfmOutputResult_t afm_results;
  uint32_t pixel_cnt;
  RESULT ret;

  set = &dCfg.afc_cfg;
  if (set->mode == HAL_AF_MODE_NOT_SET) {
    return RET_FAILURE;
  }

  ret = AfGetResult(hAf, &afm_results);
  if (ret != RET_SUCCESS) {
    return ret;
  }

  result->Window_Num = afm_results.Window_Num;
  if( result->Window_Num >= 1) {
    result->WindowA = afm_results.WindowA;
    pixel_cnt = ((result->WindowA.h_size * result->WindowA.v_size) >> 1);
    max_pixel_cnt = pixel_cnt;
  }

  if( result->Window_Num >= 2) {
    result->WindowB = afm_results.WindowB;
    pixel_cnt = (( result->WindowB.h_size * result->WindowB.v_size ) >> 1);
    if( max_pixel_cnt < pixel_cnt ) {
      max_pixel_cnt = pixel_cnt;
    }
  }

  if( result->Window_Num >= 3) {
    result->WindowC = afm_results.WindowC;
    pixel_cnt  = (( result->WindowC.h_size * result->WindowC.v_size ) >> 1);
    if( max_pixel_cnt < pixel_cnt ) {
      max_pixel_cnt = pixel_cnt;
    }
  }

  tshift = calcAfmTenengradShift(max_pixel_cnt);
  lshift = calcAfmLuminanceShift(max_pixel_cnt);
  result->VarShift = lshift << 16 | tshift;
 /* ALOGD("%s: win num: %d max_pixel_cnt: %d win: (%d,%d,%d,%d), lshift: 0x%x 0x%x\n",
  	__func__, result->Window_Num, max_pixel_cnt,
  	result->WindowA.h_offs,
  	result->WindowA.v_offs,
  	result->WindowA.h_size,
  	result->WindowA.v_size,
  	lshift, result->VarShift);*/
  result->Thres = 0x4U;
  result->LensePos = afm_results.NextLensePos;

  lastAfcResult = *result;

  return RET_SUCCESS;
}

RESULT CamIA10Engine::initADPF() {
  RESULT result = RET_FAILURE;


  return result;
}

RESULT CamIA10Engine::runADPF() {
  RESULT ret = RET_FAILURE;
  
  ret = AdpfRun(hAdpf, lastAecResult.analog_gain_code_global, mLightMode);

  return RET_SUCCESS;
}

RESULT CamIA10Engine::getADPFResults(AdpfResult_t* result) {
  RESULT ret = RET_FAILURE;
  
  ret = AdpfGetResult(hAdpf, result);

  return ret;
}

RESULT CamIA10Engine::initAWDR() {
  RESULT result = RET_FAILURE;


  return result;
}

RESULT CamIA10Engine::runAWDR() {
  RESULT ret = RET_FAILURE;

  ret = AwdrRun(hAwdr, lastAecResult.analog_gain_code_global);

  return RET_SUCCESS;
}

RESULT CamIA10Engine::getAWDRResults(AwdrResult_t* result) {
  RESULT ret = RET_FAILURE;
  ret = AwdrGetResult(hAwdr, result);

  return ret;
}

RESULT CamIA10Engine::runManISP(struct HAL_ISP_cfg_s* manCfg, struct CamIA10_Results* result) {
  RESULT ret = RET_SUCCESS;

  //may override other awb related modules, so need place it first.
  if (manCfg->updated_mask & HAL_ISP_AWB_MEAS_MASK) {
    CamerIcAwbMeasConfig_t awb_meas_result = {BOOL_FALSE, 0};
    awb_meas_result.awb_meas_mode_result = &(result->awb.MeasMode);
    awb_meas_result.awb_meas_result = &(result->awb.MeasConfig);
    awb_meas_result.awb_win = &(result->awb.awbWin);
    ret = cam_ia10_isp_awb_meas_config
          (
              manCfg->enabled[HAL_ISP_AWB_MEAS_ID],
              manCfg->awb_cfg,
              &(awb_meas_result)
          );

    if ((manCfg->awb_cfg) && (!manCfg->enabled[HAL_ISP_AWB_MEAS_ID])
        && (manCfg->awb_cfg->illuIndex >= 0)) {
      //to get default LSC,CC,awb gain settings correspoding to illu
      AwbStop(hAwb);
      awbcfg.Mode = AWB_MODE_MANUAL;
      //ALOGD("%s:illu index %d",__func__,manCfg->awb_cfg->illuIndex);
      awbcfg.idx = manCfg->awb_cfg->illuIndex;
      AwbStart(hAwb, &awbcfg);
      runAWB();
      getAWBResults(&result->awb);
    }

    if (ret != RET_SUCCESS)
      ALOGE("%s:config AWB Meas failed !", __FUNCTION__);
    result->active |= CAMIA10_AWB_MEAS_MASK;
    result->awb_meas_enabled = awb_meas_result.enabled;
  }

  if (manCfg->updated_mask & HAL_ISP_BPC_MASK) {
    ret =  cam_ia10_isp_dpcc_config
           (
               manCfg->enabled[HAL_ISP_BPC_ID],
               manCfg->dpcc_cfg,
               hCamCalibDb,
               dCfg.sensor_mode.isp_input_width,
               dCfg.sensor_mode.isp_input_height,
               &(result->dpcc)
           );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config DPCC failed !", __FUNCTION__);
    result->active |= CAMIA10_BPC_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_BLS_MASK) {
    ret = cam_ia10_isp_bls_config
          (
              manCfg->enabled[HAL_ISP_BLS_ID],
              hCamCalibDb,
              dCfg.sensor_mode.isp_input_width,
              dCfg.sensor_mode.isp_input_height,
              manCfg->bls_cfg,
              &(result->bls)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config BLS failed !", __FUNCTION__);
    result->active |= CAMIA10_BLS_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_SDG_MASK) {
    ret = cam_ia10_isp_sdg_config
          (
              manCfg->enabled[HAL_ISP_SDG_ID],
              manCfg->sdg_cfg,
              &(result->sdg)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config SDG failed !", __FUNCTION__);
    result->active |= CAMIA10_SDG_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_HST_MASK) {
    ret = cam_ia10_isp_hst_config
          (
              manCfg->enabled[HAL_ISP_HST_ID],
              manCfg->hst_cfg,
              dCfg.sensor_mode.isp_input_width,
              dCfg.sensor_mode.isp_input_height,
              &(result->hst)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config hst failed !", __FUNCTION__);
    result->active |= CAMIA10_HST_MASK;
    result->aec.actives |= CAMIA10_HST_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_LSC_MASK) {
    CamerIcLscConfig_t lsc_result = {BOOL_FALSE, 0};
    lsc_result.lsc_result = &(result->awb.LscMatrixTable);
    lsc_result.lsc_seg_result = &(result->awb.SectorConfig);
    ret = cam_ia10_isp_lsc_config
          (
              manCfg->enabled[HAL_ISP_LSC_ID],
              manCfg->lsc_cfg,
              &(lsc_result)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config LSC failed !", __FUNCTION__);
    result->active |= CAMIA10_LSC_MASK;
    result->lsc_enabled = lsc_result.enabled;
  }

  if (manCfg->updated_mask & HAL_ISP_AWB_GAIN_MASK) {
    CameraIcAwbGainConfig_t awb_result = {BOOL_FALSE, 0};
    awb_result.awb_gain_result = &(result->awb.awbGains);
    ret = cam_ia10_isp_awb_gain_config
          (
              manCfg->enabled[HAL_ISP_AWB_GAIN_ID],
              manCfg->awb_gain_cfg,
              &(awb_result)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config AWB Gain failed !", __FUNCTION__);
    result->active |= CAMIA10_AWB_GAIN_MASK;
    result->awb_gains_enabled = awb_result.enabled;
  }

  if (manCfg->updated_mask & HAL_ISP_FLT_MASK) {
    ret = cam_ia10_isp_flt_config
          (
          	  hCamCalibDb,
              manCfg->enabled[HAL_ISP_FLT_ID],
              manCfg->flt_cfg,
              dCfg.sensor_mode.isp_input_width,
              dCfg.sensor_mode.isp_input_height,
              &(result->flt)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config FLT failed !", __FUNCTION__);
    result->active |= CAMIA10_FLT_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_BDM_MASK) {
    ret = cam_ia10_isp_bdm_config
          (
              manCfg->enabled[HAL_ISP_BDM_ID],
              manCfg->bdm_cfg,
              &(result->bdm)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config BDM failed !", __FUNCTION__);
    result->active |= CAMIA10_BDM_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_CTK_MASK) {
    CameraIcCtkConfig_t ctk_result = {BOOL_FALSE, 0};
    ctk_result.ctk_matrix_result = &(result->awb.CcMatrix);
    ctk_result.ctk_offset_result = &(result->awb.CcOffset);
    ret = cam_ia10_isp_ctk_config
          (
              manCfg->enabled[HAL_ISP_CTK_ID],
              manCfg->ctk_cfg,
              &(ctk_result)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config CTK failed !", __FUNCTION__);
    result->active |= CAMIA10_CTK_MASK;
    result->ctk_enabled = ctk_result.enabled;
  }


  if (manCfg->updated_mask & HAL_ISP_CPROC_MASK) {
    ret = cam_ia10_isp_cproc_config
          (
              hCamCalibDb,
              manCfg->enabled[HAL_ISP_CPROC_ID],
              manCfg->cproc_cfg,
              &(result->cproc)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config CPROC failed !", __FUNCTION__);
    result->active |= CAMIA10_CPROC_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_IE_MASK) {
    ret = cam_ia10_isp_ie_config
          (
              manCfg->enabled[HAL_ISP_IE_ID],
              manCfg->ie_cfg,
              &(result->ie)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config IE failed !", __FUNCTION__);
    result->active |= CAMIA10_IE_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_AEC_MASK) {
    CameraIcAecConfig_t aec_result = {BOOL_FALSE, 0};
    aec_result.aec_meas_mode = (int*)(&(result->aec.meas_mode));
    aec_result.meas_win = &(result->aec.meas_win);
    ret = cam_ia10_isp_aec_config
          (
              manCfg->enabled[HAL_ISP_AEC_ID],
              manCfg->aec_cfg,
              &(aec_result)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config AEC Meas failed !", __FUNCTION__);
    result->active |= CAMIA10_AEC_MASK;
    result->aec_enabled = aec_result.enabled;

    if ((manCfg->aec_cfg) && (!aec_result.enabled) &&
        ((manCfg->aec_cfg->exp_time > 0.01) || (manCfg->aec_cfg->exp_gain > 0.01))) {
      mapHalExpToSensor
      (
          manCfg->aec_cfg->exp_gain,
          manCfg->aec_cfg->exp_time,
          result->aec.regGain,
          result->aec.regIntegrationTime
      );
      //FIXME: for some reason, kernel report error manual ae time and gain values to HAL
      //if aec is disabled , so here is just a workaround
      result->aec_enabled = BOOL_TRUE;
      result->aec.actives |= CAMIA10_AEC_MASK;
    }

  }

  /*TODOS*/
  if (manCfg->updated_mask & HAL_ISP_WDR_MASK) {
    ret = cam_ia10_isp_wdr_config
          (
              hCamCalibDb,
              manCfg->enabled[HAL_ISP_WDR_ID],
              manCfg->wdr_cfg,
              &(result->wdr)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config WDR failed !", __FUNCTION__);
    result->active |= CAMIA10_WDR_MASK;
    if (manCfg->enabled[HAL_ISP_WDR_ID] != HAL_ISP_ACTIVE_DEFAULT) {
       // stop awdr 
       awdrCfg.mode = AWDR_MODE_CONTROL_BY_MANUAL;
	   //mWdrEnabledState = BOOL_FALSE;
    } else {
      awdrCfg.mode = AWDR_MODE_CONTROL_BY_GAIN;
	   //mWdrEnabledState = BOOL_TRUE;
    } 

    mWdrEnabledState = result->wdr.enabled;
  }


  if (manCfg->updated_mask & HAL_ISP_GOC_MASK) {
    ret = cam_ia10_isp_goc_config
          (
              hCamCalibDb,
              manCfg->enabled[HAL_ISP_GOC_ID],
              manCfg->goc_cfg,
              &(result->goc),
              mWdrEnabledState
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config GOC failed !", __FUNCTION__);
    result->active |= CAMIA10_GOC_MASK;
  }

  if (manCfg->updated_mask & HAL_ISP_DPF_MASK) {
    CameraIcDpfConfig_t dpfConfig;
    ret = cam_ia10_isp_dpf_config
          (
              manCfg->enabled[HAL_ISP_DPF_ID],
              manCfg->dpf_cfg,
              &(dpfConfig)
          );

    if (ret != RET_SUCCESS)
      ALOGE("%s:config DPF failed !", __FUNCTION__);
    result->active |= CAMIA10_DPF_MASK;
    result->adpf_enabled = dpfConfig.enabled;
  }

  if (manCfg->updated_mask & HAL_ISP_DPF_STRENGTH_MASK) {
    CameraIcDpfStrengthConfig_t dpfStrengConfig;
    ret = cam_ia10_isp_dpf_strength_config
          (
              manCfg->enabled[HAL_ISP_DPF_STRENGTH_ID],
              manCfg->dpf_strength_cfg,
              &(dpfStrengConfig)
          );

    result->adpf.DynInvStrength.WeightB = dpfStrengConfig.b;
    result->adpf.DynInvStrength.WeightG = dpfStrengConfig.g;
    result->adpf.DynInvStrength.WeightR = dpfStrengConfig.r;
    if (ret != RET_SUCCESS)
      ALOGE("%s:config DPF strength failed !", __FUNCTION__);
    result->active |= CAMIA10_DPF_STRENGTH_MASK;
    result->adpf_strength_enabled = dpfStrengConfig.enabled;
  }

  if (manCfg->updated_mask & HAL_ISP_AFC_MASK) {

  }

  if (manCfg->updated_mask & HAL_ISP_3DNR_MASK) {
    if (manCfg->enabled[HAL_ISP_3DNR_ID] == HAL_ISP_ACTIVE_FALSE) {
      result->adpf.Dsp3DnrResult.Enable = 0;
      result->adpf.Dsp3DnrResult.luma_sp_nr_en = 0;
      result->adpf.Dsp3DnrResult.luma_te_nr_en = 0;
      result->adpf.Dsp3DnrResult.chrm_sp_nr_en = 0;
      result->adpf.Dsp3DnrResult.chrm_te_nr_en = 0;
      result->adpf.Dsp3DnrResult.shp_en = 0;
      result->active |= CAMIA10_DSP_3DNR_MASK;
    } else if (manCfg->enabled[HAL_ISP_3DNR_ID] == HAL_ISP_ACTIVE_SETTING) {
      result->adpf.Dsp3DnrResult.luma_sp_nr_en = manCfg->dsp_3dnr_cfg->level_cfg.luma_sp_nr_en;
      result->adpf.Dsp3DnrResult.luma_te_nr_en = manCfg->dsp_3dnr_cfg->level_cfg.luma_te_nr_en;
      result->adpf.Dsp3DnrResult.chrm_sp_nr_en = manCfg->dsp_3dnr_cfg->level_cfg.chrm_sp_nr_en;
      result->adpf.Dsp3DnrResult.chrm_te_nr_en = manCfg->dsp_3dnr_cfg->level_cfg.chrm_te_nr_en;
      result->adpf.Dsp3DnrResult.shp_en = manCfg->dsp_3dnr_cfg->level_cfg.shp_en;
      result->adpf.Dsp3DnrResult.luma_sp_nr_level = manCfg->dsp_3dnr_cfg->level_cfg.luma_sp_nr_level;
      result->adpf.Dsp3DnrResult.luma_te_nr_level = manCfg->dsp_3dnr_cfg->level_cfg.luma_te_nr_level;
      result->adpf.Dsp3DnrResult.chrm_sp_nr_level = manCfg->dsp_3dnr_cfg->level_cfg.chrm_sp_nr_level;
      result->adpf.Dsp3DnrResult.chrm_te_nr_level = manCfg->dsp_3dnr_cfg->level_cfg.chrm_te_nr_level;
      result->adpf.Dsp3DnrResult.shp_level = manCfg->dsp_3dnr_cfg->level_cfg.shp_level;

      result->adpf.Dsp3DnrResult.noise_coef_num = manCfg->dsp_3dnr_cfg->param_cfg.noise_coef_num;
      result->adpf.Dsp3DnrResult.noise_coef_den = manCfg->dsp_3dnr_cfg->param_cfg.noise_coef_den;
      result->adpf.Dsp3DnrResult.luma_default = manCfg->dsp_3dnr_cfg->param_cfg.luma_default;
      result->adpf.Dsp3DnrResult.luma_sp_rad = manCfg->dsp_3dnr_cfg->param_cfg.luma_sp_rad;
      result->adpf.Dsp3DnrResult.luma_te_max_bi_num = manCfg->dsp_3dnr_cfg->param_cfg.luma_te_max_bi_num;
      result->adpf.Dsp3DnrResult.luma_w0 = manCfg->dsp_3dnr_cfg->param_cfg.luma_w0;
      result->adpf.Dsp3DnrResult.luma_w1 = manCfg->dsp_3dnr_cfg->param_cfg.luma_w1;
      result->adpf.Dsp3DnrResult.luma_w2 = manCfg->dsp_3dnr_cfg->param_cfg.luma_w2;
      result->adpf.Dsp3DnrResult.luma_w3 = manCfg->dsp_3dnr_cfg->param_cfg.luma_w3;
      result->adpf.Dsp3DnrResult.luma_w4 = manCfg->dsp_3dnr_cfg->param_cfg.luma_w4;
      result->adpf.Dsp3DnrResult.chrm_default = manCfg->dsp_3dnr_cfg->param_cfg.chrm_default;
      result->adpf.Dsp3DnrResult.chrm_sp_rad = manCfg->dsp_3dnr_cfg->param_cfg.chrm_sp_rad;
      result->adpf.Dsp3DnrResult.chrm_te_max_bi_num = manCfg->dsp_3dnr_cfg->param_cfg.chrm_te_max_bi_num;
      result->adpf.Dsp3DnrResult.chrm_w0 = manCfg->dsp_3dnr_cfg->param_cfg.chrm_w0;
      result->adpf.Dsp3DnrResult.chrm_w1 = manCfg->dsp_3dnr_cfg->param_cfg.chrm_w1;
      result->adpf.Dsp3DnrResult.chrm_w2 = manCfg->dsp_3dnr_cfg->param_cfg.chrm_w2;
      result->adpf.Dsp3DnrResult.chrm_w3 = manCfg->dsp_3dnr_cfg->param_cfg.chrm_w3;
      result->adpf.Dsp3DnrResult.chrm_w4 = manCfg->dsp_3dnr_cfg->param_cfg.chrm_w4;
      result->adpf.Dsp3DnrResult.shp_default = manCfg->dsp_3dnr_cfg->param_cfg.shp_default;
      result->adpf.Dsp3DnrResult.src_shp_w0 = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_w0;
      result->adpf.Dsp3DnrResult.src_shp_w1 = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_w1;
      result->adpf.Dsp3DnrResult.src_shp_w2 = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_w2;
      result->adpf.Dsp3DnrResult.src_shp_w3 = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_w3;
      result->adpf.Dsp3DnrResult.src_shp_w4 = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_w4;
      result->adpf.Dsp3DnrResult.src_shp_thr = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_thr;
      result->adpf.Dsp3DnrResult.src_shp_div = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_div;
      result->adpf.Dsp3DnrResult.src_shp_l = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_l;
      result->adpf.Dsp3DnrResult.src_shp_c = manCfg->dsp_3dnr_cfg->param_cfg.src_shp_c;
      result->adpf.Dsp3DnrResult.Enable = 1;
      result->active |= CAMIA10_DSP_3DNR_MASK;
    }
  }

  return ret;
}

RESULT CamIA10Engine::getWdrConfig(struct HAL_ISP_wdr_cfg_s* wdr_cfg,
	enum HAL_ISP_WDR_MODE_e wdr_mode) {
  if (hCamCalibDb == NULL)
    return RET_FAILURE;
  else
    return cam_ia10_isp_get_wdr_config(hCamCalibDb, wdr_cfg, wdr_mode);
}

shared_ptr<CamIA10EngineItf> getCamIA10EngineItf(void) {
  return shared_ptr<CamIA10EngineItf>(new CamIA10Engine());
}
