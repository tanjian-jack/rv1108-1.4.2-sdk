/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <math.h>
#include "edog_common.h"
#include "libedog.h"

static struct libedog libedog_info;

/* degrees converted to radians */
static double radians(double d)
{
	return d * PI / 180;
}

/* radians converted to degrees */
static double degrees(double d)
{
	return d * (180 / PI);
}

static int get_point_index()
{
	if (libedog_info.point_index)
		return libedog_info.point_index - 1;
	else
		return RECORD_MAX_LEN - 1;
}

static void set_point_index()
{
	libedog_info.point_index++;
	if (libedog_info.point_index >= RECORD_MAX_LEN)
		libedog_info.point_index = 0;
}

static struct dbf_record_info *get_fast_record(void)
{
	return libedog_info.list;
}

static void add_record(struct dbf_record_info *record)
{
	struct dbf_record_info *record_cur;

#ifdef LIBEDOG_DATA1
	/* security monitoring electronic eyes do not play voice */
	if (record->type == SECURITY_MONITOR_TYPE)
		return;
#elif LIBEDOG_DATA3
	if (record->type == 6)
		return;
#endif

	record_cur = get_fast_record();
	if (record_cur == NULL) {
		libedog_info.list = record;
	} else {
		while (record_cur->next) {
			record_cur = record_cur->next;
		}
		record->pre = record_cur;
		record_cur->next = record;
	}

	libedog_info.list_count++;
}

static void bounding_coordinates(struct lat_lon_range *range, double lat,
				 double lon, double distance)
{
	double rad_dist;
	double lat_min, lat_max;
	double lon_min, lon_max;

	lat = radians(lat);
	lon = radians(lon);

	/* calculate (X km)'s radian on the circumference of the earth */
	rad_dist = distance / EARTH_RADIUS;

	/* calculate latitude range */
	lat_min = lat - rad_dist;
	lat_max = lat + rad_dist;

	/* latitude is between -90 degrees and 90 degrees */
	if (lat_min > -PI / 2 && lat_max < PI / 2) {
		/* calculate  longitude range */
		double lon_t = asin(sin(rad_dist) / cos(lat));
		lon_min = lon - lon_t;

		/* longitude is between -180 degrees and 180 degrees */
		if (lon_min < -PI)
			lon_min += 2 * PI;

		lon_max = lon + lon_t;

		if (lon_max > PI)
			lon_max -= 2 * PI;
	} else {
		lat_min = max(lat_min, -PI / 2);
		lat_max = min(lat_max, PI / 2);
		lon_min = -PI;
		lon_max = PI;
	}

	range->lat_min = degrees(lat_min);
	range->lat_max = degrees(lat_max);
	range->lon_min = degrees(lon_min);
	range->lon_max = degrees(lon_max);

	EDOG_DBG("lat_min: %lf, lat_max: %lf\n", range->lat_min, range->lat_max);
	EDOG_DBG("lon_min: %lf, lon_max: %lf\n", range->lon_min, range->lon_max);
}

static double calculate_azimuth(double aLat, double aLon, double bLat,
				double bLon)
{
	double azimuth;
	double averageLat = (aLat + bLat) / 2;

	EDOG_DBG("%s\n", __func__);
	EDOG_DBG("aLat: %lf, aLon: %lf\n", aLat, aLon);
	EDOG_DBG("bLat: %lf, bLon: %lf\n", bLat, bLon);

	if ((bLat - aLat) == 0) {
		azimuth = DEGREE_90;
	} else {
		azimuth = degrees(atan((bLon - aLon) * cos(radians(averageLat))
				       / (bLat - aLat)));
	}

	if (bLat > aLat)
		azimuth = azimuth + DEGREE_180;

	if (azimuth < 0)
		azimuth = DEGREE_360 + azimuth;

	EDOG_DBG("azimuth: %lf\n", azimuth);

	if (azimuth < 0 || azimuth > DEGREE_360)
		EDOG_DBG("calculate azimuth error: %lf\n", azimuth);

	return azimuth;
}

/* speed: km/h, return distance: km */
static double get_broadcast_distance(float speed)
{
	double distance = DIST_0M;

	if (speed < SPEED_60)
		distance = DIST_300M;
	else if (speed < SPEED_90)
		distance = DIST_500M;
	else
		distance = DIST_1KM;

	EDOG_DBG("broadcast distance: %lf\n", distance);

	return distance;
}

static void set_point(struct location_info *src_point,
		      struct location_info *dst_point)
{
	if (src_point == NULL)
		memset(dst_point, 0, sizeof(struct location_info));
	else
		memcpy(dst_point, src_point, sizeof(struct location_info));
}

/* return km */
static double two_points_distance(double aLat, double aLon, double bLat,
				  double bLon)
{
	double distance;

	//EDOG_DBG("%s\n", __func__);
	//EDOG_DBG("aLat: %lf, aLon: %lf\n", aLat, aLon);
	//EDOG_DBG("bLat: %lf, bLon: %lf\n", bLat, bLon);

	aLat = radians(aLat);
	aLon = radians(aLon);
	bLat = radians(bLat);
	bLon = radians(bLon);

	distance = acos(sin(aLat) * sin(bLat) + cos(aLat) * cos(bLat)
			* cos(bLon - aLon)) * EARTH_RADIUS;

	EDOG_DBG("%s, distance: %lf\n", __func__, distance);

	return distance;
}

static void get_voice_dir(char *dir, double distance)
{
	memset(dir, 0, VOICE_DIR_LEN);

	if (distance >= LOW_100M && distance <= HIGH_100M)       //80m ~ 120m
		memcpy(dir, VOICE_DIR_100, VOICE_DIR_LEN);
	else if (distance >= LOW_200M && distance <= HIGH_200M)  //180m ~ 220m
		memcpy(dir, VOICE_DIR_200, VOICE_DIR_LEN);
	else if (distance >= LOW_300M && distance <= HIGH_300M)  //280m ~ 320m
		memcpy(dir, VOICE_DIR_300, VOICE_DIR_LEN);
	else if (distance >= LOW_500M && distance <= HIGH_500M)  //480m ~ 520m
		memcpy(dir, VOICE_DIR_500, VOICE_DIR_LEN);
	else if (distance >= LOW_1KM && distance <= HIGH_1KM)    //950m ~ 1km
		memcpy(dir, VOICE_DIR_1000, VOICE_DIR_LEN);

	EDOG_DBG("voice_dir: %s\n", dir);
}

static int distance_compare(double distance, double pre_distance)
{
	EDOG_DBG("%s, distance: %lf, pre_distance: %lf\n", __func__,
		 distance, pre_distance);

	if (distance < pre_distance)
		return 0;
	else
		return -1;
}

static double get_azimuth_offset(double distance)
{
	double offset_azimuth = INVALID_VALUES;

	if (distance >= DIST_0M && distance <= DIST_100M)
		offset_azimuth = DEGREE_100M;
	else if (distance > DIST_100M && distance <= DIST_150M)
		offset_azimuth = DEGREE_150M;
	else if (distance > DIST_150M && distance <= DIST_200M)
		offset_azimuth = DEGREE_200M;
	else if (distance > DIST_200M && distance <= DIST_250M)
		offset_azimuth = DEGREE_250M;
	else if (distance > DIST_250M && distance <= DIST_300M)
		offset_azimuth = DEGREE_300M;
	else if (distance > DIST_300M && distance <= DIST_350M)
		offset_azimuth = DEGREE_350M;
	else if (distance > DIST_350M && distance <= DIST_400M)
		offset_azimuth = DEGREE_400M;
	else if (distance > DIST_400M && distance <= DIST_450M)
		offset_azimuth = DEGREE_450M;
	else if (distance > DIST_450M && distance <= DIST_500M)
		offset_azimuth = DEGREE_500M;
	else if (distance > DIST_500M && distance <= DIST_550M)
		offset_azimuth = DEGREE_550M;
	else if (distance > DIST_550M && distance <= DIST_600M)
		offset_azimuth = DEGREE_600M;
	else if (distance > DIST_600M && distance <= DIST_650M)
		offset_azimuth = DEGREE_650M;
	else if (distance > DIST_650M && distance <= DIST_700M)
		offset_azimuth = DEGREE_700M;
	else if (distance > DIST_700M && distance <= DIST_750M)
		offset_azimuth = DEGREE_750M;
	else if (distance > DIST_750M && distance <= DIST_800M)
		offset_azimuth = DEGREE_800M;
	else if (distance > DIST_800M && distance <= DIST_850M)
		offset_azimuth = DEGREE_850M;
	else if (distance > DIST_850M && distance <= DIST_900M)
		offset_azimuth = DEGREE_900M;
	else if (distance > DIST_900M && distance <= DIST_950M)
		offset_azimuth = DEGREE_950M;
	else if (distance > DIST_950M && distance <= DIST_1KM)
		offset_azimuth = DEGREE_1KM;

	return offset_azimuth;
}

static int record_search(double cur_move_azimuth, double distance,
			 double azimuth, double pre_distance)
{
	double offset_azimuth, total_azimuth;

	EDOG_DBG("%s, distance: %lf, pre_distance: %lf\n",
		 __func__, distance, pre_distance);

	offset_azimuth = get_azimuth_offset(distance);

	if (offset_azimuth != INVALID_VALUES) {
		total_azimuth = cur_move_azimuth + offset_azimuth;
		EDOG_DBG("total_azimuth: %lf\n", total_azimuth);

		if (total_azimuth > DEGREE_360) {
			total_azimuth -= DEGREE_360;
			if ((azimuth >= cur_move_azimuth && azimuth <= DEGREE_360)
			    || (azimuth >= 0 && azimuth <= total_azimuth)) {
				return distance_compare(distance, pre_distance);
			}
		} else {
			if (azimuth >= cur_move_azimuth && azimuth <= total_azimuth) {
				return distance_compare(distance, pre_distance);
			}
		}
	}

	return -1;
}

static int car_turning(double pre_azimuth, double cur_azimuth)
{
	int ret = -1;
	double diff_azimuth;

	if (pre_azimuth == 0 || cur_azimuth == 0)
		return ret;

	/* three consecutive points azimuth difference is more than DEGREE_TURN,
	   then that the car turning*/
	diff_azimuth = fabs(cur_azimuth - pre_azimuth);
	if (diff_azimuth >= libedog_info.degree_trun)
		ret = 0;

	return ret;
}

static void set_record_list(struct dbf_record_info *info,
			    struct lat_lon_range range,
			    int low, int high, int mid)
{
	int i;

	EDOG_DBG("%s\n", __func__);

	for (i = mid; i >= low; i--) {
		if (info[i].x <= range.lon_max &&
		    info[i].x >= range.lon_min) {
			if (info[i].y <= range.lat_max &&
			    info[i].y >= range.lat_min) {
				add_record(&info[i]);
			}
		} else {
			break;
		}
	}

	for (i = (mid + 1); i <= high; i++) {
		if (info[i].x <= range.lon_max &&
		    info[i].x >= range.lon_min) {
			if (info[i].y <= range.lat_max &&
			    info[i].y >= range.lat_min) {
				add_record(&info[i]);
			}
		} else {
			break;
		}
	}
}

static void binary_search(struct dbf_record_info *info,
			  struct lat_lon_range range, int low, int high)
{
	int mid = low + (high - low) / 2;

	if ((low + 1) >= high)
		return;

	if (info[mid].x <= range.lon_max &&
	    info[mid].x >= range.lon_min) {
		set_record_list(info, range, low, high, mid);
		return;
	} else if (info[mid].x > range.lon_max) {
		binary_search(info, range, low, mid - 1);
	} else if (info[mid].x < range.lon_min) {
		binary_search(info, range, mid + 1, high);
	}
}

/* record the electronic eyes that has been broadcast */
static void set_broadcast_points(struct dbf_record_info *info,
				 struct location_info *cur_point)
{
	int i;
	struct location_info point;

	point.search_distance = cur_point->search_distance;
	point.move_azimuth = cur_point->move_azimuth;
	point.latitude = info->y;
	point.longitude = info->x;
	point.heading = info->heading;

	for (i = 0; i < RECORD_MAX_LEN; i++) {
		if (point.latitude == libedog_info.points[i].latitude &&
		    point.longitude == libedog_info.points[i].longitude) {
			set_point((struct location_info *)NULL,
				  &libedog_info.points[libedog_info.point_index]);
			break;
		}
	}

	set_point(&point, &libedog_info.points[libedog_info.point_index]);
	set_point_index();
}

/* to find whether the electronic eye has been broadcast */
static int search_points(double lat, double lon, double cur_move_azimuth)
{
	int i;

	EDOG_DBG("%s\n", __func__);

	for (i = 0; i < RECORD_MAX_LEN; i++) {
		if (libedog_info.points[i].latitude == lat &&
		    libedog_info.points[i].longitude == lon) {
			double azimuth_diff = fabs(cur_move_azimuth -
						   libedog_info.points[i].move_azimuth);

			/* If the azimuth difference between 170 to 190,
			   the car is considered to be turning around,
			   and the electronic eye needs to be broadcast*/
			if (azimuth_diff >= DEGREE_170 && azimuth_diff <= DEGREE_190) {
				EDOG_DBG("170 ~ 190, turning around\r\n");
				return 0;
			} else {
				EDOG_DBG("be broadcasted\r\n");
				return -1;
			}
		}
	}

	return 0;
}

#ifdef LIBEDOG_DATA1
static void play_voice_data1(char *directory, char *filename)
{
	EDOG_DBG("%s\n", __func__);

	char path[PATH_LEN];

	if (directory)
		snprintf(path, PATH_LEN, "%s/%s/%s.%s", libedog_info.voice_path, directory,
			 filename, "wav");
	else
		snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, filename, "wav");

	EDOG_DBG("path: %s\n", path);

	libedog_info.audio_play(path);
}

#elif LIBEDOG_DATA2
static void play_voice_data2(int type, char *filename)
{
	EDOG_DBG("%s\n", __func__);

	char path[PATH_LEN];

	if (filename) {
		snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, filename, "wav");
	} else {
		switch (type) {
			/* Group I */
		case 1:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_1_VOICE,
				 "wav");
			break;
		case 3:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_3_VOICE,
				 "wav");
			break;
		case 11:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_11_VOICE,
				 "wav");
			break;
		case 20:
		case 33:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_20_33_VOICE,
				 "wav");
			break;
		case 30:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_30_VOICE,
				 "wav");
			break;
		case 32:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_32_VOICE,
				 "wav");
			break;

			/* Group II */
		case 5:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_5_VOICE,
				 "wav");
			break;

			/* Group III */
		case 103:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_103_VOICE,
				 "wav");
			break;
		case 104:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_104_VOICE,
				 "wav");
			break;
		case 105:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_105_VOICE,
				 "wav");
			break;

			/* Group IV */
		case 101:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_101_VOICE,
				 "wav");
			break;

			/* Group V */
		case 102:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_102_VOICE,
				 "wav");
			break;

			/* Group VI */
		case 100:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_100_VOICE,
				 "wav");
			break;

			/* Group VII */
		case 50:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_50_VOICE,
				 "wav");
			break;
		case 55:
			snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, TYPE_55_VOICE,
				 "wav");
			break;
		}

	}

	EDOG_DBG("path: %s\n", path);

	libedog_info.audio_play(path);
}

#elif LIBEDOG_DATA3
static void play_voice_data3(char *directory, int voice_type, char *filename)
{
	EDOG_DBG("%s\n", __func__);

	char e_type[1] = {0};
	char speed[3] = {0};
	char string[VOICE_TYPE_LEN] = {0};
	char voicename[VOICE_NAME_LEN + 1] = {0};
	char path[PATH_LEN] = {0};

	EDOG_DBG("voice_type: %d\n", voice_type);

	if (!voice_type)
		return;

	memset(voicename, 0, VOICE_NAME_LEN + 1);

	if (filename) {
		snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, filename, "wav");
	} else {
		switch (voice_type) {
		case TYPE_1000:
			memcpy(voicename, TYPE_1000_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4000:
			memcpy(voicename, TYPE_4000_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4030:
			memcpy(voicename, TYPE_4030_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4040:
			memcpy(voicename, TYPE_4040_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4050:
			memcpy(voicename, TYPE_4050_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4060:
			memcpy(voicename, TYPE_4060_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4070:
			memcpy(voicename, TYPE_4070_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_4080:
			memcpy(voicename, TYPE_4080_VOICE, VOICE_NAME_LEN);
			break;

		case TYPE_8000:
			memcpy(voicename, TYPE_8000_VOICE, VOICE_NAME_LEN);
			break;

		default:
			sprintf(string, "%d", voice_type);
			memcpy(e_type, string, 1);
			memcpy(speed, string + 1, 3);

			strcat(voicename, e_type);
			strcat(voicename, directory);
			strcat(voicename, speed);
			break;
		}

		snprintf(path, PATH_LEN, "%s/%s.%s", libedog_info.voice_path, voicename, "wav");
	}

	EDOG_DBG("path: %s\n", path);

	libedog_info.audio_play(path);
}
#endif

static int get_heading(int heading, double lat, double lon)
{
	int direction;
	double move_azimuth, neg_move_azimuth;

	move_azimuth = calculate_azimuth(libedog_info.pre_point.latitude,
					 libedog_info.pre_point.longitude, lat, lon);

	neg_move_azimuth = calculate_azimuth(lat, lon, libedog_info.pre_point.latitude,
					     libedog_info.pre_point.longitude);

	EDOG_DBG("move_azimuth: %lf\n", move_azimuth);
	EDOG_DBG("neg_move_azimuth: %lf\n", neg_move_azimuth);

	if (fabs(heading - neg_move_azimuth) < 90
	    || fabs(heading - (neg_move_azimuth + 360)) < 90
	    || fabs((heading + 360) - neg_move_azimuth) < 90) {
		direction = heading;
	} else if (fabs(heading - move_azimuth) < 90
		   || fabs(heading - (move_azimuth + 360)) < 90
		   || fabs((heading + 360) - move_azimuth) < 90) {
		direction = heading + 180;
		if (direction >= 360)
			direction -= 360;
	} else {
		direction = -1;
	}

	return direction;
}

static void broadcast_record_search(struct location_info *cur_point)
{
	int ret;
	double azimuth, distance;
	double pre_distance = INVALID_VALUES;
	struct dbf_record_info *record_cur;
	struct dbf_record_info info;

	memset(&info, 0, sizeof(struct dbf_record_info));

	record_cur = get_fast_record();
	while (record_cur) {
		if (search_points(record_cur->y, record_cur->x, cur_point->move_azimuth)) {
			record_cur = record_cur->next;
			continue;
		}

		distance = two_points_distance(cur_point->latitude, cur_point->longitude,
					       record_cur->y, record_cur->x);
		if (distance > cur_point->search_distance) {
			EDOG_DBG("distance=%lf > search_distance=%lf\n", distance,
				 cur_point->search_distance);
			EDOG_DBG("lat: %lf, lon: %lf\n", record_cur->y, record_cur->x);
			EDOG_DBG("---------------------\n\n");
			record_cur = record_cur->next;
			continue;
		}

		azimuth = calculate_azimuth(cur_point->latitude, cur_point->longitude,
					    record_cur->y, record_cur->x);

		ret = record_search(cur_point->move_azimuth, distance, azimuth, pre_distance);
		if (!ret) {
			EDOG_DBG("broadcast right lat: %lf, lon: %lf\n", record_cur->y, record_cur->x);
			memcpy(&info, record_cur, sizeof(struct dbf_record_info));
			pre_distance = distance;
		}

		EDOG_DBG("---------------------\n\n");
		record_cur = record_cur->next;
	}

	if (pre_distance != INVALID_VALUES) {
		if (pre_distance >= LOW_100M) {
			get_voice_dir(libedog_info.voice_dir, pre_distance);
			if (libedog_info.voice_dir[0] != 0) {
				EDOG_DBG("broadcast lat: %lf, lon: %lf\n", info.y, info.x);
				EDOG_DBG("pre_distance: %lf\n", pre_distance);
				EDOG_DBG("heading: %d\n", info.heading);
				libedog_info.pass_flag = 1;
				libedog_info.distance = pre_distance;

#ifdef LIBEDOG_DATA1
				play_voice_data1(libedog_info.voice_dir, (char *)info.prompt);
#elif LIBEDOG_DATA2
				play_voice_data2(info.type, NULL);
#elif LIBEDOG_DATA3
				play_voice_data3(libedog_info.voice_dir, info.voice_type, NULL);
#endif
			} else {
				EDOG_DBG("voice_dir is null\n");
				return;
			}
		}

		set_broadcast_points(&info, cur_point);
	}
}

static void printf_record_list()
{
	struct dbf_record_info *record_cur;

	EDOG_DBG("%s, list_count: %d\n", __func__, libedog_info.list_count);

	record_cur = get_fast_record();
	while (record_cur) {
		EDOG_DBG("type: %d, lat: %lf, lon: %lf\n", record_cur->type, record_cur->y,
			 record_cur->x);
		record_cur = record_cur->next;
	}

	EDOG_DBG("%s end\n", __func__);
}

static void clear_record_list()
{
	struct dbf_record_info *record_cur;

	EDOG_DBG("%s\n", __func__);

	record_cur = get_fast_record();
	while (record_cur) {
		if (record_cur->pre == 0) {
			libedog_info.list = record_cur->next;
			if (libedog_info.list)
				libedog_info.list->pre = 0;
		} else {
			record_cur->pre->next = record_cur->next;
			if (record_cur->next)
				record_cur->next->pre = record_cur->pre;
		}

		record_cur->pre = NULL;
		record_cur->next = NULL;

		record_cur = get_fast_record();
	}

	libedog_info.list_count = 0;
}

static double get_limit_distance(float speed)
{
	double distance = (speed * 1000) / 3600 * libedog_info.vertical_distance_time;

	return ((distance / 2) + 3) / 1000;
}

/* are there electronic eyes have been broadcasted but did not pass */
static int broadcast_not_pass(double lat, double lon, float speed)
{
	char dir[VOICE_DIR_LEN];
	double distance;
	double /*azimuth,*/ neg_azimuth;
	double vertical_distance, limit_distance;
	int heading;
	int index;

	if (!libedog_info.pass_flag)
		return 0;

	EDOG_DBG("%s\n", __func__);

	index = get_point_index();

	EDOG_DBG("speed: %f\n", speed);

	//azimuth = calculate_azimuth(lat, lon, libedog_info.points[index].latitude,
	//			    libedog_info.points[index].longitude);

	neg_azimuth = calculate_azimuth(libedog_info.points[index].latitude,
					libedog_info.points[index].longitude, lat, lon);

	distance = two_points_distance(lat, lon, libedog_info.points[index].latitude,
				       libedog_info.points[index].longitude);

	EDOG_DBG("lat: %lf, lon: %lf\n", lat, lon);
	EDOG_DBG("distance: %lf\n", distance);

	//EDOG_DBG("azimuth: %lf\n", azimuth);
	EDOG_DBG("neg_azimuth: %lf\n", neg_azimuth);

	if (distance <= libedog_info.ding_radius) {
		EDOG_DBG("heading: %d\n", libedog_info.points[index].heading);

		heading = get_heading(libedog_info.points[index].heading, lat, lon);
		if (heading == -1) {
			libedog_info.pass_flag = 0;
			libedog_info.far_away = 0;
			EDOG_DBG("no ding\n");
			return INVALID_POINT;
		}

		limit_distance = get_limit_distance(speed);
		vertical_distance = fabs(cos(radians(neg_azimuth - heading))) * distance;

		EDOG_DBG("limit_distance: %lf\n", limit_distance);
		EDOG_DBG("vertical_distance: %lf\n", vertical_distance);

		if (vertical_distance <= limit_distance) {
			libedog_info.pass_flag = 0;
			libedog_info.far_away = 0;
			EDOG_DBG("ding\n");

#ifdef LIBEDOG_DATA1
			play_voice_data1(NULL, libedog_info.ding_file_name);
#elif LIBEDOG_DATA2
			play_voice_data2(0, libedog_info.ding_file_name);
#elif LIBEDOG_DATA3
			play_voice_data3(NULL, -1, libedog_info.ding_file_name);
#endif

			return 0;
		}
	}

	if (distance > libedog_info.distance) {
		EDOG_DBG("far_away: %d, pre_distance: %lf\n", libedog_info.far_away,
			 libedog_info.distance);
		if (libedog_info.far_away == FAR_AWAY_COUNT) {
			EDOG_DBG("the car drive away from the current electronic eye\n");
			libedog_info.pass_flag = 0;
			libedog_info.far_away = 0;
			return 0;
		}

		libedog_info.far_away++;
	}

	libedog_info.distance = distance;
	return VALID_POINT;
}

/* search_distance: km */
double compute_degree(double search_distance)
{
	return degrees(atan((LANE_WIDTH * LANES_COUNT) / search_distance));
}

void edog_process(struct location_info *cur_point, float speed,
		  struct dbf_record_info *info, int count)
{
	int ret;
	struct lat_lon_range range;

	EDOG_DBG("gcjLat: %lf, gcjLng: %lf\n", cur_point->latitude,
		 cur_point->longitude);

	cur_point->search_distance = get_broadcast_distance(speed);

	if (libedog_info.pre_point.latitude == 0.0
	    && libedog_info.pre_point.longitude == 0.0) {
		EDOG_DBG("pre_point == NULL, set it\n");
		set_point(cur_point, &libedog_info.pre_point);
		return;
	}

	if (libedog_info.pre_point.latitude == cur_point->latitude
	    && libedog_info.pre_point.longitude == cur_point->longitude) {
		EDOG_DBG("the same location\n");
		return;
	}

	cur_point->move_azimuth = calculate_azimuth(libedog_info.pre_point.latitude,
				  libedog_info.pre_point.longitude, cur_point->latitude,
				  cur_point->longitude);

	ret = broadcast_not_pass(cur_point->latitude, cur_point->longitude, speed);
	if (ret == VALID_POINT) {
		set_point(cur_point, &libedog_info.pre_point);
		return;
	} else if (ret == INVALID_POINT) {
		set_point((struct location_info *)NULL, &libedog_info.pre_point);
		return;
	}

	if (!car_turning(libedog_info.pre_point.move_azimuth,
			 cur_point->move_azimuth)) {
		EDOG_DBG("car turning\n");
		set_point(cur_point, &libedog_info.pre_point);
		return;
	}

	bounding_coordinates(&range, cur_point->latitude,
			     cur_point->longitude, cur_point->search_distance);

	/* binary search method, set record list */
	binary_search(info, range, 0, count - 1);
	printf_record_list();

	if (get_fast_record()) {
		broadcast_record_search(cur_point);
	}

	set_point(cur_point, &libedog_info.pre_point);
	EDOG_DBG("pre_point, move_azimuth: %lf, lat: %lf, lon: %lf\n",
		 libedog_info.pre_point.move_azimuth,
		 libedog_info.pre_point.latitude,
		 libedog_info.pre_point.longitude);

	clear_record_list();
}

void libedog_init(libedog_audio_play audio_play, char *voice_path,
		  char *ding_file_name, int degree_trun, double ding_radius,
		  double vertical_distance_time)
{
	memset(&libedog_info, 0, sizeof(struct libedog));

	libedog_info.degree_trun = degree_trun;
	libedog_info.ding_radius = ding_radius;
	libedog_info.vertical_distance_time = vertical_distance_time;
	libedog_info.audio_play = audio_play;
	memcpy(libedog_info.voice_path, voice_path, PATH_LEN);
	memcpy(libedog_info.ding_file_name, ding_file_name, PATH_LEN);
}