/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __E_DOG_COMMON__
#define __E_DOG_COMMON__

#include <autoconfig/libedog_autoconf.h>

#ifdef LIBEDOG_DEBUG
#define EDOG_DBG(...) printf(__VA_ARGS__)
#else
#define EDOG_DBG(...)
#endif

#define PATH_LEN 128
#define VOICE_DIR_LEN 4

/* the maximum number of electronic eyes that have been broadcast */
#define RECORD_MAX_LEN 10

#ifndef max
#define max(a, b)   (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a, b)   (((a) < (b)) ? (a) : (b))
#endif

#define PI 3.14159265358979323846

/* earth's average radius: km */
#define EARTH_RADIUS 6371.393

/* chinese national standards: 3.75m */
#define LANE_WIDTH 0.00375
/* one-way lane number */
#define LANES_COUNT 7

#define INVALID_VALUES 9999.9

#define VALID_POINT 1
#define INVALID_POINT -1

/* the number of cars away from the current broadcast electronic eyes */
#define FAR_AWAY_COUNT 3

/* degrees */
#define DEGREE_90 90
#define DEGREE_170 170
#define DEGREE_180 180
#define DEGREE_190 190
#define DEGREE_360 360

#define SPEED_60 60
#define SPEED_90 90

/* voice directory name */
#define VOICE_DIR_100 "0100"
#define VOICE_DIR_200 "0200"
#define VOICE_DIR_300 "0300"
#define VOICE_DIR_500 "0500"
#define VOICE_DIR_1000 "1000"

/* voice matching distance range, km */
#define LOW_100M 0.08
#define HIGH_100M 0.12

#define LOW_200M 0.18
#define HIGH_200M 0.22

#define LOW_300M 0.28
#define HIGH_300M 0.32

#define LOW_500M 0.48
#define HIGH_500M 0.52

#define LOW_1KM 0.95
#define HIGH_1KM 1

/* distance matching azimuth, compute_degree(distance) */
#define DEGREE_100M 14.708304
#define DEGREE_150M 9.926246

#define DEGREE_200M 7.477330
#define DEGREE_250M 5.994093

#define DEGREE_300M 5.000645
#define DEGREE_350M 4.289153

#define DEGREE_400M 3.754652
#define DEGREE_450M 3.338471

#define DEGREE_500M 3.005269
#define DEGREE_550M 2.732498

#define DEGREE_600M 2.505093
#define DEGREE_650M 2.312611

#define DEGREE_700M 2.147585
#define DEGREE_750M 2.004534

#define DEGREE_800M 1.879343
#define DEGREE_850M 1.768866

#define DEGREE_900M 1.670653
#define DEGREE_950M 1.582770

#define DEGREE_1KM 1.503669

/* distance, km */
#define DIST_0M 0

#define DIST_100M 0.10
#define DIST_150M 0.15

#define DIST_200M 0.20
#define DIST_250M 0.25

#define DIST_300M 0.30
#define DIST_350M 0.35

#define DIST_400M 0.40
#define DIST_450M 0.45

#define DIST_500M 0.50
#define DIST_550M 0.55

#define DIST_600M 0.60
#define DIST_650M 0.65

#define DIST_700M 0.70
#define DIST_750M 0.75

#define DIST_800M 0.80
#define DIST_850M 0.85

#define DIST_900M 0.90
#define DIST_950M 0.95

#define DIST_1KM 1

#endif
