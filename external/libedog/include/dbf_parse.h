/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __DBF_PARSE_H__
#define __DBF_PARSE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "edog_common.h"

#define VOICE_FILENAME_LEN 40

/* security monitoring electronic eye, don't play voice */
#define SECURITY_MONITOR_TYPE 206

struct dbf_header_info {
	uint8_t file_type;
	uint8_t date[3];
	uint32_t  record_count;
	uint16_t header_length;
	uint16_t record_length;
	uint8_t reserved[20];
} __attribute__((packed));

#ifdef LIBEDOG_DATA1
struct dbf_record_info {
	/* dbf flag: delete = 0x2A, not delete = 0x20 */
	uint8_t del_flag;

	uint32_t map_id;

	/* unique identification number */
	uint32_t id;

	/* name, main information description */
	uint8_t name[200];

	/* electronic eye type, type = 206(public security monitoring) don't broadcast */
	uint32_t type;

	uint32_t rd_mesh_id;
	uint32_t rd_id;

	/* azimuth (from 0 to 359, 0 = North, 90 - East, 180 - South, 270 - West). */
	uint32_t heading;

	/* speed limitation, kmh */
	uint32_t speed;

	/* longitude */
	double x;
	/* latitude */
	double y;

	/* the longitude of a vertical projection point on a connected path. */
	double x1;
	/* the latitude of a vertical projection point on a connected path. */
	double y1;

	/* main and side roads type
	 * 0��all roads
	* 1: main road
	* 2: side road
	 */
	uint32_t road_type;

	/* altitude
	 * 0: no relationship between up and down
	* 1: non-surface road
	* 2: surface road
	*/
	uint32_t level;

	/* administrative districts */
	uint32_t dist_id;

	/* voice prompt type code */
	uint8_t prompt[VOICE_FILENAME_LEN]; //40 bytes, distribution of 8 bytes to the list(*pre, *next)

	struct dbf_record_info *pre;
	struct dbf_record_info *next;
} __attribute__((packed));

#elif LIBEDOG_DATA2
struct dbf_record_info {
	/* dbf flag: delete = 0x2A, not delete = 0x20 */
	uint8_t del_flag;

	/* object number (not actual) */
	uint32_t idx;

	/* longitude */
	double x;
	/* latitude */
	double y;

	/* type of object */
	uint32_t type;

	/* speed limitation, kmh */
	uint32_t speed;

	/* type of direction
	 * 0 - all around (360 degree)
	 * 1 - only one direction (see DIRECTION)
	 * 2 - two directions (see DIRECTION + reversed direction)
	 */
	uint32_t dircount;

	/* azimuth (from 0 to 359, 0 = North, 90 - East, 180 - South, 270 - West). */
	uint32_t heading;

	struct dbf_record_info *pre;
	struct dbf_record_info *next;
} __attribute__((packed));

/* Group I */
#define TYPE_1_VOICE "type_1"
#define TYPE_3_VOICE "type_3"
#define TYPE_11_VOICE "type_11"
#define TYPE_20_33_VOICE "type_20_33"
#define TYPE_30_VOICE "type_30"
#define TYPE_32_VOICE "type_32"

/* Group II */
#define TYPE_5_VOICE "type_5"

/* Group III */
#define TYPE_103_VOICE "type_103"
#define TYPE_104_VOICE "type_104"
#define TYPE_105_VOICE "type_105"

/* Group IV */
#define TYPE_101_VOICE "type_101"

/* Group V */
#define TYPE_102_VOICE "type_102"

/* Group VI */
#define TYPE_100_VOICE "type_100"

/* Group VII */
#define TYPE_50_VOICE "type_50"
#define TYPE_55_VOICE "type_55"

#elif LIBEDOG_DATA3
struct dbf_record_info {
	/* dbf flag: delete = 0x2A, not delete = 0x20 */
	uint8_t del_flag;

	/* serial number */
	uint32_t num;

	/* electronic eye type */
	uint32_t type;

	/* azimuth (from 0 to 359, 0 = North, 90 - East, 180 - South, 270 - West). */
	uint32_t heading;

	/* speed limitation, kmh */
	uint32_t speed;

	/* longitude */
	double x;
	/* latitude */
	double y;

	/* voice type */
	uint32_t voice_type;

	struct dbf_record_info *pre;
	struct dbf_record_info *next;
} __attribute__((packed));

#define VOICE_TYPE_LEN 4
#define VOICE_NAME_LEN 8

#define TYPE_1000 1000
#define TYPE_1000_VOICE "10050000"

#define TYPE_4000 4000
#define TYPE_4000_VOICE "40050000"

#define TYPE_4030 4030
#define TYPE_4030_VOICE "40050030"

#define TYPE_4040 4040
#define TYPE_4040_VOICE "40050040"

#define TYPE_4050 4050
#define TYPE_4050_VOICE "40050050"

#define TYPE_4060 4060
#define TYPE_4060_VOICE "40050060"

#define TYPE_4070 4070
#define TYPE_4070_VOICE "40050070"

#define TYPE_4080 4080
#define TYPE_4080_VOICE "40050080"

#define TYPE_8000 8000
#define TYPE_8000_VOICE "80050000"
#else
struct dbf_record_info {
	/* dbf flag: delete = 0x2A, not delete = 0x20 */
	uint8_t del_flag;

	/* longitude */
	double x;
	/* latitude */
	double y;

	/* type of object */
	uint32_t type;

	/* azimuth (from 0 to 359, 0 = North, 90 - East, 180 - South, 270 - West). */
	uint32_t heading;

	struct dbf_record_info *pre;
	struct dbf_record_info *next;
} __attribute__((packed));
#endif

int dbf_parse(FILE *fp, struct dbf_record_info **info);
#endif
