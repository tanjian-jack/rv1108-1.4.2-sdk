/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __LIB_EDOG_H__
#define __LIB_EDOG_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dbf_parse.h"

typedef int (* libedog_audio_play)(char *);

struct lat_lon_range {
	double lat_min;
	double lat_max;
	double lon_min;
	double lon_max;
};

struct location_info {
	int heading;

	/* GCJ-02 */
	double latitude;
	double longitude;

	/* the driving angle between two points */
	double move_azimuth;

	/* search all electronic eyes within a few meters radius */
	double search_distance;
};

struct libedog {
	/* are there electronic eyes have been broadcasted but did not pass */
	int pass_flag;

	/* the car away from the current broadcast electronic eyes */
	int far_away;

	/* the distance between the car and the current broadcast electronic eyes */
	double distance;

	/* currently broadcast electronic eyes prompt directory */
	char voice_dir[VOICE_DIR_LEN];

	/* the previous location of the car */
	struct location_info pre_point;

	/* points offset */
	int point_index;
	/* record the electronic eyes that has been broadcast */
	struct location_info points[RECORD_MAX_LEN];

	/* specify the longitude and latitude range of electronic eyes list*/
	int list_count;
	struct dbf_record_info *list;

	libedog_audio_play audio_play;
	char voice_path[PATH_LEN];
	char ding_file_name[PATH_LEN];

	int degree_trun;
	double ding_radius;
	double vertical_distance_time;
};

double compute_degree(double search_distance);

void edog_process(struct location_info *cur_point, float speed,
		  struct dbf_record_info *info, int count);

void libedog_init(libedog_audio_play audio_play, char *voice_path,
		  char *ding_file_name, int degree_trun, double ding_radius,
		  double vertical_distance_time);
#endif