cmake_minimum_required(VERSION 2.8.11)
project(edog)

set(LIBEDOG_SRC_FILES
    dbf_parse.c
    libedog.c
)

add_library(edog SHARED ${LIBEDOG_SRC_FILES})
include_directories(${edog_SOURCE_DIR}/include)

add_custom_command(TARGET edog POST_BUILD
        COMMAND mkdir -p ${CMAKE_INSTALL_PREFIX}/include/edog
        COMMAND cp -r ${edog_SOURCE_DIR}/include/* ${CMAKE_INSTALL_PREFIX}/include/edog
        COMMAND mkdir -p ${CMAKE_INSTALL_PREFIX}/lib
        COMMAND cp ${CMAKE_CURRENT_BINARY_DIR}/libedog.so ${CMAKE_INSTALL_PREFIX}/lib
        COMMENT "edog copy headers")

# If the CMake version supports it, attach header directory information
# to the targets for when we are part of a parent build (ie being pulled
# in via add_subdirectory() rather than being a standalone build).
if (DEFINED CMAKE_VERSION AND NOT "${CMAKE_VERSION}" VERSION_LESS "2.8.11")
	target_include_directories(edog
	  INTERFACE "${edog_SOURCE_DIR}/include")
# 	PRIVATE $<TARGET_PROPERTY:ion,INTERFACE_INCLUDE_DIRECTORIES>)
endif()

#target_link_libraries(edog)

#install(TARGETS cutils DESTINATION lib)
install(TARGETS edog DESTINATION lib)
install(DIRECTORY ${edog_SOURCE_DIR}/include/ DESTINATION include/edog)
