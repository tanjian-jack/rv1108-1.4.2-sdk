/**
 * Copyright (C) 2017 Fuzhou Rockchip Electronics Co., Ltd
 * author: Tianfeng Chen <ctf@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "dbf_parse.h"

static int dbf_header_parse(FILE *fp, struct dbf_header_info *dbf_header,
			    int len)
{
	EDOG_DBG("%s\n", __func__);

	memset(dbf_header, 0, len);
	if (fread(dbf_header, len, 1, fp) != 1) {
		printf("%s, read dbf file header fault.\n", __func__);
		return -1;
	}

	EDOG_DBG("record_count: %d\n", dbf_header->record_count);
	EDOG_DBG("header_length: %d\n", dbf_header->header_length);
	EDOG_DBG("record_length: %d\n", dbf_header->record_length);

	return 0;
}

static int dbf_record_read(FILE *fp, struct dbf_record_info **info,
			   int record_length, int record_count, int header_length)
{
	int i;
	char file_end;

	EDOG_DBG("%s\n", __func__);

	if (fseek(fp, header_length, SEEK_SET) != 0) {
		printf("%s, dbf file seek to the record data fault.\n", __func__);
		return -1;
	}

	for (i = 0; i < record_count; i++) {
		if (fread(&((*info)[i]), record_length, 1, fp) != 1) {
			printf("%s, read dbf file record fault.\n", __func__);
			return -1;
		}
	}

	if (fread(&file_end, 1, 1, fp) != 1) {
		printf("%s, read dbf file end flag fault.\n", __func__);
		return -1;
	}

	if (file_end != 0x1a) {
		printf("%s, did not read to the end of the dbf file.\n", __func__);
		return -1;
	}

	return 0;
}

static void swap(struct dbf_record_info *info, int i, int j)
{
	struct dbf_record_info temp = info[i];
	info[i] = info[j];
	info[j] = temp;
}

static int partition(struct dbf_record_info *info, int left, int right)
{
	int i;
	int tail = left - 1;
	double pivotx = info[right].x;
	double pivoty = info[right].y;

	for (i = left; i < right; i++) {
		if ((info[i].x - pivotx) < 0) {
			swap(info, ++tail, i);
		} else if ((info[i].x - pivotx) == 0) {
			if ((info[i].y - pivoty) <= 0)
				swap(info, ++tail, i);
		}
	}

	swap(info, tail + 1, right);

	return tail + 1;
}

static void quick_sort(struct dbf_record_info *info, int left, int right)
{
	int pivot_index;

	if (left >= right)
		return;

	pivot_index = partition(info, left, right);
	quick_sort(info, left, pivot_index - 1);
	quick_sort(info, pivot_index + 1, right);
}

static void dbf_record_sort(struct dbf_record_info *info, int count)
{
	int i;

	EDOG_DBG("%s\n", __func__);

	quick_sort(info, 0, count - 1);

	for (i = 1; i < count; i++) {
		if ((info[i].x - info[i - 1].x) < 0) {
			EDOG_DBG("%s, Longitude sorting error\n", __func__);
			EDOG_DBG("%d, x: %lf, y: %lf\n", i - 1, info[i - 1].x, info[i - 1].y);
			EDOG_DBG("%d, x: %lf, y: %lf\n", i, info[i].x, info[i].y);
		} else if ((info[i].x - info[i - 1].x) == 0) {
			if ((info[i].y - info[i - 1].y) < 0) {
				EDOG_DBG("%s, Latitude sorting error\n", __func__);
				EDOG_DBG("%d, x: %lf, y: %lf\n", i - 1, info[i - 1].x, info[i - 1].y);
				EDOG_DBG("%d, x: %lf, y: %lf\n", i, info[i].x, info[i].y);
			}
		}
	}
}

int dbf_parse(FILE *fp, struct dbf_record_info **info)
{
	int total_size = 0;
	int header_info_len = sizeof(struct dbf_header_info);
	struct dbf_header_info dbf_header;

	EDOG_DBG("%s\n", __func__);

	if (*info != NULL) {
		printf("%s, edog info != NULL.\n", __func__);
		return -1;
	}

	if (dbf_header_parse(fp, &dbf_header, header_info_len) != 0)
		return -1;

	total_size = sizeof(struct dbf_record_info) * dbf_header.record_count;
	printf("total_size: %d\n", total_size);

	*info = (struct dbf_record_info *)malloc(total_size);
	if (*info == NULL) {
		printf("%s, malloc edog info buffer error\n", __func__);
		return -1;
	}
	memset((char *)(*info), 0, total_size);

	if (dbf_record_read(fp, info, dbf_header.record_length, dbf_header.record_count,
			    dbf_header.header_length) != 0)
		return -1;

	dbf_record_sort(*info, dbf_header.record_count);

	return dbf_header.record_count;
}
