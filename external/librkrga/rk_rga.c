#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <memory.h>
#include <errno.h>
#include<stdlib.h>

#include "ion/ion.h"
#include "inc/rk_rga.h"
#include "inc/rga_copybit.h"

int rk_rga_open() {
  int fd;
	//printf("%s\n", __func__);
  if (!access("/dev/rga", R_OK | W_OK)) {
    fd = open("/dev/rga", O_RDWR, 0);
    if (fd < 0) {
      printf("open rga device error\n");
      return -1;
    }
  } else
    fd = -1;

  if (fd < 0) {
    printf("%s: rga is not opened.\n", __func__);
    return -1;
  }
  return fd;
}

void rk_rga_close(int fd) {
	//printf("%s\n", __func__);
  if (fd >= 0)
    close(fd);
}

int rk_rga_ionfdnv12_to_ionfdnv12_rotate_ext(
    int src_fd,
    int src_w,
    int src_h,
    int src_vir_w,
    int src_vir_h,
    int dst_fd,
    int dst_w,
    int dst_h,
    int rotate)  // dst_w and dst_h must be align of 16
{
  int fd = -1;
  int ret = 0;

  fd = rk_rga_open();

  if (fd <= 0)
    return -1;

  ret = rk_rga_ionfdnv12_to_ionfdnv12_rotate(fd, src_fd, src_w, src_h, src_vir_w, src_vir_h,
                               dst_fd, dst_w, dst_h, rotate);

  rk_rga_close(fd);

  return ret;
}

int rk_rga_phy_to_ionfd_offset_rotate(int rga_fd,
                           int src_phy,
                           int src_w,
                           int src_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int y_offset,
                           int rotate,
                           int src_format,
                           int dst_format) {
  int ret = 0;
  int dst_act_w;
  int dst_act_h;
  int dst_x_offset;
  int dst_y_offset;
  struct rga_req rga_cfg;
  //printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);
  memset(&rga_cfg, 0x0, sizeof(struct rga_req));
  switch(rotate) {
  	case 0:
  		rga_cfg.sina = 0;
      rga_cfg.cosa = 0;
      dst_act_w = dst_w;
      dst_act_h = dst_h;
      dst_x_offset = 0;
      dst_y_offset = 0;
  		break;
  	case 90:
  		rga_cfg.sina = 65536;
      rga_cfg.cosa = 0;
      dst_act_w = dst_h;
      dst_act_h = dst_w;
      dst_x_offset = dst_w - 1;
      dst_y_offset = 0;
  		break;
  	case 270:
  		rga_cfg.sina = -65536;
      rga_cfg.cosa = 0;
      dst_act_w = dst_h;
      dst_act_h = dst_w;
      dst_x_offset = 0;
      dst_y_offset = dst_h - 1;
  		break;
  	default:
  		rga_cfg.sina = 0;
      rga_cfg.cosa = 0;
      dst_act_w = dst_w;
      dst_act_h = dst_h;
      dst_x_offset = 0;
      dst_y_offset = 0;
  		break;
  }
  rga_cfg.scale_mode = 2;
  rga_cfg.render_mode = 0;
  rga_cfg.rotate_mode = 1;
  rga_cfg.src.format = src_format;
  rga_cfg.src.yrgb_addr = 0;
  rga_cfg.src.uv_addr = src_phy;
  rga_cfg.src.v_addr = 0;
  rga_cfg.src.act_w = src_w;
  rga_cfg.src.act_h = src_h;
  rga_cfg.src.vir_w = src_w;
  rga_cfg.src.vir_h = src_h;
  rga_cfg.dst.yrgb_addr = dst_fd;
  rga_cfg.dst.uv_addr = 0;
  rga_cfg.dst.v_addr = 0;
  rga_cfg.dst.act_w = dst_act_w;
  rga_cfg.dst.act_h = dst_act_h;
  rga_cfg.dst.vir_w = dst_w;
  rga_cfg.dst.vir_h = dst_h * 2;
  rga_cfg.dst.x_offset = dst_x_offset;
  rga_cfg.dst.y_offset = dst_y_offset + y_offset;
  rga_cfg.dst.format = dst_format;
  rga_cfg.mmu_info.mmu_en = 0;
  rga_cfg.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1;
  rga_cfg.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
  rga_cfg.clip.xmin = 0;
  rga_cfg.clip.xmax = src_w - 1;
  rga_cfg.clip.ymin = 0;
  rga_cfg.clip.ymax = src_h - 1;

  ret = ioctl(rga_fd, RGA_BLIT_SYNC, &rga_cfg);
  if (ret)
    printf("RGA_BLIT_SYNC faile(%s)\n", strerror(errno));

  return ret;
}

int rk_rga_phy_to_ionfd_rotate(int rga_fd,
                           int src_phy,
                           int src_w,
                           int src_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int rotate,
                           int src_format,
                           int dst_format) {
  int ret = 0;
  int dst_act_w;
  int dst_act_h;
  int dst_x_offset;
  int dst_y_offset;
  struct rga_req rga_cfg;
  //printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);
  memset(&rga_cfg, 0x0, sizeof(struct rga_req));
  switch(rotate) {
  	case 0:
  		rga_cfg.sina = 0;
      rga_cfg.cosa = 0;
      dst_act_w = dst_w;
      dst_act_h = dst_h;
      dst_x_offset = 0;
      dst_y_offset = 0;
  		break;
  	case 90:
  		rga_cfg.sina = 65536;
      rga_cfg.cosa = 0;
      dst_act_w = dst_h;
      dst_act_h = dst_w;
      dst_x_offset = dst_w - 1;
      dst_y_offset = 0;
  		break;
  	case 270:
  		rga_cfg.sina = -65536;
      rga_cfg.cosa = 0;
      dst_act_w = dst_h;
      dst_act_h = dst_w;
      dst_x_offset = 0;
      dst_y_offset = dst_h - 1;
  		break;
  	default:
  		rga_cfg.sina = 0;
      rga_cfg.cosa = 0;
      dst_act_w = dst_w;
      dst_act_h = dst_h;
      dst_x_offset = 0;
      dst_y_offset = 0;
  		break;
  }
  rga_cfg.scale_mode = 2;
  rga_cfg.render_mode = 0;
  rga_cfg.rotate_mode = 1;
  rga_cfg.src.format = src_format;
  rga_cfg.src.yrgb_addr = 0;
  rga_cfg.src.uv_addr = src_phy;
  rga_cfg.src.v_addr = 0;
  rga_cfg.src.act_w = src_w;
  rga_cfg.src.act_h = src_h;
  rga_cfg.src.vir_w = src_w;
  rga_cfg.src.vir_h = src_h;
  rga_cfg.dst.yrgb_addr = dst_fd;
  rga_cfg.dst.uv_addr = 0;
  rga_cfg.dst.v_addr = 0;
  rga_cfg.dst.act_w = dst_act_w;
  rga_cfg.dst.act_h = dst_act_h;
  rga_cfg.dst.vir_w = dst_w;
  rga_cfg.dst.vir_h = dst_h;
  rga_cfg.dst.x_offset = dst_x_offset;
  rga_cfg.dst.y_offset = dst_y_offset;
  rga_cfg.dst.format = dst_format;
  rga_cfg.mmu_info.mmu_en = 0;
  rga_cfg.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1;
  rga_cfg.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
  rga_cfg.clip.xmin = 0;
  rga_cfg.clip.xmax = src_w - 1;
  rga_cfg.clip.ymin = 0;
  rga_cfg.clip.ymax = src_h - 1;

  ret = ioctl(rga_fd, RGA_BLIT_SYNC, &rga_cfg);
  if (ret)
    printf("RGA_BLIT_SYNC faile(%s)\n", strerror(errno));

  return ret;
}

int rk_rga_phyrgb565_to_ionfdrgb565_rotate(int rga_fd,
                                           int src_phy,
                                           int src_w,
                                           int src_h,
                                           int dst_fd,
                                           int dst_w,
                                           int dst_h,
                                           int rotate)
{
  return rk_rga_phy_to_ionfd_rotate(rga_fd,
                                    src_phy,
                                    src_w,
                                    src_h,
                                    dst_fd,
                                    dst_w,
                                    dst_h,
                                    rotate,
                                    RGA_FORMAT_RGB_565,
                                    RGA_FORMAT_RGB_565);
}

int rk_rga_ionfd_to_ionfd(int rga_fd,
                           int src_fd,
                           int src_w,
                           int src_h,
                           int src_fmt,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int dst_fmt,
                           int rotate,
                           int rotate_mode) {
  int ret = 0;
  struct rga_req rga_cfg;
  //printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d, rotate = %d\n", __func__, src_w, src_h, dst_w, dst_h, rotate);
  memset(&rga_cfg, 0x0, sizeof(struct rga_req));
  switch (rotate) {
  case 0:
    rga_cfg.sina = 0;
    rga_cfg.cosa = 65536;
    rga_cfg.dst.act_w = dst_w;
    rga_cfg.dst.act_h = dst_h;
    rga_cfg.dst.x_offset = 0;
    rga_cfg.dst.y_offset = 0;
    break;
  case 90:
    rga_cfg.sina = 65536;
    rga_cfg.cosa = 0;
    rga_cfg.dst.act_w = dst_h;
    rga_cfg.dst.act_h = dst_w;
    rga_cfg.dst.x_offset = dst_w - 1;
    rga_cfg.dst.y_offset = 0;
    break;
  case 180:
    rga_cfg.sina = 0;
    rga_cfg.cosa = -65536;
    rga_cfg.dst.act_w = dst_w;
    rga_cfg.dst.act_h = dst_h;
    rga_cfg.dst.x_offset = dst_w - 1;
    rga_cfg.dst.y_offset = dst_h - 1;
    break;
  case 270:
    rga_cfg.sina = -65536;
    rga_cfg.cosa = 0;
    rga_cfg.dst.act_w = dst_h;
    rga_cfg.dst.act_h = dst_w;
    rga_cfg.dst.x_offset = 0;
    rga_cfg.dst.y_offset = dst_h - 1;
    break;
  }
  rga_cfg.scale_mode = 2;
  rga_cfg.render_mode = 0;
  rga_cfg.rotate_mode = rotate_mode;
  rga_cfg.src.format = src_fmt;
  rga_cfg.src.yrgb_addr = src_fd;
  rga_cfg.src.uv_addr = 0;
  rga_cfg.src.v_addr = 0;
  rga_cfg.src.act_w = src_w;
  rga_cfg.src.act_h = src_h;
  rga_cfg.src.vir_w = src_vir_w;
  rga_cfg.src.vir_h = src_vir_h;
  rga_cfg.dst.yrgb_addr = dst_fd;
  rga_cfg.dst.uv_addr = 0;
  rga_cfg.dst.v_addr = 0;
  rga_cfg.dst.vir_w = dst_w;
  rga_cfg.dst.vir_h = dst_h;
  rga_cfg.dst.format = dst_fmt;
  rga_cfg.mmu_info.mmu_en = 0;
  rga_cfg.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1;
  rga_cfg.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
  rga_cfg.clip.xmin = 0;
  rga_cfg.clip.xmax = dst_w - 1;
  rga_cfg.clip.ymin = 0;
  rga_cfg.clip.ymax = dst_h - 1;

  ret = ioctl(rga_fd, RGA_BLIT_SYNC, &rga_cfg);
  if (ret)
    printf("RGA_BLIT_SYNC faile(%s)\n", strerror(errno));

  return ret;
}

int rk_rga_ionfd_to_ionfd_rotate(int rga_fd,
                           int src_fd,
                           int src_w,
                           int src_h,
                           int src_fmt,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int dst_fmt,
                           int rotate) {
  return rk_rga_ionfd_to_ionfd(rga_fd, src_fd, src_w, src_h, src_fmt,
                               src_vir_w, src_vir_h, dst_fd, dst_w, dst_h,
                               dst_fmt, rotate, 1);
}

int rk_rga_ionfd_to_ionfd_mirror(int rga_fd,
                           int src_fd,
                           int src_w,
                           int src_h,
                           int src_fmt,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int dst_fmt) {
    return rk_rga_ionfd_to_ionfd(rga_fd, src_fd, src_w, src_h, src_fmt,
                                 src_vir_w, src_vir_h, dst_fd, dst_w, dst_h,
                                 dst_fmt, 0, 2);
}

int rk_rga_ionfd_to_ionfd_rotate_offset_ext(int rga_fd,
                          int src_fd,
                          int src_w,
                          int src_h,
                          int src_fmt,
                          int src_vir_w,
                          int src_vir_h,
                          int src_act_w,
                          int src_act_h,
                          int src_x_offset,
                          int src_y_offset,
                          int dst_fd,
                          int dst_w,
                          int dst_h,
                          int dst_fmt,
                          int rotate) {
 int ret = 0;
 struct rga_req rga_cfg;

 memset(&rga_cfg, 0x0, sizeof(struct rga_req));
 switch (rotate) {
 case 0:
   rga_cfg.sina = 0;
   rga_cfg.cosa = 65536;
   rga_cfg.dst.act_w = dst_w;
   rga_cfg.dst.act_h = dst_h;
   rga_cfg.dst.x_offset = 0;
   rga_cfg.dst.y_offset = 0;
   break;
 case 90:
   rga_cfg.sina = 65536;
   rga_cfg.cosa = 0;
   rga_cfg.dst.act_w = dst_h;
   rga_cfg.dst.act_h = dst_w;
   rga_cfg.dst.x_offset = dst_w - 1;
   rga_cfg.dst.y_offset = 0;
   break;
 case 180:
   rga_cfg.sina = 0;
   rga_cfg.cosa = -65536;
   rga_cfg.dst.act_w = dst_w;
   rga_cfg.dst.act_h = dst_h;
   rga_cfg.dst.x_offset = dst_w - 1;
   rga_cfg.dst.y_offset = dst_h - 1;
   break;
 case 270:
   rga_cfg.sina = -65536;
   rga_cfg.cosa = 0;
   rga_cfg.dst.act_w = dst_h;
   rga_cfg.dst.act_h = dst_w;
   rga_cfg.dst.x_offset = 0;
   rga_cfg.dst.y_offset = dst_h - 1;
   break;
 }
 rga_cfg.scale_mode = 2;
 rga_cfg.render_mode = 0;
 rga_cfg.rotate_mode = 1;
 rga_cfg.src.format = src_fmt;
 rga_cfg.src.yrgb_addr = src_fd;
 rga_cfg.src.uv_addr = 0;
 rga_cfg.src.v_addr = 0;
 rga_cfg.src.act_w = src_act_w;
 rga_cfg.src.act_h = src_act_h;
 rga_cfg.src.vir_w = src_vir_w;
 rga_cfg.src.vir_h = src_vir_h;
 rga_cfg.src.x_offset = src_x_offset;
 rga_cfg.src.y_offset = src_y_offset;
 rga_cfg.dst.yrgb_addr = dst_fd;
 rga_cfg.dst.uv_addr = 0;
 rga_cfg.dst.v_addr = 0;
 rga_cfg.dst.vir_w = dst_w;
 rga_cfg.dst.vir_h = dst_h;
 rga_cfg.dst.format = dst_fmt;
 rga_cfg.mmu_info.mmu_en = 0;
 rga_cfg.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1;
 rga_cfg.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
 rga_cfg.clip.xmin = 0;
 rga_cfg.clip.xmax = dst_w - 1;
 rga_cfg.clip.ymin = 0;
 rga_cfg.clip.ymax = dst_h - 1;

 ret = ioctl(rga_fd, RGA_BLIT_SYNC, &rga_cfg);
 if (ret)
   printf("RGA_BLIT_SYNC faile(%s)\n", strerror(errno));

 return ret;
}

int rk_rga_ionfdnv12_to_ionfdnv12_rotate(int rga_fd,
                           int src_fd,
                           int src_w,
                           int src_h,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int rotate) {
  int ret = 0;
  ret = rk_rga_ionfd_to_ionfd_rotate(rga_fd, src_fd, src_w, src_h,
                                     RGA_FORMAT_YCBCR_420_SP, src_vir_w, src_vir_h,
                                     dst_fd, dst_w, dst_h,
                                     RGA_FORMAT_YCBCR_420_SP, rotate);
  return ret;
}

int rk_rga_ionfd_to_ionfd_scal(int fd,
                     int src_fd,
                     int src_w,
                     int src_h,
                     int src_fmt,
                     int dst_fd,
                     int dst_w,
                     int dst_h,
                     int dst_fmt,
                     int x,
                     int y,
                     int scal_w,
                     int scal_h,
                     int src_vir_w,
                     int src_vir_h)
{
  struct rga_req req;
  int ret = 0;
	//printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);
  memset(&req, 0x0, sizeof(struct rga_req));

  req.src.act_w = src_w;
  req.src.act_h = src_h;

  req.src.vir_w = src_vir_w;
  req.src.vir_h = src_vir_h;
  req.src.yrgb_addr = src_fd;
  req.src.uv_addr = 0;
  req.src.v_addr = 0;
  req.src.format = src_fmt;

  req.dst.act_w = scal_w;
  req.dst.act_h = scal_h;

  req.dst.vir_w = dst_w;
  req.dst.vir_h = dst_h;
  req.dst.yrgb_addr = dst_fd;
  req.dst.x_offset = x;
  req.dst.y_offset = y;

  req.dst.format = dst_fmt;

  req.render_mode = 0;
  req.mmu_info.mmu_en = 0;
  req.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1 | 1 << 31 | 1 << 8 | 1 << 10;

  ret = ioctl(fd, RGA_BLIT_SYNC, &req);

  if (ret != 0) {
    printf("%s:  rga operation error\n", __func__);
    return -1;
  }

  return 0;
}

int rk_rga_ionphy_to_ionphy_scal(int fd,
                     int src_phy,
                     int src_w,
                     int src_h,
                     int src_fmt,
                     int dst_phy,
                     int dst_w,
                     int dst_h,
                     int dst_fmt,
                     int x,
                     int y,
                     int scal_w,
                     int scal_h,
                     int src_vir_w,
                     int src_vir_h)
{
  struct rga_req req;
  int ret = 0;
	//printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);
  memset(&req, 0x0, sizeof(struct rga_req));

  req.src.act_w = src_w;
  req.src.act_h = src_h;

  req.src.vir_w = src_vir_w;
  req.src.vir_h = src_vir_h;

  req.src.yrgb_addr = 0;
  req.src.uv_addr = src_phy;
  req.src.v_addr = 0;

  req.src.format = src_fmt;

  req.dst.act_w = scal_w;
  req.dst.act_h = scal_h;

  req.dst.vir_w = dst_w;
  req.dst.vir_h = dst_h;
  req.dst.yrgb_addr = 0;
  req.dst.uv_addr = dst_phy;
  req.dst.v_addr = 0;
  req.dst.x_offset = x;
  req.dst.y_offset = y;

  req.dst.format = dst_fmt;

  req.render_mode = 0;
  req.mmu_info.mmu_en = 0;
  req.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1 | 1 << 31 | 1 << 8 | 1 << 10;

  ret = ioctl(fd, RGA_BLIT_SYNC, &req);

  if (ret != 0) {
    printf("%s:  rga operation error\n", __func__);
    return -1;
  }

  return 0;
}


int rk_rga_ionfdnv12_to_ionfdnv12_scal(int fd,
                     int src_fd,
                     int src_w,
                     int src_h,
                     int dst_fd,
                     int dst_w,
                     int dst_h,
                     int x,
                     int y,
                     int scal_w,
                     int scal_h,
                     int src_vir_w,
                     int src_vir_h)
{
  int ret = 0;
  ret = rk_rga_ionfd_to_ionfd_scal(fd, src_fd, src_w, src_h, RGA_FORMAT_YCBCR_420_SP,
                                   dst_fd, dst_w, dst_h, RGA_FORMAT_YCBCR_420_SP, x, y,
                                   scal_w, scal_h, src_vir_w, src_vir_h);
  return ret;
}

int rk_rga_ionfdnv12_to_ionfdnv12_scal_ext(int src_fd,
                     int src_w,
                     int src_h,
                     int dst_fd,
                     int dst_w,
                     int dst_h,
                     int x,
                     int y,
                     int scal_w,
                     int scal_h,
                     int src_vir_w,
                     int src_vir_h)
{
  int fd = -1;
  int ret = 0;
	//printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);

  fd = rk_rga_open();

  if (fd <= 0)
    return -1;

  ret = rk_rga_ionfdnv12_to_ionfdnv12_scal(fd, src_fd,
                     src_w,
                     src_h,
                     dst_fd,
                     dst_w,
                     dst_h,
                     x,
                     y,
                     scal_w,
                     scal_h,
                     src_vir_w,
                     src_vir_h);

  rk_rga_close(fd);

  return ret;
}

int rk_rga_phy_to_ionfd_scal(int rga_fd,
                           int src_phy,
                           int src_w,
                           int src_h,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h,
                           int src_format,
                           int dst_format) {
  int ret = 0;
  struct rga_req rga_cfg;
  memset(&rga_cfg, 0x0, sizeof(struct rga_req));
  //printf("%s src_w = %d, src_h = %d, src_vir_w = %d, src_vir_h = %d\n", __func__, src_w, src_h, src_vir_w, src_vir_h);
  rga_cfg.sina = 0;
  rga_cfg.cosa = 0;
  rga_cfg.scale_mode = 2;
  rga_cfg.render_mode = 0;
  rga_cfg.rotate_mode = 0;
  rga_cfg.src.format = src_format;
  rga_cfg.src.yrgb_addr = 0;
  rga_cfg.src.uv_addr = src_phy;
  rga_cfg.src.v_addr = 0;
  rga_cfg.src.act_w = src_w;
  rga_cfg.src.act_h = src_h;
  rga_cfg.src.vir_w = src_vir_w;
  rga_cfg.src.vir_h = src_vir_h;
  rga_cfg.dst.yrgb_addr = dst_fd;
  rga_cfg.dst.uv_addr = 0;
  rga_cfg.dst.v_addr = 0;
  rga_cfg.dst.act_w = dst_w;
  rga_cfg.dst.act_h = dst_h;
  rga_cfg.dst.vir_w = dst_w;
  rga_cfg.dst.vir_h = dst_h;
  rga_cfg.dst.x_offset = 0;
  rga_cfg.dst.y_offset = 0;
  rga_cfg.dst.format = dst_format;
  rga_cfg.mmu_info.mmu_en = 0;
  rga_cfg.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1;
  rga_cfg.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
  rga_cfg.clip.xmin = 0;
  rga_cfg.clip.xmax = src_w - 1;
  rga_cfg.clip.ymin = 0;
  rga_cfg.clip.ymax = src_h - 1;

  ret = ioctl(rga_fd, RGA_BLIT_SYNC, &rga_cfg);
  if (ret)
    printf("RGA_BLIT_SYNC faile(%s)\n", strerror(errno));

  return ret;
}


int rk_rga_phyrgb565_to_ionfdrgb565_scal(int rga_fd,
                           int src_phy,
                           int src_w,
                           int src_h,
                           int src_vir_w,
                           int src_vir_h,
                           int dst_fd,
                           int dst_w,
                           int dst_h)
{
  return rk_rga_phy_to_ionfd_scal(rga_fd,
                                  src_phy,
                                  src_w,
                                  src_h,
                                  src_vir_w,
                                  src_vir_h,
                                  dst_fd,
                                  dst_w,
                                  dst_h,
                                  RGA_FORMAT_RGB_565,
                                  RGA_FORMAT_RGB_565);
}

int rk_rga_ionfd_to_ionfd_rotate_ext(
    int src_fd,
    int src_w,
    int src_h,
    int src_fmt,
    int src_vir_w,
    int src_vir_h,
    int dst_fd,
    int dst_w,
    int dst_h,
    int dst_fmt,
    int rotate)  // dst_w and dst_h must be align of 16
{
  int fd = -1;
  int ret = 0;

  fd = rk_rga_open();

  if (fd <= 0)
    return -1;

  ret = rk_rga_ionfd_to_ionfd_rotate(fd , src_fd, src_w, src_h, src_fmt, src_vir_w, src_vir_h,
                                     dst_fd, dst_w, dst_h, dst_fmt, rotate);

  rk_rga_close(fd);

  return ret;
}

int rk_rga_ionfd_to_ionfd_scal_ext(int src_fd,
                     int src_w,
                     int src_h,
                     int src_fmt,
                     int dst_fd,
                     int dst_w,
                     int dst_h,
                     int dst_fmt,
                     int x,
                     int y,
                     int scal_w,
                     int scal_h,
                     int src_vir_w,
                     int src_vir_h)
{
  int fd = -1;
  int ret = 0;
	//printf("%s src_w = %d, src_h = %d, dst_w = %d, dst_h = %d\n", __func__, src_w, src_h, dst_w, dst_h);

  fd = rk_rga_open();

  if (fd <= 0)
    return -1;

  ret = rk_rga_ionfd_to_ionfd_scal(fd, src_fd,
                     src_w,
                     src_h,
                     src_fmt,
                     dst_fd,
                     dst_w,
                     dst_h,
                     dst_fmt,
                     x,
                     y,
                     scal_w,
                     scal_h,
                     src_vir_w,
                     src_vir_h);

  rk_rga_close(fd);

  return ret;
}

