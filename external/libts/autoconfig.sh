#! /bin/sh
CC=arm-linux-gcc \
CXX=arm-linux-g++ \
LD=arm-linux-ld \
AS=arm-linux-as \
AR=arm-linux-ar \
./configure  --prefix=$(pwd)/../../out/system \
--host=arm-linux  \
--target=arm-linux \
--build=i386-linux \
--with-osname=linux \
--with-targetname=shadow \
--enable-input=yes
