#! /bin/sh
CC=arm-linux-gcc \
CXX=arm-linux-g++ \
LD=arm-linux-ld \
AS=arm-linux-as \
AR=arm-linux-ar \
CFLAGS="-I$(pwd)/../../out/system/include/" \
LDFLAGS="-L$(pwd)/../../out/system/lib/" \
./configure  --prefix=$(pwd)/../../../out/system \
--host=arm-linux  \
--target=arm-linux \
--build=i386-linux \
--with-python=no
