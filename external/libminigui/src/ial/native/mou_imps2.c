/*
** $Id: mou_imps2.c 11733 2009-06-02 01:20:06Z weiym $
**
** mou_imps2.c: Intelligent PS/2 Mouse Driver
**
** Copyright (C) 2002 ~ 2006 Feynman Software.
**
** Create Date: 2002/10/15 
*/

/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
** TODO:
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

#include "common.h"
#include "ial.h"
#include "gal.h"
#include "native.h"

#ifdef _MGCONSOLE_IMPS2

#define SCALE           3       /* default scaling factor for acceleration */
#define THRESH          5       /* default threshhold for acceleration */

/* The intialization string of imps/2 mouse, comes from
 * xc/program/Xserver/hw/xfree86/input/mouse/mouse.c
 */
static unsigned char IMPS2_Param [] = {243,200,243,100,243,80};

static int  IMPS2_Open (const char* mdev);
static void IMPS2_Close (void);
static int  IMPS2_GetButtonInfo (void);
static void IMPS2_GetDefaultAccel (int *pscale, int *pthresh);
static int  IMPS2_Read (int *dx, int *dy, int *dz, int *bp);
static void IMPS2_Suspend (void);
static int IMPS2_Resume (void);

MOUSEDEVICE mousedev_IMPS2 = {
    IMPS2_Open,
    IMPS2_Close,
    IMPS2_GetButtonInfo,
    IMPS2_GetDefaultAccel,
    IMPS2_Read,
    IMPS2_Suspend,
    IMPS2_Resume
};

static int mouse_fd;

static int IMPS2_Open (const char* mdev)
{
    mouse_fd = open (mdev, O_RDWR | O_NONBLOCK);
    if (mouse_fd < 0) {
        mouse_fd = open (mdev, O_RDONLY | O_NONBLOCK);
        if (mouse_fd < 0)
            return -1;
    }

    fprintf(stderr, "IMPS2_Open mdev: %s  mouse_fd %d\n", mdev, mouse_fd);

    return mouse_fd;
}

static void IMPS2_Close (void)
{
    if (mouse_fd > 0)
        close (mouse_fd);

    mouse_fd = -1;
}

static int IMPS2_GetButtonInfo(void)
{
    return BUTTON_L | BUTTON_M | BUTTON_R | WHEEL_UP | WHEEL_DOWN;
}

static void IMPS2_GetDefaultAccel(int *pscale, int *pthresh)
{
    *pscale = SCALE;
    *pthresh = THRESH;
}

static int IMPS2_Read (int *dx, int *dy, int *dz, int *bp)
{
    int n;
    struct input_event input_mouse;

    while (n = read (mouse_fd, &input_mouse, sizeof(input_mouse))) {
        if (n < 0) {
            if (errno == EINTR)
                continue;
            else
                return -1;
        }

        switch(input_mouse.type) 
        { 
            case EV_KEY: 
            {
                switch(input_mouse.code) 
                { 
                    case BTN_LEFT:   
                    if(input_mouse.value==1) 
                        *bp = BUTTON_L; 
                    break; 
                    case BTN_RIGHT: 
                    if(input_mouse.value==1) 
                        *bp = BUTTON_R;       
                    break; 
                    case BTN_MIDDLE: 
                    if(input_mouse.value==1) 
                        *bp = BUTTON_M;   
                    break;
                    default:
                        *bp = 0;
                    break;

                }

                return 1;
            } 
            break; 
            case EV_REL: 
            { 
                switch(input_mouse.code) 
                { 
                    case REL_X: 
                        *dx = input_mouse.value;
                    break; 
                    case REL_Y:
						*dy = input_mouse.value;
                    break; 
					default:
						*dx = 0;
						*dy = 0;
                    break; 

                } 
                *bp = 0;
                return 1;

            } 
            break; 
        }
    }
    return 0;
}

static void IMPS2_Suspend (void)
{
    IMPS2_Close();
}

static int IMPS2_Resume (void)
{
    return IMPS2_Open (IAL_MDev);
}

#endif /* _MGCONSOLE_IMPS2 */

