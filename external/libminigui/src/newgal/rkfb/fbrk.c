#include <stdio.h>
#include "stdint.h"

#include "common.h"
#include "newgal.h"
#include "blit.h"
#include "fbmatrox.h"
#include "matrox_mmio.h"
#include "memops.h"
#include "fbrk.h"
#include "rga.h"
#include <fcntl.h>


static int fd_rga = -1;

/* Sets video mem colorkey and accelerated blit function */
static int SetHWColorKey(_THIS, GAL_Surface *surface, Uint32 key)
{
	fprintf(stderr, "%s\n", __func__);
    return(0);
}

/* Sets per surface hardware alpha value */
#if 0
static int SetHWAlpha(_THIS, GAL_Surface *surface, Uint8 value)
{
    return(0);
}
#endif



static int FillRectByCpu(_THIS, GAL_Surface *dst, GAL_Rect *rect, Uint32 color)
{

	//printf("FillHWRect 11\n");
	GAL_VideoDevice *video = dst->video;
	int x, y;
	Uint8 *row;
	GAL_Rect my_dstrect;

    if (!video)
	{
		video = current_video;
		this = current_video;
	}
	//fprintf(stderr, "%s\n", __func__);
	my_dstrect = *rect;

	row = (Uint8 *)dst->pixels+my_dstrect.y*dst->pitch+
			my_dstrect.x*dst->format->BytesPerPixel;
	if ( dst->format->palette || (color == 0) ) {
		x = my_dstrect.w*dst->format->BytesPerPixel;
		if ( !color && !((long)row&3) && !(x&3) && !(dst->pitch&3) ) {
			int n = x >> 2;
			for ( y=my_dstrect.h; y; --y ) {
				GAL_memset4(row, 0, n);
				row += dst->pitch;
			}
		} else {
			{
				for(y = my_dstrect.h; y; y--) {
					memset(row, color, x);
					row += dst->pitch;
				}
			}
		}
	} else {
		switch (dst->format->BytesPerPixel) {
			case 2:
			for ( y=my_dstrect.h; y; --y ) {
				Uint16 *pixels = (Uint16 *)row;
				Uint16 c = color;
				Uint32 cc = (Uint32)c << 16 | c;
				int n = my_dstrect.w;
				if((unsigned long)pixels & 3) {
					*pixels++ = c;
					n--;
				}
				if(n >> 1)
					GAL_memset4(pixels, cc, n >> 1);
				if(n & 1)
					pixels[n - 1] = c;
				row += dst->pitch;
			}
			break;

			case 3:
			if(GAL_BYTEORDER == GAL_BIG_ENDIAN)
				color <<= 8;
			for ( y=my_dstrect.h; y; --y ) {
				Uint8 *pixels = row;
				for ( x=my_dstrect.w; x; --x ) {
					memcpy(pixels, &color, 3);
					pixels += 3;
				}
				row += dst->pitch;
			}
			break;

			case 4:
			for(y = my_dstrect.h; y; --y) {
				GAL_memset4(row, color, my_dstrect.w);
				row += dst->pitch;
			}
			break;
		}
	}    
	return 0;
}    
static RgaSURF_FORMAT GetRgaFmt(Uint8 PerPixel)
{
    RgaSURF_FORMAT rgaFormat;
	fprintf(stderr, "%s PerPixel = %d\n", __func__, PerPixel);
    switch (PerPixel)
    {
        case 2:
            rgaFormat = RK_FORMAT_RGB_565;
            break;
        case 3:
            rgaFormat = RK_FORMAT_RGB_888;
            break;
        case 4:
            rgaFormat = RK_FORMAT_RGBX_8888;
            break;
        default:
            rgaFormat = RK_FORMAT_UNKNOWN;
            break;
    }
    return  rgaFormat;       
}
// 0:no cover,  1:coverd
static int IsRectcovered(GAL_Rect *srcrect, GAL_Rect *dstrect)
{
 	fprintf(stderr, "%s\n", __func__);     
    if((srcrect->x+srcrect->w) <= dstrect->x)
        return 0;
    else if((dstrect->x+dstrect->w) <= srcrect->x)   
        return 0;
    else if((srcrect->y+srcrect->h) <= dstrect->y)   
        return 0;
    else if((dstrect->y+dstrect->h) <= srcrect->y)    
        return 0;
    else
        return 1;
   
}
static int FillHWRect(_THIS, GAL_Surface *dst, GAL_Rect *rect, Uint32 color)
{
#if USE_RGA_OPTME
    void *     dstLogical;
    unsigned int      dstWidth;
    unsigned int      dstHeight;
    RgaSURF_FORMAT dstFormat;
    unsigned int        n = 0;

    struct rga_req  Rga_Request;
    RECT_RGA clip;
    unsigned int      Xoffset;
    unsigned int      Yoffset;
    unsigned int      WidthAct;
    unsigned int      HeightAct;
    COLOR_FILL FillColor ;    
	fprintf(stderr, "%s\n", __func__);
    if(rect->w * rect->h > RGA_MIN_PIXELS)  // do by cpu    
    {   	 
        printf_debug("FillHWRect by rga \n");
        if(fd_rga < 0)
            fd_rga = open("/dev/rga", O_RDWR, 0);
        if(fd_rga < 0)
        {
            printf("fd_rga failed line=%d... \n",__LINE__);
            goto dobycpu;
        }
       

        memset(&FillColor , 0x0, sizeof(COLOR_FILL));
        memset(&Rga_Request, 0x0, sizeof(Rga_Request));

        /* >>> Begin dest surface information. */
        dstLogical = dst->pixels;
        dstWidth = dst->w;
        dstHeight = dst->h;
        dstFormat = GetRgaFmt (dst->format->BytesPerPixel);
        
        if(dstFormat == RK_FORMAT_UNKNOWN)
        {
            printf("fd_rga format err[%d]... \n",__LINE__,dstFormat);
        
             goto dobycpu;
        }
        clip.xmin = 0;
        clip.xmax = dstWidth - 1;
        clip.ymin = 0;
        clip.ymax = dstHeight - 1;

        RGA_set_dst_vir_info(&Rga_Request,0,  dstLogical, 0, dstWidth, dstHeight, &clip, dstFormat, 0);
        RGA_set_color_fill_mode(&Rga_Request, &FillColor, 0, 0, color, 0, 0, 0, 0, 0);

        Xoffset = rect->x;
        Yoffset = rect->y;
        WidthAct = rect->w;
        HeightAct = rect->h;

        printf_debug("FillHWRect vir[%d,%d] act[%d,%d,%d,%d],addr=%x \n",dst->w,dst->h,rect->x,rect->y,rect->w,rect->h,dstLogical);
        RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
        Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);

        RGA_set_dst_act_info(&Rga_Request, WidthAct, HeightAct, Xoffset, Yoffset);
		
        if (ioctl(fd_rga, RGA_BLIT_SYNC, &Rga_Request) != 0)
        {
            printf("%s(%d):  RGA_BLIT_ASYNC Failed \n", __FUNCTION__, __LINE__);
            goto dobycpu;
        }
        return 0;
    }

dobycpu:    
#endif
    FillRectByCpu(this,dst, rect,  color);
    return 0;
}

static int HWAccelBlit(GAL_Surface *src, GAL_Rect *srcrect,
                       GAL_Surface *dst, GAL_Rect *dstrect)
{

#if Use_Time_Debug
    struct timeval tpend1, tpend2;
    long usec1 = 0;
    gettimeofday(&tpend1, NULL);
#endif

#if USE_RGA_OPTME
    void *              srcLogical  = NULL;
    unsigned int        srcWidth;
    unsigned int        srcHeight;
    RgaSURF_FORMAT      srcFormat;

    void *              dstLogical  = NULL;
    unsigned int        dstWidth;
    unsigned int        dstHeight;
    RgaSURF_FORMAT      dstFormat;
    int Rotation = 0;
    float hfactor;
    float vfactor;
    int  stretch;
    int  yuvFormat;
    struct rga_req  Rga_Request;
    RECT_RGA clip;
    unsigned char RotateMode = 0;
    unsigned int      Xoffset;
    unsigned int      Yoffset;
    unsigned int      WidthAct;
    unsigned int      HeightAct;
    unsigned char   scale_mode = 2;
    unsigned char   dither_en = 0;
    int ZoneCover = 0;
	fprintf(stderr, "%s\n", __func__);
	printf_debug("HWAccelBlit [%d X %d] flag=%x\n",srcrect->w , srcrect->h,dst->flags);
	
    if(((srcrect->w * srcrect->h ) > RGA_MIN_PIXELS)
        && (!(dst->flags & GAL_RLEACCEL)))  // which is done by cpu    
    {

       if(fd_rga < 0)
            fd_rga = open("/dev/rga", O_RDWR, 0);
        if(fd_rga < 0)
        {
            printf("fd_rga failed line=%d... \n",__LINE__);
            goto blitdobycpu;
        }

        dither_en = src->format->BytesPerPixel == dst->format->BytesPerPixel ? 0 : 1;
        memset(&Rga_Request, 0x0, sizeof(Rga_Request));
        srcLogical = src->pixels;
        srcWidth = src->w;
        srcHeight = src->h;
        srcFormat = GetRgaFmt (src->format->BytesPerPixel);

        dstLogical = dst->pixels;
        dstWidth = dst->w;
        dstHeight = dst->h;      
        dstFormat = GetRgaFmt (dst->format->BytesPerPixel);
        clip.xmin = 0;
        clip.xmax = dstWidth - 1;
        clip.ymin = 0;
        clip.ymax = dstHeight - 1;

        printf_debug("src=%p,dst=%p\n",src,dst);
        printf_debug("HWAccelBlit src_info vir[%d,%d] act[%d,%d,%d,%d],addr=%x,fmt=%d,bpp=%d\n",
                src->w,src->h,srcrect->x,srcrect->y,srcrect->w,srcrect->h,srcLogical,srcFormat,src->format->BytesPerPixel);
        printf_debug("HWAccelBlit dst_info vir[%d,%d] act[%d,%d,%d,%d],addr=%x,,fmt=%d,bpp=%d \n",
                dst->w,dst->h,dstrect->x,dstrect->y,dstrect->w,dstrect->h,dstLogical,dstFormat,dst->format->BytesPerPixel);
        
        
        if(srcFormat == RK_FORMAT_UNKNOWN ||dstFormat == RK_FORMAT_UNKNOWN )
        {
            printf("fd_rga format err[%d,%d]... \n",__LINE__,srcFormat,dstFormat);    
            goto blitdobycpu;
        }      
        if(src == dst)
        {
            ZoneCover = IsRectcovered(srcrect,dstrect);
        }
        if(!ZoneCover)
        {
            RGA_set_src_vir_info(&Rga_Request, srcLogical, 0,  0, srcWidth, srcHeight, srcFormat, 0);
            RGA_set_dst_vir_info(&Rga_Request, dstLogical, 0,  0, dstWidth, dstHeight, &clip, dstFormat, 0);
            //RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 1, 3, 0);
            RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
            RGA_set_src_act_info(&Rga_Request, srcrect->w, srcrect->h,  srcrect->x, srcrect->y);
            RGA_set_dst_act_info(&Rga_Request, dstrect->w, dstrect->h,  dstrect->x, dstrect->y);
            RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
            Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
            if (ioctl(fd_rga, RGA_BLIT_SYNC, &Rga_Request) != 0)
            {
                printf("%s(%d):  RGA_BLIT_ASYNC Failed \n", __FUNCTION__, __LINE__);
                goto blitdobycpu;
            }
        }
        else
        {
            printf_debug("ZoneCoverd do by rga \n");
        
            void *pmem = NULL;

           // pmem = (char*)malloc(srcrect->w*srcrect->h*src->format->BytesPerPixel);
            pmem = (void*)((int )srcLogical + 0x7D0000);
            if(NULL == pmem)
            {
                printf("%s(%d): malloc failed \n", __FUNCTION__, __LINE__);
                goto blitdobycpu;

            }
            RGA_set_src_vir_info(&Rga_Request, srcLogical, 0,  0, srcWidth, srcHeight, srcFormat, 0);
            RGA_set_dst_vir_info(&Rga_Request, pmem, 0,  0, srcrect->w, srcrect->h, &clip, srcFormat, 0);
            //RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 1, 3, 0);
            RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
            RGA_set_src_act_info(&Rga_Request, srcrect->w, srcrect->h,  srcrect->x, srcrect->y);
            RGA_set_dst_act_info(&Rga_Request, srcrect->w, srcrect->h, 0,0);
            RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
            Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);
            if (ioctl(fd_rga, RGA_BLIT_ASYNC, &Rga_Request) != 0)
            {
                printf("%s(%d):  RGA_BLIT_ASYNC Failed \n", __FUNCTION__, __LINE__);
                //free(pmem);
                goto blitdobycpu;
            }
            RGA_set_src_vir_info(&Rga_Request, pmem, 0,  0, srcrect->w, srcrect->h, srcFormat, 0);
            RGA_set_dst_vir_info(&Rga_Request, dstLogical, 0,  0, dstWidth, dstHeight, &clip, dstFormat, 0);
            //RGA_set_alpha_en_info(&Rga_Request, 1, 1, 0, 1, 3, 0);
            RGA_set_bitblt_mode(&Rga_Request, scale_mode, RotateMode, Rotation, dither_en, 0, 0);
            RGA_set_src_act_info(&Rga_Request, srcrect->w, srcrect->h, 0,0);
            RGA_set_dst_act_info(&Rga_Request, dstrect->w, dstrect->h,  dstrect->x, dstrect->y);
            RGA_set_mmu_info(&Rga_Request, 1, 0, 0, 0, 0, 2);
            Rga_Request.mmu_info.mmu_flag |= (1 << 31) | (1 << 10) | (1 << 8);

            if (ioctl(fd_rga, RGA_BLIT_SYNC, &Rga_Request) != 0)
            {
                printf("%s(%d):  RGA_BLIT_ASYNC Failed \n", __FUNCTION__, __LINE__);
                //free(pmem);
                goto blitdobycpu;
            }

          
            //free(pmem);
        }
#if Use_Time_Debug        
        gettimeofday(&tpend2, NULL);
        usec1 = 1000 * (tpend2.tv_sec - tpend1.tv_sec) + (tpend2.tv_usec - tpend1.tv_usec) / 1000;
        printf("blit do by rga use time=%ld ms\n",  usec1); 
#endif        
        return 0;
     
    }
blitdobycpu:    
#endif
	GAL_SoftBlit(src, srcrect, dst, dstrect);
	//FB_FlipDisplayBuffer(this, dst);
#if Use_Time_Debug        	
    gettimeofday(&tpend2, NULL);
    usec1 = 1000 * (tpend2.tv_sec - tpend1.tv_sec) + (tpend2.tv_usec - tpend1.tv_usec) / 1000;
    printf("blit do by cpu use time=%ld ms \n",  usec1); 
#endif
    return(0);
}

static int CheckHWBlit(_THIS, GAL_Surface *src, GAL_Surface *dst)
{
    int accelerated;
	printf_debug("CheckHWBlit \n");
	fprintf(stderr, "%s\n", __func__);
    /* Set initial acceleration on */
    src->flags |= GAL_HWACCEL;

    /* Set the surface attributes */
    if ( (src->flags & GAL_SRCALPHA) == GAL_SRCALPHA ) {
        if ( ! this->info.blit_hw_A ) {
            src->flags &= ~GAL_HWACCEL;
        }
    }
    if ( (src->flags & GAL_SRCCOLORKEY) == GAL_SRCCOLORKEY ) {
        if ( ! this->info.blit_hw_CC ) {
            src->flags &= ~GAL_HWACCEL;
        }
    }
    if ( (src->flags & GAL_SRCPIXELALPHA) == GAL_SRCPIXELALPHA ) {
        src->flags &= ~GAL_HWACCEL;
    }
    /* Check to see if final surface blit is accelerated */
    accelerated = !!(src->flags & GAL_HWACCEL);
    if ( accelerated ) {
        src->map->hw_blit = HWAccelBlit;
    }
    return(accelerated);
}

#if 0
/* Useful for determining the video hardware capabilities */
typedef struct {
    Uint32 hw_available :1;         /* Flag: Can you create hardware surfaces? */
    Uint32 mlt_surfaces :1;         /* Flag: Does VideoInit return different surfaces? */
    Uint32 UnusedBits   :8;         /* Flag: Can you talk to a window manager? */
    Uint32 blit_hw      :1;         /* Flag: Accelerated blits HW --> HW */
    Uint32 blit_hw_CC   :1;         /* Flag: Accelerated blits with Colorkey */
    Uint32 blit_hw_A    :1;         /* Flag: Accelerated blits with Alpha */
    Uint32 blit_sw      :1;         /* Flag: Accelerated blits SW --> HW */
    Uint32 blit_sw_CC   :1;         /* Flag: Accelerated blits with Colorkey */
    Uint32 blit_sw_A    :1;         /* Flag: Accelerated blits with Alpha */
    Uint32 blit_fill    :1;         /* Flag: Accelerated color fill */
    Uint32 UnusedBits3  :16;
    Uint32 video_mem;               /* The total amount of video memory (in K) */
    GAL_PixelFormat *vfmt;          /* Value: The format of the video surface */
} GAL_VideoInfo;
#endif

void FB_RkAccel(_THIS)
{
	fprintf(stderr, "%s\n", __func__);
    /* We have hardware accelerated surface functions */
    this->CheckHWBlit = CheckHWBlit;

    /* has an accelerated color fill  */
    this->info.blit_fill = 1;
    this->FillHWRect = FillHWRect;

    /* has accelerated normal and colorkey blits. */
    this->info.blit_hw = 1;
    this->info.blit_hw_A = 0;
    this->info.blit_hw_CC = 0;	
    this->SetHWColorKey = 0;//SetHWColorKey;

    this->info.blit_sw = 1;
    this->info.blit_sw_A = 0;
    this->info.blit_sw_CC = 0;	

#if 0 
    this->SetHWAlpha = SetHWAlpha;
#endif
}
     

