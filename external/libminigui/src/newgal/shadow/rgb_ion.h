#ifndef __RGB_ION_H__
#define __RGB_ION_H__

#include "ion/ion.h"

struct rgb_ion {
  int client;
  int width;
  int height;
  void* buffer;
  int fd;
  ion_user_handle_t handle;
  size_t size;
  unsigned long phys;
};

int rgb_ion_alloc(struct rgb_ion* rgb_ion, int length);
int rgb_ion_free(struct rgb_ion* rgb_ion);

#endif
