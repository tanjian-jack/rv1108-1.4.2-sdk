#include "rgb_ion.h"

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "ion/ion.h"
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>

int rgb_ion_alloc(struct rgb_ion* rgb_ion, int length) {
  rgb_ion->client = ion_open();
  if (rgb_ion->client <= 0) {
    printf("%s:open /dev/ion failed!\n", __func__);
    return -1;
  }

  rgb_ion->size = length;
  printf("%s size = %d\n", __func__, rgb_ion->size);
  int ret = ion_alloc(rgb_ion->client, rgb_ion->size, 0,
                      ION_HEAP_TYPE_DMA_MASK, 0, &rgb_ion->handle);
  if (ret) {
    printf("ion_alloc failed!\n");
    return -1;
  }
  ret = ion_share(rgb_ion->client, rgb_ion->handle, &rgb_ion->fd);
  if (ret) {
    printf("ion_share failed!\n");
    return -1;
  }
  ion_get_phys(rgb_ion->client, rgb_ion->handle, &rgb_ion->phys);
  rgb_ion->buffer = mmap(NULL, rgb_ion->size, PROT_READ | PROT_WRITE,
                           MAP_SHARED | MAP_LOCKED, rgb_ion->fd, 0);
  if (!rgb_ion->buffer) {
    printf("%s mmap failed!\n", __func__);
    return -1;
  }

  return 0;
}

int rgb_ion_free(struct rgb_ion* rgb_ion) {
  int ret = 0;

  if (rgb_ion->buffer) {
    munmap(rgb_ion->buffer, rgb_ion->size);
    rgb_ion->buffer = NULL;
  }

  if (rgb_ion->fd) {
    close(rgb_ion->fd);
    rgb_ion->fd = 0;
  }

  if (rgb_ion->client) {
    if (rgb_ion->handle) {
      ret = ion_free(rgb_ion->client, rgb_ion->handle);
      if (ret)
        printf("ion_free failed!\n");
      rgb_ion->handle = 0;
    }

    ion_close(rgb_ion->client);
    rgb_ion->client = 0;
  }

  memset(rgb_ion, 0, sizeof(struct rgb_ion));

  return ret;
}

