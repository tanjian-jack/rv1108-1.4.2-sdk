#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path

if [ "$rebuild" == "yes"  ]; then
    make distclean
    rm Makefile
    [ -f autom4te.cache/requests  ] && rm autom4te.cache/requests
fi

#for  project config
result=$(find ./ -maxdepth 1 -name Makefile)
if [ "$result" = "" ];then
	echo $1" do configure"
	sh $(pwd)/autoconfig.sh
fi

cpu_cores=`cat /proc/cpuinfo | grep processor | wc -l`
make -j$cpu_cores
make install
INSTALL_INC_DIR=$(pwd)/../../out/system/include/minigui
[ ! -f $INSTALL_INC_DIR ] && mkdir -p $INSTALL_INC_DIR
cp mgconfig.h $INSTALL_INC_DIR

cd $current_path
