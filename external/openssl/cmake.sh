#! /bin/bash
current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target" ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path

if [ "$rebuild" == "yes" ]; then
    make clean
    ./Configure linux-generic32 --prefix=$(pwd)/../../out/system/ \
	    shared no-zlib no-asm no-threads no-sse2 no-compno-gmp no-rfc3779 \
	    no-krb5 no-rc5 no-zlib-dynamic no-hw no-cipher no-md2 no-mdc2no-rc2 \
	    no-idea no-camellia no-ec no-ecdsa no-ecdh no-store no-ripemd \
	    no-rc4no-bf no-cast no-dsa no-ssl no-ssl2 no-ssl3 no-perlasm
fi

cpu_cores=`cat /proc/cpuinfo | grep processor | wc -l`
make
make install_sw
rm -f $(pwd)/../../out/system/lib/libssl.a
rm -f $(pwd)/../../out/system/lib/libcrypto.a

cd $current_path
