#ifndef _NVTK_GPS_H_
#define _NVTK_GPS_H_
#include <stdio.h>
#include <stdint.h>

enum gsensor_orient {
    GSENSOR_ORIENT_0,
    GSENSOR_ORIENT_90,
    GSENSOR_ORIENT_180,
    GSENSOR_ORIENT_270,
};

enum gsensor_shake {
    GSENSOR_SHAKE_LEFT,
    GSENSOR_SHAKE_RIGHT,
};

struct rmc_info {
    int32_t time_hour;
    int32_t time_min;
    int32_t time_sec;
    int32_t year;
    int32_t month;
    int32_t day;
    int8_t status;
    int8_t ns_ind;
    int8_t ew_ind;
    int8_t reserved0;
    float latitude;
    float longitude;
    float speed;
    float angle;
};

struct axis_data {
    uint32_t x_acc;
    uint32_t y_acc;
    uint32_t z_acc;
};

struct gsensor_data {
    struct axis_data axis;
    enum gsensor_orient orient;
    enum gsensor_orient shake;
};

struct nvtk_gps_data {
    uint32_t reserved0;
    uint8_t iq_ver[16];
    uint8_t iq_build_data[16];
    struct rmc_info rmc_info;
    struct gsensor_data gs_data;
};

int mov_gps_cache_init (AVFormatContext *s);
int mov_gps_cache_add_record (AVFormatContext *s, void *data);

#endif
