#include "avio.h"
#include "avio_internal.h"
#include "movenc.h"
#include "avformat.h"
#include "nvtk_gps.h"
#include "movenc_gps.h"

#define MAX_GPS_RECORD 255

//FIXME support 64 bit variant with wide placeholders
static int64_t update_size(AVIOContext *pb, int64_t pos)
{
    int64_t curpos = avio_tell(pb);
    avio_seek(pb, pos, SEEK_SET);
    avio_wb32(pb, curpos - pos); /* rewrite size */
    avio_seek(pb, curpos, SEEK_SET);

    return curpos - pos;
}

static int mov_write_nvtk_gpsdate(AVIOContext *pb, struct nvtk_gps_data *data)
{
    uint8_t *p = NULL;
    int len;
    strncpy(data->iq_ver, "GPS test", sizeof(data->iq_ver));

    len = sizeof(*data);
    if (len > 0) {
        int size = len + 12;

        avio_wb32(pb, size);
        ffio_wfourcc(pb, "free");
        ffio_wfourcc(pb, "GPS ");
        avio_write(pb, (uint8_t *)data, len);

        return size;
    }
    return 0;
}

static int mov_write_gpsdate_tag(MOVMuxContext *mov, AVIOContext *pb,
                                 AVIOContext *pb_index, int64_t udta_pos)
{
    int offset = 0;
    int64_t pos;
    int i;

    pos = avio_tell(pb);
    avio_wb32(pb, 0); /* size placeholder */
    ffio_wfourcc(pb, "GPS+");
    offset = rk_avio_get_offset(pb);
    for (i = 0; i < mov->gps_cache.nb_records; i++) {
        int pos, size;
        struct nvtk_gps_data *data = NULL;

        pos = avio_tell(pb) + offset;
        data = (mov->gps_cache.data);
        data += i;
        if (data == NULL) {
            i--;
            break;
        }
        size = mov_write_nvtk_gpsdate (pb, data);
        if (size > 0) {
            avio_wb32(pb_index, pos);
            avio_wb32(pb_index, size);
        }
    }

    update_size(pb, pos);
    update_size(pb, udta_pos);

    return i;
}

int mov_gps_cache_init (AVFormatContext *s)
{
    MOVMuxContext *mov = s->priv_data;

    mov->gps_cache.data = calloc (MAX_GPS_RECORD, sizeof(struct nvtk_gps_data));
    if (mov->gps_cache.data != NULL) {
        mov->gps_cache.size = MAX_GPS_RECORD * sizeof(struct nvtk_gps_data);
    } else {
        mov->gps_cache.size = 0;

        return -ENOMEM;
    }
    mov->gps_cache.nb_records = 0;

    return 0;
}

int mov_gps_cache_deinit (AVFormatContext *s)
{
    MOVMuxContext *mov = s->priv_data;

    free (mov->gps_cache.data);
    mov->gps_cache.size = 0;
    mov->gps_cache.nb_records = 0;

    return 0;
}

int mov_gps_cache_add_record (AVFormatContext *s, void *data)
{
    MOVMuxContext *mov = s->priv_data;
    struct nvtk_gps_data *nvtk_data = (struct nvtk_gps_data *)data;
    struct nvtk_gps_data *cur = mov->gps_cache.data;

    cur += mov->gps_cache.nb_records;
    if (nvtk_data == NULL || cur == NULL)
        return -EINVAL;

    memcpy(cur, nvtk_data, sizeof(*nvtk_data));
    mov->gps_cache.nb_records++;

    return 0;
}

int mov_write_gps_tag(AVIOContext *pb, MOVMuxContext *mov,
                      AVFormatContext *s, int64_t pos)
{
    AVIOContext *pb_buf;
    int ret, size;
    uint8_t *buf;

    ret = avio_open_dyn_buf(&pb_buf);
    if (ret < 0)
        return ret;

    ret = mov_write_gpsdate_tag(mov, pb, pb_buf, pos);

    if ((size = avio_close_dyn_buf(pb_buf, &buf)) > 0) {
        avio_wb32(pb, size + 16);
        ffio_wfourcc(pb, "gps ");
        /* GPS data version */
        avio_wb32(pb, 0x101);
        /* numbers of gps data */
        avio_wb32(pb, ret);
        avio_write(pb, buf, size);
    }
    av_free(buf);

    return 0;
}
