#ifndef _MOVENC_GPS_H_
#define _MOVENC_GPS_H_

/* Internal */
int mov_write_gps_tag(AVIOContext *pb, MOVMuxContext *mov,
                      AVFormatContext *s, int64_t pos);
int mov_gps_cache_deinit (AVFormatContext *s);
#endif
