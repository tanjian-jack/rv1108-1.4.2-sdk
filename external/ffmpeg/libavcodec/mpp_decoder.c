#include <stdbool.h>
#include <unistd.h>

#include <libavutil/time.h>
#include <cvr_ffmpeg_shared.h>
#include "avcodec.h"
#include "internal.h"
#include "libavutil/avassert.h"
#include "h264.h"

#include <mpp/mpp_buffer.h>
#include <mpp/mpp_err.h>
#include <mpp/mpp_frame.h>
#include <mpp/mpp_meta.h>
#include <mpp/mpp_packet.h>
#include <mpp/mpp_task.h>
#include <mpp/rk_mpi.h>
#include <mpp/rk_type.h>

typedef struct _RkMppDecContext {
    MppCodingType video_type;
    MppCtx ctx;
    MppApi *mpi;
    MppPacket packet;
    MppFrame frame;
} RkMppDecContext;

static bool mpp_hw_support(enum AVCodecID codec_id, MppCtxType type)
{
    switch (type) {
    case MPP_CTX_DEC:
        switch (codec_id) {
        case AV_CODEC_ID_H264:
        case AV_CODEC_ID_MJPEG:
            return true;
        default:
            return false;
        }
        break;

    case MPP_CTX_ENC:
        switch (codec_id) {
        case AV_CODEC_ID_H264:
            return true;
        default:
            return false;
        }
        break;
    }

    return false;
}

static int rkdec_prepare(AVCodecContext * avctx)
{
    RkMppDecContext *rkdec_ctx = avctx->priv_data;

    MpiCmd mpi_cmd = MPP_CMD_BASE;
    MppParam param = NULL;
    uint32_t need_split = 1;
    MppPollType block = MPP_POLL_BLOCK;
    int64_t timeout = 80;
    int ret;

    ret = mpp_create(&rkdec_ctx->ctx, &rkdec_ctx->mpi);
    if (ret != 0) {
        av_log(avctx, AV_LOG_ERROR, "mpp create failed");

        return -1;
    }
    mpi_cmd = MPP_DEC_SET_PARSER_SPLIT_MODE;
    param = &need_split;
    ret = rkdec_ctx->mpi->control(rkdec_ctx->ctx, mpi_cmd, param);
    if (ret != 0) {
        av_log(avctx, AV_LOG_ERROR, "mpp control %d failed", mpi_cmd);
        mpp_destroy(rkdec_ctx->ctx);

        return -1;
    }

    ret = mpp_init(rkdec_ctx->ctx, MPP_CTX_DEC, rkdec_ctx->video_type);
    if (ret != 0) {
        av_log(avctx, AV_LOG_ERROR, "mpp init failed");
        mpp_destroy(rkdec_ctx->ctx);

        return -1;
    }

    mpi_cmd = MPP_SET_OUTPUT_BLOCK;
    param = &block;
    ret = rkdec_ctx->mpi->control(rkdec_ctx->ctx, mpi_cmd, param);

    mpi_cmd = MPP_SET_OUTPUT_BLOCK_TIMEOUT;
    param = &timeout;
    ret = rkdec_ctx->mpi->control(rkdec_ctx->ctx, mpi_cmd, param);

    if (avctx->extradata != NULL && avctx->extradata_size != 0) {
        mpp_packet_init(&rkdec_ctx->packet, avctx->extradata,
                        avctx->extradata_size);
        mpp_packet_set_extra_data(rkdec_ctx->packet);
        rkdec_ctx->mpi->decode_put_packet(rkdec_ctx->ctx,
                                          rkdec_ctx->packet);
        mpp_packet_deinit(&rkdec_ctx->packet);
        rkdec_ctx->packet = NULL;
    }

    return 0;
}

static int rkdec_init(AVCodecContext * avctx)
{
    RkMppDecContext *rkdec_ctx = avctx->priv_data;

    memset(rkdec_ctx, 0, sizeof(*rkdec_ctx));

    if (!mpp_hw_support(avctx->codec_id, MPP_CTX_DEC)) {
        return -1;
    }

    memset(rkdec_ctx, 0, sizeof(RkMppDecContext));

    rkdec_ctx->video_type = MPP_VIDEO_CodingUnused;
    switch (avctx->codec_id) {
    case AV_CODEC_ID_H264:
        rkdec_ctx->video_type = MPP_VIDEO_CodingAVC;
        break;
    case AV_CODEC_ID_MJPEG:
        rkdec_ctx->video_type = MPP_VIDEO_CodingMJPEG;
        break;
    default:
        av_log(avctx, AV_LOG_ERROR, "codec id %d is not supported",
               avctx->codec_id);
        break;
    }
    avctx->pix_fmt = AV_PIX_FMT_NV12;

    return rkdec_prepare(avctx);
}

static int rkdec_deinit(AVCodecContext * avctx)
{

    RkMppDecContext *rkdec_ctx = avctx->priv_data;

    rkdec_ctx->mpi->reset(rkdec_ctx->ctx);

    mpp_destroy(rkdec_ctx->ctx);

    return 0;
}

static void free_mem_from_mpp(void *ctx, void *mem)
{
    RkMppDecContext *rkdec_ctx = (RkMppDecContext *) ctx;
    MppFrame frame = (MppFrame) ((DataBuffer_t *) mem)->handle;
    if (rkdec_ctx && frame)
        mpp_frame_deinit(&frame);
}

static int rkdec_decode_frame(AVCodecContext * avctx, void *data,
                              int *got_frame, AVPacket * packet)
{
    RkMppDecContext *rkdec_ctx = avctx->priv_data;
    int ret = 0;
    AVFrame *frame = (AVFrame *) data;

    rkdec_ctx->frame = NULL;
    if (packet->data && packet->size != 0) {
        ret = mpp_packet_init(&rkdec_ctx->packet, packet->data, packet->size);
        if (ret) {
            av_log(avctx, AV_LOG_ERROR, "mpp packet init failed!\n");
            return ret;
        }

        if (packet->pts != AV_NOPTS_VALUE) {
            mpp_packet_set_pts(rkdec_ctx->packet,
                               av_q2d(avctx->time_base) * (packet->pts) *
                               1000000ll);
        } else {
            mpp_packet_set_pts(rkdec_ctx->packet,
                               av_q2d(avctx->time_base) * (packet->dts) *
                               1000000ll);
        }

        ret =
            rkdec_ctx->mpi->decode_put_packet(rkdec_ctx->ctx,
                                              rkdec_ctx->packet);
        if (ret)
            goto error;
        mpp_packet_deinit(&rkdec_ctx->packet);
    }

    do {
        ret = rkdec_ctx->mpi->decode_get_frame(rkdec_ctx->ctx,
                                               &rkdec_ctx->frame);
        if (ret) {
            av_log(avctx, AV_LOG_WARNING, "decode get frame failed!\n");
            goto error;
        }

        if (rkdec_ctx->frame) {
            image_handle handle = NULL;
            MppBuffer *buffer = mpp_frame_get_buffer(rkdec_ctx->frame);

            if (mpp_frame_get_info_change(rkdec_ctx->frame)) {
                av_log(avctx, AV_LOG_INFO,
                       "decode_get_frame get info changed found\n");
                rkdec_ctx->mpi->control(rkdec_ctx->ctx,
                                        MPP_DEC_SET_INFO_CHANGE_READY,
                                        NULL);
                avctx->coded_width =
                    mpp_frame_get_hor_stride(rkdec_ctx->frame);
                avctx->coded_height =
                    mpp_frame_get_ver_stride(rkdec_ctx->frame);
                if (avctx->width == 0 || avctx->height == 0) {
                    avctx->width = mpp_frame_get_width(rkdec_ctx->frame);
                    avctx->height = mpp_frame_get_height(rkdec_ctx->frame);
                }
                mpp_frame_deinit(&rkdec_ctx->frame);
                goto done;
            }

            if (!buffer) {
                ret = AVERROR_EOF;
                goto error;
            }

            *got_frame = 1;
            if (mpp_frame_get_discard(rkdec_ctx->frame)
                || mpp_frame_get_errinfo(rkdec_ctx->frame)) {
                *got_frame = 0;
                av_log(avctx, AV_LOG_WARNING, "fault in the frame, drop\n");
                mpp_frame_deinit(&rkdec_ctx->frame);
                break;
            } else {
                ret = ff_decode_frame_props(avctx, frame);
                if (ret < 0) {
                    av_log(avctx, AV_LOG_WARNING,
                           "Failed to props buffer!!!:%d!\n", ret);
                    goto error;
                }
                handle = (image_handle) frame->data[3];
            }

            switch (frame->format) {
            case AV_PIX_FMT_NV12:
                if (handle) {
                    handle->image_buf.phy_fd = mpp_buffer_get_fd(buffer);
                    handle->image_buf.handle = rkdec_ctx->frame;
                    handle->image_buf.vir_addr =
                        mpp_buffer_get_ptr(buffer);
                    handle->image_buf.buf_size = mpp_buffer_get_size(buffer);
                    handle->rkdec_ctx = rkdec_ctx;
                    handle->free_func = free_mem_from_mpp;
                } else if (frame->data[0]) {
                    uint8_t *tmpBuffer = mpp_buffer_get_ptr(buffer);

                    if (tmpBuffer == NULL)
                        goto error;

                    memcpy(frame->data[0], tmpBuffer,
                           frame->linesize[0] * frame->height);
                    tmpBuffer =
                        tmpBuffer +
                        avctx->coded_width * avctx->coded_height;
                    memcpy(frame->data[1], tmpBuffer,
                           (frame->linesize[1] * frame->height) >> 1);
                    mpp_frame_deinit(&rkdec_ctx->frame);
                } else {
                    mpp_frame_deinit(&rkdec_ctx->frame);
                }
                break;
            default:
                av_log(avctx, AV_LOG_ERROR,
                       "frame format %d is not support!\n", frame->format);
                goto error;
            }
            break;
        } else {
            av_log(avctx, AV_LOG_WARNING, "empty frame\n");
            ret = AVERROR_UNKNOWN;
            goto error;
        }
    } while (1);

done:
    return packet->size;

error:
    if (rkdec_ctx->frame != NULL)
        mpp_frame_deinit(&rkdec_ctx->frame);
    if (rkdec_ctx->packet != NULL)
        mpp_packet_deinit(&rkdec_ctx->packet);
    return ret;
}


static void rkdec_decode_flush(AVCodecContext * avctx)
{
    RkMppDecContext *rkdec_ctx = avctx->priv_data;

    rkdec_ctx->mpi->reset(rkdec_ctx->ctx);
}

#define DECLARE_RK_MPP_DEC_VIDEO_DECODER(TYPE, CODEC_ID)                     	\
    AVCodec ff_##TYPE##_decoder = {                                         \
        .name           = #TYPE,                                            \
        .long_name      = NULL_IF_CONFIG_SMALL(#TYPE " hw decoder"),        \
        .type           = AVMEDIA_TYPE_VIDEO,                               \
        .id             = CODEC_ID,                                         \
        .priv_data_size = sizeof(RkMppDecContext),                          \
        .init           = rkdec_init,                                       \
        .close          = rkdec_deinit,                                     \
        .decode         = rkdec_decode_frame,                               \
        .capabilities   = CODEC_CAP_DELAY,                                  \
        .flush          = rkdec_decode_flush,                               \
        .pix_fmts       = (const enum AVPixelFormat[]) {                    \
            AV_PIX_FMT_NV12,                        \
            AV_PIX_FMT_NONE							\
        },									\
    };

DECLARE_RK_MPP_DEC_VIDEO_DECODER(h264_mpp, AV_CODEC_ID_H264)
