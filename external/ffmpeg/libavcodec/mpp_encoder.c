#include <unistd.h>
#include "avcodec.h"
#include "internal.h"
#include "mpegvideo.h"

#include <cvr_ffmpeg_shared.h>

#include <mpp/rk_mpi.h>

#define DIV3_TAG 861292868

typedef struct _RkMppEncContext {
	MppCodingType				video_type;
	MppCtx						ctx;
	MppApi						*mpi;
	MppPacket					packet;
	MppFrame					frame;
	MppBuffer					osd_data;
} RkMppEncContext;


static MppFrameFormat rkenc_ffmpeg2vpu_color_space(AVCodecContext *avctx)
{
	switch (avctx->pix_fmt) {
	case AV_PIX_FMT_YUV420P:
		return MPP_FMT_YUV420P;
	case AV_PIX_FMT_NV12:
		return MPP_FMT_YUV420SP;
	case AV_PIX_FMT_NV21:
		return MPP_FMT_YUV420SP_VU;
	case AV_PIX_FMT_YUV422P:
		return MPP_FMT_YUV422P;
	case AV_PIX_FMT_NV16:
		return MPP_FMT_YUV422SP;
	case AV_PIX_FMT_NV61:
		return MPP_FMT_YUV422SP_VU;
	case AV_PIX_FMT_YVYU422:
		return MPP_FMT_YUV422_YUYV;
	case AV_PIX_FMT_UYVY422:
		return MPP_FMT_YUV422_UYVY;
	case AV_PIX_FMT_RGB565LE:
		return MPP_FMT_RGB565;
	case AV_PIX_FMT_BGR565LE:
		return MPP_FMT_BGR565;
	case AV_PIX_FMT_RGB24:
		return MPP_FMT_RGB888;
	case AV_PIX_FMT_BGR24:
		return MPP_FMT_BGR888;
	case AV_PIX_FMT_RGB32:
		return MPP_FMT_ARGB8888;
	case AV_PIX_FMT_BGR32:
		return MPP_FMT_ABGR8888;
	default:
		av_log(avctx, AV_LOG_ERROR, "unsupport compress format %d", avctx->pix_fmt);
		break;
	}
	return -1;
}

static int rkenc_prepare(AVCodecContext *avctx)
{
	RkMppEncContext *rkenc_ctx = avctx->priv_data;
	MppFrameFormat pic_type;
	MpiCmd mpi_cmd = MPP_CMD_BASE;
	MppParam param = NULL;
	MppEncCfgSet cfg;
	MppEncRcCfg *rc_cfg = &cfg.rc;
	MppEncPrepCfg *prep_cfg = &cfg.prep;
	MppEncCodecCfg *codec_cfg = &cfg.codec;
	unsigned int need_block = 1;
	int profile = 100;
	int ret;
	int fps_in = avctx->time_base.den / avctx->time_base.num;
	int bps = avctx->bit_rate;
#ifdef DEBUG
	int debug_bit_rate = 0;
	int debug_rc_mode = 0;
	int debug_qp = 0;
	FILE *f = fopen("/tmp/ffmpeg_debug_config", "r");
	if (f) {
		fscanf(f, "%d %d %d", &debug_bit_rate, &debug_rc_mode, &debug_qp);
		fclose(f);
	} else {
		printf("ffmpeg debug mode, /tmp/ffmpeg_debug_config is not exist!\n");
		av_assert0(0);
	}
	printf("debug_bit_rate:%d, debug_rc_mode:%d, debug_qb: %d\n", debug_bit_rate,
	       debug_rc_mode, debug_qp);
#endif

	if (fps_in < 1)
		fps_in = 1;
	else if (fps_in > ((1 << 16) - 1))
		fps_in = (1 << 16) - 1;

	// encoder demo
	ret = mpp_create(&rkenc_ctx->ctx, &rkenc_ctx->mpi);

	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpp_create failed\n");
		goto RET;
	}

	mpi_cmd = MPP_SET_INPUT_BLOCK;
	param = &need_block;
	ret = rkenc_ctx->mpi->control(rkenc_ctx->ctx, mpi_cmd, param);
	if (ret != 0) {
		av_log(avctx, AV_LOG_ERROR, "mpp control %d failed\n", mpi_cmd);
		goto RET;
	}

	ret = mpp_init(rkenc_ctx->ctx, MPP_CTX_ENC, rkenc_ctx->video_type);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpp_init failed\n");
		goto RET;
	}

	pic_type = rkenc_ffmpeg2vpu_color_space(avctx);
	if (pic_type == -1) {
		av_log(avctx, AV_LOG_ERROR, "error input pixel format!\n");
		ret = -1;
		goto RET;
	}
	/* NOTE: in mpp, chroma_addr = luma_addr + hor_stride*ver_stride,
	 * so if there is no gap between luma and chroma in yuv,
	 * hor_stride MUST be set to be width, ver_stride MUST be set to height.
	 */
	prep_cfg->change = MPP_ENC_PREP_CFG_CHANGE_INPUT |
			   MPP_ENC_PREP_CFG_CHANGE_FORMAT;
	prep_cfg->width         = avctx->width;
	prep_cfg->height        = avctx->height;
	prep_cfg->hor_stride    = avctx->width;
	prep_cfg->ver_stride    = avctx->height;
	prep_cfg->format        = pic_type;
	ret = rkenc_ctx->mpi->control(rkenc_ctx->ctx, MPP_ENC_SET_PREP_CFG, prep_cfg);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpi control enc set prep cfg failed ret %d\n",
		       ret);
		goto RET;
	}

	rc_cfg->change  = MPP_ENC_RC_CFG_CHANGE_ALL;
	/*
	 * rc_mode - rate control mode
	 * Mpp balances quality and bit rate by the mode index
	 * Mpp provide 5 level of balance mode of quality and bit rate
	 * 1 - only quality mode: only quality parameter takes effect
	 * 2 - more quality mode: quality parameter takes more effect
	 * 3 - balance mode     : balance quality and bitrate 50 to 50
	 * 4 - more bitrate mode: bitrate parameter takes more effect
	 * 5 - only bitrate mode: only bitrate parameter takes effect
	 */
	rc_cfg->rc_mode = avctx->compression_level;
	/*
	 * quality - quality parameter
	 * mpp does not give the direct parameter in different protocol.
	 * mpp provide total 5 quality level 1 ~ 5
	 * 0 - auto
	 * 1 - worst
	 * 2 - worse
	 * 3 - medium
	 * 4 - better
	 * 5 - best
	 */
	rc_cfg->quality = avctx->global_quality;
	switch (rc_cfg->rc_mode) {
	case MPP_ENC_RC_MODE_CBR:
		// constant bitrate has very small bps range of 1/16 bps
		rc_cfg->bps_target = bps;
		rc_cfg->bps_max = bps * 17 / 16;
		rc_cfg->bps_min = bps * 15 / 16;
		break;
	case MPP_ENC_RC_MODE_VBR:
		// variable bitrate has large bps range
		rc_cfg->bps_target = bps;
		rc_cfg->bps_max = bps * 3 / 2;
		rc_cfg->bps_min = bps * 1 / 2;
		break;
	default:
		// TODO
		printf("right now rc_mode=%d is untested\n", rc_cfg->rc_mode);
	}

	/* fix input / output frame rate */
	rc_cfg->fps_in_flex      = 0;
	rc_cfg->fps_in_num       = fps_in;
	rc_cfg->fps_in_denorm    = 1;
	rc_cfg->fps_out_flex     = 0;
	rc_cfg->fps_out_num      = fps_in;
	rc_cfg->fps_out_denorm   = 1;

	rc_cfg->gop              = avctx->gop_size;
	rc_cfg->skip_cnt         = 0;

	ret = rkenc_ctx->mpi->control(rkenc_ctx->ctx, MPP_ENC_SET_RC_CFG, rc_cfg);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpi control enc set rc cfg failed ret %d\n", ret);
		goto RET;
	}

	codec_cfg->h264.change = MPP_ENC_H264_CFG_CHANGE_PROFILE |
				 MPP_ENC_H264_CFG_CHANGE_ENTROPY;
	profile = avctx->profile;
	if (profile != 66 && profile != 77) {
		/* default PROFILE_HIGH 100 */
		profile = 100;
	}

	codec_cfg->h264.profile  = profile;
	codec_cfg->h264.level    = avctx->level;
	codec_cfg->h264.entropy_coding_mode = (profile == 66 ||
					       profile == 77) ? (0) : (1);
	codec_cfg->h264.cabac_init_idc  = 0;
	// setup QP on CQP mode
	codec_cfg->h264.change |= MPP_ENC_H264_CFG_CHANGE_QP_LIMIT;
	if (rc_cfg->rc_mode == MPP_ENC_RC_MODE_CBR) {
		codec_cfg->h264.qp_init = avctx->qval;
		codec_cfg->h264.qp_min = avctx->qmin;
		codec_cfg->h264.qp_max = avctx->qmax;
		codec_cfg->h264.qp_max_step = avctx->max_qdiff;
	}
	ret = rkenc_ctx->mpi->control(rkenc_ctx->ctx, MPP_ENC_SET_CODEC_CFG, codec_cfg);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpi control enc set codec cfg failed ret %d\n",
		       ret);
		goto RET;
	}

	ret = rkenc_ctx->mpi->control(rkenc_ctx->ctx, MPP_ENC_GET_EXTRA_INFO,
				      &rkenc_ctx->packet);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpi control enc get extra info failed\n");
		goto RET;
	}

	/* get and write sps/pps for H.264 */
	if (rkenc_ctx->packet) {
		void *ptr   = mpp_packet_get_pos(rkenc_ctx->packet);
		size_t len  = mpp_packet_get_length(rkenc_ctx->packet);

		if (avctx->extradata != NULL) {
			av_free(avctx->extradata);
		}
		avctx->extradata = av_malloc(len);
		if (avctx->extradata == NULL) {
			av_log(avctx, AV_LOG_ERROR, "extradata malloc failed\n");
			ret = -1;
			goto RET;
		}
		avctx->extradata_size = len;
		memcpy(avctx->extradata, ptr, len);

		rkenc_ctx->packet = NULL;
	}
RET:
	if (ret) {
		if (rkenc_ctx->ctx) {
			mpp_destroy(rkenc_ctx->ctx);
			rkenc_ctx->ctx = NULL;
		}
		rkenc_ctx->mpi = NULL;
	}

	return ret;
}

static int rkenc_init(AVCodecContext *avctx)
{
	RkMppEncContext *rkenc_ctx = avctx->priv_data;

	switch (avctx->codec_id) {
	case AV_CODEC_ID_H264:
		break;
	default:
		av_log(avctx, AV_LOG_ERROR, "unspport codec %d\n", avctx->codec_id);
		return -1;
	}

	memset(rkenc_ctx, 0, sizeof(RkMppEncContext));

	rkenc_ctx->video_type = MPP_VIDEO_CodingUnused;
	switch (avctx->codec_id) {
	case AV_CODEC_ID_H264:
		rkenc_ctx->video_type = MPP_VIDEO_CodingAVC;
		break;
	default:
		av_log(avctx, AV_LOG_ERROR, "codec id %d is not supported\n", avctx->codec_id);
		return -1;
	}

	return rkenc_prepare(avctx);
}

static int rkenc_deinit(AVCodecContext *avctx)
{
	RkMppEncContext *rkenc_ctx = avctx->priv_data;

	mpp_destroy(rkenc_ctx->ctx);

	return 0;
}

static int rkenc_ffmpeg2vpu_get_buf_size(AVCodecContext *avctx,
		const AVFrame *frame, int *size)
{
	int w_align;
	int h_align;
	//w_align = (frame->width + 15) & (~15);
	//h_align = (frame->height + 15) & (~15);
	w_align = frame->width;
	h_align = frame->height;

	switch (frame->format) {
	case AV_PIX_FMT_YUV420P:
	case AV_PIX_FMT_NV12:
	case AV_PIX_FMT_NV21:
		*size = w_align * h_align * 3 / 2;
		return 0;
	case AV_PIX_FMT_YUV422P:
	case AV_PIX_FMT_NV16:
	case AV_PIX_FMT_NV61:
	case AV_PIX_FMT_YVYU422:
	case AV_PIX_FMT_UYVY422:
		*size = w_align * h_align * 2;
		return 0;
	case AV_PIX_FMT_RGB565LE:
	case AV_PIX_FMT_BGR565LE:
		*size = w_align * h_align * 2;
		return 0;
	case AV_PIX_FMT_RGB24:
	case AV_PIX_FMT_BGR24:
		*size = w_align * h_align * 3;
		return 0;
	case AV_PIX_FMT_RGB32:
	case AV_PIX_FMT_BGR32:
		*size = w_align * h_align * 4;
		return 0;
	default:
		av_log(avctx, AV_LOG_ERROR, "unsupport compress format %d\n", frame->format);
		break;
	}

	*size = 0;
	return -1;
}

static int rkenc_encode_frame(AVCodecContext *avctx, AVPacket *packet,
			      const AVFrame *frame, int *got_packet)
{
	int ret = 0;
	RkMppEncContext *rkenc_ctx = avctx->priv_data;
	DataBuffer_t *pkt_data = NULL;
	DataBuffer_t *mv_data = NULL;
	DataBuffer_t *frame_data = NULL;
	MppBuffer	pic_buf = NULL;
	MppBuffer	str_buf = NULL;
	MppBuffer   mv_buf = NULL;
	MppTask task = NULL;
	MppBufferInfo info;

	*got_packet = 0;
	if (!frame) {
		return 0;
	}

	pkt_data = (DataBuffer_t *)packet->user_reserve_buf;
	frame_data = (DataBuffer_t *)frame->user_reserve_buf;
	mv_data = (DataBuffer_t *)packet->user_reserve_buf + 1;

	mpp_frame_init(&rkenc_ctx->frame);

	mpp_frame_set_width(rkenc_ctx->frame, avctx->width);
	mpp_frame_set_height(rkenc_ctx->frame, avctx->height);
	mpp_frame_set_hor_stride(rkenc_ctx->frame, (avctx->width + 15) & (~15));
	mpp_frame_set_ver_stride(rkenc_ctx->frame, (avctx->height + 15) & (~15));

	/* import frame_data */
	memset(&info, 0, sizeof(info));
	info.type = MPP_BUFFER_TYPE_ION;
	rkenc_ffmpeg2vpu_get_buf_size(avctx, frame, &info.size);
	info.fd = frame_data->phy_fd;
	info.ptr = frame_data->vir_addr;

	if (!info.size || info.size > frame_data->buf_size) {
		av_log(avctx, AV_LOG_ERROR, "input frame data size wrong\n");
		ret = -1;

		goto ENCODE_FREE_FRAME;
	}

	ret = mpp_buffer_import(&pic_buf, &info);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "pic_buf import buffer failed\n");
		goto ENCODE_FREE_FRAME;
	}

	mpp_frame_set_buffer(rkenc_ctx->frame, pic_buf);

	/* import pkt_data */
	memset(&info, 0, sizeof(info));
	info.type = MPP_BUFFER_TYPE_ION;
	info.size = pkt_data->buf_size;
	info.fd = pkt_data->phy_fd;
	info.ptr = pkt_data->vir_addr;

	ret = mpp_buffer_import(&str_buf, &info);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "str_buf import buffer failed\n");
		goto ENCODE_FREE_PACKET;
	}
	mpp_packet_init_with_buffer(&rkenc_ctx->packet, str_buf);

	/* import mv_data */
	memset(&info, 0, sizeof(info));
	info.type = MPP_BUFFER_TYPE_ION;
	info.size = mv_data->buf_size;
	info.fd = mv_data->phy_fd;
	info.ptr = mv_data->vir_addr;

	if (info.size > 0) {
		ret = mpp_buffer_import(&mv_buf, &info);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR, "mv_buf import buffer failed\n");
			goto ENCODE_FREE_MV;
		}
	}

	/* encode process */
	ret = rkenc_ctx->mpi->poll(rkenc_ctx->ctx, MPP_PORT_INPUT, MPP_POLL_BLOCK);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpp input poll failed\n");
		goto ENCODE_FREE_PACKET;
	}
	ret = rkenc_ctx->mpi->dequeue(rkenc_ctx->ctx, MPP_PORT_INPUT, &task);
	if (ret || NULL == task) {
		av_log(avctx, AV_LOG_ERROR, "mpp task input dequeue failed\n");
		goto ENCODE_FREE_PACKET;
	}

	mpp_task_meta_set_frame(task, KEY_INPUT_FRAME,  rkenc_ctx->frame);
	mpp_task_meta_set_packet(task, KEY_OUTPUT_PACKET, rkenc_ctx->packet);
	mpp_task_meta_set_buffer(task, KEY_MOTION_INFO, mv_buf);

	ret = rkenc_ctx->mpi->enqueue(rkenc_ctx->ctx, MPP_PORT_INPUT, task);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "mpp task input enqueue failed\n");
		goto ENCODE_FREE_PACKET;
	}
	task = NULL;

	ret = rkenc_ctx->mpi->poll(rkenc_ctx->ctx, MPP_PORT_OUTPUT, MPP_POLL_BLOCK);
	if (ret) {
		av_log(avctx, AV_LOG_ERROR, "ret %d mpp output poll failed\n", ret);
		goto ENCODE_FREE_PACKET;
	}

	ret = rkenc_ctx->mpi->dequeue(rkenc_ctx->ctx, MPP_PORT_OUTPUT, &task);
	if (ret || NULL == task) {
		av_log(avctx, AV_LOG_ERROR, "ret %d mpp task output dequeue failed\n", ret);
		goto ENCODE_FREE_PACKET;
	}

	if (task) {
		MppFrame frame_out = NULL;
		MppPacket packet_out = NULL;

		mpp_task_meta_get_packet(task, KEY_OUTPUT_PACKET, &packet_out);

		ret = rkenc_ctx->mpi->enqueue(rkenc_ctx->ctx, MPP_PORT_OUTPUT, task);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR, "mpp task output enqueue failed\n");
			goto ENCODE_FREE_PACKET;
		}

		// dequeue task from MPP_PORT_INPUT to release frame
		ret = rkenc_ctx->mpi->poll(rkenc_ctx->ctx, MPP_PORT_INPUT, MPP_POLL_BLOCK);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR, "ret %d mpp input poll failed\n", ret);
			goto ENCODE_FREE_PACKET;
		}

		ret = rkenc_ctx->mpi->dequeue(rkenc_ctx->ctx, MPP_PORT_INPUT, &task);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR, "failed to dequeue from input port ret %d\n", ret);
			goto ENCODE_FREE_PACKET;
		}

		mpp_task_meta_get_frame(task, KEY_INPUT_FRAME, &frame_out);
		ret = rkenc_ctx->mpi->enqueue(rkenc_ctx->ctx, MPP_PORT_INPUT, task);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR, "mpp task output enqueue failed\n");
			goto ENCODE_FREE_PACKET;
		}
		task = NULL;
	}

	if (rkenc_ctx->packet != NULL) {
		if ((ret = ff_alloc_packet2(avctx, packet,
					    mpp_packet_get_length(rkenc_ctx->packet), 0)) < 0) {
			av_log(avctx, AV_LOG_ERROR, "failed to alloc for packet\n");
			*got_packet = 0;
			goto ENCODE_FREE_PACKET;
		}
		//printf("got packet length: %d kB\n", packet->size / 1024);
		packet->pts = frame->pts;
		packet->dts = frame->pts;

#ifndef MPP_PACKET_FLAG_INTRA
#define MPP_PACKET_FLAG_INTRA           (0x00000008)
#endif
		if ((mpp_packet_get_flag(rkenc_ctx->packet) & MPP_PACKET_FLAG_INTRA) ? (1) :
		    (0))
			packet->flags |= AV_PKT_FLAG_KEY;
		packet->flags |=
			AV_PKT_FLAG_ONE_NAL; // rk guarantee returning only one slice, and start with 00 00 01
		*got_packet = 1;
	} else {
		*got_packet = 0;
	}

ENCODE_FREE_PACKET:
	if (pic_buf) {
		mpp_buffer_put(pic_buf);
		pic_buf = NULL;
	}
	if (str_buf) {
		mpp_buffer_put(str_buf);
		str_buf = NULL;
	}

	mpp_packet_deinit(&rkenc_ctx->packet);
	rkenc_ctx->packet = NULL;

ENCODE_FREE_FRAME:
	mpp_frame_deinit(&rkenc_ctx->frame);
	rkenc_ctx->frame = NULL;

ENCODE_FREE_MV:
	if (mv_buf) {
		mpp_buffer_put(mv_buf);
		mv_buf = NULL;
	}

	if (rkenc_ctx->osd_data) {
		mpp_buffer_put(rkenc_ctx->osd_data);
		rkenc_ctx->osd_data = NULL;
	}

	return ret;
}

static int rkenc_control(AVCodecContext *avctx, int cmd, void *param)
{
	int ret = 0;
	RkMppEncContext *rkenc_ctx = (RkMppEncContext *)avctx->priv_data;
	MppApi *mpi = rkenc_ctx->mpi;
	MpiCmd mpi_cmd = (MpiCmd)cmd;

	if (mpi_cmd == MPP_ENC_SET_OSD_DATA_CFG) {
		MppEncOSDData *src = (MppEncOSDData *)param;
		MppEncOSDData dst;
		MppBufferInfo info;
		DataBuffer_t *src_buf = (DataBuffer_t *)src->buf;
		MppBuffer *dst_buf = &rkenc_ctx->osd_data;

		memset(&info, 0, sizeof(info));
		info.type = MPP_BUFFER_TYPE_ION;
		info.size = src_buf->buf_size;
		info.fd = src_buf->phy_fd;
		info.ptr = src_buf->vir_addr;

		if (info.size == 0) {
			av_log(avctx, AV_LOG_ERROR, "rkenc_control osd data buf size is zero\n");
			return 1;
		}
		if (*dst_buf) {
			av_log(avctx, AV_LOG_ERROR,
			       "rkenc_control osd data buf should be free after last frame encoding\n");
			return 1;
		}

		ret = mpp_buffer_import(dst_buf, &info);
		if (ret) {
			av_log(avctx, AV_LOG_ERROR,
			       "rkenc_control osd_data buf import buffer failed\n");
			if (*dst_buf) {
				mpp_buffer_put(*dst_buf);
				*dst_buf = NULL;
			}
			return ret;
		}

		memcpy(&dst, src, sizeof(dst));
		dst.buf = *dst_buf;

		ret = mpi->control(rkenc_ctx->ctx, mpi_cmd, (MppParam)(&dst));
	} else {
		ret = mpi->control(rkenc_ctx->ctx, mpi_cmd, (MppParam)param);
	}

	if (ret != 0) {
		av_log(avctx, AV_LOG_ERROR, "mpp control ctx %p cmd 0x%08x param %p failed\n",
		       rkenc_ctx->ctx, cmd, param);
		return ret;
	}

	return 0;
}
#define DECLARE_RK_MPP_ENC_VIDEO_ENCODER(TYPE, CODEC_ID)                     \
    AVCodec ff_##TYPE##_encoder = {                                         \
        .name           = #TYPE,                                            \
        .long_name      = NULL_IF_CONFIG_SMALL(#TYPE " hw encoder"),        \
        .type           = AVMEDIA_TYPE_VIDEO,                               \
        .id             = CODEC_ID,                                         \
        .priv_data_size = sizeof(RkMppEncContext),                          \
        .init           = rkenc_init,                                       \
        .close          = rkenc_deinit,                                     \
        .encode2        = rkenc_encode_frame,                               \
        .capabilities   = CODEC_CAP_DELAY,                                  \
        .control        = rkenc_control,                                    \
        .pix_fmts       = (const enum AVPixelFormat[]) {                    \
            AV_PIX_FMT_YUV420P,						\
            AV_PIX_FMT_NV12,						\
            AV_PIX_FMT_NV21,						\
            AV_PIX_FMT_YUV422P,						\
            AV_PIX_FMT_NV16,						\
            AV_PIX_FMT_NV61,						\
            AV_PIX_FMT_YVYU422,						\
            AV_PIX_FMT_UYVY422,						\
            AV_PIX_FMT_RGB565LE,					\
            AV_PIX_FMT_BGR565LE,					\
            AV_PIX_FMT_RGB24,						\
            AV_PIX_FMT_BGR24,						\
            AV_PIX_FMT_RGB32,						\
            AV_PIX_FMT_BGR32,						\
            AV_PIX_FMT_NONE						\
        },								\
    };

DECLARE_RK_MPP_ENC_VIDEO_ENCODER(h264_mpp, AV_CODEC_ID_H264)
