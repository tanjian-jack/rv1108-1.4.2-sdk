#! /bin/sh
#--disable-network
#-funwind-tables -g -rdynamic -O0
#-DDEBUG
PATH=$(pwd)/../../out/system/bin:$PATH \
./configure \
--pkg-config=pkg-config \
--pkgconfigdir=$(pwd)/../../out/system/lib/pkgconfig \
--prefix=$(pwd)/../../out/system \
--extra-cflags="-I$(pwd)/../../out/system/include -DFS_CACHE=1 -fexceptions -finstrument-functions -funwind-tables -g -rdynamic -O0 -mfpu=neon -Wno-multichar" \
--extra-ldflags="-L$(pwd)/../../out/system/lib " \
--enable-gpl \
--enable-nonfree \
--enable-cross-compile \
--disable-programs \
--disable-doc \
--disable-avdevice \
--disable-postproc \
--disable-swscale \
--disable-avfilter \
--disable-w32threads --disable-os2threads \
--disable-pixelutils \
--disable-everything \
--disable-hwaccels \
--enable-encoder=h264_mpp \
--enable-decoder=h264_mpp \
--enable-encoder=libfdk_aac \
--enable-decoder=libfdk_aac \
--enable-decoder=pcm_s16le \
--enable-muxer=mp4 \
--enable-muxer=mpegts \
--enable-muxer=rtsp \
--enable-muxer=rtp_mpegts \
--enable-demuxer=mov \
--enable-demuxer=wav \
--enable-protocol=file \
--enable-protocol=udp \
--disable-audiotoolbox \
--disable-iconv \
--disable-libxcb \
--disable-libxcb-shm \
--disable-libxcb-xfixes \
--disable-libxcb-shape \
--disable-schannel \
--disable-sdl \
--disable-securetransport \
--disable-videotoolbox \
--disable-xlib \
--disable-asm \
--enable-vfp \
--enable-neon \
--enable-libfdk-aac \
--disable-zlib \
--cross-prefix="../../prebuilts/toolschain/usr/bin/arm-linux-" \
--target-os=linux \
--arch=armv7-a \
--enable-static --disable-shared --disable-runtime-cpudetect \
--disable-optimizations --disable-stripping
#--disable-debug \
#--enable-small
