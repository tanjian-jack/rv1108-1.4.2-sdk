#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../..
source config/envsetup.sh
echo $bash_path
cd $bash_path

ROOT_PATH=$(pwd)
SBIN_PATH=$ROOT_PATH/src
LIBS_PATH=$ROOT_PATH/src/.libs
CONFIGS_PATH=$ROOT_PATH/doc/config

PCRE_PATH=$ROOT_PATH/pcre
PCRE_BUILD=$ROOT_PATH/pcre/build
PCRE_LIBS=$PCRE_BUILD/lib

RELEASE_PATH=$(pwd)/../../out/system

release_lib="mod_indexfile.so mod_dirlisting.so mod_staticfile.so mod_access.so mod_cgi.so"
release_bin="lighttpd"
release_etc=
pcre_release="libpcre.so libpcre.so.0 libpcre.so.0.0.1"

[ ! -d $RELEASE_PATH/sbin ] && mkdir $RELEASE_PATH/sbin
[ ! -d $RELEASE_PATH/lib ] && mkdir $RELEASE_PATH/lib
[ ! -d $RELEASE_PATH/etc ] && mkdir $RELEASE_PATH/etc

cd $SBIN_PATH && cp -r $release_bin $RELEASE_PATH/sbin/
cd $LIBS_PATH && cp -r $release_lib $RELEASE_PATH/lib/
cd $PCRE_LIBS && cp -r $pcre_release $RELEASE_PATH/lib/
cd $ROOT_PATH && cp -r lighttpd.conf $RELEASE_PATH/etc/

cd $current_path
