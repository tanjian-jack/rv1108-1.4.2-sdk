/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Graphics and Display Platform Center.
 *
 *  Created by felix.zeng@rock-chips.com
 *
 *  Object Detecting & Tracking
 */

#ifndef __ODT_WRAPPER_H_
#define __ODT_WRAPPER_H_

#define ODT_VERSION "V2.6-20170425"

#define ODT_MAX_OBJECT 8

#ifdef __cplusplus
extern "C" {
#endif

/*

Usage:

    // initialize ODT context
    int mode = ODT_MODE_DAY;
    int width = OdtWrapper_getRequestImageWidth(mode);
    int height = OdtWrapper_getRequestImageHeight(mode);
    OdtWrapper_initizlize(width, height, mode);
    OdtWrapper_setCalibrateLine(height / 2.0, 0.95f * height);
    OdtWrapper_setDetectorMode(ODT_MODE_DAY);

    // alloc ION buffer
    int fcwDataSize = OdtWrapper_getAllocDataSize();
    u8 *buffer = ionAlloc(fcwDataSize);
    OdtWrapper_setAllocData(buffer);

    // run
    OdtWrapper_setDetectorStatus(ODT_STATUS_ON);

    ...

    // do every frame as possible
    {
        // prepare DSP processing thread
        OdtWrapper_taskPrepare(buffer, 4);

        // get result when DSP processing finish
        OdtWrapper_updateResult(&OdtObjectInfos);

        // draw rectangles and distances to fb
        notifyUiToDraw(&OdtObjectInfos);
    }

    ...

    // release ION buffer
    ionFree(buffer);

    // release ODT context
    OdtWrapper_release();
*/

typedef struct OdtFrame
{

    void *image;

    int width;
    int height;

    int format;

    void *priv;

} OdtFrame;

typedef struct OdtPrepareData
{

    void *data;
    int size;

    void *buffer;
    void *priv;

} OdtPrepareData;

/* Detector Status */
enum
{
    ODT_STATUS_ON = 0,
    ODT_STATUS_OFF
};

/* Detector Mode */
enum
{
    ODT_MODE_FCW_DAY = 0, // request size 640x360
    ODT_MODE_FCW_NIGHT,   // request size 640x360
    ODT_MODE_FACE,        // request size 320x180
    ODT_MODE_PEDESTRIAN,  // request size 240x135
    ODT_MODE_GESTURE,     // request size 240x135
    ODT_MODE_MAX
};

/* Object Information, rectangle and distance */
typedef struct OdtObjectInfo
{
    int x;
    int y;
    int width;
    int height;
    float distance;
} OdtObjectInfo;

/* Object Information Array */
typedef struct OdtObjectInfos
{
    int valid;
    int objectNum;
    OdtObjectInfo objects[ODT_MAX_OBJECT];
} OdtObjectInfos;

typedef struct OdtMotionBlockTable
{
    int width;  // block width, not real image width
    int height; // block height, not real image height
    int changeBlocks;
    char *data; // total size = width * height
                // when data[i] == 0, it means that the block i is not changed.
                // when data[i] == 1, it means that the block i is changed.
                // current table (blockX, blockY) = (i/width, i/height)
                // you may need to map (blockX, blockY) to (ImageX, ImageY)
} OdtMotionBlockTable;

int OdtWrapper_getRequestImageWidth(int mode);
int OdtWrapper_getRequestImageHeight(int mode);

int OdtWrapper_getCurrentImageWidth();
int OdtWrapper_getCurrentImageHeight();

void OdtWrapper_setReleaseBufCb(void (*release_buf_cb)(OdtFrame *));
void OdtWrapper_setRequestDspCb(void (*request_dsp_cb)(OdtPrepareData *));

/*
    Here generate the static context, and initialize about odt everything.
    Suggest using default value: width x height = OdtWrapper_getDefaultImageWidth() x OdtWrapper_getDefaultImageHeight()
        width:
            Specifies the width of the image
        height:
            Specifies the height of the image
 */
void OdtWrapper_initizlize(int width, int height, int mode);

/*
    Free everything.
 */
void OdtWrapper_release();

/*
    Get license to authorize or not.
        return:
            true can be authorized, Otherwise, false is not
 */
int OdtWrapper_isAuthorize();

/*
    Get current detector mode, day or night.
        return:
            detector mode, ODT_MODE_FCW_DAY or ODT_MODE_FCW_NIGHT
 */
int OdtWrapper_getDetectorMode();

/*
    Set detector mode, day or night.
        mode:
            detector mode, ODT_MODE_FCW_DAY or ODT_MODE_FCW_NIGHT
 */
void OdtWrapper_setDetectorMode(int mode);

int OdtWrapper_getDetectorLevel();
void OdtWrapper_setDetectorLevel(int level);

/*
    Set running status, on or off.
    Must be invoke ODT_STATUS_ON when ready to run
        status:
            running status, ODT_STATUS_ON or ODT_STATUS_OFF
 */
int OdtWrapper_getDetectorStatus();
void OdtWrapper_setDetectorStatus(int status);

/*
    Set odt calibrate line, contains horizon line and vehicle head line.
        horizonLine:
            horizon line height on image
        headLine:
            vehicle head line height on image
 */
void OdtWrapper_setCalibrateLine(int horizonLine, int headLine);

/*
    Get fcw need ION buffer size to alloc.
    Must be invoked after initializing.
        return:
            need alloc size
 */
int OdtWrapper_getAllocDataSize();

/*
    Set fcw start address that get from ION buffer.
        data:
            ion start address
 */
void OdtWrapper_setAllocData(OdtPrepareData *prepareData);

/*
    Prepare the yuv or gray image buffer, and set max task number on one frame processing.
        imageGray:
            yuv or gray image buffer
        maxTaskNum:
            max task number
        return:
            real task number

 */
int OdtWrapper_taskPrepare(OdtFrame *frame, int maxTaskNum);

/*
    Get fcw result when dsp process finish on one frame.
        objectInfos:
            object information, contains rectangle and distance
 */
void OdtWrapper_updateResult(OdtObjectInfos *objectInfos);

void (*OdtWrapper_getRecEventCb())(int cmd, void *msg0, void *msg1);

// just for debugging
void OdtWrapper_getDebugObjects(OdtObjectInfos *objectInfos);
void *OdtWrapper_getContext();

#ifdef __cplusplus
}
#endif

#endif // end of __ODT_WRAPPER_H_
