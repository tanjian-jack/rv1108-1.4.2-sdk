/*
 * Copyright (c) 2016-2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu zhichao.yu@rock-chips.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include <iostream>

#include <sys/mman.h>
#include <ion/ion.h>
#include "dsp_ioctl.h"

#define DMA_1D_TRANSFER  0
#define DMA_2D_TRANSFER  1
#define TEST_MODE        DMA_2D_TRANSFER

struct test_params_arm {
  long unsigned int src_phy;
  long unsigned int dst_phy;

  int src_fd;
  int dst_fd;

  uint8_t* src;
  uint8_t* dst;

  ion_user_handle_t src_hdl;
  ion_user_handle_t dst_hdl;

  uint32_t size;
};

struct test_params_dsp {
  uint32_t src_phy;
  uint32_t dst_phy;

  uint32_t width;
  uint32_t height;

  uint32_t block_width;
  uint32_t block_height;

  uint32_t mode;
  uint32_t cost_cycles; // Return by DSP.
};

static int g_ion_client = -1;

static int prepare_test_params(struct test_params_arm *params, uint32_t size) {
  int ret = -1;

  memset(params, 0, sizeof(*params));
  params->src_hdl = -1;
  params->dst_hdl = -1;
  params->src_fd = -1;
  params->dst_fd = -1;

  ret = ion_alloc(g_ion_client, size, 0,
                  ION_HEAP_TYPE_DMA_MASK, 0, &params->src_hdl);
  if (ret) {
    printf("Cannot alloc test src buffer.\n");
    return ret;
  }

  ret = ion_alloc(g_ion_client, size, 0,
                  ION_HEAP_TYPE_DMA_MASK, 0, &params->dst_hdl);
  if (ret) {
    printf("Cannot alloc test dst buffer.\n");
    return ret;
  }

  ion_get_phys(g_ion_client, params->src_hdl, &params->src_phy);
  ion_get_phys(g_ion_client, params->dst_hdl, &params->dst_phy);

  ret = ion_share(g_ion_client, params->src_hdl, &params->src_fd);
  if (ret) {
    printf("Share ion failed.\n");
    return -1;
  }

  ret = ion_share(g_ion_client, params->dst_hdl, &params->dst_fd);
  if (ret) {
    printf("Share ion failed.\n");
    return -1;
  }

  params->src = (uint8_t*)mmap(NULL, size, PROT_READ | PROT_WRITE,
                               MAP_SHARED | MAP_LOCKED, params->src_fd, 0);
  params->dst = (uint8_t*)mmap(NULL, size, PROT_READ | PROT_WRITE,
                               MAP_SHARED | MAP_LOCKED, params->dst_fd, 0);

  memset(params->src, 0x80, size);
  memset(params->dst, 0, size);

  params->size = size;

  return 0;
}

static int unprepare_test_params(struct test_params_arm *params) {
  if (params->src)
    munmap(params->src, params->size);
  if (params->dst)
    munmap(params->dst, params->size);

  if (params->src_fd >= 0)
    close(params->src_fd);
  if (params->dst_fd >= 0)
    close(params->src_fd);

  if (params->src_hdl >= 0)
    ion_free(g_ion_client, params->src_hdl);
  if (params->dst_hdl >= 0)
    ion_free(g_ion_client, params->dst_hdl);
  
  return 0;
}

int main(int argc, char **argv) {
  int dsp_fd = -1;
  int width = 1280;
  int height = 1280;
  int size = width * height;
  struct dsp_user_work test_work;
  struct test_params_arm params_arm;
  struct test_params_dsp params_dsp;

  memset(&test_work, 0, sizeof(test_work));
  memset(&params_dsp, 0, sizeof(params_dsp));

  g_ion_client = ion_open();
  if (g_ion_client < 0) {
    printf("Cannot open ion device.\n");
    return -1;
  }

  // Open DSP device.
  dsp_fd = open("/dev/dsp", O_RDWR);
  if (dsp_fd < 0) {
    printf("Cannot open dsp device.\n");
    return -1;
  }

  // Initialize work struct.
  if (prepare_test_params(&params_arm, size)) {
    unprepare_test_params(&params_arm);
    printf("Prepare test parameter failed.\n");
    return -1;
  }
  params_dsp.src_phy = params_arm.src_phy;
  params_dsp.dst_phy = params_arm.dst_phy;
  params_dsp.mode = TEST_MODE;
  params_dsp.width = width;
  params_dsp.height = height;
  params_dsp.block_width = 128;
  params_dsp.block_height = 128;

  test_work.magic = DSP_ALGORITHM_WORK_MAGIC;
  test_work.id = 0x78787878;
  test_work.algorithm.type = DSP_ALGORITHM_COPY;
  test_work.algorithm.packet_virt = (u32)&params_dsp;
  test_work.algorithm.size = sizeof(params_dsp);

  // Request DSP work. The test algorithm just copy src buffer to dst buffer
  // using DDMA interface.
  printf("Test work start, mode=%d, src=0x%08x, dst=0x%08x.\n",
         params_dsp.mode, params_dsp.src_phy, params_dsp.dst_phy);
  ioctl(dsp_fd, DSP_IOC_QUEUE_WORK, &test_work);

  // Process result, check buffer copy operation is success or not.
  ioctl(dsp_fd, DSP_IOC_DEQUEUE_WORK, &test_work);
  printf("Test work done, cost cycles=%d.\n", params_dsp.cost_cycles);
  if (memcmp(params_arm.src, params_arm.dst, size))
    printf("Test failed, src buffer is not equal to dst buffer.\n");
  else
    printf("Test success.\n");

  unprepare_test_params(&params_arm);
  close(dsp_fd);
  close(g_ion_client);
  return 0;
}
