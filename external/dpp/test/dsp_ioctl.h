/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  author: ZhiChao Yu zhichao.yu@rock-chips.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _ARCH_ARM_MACH_RK_DSP_H_
#define _ARCH_ARM_MACH_RK_DSP_H_
#include <stdint.h>
#include <sys/ioctl.h>

#define u32 unsigned int
#define s32 signed int
#define u16 unsigned short
#define s16 signed short
#define s8  unsigned char
#define u8  signed char

#define DSP_MAX_IMAGE 16

/*
 * DSP driver ioctl definition
 */
#define DSP_IOC_MAGIC             'x'
#define DSP_IOC_QUEUE_WORK         _IOW(DSP_IOC_MAGIC, 1, unsigned long)
#define DSP_IOC_DEQUEUE_WORK       _IOR(DSP_IOC_MAGIC, 2, unsigned long)

/*
 * DSP error codes which will be returned in result
 */
#define DSP_WORK_SUCCESS   0x00000000
#define DSP_WORK_EABANDON  0x80000001
#define DSP_WORK_ECOPY     0x80000002
#define DSP_WORK_ETIMEOUT  0x80000003

/*
 * User work magic, used by dsp kernel driver to check
 * the work parameter is valid or not
 */
#define DSP_ALGORITHM_WORK_MAGIC  0x20462046
#define DSP_CONFIG_WORK_MAGIC     0x95279527

/*
 * DSP algorithm types. User application should set type
 * in the type member of struct dsp_algorithm_work
 */
#define DSP_ALGORITHM_COPY    0x00000001
#define DSP_ALGORITHM_ADAS    0x00000002
#define DSP_ALGORITHM_3DNR    0x00000008
#define DSP_ALGORITHM_IDC     0x00000080
#define DSP_ALGORITHM_DVS     0x00000100
#define DSP_ALGORITHM_CV      0x40000001
#define DSP_ALGORITHM_TEST    0x80000000

/*
 * DSP config types
 */
#define DSP_CONFIG_INIT   0x80000001

enum dsp_work_type {
	DSP_ALGORITHM_WORK = 1,
	DSP_CONFIG_WORK    = 2,
};

/*
 * dsp_algorithm_params - parameters used by DSP core to process
 * an algorithm request
 *
 * @type: algorithm type, user specific, known by user application and DSP
 * @reserved: reserved for kernel driver use
 * @packet_virt: algorithm parameter packet virtual address
 * @size: size of algorithm parameter packet
 */
struct dsp_algorithm_params {
	u32 type;
	u32 reserved;
	u32 packet_virt;
	u32 size;
};

/*
 * dsp_user_work - This struct is used by user ioctl
 *
 * @magic: work magic should be DSP_ALGORITHM_WORK_MAGIC
 * @id: work id
 * @result: work result, if success result is 0
 * @algorithm: algorithm parameters
 */
struct dsp_user_work {
	u32 magic;
	u32 id;
	u32 result;

	struct dsp_algorithm_params algorithm;
};

#endif
