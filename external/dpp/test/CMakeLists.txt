project(DPP_TEST)
cmake_minimum_required(VERSION 2.8.12)

add_subdirectory(cv)

set(DSP_TEST_SRC_FILES
  dsp_test.cc
)

# dsp_test
add_executable(dsp_test ${DSP_TEST_SRC_FILES})
target_include_directories(dsp_test
  PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
)
target_link_libraries(dsp_test ion)
install(TARGETS dsp_test DESTINATION bin)
