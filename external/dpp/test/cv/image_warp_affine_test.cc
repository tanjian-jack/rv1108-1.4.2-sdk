/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: ZhiChao Yu  zhichao.yu@rock-chips.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <iostream>
#include <sys/mman.h>
#include <ion/ion.h>
#include "dsp_ioctl.h"

#define INPUT_WIDTH  640
#define INPUT_HEIGHT 480
#define INPUT_SIZE   (INPUT_WIDTH * INPUT_HEIGHT * 3)

#define OUTPUT_WIDTH  640
#define OUTPUT_HEIGHT 480
#define OUTPUT_SIZE   (INPUT_WIDTH * OUTPUT_HEIGHT * 3)

struct image_warp_affine_params {
  unsigned long input;
  uint32_t input_width;
  uint32_t input_height;

  unsigned long output;
  uint32_t output_width;
  uint32_t output_height;

  void* input_virt;
  void* output_virt;
};

static int prepare_test_params(struct image_warp_affine_params *params) {
  int ret = -1;
  int client = -1;
  ion_user_handle_t input_hdl, output_hdl;

  int share_fd_input, share_fd_output;

  void* buffer_input, *buffer_output;

  client = ion_open();
  if (client < 0) {
    printf("Cannot open ion device.\n");
    return -1;
  }

  ret = ion_alloc(client, INPUT_SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &input_hdl);
  if (ret) {
    printf("Cannot alloc test input buffer.\n");
    return ret;
  }

  ret = ion_alloc(client, OUTPUT_SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &output_hdl);
  if (ret) {
    printf("Cannot alloc test output buffer.\n");
    return ret;
  }

  ret = ion_share(client, input_hdl, &share_fd_input);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, input_hdl);
    return -1;
  }

  ret = ion_share(client, output_hdl, &share_fd_output);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, output_hdl);
    return -1;
  }

  buffer_input = mmap(NULL, INPUT_SIZE, PROT_READ | PROT_WRITE,
                      MAP_SHARED | MAP_LOCKED, share_fd_input, 0);

  buffer_output = mmap(NULL, OUTPUT_SIZE, PROT_READ | PROT_WRITE,
                       MAP_SHARED | MAP_LOCKED, share_fd_output, 0);

  ion_get_phys(client, input_hdl, &params->input);
  ion_get_phys(client, output_hdl, &params->output);

  params->input_virt = buffer_input;
  params->output_virt = buffer_output;

  params->input_width = INPUT_WIDTH;
  params->input_height = INPUT_HEIGHT;
  params->output_width = OUTPUT_WIDTH;
  params->output_height = OUTPUT_HEIGHT;

  ion_close(client);

  // Read input image
  std::ifstream istream;
  istream.open("/mnt/sdcard/input.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input file.\n");
    return false;
  }
  istream.seekg(0, istream.end);
  size_t length = istream.tellg();
  istream.seekg(0, istream.beg);
  istream.read((char*)params->input_virt, length);
  istream.close();

  return 0;
}

int main(int argc, char **argv) {
  int dsp_fd = -1;
  struct dsp_user_work test_work;
  struct image_warp_affine_params params;

  memset(&test_work, 0, sizeof(test_work));
  memset(&params, 0, sizeof(params));

  // Open DSP device
  dsp_fd = open("/dev/dsp", O_RDWR);
  if (dsp_fd < 0) {
    std::cout << "Cannot open dsp device" << std::endl;
    return -1;
  }

  // Initialize work struct
  if (prepare_test_params(&params)) {
    std::cout << "Prepare test parameter failed" << std::endl;
    return -1;
  }
  test_work.magic = DSP_ALGORITHM_WORK_MAGIC;
  test_work.id = 0x78787878;
  test_work.algorithm.type = 0x40000006;
  test_work.algorithm.packet_virt = (u32)&params;
  test_work.algorithm.size = sizeof(params);

  // Request DSP work
  std::cout << "Test work start" << std::endl;
  ioctl(dsp_fd, DSP_IOC_QUEUE_WORK, &test_work);
  ioctl(dsp_fd, DSP_IOC_DEQUEUE_WORK, &test_work);
  std::cout << "Test work done" << std::endl;

  // Write output image
  std::ofstream fstream;
  fstream.open("/mnt/sdcard/output.yuv", std::ios_base::binary);
  fstream.write((char*)params.output_virt, OUTPUT_WIDTH * OUTPUT_HEIGHT);
  fstream.close();

  close(dsp_fd);
  return 0;
}
