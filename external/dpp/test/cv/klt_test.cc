/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: ZhiQiang Zhuo <zzq@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <iostream>
#include <sys/mman.h>
#include <ion/ion.h>
#include "dsp_ioctl.h"

#define WIDTH  640
#define HEIGHT 480
#define SIZE   (WIDTH * HEIGHT * 30)

typedef short feature_pos_t;

typedef struct {
feature_pos_t* p_x;
feature_pos_t* p_y;
uint* p_id;
uint count;
uint capacity;
} feature_list_t;

struct klt_params {
	unsigned long src0_phy;    // 640*480
	unsigned long src1_phy;    // 320*240
	unsigned long src2_phy;    // 160*120
	unsigned long src3_phy;    // detection_log
  unsigned long dst_phy;

	uint width;
	uint height;
	int num_frames;
	int num_levels;
	uint detector_max_features;

	uint klt_max_features;
	int num_iterations;
	uint max_residue;
	uint track_width;
	uint track_height;
	uint min_determinant;
	feature_list_t* p_features_prev;
	feature_list_t* p_features_curr;

	void* src_virt0;
	void* src_virt1;
	void* src_virt2;
	void* src_virt3;
  void* dst_virt;
};

static int prepare_test_params(struct klt_params *params) {
  int ret = -1;
  int client = -1;
  ion_user_handle_t src_hdl0, src_hdl1,src_hdl2,src_hdl3,dst_hdl;

  int share_fd0_src,share_fd1_src,share_fd2_src,share_fd3_src;
  int share_fd_dst;

  void* buffer0_src;
  void* buffer1_src;
  void* buffer2_src;
  void* buffer3_src;
  void* buffer_dst;


  client = ion_open();
  if (client < 0) {
    printf("Cannot open ion device.\n");
    return -1;
  }

  ret = ion_alloc(client, SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl0);
  if (ret) {
    printf("Cannot alloc test src0 buffer.\n");
    return ret;
  }

  int size1 = 320*240*30;
  ret = ion_alloc(client, size1, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl1);
  if (ret) {
    printf("Cannot alloc test src1 buffer.\n");
    return ret;
  }

  int size2 = 160*120*30;
  ret = ion_alloc(client, size2, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl2);
  if (ret) {
    printf("Cannot alloc test src2 buffer.\n");
    return ret;
  }

  int size3 = 215840;
  ret = ion_alloc(client, size3, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl3);
  if (ret) {
    printf("Cannot alloc test src3 buffer.\n");
    return ret;
  }

  ret = ion_alloc(client, SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &dst_hdl);
  if (ret) {
    printf("Cannot alloc test dst buffer.\n");
    return ret;
  }

  ret = ion_share(client, src_hdl0, &share_fd0_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl0);
    return -1;
  }

  ret = ion_share(client, src_hdl1, &share_fd1_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl1);
    return -1;
  }

  ret = ion_share(client, src_hdl2, &share_fd2_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl2);
    return -1;
  }

  ret = ion_share(client, src_hdl3, &share_fd3_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl3);
    return -1;
  }

  ret = ion_share(client, dst_hdl, &share_fd_dst);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, dst_hdl);
    return -1;
  }

  buffer0_src = mmap(NULL, SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd0_src, 0);
  buffer1_src = mmap(NULL, size1, PROT_READ | PROT_WRITE,
	                MAP_SHARED | MAP_LOCKED, share_fd1_src, 0);
  buffer2_src = mmap(NULL, size2, PROT_READ | PROT_WRITE,
	                MAP_SHARED | MAP_LOCKED, share_fd2_src, 0);
  buffer3_src = mmap(NULL, size3, PROT_READ | PROT_WRITE,
	                MAP_SHARED | MAP_LOCKED, share_fd3_src, 0);
  buffer_dst = mmap(NULL, SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd_dst, 0);

  ion_get_phys(client, src_hdl0, &params->src0_phy);
  ion_get_phys(client, src_hdl1, &params->src1_phy);
  ion_get_phys(client, src_hdl2, &params->src2_phy);
  ion_get_phys(client, src_hdl3, &params->src3_phy);
  ion_get_phys(client, dst_hdl, &params->dst_phy);

  params->src_virt0 = buffer0_src;
  params->src_virt1 = buffer1_src;
  params->src_virt2 = buffer2_src;
  params->src_virt3 = buffer3_src;
  params->dst_virt = buffer_dst;

  params->width = WIDTH;
  params->height = HEIGHT;
  params->num_frames = 10;
  params->num_levels = 3;
  params->detector_max_features = 256;
  params->klt_max_features = 256;
  params->num_iterations = 10;
  params->max_residue = 2000;
  params->track_width = 16;
  params->track_height = 16;
  params->min_determinant = 100;

  ion_close(client);

  std::ifstream istream;
  istream.open("/mnt/sdcard/pyr_l0_640x480_P400.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input0 file.\n");
    return false;
  }

  istream.seekg(0, istream.end);
  size_t length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input0 data length=%d\n", length);
  istream.read((char*)params->src_virt0, length);

  istream.close();
  istream.open("/mnt/sdcard/pyr_l1_320x240_P400.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input1 file.\n");
    return false;
  }

  istream.seekg(0, istream.end);
  length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input1 data length=%d\n", length);
  istream.read((char*)params->src_virt1, length);

  istream.close();
  istream.open("/mnt/sdcard/pyr_l2_160x120_P400.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input2 file.\n");
    return false;
  }
  istream.seekg(0, istream.end);
  length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input2 data length=%d\n", length);
  istream.read((char*)params->src_virt2, length);
  istream.close();

  istream.open("/mnt/sdcard/detection_log.txt",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input3 file.\n");
    return false;
  }
  istream.seekg(0, istream.end);
  length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input3 data length=%d\n", length);
  istream.read((char*)params->src_virt3, length);
  istream.close();

  return 0;
}

int main(int argc, char **argv) {
  int dsp_fd = -1;
  struct dsp_user_work test_work;
  struct klt_params params;

  memset(&test_work, 0, sizeof(test_work));
  memset(&params, 0, sizeof(params));

  // Open DSP device
  dsp_fd = open("/dev/dsp", O_RDWR);
  if (dsp_fd < 0) {
    std::cout << "Cannot open dsp device" << std::endl;
    return -1;
  }

  // Initialize work struct
  if (prepare_test_params(&params)) {
    std::cout << "Prepare test parameter failed" << std::endl;
    return -1;
  }
  test_work.magic = DSP_ALGORITHM_WORK_MAGIC;
  test_work.id = 0x78787878;
  test_work.algorithm.type = 0x4000000b;
  test_work.algorithm.packet_virt = (u32)&params;
  test_work.algorithm.size = sizeof(params);

  // Request DSP work
  std::cout << "Test work start" << std::endl;
  ioctl(dsp_fd, DSP_IOC_QUEUE_WORK, &test_work);
  ioctl(dsp_fd, DSP_IOC_DEQUEUE_WORK, &test_work);
  std::cout << "Test work done" << std::endl;

  close(dsp_fd);
  return 0;
}
