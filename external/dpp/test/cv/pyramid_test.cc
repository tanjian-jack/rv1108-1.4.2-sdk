/*
 *  Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 *  Author: ZhiQiang Zhuo  <zzq@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <iostream>
#include <sys/mman.h>
#include <ion/ion.h>
#include "dsp_ioctl.h"

#define WIDTH        512
#define HEIGHT       512
#define NUM_LEVELS   3
#define INPUT_SIZE   (WIDTH * HEIGHT)
#define OUTPUT_SIZE  (INPUT_SIZE * NUM_LEVELS)

enum KernelSize {
  // SIZE3x3   = 1, SIZE3X3 is not supported for pyramid.
  SIZE5x5   = 2,
  SIZE7x7   = 3,
  SIZE9x9   = 4,
  SIZE11x11 = 5
};

enum PyramidType {
  GAUSSIAN,
  LAPLACIAN
};

struct pyramid_params {
  unsigned long src_phy;
  unsigned long dst_phy;
  uint32_t frame_width;
  uint32_t frame_height;
  KernelSize ksize;
  PyramidType pyramid_type;
  uint num_levels;
  uint stride;
  void* src_virt;
  void* dst_virt;
};

static int prepare_test_params(struct pyramid_params *params) {
  int ret = -1;
  int client = -1;
  ion_user_handle_t src_hdl, dst_hdl;

  int share_fd_src;
  int share_fd_dst;

  void* buffer_src;
  void* buffer_dst;

  client = ion_open();
  if (client < 0) {
    printf("Cannot open ion device.\n");
    return -1;
  }

  ret = ion_alloc(client, INPUT_SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl);
  if (ret) {
    printf("Cannot alloc test src buffer.\n");
    return ret;
  }

  ret = ion_alloc(client, OUTPUT_SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &dst_hdl);
  if (ret) {
    printf("Cannot alloc test dst buffer.\n");
    return ret;
  }

  ret = ion_share(client, src_hdl, &share_fd_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl);
    return -1;
  }

  ret = ion_share(client, dst_hdl, &share_fd_dst);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, dst_hdl);
    return -1;
  }

  buffer_src = mmap(NULL, INPUT_SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd_src, 0);
  buffer_dst = mmap(NULL, OUTPUT_SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd_dst, 0);

  ion_get_phys(client, src_hdl, &params->src_phy);
  ion_get_phys(client, dst_hdl, &params->dst_phy);

  params->src_virt = buffer_src;
  params->dst_virt = buffer_dst;

  params->pyramid_type = GAUSSIAN;
  //params->pyramid_type = LAPLACIAN;
  params->ksize = SIZE5x5;
  params->num_levels = NUM_LEVELS;
  params->stride = WIDTH;

  params->frame_width = WIDTH;
  params->frame_height = HEIGHT;

  ion_close(client);

  std::ifstream istream;

  istream.open("/mnt/sdcard/input.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input file.\n");
    return false;
  }

  istream.seekg(0, istream.end);
  size_t length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input data length=%d\n", length);
  istream.read((char*)params->src_virt, length);

  istream.close();

  return 0;
}

static int pyramid_get_dst_size(int level_num, int width, int height) {
  if (level_num == 0)
    return width * height;

  return (width / (level_num * 2)) * (height / (level_num * 2));
}

int main(int argc, char **argv) {
  int dsp_fd = -1;
  struct dsp_user_work test_work;
  struct pyramid_params params;

  memset(&test_work, 0, sizeof(test_work));
  memset(&params, 0, sizeof(params));

  // Open DSP device
  dsp_fd = open("/dev/dsp", O_RDWR);
  if (dsp_fd < 0) {
    std::cout << "Cannot open dsp device" << std::endl;
    return -1;
  }

  // Initialize work struct
  if (prepare_test_params(&params)) {
    std::cout << "Prepare test parameter failed" << std::endl;
    return -1;
  }
  test_work.magic = DSP_ALGORITHM_WORK_MAGIC;
  test_work.id = 0x78787878;
  test_work.algorithm.type = 0x40000009;
  test_work.algorithm.packet_virt = (u32)&params;
  test_work.algorithm.size = sizeof(params);

  // Request DSP work
  std::cout << "Test work start" << std::endl;
  ioctl(dsp_fd, DSP_IOC_QUEUE_WORK, &test_work);
  ioctl(dsp_fd, DSP_IOC_DEQUEUE_WORK, &test_work);
  std::cout << "Test work done" << std::endl;

  // Write output pyramid
  uint8_t* output = (uint8_t*)params.dst_virt;
  for (int i = 0; i < NUM_LEVELS; i++) {
    int size = pyramid_get_dst_size(i, WIDTH, HEIGHT);

    char output_name[128] = { 0 };
    sprintf(output_name, "/mnt/sdcard/pyramid%d.yuv", i);

    std::ofstream fstream;
    fstream.open(output_name, std::ios_base::binary);
    fstream.write((char*)output, size);
    fstream.close();

    output += size;
  }

  close(dsp_fd);
  return 0;
}
