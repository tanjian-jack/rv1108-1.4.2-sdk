/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *  Author: ZhiChao Yu  zhichao.yu@rock-chips.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <iostream>
#include <sys/mman.h>
#include <ion/ion.h>
#include "dsp_ioctl.h"

#define WIDTH  640
#define HEIGHT 480
#define SIZE   (WIDTH * HEIGHT * 3) // YUV400

typedef short feature_pos_t;

typedef struct {
    feature_pos_t* p_x;
    feature_pos_t* p_y;
    uint* p_id;
    uint count;
    uint capacity;
} feature_list_t;

struct fast9_test_params {
  unsigned long src_phy;
  unsigned long dst_phy;
  unsigned long width;
  unsigned long height;
  feature_list_t feature_list;
  void* src_virt;
  void* dst_virt;
};

static int prepare_test_params(struct fast9_test_params *params) {
  int ret = -1;
  int client = -1;
  ion_user_handle_t src_hdl, dst_hdl;

  int share_fd_src;
  int share_fd_dst;

  void* buffer_src;
  void* buffer_dst;

  client = ion_open();
  if (client < 0) {
    printf("Cannot open ion device.\n");
    return -1;
  }

  ret = ion_alloc(client, SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &src_hdl);
  if (ret) {
    printf("Cannot alloc test src buffer.\n");
    return ret;
  }

  ret = ion_alloc(client, SIZE, 0, ION_HEAP_TYPE_DMA_MASK, 0, &dst_hdl);
  if (ret) {
    printf("Cannot alloc test dst buffer.\n");
    return ret;
  }

  ret = ion_share(client, src_hdl, &share_fd_src);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, src_hdl);
    return -1;
  }

  ret = ion_share(client, dst_hdl, &share_fd_dst);
  if (ret) {
    printf("Share ion failed.\n");
    ion_free(client, dst_hdl);
    return -1;
  }

  buffer_src = mmap(NULL, SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd_src, 0);
  buffer_dst = mmap(NULL, SIZE, PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_LOCKED, share_fd_dst, 0);

  ion_get_phys(client, src_hdl, &params->src_phy);
  ion_get_phys(client, dst_hdl, &params->dst_phy);

  params->src_virt = buffer_src;
  params->dst_virt = buffer_dst;

  params->width = WIDTH;
  params->height = HEIGHT;

  ion_close(client);

  std::ifstream istream;
  istream.open("/mnt/sdcard/input.yuv",
               std::ios_base::in | std::ios_base::binary);
  if (!istream) {
    printf("Cannot open input file.\n");
    return false;
  }

  istream.seekg(0, istream.end);
  size_t length = istream.tellg();
  istream.seekg(0, istream.beg);

  printf("Read input data length=%d\n", length);
  istream.read((char*)params->src_virt, length);

  istream.close();

  return 0;
}

int main(int argc, char **argv) {
  int dsp_fd = -1;
  struct dsp_user_work test_work;
  struct fast9_test_params params;

  memset(&test_work, 0, sizeof(test_work));
  memset(&params, 0, sizeof(params));

  // Open DSP device
  dsp_fd = open("/dev/dsp", O_RDWR);
  if (dsp_fd < 0) {
    std::cout << "Cannot open dsp device" << std::endl;
    return -1;
  }

  // Initialize work struct
  if (prepare_test_params(&params)) {
    std::cout << "Prepare test parameter failed" << std::endl;
    return -1;
  }
  test_work.magic = DSP_ALGORITHM_WORK_MAGIC;
  test_work.id = 0x78787878;
  test_work.algorithm.type = 0x40000001;
  test_work.algorithm.packet_virt = (u32)&params;
  test_work.algorithm.size = sizeof(params);

  // Request DSP work
  std::cout << "Test work start" << std::endl;
  ioctl(dsp_fd, DSP_IOC_QUEUE_WORK, &test_work);
  ioctl(dsp_fd, DSP_IOC_DEQUEUE_WORK, &test_work);
  std::cout << "Test work done" << std::endl;

  printf("Feature list count=%d\n", params.feature_list.count);

  close(dsp_fd);
  return 0;
}
