/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_TABLE_H_
#define DPP_TABLE_H_

#include <stdint.h>

#include "error.h"

#include "dpp_packet.h"
#include "dpp_frame.h"

#include "algo/algorithm_type.h"
#include "common/config.h"
#include "mm/buffer.h"
#include "mm/cma_allocator.h"
#include "mm/pooled_buffer.h"
#include "mm/wrap_buffer.h"

#include "device/dsp/dsp_params.h"

namespace rockchip {
namespace dpp {

typedef struct sTaskInfo {
    Buffer::SharedPtr input_buffer;
    Buffer::SharedPtr output_buffer;
    Buffer::SharedPtr pk_buffer;
    Buffer::SharedPtr rtf_buffer;
    Buffer::SharedPtr vec_buffer;
    Buffer::SharedPtr idc_input_buffer;
    Buffer::SharedPtr map_buffer;

    DppPacket* packet;
    DppFrame* frame;

    uint64_t index;
} TaskInfo;

typedef struct sDppTable {
    uint32_t size;

    BufferPool* frame_pool;
    BufferPool* idc_pool;
    BufferPool* pk_pool;
    BufferPool* rtf_pool;
    BufferPool* vec_pool;

    TaskInfo task_info[FRAME_OUT_BUFF_LIMIT];
    uint64_t frame_count;
} DppTable;

typedef enum eFrameIndex{
    FRM_IDX_PREV,
    FRM_IDX_CURR,

    FRM_IDX_BUTT,
} FrameIndex;

DppRet dpp_table_init(DppTable** table);
DppRet dpp_table_deinit(DppTable* table);
DppRet dpp_table_put_packet(DppTable* table, AlgorithmType type, DppPacket* packet);
DppFrame* dpp_table_get_frame(DppTable* table, AlgorithmType type,
                             uint64_t frm_cnt, union algorithm_params *params);
void dpp_table_buffer_release(DppTable* table, AlgorithmType type);
uint64_t dpp_table_curr_frm_cnt(DppTable* table);
DppRet dpp_table_dsp_config(DppTable* table, AlgorithmType type, uint64_t count,
                            union algorithm_params *params, int fd);

} // namespace dpp
} // namespace rockchip

#endif // DPP_TABLE_H_
