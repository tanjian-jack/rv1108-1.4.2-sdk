/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "dpp_packet.h"

#include "utils/logger.h"

DppRet dpp_packet_init(DppPacket** packet, uint32_t width, uint32_t height)
{
    if (!packet) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return kErrorParams;
    }

    (*packet) = new DppPacket();
    if (!(*packet)) {
        DPPE("Create packet failed\n");
        return kErrorMem;
    }
    (*packet)->input_width = width;
    (*packet)->input_height = height;

    return kSuccess;
}

DppRet dpp_packet_deinit(DppPacket* packet)
{
    if (!packet) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return kErrorParams;
    }

    packet->input_buffer.reset();

    if (packet->params)
        free(packet->params);

    delete packet;
    return kSuccess;
}

void dpp_packet_set_params(DppPacket* packet, void* params, uint32_t size)
{
    if (!packet || size == 0 || !params) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return;
    }
    packet->params = malloc(size);
    packet->params_size = size;
    memcpy(packet->params, params, size);
}

DppRet dpp_packet_get_params(const DppPacket* packet, void** params, uint32_t* size)
{
    if (params) {
        (*params) = packet->params;
        (*size) = packet->params_size;
        return kSuccess;
    }

    return kFailure;
}

void dpp_packet_set_noise(DppPacket* packet, uint32_t* noise)
{
    packet->noise[0] = noise[0];
    packet->noise[1] = noise[1];
    packet->noise[2] = noise[2];
}

DppRet dpp_packet_get_noise(const DppPacket* packet, uint32_t* noise)
{
    noise[0] = packet->noise[0];
    noise[1] = packet->noise[1];
    noise[2] = packet->noise[2];

    return kSuccess;
}

/*
 * object access function macro
 */
#define DPP_PACKET_ACCESSORS(type, field)                  \
    type dpp_packet_get_##field(const DppPacket* packet)   \
    {                                                      \
        return packet->field;                              \
    }                                                      \
    void dpp_packet_set_##field(DppPacket* packet, type v) \
    {                                                      \
        packet->field = v;                                 \
    }

DPP_PACKET_ACCESSORS(dpp::Buffer::SharedPtr, input_buffer)
DPP_PACKET_ACCESSORS(void*, private_data)
DPP_PACKET_ACCESSORS(uint32_t, input_width)
DPP_PACKET_ACCESSORS(uint32_t, input_height)
DPP_PACKET_ACCESSORS(uint32_t, output_width)
DPP_PACKET_ACCESSORS(uint32_t, output_height)
DPP_PACKET_ACCESSORS(uint32_t, adas_mode)
DPP_PACKET_ACCESSORS(struct timeval, pts)
DPP_PACKET_ACCESSORS(int, idc_enabled)
DPP_PACKET_ACCESSORS(int, nr_enabled)
DPP_PACKET_ACCESSORS(int, odt_enabled)
DPP_PACKET_ACCESSORS(int, ldw_enabled)
DPP_PACKET_ACCESSORS(uint64_t, exp_start)
DPP_PACKET_ACCESSORS(uint64_t, exp_end)
DPP_PACKET_ACCESSORS(uint32_t, downscale_width)
DPP_PACKET_ACCESSORS(uint32_t, downscale_height)
DPP_PACKET_ACCESSORS(int, downscale_enabled)
