/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dpp_table.h"

#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>

#include <iostream>
#include <fstream>

#include <rk_rga/rk_rga.h>
#include <rk_rga/rga_copybit.h>

#include "mm/buffer_pool.h"
#include "utils/logger.h"
#if ENABLE_ODT_ADAS
#include "libodt_adas/OdtAdasWrapper.h"
#endif

#include "algo/face/face.h"

#define FIRST_FRAME_CNT 0

namespace rockchip {
namespace dpp {

// IDC remap tables
static const char* g_idc_map_path = "/etc/dsp/idc/dcmapxyfixed.bin";

DppRet dpp_table_init(DppTable** table)
{
    DppRet ret = kSuccess;

    if (!table)
        return kErrorParams;

    DppTable* p = (DppTable*)calloc(1, sizeof(DppTable));
    if (!p) {
        DPPE("malloc failed\n");
        return kErrorMem;
    }
    p->frame_count = FIRST_FRAME_CNT;

    p->frame_pool = new BufferPool("frame_pool", FRAME_OUT_BUFF_LIMIT);
    if(!p->frame_pool) {
        DPPE("Create frame buffer pool failed\n");
        goto error;
    }

    p->idc_pool = new BufferPool("idc_pool", FRAME_OUT_BUFF_LIMIT);
    if(!p->idc_pool) {
        DPPE("Create idc buffer pool failed\n");
        goto error;
    }

    p->pk_pool = new BufferPool("pk_pool", FRAME_OUT_BUFF_LIMIT);
    if(!p->pk_pool) {
        DPPE("Create pk buffer pool failed\n");
        goto error;
    }

    p->rtf_pool = new BufferPool("rtf_pool", FRAME_OUT_BUFF_LIMIT);
    if(!p->rtf_pool) {
        DPPE("Create rtf buffer pool failed\n");
        goto error;
    }

    p->vec_pool = new BufferPool("vec_pool", FRAME_OUT_BUFF_LIMIT);
    if(!p->vec_pool) {
        DPPE("Create rtf buffer pool failed\n");
        goto error;
    }

    *table = p;

error:
    if (ret != kSuccess) {
        if (p->frame_pool)
            delete p->frame_pool;

        if (p->idc_pool)
            delete p->idc_pool;

        if (p->pk_pool)
            delete p->pk_pool;

        if (p->rtf_pool)
            delete p->rtf_pool;

        if (p->vec_pool)
            delete p->vec_pool;

        *table = nullptr;
    }

    return ret;
}

DppRet dpp_table_deinit(DppTable* table)
{
    if (!table) {
        DPPE("invalid NULL pointer input\n");
        return kErrorParams;
    }

    if (table->frame_pool)
        delete table->frame_pool;

    if (table->idc_pool)
        delete table->idc_pool;

    if (table->pk_pool)
        delete table->pk_pool;

    if (table->rtf_pool)
        delete table->rtf_pool;

    if (table->vec_pool)
        delete table->vec_pool;

    free(table);
    return kSuccess;
}

static TaskInfo *dpp_table_get_prev_task(DppTable* table, uint64_t count)
{
    uint32_t pos = count % FRAME_OUT_BUFF_LIMIT;

    if(pos == 0)
        pos = FRAME_OUT_BUFF_LIMIT - 1;
    else
        pos -= 1;

    return &table->task_info[pos];
}

static inline TaskInfo *dpp_table_get_task(DppTable* table, uint64_t count)
{
    return &table->task_info[count % FRAME_OUT_BUFF_LIMIT];
}

static inline void dpp_table_frm_inc(DppTable* table)
{
    table->frame_count++;
}

uint64_t dpp_table_curr_frm_cnt(DppTable* table)
{
    if (!table) {
        DPPE("invalid NULL pointer input\n");
        return kErrorParams;
    }

    return table->frame_count;
}


static DppRet dpp_table_prepare_idc_map(DppTable* table, TaskInfo* first_task)
{
    uint32_t map_size = 0;

    if (!first_task->map_buffer) {
        // Read tables.
        std::ifstream fstream;
        fstream.open(g_idc_map_path, std::ios_base::in | std::ios_base::binary);
        fstream.seekg(0, fstream.end);
        map_size = fstream.tellg();
        first_task->map_buffer = std::shared_ptr<Buffer>(CmaAlloc(map_size));
        fstream.seekg(0, fstream.beg);
        fstream.read((char*)first_task->map_buffer->address(), map_size);
        fstream.close();
    }

    return kSuccess;
}

DppRet dpp_table_put_packet(DppTable* table, AlgorithmType type, DppPacket* packet)
{
  DppFrame* frame = nullptr;

  TaskInfo* curr_task = dpp_table_get_task(table, table->frame_count);
  uint32_t input_width = dpp_packet_get_input_width(packet);
  uint32_t input_height = dpp_packet_get_input_height(packet);
  uint32_t output_width = dpp_packet_get_output_width(packet);
  uint32_t output_height = dpp_packet_get_output_height(packet);
  uint32_t input_size = input_width * input_height * 3 / 2;
  uint32_t output_size = output_width * output_height * 3 / 2;
  curr_task->packet = packet;

  switch (type) {
  case kNoiseReduction: {
    dpp_table_prepare_idc_map(table, dpp_table_get_task(table, 0));

    if(kSuccess != dpp_frame_init(&frame)) {
      DPPE("dpp_frame_init failed.\n");
      return kFailure;
    }
    dpp_frame_set_type(frame, kNoiseReduction);

    // Set output buffer to the created frame, this buffer will be put back
    // to the buffer group when frame is deinited.
    curr_task->input_buffer = dpp_packet_get_input_buffer(packet);
    curr_task->output_buffer = table->frame_pool->GetBuffer(output_size);
    dpp_frame_set_output_buffer(frame, curr_task->output_buffer);

    curr_task->frame = frame;
    dpp_frame_set_output_width(frame, output_width);
    dpp_frame_set_output_height(frame, output_height);

    curr_task->idc_input_buffer = table->idc_pool->GetBuffer(output_size);
    curr_task->pk_buffer = table->pk_pool->GetBuffer(input_size);
    curr_task->rtf_buffer = table->rtf_pool->GetBuffer(input_width * input_height / 4);

    uint32_t vec_size = input_width * input_height * 60 / 1024 + ISP_CONTROL_PARAMS_SIZE;
    curr_task->vec_buffer = table->vec_pool->GetBuffer(vec_size);

    void* params = NULL;
    uint32_t params_size = 0;
    dpp_packet_get_params(packet, &params, &params_size);
    if (params && params_size) {
      if (params_size > ISP_CONTROL_PARAMS_SIZE)
        DPPW("The ISP control params size is larger than %d\n", ISP_CONTROL_PARAMS_SIZE);
      memcpy(curr_task->vec_buffer->address(), params, params_size);
    }

    curr_task->index = table->frame_count;
    break;
  }

  case kImageDistortionCorrection: {
    dpp_table_prepare_idc_map(table, dpp_table_get_task(table, 0));

    if(kSuccess != dpp_frame_init(&frame)) {
      DPPE("dpp_frame_init failed.\n");
      return kFailure;
    }
    curr_task->input_buffer = dpp_packet_get_input_buffer(packet);
    curr_task->output_buffer = table->frame_pool->GetBuffer(output_size);
    curr_task->frame = frame;
    dpp_frame_set_output_buffer(frame, curr_task->output_buffer);
    dpp_frame_set_type(frame, kImageDistortionCorrection);
    dpp_frame_set_output_width(frame, output_width);
    dpp_frame_set_output_height(frame, output_height);

    curr_task->index = table->frame_count;
    break;
  }

  case kFrontCollisionWarning: {
    if(kSuccess != dpp_frame_init(&frame)) {
      DPPE("dpp_frame_init failed.\n");
      return kFailure;
    }
    curr_task->input_buffer = dpp_packet_get_input_buffer(packet);
    curr_task->frame = frame;
    dpp_frame_set_type(frame, kFrontCollisionWarning);
    dpp_frame_set_output_width(frame, output_width);
    dpp_frame_set_output_height(frame, output_height);

    curr_task->index = table->frame_count;
    break;
  }

  default:
    assert(0);
    break;
  }

  dpp_frame_set_private_data(frame, dpp_packet_get_private_data(packet));

  return kSuccess;
}

DppFrame* dpp_table_get_frame(DppTable* table, AlgorithmType type,
                             uint64_t count, union algorithm_params* params)
{
  DppFrame* frame = nullptr;

  if (!table) {
    DPPE("invalid NULL pointer input\n");
    return frame;
  }

  TaskInfo* curr_task = dpp_table_get_task(table, count);
  TaskInfo* prev_task  = dpp_table_get_prev_task(table, count);

  switch (type) {
  case kNoiseReduction: {
    struct nr_params* config_3dnr = (struct nr_params*)params;

    uint32_t noise[3];
    noise[0] = config_3dnr->noise_y;
    noise[1] = config_3dnr->noise_u;
    noise[2] = config_3dnr->noise_v;
    dpp_frame_set_noise(curr_task->frame, noise);
    dpp_frame_set_sharpness(curr_task->frame,
                            (struct dpp_sharpness*)&config_3dnr->sharp);

    frame = curr_task->frame;
    curr_task->frame = nullptr;

    void *params = nullptr;
    uint32_t params_size = 0;
    dpp_packet_get_params(curr_task->packet, &params, &params_size);
    memcpy(params, curr_task->vec_buffer->address(), params_size);
    if(curr_task->vec_buffer)
        curr_task->vec_buffer.reset();

    if(count > FIRST_FRAME_CNT) {
      if(prev_task->pk_buffer)
        prev_task->pk_buffer.reset();

      if(prev_task->rtf_buffer)
        prev_task->rtf_buffer.reset();

      if (prev_task->idc_input_buffer)
        prev_task->idc_input_buffer.reset();

      if(prev_task->output_buffer)
        prev_task->output_buffer.reset();

      if (prev_task->input_buffer)
        prev_task->input_buffer.reset();

      if(prev_task->packet) {
        dpp_packet_deinit(prev_task->packet);
        prev_task->packet = nullptr;
      }
    }
    break;
  }

  case kImageDistortionCorrection: {
    frame = curr_task->frame;
    curr_task->frame = nullptr;

    if (curr_task->output_buffer)
      curr_task->output_buffer.reset();

    if (curr_task->input_buffer)
      curr_task->input_buffer.reset();

    dpp_packet_deinit(curr_task->packet);
    curr_task->packet = nullptr;
    break;
  }

  case kFrontCollisionWarning: {
    struct adas_params* config_adas = (struct adas_params*)params;

    frame = curr_task->frame;
    curr_task->frame = nullptr;

    if (curr_task->input_buffer)
      curr_task->input_buffer.reset();

    struct adas_output* adas_output = (struct adas_output*)calloc(1, sizeof(struct adas_output));

    if (adas_output) {
      memcpy(adas_output, &config_adas->output, sizeof(*adas_output));

#if ENABLE_ODT_ADAS
      if (adas_output->odt.valid)
        OdtWrapper_updateResult((OdtObjectInfos*)&adas_output->odt);
#endif
      dpp_frame_set_adas_output(frame, adas_output);
    }

    dpp_packet_deinit(curr_task->packet);
    curr_task->packet = nullptr;
    break;
  }

  case kFaceDetection: {
    frame = curr_task->frame;
    curr_task->frame = nullptr;

    if (curr_task->output_buffer)
      curr_task->output_buffer.reset();

    if (curr_task->input_buffer)
      curr_task->input_buffer.reset();

    dpp_packet_deinit(curr_task->packet);
    curr_task->packet = nullptr;

    break;
  }

  case kRsBodyDetection:
  case kRsFaceDetection: {
    frame = curr_task->frame;
    curr_task->frame = nullptr;

    if (curr_task->output_buffer)
      curr_task->output_buffer.reset();

    if (curr_task->input_buffer)
      curr_task->input_buffer.reset();

    if(curr_task->pk_buffer)
      curr_task->pk_buffer.reset();

    // Move this code to dpp_table_config
    // dpp_packet_deinit(curr_input->packet);
    // curr_input->packet = nullptr;
    break;
  }

  default:
    assert(0);
    break;
  }

  return frame;
}

void dpp_table_buffer_release(DppTable* table, AlgorithmType type)
{
  if (!table) {
    DPPE("invalid NULL pointer input\n");
    return;
  }

  switch (type) {
  default: {
    for(int i = 0; i < FRAME_OUT_BUFF_LIMIT; i++) {
      TaskInfo* curr_task = dpp_table_get_task(table, i);

      if (curr_task->output_buffer)
        curr_task->output_buffer.reset();

      if (curr_task->rtf_buffer)
        curr_task->rtf_buffer.reset();

      if (curr_task->vec_buffer)
        curr_task->vec_buffer.reset();

      if (curr_task->pk_buffer)
        curr_task->pk_buffer.reset();

      if (curr_task->idc_input_buffer)
        curr_task->idc_input_buffer.reset();

      if (curr_task->map_buffer)
        curr_task->map_buffer.reset();

      if (curr_task->input_buffer)
        curr_task->input_buffer.reset();

      if(curr_task->packet) {
        dpp_packet_deinit(curr_task->packet);
        curr_task->packet = nullptr;
      }
      if(curr_task->frame) {
        dpp_frame_deinit(curr_task->frame);
        curr_task->frame = nullptr;
      }
    }
    break;
  }
  }
}

int dpp_rga_yuv_scal(int src_fd, int src_w, int src_h, int dst_fd, int dst_w, int dst_h,
  int x, int y, int scal_w, int scal_h, int src_vir_w, int src_vir_h)
{
  struct rga_req  req;
  int ret = 0;

  int fd;

  if(!access("/dev/rga", R_OK | W_OK)) {
    fd = open("/dev/rga", O_RDWR, 0);
    if(fd < 0) {
      printf("open rga device error\n");
      return -1;
    }
  } else {
    fd = -1;
  }

  if(fd < 0) {
    printf("%s: rga is not opened.\n", __func__);
    return -1;
  }

  memset(&req, 0x0, sizeof(req));

  req.src.act_w = src_w;
  req.src.act_h = src_h;

  req.src.vir_w = src_vir_w;
  req.src.vir_h = src_vir_h;
  req.src.yrgb_addr = src_fd;
  req.src.uv_addr = 0;
  req.src.v_addr = 0;
  req.src.format = RGA_FORMAT_YCBCR_420_SP;

  req.dst.act_w = scal_w;
  req.dst.act_h = scal_h;

  req.dst.vir_w = dst_w;
  req.dst.vir_h = dst_h;
  req.dst.yrgb_addr = dst_fd;
  req.dst.x_offset = x;
  req.dst.y_offset = y;

  req.dst.format = RGA_FORMAT_YCBCR_420_SP;

  req.render_mode = 0;
  req.mmu_info.mmu_en = 0;
  req.mmu_info.mmu_flag = ((2 & 0x3) << 4) | 1 | 1 << 31 | 1 << 8 | 1 << 10;

  ret = ioctl(fd, RGA_BLIT_SYNC, &req);

  if(ret != 0) {
    printf("%s:  rga operation error\n", __func__);
    return -1;
  }

  if (fd >= 0)
    close(fd);

  return 0;
}

DppRet dpp_table_frame_rga_zoom(TaskInfo* task, Buffer::SharedPtr input,
                                Buffer::SharedPtr output)
{
    int src_fd, src_w, src_h, dst_fd, dst_w, dst_h;

    if(!task || !output)
        return kErrorPtr;

    src_fd = input->fd();
    src_w = dpp_packet_get_input_width(task->packet);
    src_h = dpp_packet_get_input_height(task->packet);
    dst_fd = output->fd();
    dst_w = src_w / 4;
    dst_h = src_h / 4;
    //DPPI("src_w:%d,src_h:%d,dst_w:%d,dst_h:%d\n",src_w,src_h,dst_w,dst_h);
    if (dpp_rga_yuv_scal(src_fd, src_w, src_h,
                         dst_fd, dst_w, dst_h,
                         0, 0, dst_w, dst_h, src_w, src_h)) {
        DPPE("%s: rk_rga_ionfdnv12_to_ionfdnv12_scal fail!\n", __func__);
        return kFailure;
    }

    return kSuccess;
}

DppRet dpp_table_dsp_config(DppTable* table, AlgorithmType type, uint64_t count,
                            union algorithm_params *params, int fd)
{
  if (!table || !params) {
    DPPE("invalid NULL pointer input\n");
    return kErrorParams;
  }

  TaskInfo* curr_task  = dpp_table_get_task(table, count);
  TaskInfo* prev_task  = dpp_table_get_prev_task(table, count);
  TaskInfo* first_task = dpp_table_get_task(table, 0);

  switch (type) {
  case kNoiseReduction: {
    struct nr_params* config_3dnr = (struct nr_params*)params;

    if(count > FIRST_FRAME_CNT) {
      config_3dnr->prev_input = prev_task->input_buffer->phys_address();
      config_3dnr->prev_output = prev_task->output_buffer->phys_address();
      config_3dnr->prev_pk = prev_task->pk_buffer->phys_address();
      config_3dnr->prev_rtf = prev_task->rtf_buffer->phys_address();
      config_3dnr->prev_idc_input = prev_task->idc_input_buffer->phys_address();
    }

    config_3dnr->curr_input = curr_task->input_buffer->phys_address();
    config_3dnr->curr_output = curr_task->output_buffer->phys_address();
    config_3dnr->curr_pk = curr_task->pk_buffer->phys_address();
    config_3dnr->curr_rtf = curr_task->rtf_buffer->phys_address();
    config_3dnr->curr_vec = curr_task->vec_buffer->phys_address();
    config_3dnr->curr_idc_input = curr_task->idc_input_buffer->phys_address();

    if (first_task->map_buffer) {
      config_3dnr->map_table = first_task->map_buffer->phys_address();
      config_3dnr->idc_enabled = dpp_packet_get_idc_enabled(curr_task->packet);
    } else {
      config_3dnr->idc_enabled = 0;
    }
    config_3dnr->nr_enabled = dpp_packet_get_nr_enabled(curr_task->packet);

    config_3dnr->width = dpp_frame_get_output_width(curr_task->frame);
    config_3dnr->height = dpp_frame_get_output_height(curr_task->frame);
    config_3dnr->frame_count = dpp_table_curr_frm_cnt(table);

    uint32_t noise[3];
    dpp_packet_get_noise(curr_task->packet, noise);
    config_3dnr->noise_y = noise[0];
    config_3dnr->noise_u = noise[1];
    config_3dnr->noise_v = noise[2];

    dpp_frame_set_pts(curr_task->frame, dpp_packet_get_pts(curr_task->packet));
    dpp_frame_set_exp_start(curr_task->frame, dpp_packet_get_exp_start(curr_task->packet));
    dpp_frame_set_exp_end(curr_task->frame, dpp_packet_get_exp_end(curr_task->packet));

    break;
  }

  case kImageDistortionCorrection: {
    struct idc_params* config_idc = (struct idc_params*)params;

    config_idc->map_table = first_task->map_buffer->phys_address();
    config_idc->input = curr_task->input_buffer->phys_address();
    config_idc->output = curr_task->output_buffer->phys_address();
    config_idc->width = dpp_frame_get_output_width(curr_task->frame);
    config_idc->height = dpp_frame_get_output_height(curr_task->frame);
    config_idc->enabled = dpp_packet_get_idc_enabled(curr_task->packet);

    dpp_frame_set_pts(curr_task->frame, dpp_packet_get_pts(curr_task->packet));

    break;
  }

  case kFrontCollisionWarning: {
    struct adas_params* config_adas = (struct adas_params*)params;

    config_adas->width = dpp_packet_get_input_width(curr_task->packet);
    config_adas->height = dpp_packet_get_input_height(curr_task->packet);
    config_adas->mode = dpp_packet_get_adas_mode(curr_task->packet);
    config_adas->input = curr_task->input_buffer->phys_address();
    config_adas->ldw_enabled = dpp_packet_get_ldw_enabled(curr_task->packet);
    config_adas->odt_enabled = dpp_packet_get_odt_enabled(curr_task->packet);
    break;
  }

  default:
    assert(0);
    break;
  }

  dpp_table_frm_inc(table);

  return kSuccess;
}

} // namespace dpp
} // namespace rockchip
