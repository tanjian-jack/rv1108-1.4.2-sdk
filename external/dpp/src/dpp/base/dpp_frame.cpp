/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "dpp_frame.h"

#include "utils/logger.h"

DppRet dpp_frame_init(DppFrame** frame)
{
    if (!frame)
        return kErrorParams;

    (*frame) = new DppFrame();
    if (!(*frame)) {
        DPPE("Cannot create DppFrame\n");
        return kErrorMem;
    }

    return kSuccess;
}

DppRet dpp_frame_deinit(DppFrame* frame)
{
    if (!frame) {
        DPPE("Invalid parameter, fun=%s\n", __func__);
        return kErrorParams;
    }

    if (frame->adas_output)
        free(frame->adas_output);

    frame->output_buffer.reset();
    frame->downscale.reset();

    delete frame;
    return kSuccess;
}

void dpp_frame_get_noise(const DppFrame* frame, uint32_t* noise)
{
    noise[0] = frame->noise[0];
    noise[1] = frame->noise[1];
    noise[2] = frame->noise[2];
}

void dpp_frame_set_noise(DppFrame* frame, uint32_t* noise)
{
    frame->noise[0] = noise[0];
    frame->noise[1] = noise[1];
    frame->noise[2] = noise[2];
}

void dpp_frame_get_sharpness(const DppFrame* frame, struct dpp_sharpness* sharpness)
{
    memcpy(sharpness, &frame->sharpness, sizeof(*sharpness));
}

void dpp_frame_set_sharpness(DppFrame* frame, struct dpp_sharpness* sharpness)
{
    memcpy(&frame->sharpness, sharpness, sizeof(*sharpness));
}

#define DPP_FRAME_ACCESSORS(type, field)                \
    type dpp_frame_get_##field(const DppFrame* frame)   \
    {                                                   \
        return frame->field;                            \
    }                                                   \
    void dpp_frame_set_##field(DppFrame* frame, type v) \
    {                                                   \
        frame->field = v;                               \
    }

DPP_FRAME_ACCESSORS(dpp::Buffer::SharedPtr, output_buffer)
DPP_FRAME_ACCESSORS(uint32_t, output_width)
DPP_FRAME_ACCESSORS(uint32_t, output_height)
DPP_FRAME_ACCESSORS(void*, private_data)
DPP_FRAME_ACCESSORS(AlgorithmType, type)
DPP_FRAME_ACCESSORS(struct adas_output*, adas_output)
DPP_FRAME_ACCESSORS(struct timeval, pts)
DPP_FRAME_ACCESSORS(uint64_t, exp_start)
DPP_FRAME_ACCESSORS(uint64_t, exp_end)
DPP_FRAME_ACCESSORS(dpp::Buffer::SharedPtr, downscale)
DPP_FRAME_ACCESSORS(uint32_t, downscale_width)
DPP_FRAME_ACCESSORS(uint32_t, downscale_height)