/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_CMA_BUFFER_H_
#define DPP_MM_CMA_BUFFER_H_

#include <assert.h>

#include <ion/ion.h>
#include <sys/mman.h>

#include "buffer.h"

#include "common/definition_magic.h"
#include "utils/logger.h"

namespace rockchip {
namespace dpp {

class CmaBuffer : public Buffer {
 public:
    CmaBuffer() = delete;

    DPP_DECLARE_SHARED_PTR(CmaBuffer);

    CmaBuffer(int ion_handle, int fd,
              uint32_t phys_address, const size_t size)
        : Buffer(size, nullptr, phys_address, fd) {
        ion_handle_ = ion_handle;
    }

    virtual ~CmaBuffer() {
        if (address_)
            munmap(address_, size_);
        release();
    }

    virtual void release(void) override;

    virtual void* address(void) override {
        if (address_ == nullptr) {
            address_ = mmap(NULL, size_, PROT_READ | PROT_WRITE,
                            MAP_SHARED | MAP_LOCKED, fd_, 0);

            assert(address_ != nullptr);

            DPPI("CmaBuffer mapped, fd=%d, address=0x%p\n", fd_, address_);
        }

        return address_;
    }

    int ion_handle(void) const {
        return ion_handle_;
    }

 private:
    int ion_handle_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_CMA_BUFFER_H_