/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cma_allocator.h"

#include <unistd.h>

#include "cma_buffer.h"

#include "utils/logger.h"

namespace rockchip {
namespace dpp {

Buffer* CmaAllocator::alloc(const size_t size)
{
    ion_user_handle_t hdl;
    int ret = ion_alloc(ion_client_, size, 0, ION_HEAP_TYPE_DMA_MASK, 0, &hdl);
    if (ret) {
        DPPE("Alloc ion buffer failed.\n");
        return nullptr;
    }

    int fd;
    ret = ion_share(ion_client_, hdl, &fd);
    if (ret) {
        DPPE("Share ion failed.\n");
        ion_free(ion_client_, hdl);
        return nullptr;
    }

    unsigned long phys_address;
    ion_get_phys(ion_client_, hdl, &phys_address);

    CmaBuffer* cma_buffer = new CmaBuffer(hdl, fd, phys_address, size);

    usage_ += size;

    DPPD("CmaAllocator::alloc: fd=%d, size=%d, total=%d\n", fd, size, usage_);
    return cma_buffer;
}

void CmaAllocator::free(Buffer* buffer)
{
    CmaBuffer* cma_buffer = dynamic_cast<CmaBuffer*>(buffer);
    if (cma_buffer == nullptr)
        return;

    int fd = cma_buffer->fd();
    if (fd > 0)
        close(fd);

    usage_ -= cma_buffer->size();

    DPPD("CmaAllocator::free: fd=%d, size=%d, remain=%d\n",
         cma_buffer->fd(), cma_buffer->size(), usage_);

    ion_user_handle_t hdl = cma_buffer->ion_handle();
    if (hdl) {
        int ret = ion_free(ion_client_, hdl);
        if (ret)
            DPPE("Free ion buffer failed!\n");
    }
}

} // namespace dpp
} // namespace rockchip