/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_WRAP_BUFFER_H_
#define DPP_MM_WRAP_BUFFER_H_

#include "buffer.h"

#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

typedef void(*ReleaseWrapBuffer)(void* data);

class WrapBuffer : public Buffer {
 public:
    WrapBuffer() = delete;
    WrapBuffer(const int fd, void* address,
               const uint32_t phys_address, const size_t size, void* data)
        : Buffer(size, address, phys_address, fd), private_data_(data) {}

    virtual ~WrapBuffer() {
        release();
    }

    DPP_DECLARE_SHARED_PTR(WrapBuffer);

    virtual void release(void) override {
        if (release_)
            release_(private_data_);
    }

    virtual void RegisterReleaseCallback(ReleaseWrapBuffer release) {
        release_ = release;
    }

    const uint32_t phys_address(void) const override {
        return phys_address_;
    }

    void set_private_data(void* data) {
        private_data_ = data;
    }

    void* private_data(void) const {
        return private_data_;
    }

 private:
    void* private_data_;
    ReleaseWrapBuffer release_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_WRAP_BUFFER_H_