/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_CMA_ALLOCATOR_H_
#define DPP_MM_CMA_ALLOCATOR_H_

#include <assert.h>

#include <ion/ion.h>

#include "allocator.h"

namespace rockchip {
namespace dpp {

// CmaAllocator class is singleton.
class CmaAllocator : public Allocator {
 public:
    virtual Buffer* alloc(const size_t size) override;
    virtual void free(Buffer* buffer) override;

    static CmaAllocator& GetInstance() {
        static CmaAllocator dpp_cma_allocator;
        return dpp_cma_allocator;
    }

 private:
    CmaAllocator() {
        ion_client_ = ion_open();
        usage_ = 0;
        assert(ion_client_ != 0);
    }

    CmaAllocator(const CmaAllocator &);
    CmaAllocator& operator = (const CmaAllocator &);

    virtual ~CmaAllocator() {
        ion_close(ion_client_);
        ion_client_ = 0;
        assert(usage_ == 0);
    }

    int usage_;
    int ion_client_;
};

#define CmaAlloc(size) CmaAllocator::GetInstance().alloc(size)
#define CmaFree(buffer) CmaAllocator::GetInstance().free(buffer)

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_CMA_ALLOCATOR_H_