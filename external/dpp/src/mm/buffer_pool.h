/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_BUFFER_POOL_H_
#define DPP_MM_BUFFER_POOL_H_

#include "buffer.h"

#include "base/synchronized_list.h"
#include "common/error.h"
#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

class BufferPool {
 public:
    BufferPool() = delete;
    BufferPool(const char* name, uint32_t capacity)
        : name_(name), capacity_(capacity), used_count_(0) {}

    virtual ~BufferPool();

    DPP_DECLARE_SHARED_PTR(BufferPool);

    const uint32_t capacity(void) const {
        return capacity_;
    }

    void set_capacity(const uint32_t capacity) {
        capacity_ = capacity;
    }

    const uint32_t unused_count(void) const {
        return unused_.size() + (capacity_ - used_count_);
    }

    virtual Buffer::SharedPtr GetBuffer(size_t size);

    virtual DppRet PutBuffer(Buffer::SharedPtr buffer);

 private:
    std::string name_;
    uint32_t capacity_;
    uint32_t used_count_;
    SynchronizedList<Buffer::SharedPtr> unused_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_BUFFER_POOL_H_