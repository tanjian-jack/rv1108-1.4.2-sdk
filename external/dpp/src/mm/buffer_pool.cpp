/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "buffer_pool.h"

#include "cma_allocator.h"
#include "pooled_buffer.h"

#include <assert.h>

#include "utils/logger.h"

namespace rockchip {
namespace dpp {

BufferPool::~BufferPool()
{
    if (used_count_ > 0) {
        DPPE("Memory leak in BufferPool: %s\n", name_.c_str());
        assert(0);
    }

    while (!unused_.empty()) {
        Buffer::SharedPtr buffer = unused_.front();
        unused_.pop_front();
        buffer.reset();
    }
}

Buffer::SharedPtr BufferPool::GetBuffer(size_t size)
{
    std::unique_lock<std::mutex> lock(unused_.mutex());

    DPPI("GetBuffer from %s, unused_size=%d, used_count=%d, capacity=%d\n",
         name_.c_str(), unused_.size(), used_count_, capacity_);

    if (unused_.empty()) {
        if (unused_.size() + used_count_ < capacity_) {
            Buffer::SharedPtr new_buffer =
                    std::shared_ptr<Buffer>(CmaAlloc(size));
            unused_.push_back(new_buffer);
        } else {
            // Waiting buffer released from user. If 1 second timeout happened,
            // it means that the buffer has been leaked somewhere.
            unused_.wait_for(lock, 1);
            while (unused_.empty()) {
                DPPW("BufferPool %s is empty after 1 second, "
                     "maybe leaked somewhere.\n", name_.c_str());
                unused_.wait_for(lock, 1);
            }
        }
    }

    // Use PooledBuffer to wrap the buffer in pool. The real buffer instance
    // will be returned to pool when PooledBuffer destruct.
    Buffer::SharedPtr buffer = unused_.front();
    Buffer::SharedPtr pooled = Buffer::SharedPtr(new PooledBuffer(this, buffer));
    unused_.pop_front();
    used_count_++;
    return pooled;
}

// Should be called in PooledBuffer release() function.
DppRet BufferPool::PutBuffer(Buffer::SharedPtr buffer)
{
    std::unique_lock<std::mutex> lock(unused_.mutex());

    DPPI("Buffer put back to BufferPool: %s\n", name_.c_str());

    unused_.push_back(buffer);
    unused_.signal();
    used_count_--;

    return kSuccess;
}

} // namespace dpp
} // namespace rockchip