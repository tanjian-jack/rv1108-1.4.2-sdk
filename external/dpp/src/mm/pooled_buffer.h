/*
 * Copyright (c) 2018 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_POOLED_BUFFER_H_
#define DPP_MM_POOLED_BUFFER_H_

#include "buffer.h"

#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

class BufferPool;

class PooledBuffer : public Buffer {
 public:
    PooledBuffer() = delete;
    PooledBuffer(BufferPool* pool, Buffer::SharedPtr buffer)
        : Buffer(buffer->size()), pool_(pool), buffer_(buffer) {}

    virtual ~PooledBuffer() {
        release();
    }

    DPP_DECLARE_SHARED_PTR(PooledBuffer);

    virtual size_t size(void) const override {
        return buffer_->size();
    }

    virtual void* address(void) override {
        return buffer_->address();
    }

    virtual void set_address(void* addr) override {
        buffer_->set_address(addr);
    }

    virtual void set_phys_address(const uint32_t phys) override {
        buffer_->set_phys_address(phys);
    }

    virtual const uint32_t phys_address(void) const override {
        return buffer_->phys_address();
    }

    virtual void set_fd(const int fd) override {
        buffer_->set_fd(fd);
    }

    virtual const int fd(void) const override {
        return buffer_->fd();
    }

    virtual void release(void) override;

 private:
    BufferPool* pool_;
    Buffer::SharedPtr buffer_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_POOLED_BUFFER_H_