/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_BUFFER_H_
#define DPP_MM_BUFFER_H_

#include <stddef.h>
#include <stdint.h>

#include "common/definition_magic.h"
#include "utils/logger.h"

namespace rockchip {
namespace dpp {

class Buffer {
 public:
    Buffer() = delete;
    Buffer(size_t size, void* address, uint32_t phys, int fd)
        : size_(size), address_(address), phys_address_(phys), fd_(fd) {}
    Buffer(size_t size, void* address, uint32_t phys)
        : size_(size), address_(address), phys_address_(phys), fd_(-1) {}
    Buffer(size_t size, void* address)
        : size_(size), address_(address), phys_address_(0), fd_(-1) {}
    Buffer(size_t size)
        : size_(size), address_(nullptr), phys_address_(0), fd_(-1) {}

    DPP_DECLARE_SHARED_PTR(Buffer);

    virtual ~Buffer() {
        release();
    }

    virtual size_t size(void) const {
        return size_;
    }

    virtual void* address(void) {
        return address_;
    }

    virtual void set_address(void* addr) {
        address_ = addr;
    }

    virtual void set_phys_address(const uint32_t phys) {
        phys_address_ = phys;
    }

    virtual const uint32_t phys_address(void) const {
        return phys_address_;
    }

    virtual void set_fd(const int fd) {
        fd_ = fd;
    }

    virtual const int fd(void) const {
        return fd_;
    }

    virtual void release(void) {
        size_ = 0;
        address_ = nullptr;
    }

 protected:
    size_t size_;
    void* address_;
    uint32_t phys_address_;
    int fd_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_BUFFER_H_