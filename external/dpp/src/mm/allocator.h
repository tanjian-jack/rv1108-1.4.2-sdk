/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_MM_ALLOCATOR_H_
#define DPP_MM_ALLOCATOR_H_

namespace rockchip {
namespace dpp {

class Buffer;

class Allocator {
 public:
    Allocator() {}
    virtual ~Allocator() {}

    virtual Buffer* alloc(const size_t size) = 0;
    virtual void free(Buffer* buffer) = 0;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_MM_ALLOCATOR_H_