/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "legacy_task.h"

#include <string.h>

static uint32_t dpp_convert_type(AlgorithmType type)
{
    uint32_t dsp_type = 0;

    switch(type) {
    case kNoiseReduction:
        dsp_type = DSP_ALGORITHM_3DNR;
        break;

    case kImageDistortionCorrection:
        dsp_type = DSP_ALGORITHM_IDC;
        break;

    case kDigitalVideoStabilization:
        dsp_type = DSP_ALGORITHM_DVS;
        break;

    case kFrontCollisionWarning:
        dsp_type = DSP_ALGORITHM_ADAS;
        break;

    case kFaceDetection:
        dsp_type = 0x90000001;
        break;

    case kRsFaceDetection:
        dsp_type = 0xa0000001;
        break;

    case kRsBodyDetection:
        dsp_type = 0xa0000002;
        break;

    case kRsFaceTracking:
        dsp_type = 0xa0000003;
        break;

    default:
        DPPE("Unknow algorithm, type=%d\n", type);
        assert(0);
        break;
    }

    return dsp_type;
}

namespace rockchip {
namespace dpp {

DppRet LegacyTask::Prepare(int fd)
{
    dpp_table_put_packet(table_, type_, packet_);
    count_ = dpp_table_curr_frm_cnt(table_);
    set_id((uint32_t)this);

    memset(&dsp_params_, 0, sizeof(dsp_params_));

    dsp_params_.work.id = id();
    dsp_params_.work.magic = DSP_ALGORITHM_WORK_MAGIC;

    dsp_params_.work.algorithm.type = dpp_convert_type(type_);
    dsp_params_.work.algorithm.packet_virt = (unsigned int)(&dsp_params_.algo_params);
    dsp_params_.work.algorithm.size = sizeof(dsp_params_.algo_params);

    return dpp_table_dsp_config(table_, type_, count_, &dsp_params_.algo_params, fd);
}

DppRet LegacyTask::Release(void)
{
    return kSuccess;
}

} // namespace dpp
} // namespace rockchip