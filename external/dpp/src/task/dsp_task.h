 /*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_TASK_DSP_TASK_H_
#define DPP_TASK_DSP_TASK_H_

#include "task.h"

#include "algo/algorithm_type.h"
#include "common/definition_magic.h"

#include "device/dsp/dsp_params.h"

namespace rockchip {
namespace dpp {

class DspTask : public Task {
 public:
    DspTask() = delete;
    DspTask(const AlgorithmType type) : type_(type) {}
    virtual ~DspTask() {}

    DPP_DECLARE_SHARED_PTR(DspTask);

    AlgorithmType type(void) const {
        return type_;
    }

    struct dsp_params* dsp_params(void) {
        return &dsp_params_;
    }

 protected:
    AlgorithmType type_;
    struct dsp_params dsp_params_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_TASK_DSP_TASK_H_