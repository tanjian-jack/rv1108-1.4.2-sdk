/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __DPP_FRAME_H__
#define __DPP_FRAME_H__

#include <stdint.h>
#include <sys/time.h>

#include "algo/algorithm_type.h"
#include "common/error.h"
#include "mm/buffer.h"

#include "algo/adas/adas.h"

using namespace rockchip;

struct dpp_sharpness {
    uint32_t src_shp_w0;
    uint32_t src_shp_w1;
    uint32_t src_shp_w2;
    uint32_t src_shp_w3;
    uint32_t src_shp_w4;
    uint32_t src_shp_thr;
    uint32_t src_shp_div;
    uint32_t src_shp_l;
    uint32_t src_shp_c;
};

typedef struct DppFrameImpl_t {
    AlgorithmType type;

    // buffer information
    dpp::Buffer::SharedPtr output_buffer;
    uint32_t output_width;
    uint32_t output_height;

    // Downscale buffer information.
    // DppBufferInfo* downscale_info;
    dpp::Buffer::SharedPtr downscale;
    uint32_t downscale_width;
    uint32_t downscale_height;

    struct adas_output* adas_output;

    struct timeval pts;

    uint64_t exp_start;
    uint64_t exp_end;

    // for 3dnr
    struct dpp_sharpness sharpness;
    uint32_t noise[3];

    void* private_data;
} DppFrame;

/*
 * DppFrame interface
 *
 * @dpp_isp_data: ISP data
 * @dpp_dsp_data: DSP data
 * @buf_info:     buffer info form external create
 * @buffer:       cma buffer of dpp management
 * @width:        packet buffer width
 * @height:       packet buffer height
 */
DppRet dpp_frame_init(DppFrame** frame);
DppRet dpp_frame_deinit(DppFrame* frame);

void dpp_frame_set_output_buffer(DppFrame* frame, dpp::Buffer::SharedPtr buffer);
void dpp_frame_set_output_width(DppFrame* frame, uint32_t width);
void dpp_frame_set_output_height(DppFrame* frame, uint32_t height);
void dpp_frame_set_private_data(DppFrame* frame, void* data);
void dpp_frame_set_type(DppFrame* frame, AlgorithmType type);
void dpp_frame_set_adas_output(DppFrame* frame, struct adas_output* adas_output);
void dpp_frame_set_pts(DppFrame* frame, struct timeval pts);
void dpp_frame_set_noise(DppFrame* frame, uint32_t* noise);
void dpp_frame_set_sharpness(DppFrame* frame, struct dpp_sharpness* sharpness);

dpp::Buffer::SharedPtr dpp_frame_get_output_buffer(const DppFrame* frame);
uint32_t dpp_frame_get_output_width(const DppFrame* frame);
uint32_t dpp_frame_get_output_height(const DppFrame* frame);
void* dpp_frame_get_private_data(const DppFrame* frame);
AlgorithmType dpp_frame_get_type(const DppFrame* frame);
struct adas_output* dpp_frame_get_adas_output(const DppFrame* frame);
struct timeval dpp_frame_get_pts(const DppFrame* frame);
void dpp_frame_get_noise(const DppFrame* frame, uint32_t* noise);
void dpp_frame_get_sharpness(const DppFrame* frame, struct dpp_sharpness* sharpness);

// Exposure timestamp
uint64_t dpp_frame_get_exp_start(const DppFrame* frame);
uint64_t dpp_frame_get_exp_end(const DppFrame* frame);
void dpp_frame_set_exp_start(DppFrame* frame, uint64_t exp_start);
void dpp_frame_set_exp_end(DppFrame* frame, uint64_t exp_end);

// Downscale buffer information.
uint32_t dpp_frame_get_downscale_width(const DppFrame* frame);
uint32_t dpp_frame_get_downscale_height(const DppFrame* frame);
void dpp_frame_set_downscale_width(DppFrame* frame, uint32_t width);
void dpp_frame_set_downscale_height(DppFrame* frame, uint32_t height);
dpp::Buffer::SharedPtr dpp_frame_get_downscale(const DppFrame* frame);
void dpp_frame_set_downscale(DppFrame* frame, dpp::Buffer::SharedPtr buffer);

#endif /*__DPP_FRAME_H__*/
