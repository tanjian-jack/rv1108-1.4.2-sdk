/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_CORE_CORE_H_
#define DPP_CORE_CORE_H_

#include "error.h"

#include "algo/algorithm_type.h"
#include "base/synchronized_list.h"
#include "base/thread.h"
#include "device/dsp_device.h"
#include "mm/buffer.h"
#include "task/legacy_task.h"

namespace rockchip {
namespace dpp {

class DppCore {
 public:
    DppCore();
    virtual ~DppCore();

    DppRet init(AlgorithmType type);
    DppRet start();
    DppRet stop();
    DppRet put_packet(DppPacket* packet, bool need_process);
    DppRet get_packet(DppPacket** packet);
    DppRet put_frame(DppFrame* frame);
    DppRet get_frame(DppFrame** frame);

    const int dsp_fd(void) const {
        return dsp_->fd();
    }

    ThreadStatus ProcessorStatus(void) const {
        if (processor_)
            return processor_->status();

        return kThreadUninited;
    }

    DppRet Process(LegacyTask::SharedPtr task) {
        // Readsense algorithms call DSP ioctl directly.
        if (algorithm_type_ == kFaceDetection   ||
            algorithm_type_ == kRsFaceDetection ||
            algorithm_type_ == kRsBodyDetection ||
            algorithm_type_ == kRsFaceTracking)
            return kSuccess;
        else
            return dsp_->Process(task);
    }

    AlgorithmType algorithm_type(void) const {
        return algorithm_type_;
    }

    DppTable* table(void) {
        return table_;
    }

    Buffer::SharedPtr odt_buffer(void) {
        return odt_buffer_;
    }

 private:
    DspDevice::SharedPtr dsp_;
    Thread::SharedPtr processor_;
    AlgorithmType algorithm_type_;
    SynchronizedList<DppPacket*> pending_;
    SynchronizedList<DppFrame*> done_;
    DppTable* table_;

    Buffer::SharedPtr odt_buffer_;

    void clear();

    DppCore(const DppCore &);
    DppCore &operator=(const DppCore &);
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_CORE_CORE_H_
