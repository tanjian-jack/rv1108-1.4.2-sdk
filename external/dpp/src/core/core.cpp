/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "core.h"

#include <assert.h>
#include <string.h>

#include <mutex>

#include "config.h"

#if ENABLE_ODT_ADAS
#include "libodt_adas/OdtAdasWrapper.h"
#endif

#include "mm/buffer_pool.h"
#include "mm/cma_allocator.h"
#include "utils/logger.h"

using namespace rockchip::dpp;

static const int kMaxPacketListCapacity = 10;

#if ENABLE_ODT_ADAS
static void dpp_adas_input_release_callback(OdtFrame* input)
{
    DppPacket* packet = (DppPacket*)input->priv;
    dpp_packet_deinit(packet);
}

static void dpp_adas_request_work_callback(OdtPrepareData* prepare_data)
{
    DppCore* dpp = (DppCore*)prepare_data->priv;
    assert(dpp);

    DppPacket* packet;
    dpp_packet_init(&packet, OdtWrapper_getCurrentImageWidth(),
                    OdtWrapper_getCurrentImageHeight());

    dpp_packet_set_input_buffer(packet, dpp->odt_buffer());
    dpp_packet_set_odt_enabled(packet, true);
    // dpp_packet_set_adas_mode(packet, ADASWrapper_nativeGetDetectorMode());
    dpp->put_packet(packet, false);
}
#endif

void* dpp_core_processor(void* data)
{
    DppCore* core = (DppCore*)data;
    struct timeval t1, t2;

    DppPacket* packet = nullptr;
    DppFrame* frame = nullptr;
    bool waiting_frame = false;

    while (kThreadRunning == core->ProcessorStatus()) {
        if (kSuccess == core->get_packet(&packet) && packet) {
            gettimeofday(&t1, nullptr);

            // Preparing task
            LegacyTask::SharedPtr task(new LegacyTask(core->algorithm_type(),
                                                      core->table(), packet));
            task->Prepare(core->dsp_fd());

            // Processing task
            if (kSuccess != core->Process(task)) {
                DPPE("DSP work timeout, maybe DSP is dead.\n");
                assert(0);
            }

            DppFrame* frame = dpp_table_get_frame(core->table(),
                                                  core->algorithm_type(),
                                                  task->count(),
                                                  &task->dsp_params()->algo_params);
            core->put_frame(frame);

            gettimeofday(&t2, nullptr);

            uint32_t cost_time = (t2.tv_sec - t1.tv_sec) * 1000 +
                               (t2.tv_usec - t1.tv_usec) / 1000;
            if (cost_time > 180)
                DPPW("DSP processing cost too much time, %dms.\n", cost_time);
        }
    }

    return nullptr;
}

namespace rockchip  {
namespace dpp {

DppCore::DppCore() : algorithm_type_(kDummyAlgorithm) {}

DppRet DppCore::init(AlgorithmType type)
{
    algorithm_type_ = type;

    dsp_ = std::make_shared<DspDevice>();

    DppRet ret = dpp_table_init(&table_);
    if(kSuccess != ret) {
        DPPE("dpp_table_init failed\n");
        clear();
        return ret;
    }

#if ENABLE_ODT_ADAS
    if (algorithm_type() == kFrontCollisionWarning) {
        int mode = ODT_MODE_FCW_DAY;
        int width = OdtWrapper_getRequestImageWidth(mode);
        int height = OdtWrapper_getRequestImageHeight(mode);
        OdtWrapper_initizlize(width, height, mode);

        int horizon_line, head_line;
        dpp_adas_get_calibrate_line(&horizon_line, &head_line);
        OdtWrapper_setCalibrateLine(horizon_line, head_line);

        OdtWrapper_setReleaseBufCb(dpp_adas_input_release_callback);
        OdtWrapper_setRequestDspCb(dpp_adas_request_work_callback);

        int size = OdtWrapper_getAllocDataSize();
        odt_buffer_ = std::shared_ptr<Buffer>(CmaAlloc(size));

        OdtPrepareData prepare_data;
        prepare_data.data = odt_buffer_->address();
        prepare_data.size = size;
        prepare_data.priv = this;
        OdtWrapper_setAllocData(&prepare_data);
    }
#endif

    return kSuccess;
}

DppCore::~DppCore()
{
    clear();
}

void DppCore::clear()
{
    assert(pending_.size() == 0);

    DppFrame* frame = nullptr;
    done_.lock();
    while (done_.size()) {
        frame = done_.back();
        done_.pop_back();
        dpp_frame_deinit(frame);
        DPPW("unprocessed frame in list, reamin=%d\n", done_.size());
    }
    done_.unlock();

#if ENABLE_ODT_ADAS
    if (algorithm_type() == kFrontCollisionWarning)
        OdtWrapper_release();
    odt_buffer_.reset();
#endif

    // Release table buffer for frame buffer reference.
    if (table_) {
        dpp_table_buffer_release(table_, algorithm_type());
        dpp_table_deinit(table_);
    }
}

DppRet DppCore::start() {
    processor_ = std::make_shared<Thread>("dpp_processor",
                                          dpp_core_processor, this);

#if ENABLE_ODT_ADAS
    if (algorithm_type() == kFrontCollisionWarning)
        OdtWrapper_setDetectorStatus(ODT_STATUS_ON);
#endif

    return kSuccess;
}

DppRet DppCore::stop()
{
    processor_->set_status(kThreadStopping);

    pending_.signal();

    // Destory processor thread.
    processor_->join();
    processor_.reset();

#if ENABLE_ODT_ADAS
    if (algorithm_type() == kFrontCollisionWarning)
        OdtWrapper_setDetectorStatus(ODT_STATUS_OFF);
#endif

    // Notify get_frame
    done_.signal();

    // Drop all unprocessed packets when DPP has been stopped.
    DppPacket* packet = nullptr;
    pending_.lock();
    while (pending_.size()) {
        packet = pending_.back();
        pending_.pop_back();
        dpp_packet_deinit(packet);
        packet = nullptr;
        DPPW("unprocessed packets in list, remain=%d\n", pending_.size());
    }
    pending_.unlock();

    return kSuccess;
}

DppRet DppCore::put_packet(DppPacket* packet, bool need_process)
{
    if (!packet) {
        DPPE("invalid input packet 0x%x \n", packet);
        return kErrorPtr;
    }

    if (processor_->status() != kThreadRunning) {
        DPPE("DPP work thread is not in running status, drop this packet\n");
        return kSuccess;
    }

#if ENABLE_ODT_ADAS
    if (algorithm_type() == kFrontCollisionWarning) {
        if (dpp_packet_get_ldw_enabled(packet) && dpp_packet_get_odt_enabled(packet)) {
            DPPE("Cannot support LDW and FCW at the same time currently.\n");
            dpp_packet_deinit(packet);
            return kFailure;
        }

        // ODT need some preparing work to do.
        if (dpp_packet_get_odt_enabled(packet) && need_process) {
            Buffer::SharedPtr input_buffer = dpp_packet_get_input_buffer(packet);
            OdtFrame input;
            input.image = input_buffer->address();
            input.width = dpp_packet_get_input_width(packet);
            input.height = dpp_packet_get_input_height(packet);
            input.priv = packet;
            OdtWrapper_setDetectorMode(dpp_packet_get_adas_mode(packet));
            OdtWrapper_taskPrepare(&input, 3);

            return kSuccess;
        }
    }
#endif

    std::lock_guard<std::mutex> lock(pending_.mutex());

    pending_.push_back(packet);

    if (pending_.size() > kMaxPacketListCapacity) {
        // Too many packets in list is waiting for processing, we should drop
        // the oldest one.
        // printf("Drop an oldest packet, type=%d.\n", algorithm_type());

        DppPacket* oldest = pending_.front();
        pending_.pop_front();
        dpp_packet_deinit(oldest);
    }

    pending_.signal();

    return kSuccess;
}

DppRet DppCore::get_packet(DppPacket** packet)
{
    if(!packet) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return kErrorPtr;
    }

    std::unique_lock<std::mutex> lock(pending_.mutex());

    if (0 == pending_.size()) {
        struct timeval t1, t2;
        gettimeofday(&t1, nullptr);
        if (pending_.wait_for(lock, 5) == kErrorTimeout) {
            DPPE("Wait packet from PU timeout.\n");
            *packet = nullptr;
            return kErrorTimeout;
        }
        gettimeofday(&t2, nullptr);

        uint32_t cost_time = (t2.tv_sec - t1.tv_sec) * 1000 +
                             (t2.tv_usec - t1.tv_usec) / 1000;
        if (cost_time > 120 && algorithm_type() != kFrontCollisionWarning)
            DPPW("Wait packet too much time, %dms, packet_count=%d.\n",
                 cost_time, pending_.size());
    }

    if (pending_.size() == 0) {
        *packet = nullptr;
        DPPW("Pending list has been signaled but nothing in list\n");
        return kSuccess;
    }

    DppPacket* first = pending_.front();
    pending_.pop_front();

    *packet = first;
    return kSuccess;
}

DppRet DppCore::put_frame(DppFrame* frame)
{
    if(!frame) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return kErrorPtr;
    }

    std::lock_guard<std::mutex> lock(done_.mutex());
    done_.push_back(frame);
    done_.signal();

    return kSuccess;
}

DppRet DppCore::get_frame(DppFrame** frame)
{
    if(!frame) {
        DPPE("Invalid parameter, func=%s\n", __func__);
        return kErrorPtr;
    }

    std::unique_lock<std::mutex> lock(done_.mutex());

    if (done_.size() == 0) {
        if (done_.wait_for(lock, 5) == kErrorTimeout) {
            DPPE("Get frame from dpp timeout, algorithm_type=%d.\n", algorithm_type_);
            *frame = nullptr;
            return kErrorTimeout;
        }
    }

    if (done_.size() == 0) {
        *frame = nullptr;
        DPPW("Done list has been signaled but nothing in list\n");
        return kSuccess;
    }

    DppFrame* first = done_.front();
    done_.pop_front();

    *frame = first;
    return kSuccess;
}

} // namespace dpp
} // namespace rockchip
