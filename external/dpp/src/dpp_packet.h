/*
 *  Copyright (c) 2016 Rockchip Electronics Co. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __DPP_PACKET_H__
#define __DPP_PACKET_H__

#include <stdint.h>
#include <sys/time.h>

#include "common/error.h"
#include "mm/buffer.h"

using namespace rockchip;

typedef struct DppPacketImpl_t {
    dpp::Buffer::SharedPtr input_buffer;
    uint32_t input_width;
    uint32_t input_height;
    uint32_t output_width;
    uint32_t output_height;

    uint32_t adas_mode;

    int idc_enabled;
    int nr_enabled;
    int ldw_enabled;
    int odt_enabled;

    uint32_t downscale_width;
    uint32_t downscale_height;
    int downscale_enabled;

    void* params;
    uint32_t params_size;
    uint32_t noise[3];
    struct timeval pts;
    uint64_t exp_start;
    uint64_t exp_end;

    void* private_data;
} DppPacket;

DppRet dpp_packet_init(DppPacket** packet, uint32_t width, uint32_t height);
DppRet dpp_packet_deinit(DppPacket* packet);

void dpp_packet_set_input_buffer(DppPacket* packet, dpp::Buffer::SharedPtr buffer);
void dpp_packet_set_input_width(DppPacket* packet, uint32_t width);
void dpp_packet_set_input_height(DppPacket* packet, uint32_t height);
void dpp_packet_set_output_width(DppPacket* packet, uint32_t width);
void dpp_packet_set_output_height(DppPacket* packet, uint32_t height);
void dpp_packet_set_params(DppPacket* packet, void* params, uint32_t size);
void dpp_packet_set_private_data(DppPacket* packet, void *data);
void dpp_packet_set_dvs_remap(DppPacket* packet, uint32_t remap);
void dpp_packet_set_adas_mode(DppPacket* packet, uint32_t mode);
void dpp_packet_set_pts(DppPacket* packet, struct timeval pts);
void dpp_packet_set_noise(DppPacket* packet, uint32_t* noise);
void dpp_packet_set_idc_enabled(DppPacket* packet, int enabled);
void dpp_packet_set_nr_enabled(DppPacket* packet, int enabled);
void dpp_packet_set_odt_enabled(DppPacket* packet, int enabled);
void dpp_packet_set_ldw_enabled(DppPacket* packet, int enabled);

dpp::Buffer::SharedPtr dpp_packet_get_input_buffer(const DppPacket* packet);
uint32_t dpp_packet_get_input_width(const DppPacket* packet);
uint32_t dpp_packet_get_input_height(const DppPacket* packet);
uint32_t dpp_packet_get_output_width(const DppPacket* packet);
uint32_t dpp_packet_get_output_height(const DppPacket* packet);
unsigned int dpp_packet_get_output_width(const DppPacket* packet);
unsigned int dpp_packet_get_output_height(const DppPacket* packet);
uint32_t dpp_packet_get_adas_mode(const DppPacket* packet);
unsigned int dpp_packet_get_buf_size(const DppPacket* packet);
void *dpp_packet_get_private_data(const DppPacket* packet);
uint32_t dpp_packet_get_dvs_remap(const DppPacket* packet);
DppRet dpp_packet_get_params(const DppPacket* packet, void **params, uint32_t* size);
struct timeval dpp_packet_get_pts(const DppPacket* packet);
DppRet dpp_packet_get_noise(const DppPacket* packet, uint32_t* noise);
int dpp_packet_get_idc_enabled(const DppPacket* packet);
int dpp_packet_get_nr_enabled(const DppPacket* packet);
int dpp_packet_get_odt_enabled(const DppPacket* packet);
int dpp_packet_get_ldw_enabled(const DppPacket* packet);

// Exposure timestamp
uint64_t dpp_packet_get_exp_start(const DppPacket* packet);
uint64_t dpp_packet_get_exp_end(const DppPacket* packet);
void dpp_packet_set_exp_start(DppPacket* packet, uint64_t exp_start);
void dpp_packet_set_exp_end(DppPacket* packet, uint64_t exp_end);

// Downscale information
uint32_t dpp_packet_get_downscale_width(const DppPacket* packet);
uint32_t dpp_packet_get_downscale_height(const DppPacket* packet);
int dpp_packet_get_downscale_enabled(const DppPacket* packet);
void dpp_packet_set_downscale_width(DppPacket* packet, uint32_t width);
void dpp_packet_set_downscale_height(DppPacket* packet, uint32_t height);
void dpp_packet_set_downscale_enabled(DppPacket* packet, int enabled);

#endif /*__DPP_PACKET_H__*/
