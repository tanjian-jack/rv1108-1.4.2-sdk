 /*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dsp_device.h"

#include "dsp/dsp_ioctl.h"
#include "utils/logger.h"

namespace rockchip {
namespace dpp {

const char* rockchip::dpp::DspDevice::kName = "/dev/dsp";

DppRet DspDevice::Process(DspTask::SharedPtr task)
{
    DppRet ret = kFailure;

    // Queue a work request to DSP, then waiting to get the result.
    ioctl(fd(), DSP_IOC_QUEUE_WORK, &task->dsp_params()->work);
    ioctl(fd(), DSP_IOC_DEQUEUE_WORK, &task->dsp_params()->work);

    if(task->dsp_params()->work.result != DSP_WORK_SUCCESS) {
        DPPE("work failed, id=0x%08x, result=0x%08x\n",
             task->id(), task->dsp_params()->work.result);
        ret = kErrorDevice;
    } else {
        ret = kSuccess;
    }

    return ret;
}

} // namespace dpp
} // namespace rockchip