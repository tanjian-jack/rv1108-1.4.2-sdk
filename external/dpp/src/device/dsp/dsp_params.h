/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_HAL_DSP_PARAMS_H_
#define DPP_HAL_DSP_PARAMS_H_

#include "dsp_ioctl.h"

#include "algo/adas/adas.h"
#include "algo/idc/idc.h"
#include "algo/nr/nr.h"

union algorithm_params {
    struct nr_params nr;
    struct idc_params idc;
    struct adas_params adas;
};

struct dsp_params {
    struct dsp_user_work work;
    union algorithm_params algo_params;
};

#endif // DPP_HAL_DSP_PARAMS_H_


