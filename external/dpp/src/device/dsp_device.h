 /*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_DEVICE_DSP_DEVICE_H_
#define DPP_DEVICE_DSP_DEVICE_H_

#include "device.h"
#include "error.h"

#include "task/dsp_task.h"
#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

class DspDevice : public Device {
 public:
  DspDevice() : Device(kName) {}
  virtual ~DspDevice() = default;

  DPP_DECLARE_SHARED_PTR(DspDevice);

  virtual DppRet Process(DspTask::SharedPtr task);

  static const char* kName;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_DEVICE_DSP_DEVICE_H_
