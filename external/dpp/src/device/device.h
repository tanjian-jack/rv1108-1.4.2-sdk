/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_DEVICE_DEVICE_H_
#define DPP_DEVICE_DEVICE_H_

#include <fcntl.h>
#include <unistd.h>

#include <string>

#include "error.h"

#include "utils/logger.h"

#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

class Device {
public:
    Device(const char* name) : name_(name), fd_(-1) {
        Open();
    }

    virtual ~Device() {
        Close();
    }

    DPP_DECLARE_SHARED_PTR(Device);

    DppRet Open(void) {
        fd_ = open(name_.c_str(), O_RDWR);
        if (fd_ < 0) {
            DPPE("Open device failed, name=%s\n", name_.c_str());
            return kErrorDevice;
        }

        return kSuccess;
    }

    void Close(void) {
        if (fd_ >= 0)
            close(fd_);
        fd_ = -1;
    }

    int fd(void) const {
        return fd_;
    }

    const std::string& name(void) const {
        return name_;
    }

private:
    std::string name_;
    int fd_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_DEVICE_DEVICE_H_
