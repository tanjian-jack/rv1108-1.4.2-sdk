/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_BASE_SYNCHRONIZED_LIST_H_
#define DPP_BASE_SYNCHRONIZED_LIST_H_

#include <condition_variable>
#include <list>
#include <mutex>

#include "common/error.h"

namespace rockchip {
namespace dpp {

template <class T>
class SynchronizedList {
 public:
    SynchronizedList() {}
    virtual ~SynchronizedList() {}

    const int size(void) const {
        return list_.size();
    }

    void pop_back(void) {
        list_.pop_back();
    }

    void push_back(T& value) {
        list_.push_back(value);
    }

    T& back(void) {
        return list_.back();
    }

    T& front(void) {
        return list_.front();
    }

    void pop_front() {
        list_.pop_front();
    }

    void push_front(T& value) {
        list_.push_front(value);
    }

    void lock(void) {
        mutex_.lock();
    }

    void unlock(void) {
        mutex_.unlock();
    }

    void signal(void) {
        cond_.notify_all();
    }

    void wait(std::unique_lock<std::mutex>& lock) {
        cond_.wait(lock);
    }

    bool empty(void) const {
        return list_.empty();
    }

    DppRet wait_for(std::unique_lock<std::mutex>& lock, uint32_t seconds) {
        if (cond_.wait_for(lock, std::chrono::seconds(seconds)) ==
            std::cv_status::timeout)
            return kErrorTimeout;
        else
            return kSuccess;
    }

    std::mutex& mutex(void) {
        return mutex_;
    }

 private:
    std::list<T> list_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_BASE_SYNCHRONIZED_LIST_H_