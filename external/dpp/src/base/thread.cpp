 /*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "thread.h"

#include <sys/prctl.h>

#include "utils/logger.h"

namespace rockchip {
namespace dpp {

// Thread stack size 128K.
const size_t Thread::kThreadStackSize = 128 * 1024;

// Use a thread function wrapper to set thread name.
void* thread_func_wrapper(void* data)
{
    Thread* thread = (Thread*)data;

    prctl(PR_SET_NAME, thread->name().c_str(), 0, 0, 0);
    return thread->func_(thread->arg_);
}

Thread::Thread(const char* name, pthread_func_t func, void* arg)
    : name_(name), func_(func), arg_(arg),
      tid_(0), status_(kThreadUninited)
{
    pthread_attr_t attr;

    if (pthread_attr_init(&attr)) {
        DPPE("pthread_attr_init failed!\n");
        return;
    }
    if (pthread_attr_setstacksize(&attr, Thread::kThreadStackSize)) {
        DPPE("pthread_attr_setstacksize failed!\n");
        goto out;
    }

    status_ = kThreadRunning;
    if (pthread_create(&tid_, &attr, thread_func_wrapper, this)) {
        status_ = kThreadUninited;
        DPPE("pthread create failed!\n");
    }

out:
    if (pthread_attr_destroy(&attr))
        DPPE("pthread_attr_destroy failed!\n");
}

Thread::~Thread()
{
    if (joinable())
        std::terminate();
}

} // namespace dpp
} // namespace rockchip