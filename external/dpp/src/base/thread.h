 /*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_BASE_THREAD_H_
#define DPP_BASE_THREAD_H_

#include <pthread.h>

#include <string>

#include "common/definition_magic.h"

namespace rockchip {
namespace dpp {

typedef void* (*pthread_func_t)(void* arg);

enum ThreadStatus {
    kThreadUninited = 1,
    kThreadRunning  = 2,
    kThreadWaiting  = 3,
    kThreadStopping = 4,
};

class Thread {
 public:
    Thread(Thread&) = delete;
    Thread(const Thread&) = delete;
    Thread() : tid_(0), status_(kThreadUninited) {}
    explicit Thread(const char* name, pthread_func_t func, void* arg);
    virtual ~Thread();

    DPP_DECLARE_SHARED_PTR(Thread);

    bool joinable(void) const { return tid_ != 0; }

    void join(void) {
        pthread_join(tid_, nullptr);
        tid_ = 0;
    }

    void detach(void) {
        pthread_detach(tid_);
        tid_ = 0;
    }

    const std::string& name(void) const {
        return name_;
    }

    void set_status(const ThreadStatus status) {
        status_ = status;
    }

    const ThreadStatus status(void) const {
        return status_;
    }

    friend void* thread_func_wrapper(void* data);

 private:
    static const size_t kThreadStackSize;
    pthread_t tid_;
    ThreadStatus status_;
    std::string name_;
    pthread_func_t func_;
    void* arg_;
};

} // namespace dpp
} // namespace rockchip

#endif // DPP_BASE_THREAD_H_