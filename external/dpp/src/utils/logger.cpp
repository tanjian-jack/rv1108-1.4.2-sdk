/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "logger.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>

namespace rockchip {

#define LOGGER_MAX_LINE_SIZE 1024

Logger::DebugLevel Logger::_debug_level = Logger::DebugLevel(DPP_DEBUG_DEFAULT_LEVEL);

void Logger::Error(const char* format, ...)
{
    char line[LOGGER_MAX_LINE_SIZE] = { 0 };
    va_list v;

    va_start(v, format);
    vsnprintf(line, sizeof(line), format, v);
    va_end(v);

    if (_debug_level >= kErrorLevel)
        printf("DPPE: %s", line);
}

void Logger::Warning(const char* format, ...)
{
    char line[LOGGER_MAX_LINE_SIZE] = { 0 };
    va_list v;

    va_start(v, format);
    vsnprintf(line, sizeof(line), format, v);
    va_end(v);

    if (_debug_level >= kWarningLevel)
        printf("DPPW: %s", line);
}

void Logger::Debug(const char* format, ...)
{
    char line[LOGGER_MAX_LINE_SIZE] = { 0 };
    va_list v;

    va_start(v, format);
    vsnprintf(line, sizeof(line), format, v);
    va_end(v);

    if (_debug_level >= kDebugLevel)
        printf("DPPD: %s", line);
}

void Logger::Info(const char* format, ...)
{
    char line[LOGGER_MAX_LINE_SIZE] = { 0 };
    va_list v;

    va_start(v, format);
    vsnprintf(line, sizeof(line), format, v);
    va_end(v);

    if (_debug_level >= kInfoLevel)
        printf("DPPI: %s", line);
}

} // namespace rockchip
