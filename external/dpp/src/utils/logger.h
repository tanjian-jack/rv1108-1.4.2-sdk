/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_UTILS_LOGGER_H_
#define DPP_UTILS_LOGGER_H_

#ifdef DEBUG
#define DPP_DEBUG_DEFAULT_LEVEL 2
#else
#define DPP_DEBUG_DEFAULT_LEVEL 0
#endif

namespace rockchip {

class Logger {
public:
    Logger();
    ~Logger();

    typedef enum {
        kErrorLevel    = 0,
        kWarningLevel  = 1,
        kDebugLevel    = 2,
        kInfoLevel     = 3,
    } DebugLevel;

    static void Error(const char* format, ...);
    static void Warning(const char* format, ...);
    static void Debug(const char* format, ...);
    static void Info(const char* format, ...);

    static DebugLevel debug_level(void) {
        return _debug_level;
    }

    static void set_debug_level(const DebugLevel level) {
        _debug_level = level;
    }

private:
    static DebugLevel _debug_level;
};

#define DPPE rockchip::Logger::Error
#define DPPD rockchip::Logger::Debug
#define DPPW rockchip::Logger::Warning
#define DPPI rockchip::Logger::Info

#define debug_func_enter() DPPI("%s enter\n", __func__)
#define debug_func_exit()  DPPI("%s exit\n", __func__)

} // namespace rockchip

#endif // DPP_UTILS_LOGGER_H_
