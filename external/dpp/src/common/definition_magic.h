/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_COMMON_DEFINITION_MAGIC_H_
#define DPP_COMMON_DEFINITION_MAGIC_H_

#include <memory>

#define DPP_DECLARE_CREATE_METHOD(__TYPE__)         \
  static __TYPE__* create() {                       \
    __TYPE__* pRet = new __TYPE__();                \
    if (pRet && !pRet->init()) {                    \
      return pRet;                                  \
    } else {                                        \
      if (pRet)                                     \
        delete pRet;                                \
      return NULL;                                  \
    }                                               \
  }

#define DPP_DECLARE_SHARED_PTR(__TYPE__)            \
  typedef std::shared_ptr<__TYPE__> SharedPtr


#endif // DPP_COMMON_DEFINITION_MAGIC_H_