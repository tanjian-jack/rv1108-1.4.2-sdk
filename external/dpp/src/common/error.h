/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ERROR_H_
#define DPP_ERROR_H_

typedef enum {
    kSuccess        =  0,
    kFailure        = -1,

    kErrorUnknown   = -2,
    kErrorPtr       = -3,
    kErrorMem       = -4,
    kErrorDevice    = -5,
    kErrorDequeue   = -6,
    kErrorTimeout   = -7,
    kErrorParams    = -8,
} DppRet;

#endif // DPP_ERROR_H_