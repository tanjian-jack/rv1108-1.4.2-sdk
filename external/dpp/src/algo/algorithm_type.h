/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_ALGORITHM_TYPE_H_
#define DPP_ALGO_ALGORITHM_TYPE_H_

enum AlgorithmType {
    kDummyAlgorithm                 = 0,
    kNoiseReduction               = 2,
    kImageDistortionCorrection      = 3,
    kDigitalVideoStabilization      = 4,
    kFrontCollisionWarning          = 5,
    kLaneDepartureWarning           = 6,
    kObjectDetection                = 7,
    kFaceDetection                  = 8,
    kRsFaceDetection                = 20,
    kRsFaceTracking                 = 21,
    kRsBodyDetection                = 22,
};

#endif // DPP_ALGO_ALGORITHM_TYPE_H_