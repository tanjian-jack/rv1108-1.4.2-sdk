/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_ADAS_ADAS_H_
#define DPP_ALGO_ADAS_ADAS_H_

#include <stdint.h>

#define ODT_MAX_OBJECT       8
#define ADAS_FCW_WIDTH       640
#define ADAS_FCW_HEIGHT      360

#define ADAS_LDW_WIDTH       320
#define ADAS_LDW_HEIGHT      240

enum adas_type {
    DPP_ADAS_LDW = 0, // car line detection
    DPP_ADAS_ODT,     // car block detection
};

enum adas_mode {
    ADAS_MODE_DAY   = 0,
    ADAS_MODE_NIGHT = 1,
};

// The struct should be the same as the struct defined in OdtWrapper.h
struct odt_object {
    int x;
    int y;
    int width;
    int height;
    float distance;
};

struct odt_output {
    int valid;
    int count;
    struct odt_object objects[ODT_MAX_OBJECT];
};

struct ldw_output {
    int valid;

    // 0 turn left; 1 run normal; 2 turn right
    int turn_flag;

    // [0][0] left; [0][1] right
    int end_points[4][2];
};

struct adas_output {
    struct ldw_output ldw;
    struct odt_output odt;
};

struct adas_params {
    uint32_t input;
    uint32_t ldw_enabled;
    uint32_t odt_enabled;
    uint32_t mode;
    uint32_t width;
    uint32_t height;
    struct adas_output output;
};

#ifdef __cplusplus
extern "C" {
#endif

extern int dpp_adas_set_calibrate_line(int horizon_line, int head_line);
extern int dpp_adas_get_calibrate_line(int* horizon_line, int* head_line);

#ifdef __cplusplus
}
#endif

#endif // DPP_ALGO_ADAS_ADAS_H_