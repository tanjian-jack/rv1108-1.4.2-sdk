/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "adas.h"

#if ENABLE_ODT_ADAS
#include "libodt_adas/OdtAdasWrapper.h"
#endif

static int g_adas_horizon_line = ADAS_FCW_HEIGHT / 2.0;
static int g_adas_head_line = 0.95f * ADAS_FCW_HEIGHT;

int dpp_adas_set_calibrate_line(int horizon_line, int head_line)
{
    if (horizon_line < 0 || head_line < 0)
        return -1;
    if (horizon_line > ADAS_FCW_HEIGHT || head_line > ADAS_FCW_HEIGHT)
        return -1;

    /* Save valid calibrate lines. */
    g_adas_horizon_line = horizon_line;
    g_adas_head_line = head_line;

#if ENABLE_ODT_ADAS
    /* Try to set calibrate lines to ODT library. */
    OdtWrapper_setCalibrateLine(g_adas_horizon_line, g_adas_head_line);
#endif

    return 0;
}

int dpp_adas_get_calibrate_line(int* horizon_line, int* head_line)
{
    if (!horizon_line || !head_line)
        return -1;

    *horizon_line = g_adas_horizon_line;
    *head_line = g_adas_head_line;

    return 0;
}
