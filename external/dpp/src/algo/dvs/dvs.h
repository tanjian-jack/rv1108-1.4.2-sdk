/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_DVS_DVS_H_
#define DPP_ALGO_DVS_DVS_H_

// DVS is abbreviation of digital video stabilization algorithm.
struct dvs_params {
    uint32_t input;
    uint32_t output;
    uint32_t dvs_map;
    uint32_t idc_map;
    uint32_t input_width;
    uint32_t input_height;
    uint32_t output_width;
    uint32_t output_height;
    uint32_t downscale;
    uint32_t downscale_width;
    uint32_t downscale_height;
    uint32_t downscale_enabled;
};

#endif // DPP_ALGO_DVS_DVS_H_