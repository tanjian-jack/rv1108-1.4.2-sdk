/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_NR_NR_H_
#define DPP_ALGO_NR_NR_H_

#include <stdint.h>

#define ISP_CONTROL_PARAMS_SIZE 1024

struct nr_sharp {
    uint32_t src_shp_w0;
    uint32_t src_shp_w1;
    uint32_t src_shp_w2;
    uint32_t src_shp_w3;
    uint32_t src_shp_w4;
    uint32_t src_shp_thr;
    uint32_t src_shp_div;
    uint32_t src_shp_l;
    uint32_t src_shp_c;
};

// 3DNR algorithm parameters.
struct nr_params {
    uint32_t nr_enabled;
    uint32_t idc_enabled;
    uint32_t width;
    uint32_t height;
    uint32_t curr_input;
    uint32_t curr_output;
    uint32_t curr_pk;
    uint32_t curr_vec;
    uint32_t curr_rtf;
    uint32_t prev_input;
    uint32_t prev_output;
    uint32_t prev_pk;
    uint32_t prev_rtf;
    uint32_t map_table;
    uint32_t curr_idc_input;
    uint32_t prev_idc_input;
    uint32_t frame_count;
    uint32_t noise_y;
    uint32_t noise_u;
    uint32_t noise_v;
    struct nr_sharp sharp;
};

#endif // DPP_ALGO_NR_NR_H_