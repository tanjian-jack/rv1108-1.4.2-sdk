/*
 * Copyright (c) 2018 Rockchip Electronics Co. Ltd.
 * Author: Zhichao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_FACE_FACE_H_
#define DPP_ALGO_FACE_FACE_H_

#define MAX_FACE_COUNT 10
#define MAX_FACE_POINT 21

struct face_rect {
    int left;
    int top;
    int right;
    int bottom;
};

struct face_point {
    float x;
    float y;
};

struct face {
    int id;
    int grade;
    struct face_rect rect;
    struct face_point landmark[MAX_FACE_POINT];
};

struct face_detection_output {
    int count;
    struct face faces[MAX_FACE_COUNT];
};

#endif // DPP_ALGO_FACE_FACE_H_
