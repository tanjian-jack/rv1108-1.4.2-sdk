/*
 * Copyright (c) 2017 Rockchip Electronics Co. Ltd.
 * Author: ZhiChao Yu <zhichao.yu@rock-chips.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DPP_ALGO_IDC_IDC_H_
#define DPP_ALGO_IDC_IDC_H_

#include <stdint.h>

// IDC is abbreviation of image distortion correction algorithm.
struct idc_params {
    uint32_t map_table;
    uint32_t input;
    uint32_t output;
    uint32_t width;
    uint32_t height;
    uint32_t enabled;
};

#endif // DPP_ALGO_IDC_IDC_H_