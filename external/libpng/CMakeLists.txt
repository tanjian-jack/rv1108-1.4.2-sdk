cmake_minimum_required(VERSION 2.8.11)

set(project_name libpng)
project(${project_name})

set(fake_target_name ${project_name})

add_custom_command(OUTPUT gen
    COMMAND  ${CMAKE_CURRENT_SOURCE_DIR}/cmake.sh ${fake_target_name}
    COMMENT "build ${project_name}"
)

add_custom_target(${fake_target_name} ALL DEPENDS gen)
add_dependencies(${fake_target_name} zlib)
