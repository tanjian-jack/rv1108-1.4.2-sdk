#! /bin/bash

current_path=$(pwd)

rebuild=no
fake_target=CMakeFiles/$1
if [ ! -e "$fake_target"  ]; then
    touch $fake_target
    rebuild=yes
fi

bash_path=$(dirname $BASH_SOURCE)
cd $bash_path
bash_path=$(pwd)
cd ../../../
source config/envsetup.sh
echo $bash_path
cd $bash_path

if [ "$rebuild" == "yes"  ]; then
    make clean
fi

cpu_cores=`cat /proc/cpuinfo | grep processor | wc -l`
make -j$cpu_cores LDFLAGS+=-ldl
make install

cd $current_path
