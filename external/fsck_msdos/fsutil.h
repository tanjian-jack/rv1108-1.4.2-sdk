#ifndef _FS_UTIL_H
#define _FS_UTIL_H

#define pwarn printf
#define pfatal printf

#include "fsck_alloc.h"

#define malloc(n) fsck_alloc(__func__, __LINE__, n)
#define calloc(a, b) fsck_alloc(__func__, __LINE__, a * b)
#define free(p) fsck_free(__func__, __LINE__, p)

#endif
