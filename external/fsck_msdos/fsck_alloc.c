#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

#include "fsck_alloc.h"

#if defined(FSCK_USE_MMAP) || defined(FSCK_USE_ION)

#define SIZE_MB(n)		(n << 20)
#define MAX_MMAP_BUFFER_SIZE	SIZE_MB(100)
#define MMAP_BUFFER_STEP	SIZE_MB(10)

static void *g_buffer = NULL;
static int g_size = 0;
static int g_offset = 0;
static int g_buf_fd = -1;

#ifdef FSCK_USE_ION
#include <ion/ion.h>

static int g_ion_fd = -1;
static int g_ion_hdl = -1;
#endif

static void __attribute__((constructor)) init_alloc(void) {
#ifndef FSCK_USE_ION
	g_buf_fd = open("/tmp/fsck.tmp", O_RDWR|O_CREAT|O_TRUNC, 0644);
	if (fd < 0)
		return;
	g_size = MAX_MMAP_BUFFER_SIZE;
	lseek(fd, g_size, SEEK_CUR);
	write(fd, "", 1);
#else // FSCK_USE_ION
	int ret;

	g_ion_fd = ion_open();
	if (g_ion_fd < 0)
		return;

	g_size = MAX_MMAP_BUFFER_SIZE;
	while (g_size > 0) {
		ret = ion_alloc(g_ion_fd, g_size, 0,
				ION_HEAP_TYPE_DMA_MASK, 0, &g_ion_hdl);
		if (!ret)
			break;
		g_size -= MMAP_BUFFER_STEP;
	}
	if (g_size <= 0)
		return;

	ret = ion_share(g_ion_fd, g_ion_hdl, &g_buf_fd);
	if (ret < 0)
		return;
#endif // FSCK_USE_ION

	g_buffer = mmap(NULL, g_size, PROT_READ|PROT_WRITE,
			MAP_SHARED, g_buf_fd, 0);
	if (g_buffer == (void *)-1)
		g_size = 0;

	if (g_size)
		fprintf(stderr, "%s, %d mmaped:%p(0x%x)\n",
			__func__, __LINE__, g_buffer, g_size);
}

static void __attribute__((destructor)) deinit_alloc(void) {
	if (g_size)
		munmap(g_buffer, g_size);

	if (g_buf_fd >= 0)
		close(g_buf_fd);

#ifdef FSCK_USE_ION
	if (g_ion_hdl >= 0)
		ion_free(g_ion_fd, g_ion_hdl);
	if (g_ion_fd >= 0)
		ion_close(g_ion_fd);
#endif
}

#define IS_FAT_RAW(func, size) (!strcmp(func, "_readfat"))
#define IS_FAT(func, size) (!strcmp(func, "readfat"))
#define IS_DIR_TO_DO(func, size) (!strcmp(func, "newDirTodo"))
#define IS_DIR_ENTRY(func, size) (!strcmp(func, "newDosDirEntr"))
#define IS_DOT_DOT(func, size) (!strcmp(func, "check_dot_dot"))

static int g_fat = 0;

void *do_alloc(const char *func, const int line, unsigned int size) {
	// mmaped buf ran out, fallback to malloc
	if (g_size - g_offset < size)
		goto malloc;

	// dot dot & fat raw buf would be freed soon, so bypass them
	if (IS_DOT_DOT(func, size) || IS_FAT_RAW(func, size))
		goto malloc;

	if (IS_FAT(func, size)) {
		// Backup FAT will be freed soon, so place it at the end
		if (g_fat)
			return g_buffer + g_size - size;

		g_fat = 1;
	}

	g_offset += size;

	return g_buffer + g_offset - size;
malloc:
	return malloc(size);
}

void do_free(const char *func, const int line, void *data) {
	// Almost all frees happen at the end, except for backup FAT.
	if (data >= g_buffer && data < (g_buffer + g_size))
		return;

	return free(data);
}

#else // defined(FSCK_USE_MMAP) || defined(FSCK_USE_ION)

void *do_alloc(const char *func, const int line, unsigned int size) {
	return malloc(size);
}

void do_free(const char *func, const int line, void *data) {
	free(data);
}

#endif // defined(FSCK_USE_MMAP) || defined(FSCK_USE_ION)

#ifdef FSCK_MEMORY_TRACE

static int total = 0;

struct buffer {
	int size;
	int data;
};

void *fsck_alloc(const char *func, const int line, unsigned int size) {
	struct buffer *buf;

	total += size;
	fprintf(stderr, "%20s[%d]:\tmalloc(%10d)\ttotal(%10d)\n",
		func, line, size, total);

	buf = do_alloc(func, line, sizeof(struct buffer) + size);
	buf->size = size;

	return &buf->data;
}

void fsck_free(const char *func, const int line, void *data) {
	struct buffer *buf = data - sizeof(buf->size);

	total -= buf->size;
	fprintf(stderr, "%20s[%d]:\t  free(%10d)\ttotal(%10d)\n",
		func, line, buf->size, total);

	return do_free(func, line, buf);
}

#else // FSCK_MEMORY_TRACE

void *__attribute__((weak)) fsck_alloc(const char *func, const int line, unsigned int size) {
	void *data = do_alloc(func, line, size);

	fprintf(stderr, "%20s[%d]:\tmalloc(%10d):%p\n",
		func, line, size, data);

	return data;
}

void __attribute__((weak)) fsck_free(const char *func, const int line, void *data) {
	fprintf(stderr, "%20s[%d]:\t  free:%p\n",
		func, line, data);

	return do_free(func, line, data);
}

#endif // FSCK_MEMORY_TRACE
