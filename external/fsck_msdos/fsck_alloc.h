#ifndef MALLOC_TEST_H
#define MALLOC_TEST_H

void *fsck_alloc(const char *func, const int line, unsigned int size);
void fsck_free(const char *func, const int line, void *data);

#endif
